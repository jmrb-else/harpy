# README

This is the project of putting all python plotting and analysis libraries together.

## Organization

### Main directory

This directory will contain the global scripts used in most tools.

### Task-specific subdirectories

Those subdirectories named after a project/task/study will contain the scripts developed to analyse data for that specific propouse, e.g., patchwork, lump, etc.

### `history` subdirectory

All those plotting and analysis scripts developed long before this repository was initialized, and have fulfilled their purpose, will be stored in this subdirectory for reference.

> *Good practice*: save each set of scripts in a folder named after the project/author, and a short `README` file describing the usage and/or referencing the bibliography (papers, books, thesis, etc.) where these tools were used.

### `devel` subdirectory

Place for:
- all those plotting and analyisis scripts queued to be included in the main directory, or task-specific directories,
- under development scripts,
- whatnot

*Good practice*: save each set of scripts in a folder named after the project/author, and a short `README` file describing the usage and/or referencing the bibliography (papers, books, thesis, etc.) where these tools were/are being used.

## Documentation

All modules, functions, classes, etc., will be documented with format for [sphinx](https://www.sphinx-doc.org). A good start is [this tutorial](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html), [this tutorial](https://matplotlib.org/sampledoc/), [this tutorial](https://docutils.sourceforge.io/rst.html), and mainly [this tutorial](https://numpydoc.readthedocs.io/en/latest/format.html).

## Recomendations for developers

Every new parameter added to the argument list of each function, as good practice, should be included in the list of parameters in the description of the function.