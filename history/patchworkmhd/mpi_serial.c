#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

/* A macro to check for MPI errors */
#define MPI_ERROR(command)\
                  {\
                    int mpires=command;\
                    if (mpires!=MPI_SUCCESS)\
                    {\
                       fprintf(stderr,\
                          "MPI ERROR DETECTED in FILE %s, LINE %d\n",\
                           __FILE__, __LINE__);\
                       MPI_Abort(MPI_COMM_WORLD, -1);\
                    }\
                  }

int main(int argc, char **argv)
{
  int mpirank=-1;
  int mpisize=-1;
  MPI_ERROR(MPI_Init(&argc, &argv));
  MPI_ERROR(MPI_Comm_rank(MPI_COMM_WORLD, &mpirank));
  MPI_ERROR(MPI_Comm_size(MPI_COMM_WORLD, &mpisize));

  int status;
  pid_t worker_pid;

  /* test to see if the script exists and is executable by us */
  if (access(argv[1], X_OK))
  {
    fprintf(stderr, "The script or program '%s' does not exist,\n"
                    "or is not executable.\n", argv[1]);
    MPI_Abort(MPI_COMM_WORLD,-1);
  }

  const int inum    = argc - 2;

//   if (inum < mpisize)
//   {
//     fprintf(stderr, "make sure #mpitasks < #jobs\n");
//     MPI_Abort(MPI_COMM_WORLD,-1);
//   }

  const char *comm  = argv[1];
  const int delta   = inum / mpisize;

  const int mystart = 1 + mpirank * delta;
  const int myend   = (mpirank != mpisize-1)  ?  ( mpirank + 1 ) * delta  : inum;

  /* local total number of strings */
  const int mylen   = myend - mystart + 1 ;

  char **mystring   = malloc(sizeof(char*) * (mylen + 2) );

  printf("mpirank = %d   mystart = %d,  myend = %d, mylen = %d, inum = %d , delta = %d\n", mpirank,mystart, myend, mylen, inum,delta);

  int j = 0;
  int i ;
  mystring[j++] = argv[1] ; 
  for(i = mystart; i <= myend; i++) {
    mystring[j++] = argv[i+1] ; 
  }
  mystring[j] = '\0' ;

  /* fork a process to do the actual work. */
  if( myend >= mystart ) { 
    worker_pid = fork();
    if (!worker_pid)
      {
	execv(comm,  &mystring[0]);
      }
    /* wait for the worker to finish */
    waitpid(worker_pid, &status,0);
  }

  free(mystring) ;

  MPI_ERROR(MPI_Barrier(MPI_COMM_WORLD));
  /* MUST be called */
  MPI_ERROR(MPI_Finalize());
  return 0;
}
