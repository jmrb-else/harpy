#!/usr/bin/env python2

from __future__ import division
from __future__ import print_function

from math import pi
import numpy as np
import h5py

#from scipy.interpolate import griddata
#from scipy.interpolate import interp1d

# import matplotlib
# import matplotlib.pyplot as plt 

import sys,os,getopt

# the following module allows one to start an ipyhton session at a given point
# in the file with the command IPython.embed(). this allows us to access the
# local functions and variables (closest thing to 'stop' in IDL).
#import IPython

def ph_dist(phi1, phi2):
    dist  = np.abs(phi1 - phi2)
    while dist > pi:
        dist = 2*pi - dist
    while dist <= -pi:
        dist = 2*pi + dist
    return dist


def interp_phi(phi0,itime=0, dirname=None, dirout=None, funcname='uu',
               userad=False):

    if dirname is None:
        dirname = os.getcwd()  # should tell me my current working directory,
                               # *not* where this script is located
    if dirout is None:
        dirout   = dirname

    timeid   = '%06d' %itime

    dumpfile = dirname + '/KDHARM0.' +  timeid + '.h5'
    radfile  = dirname + '/KDHARM0.RADFLUX.' +  timeid + '.h5'

    fullname    = dumpfile
    outfilename = dirname + '/KDHARM0.' +  timeid + '_ph.h5'
    if userad:
        fullname = radfile
        outfilename = dirname + '/KDHARM0.RADFLUX.' +  timeid + '_ph.h5'

    h5file   = h5py.File(fullname,'r')

    x1 = h5file['x1'].value
    x2 = h5file['x2'].value
    x3 = h5file['x3'].value
    time = h5file['Header/Grid/t'].value[0]

    func     = h5file[funcname].value

    h5file.close()

    N1 = len(x1[:,0,0])
    N2 = len(x1[0,:,0])

    func_out = np.zeros([N1,N2,1])
    rr_out   = np.zeros([N1,N2,1])
    th_out   = np.zeros([N1,N2,1])
    ph_out   = np.zeros([N1,N2,1])

    for j in range(N2):
        for i in range(N1):

            # we want to find phi1 and phi2 that are closest to phi0 for these
            # i,j

            dist            = np.abs(phi0 - x3[i,j,:])
            dist[dist > pi] = 2*pi - dist[dist > pi] 

            # k1 is the index of phi1, the closest point to phi0
            k1       = dist.argmin()
            dist[k1] = 1.e10
            # k2 is the index of phi2, the second closest point to phi0
            k2       = dist.argmin()

            phi1     = x3[i,j,k1]
            rr1      = x1[i,j,k1]
            f1       = func[i,j,k1]

            phi2     = x3[i,j,k2]
            rr2      = x1[i,j,k2]
            f2       = func[i,j,k2]

            f0   =    ph_dist(phi2, phi0)/ph_dist(phi2, phi1) * f1   \
                   +  ph_dist(phi1, phi0)/ph_dist(phi2, phi1) * f2

            rr0  = rr1 + (rr2-rr1)/ph_dist(phi2,phi1) * ph_dist(phi0,phi1)

            func_out[i,j,0] = f0
            rr_out[i,j,0]   = rr0
            th_out[i,j,0]   = x2[i,j,k1]
            ph_out[i,j,0]   = phi0


    outfile = h5py.File(outfilename, 'w')
    outfile[funcname] = func_out
    outfile['x1']     = rr_out
    outfile['x2']     = th_out
    outfile['x3']     = ph_out
    outfile['Header/Grid/t'] = np.array([time])
    outfile.close()

    return


def interp(phi0):
    nt = 1
    
    for itime in range(nt):

        interp_phi(phi0,itime=itime, dirname=None, dirout=None, funcname='uu',
                   userad=False)
        # interp_phi(phi0,itime=itime, dirname=None, dirout=None, funcname='rho',
        #            userad=True)

    return

def main(argv=None):

    if argv is None:
        argv = sys.argv

    def usage():
        sys.exit('Usage: %s [-h, --help]  <phi0>  <itime>  <funcname>  <userad>' % sys.argv[0] )

    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.GetoptError, err:
        print(str(err))
        usage()

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()

    if len(args) == 0:
        usage()

    phi0     = float(args[0])
    itime    = int(args[1])
    funcname = args[2]
    userad   = int(args[3])

    interp_phi(phi0,itime=itime, dirname=None, dirout=None, funcname=funcname,
               userad=userad)

if __name__ == "__main__":
    main()

