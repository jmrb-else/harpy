#include "defs.h"

#define SET_TSHRINK (0)

/* Added variables to compile in stand-alone interpolator */
void initialize_metric_routines(char *name);

//double nz_params[38];

// Whether or not to set s parameter for NZ-IZ transition Function to 12. from initial_bbh_separation/M
// Intended to fix the connection for 100M separation BHOG runs
#define NZ_IZ_TRANS_FIX (1)
#if( NZ_IZ_TRANS_FIX )
#  define SNEW 12.
#endif

// Define this local macros at the top of metric function in harm3d
// Should we fudge the IZ-NZ coordinate transformation well inside the
// horizon? yes (1), no (0)
#define FUDGE_COORD (1)
// What fraction of radius = m1 should we start fudging the coord. transf.
// where m1 is the BH mass passed to the IZ function.
#define FUDGE_FRAC (0.1)

// Verbosity level:                                                             
#define NO   (0)
#define LOW  (1)
#define HIGH (2)
//#define VERBOSE (LOW) // Now it is defined below on routine basis.

// For local Newton's solver:
 /* your choice of floating-point data type */
  #define FTYPE double

 // Defines the number of equations to be minimalized by the solver.
  #define NEWT_DIM 1

  #define MAX_NEWT_ITER 30     /* Max. # of Newton-Raphson iterations for find_root_2D(); */
  #define NEWT_TOL   1.0e-10    /* Min. of tolerance allowed for Newton-Raphson iterations */
  #define MIN_NEWT_TOL  1.0e-10    /* Max. of tolerance allowed for Newton-Raphson iterations */
  #define EXTRA_NEWT_ITER 2

 // The function passed to the Newton solver may be parametrized.
 // These parameters are stored in a vector of lenght PARAM_DIM.
  #define PARAM_DIM 4

// Prototype of local functions:
// Time functions and friends:
static void time_tc_etal_funcs_v1 (double time_tc_etal_args[4], double tc_phi_omega[3]);
static void time_r12_resid_jac_v1 (double time_r12_resid_jac_args[4], double R, double r12_resid_jac[2]);

static double time_r12dot_v1 (double m1, double m2, double R);

static void time_functions_opt_v4 (double time_functions_args[6], double time_fcns[19]);

static void func_1d_orig(FTYPE params[], FTYPE x[], FTYPE dx[], FTYPE resid[],
          FTYPE jac[][NEWT_DIM], FTYPE *f, FTYPE *df, int n);

static int general_newton_raphson( FTYPE params[], FTYPE x[], int n,
               void (*funcd) (FTYPE [], FTYPE [], FTYPE [], FTYPE [],
                    FTYPE [][NEWT_DIM], FTYPE *,
                    FTYPE *, int) );

// Spacetime functions and friends:


#if( SET_TSHRINK )
static void IZgab_transf_opt_v13_r12const ( double fcons_00, double IZ_fots_derivs[200], double t, double x, double y, double z, double m1, double cgret[4]);

static void IZgab_jac_opt_v13_r12const ( double fcons_00, double IZ_fots_derivs[200], double x, double y, double z, double m1, double cgret[16]);

#endif


/********
dyn_fullpn_nz_iz_2nd_gcov_func is based on Fullgab_func at 
./init_data/confcurvBBHID/maple/test_c-files/Profiling/Ricci_dr_v5/Fullgab_func_v11.c
********/

#define VERBOSE (HIGH) 
#define DEBUG_T (10756.6)
#define DEBUG_X (7.5)
#define DEBUG_Y (0.0)
#define DEBUG_Z (0.0)
#define EPS (1.0e-13)



/********
dyn_fullpn_nz_iz_2nd_gcov_func_setup() is based on time_functions_dr at 
./init_data/confcurvBBHID/maple/test-c-files/Profiling/Ricci_dr_v4a/time_functions_dr_v5.c
********/

//void dyn_fullpn_nz_iz_2nd_gcov_func_setup(double tt, struct of_bbh_traj *bbh_traj){
void set_bbh_traj_data(double tt, struct of_bbh_traj *bbh_traj) 
{

// BHB system: 
 const double m1 = m_bh1, m2 = m_bh2, b = initial_bbh_separation;

// Parameters used in the transition functions in order 
// to glue the metric together:
 /* double lambda, r1T, r2T; */
 /* double r1T0, r2T0, w1T0, w2T0, xNT0, wNT0; */
 /* double MM, bb, b2, b3, m1cube, m2cube, b3oM; // helper parameters */
 /* const double oo7 = 0.14285714285714285714285714285714286; // = 1.0/7.0; */
 /* const double PI = 3.1415926535897932384626433832795029; */

// Function argument arrays:
 double time_tc_etal_args[4];
 double time_r12_resid_jac_args[4]; 
 double time_functions_args[6]; 
 double tc_phi_omega[3];
 double time_fcns[19];  

// Trajectory functions (they depend on time only):
 double  t_c; 
 double  phi, omega;
 double  r12;
 double  r12dot;

// 1D Newton-Raphson's solution vector:
 double x_1D[1];
 int retval = 0;
 static unsigned int i_retval = 0;

#if(SET_TSHRINK)
// In order to set a static binary separation:
 static double omega_0, t_c_0;
#endif
 static unsigned short int local_first_call = 1;

 /* if( local_first_call ) { */
 /*   iz1_const = IZ_const_funcs_v0(m_bh1,m_bh2); */
 /*   iz2_const = IZ_const_funcs_v0(m_bh2,m_bh1); */
 /* } */


#if(SET_TSHRINK)
//Initial guess for the Newton's solver:
 r12 = b; 

 time_r12_resid_jac_args[0] = tt - t_shrink_bbh;
 time_functions_args[0] = tt;

 time_tc_etal_args[1] = time_r12_resid_jac_args[1] = time_functions_args[1] = m1;
 time_tc_etal_args[2] = time_r12_resid_jac_args[2] = time_functions_args[2] = m2;

 time_r12_resid_jac_args[3] = r12;
 time_tc_etal_args[3] = b;

// Calculated once per time step:

//Coalescence time, phase phi and frequency omega:
 if( local_first_call ) {
   time_tc_etal_args[0] = 0. ;
   time_tc_etal_funcs_v1 (time_tc_etal_args,tc_phi_omega);
   omega_0 = tc_phi_omega[2];
   t_c_0   = tc_phi_omega[0];
 }

// Note that the condition below introduces a kink in the omega and t_c
// functions. As a result the mixed time derivative stencils as applied to 
// metric derivatives are effectively modified, leading to a slowly divergent 
// approximation as a function of resolution. Therefore quantities like the
// Ricci scalar at tt = t_shrink_bbh will not show second order convergence. 
// At any other time the convergence is evident though. Since this happens
// only for a small range in time [t_shrink_bbh-dt,t_shrink_bbh+dt]; the
// calculated Ricci scalar is still small in that range when compared to
// other time intervals; and these errors do not propagate to future 
// calculations of the metric, it is safe to leave the logic below untouched. 
 if( tt < t_shrink_bbh ) {
   omega = omega_0;
   t_c   = t_c_0;
   phi = phi_0_bbh  + omega * tt;
   time_functions_args[4] = r12 = initial_bbh_separation;
   r12dot = 0.0;
 }
 else {
   time_tc_etal_args[0]       = tt - t_shrink_bbh;
   time_tc_etal_funcs_v1 (time_tc_etal_args,tc_phi_omega);
   t_c   = tc_phi_omega[0];
   phi   = tc_phi_omega[1];
   omega = tc_phi_omega[2];
   phi   += phi_0_bbh +  omega_0*t_shrink_bbh;

//Call Newton-Raphson solver to numerically obtain r12:
  x_1D[0] = r12; // Initial guess.
  retval = general_newton_raphson(time_r12_resid_jac_args, x_1D, 1, func_1d_orig);
  time_functions_args[4] = r12 = x_1D[0];
  r12dot = time_r12dot_v1 (m1,m2,r12);
 }
#else // SET_TSHRINK

//Initial guess for the Newton's solver:
 r12 = b; 

 time_r12_resid_jac_args[0] = tt;
 time_functions_args[0] = tt;

 time_tc_etal_args[1] = time_r12_resid_jac_args[1] = time_functions_args[1] = m1;
 time_tc_etal_args[2] = time_r12_resid_jac_args[2] = time_functions_args[2] = m2;

 time_r12_resid_jac_args[3] = r12;
 time_tc_etal_args[3] = b;

// Calculated once per time step:

   time_tc_etal_args[0]       = tt;
   time_tc_etal_funcs_v1 (time_tc_etal_args,tc_phi_omega);
   t_c   = tc_phi_omega[0];
   phi   = tc_phi_omega[1];
   omega = tc_phi_omega[2];

//Call Newton-Raphson solver to numerically obtain r12:
  x_1D[0] = r12; // Initial guess.
  retval = general_newton_raphson(time_r12_resid_jac_args, x_1D, 1, func_1d_orig);
  time_functions_args[4] = r12 = x_1D[0];
  r12dot = time_r12dot_v1 (m1,m2,r12);
#endif // SET_TSHRINK

 local_first_call = 0 ;
 
 if(retval > 0 && i_retval <= 10){
   i_retval++;
   fprintf(stderr,"Newton-Raphson failure. Returned %d on function %s at %s, line %d.\n",retval,__func__,  __FILE__, __LINE__);
 }

 time_functions_args[3] = omega;
 time_functions_args[5] = phi;


 time_functions_opt_v4 (time_functions_args, time_fcns);

 // Transiton function parameters. Now they are constants
 // but they can easily become functions of time here.
 /* MM = m1+m2; */
 /* m1cube = m1*m1*m1; */
 /* m2cube = m2*m2*m2; */
 /* bb = r12; */
 /* b2 = bb*bb; */
 /* b3 = b2*bb; */
 /* b3oM = b3/MM; */
 /* lambda = PI*sqrt(b3oM); */
 /* r1T = pow(m1cube*b2*b3oM,oo7); */
 /* r2T = pow(m2cube*b2*b3oM,oo7); */

 /* r1T0 = 0.256*r1T; */
 /* r2T0 = 0.256*r2T; */
 /* w1T0 = 3.17*pow(MM*MM*b2*b3,oo7); */
 /* w2T0 = w1T0; */

 /* xNT0 = 2.2*m2 - m1*bb/MM; */
 /* wNT0 = bb-2.2*MM; */

 bbh_traj->xi1x   = time_fcns[0];
 bbh_traj->xi1y   = time_fcns[1];
 bbh_traj->xi1z   = 0.;
 bbh_traj->xi2x   = time_fcns[2];
 bbh_traj->xi2y   = time_fcns[3];
 bbh_traj->xi2z   = 0.;
 bbh_traj->v1x    = time_fcns[4];
 bbh_traj->v1y    = time_fcns[5];
 bbh_traj->v1z    = 0.;
 bbh_traj->v2x    = time_fcns[6];
 bbh_traj->v2y    = time_fcns[7];
 bbh_traj->v2z    = 0.;
 bbh_traj->t_c    = t_c;
 bbh_traj->phi    = phi;
 bbh_traj->omega  = omega;
 bbh_traj->r12    = r12;
 bbh_traj->r12dot = r12dot;
 

  /* printf("Printing traj info in metric function\n"); */
  /* printf("bbh_traj->x1 %e %e %e\n",bbh_traj->xi1x,bbh_traj->xi1y,bbh_traj->xi1z); */
  /* printf("bbh_traj->x2 %e %e %e\n",bbh_traj->xi2x,bbh_traj->xi2y,bbh_traj->xi2z); */
  /* printf("t_c omega r12 r12dot %e %e %e %e\n",bbh_traj->t_c,bbh_traj->omega,bbh_traj->r12,bbh_traj->r12dot); */


  /* nz_params[ 0] = time_fcns[0];  // xi1x; */
  /* nz_params[ 1] = time_fcns[1];  // xi1y; */
  /* nz_params[ 2] = time_fcns[2];  // xi2x; */
  /* nz_params[ 3] = time_fcns[3];  // xi2y; */
  /* nz_params[ 4] = time_fcns[4];  // v1x; */
  /* nz_params[ 5] = time_fcns[5];  // v1y; */
  /* nz_params[ 6] = time_fcns[6];  // v2x; */
  /* nz_params[ 7] = time_fcns[7];  // v2y; */
  /* nz_params[ 8] = time_fcns[8];  // v1; */
  /* nz_params[ 9] = time_fcns[9];  // v2; */
  /* nz_params[10] = time_fcns[10]; // v12x; */
  /* nz_params[11] = time_fcns[11]; // v12y; */
  /* nz_params[12] = time_fcns[12]; // v12; */
  /* nz_params[13] = time_fcns[13]; // v1v2; // dot product of v1 and v2 */
  /* nz_params[14] = time_fcns[14]; // n12x;  */
  /* nz_params[15] = time_fcns[15]; // n12y;  */
  /* nz_params[16] = time_fcns[16]; // n12v12;  */
  /* nz_params[17] = time_fcns[17]; // n12v1;  */
  /* nz_params[18] = time_fcns[18]; // n12v2;  */
  /* nz_params[19] = t_c;  // time to merger from t=0 */
  /* nz_params[20] = phi;  // orbital phase change from t=0 */
  /* nz_params[21] = omega;// current orbital phase rate of change */
  /* nz_params[22] = r12;  // current separation */
  /* nz_params[23] = r12dot;  // current separation radial speed */
  /* nz_params[24] = 0.;   //  xi1z */
  /* nz_params[25] = 0.;   //  xi2z */
  /* nz_params[26] = 0.;   //  v1z */
  /* nz_params[27] = 0.;   //  v2z */
  /* nz_params[28] = 0.;   //  v12z */
  /* nz_params[29] = 0.;   //  n12z */
  /* nz_params[30] = r1T0;   // Inner1-Near radius trans. func. parameter  */
  /* nz_params[31] = w1T0;   // Inner1-Near width trans. func. parameter  */
  /* nz_params[32] = r2T0;   // Inner2-Near radius trans. func. parameter  */
  /* nz_params[33] = w2T0;   // Inner2-Near width trans. func. parameter  */
  /* nz_params[34] = xNT0;   // Near-Inner radius trans. func. parameter  */
  /* nz_params[35] = wNT0;   // Near-Inner width trans. func. parameter  */
  /* nz_params[36] = lambda; // Near-Far trans. func. parameter  */
  /* nz_params[37] = tt;  // Current global time */


  /* bh1_traj[ncurr][TT] = tt; */
  /* bh1_traj[ncurr][XX] = nz_params[ 0]; */
  /* bh1_traj[ncurr][YY] = nz_params[ 1]; */
  /* bh1_traj[ncurr][ZZ] = nz_params[24]; */

  /* bh2_traj[ncurr][TT] = tt; */
  /* bh2_traj[ncurr][XX] = nz_params[ 2]; */
  /* bh2_traj[ncurr][YY] = nz_params[ 3]; */
  /* bh2_traj[ncurr][ZZ] = nz_params[25]; */


return;
}
#undef VERBOSE

/************************************************************

  general_newton_raphson():

    -- performs Newton-Rapshon method on an arbitrary system.

    -- inspired in part by Num. Rec.'s routine newt();

    -- Adapted from the the Harm3d implementation by Bruno Mundim.

    Arguments:

       -- x[]   = set of independent variables to solve for;
       -- n     = number of independent variables and residuals;
       -- funcd = name of function that calculates residuals, etc.;

*****************************************************************/


static int general_newton_raphson( FTYPE params[], FTYPE x[], int n,
               void (*funcd) (FTYPE [], FTYPE [], FTYPE [], FTYPE [],
                    FTYPE [][NEWT_DIM], FTYPE *,
                    FTYPE *, int) )
{
  FTYPE f, df, dx[NEWT_DIM], resid[NEWT_DIM], jac[NEWT_DIM][NEWT_DIM];
 // FTYPE x_orig[NEWT_DIM],x_old[NEWT_DIM];
  FTYPE errx; 
  int    n_iter, id, i_extra, doing_extra;

  int   keep_iterating;


  // Initialize various parameters and variables:
  errx = 1. ;
  df = f = 1.;
  i_extra = doing_extra = 0;
  //for( id = 0; id < n ; id++)  x_old[id] = x_orig[id] = x[id] ;


  n_iter = 0;


  /* Start the Newton-Raphson iterations : */
  keep_iterating = 1;
  while( keep_iterating ) {

    (*funcd) (params, x, dx, resid, jac, &f, &df, n);  /* returns with new dx, f, df */

    /* Save old values before calculating the new: */
    errx = 0.;
   // for( id = 0; id < n ; id++) {
   //   x_old[id] = x[id] ;
   // }

    /* don't use line search : */
    for( id = 0; id < n ; id++) {
      x[id] += dx[id]  ;
    }

    /****************************************/
    /* Calculate the convergence criterion */
    /****************************************/

    errx  = (x[0]==0.) ?  fabs(dx[0]) : fabs(dx[0]/x[0]);


    /****************************************/
    /* Make sure that the new x[] is physical : */
    /****************************************/
    x[0] = fabs(x[0]);


    /*****************************************************************************/
    /* If we've reached the tolerance level, then just do a few extra iterations */
    /*  before stopping                                                          */
    /*****************************************************************************/

    if( (fabs(errx) <= NEWT_TOL) && (doing_extra == 0) && (EXTRA_NEWT_ITER > 0) ) {
      doing_extra = 1;
    }

    if( doing_extra == 1 ) i_extra++ ;

    if( ((fabs(errx) <= NEWT_TOL)&&(doing_extra == 0)) ||
   (i_extra > EXTRA_NEWT_ITER) || (n_iter >= (MAX_NEWT_ITER-1)) ) {
      keep_iterating = 0;
    }

    n_iter++;

  }   // END of while(keep_iterating)


  /*  Check for bad untrapped divergences : */
  if( (finite(f)==0) || (finite(df)==0)  ) {
    return(2);
  }


  if( fabs(errx) > MIN_NEWT_TOL){
    return(1);
  }
  if( (fabs(errx) <= MIN_NEWT_TOL) && (fabs(errx) > NEWT_TOL) ){
    return(0);
  }
  if( fabs(errx) <= NEWT_TOL ){
    return(0);
  }

  return(0);

}



/**********************************************************************/
/*********************************************************************************
   func_1d_orig():

        -- calculates the residuals, and Newton step for general_newton_raphson();
        -- for this method, x=R here;

     Arguments:
     params   = function parameters (on input); 
          x   = current value of independent var's (on input & output);
         dx   = Newton-Raphson step (on output);
        resid = residuals based on x (on output);
         jac  = Jacobian matrix based on x (on output);
         f    =  resid.resid/2  (on output)
        df    = -2*f;  (on output)
         n    = dimension of x[];
 *********************************************************************************/

static void func_1d_orig(FTYPE params[], FTYPE x[], FTYPE dx[], FTYPE resid[],
          FTYPE jac[][NEWT_DIM], FTYPE *f, FTYPE *df, int n)
{
  FTYPE resid_jac[2];
  FTYPE R;

  R = x[0];

  time_r12_resid_jac_v1 (params, R, resid_jac);
 
  resid[0] = resid_jac[0];
  jac[0][0] = resid_jac[1];

  dx[0] = -resid[0]/jac[0][0];

  *f = 0.5*resid[0]*resid[0];
  *df = -2. * (*f);

}

/********
time_tc_etal_funcs_v1 is based on time_tc_etal_funcs_v1 at
./init_data/confcurvBBHID/maple/test_c-files/Profiling/Ricci_dr_v4a/time_tc_etal_funcs_v1.c
********/

static void time_tc_etal_funcs_v1 (double time_tc_etal_args[4], double cgret[3])
{
  double result[3];
  double t1;
  double t102;
  double t103;
  double t110;
  double t111;
  double t112;
  double t113;
  double t114;
  double t115;
  double t116;
  double t118;
  double t119;
  double t12;
  double t123;
  double t124;
  double t128;
  double t129;
  double t134;
  double t138;
  double t142;
  double t144;
  double t145;
  double t149;
  double t150;
  double t151;
  double t152;
  double t158;
  double t159;
  double t16;
  double t164;
  double t168;
  double t169;
  double t17;
  double t170;
  double t171;
  double t172;
  double t173;
  double t175;
  double t179;
  double t182;
  double t189;
  double t19;
  double t193;
  double t196;
  double t2;
  double t20;
  double t21;
  double t225;
  double t230;
  double t25;
  double t26;
  double t27;
  double t28;
  double t37;
  double t38;
  double t4;
  double t40;
  double t43;
  double t46;
  double t47;
  double t5;
  double t52;
  double t56;
  double t57;
  double t58;
  double t59;
  double t6;
  double t62;
  double t65;
  double t7;
  double t70;
  double t77;
  double t87;
  double t9;
  result[0] = 0.0;
  result[1] = 0.0;
  result[2] = 0.0;
  t1 = 0.1e1 / time_tc_etal_args[1];
  t2 = 0.1e1 / time_tc_etal_args[2];
  t4 = time_tc_etal_args[1] + time_tc_etal_args[2];
  t5 = 0.1e1 / t4;
  t6 = t5 * time_tc_etal_args[3];
  t7 = log(t6);
  t9 = time_tc_etal_args[1] * time_tc_etal_args[2];
  t12 = pow(time_tc_etal_args[3], 0.2e1);
  t16 = t4 * t4;
  t17 = 0.1e1 / t16;
  t19 = pow(time_tc_etal_args[1], 0.2e1);
  t20 = pow(time_tc_etal_args[2], 0.2e1);
  t21 = t19 * t20;
  t25 = t4 / time_tc_etal_args[3];
  t26 = sqrt(t25);
  t27 = t26 * t25;
  t28 = time_tc_etal_args[3] * t27;
  t37 = t26 * t16 / t12;
  t38 = t12 * t37;
  t40 = t5 * time_tc_etal_args[1] * time_tc_etal_args[2];
  t43 = t12 * t27;
  t46 = t16 * t4;
  t47 = 0.1e1 / t46;
  t52 = t12 * time_tc_etal_args[3];
  t56 = t26 * t46;
  t57 = t16 * t16;
  t58 = 0.1e1 / t57;
  t59 = t21 * t58;
  t62 = t9 * t17;
  t65 = t37 * t52;
  t70 = 0.688484966400e12 * t7 * t4 * t9 - 0.54765849600e11 * t12 * t5 * t9 - 0.120289276800e12 * time_tc_etal_args[3] * t17 * t21 - 0.11027669422410641783707098553578337e15 * t28 * t9 + 0.33734479619021060931934068731488966e13 * t28 * t17 * t19 * t20 - 0.87455958104695531605195402905269691e14 * t38 * t40 + 0.23595701591446425132718911462462228e13 * t43 * t40 - 0.16129874134777829680569568382542538e14 * t38 * t47 * t19 * t20 + 0.99946210874615495874668166971785276e13 * t56 * t59 + 0.19731242216880382228169959185525602e15 * t56 * t62 - 0.30723569780529199391561082633414359e13 * t65 * t62 - 0.23471078400e11 * t52 - 0.34210528058025727901875918116446266e13 * t46;
  t77 = log(t25);
  t87 = t19 * time_tc_etal_args[1] * t20 * time_tc_etal_args[2];
  t102 = -0.163086739200e12 * t12 * t4 + 0.92380653735087881037192248631571409e14 * t56 - 0.75091330597878175617679244154410733e13 * t65 + 0.47191403182892850265437822924924455e12 * t27 * t52 - 0.765380689920e12 * t46 * t77 - 0.489744439800e12 * time_tc_etal_args[3] * t16 + 0.98029008000e11 * time_tc_etal_args[3] * time_tc_etal_args[1] * time_tc_etal_args[2] - 0.330062040000e12 * t87 * t47 + 0.774167209200e12 * t5 * t19 * t20 - 0.14795116031841479396734141640465917e14 * t4 * time_tc_etal_args[1] * time_tc_etal_args[2] - 0.11739724989007561205942531831640464e15 * t38 * t4 + 0.38245702591124664715388268953273159e14 * t28 * t16 + 0.10950465480633569124093161490218085e14 * t43 * t4;
  t103 = t70 + t102;
  result[0] = -0.83214114269244654732183076854278668e-12 * t1 * t2 * t6 * t103;
  t110 = t9 * t47 * (result[0] - 0.1e1 * time_tc_etal_args[0]);
  t111 = pow(t110, 0.1e1 / 0.8e1);
  t112 = t111 * t111;
  t113 = t112 * t112;
  t114 = t113 * t111;
  t115 = t114 * t1;
  t116 = t2 * t16;
  t118 = 0.46068948412698412698412698412698413e0 + 0.57291666666666666666666666666666667e0 * t62;
  t119 = pow(t110, 0.1e1 / 0.4e1);
  t123 = t112 * t111;
  t124 = 0.1e1 / t123;
  t128 = 0.64187220705339427437641723356009070e0 + 0.11039612785218253968253968253968254e1 * t62 + 0.90576171875000000000000000000000000e0 * t59;
  t129 = sqrt(t110);
  t134 = -0.22463843936011904761904761904761905e0 + 0.31738281250000000000000000000000000e-1 * t62;
  t138 = log(0.20000000000000000000000000000000000e0 * t110);
  t142 = log(0.78125000000000000000000000000000000e-3 * t110);
  t144 = 0.19530772951368314627863318190889483e2 * t62;
  t145 = 0.84231240408761160714285714285714286e-1 * t59;
  t149 = 0.66665366843894675925925925925925926e0 * t87 / t57 / t16;
  t150 = 0.2268860596302176584580453899548089e0 + 0.23883928571428571428571428571428571e0 * t142 - t144 + t145 - t149;
  t151 = t119 * t119;
  t152 = t151 * t119;
  t158 = 0.10871263764973220190854119425547997e1 + 0.94715905567956349206349206349206349e0 * t62 - 0.27469501798115079365079365079365079e0 * t59;
  t159 = t113 * t123;
  t164 = t116 * (0.1e1 + 0.14953487812212205419118989941409134e1 * t118 / t119 - 0.43084880002527500828713413684862660e1 * t124 + 0.22360679774997896964091736687312762e1 * t128 / t129 + 0.85902563734456047714081822386267747e1 * t134 / t114 * t138 + 0.33437015248821101200161653662932397e1 * t150 / t152 + 0.12845429398409707034828789831917116e2 * t158 / t159);
  t168 = t58 * time_tc_etal_args[3] * t103;
  t169 = 0.83214114269244654732183076854278668e-12 * t168;
  t170 = pow(-t169, 0.1e1 / 0.8e1);
  t171 = t170 * t170;
  t172 = t171 * t171;
  t173 = t172 * t170;
  t175 = pow(-t169, 0.1e1 / 0.4e1);
  t179 = t171 * t170;
  t182 = sqrt(-t169);
  t189 = log(-0.16642822853848930946436615370855734e-12 * t168);
  t193 = log(-0.65011026772847386509518028792405210e-15 * t168);
  t196 = t175 * t175;
  result[1] = -0.36571581999591486745176887117406210e0 * t115 * t164 + 0.36571581999591486745176887117406210e0 * t173 * t1 * t116 * (0.1e1 + 0.14953487812212205419118989941409134e1 * t118 / t175 - 0.43084880002527500828713413684862660e1 / t179 + 0.22360679774997896964091736687312762e1 * t128 / t182 + 0.85902563734456047714081822386267747e1 * t134 / t173 * t189 + 0.33437015248821101200161653662932397e1 * (0.2268860596302176584580453899548089e0 + 0.23883928571428571428571428571428571e0 * t193 - t144 + t145 - t149) / t196 / t175 + 0.12845429398409707034828789831917116e2 * t158 / t172 / t179);
  t225 = t134 / t114 / t110;
  t230 = 0.1e1 / t152 / t110;
  result[2] = -0.20000000000000000000000000000000000e0 * (-0.11428619374872339607867777224189441e1 * t124 * t1 * t164 - 0.36571581999591486745176887117406210e0 * t115 * t116 * (-0.18691859765265256773898737426761418e1 * t118 / t119 / t110 + 0.80784150004739064053837650659117488e1 / t123 / t110 - 0.55901699437494742410229341718281905e1 * t128 / t129 / t110 - 0.26844551167017514910650569495708671e2 * t225 * t138 + 0.42951281867228023857040911193133874e2 * t225 + 0.39930364192230556120728760512653645e1 * t230 - 0.12538880718307912950060620123599649e2 * t150 * t230 - 0.56198753618042468277375955514637382e2 * t158 / t159 / t110)) * time_tc_etal_args[1] * time_tc_etal_args[2] * t47;
  cgret[0] = result[0];
  cgret[1] = result[1];
  cgret[2] = result[2];
}

/********
time_r12_resid_jac_v1 is based on time_r12_resid_jac_v1 at
./init_data/confcurvBBHID/maple/test_c-files/Profiling/Ricci_dr_v4a/time_r12_resid_jac_v1.c
********/

static void time_r12_resid_jac_v1 (double time_r12_resid_jac_args[4], double R, double cgret[2])
{
  double result[2];
  double t10;
  double t102;
  double t107;
  double t111;
  double t114;
  double t115;
  double t116;
  double t117;
  double t118;
  double t12;
  double t121;
  double t122;
  double t125;
  double t132;
  double t135;
  double t136;
  double t139;
  double t146;
  double t15;
  double t155;
  double t16;
  double t162;
  double t17;
  double t173;
  double t174;
  double t188;
  double t19;
  double t193;
  double t199;
  double t21;
  double t214;
  double t22;
  double t238;
  double t24;
  double t25;
  double t26;
  double t3;
  double t30;
  double t31;
  double t32;
  double t33;
  double t36;
  double t37;
  double t4;
  double t40;
  double t43;
  double t45;
  double t49;
  double t5;
  double t55;
  double t58;
  double t6;
  double t62;
  double t63;
  double t64;
  double t66;
  double t69;
  double t70;
  double t73;
  double t74;
  double t77;
  double t90;
  double t91;
  result[0] = 0.0;
  result[1] = 0.0;
  t3 = 0.1e1 / time_r12_resid_jac_args[1] / time_r12_resid_jac_args[2];
  t4 = time_r12_resid_jac_args[1] + time_r12_resid_jac_args[2];
  t5 = 0.1e1 / t4;
  t6 = t5 * time_r12_resid_jac_args[3];
  t10 = pow(time_r12_resid_jac_args[1], 0.2e1);
  t12 = pow(time_r12_resid_jac_args[2], 0.2e1);
  t15 = t4 * t4;
  t16 = t15 * t4;
  t17 = 0.1e1 / t16;
  t19 = 0.330062040000e12 * t10 * time_r12_resid_jac_args[1] * t12 * time_r12_resid_jac_args[2] * t17;
  t21 = t5 * t10 * t12;
  t22 = 0.774167209200e12 * t21;
  t24 = t4 * time_r12_resid_jac_args[1] * time_r12_resid_jac_args[2];
  t25 = 0.14795116031841479396734141640465917e14 * t24;
  t26 = pow(time_r12_resid_jac_args[3], 0.2e1);
  t30 = t4 / time_r12_resid_jac_args[3];
  t31 = sqrt(t30);
  t32 = t31 * t15 / t26;
  t33 = t26 * t32;
  t36 = t31 * t30;
  t37 = time_r12_resid_jac_args[3] * t36;
  t40 = t26 * t36;
  t43 = 0.1e1 / t15;
  t45 = t43 * t10 * t12;
  t49 = t5 * time_r12_resid_jac_args[1] * time_r12_resid_jac_args[2];
  t55 = t17 * t10 * t12;
  t58 = t26 * time_r12_resid_jac_args[3];
  t62 = t31 * t16;
  t63 = t10 * t12;
  t64 = t15 * t15;
  t66 = t63 / t64;
  t69 = time_r12_resid_jac_args[1] * time_r12_resid_jac_args[2];
  t70 = t69 * t43;
  t73 = 0.98029008000e11 * time_r12_resid_jac_args[3] * time_r12_resid_jac_args[1] * time_r12_resid_jac_args[2] - t19 + t22 - t25 - 0.11739724989007561205942531831640464e15 * t33 * t4 + 0.38245702591124664715388268953273159e14 * t37 * t15 + 0.10950465480633569124093161490218085e14 * t40 * t4 + 0.33734479619021060931934068731488966e13 * t37 * t45 - 0.87455958104695531605195402905269691e14 * t33 * t49 + 0.23595701591446425132718911462462228e13 * t40 * t49 - 0.16129874134777829680569568382542538e14 * t33 * t55 + 0.99946210874615495874668166971785276e13 * t62 * t66 + 0.19731242216880382228169959185525602e15 * t62 * t70;
  t74 = t32 * t58;
  t77 = log(t6);
  t90 = 0.34210528058025727901875918116446266e13 * t16;
  t91 = log(t30);
  t102 = -0.30723569780529199391561082633414359e13 * t74 * t70 + 0.688484966400e12 * t77 * t4 * t69 - 0.54765849600e11 * t26 * t5 * t69 - 0.120289276800e12 * time_r12_resid_jac_args[3] * t43 * t63 - 0.11027669422410641783707098553578337e15 * t37 * t69 - 0.23471078400e11 * t58 - t90 - 0.765380689920e12 * t16 * t91 - 0.489744439800e12 * time_r12_resid_jac_args[3] * t15 - 0.163086739200e12 * t26 * t4 + 0.92380653735087881037192248631571409e14 * t62 - 0.75091330597878175617679244154410733e13 * t74 + 0.47191403182892850265437822924924455e12 * t36 * t58;
  t107 = t5 * R;
  t111 = R * R;
  t114 = 0.1e1 / R;
  t115 = t4 * t114;
  t116 = sqrt(t115);
  t117 = t116 * t15 / t111;
  t118 = t111 * t117;
  t121 = t116 * t115;
  t122 = R * t121;
  t125 = t111 * t121;
  t132 = t111 * R;
  t135 = t116 * t16 / t132;
  t136 = t135 * t132;
  t139 = t117 * t132;
  t146 = t19 - t22 + t25 - 0.98029008000e11 * R * time_r12_resid_jac_args[1] * time_r12_resid_jac_args[2] + 0.11739724989007561205942531831640464e15 * t118 * t4 - 0.38245702591124664715388268953273159e14 * t122 * t15 - 0.10950465480633569124093161490218085e14 * t125 * t4 - 0.23595701591446425132718911462462228e13 * t125 * t49 + 0.87455958104695531605195402905269691e14 * t118 * t49 - 0.99946210874615495874668166971785276e13 * t136 * t66 + 0.30723569780529199391561082633414359e13 * t139 * t70 - 0.19731242216880382228169959185525602e15 * t136 * t70 + 0.16129874134777829680569568382542538e14 * t118 * t55;
  t155 = log(t107);
  t162 = log(t115);
  t173 = -0.33734479619021060931934068731488966e13 * t122 * t45 + 0.54765849600e11 * t111 * t5 * t69 + 0.120289276800e12 * R * t43 * t63 - 0.688484966400e12 * t155 * t4 * t69 + 0.11027669422410641783707098553578337e15 * t122 * t69 + 0.23471078400e11 * t132 + t90 + 0.765380689920e12 * t16 * t162 + 0.489744439800e12 * R * t15 + 0.163086739200e12 * t111 * t4 - 0.47191403182892850265437822924924455e12 * t121 * t132 - 0.92380653735087881037192248631571409e14 * t136 + 0.75091330597878175617679244154410733e13 * t139;
  t174 = t146 + t173;
  result[0] = time_r12_resid_jac_args[0] + 0.83214114269244654732183076854278668e-12 * t3 * t6 * (t73 + t102) + 0.83214114269244654732183076854278668e-12 * t3 * t107 * t174;
  t188 = R * t117;
  t193 = t116 * t114;
  t199 = t135 * t111;
  t214 = 0.120289276800e12 * t45 - 0.10836320103763241117591752172739086e15 * t121 * time_r12_resid_jac_args[1] * time_r12_resid_jac_args[2] + 0.35393552387169637699078367193693342e13 * t116 * time_r12_resid_jac_args[1] * time_r12_resid_jac_args[2] + 0.55812678785295880774902350684330920e15 * t188 * t4 - 0.40673763610736682152606134019038854e14 * t122 * t4 + 0.57368553886686997073082403429909739e14 * t193 * t16 + 0.70787104774339275398156734387386683e12 * t116 * R * t4 - 0.29983863262384648762400450091535583e14 * t199 * t66 + 0.92170709341587598174683247900243077e13 * t118 * t70 - 0.59193726650641146684509877556576807e15 * t199 * t70 + 0.86550539380020444119633937730393546e15 * t188 * t49 + 0.67240922075671082917272995205209924e14 * t188 * t55 - 0.12400032763421584874434052950846035e14 * t122 * t49 - 0.16541504133615962675560647830367505e15 * t193 * t24;
  t238 = 0.50601719428531591397901103097233449e13 * t193 * t21 + 0.109531699200e12 * t107 * t69 - 0.688484966400e12 * t115 * t69 - 0.43698133298846680294617327829505243e14 * t121 * t43 * t63 + 0.70413235200e11 * t111 + 0.489744439800e12 * t15 - 0.765380689920e12 * t16 * t114 + 0.326173478400e12 * R * t4 - 0.33173882731631369486395156474428475e15 * t121 * t15 + 0.16425698220950353686139742235327128e14 * t116 * t15 - 0.14157420954867855079631346877477337e13 * t125 - 0.27714196120526364311157674589471423e15 * t199 + 0.22527399179363452685303773246323220e14 * t118 - 0.98029008000e11 * t69;
  result[1] = 0.83214114269244654732183076854278668e-12 * t3 * t5 * t174 + 0.83214114269244654732183076854278668e-12 * t3 * t107 * (t214 + t238);
  cgret[0] = result[0];
  cgret[1] = result[1];
}

/********
time_r12dot_v1 is based on time_r12dot_v1 at
./init_data/confcurvBBHID/maple/test_c-files/Profiling/Ricci_dr_v4a/time_r12dot_v1.c
********/

static double time_r12dot_v1 (double m1, double m2, double R)
{
  double t11;
  double t118;
  double t12;
  double t13;
  double t137;
  double t14;
  double t157;
  double t171;
  double t20;
  double t24;
  double t25;
  double t26;
  double t27;
  double t3;
  double t31;
  double t33;
  double t34;
  double t36;
  double t38;
  double t4;
  double t44;
  double t48;
  double t5;
  double t50;
  double t55;
  double t57;
  double t6;
  double t61;
  double t7;
  double t73;
  double t74;
  double t75;
  double t79;
  double t8;
  double t83;
  double t93;
  double t97;
  t3 = 0.1e1 / m1 / m2;
  t4 = m1 + m2;
  t5 = 0.1e1 / t4;
  t6 = R * R;
  t7 = t6 * R;
  t8 = t5 * t7;
  t11 = m1 * m2;
  t12 = t4 * t4;
  t13 = 0.1e1 / t12;
  t14 = t11 * t13;
  t20 = 0.87113095238095238095238095238095238e1 + 0.12500000000000000000000000000000000e1 * t14;
  t24 = 0.1e1 / R;
  t25 = t4 * t24;
  t26 = sqrt(t25);
  t27 = t25 * t26;
  t31 = R * t4;
  t33 = m1 * m1;
  t34 = m2 * m2;
  t36 = t12 * t12;
  t38 = t33 * t34 / t36;
  t44 = 0.32339395943562610229276895943562610e2 + 0.42222222222222222222222222222222222e2 * t14;
  t48 = t3 * t4;
  t50 = 0.1751e4 + 0.588e3 * t14;
  t55 = t3 * t5;
  t57 = 0.38188988095238095238095238095238095e2 + 0.15625000000000000000000000000000000e2 * t14;
  t61 = t26 * t12 / t6;
  t73 = t12 * t4;
  t74 = t6 * t6;
  t75 = t74 * R;
  t79 = log(t5 * R);
  t83 = m2 * t13;
  t93 = -0.36718750000000000000000000000000000e1 + (0.1581407613084960332998891181041593e1 + 0.73333333333333333333333333333333333e1 * t79) * m1 * t83 + 0.84375000000000000000000000000000000e0 * t38 + 0.78125000000000000000000000000000000e-1 * t33 * m1 * t34 * m2 / t36 / t12;
  t97 = 0.1e1 / t74;
  t118 = log(0.16e2 * t25);
  t137 = 0.10600529e8 - 0.2121840e7 * t14 + 0.2603664e7 * t38;
  t157 = R * t27;
  t171 = -0.78125000000000000000000000000000000e-1 * t3 * t8 - 0.39062500000000000000000000000000000e-1 * t3 * t6 * (-0.7e1 + t14) - 0.78125000000000000000000000000000000e-1 * t3 * t6 * t20 + 0.98174770424681038701957605727484466e0 * t3 * t8 * t27 - 0.29296875000000000000000000000000000e-1 * t3 * t31 * (-0.7e1 + 0.49e2 * t14 + t38) + 0.78125000000000000000000000000000000e-1 * t3 * t31 * t44 - 0.23251488095238095238095238095238095e-3 * t48 * R * t50 * t20 - 0.24543692606170259675489401431871116e0 * t55 * t7 * t57 * t61 + 0.29218681674012213899392144561751329e-2 * t3 * t6 * t50 * t27 + 0.98174770424681038701957605727484466e0 * t3 * t6 * t27 * t20 - 0.15625000000000000000000000000000000e0 * (0.50000000000000000000000000000000000e0 * t11 * t73 / t75 * t93 - 0.50000000000000000000000000000000000e0 * t11 * t24 * (0.73333333333333333333333333333333333e1 * t97 * m1 * m2 * t4 - 0.3e1 * t93 * t73 * t97)) / t33 / t34 * t5 * t75 + 0.78125000000000000000000000000000000e-1 * t3 * t12 * (0.59896644558646894557107862562560752e2 - 0.81523809523809523809523809523809524e1 * t118 + (-0.43304078261308496033299889118104159e3 + 0.36666666666666666666666666666666667e2 * t79) * m1 * t83 - 0.42555555555555555555555555555555556e2 * t38) + 0.23251488095238095238095238095238095e-3 * t3 * t12 * t50 * t44 - 0.12337005501361698273543113749845189e2 * t3 * t12 - 0.76889841584782060972537163013353490e-7 * t3 * t12 * t137 * t20 + 0.24543692606170259675489401431871116e0 * t55 * (0.15660590277777777777777777777777778e3 + 0.33448875661375661375661375661375661e3 * t14 + 0.16943121693121693121693121693121693e2 * t38) * t26 * t73 - 0.73046704185030534748480361404378322e-3 * t3 * t6 * t50 * t57 * t61 - 0.98174770424681038701957605727484466e0 * t48 * t157 * t44 + 0.96622624583373723212275610323251750e-6 * t48 * R * t137 * t27 - 0.36523352092515267374240180702189163e-3 * t48 * t157 * (-0.11761e5 + 0.2436e4 * t14) * t20;
  return(0.1e1 / t171);
}

/********
time_functions_opt_v4 is based on time_functions_opt_v4 at
./init_data/confcurvBBHID/maple/test_c-files/Ricci_dr_drop_v2/time_functions_opt_v4.c
********/

static void time_functions_opt_v4 (double time_functions_args[6], double cgret[19])
{
  double result[19];
  double t1;
  double t10;
  double t105;
  double t106;
  double t108;
  double t11;
  double t111;
  double t113;
  double t117;
  double t118;
  double t12;
  double t122;
  double t125;
  double t130;
  double t134;
  double t135;
  double t136;
  double t137;
  double t138;
  double t139;
  double t14;
  double t142;
  double t143;
  double t146;
  double t147;
  double t148;
  double t15;
  double t150;
  double t153;
  double t154;
  double t155;
  double t156;
  double t16;
  double t163;
  double t166;
  double t169;
  double t17;
  double t170;
  double t171;
  double t172;
  double t174;
  double t177;
  double t178;
  double t18;
  double t181;
  double t182;
  double t185;
  double t186;
  double t187;
  double t188;
  double t192;
  double t194;
  double t195;
  double t196;
  double t198;
  double t2;
  double t20;
  double t201;
  double t205;
  double t207;
  double t21;
  double t211;
  double t216;
  double t220;
  double t223;
  double t228;
  double t229;
  double t23;
  double t231;
  double t235;
  double t24;
  double t242;
  double t244;
  double t248;
  double t25;
  double t253;
  double t257;
  double t259;
  double t26;
  double t28;
  double t280;
  double t293;
  double t298;
  double t299;
  double t3;
  double t300;
  double t306;
  double t307;
  double t31;
  double t311;
  double t32;
  double t34;
  double t35;
  double t36;
  double t39;
  double t46;
  double t48;
  double t49;
  double t5;
  double t51;
  double t54;
  double t57;
  double t58;
  double t61;
  double t76;
  double t77;
  double t8;
  double t82;
  double t83;
  double t85;
  double t88;
  double t89;
  double t90;
  double t92;
  double t93;
  double t97;
  double t98;
  result[0] = 0.0;
  result[1] = 0.0;
  result[2] = 0.0;
  result[3] = 0.0;
  result[4] = 0.0;
  result[5] = 0.0;
  result[6] = 0.0;
  result[7] = 0.0;
  result[8] = 0.0;
  result[9] = 0.0;
  result[10] = 0.0;
  result[11] = 0.0;
  result[12] = 0.0;
  result[13] = 0.0;
  result[14] = 0.0;
  result[15] = 0.0;
  result[16] = 0.0;
  result[17] = 0.0;
  result[18] = 0.0;
//  t1 = pow(time_functions_args[4], 0.2e1);
  t1 = time_functions_args[4]*time_functions_args[4];
  t2 = 0.1e1 / t1;
  t3 = cos(time_functions_args[5]);
  t5 = t1 * time_functions_args[4];
  t8 = t3 * time_functions_args[1];
  t10 = time_functions_args[1] - 0.1e1 * time_functions_args[2];
  t11 = time_functions_args[2] * t10;
  t12 = t11 * time_functions_args[4];
  t14 = 0.3780e4 * t8 * t12;
  t15 = time_functions_args[2] + time_functions_args[1];
  t16 = t3 * t15;
  t17 = time_functions_args[1] * time_functions_args[2];
  t18 = t17 * t10;
  t20 = 0.7211e4 * t16 * t18;
  t21 = 0.1e1 / t15;
//  t23 = pow(time_functions_args[1], 0.2e1);
  t23 = time_functions_args[1]*time_functions_args[1];
//  t24 = pow(time_functions_args[2], 0.2e1);
  t24 = time_functions_args[2]*time_functions_args[2];
  t25 = t23 * t24;
  t26 = t25 * t10;
  t28 = 0.1260e4 * t3 * t21 * t26;
  t31 = log(time_functions_args[4] * t21);
  t32 = t11 * t31;
  t34 = 0.9240e4 * t16 * time_functions_args[1] * t32;
  t35 = sin(time_functions_args[5]);
  t36 = time_functions_args[3] * t35;
  t39 = 0.1008e4 * t18 * t36 * t1;
  result[0] = 0.79365079365079365079365079365079365e-3 * t2 * (0.1260e4 * t3 * time_functions_args[2] * t5 + t14 - t20 - t28 + t34 + t39) * t21;
  t46 = t35 * time_functions_args[1];
  t48 = 0.3780e4 * t46 * t12;
  t49 = t35 * t15;
  t51 = 0.7211e4 * t49 * t18;
  t54 = 0.1260e4 * t35 * t21 * t26;
  t57 = 0.9240e4 * t49 * time_functions_args[1] * t32;
  t58 = time_functions_args[3] * t3;
  t61 = 0.1008e4 * t18 * t58 * t1;
  result[1] = -0.79365079365079365079365079365079365e-3 * t2 * (-0.1260e4 * t35 * time_functions_args[2] * t5 - t48 + t51 + t54 - t57 + t61) * t21;
  result[2] = 0.79365079365079365079365079365079365e-3 * t2 * (-0.1260e4 * t8 * t5 + t14 - t20 - t28 + t34 + t39) * t21;
  t76 = 0.79365079365079365079365079365079365e-3 * t2 * (0.1260e4 * t46 * t5 - t48 + t51 + t54 - t57 + t61) * t21;
  result[3] = -t76;
  t77 = time_functions_args[2] * t5;
  t82 = 0.3780e4 * t36 * time_functions_args[1] * t12;
  t83 = t36 * t15;
  t85 = 0.7211e4 * t83 * t18;
  t88 = 0.1260e4 * t36 * t21 * t26;
  t89 = t10 * t31;
  t90 = t17 * t89;
  t92 = 0.9240e4 * t83 * t90;
//  t93 = pow(time_functions_args[3], 0.2e1);
  t93 = time_functions_args[3]*time_functions_args[3];
  t97 = 0.1008e4 * t18 * t93 * t3 * t1;
  t98 = -0.1260e4 * t36 * t77 - t82 + t85 + t88 - t92 + t97;
  result[4] = 0.79365079365079365079365079365079365e-3 * t2 * t98 * t21;
  t105 = 0.3780e4 * t58 * time_functions_args[1] * t12;
  t106 = t58 * t15;
  t108 = 0.7211e4 * t106 * t18;
  t111 = 0.1260e4 * t58 * t21 * t26;
  t113 = 0.9240e4 * t106 * t90;
  t117 = 0.1008e4 * t18 * t93 * t35 * t1;
  t118 = -0.1260e4 * t58 * t77 - t105 + t108 + t111 - t113 - t117;
  result[5] = -0.79365079365079365079365079365079365e-3 * t2 * t118 * t21;
  t122 = time_functions_args[1] * t5;
  t125 = 0.1260e4 * t36 * t122 - t82 + t85 + t88 - t92 + t97;
  result[6] = 0.79365079365079365079365079365079365e-3 * t2 * t125 * t21;
  t130 = 0.1260e4 * t58 * t122 - t105 + t108 + t111 - t113 - t117;
  result[7] = -0.79365079365079365079365079365079365e-3 * t2 * t130 * t21;
  t134 = time_functions_args[3] * t2;
  t135 = t15 * t23;
  t136 = t135 * t24;
  t137 = t10 * t10;
  t138 = t137 * time_functions_args[4];
  t139 = log(time_functions_args[4]);
  t142 = 0.69854400e8 * t136 * t138 * t139;
  t143 = log(t15);
  t146 = 0.69854400e8 * t136 * t138 * t143;
  t147 = t15 * t15;
  t148 = t147 * t23;
  t150 = t137 * t139;
  t153 = 0.170755200e9 * t148 * t24 * t150 * t143;
  t154 = t24 * t5;
  t155 = t154 * t15;
  t156 = time_functions_args[1] * t10;
  t163 = t24 * t137;
  t166 = 0.133259280e9 * t148 * t163 * t139;
  t169 = 0.133259280e9 * t148 * t163 * t143;
  t170 = t23 * time_functions_args[1];
  t171 = t24 * time_functions_args[2];
  t172 = t170 * t171;
  t174 = 0.23284800e8 * t172 * t150;
  t177 = 0.23284800e8 * t172 * t137 * t143;
  t178 = t139 * t139;
  t181 = 0.85377600e8 * t148 * t163 * t178;
  t182 = t143 * t143;
  t185 = 0.85377600e8 * t148 * t163 * t182;
  t186 = t142 - t146 - t153 + 0.23284800e8 * t155 * t156 * t139 - 0.23284800e8 * t155 * t156 * t143 - t166 + t169 - t174 + t177 + t181 + t185;
  t187 = t1 * t1;
  t188 = t187 * t1;
  t192 = 0.51998521e8 * t148 * t163;
  t194 = 0.18171720e8 * t172 * t137;
  t195 = 0.1e1 / t147;
  t196 = t23 * t23;
  t198 = t24 * t24;
  t201 = 0.1587600e7 * t195 * t196 * t198 * t137;
  t205 = 0.1016064e7 * t25 * t137 * t187 * t93;
  t207 = t24 * t187 * t156;
  t211 = t154 * t15 * time_functions_args[1] * t10;
  t216 = t171 * t5 * t21 * t23 * t10;
  t220 = 0.14288400e8 * t25 * t137 * t1;
  t223 = 0.54515160e8 * t135 * t163 * time_functions_args[4];
  t228 = 0.9525600e7 * t21 * t170 * t171 * t137 * time_functions_args[4];
  t229 = 0.1587600e7 * t24 * t188 + t192 + t194 + t201 + t205 + 0.9525600e7 * t207 - 0.18171720e8 * t211 - 0.3175200e7 * t216 + t220 - t223 - t228;
  t231 = sqrt(t186 + t229);
  result[8] = 0.79365079365079365079365079365079365e-3 * t134 * t21 * t231;
  t235 = t23 * t5 * t15;
  t242 = -0.23284800e8 * t235 * t11 * t139 + 0.23284800e8 * t235 * t11 * t143 - t166 + t169 - t174 + t177 + t181 + t185 + t220 - t223 - t228;
  t244 = t23 * t187 * t11;
  t248 = t77 * t23 * t10 * t15;
  t253 = t170 * t5 * t21 * t24 * t10;
  t257 = t205 - 0.9525600e7 * t244 + 0.18171720e8 * t248 + 0.3175200e7 * t253 + t142 - t146 - t153 + t192 + t194 + t201 + 0.1587600e7 * t23 * t188;
  t259 = sqrt(t242 + t257);
  result[9] = 0.79365079365079365079365079365079365e-3 * t134 * t21 * t259;
  result[10] = -0.1e1 * t36 * time_functions_args[4];
  result[11] = t58 * time_functions_args[4];
  result[12] = time_functions_args[3] * time_functions_args[4];
  t280 = t31 * t31;
  t293 = t194 + t201 + t220 - t223 - t228 + t205 + 0.4762800e7 * t207 - 0.9085860e7 * t211 - 0.1587600e7 * t216 + t192 - 0.4762800e7 * t244 + 0.9085860e7 * t248 + 0.1587600e7 * t253 + 0.69854400e8 * t136 * t138 * t31 + 0.11642400e8 * t155 * t156 * t31 - 0.11642400e8 * t135 * time_functions_args[2] * t89 * t5 + 0.85377600e8 * t148 * t163 * t280 - 0.133259280e9 * t148 * t163 * t31 - 0.23284800e8 * t172 * t137 * t31 - 0.1587600e7 * time_functions_args[2] * t188 * time_functions_args[1];
  result[13] = 0.62988158226253464348702443940539179e-6 * t93 * t293 / t187 * t195;
  t298 = result[0] - result[2];
  t299 = 0.1e1 / time_functions_args[4];
  result[14] = t298 * t299;
  t300 = result[1] + t76;
  result[15] = t300 * t299;
  result[16] = -0.1e1 * t298 * time_functions_args[3] * t35 + t300 * time_functions_args[3] * t3;
  t306 = 0.1e1 / t5;
  t307 = t298 * t306;
  t311 = t300 * t306;
  result[17] = 0.79365079365079365079365079365079365e-3 * t307 * t98 * t21 - 0.79365079365079365079365079365079365e-3 * t311 * t118 * t21;
  result[18] = 0.79365079365079365079365079365079365e-3 * t307 * t125 * t21 - 0.79365079365079365079365079365079365e-3 * t311 * t130 * t21;
  cgret[0] = result[0];
  cgret[1] = result[1];
  cgret[2] = result[2];
  cgret[3] = result[3];
  cgret[4] = result[4];
  cgret[5] = result[5];
  cgret[6] = result[6];
  cgret[7] = result[7];
  cgret[8] = result[8];
  cgret[9] = result[9];
  cgret[10] = result[10];
  cgret[11] = result[11];
  cgret[12] = result[12];
  cgret[13] = result[13];
  cgret[14] = result[14];
  cgret[15] = result[15];
  cgret[16] = result[16];
  cgret[17] = result[17];
  cgret[18] = result[18];
}


/********
IZ_time_funcs_v0 is based on IZ_time_funcs_v0 at
./init_data/confcurvBBHID/maple/test_c-files/Profiling/Ricci_dr_v4a/IZ_time_funcs_v0.c
********/
/*
static void IZ_time_funcs_v0 (
  double m1,
  double m2,
  double r12,
  double r12dot,
  double phi,
  double omega,
  double b,
  double cgret[200])
{
  double result[200];
  double t1;
  double t10;
  double t100;
  double t102;
  double t106;
  double t107;
  double t11;
  double t111;
  double t115;
  double t117;
  double t122;
  double t124;
  double t125;
  double t126;
  double t129;
  double t13;
  double t131;
  double t133;
  double t134;
  double t14;
  double t140;
  double t142;
  double t146;
  double t147;
  double t15;
  double t150;
  double t155;
  double t158;
  double t159;
  double t16;
  double t160;
  double t161;
  double t164;
  double t167;
  double t168;
  double t169;
  double t17;
  double t170;
  double t171;
  double t175;
  double t177;
  double t178;
  double t181;
  double t182;
  double t185;
  double t187;
  double t189;
  double t19;
  double t191;
  double t192;
  double t193;
  double t194;
  double t196;
  double t2;
  double t20;
  double t200;
  double t201;
  double t205;
  double t207;
  double t208;
  double t212;
  double t214;
  double t215;
  double t218;
  double t219;
  double t220;
  double t222;
  double t223;
  double t224;
  double t228;
  double t229;
  double t23;
  double t231;
  double t234;
  double t238;
  double t24;
  double t240;
  double t243;
  double t244;
  double t245;
  double t246;
  double t248;
  double t25;
  double t253;
  double t254;
  double t257;
  double t259;
  double t261;
  double t262;
  double t263;
  double t264;
  double t268;
  double t27;
  double t272;
  double t274;
  double t277;
  double t28;
  double t280;
  double t282;
  double t284;
  double t285;
  double t286;
  double t289;
  double t294;
  double t295;
  double t3;
  double t30;
  double t304;
  double t308;
  double t309;
  double t31;
  double t312;
  double t313;
  double t316;
  double t329;
  double t332;
  double t337;
  double t34;
  double t343;
  double t344;
  double t347;
  double t35;
  double t351;
  double t352;
  double t356;
  double t358;
  double t363;
  double t364;
  double t365;
  double t369;
  double t376;
  double t38;
  double t380;
  double t384;
  double t388;
  double t39;
  double t398;
  double t4;
  double t40;
  double t406;
  double t41;
  double t411;
  double t416;
  double t417;
  double t418;
  double t42;
  double t422;
  double t425;
  double t428;
  double t43;
  double t432;
  double t437;
  double t44;
  double t443;
  double t446;
  double t448;
  double t459;
  double t46;
  double t47;
  double t473;
  double t5;
  double t500;
  double t502;
  double t505;
  double t508;
  double t528;
  double t53;
  double t55;
  double t567;
  double t572;
  double t577;
  double t586;
  double t597;
  double t598;
  double t601;
  double t602;
  double t605;
  double t606;
  double t609;
  double t610;
  double t614;
  double t618;
  double t623;
  double t626;
  double t629;
  double t638;
  double t64;
  double t641;
  double t649;
  double t657;
  double t660;
  double t679;
  double t68;
  double t682;
  double t683;
  double t684;
  double t686;
  double t687;
  double t689;
  double t694;
  double t696;
  double t697;
  double t7;
  double t70;
  double t707;
  double t710;
  double t713;
  double t716;
  double t719;
  double t72;
  double t722;
  double t724;
  double t727;
  double t730;
  double t732;
  double t735;
  double t737;
  double t739;
  double t740;
  double t745;
  double t748;
  double t754;
  double t755;
  double t756;
  double t759;
  double t76;
  double t762;
  double t767;
  double t768;
  double t77;
  double t770;
  double t774;
  double t79;
  double t791;
  double t796;
  double t797;
  double t799;
  double t8;
  double t80;
  double t800;
  double t803;
  double t804;
  double t811;
  double t82;
  double t84;
  double t85;
  double t851;
  double t856;
  double t87;
  double t89;
  double t894;
  double t90;
  double t903;
  double t915;
  double t916;
  double t917;
  double t92;
  double t920;
  double t926;
  double t93;
  double t934;
  double t935;
  double t944;
  double t96;
  double t966;
  double t982;
  result[0] = 0.0;
  result[1] = 0.0;
  result[2] = 0.0;
  result[3] = 0.0;
  result[4] = 0.0;
  result[5] = 0.0;
  result[6] = 0.0;
  result[7] = 0.0;
  result[8] = 0.0;
  result[9] = 0.0;
  result[10] = 0.0;
  result[11] = 0.0;
  result[12] = 0.0;
  result[13] = 0.0;
  result[14] = 0.0;
  result[15] = 0.0;
  result[16] = 0.0;
  result[17] = 0.0;
  result[18] = 0.0;
  result[19] = 0.0;
  result[20] = 0.0;
  result[21] = 0.0;
  result[22] = 0.0;
  result[23] = 0.0;
  result[24] = 0.0;
  result[25] = 0.0;
  result[26] = 0.0;
  result[27] = 0.0;
  result[28] = 0.0;
  result[29] = 0.0;
  result[30] = 0.0;
  result[31] = 0.0;
  result[32] = 0.0;
  result[33] = 0.0;
  result[34] = 0.0;
  result[35] = 0.0;
  result[36] = 0.0;
  result[37] = 0.0;
  result[38] = 0.0;
  result[39] = 0.0;
  result[40] = 0.0;
  result[41] = 0.0;
  result[42] = 0.0;
  result[43] = 0.0;
  result[44] = 0.0;
  result[45] = 0.0;
  result[46] = 0.0;
  result[47] = 0.0;
  result[48] = 0.0;
  result[49] = 0.0;
  result[50] = 0.0;
  result[51] = 0.0;
  result[52] = 0.0;
  result[53] = 0.0;
  result[54] = 0.0;
  result[55] = 0.0;
  result[56] = 0.0;
  result[57] = 0.0;
  result[58] = 0.0;
  result[59] = 0.0;
  result[60] = 0.0;
  result[61] = 0.0;
  result[62] = 0.0;
  result[63] = 0.0;
  result[64] = 0.0;
  result[65] = 0.0;
  result[66] = 0.0;
  result[67] = 0.0;
  result[68] = 0.0;
  result[69] = 0.0;
  result[70] = 0.0;
  result[71] = 0.0;
  result[72] = 0.0;
  result[73] = 0.0;
  result[74] = 0.0;
  result[75] = 0.0;
  result[76] = 0.0;
  result[77] = 0.0;
  result[78] = 0.0;
  result[79] = 0.0;
  result[80] = 0.0;
  result[81] = 0.0;
  result[82] = 0.0;
  result[83] = 0.0;
  result[84] = 0.0;
  result[85] = 0.0;
  result[86] = 0.0;
  result[87] = 0.0;
  result[88] = 0.0;
  result[89] = 0.0;
  result[90] = 0.0;
  result[91] = 0.0;
  result[92] = 0.0;
  result[93] = 0.0;
  result[94] = 0.0;
  result[95] = 0.0;
  result[96] = 0.0;
  result[97] = 0.0;
  result[98] = 0.0;
  result[99] = 0.0;
  result[100] = 0.0;
  result[101] = 0.0;
  result[102] = 0.0;
  result[103] = 0.0;
  result[104] = 0.0;
  result[105] = 0.0;
  result[106] = 0.0;
  result[107] = 0.0;
  result[108] = 0.0;
  result[109] = 0.0;
  result[110] = 0.0;
  result[111] = 0.0;
  result[112] = 0.0;
  result[113] = 0.0;
  result[114] = 0.0;
  result[115] = 0.0;
  result[116] = 0.0;
  result[117] = 0.0;
  result[118] = 0.0;
  result[119] = 0.0;
  result[120] = 0.0;
  result[121] = 0.0;
  result[122] = 0.0;
  result[123] = 0.0;
  result[124] = 0.0;
  result[125] = 0.0;
  result[126] = 0.0;
  result[127] = 0.0;
  result[128] = 0.0;
  result[129] = 0.0;
  result[130] = 0.0;
  result[131] = 0.0;
  result[132] = 0.0;
  result[133] = 0.0;
  result[134] = 0.0;
  result[135] = 0.0;
  result[136] = 0.0;
  result[137] = 0.0;
  result[138] = 0.0;
  result[139] = 0.0;
  result[140] = 0.0;
  result[141] = 0.0;
  result[142] = 0.0;
  result[143] = 0.0;
  result[144] = 0.0;
  result[145] = 0.0;
  result[146] = 0.0;
  result[147] = 0.0;
  result[148] = 0.0;
  result[149] = 0.0;
  result[150] = 0.0;
  result[151] = 0.0;
  result[152] = 0.0;
  result[153] = 0.0;
  result[154] = 0.0;
  result[155] = 0.0;
  result[156] = 0.0;
  result[157] = 0.0;
  result[158] = 0.0;
  result[159] = 0.0;
  result[160] = 0.0;
  result[161] = 0.0;
  result[162] = 0.0;
  result[163] = 0.0;
  result[164] = 0.0;
  result[165] = 0.0;
  result[166] = 0.0;
  result[167] = 0.0;
  result[168] = 0.0;
  result[169] = 0.0;
  result[170] = 0.0;
  result[171] = 0.0;
  result[172] = 0.0;
  result[173] = 0.0;
  result[174] = 0.0;
  result[175] = 0.0;
  result[176] = 0.0;
  result[177] = 0.0;
  result[178] = 0.0;
  result[179] = 0.0;
  result[180] = 0.0;
  result[181] = 0.0;
  result[182] = 0.0;
  result[183] = 0.0;
  result[184] = 0.0;
  result[185] = 0.0;
  result[186] = 0.0;
  result[187] = 0.0;
  result[188] = 0.0;
  result[189] = 0.0;
  result[190] = 0.0;
  result[191] = 0.0;
  result[192] = 0.0;
  result[193] = 0.0;
  result[194] = 0.0;
  result[195] = 0.0;
  result[196] = 0.0;
  result[197] = 0.0;
  result[198] = 0.0;
  result[199] = 0.0;
  result[0] = cos(phi);
  result[1] = sin(phi);
  t1 = m2 * r12;
  t2 = m1 + m2;
  t3 = 0.1e1 / t2;
  t4 = t3 * result[0];
  result[2] = t1 * t4;
  t5 = t3 * result[1];
  result[3] = t1 * t5;
  t7 = r12 - 0.1e1 * b;
  t8 = r12 * r12;
  t10 = b * b;
  t11 = t8 + r12 * b + t10;
  t13 = 0.3e1 * m2;
  t14 = 0.2e1 * m1;
  t15 = t13 + t14;
  t16 = t2 * t2;
  t17 = 0.1e1 / t16;
  t19 = 0.1e1 / m1;
  t20 = t15 * t17 * t19;
  t23 = t16 * t2;
  t24 = 0.4e1 * t23;
  t25 = m2 * t16;
  t27 = m2 * m2;
  t28 = t27 * m2;
  t30 = t24 - 0.4e1 * t25 - 0.3e1 * t28;
  t31 = t30 * t7;
  t34 = 0.1e1 / t23;
  t35 = (r12 + b) * t19 * t34;
  result[4] = 0.13020833333333333333333333333333333e-1 * t7 * t11 * t20 + 0.48828125000000000000000000000000000e-2 * t31 * t35;
  t38 = sqrt(t2);
  t39 = 0.1e1 / t38;
  t40 = m2 * t39;
  t41 = pow(result[1], 0.2e1);
  t42 = t41 * t41;
  t43 = pow(result[0], 0.2e1);
  t44 = t43 * t43;
  t46 = sqrt(r12);
  t47 = 0.1e1 / t46;
  t53 = -0.5e1 * m1 - 0.4e1 * m2;
  t55 = 0.1e1 / t46 / r12;
  t64 = t27 * t2;
  t68 = t27 * (0.17e2 * t25 + 0.30e2 * t28 - 0.17e2 * t64 + 0.12e2 * t23);
  t70 = 0.1e1 / t38 / t16;
  t72 = 0.1e1 / t46 / t8;
  result[5] = 0.41666666666666666666666666666666667e-1 * t40 * t42 * t44 * t47 + 0.50000000000000000000000000000000000e0 * t40 * t53 * t55 - 0.1e1 * t47 * t39 * m2 + 0.83333333333333333333333333333333333e-1 * t68 * t70 * t72;
  t76 = m2 * t38;
  t77 = t8 * t8;
  t79 = 0.1e1 / t46 / t77;
  t80 = t76 * t79;
  result[6] = -0.50000000000000000000000000000000000e0 * t80;
  result[7] = 0.25000000000000000000000000000000000e1 * t80;
  t82 = t8 * r12;
  t84 = 0.1e1 / t46 / t82;
  t85 = t76 * t84;
  result[8] = -0.2e1 * t85;
  t87 = m2 * t2;
  t89 = 0.3e1 * t27;
  t90 = 0.8e1 * t16;
  t92 = m2 * (-0.19e2 * t87 + t89 + t90);
  t93 = t92 * t41;
  t96 = 0.1e1 / t38 / t2;
  t100 = 0.2e1 * t16;
  t102 = t100 + t89 - 0.3e1 * t87;
  t106 = 0.1e1 * m2;
  t107 = -t106 - t14;
  result[9] = 0.50000000000000000000000000000000000e0 * t93 * t43 * t72 * t96 + 0.50000000000000000000000000000000000e0 * t40 * t102 * t84 - 0.1e1 * t40 * t107 * t72;
  t111 = t84 * result[1];
  result[10] = 0.2e1 * t76 * t111 * result[0];
  t115 = 0.7e1 * t87;
  t117 = 0.6e1 * t27 + t115 + 0.5e1 * t16;
  t122 = 0.2e1 * m2;
  t124 = t40 * (m1 + t122);
  t125 = t84 * t41;
  t126 = t125 * t43;
  result[11] = 0.83333333333333333333333333333333333e-1 * t40 * t117 * t79 + 0.50000000000000000000000000000000000e0 * t85 + 0.25000000000000000000000000000000000e0 * t124 * t126;
  t129 = m1 * t79;
  result[12] = 0.46666666666666666666666666666666667e1 * t76 * t129;
  t131 = m1 * t84;
  result[13] = t76 * t131;
  t133 = 0.1e1 * t16;
  t134 = 0.6e1 * t87 - t89 - t133;
  result[14] = -0.33333333333333333333333333333333333e0 * t40 * t79 * t134 + 0.4e1 * t76 * t126;
  t140 = 0.9e1 * t27;
  t142 = t140 - t115 + 0.10e2 * t16;
  t146 = t40 * t41;
  t147 = t43 * t84;
  t150 = 0.56e2 * m1 + 0.69e2 * m2;
  result[15] = -0.33333333333333333333333333333333333e0 * t40 * t79 * t142 - 0.25000000000000000000000000000000000e0 * t146 * t147 * t150;
  t155 = 0.1e1 * t25;
  t158 = m2 * (t28 - 0.5e1 * t64 - t24 - t155) * t17;
  t159 = result[1] * result[0];
  t160 = 0.1e1 / t8;
  t161 = t159 * t160;
  t164 = 0.8e1 * m1;
  t167 = m2 * (-t164 - t13) * t3;
  t168 = t41 * result[1];
  t169 = t43 * result[0];
  t170 = t168 * t169;
  t171 = 0.1e1 / r12;
  t175 = 0.7e1 * m1;
  t177 = m2 * (-t175 + m2);
  t178 = t160 * result[1];
  t181 = t3 * t171;
  t182 = t27 * result[1];
  result[16] = 0.25000000000000000000000000000000000e0 * t158 * t161 - 0.16666666666666666666666666666666667e0 * t167 * t170 * t171 + 0.50000000000000000000000000000000000e0 * t177 * t178 - 0.50000000000000000000000000000000000e0 * t181 * t182;
  t185 = m2 * t15;
  t187 = 0.50000000000000000000000000000000000e0 * t185 * t181;
  t189 = 0.50000000000000000000000000000000000e0 * t181 * t27;
  result[17] = t187 - t189;
  t191 = 0.4e1 * m1;
  t192 = t191 + t13;
  t193 = m2 * t171 * t192;
  t194 = m1 * m2;
  t196 = -0.3e1 + t194 * t17;
  t200 = 0.1e1 + 0.50000000000000000000000000000000000e0 * t2 * t196 * t171;
  t201 = 0.1e1 / t200;
  result[18] = 0.50000000000000000000000000000000000e0 * t193 * t5 * result[0] * t201;
  t205 = m2 * t160;
  result[19] = -0.1e1 * t205;
  t207 = result[0] * m2;
  t208 = m1 * t160;
  result[20] = 0.2e1 * t207 * t208;
  t212 = 0.3e1 * m1;
  t214 = m2 * (-t212 + m2);
  t215 = 0.1e1 / t82;
  result[21] = 0.50000000000000000000000000000000000e0 * t205 * result[0] + 0.50000000000000000000000000000000000e0 * t214 * t215;
  t218 = 0.1e1 / t77;
  t219 = t194 * t218;
  result[22] = 0.50000000000000000000000000000000000e0 * t219;
  t220 = 0.1e1 * m1;
  t222 = m2 * (-t220 + m2);
  t223 = t77 * r12;
  t224 = 0.1e1 / t223;
  result[23] = -0.12500000000000000000000000000000000e0 * t222 * t224;
  result[24] = -0.3e1 * t219;
  t228 = m2 * t224;
  t229 = -t106 - t175;
  result[25] = 0.25000000000000000000000000000000000e0 * t228 * t229;
  t231 = t228 * t2;
  result[26] = -0.25000000000000000000000000000000000e0 * t231;
  result[27] = 0.3e1 * t194 * t215;
  result[28] = t219;
  result[29] = -result[27];
  result[30] = 0.25000000000000000000000000000000000e1 * result[28];
  t234 = -t175 - t122;
  result[31] = -0.25000000000000000000000000000000000e0 * t228 * t234;
  result[32] = 0.27500000000000000000000000000000000e1 * t231;
  result[33] = -0.1e1 * result[28];
  t238 = m2 * t218;
  result[34] = t238 * t107;
  t240 = m2 * (m2 - t14);
  result[35] = -0.1e1 * t240 * t215;
  t243 = m1 * m1;
  t244 = m2 * t243;
  t245 = t3 * t215;
  result[36] = t244 * t245;
  t246 = t160 * result[0];
  result[37] = 0.50000000000000000000000000000000000e0 * t177 * t246;
  t248 = t159 * t224;
  result[38] = 0.3e1 * t194 * t248;
  t253 = m2 * (t27 + t16 - 0.1e1 * t87) * t3;
  t254 = t159 * t218;
  result[39] = -0.1e1 * t253 * t254;
  t257 = t194 * t254;
  result[40] = 0.6e1 * t257;
  result[41] = -0.10e2 * t257;
  t259 = t159 * t215;
  result[42] = 0.3e1 * t194 * t259;
  t261 = m2 * result[1];
  t262 = result[0] * t224;
  t263 = 0.7e1 * m2;
  t264 = t263 - t212;
  result[43] = 0.50000000000000000000000000000000000e0 * t261 * t262 * t264;
  t268 = t261 * t262 * t2;
  result[44] = -0.55000000000000000000000000000000000e1 * t268;
  t272 = m2 * (-t133 + t89 + t87) * t3;
  result[45] = t272 * t254;
  t274 = m2 * (-t191 - t106);
  result[46] = -0.50000000000000000000000000000000000e0 * t274 * t254;
  t277 = 0.4e1 * t87;
  t280 = m2 * (t27 - t277 + t100) * t3;
  t282 = m2 * t168;
  t284 = t282 * t169 * t160;
  t285 = 0.16666666666666666666666666666666667e0 * t284;
  result[47] = t280 * t259 - t285;
  t286 = t27 * result[0];
  t289 = m2 * t17;
  t294 = -0.16e2 * t25 + 0.8e1 * t64 - 0.1e1 * t28 + 0.20e2 * t23;
  t295 = t294 * t160;
  result[48] = 0.50000000000000000000000000000000000e0 * t181 * t286 + 0.12500000000000000000000000000000000e0 * t289 * t295 * result[0];
  result[49] = result[17];
  t304 = m2 * (t277 - 0.6e1 * t16 + t27) * t3;
  result[50] = 0.50000000000000000000000000000000000e0 * t205 * result[1] + 0.25000000000000000000000000000000000e0 * t304 * t259 + 0.83333333333333333333333333333333333e-1 * t284;
  result[51] = result[19];
  result[52] = -result[18];
  t308 = -t122 + m1;
  t309 = m2 * t308;
  result[53] = 0.75000000000000000000000000000000000e0 * t309 * t224;
  result[54] = -result[22];
  t312 = pow(t14 + m2, 0.2e1);
  t313 = m2 * t312;
  result[55] = -0.50000000000000000000000000000000000e0 * t313 * t245;
  result[56] = result[30];
  result[57] = result[29];
  t316 = t27 * t224;
  result[58] = 0.52500000000000000000000000000000000e1 * t316 - 0.32500000000000000000000000000000000e1 * t231;
  result[59] = 0.15000000000000000000000000000000000e1 * t231;
  result[60] = result[28];
  t329 = m2 * (0.2e1 * t28 - t24 - 0.9e1 * t64 - t155) * t17;
  t332 = t289 * t160;
  t337 = 0.16e2 * t16 * r12 - 0.2e1 * t87 * r12;
  result[61] = 0.8e1 * t76 * m1 * t72 - 0.12500000000000000000000000000000000e0 * t289 * t295 * result[1] - 0.25000000000000000000000000000000000e0 * t329 * t161 - 0.83333333333333333333333333333333333e-1 * t332 * t170 * t337;
  result[62] = 0.2e1 * t261 * t208 * result[0];
  t343 = -t220 + t122;
  t344 = m2 * t343;
  result[63] = 0.75000000000000000000000000000000000e0 * t344 * t248;
  t347 = t27 * t343 * t3;
  result[64] = -0.75000000000000000000000000000000000e0 * t347 * t254;
  result[65] = 0.50000000000000000000000000000000000e0 * t257;
  result[66] = result[42];
  result[67] = -result[66];
  t351 = 0.8e1 * m2;
  t352 = 0.13e2 * m1 - t351;
  result[68] = -0.25000000000000000000000000000000000e0 * t261 * t262 * t352;
  t356 = 0.6e1 * m2;
  t358 = t356 - 0.57e2 * m1;
  result[69] = -0.25000000000000000000000000000000000e0 * t261 * t262 * t358;
  result[70] = -0.15000000000000000000000000000000000e1 * t268;
  t363 = m2 * t3;
  t364 = t363 * result[1];
  t365 = result[0] * t218;
  t369 = 0.18e2 * t27 - 0.60e2 * t87 + 0.36e2 * t16;
  result[71] = -0.25000000000000000000000000000000000e0 * t364 * t365 * t369;
  t376 = 0.25e2 * t87 - 0.12e2 * t16 - 0.21e2 * t27;
  result[72] = -0.25000000000000000000000000000000000e0 * t364 * t365 * t376;
  t380 = result[0] * t215;
  t384 = 0.4e1 * t16 - 0.2e1 * t87 - 0.1e1 * t27;
  result[73] = 0.50000000000000000000000000000000000e0 * t364 * t380 * t384;
  t388 = t100 + 0.2e1 * t27 - t277;
  result[74] = 0.50000000000000000000000000000000000e0 * t364 * t380 * t388 - t285;
  t398 = t27 * (-0.16e2 * t64 + 0.40e2 * t25 + 0.24e2 * t23 + 0.51e2 * t28) * t34;
  t406 = 0.2e1 * t27 * t8 - 0.16e2 * t87 * t8;
  t411 = t27 * (-t164 - t263);
  t416 = t42 * result[1];
  t417 = t44 * result[0];
  t418 = t416 * t417;
  result[75] = 0.41666666666666666666666666666666667e-1 * t398 * t159 * t171 - 0.83333333333333333333333333333333333e-1 * t332 * t170 * t406 + 0.16666666666666666666666666666666667e0 * t411 * t17 * t168 * t169 + 0.83333333333333333333333333333333333e-2 * t363 * t418 * r12;
  t422 = t194 * t160;
  result[76] = t187 - t189 + 0.50000000000000000000000000000000000e0 * t422;
  result[77] = result[51];
  t425 = m2 * (-t351 + m1);
  result[78] = 0.25000000000000000000000000000000000e0 * t425 * t224;
  result[79] = 0.50000000000000000000000000000000000e0 * t27 * t218;
  result[80] = result[54];
  result[81] = result[56];
  result[82] = result[57];
  result[83] = result[59];
  t428 = m1 - t356;
  result[84] = -0.75000000000000000000000000000000000e0 * t228 * t428;
  result[85] = t238 * t308;
  result[86] = result[33];
  t432 = m2 * (t351 - t220);
  result[87] = 0.25000000000000000000000000000000000e0 * t432 * t248;
  result[88] = result[65];
  result[89] = -0.75000000000000000000000000000000000e1 * t257;
  result[90] = result[70];
  t437 = (0.14e2 * m2 - t175) * m2;
  result[91] = -0.75000000000000000000000000000000000e0 * t437 * t248;
  t443 = m2 * (t90 - 0.23e2 * t87 + t140) * t3;
  result[92] = -0.50000000000000000000000000000000000e0 * t443 * t254;
  result[93] = -t285;
  result[94] = m1 * phi;
  result[95] = result[94];
  result[96] = result[95];
  result[97] = result[96];
  result[98] = result[97];
  result[99] = result[98];
  t446 = result[1] * omega;
  result[100] = -0.1e1 * t446;
  result[101] = result[0] * omega;
  t448 = m2 * r12dot;
  result[102] = t448 * t4 - 0.1e1 * t1 * t5 * omega;
  result[103] = t448 * t5 + t1 * t4 * omega;
  t459 = r12 * r12dot;
  result[104] = 0.13020833333333333333333333333333333e-1 * r12dot * t11 * t20 + 0.13020833333333333333333333333333333e-1 * t7 * (0.2e1 * t459 + r12dot * b) * t20 + 0.48828125000000000000000000000000000e-2 * t30 * r12dot * t35 + 0.48828125000000000000000000000000000e-2 * t31 * r12dot * t19 * t34;
  t473 = t40 * t168;
  result[105] = 0.16666666666666666666666666666666667e0 * t473 * t417 * t47 * omega - 0.16666666666666666666666666666666667e0 * t40 * t416 * t169 * t47 * omega - 0.20833333333333333333333333333333333e-1 * t40 * t42 * t44 * t55 * r12dot - 0.75000000000000000000000000000000000e0 * t40 * t53 * t72 * r12dot + 0.50000000000000000000000000000000000e0 * t55 * t39 * t448 - 0.20833333333333333333333333333333333e0 * t68 * t70 * t84 * r12dot;
  t500 = 0.1e1 / t46 / t223;
  t502 = t76 * t500 * r12dot;
  result[106] = 0.22500000000000000000000000000000000e1 * t502;
  result[107] = -0.11250000000000000000000000000000000e2 * t502;
  t505 = t76 * t79 * r12dot;
  result[108] = 0.7e1 * t505;
  t508 = t96 * omega;
  result[109] = t92 * result[1] * t169 * t72 * t508 - 0.1e1 * t92 * t168 * result[0] * t72 * t508 - 0.12500000000000000000000000000000000e1 * t93 * t147 * t96 * r12dot - 0.17500000000000000000000000000000000e1 * t40 * t102 * t79 * r12dot + 0.25000000000000000000000000000000000e1 * t40 * t107 * t84 * r12dot;
  t528 = t159 * r12dot;
  result[110] = -0.7e1 * t80 * t528 + 0.2e1 * t76 * t147 * omega - 0.2e1 * t76 * t125 * omega;
  result[111] = -0.37500000000000000000000000000000000e0 * t40 * t117 * t500 * r12dot - 0.17500000000000000000000000000000000e1 * t505 - 0.87500000000000000000000000000000000e0 * t124 * t79 * t41 * t43 * r12dot + 0.50000000000000000000000000000000000e0 * t124 * t111 * t169 * omega - 0.50000000000000000000000000000000000e0 * t124 * t84 * t168 * result[101];
  result[112] = -0.21e2 * t76 * m1 * t500 * r12dot;
  result[113] = -0.35000000000000000000000000000000000e1 * t76 * t129 * r12dot;
  t567 = t169 * t84;
  t572 = result[0] * t84;
  t577 = t43 * t79;
  result[114] = 0.15000000000000000000000000000000000e1 * t40 * t500 * t134 * r12dot + 0.8e1 * t76 * result[1] * t567 * omega - 0.8e1 * t76 * t168 * t572 * omega - 0.14e2 * t76 * t41 * t577 * r12dot;
  t586 = t150 * omega;
  result[115] = 0.15000000000000000000000000000000000e1 * t40 * t500 * t142 * r12dot - 0.50000000000000000000000000000000000e0 * t40 * result[1] * t567 * t586 + 0.50000000000000000000000000000000000e0 * t473 * t572 * t586 + 0.87500000000000000000000000000000000e0 * t146 * t577 * t150 * r12dot;
  t597 = t43 * omega;
  t598 = t597 * t160;
  t601 = t41 * omega;
  t602 = t601 * t160;
  t605 = t215 * r12dot;
  t606 = t159 * t605;
  t609 = t41 * t44;
  t610 = t171 * omega;
  t614 = t42 * t43;
  t618 = t160 * r12dot;
  t623 = t215 * result[1] * r12dot;
  t626 = t246 * omega;
  t629 = t3 * t160;
  result[116] = 0.25000000000000000000000000000000000e0 * t158 * t598 - 0.25000000000000000000000000000000000e0 * t158 * t602 - 0.50000000000000000000000000000000000e0 * t158 * t606 - 0.50000000000000000000000000000000000e0 * t167 * t609 * t610 + 0.50000000000000000000000000000000000e0 * t167 * t614 * t610 + 0.16666666666666666666666666666666667e0 * t167 * t170 * t618 - 0.1e1 * t177 * t623 + 0.50000000000000000000000000000000000e0 * t177 * t626 + 0.50000000000000000000000000000000000e0 * t629 * t182 * r12dot - 0.50000000000000000000000000000000000e0 * t181 * t286 * omega;
  t638 = 0.50000000000000000000000000000000000e0 * t185 * t629 * r12dot;
  t641 = 0.50000000000000000000000000000000000e0 * t629 * t27 * r12dot;
  result[117] = -t638 + t641;
  t649 = omega * t201;
  t657 = m2 * t215;
  t660 = t200 * t200;
  result[118] = -0.50000000000000000000000000000000000e0 * t205 * t192 * t3 * t159 * t201 * r12dot + 0.50000000000000000000000000000000000e0 * t193 * t3 * t43 * t649 - 0.50000000000000000000000000000000000e0 * t193 * t3 * t41 * t649 + 0.25000000000000000000000000000000000e0 * t657 * t192 * result[1] * result[0] / t660 * t196 * r12dot;
  result[119] = 0.2e1 * t657 * r12dot;
  result[120] = -0.2e1 * t446 * t422 - 0.4e1 * t207 * m1 * t215 * r12dot;
  t679 = t218 * r12dot;
  result[121] = -0.1e1 * t657 * result[0] * r12dot - 0.50000000000000000000000000000000000e0 * t205 * t446 - 0.15000000000000000000000000000000000e1 * t214 * t679;
  t682 = t224 * r12dot;
  t683 = t194 * t682;
  t684 = 0.2e1 * t683;
  result[122] = -t684;
  t686 = 0.1e1 / t77 / t8;
  t687 = t686 * r12dot;
  result[123] = 0.62500000000000000000000000000000000e0 * t222 * t687;
  result[124] = 0.12e2 * t683;
  t689 = m2 * t686;
  result[125] = -0.12500000000000000000000000000000000e1 * t689 * t229 * r12dot;
  t694 = t689 * t2 * r12dot;
  result[126] = 0.12500000000000000000000000000000000e1 * t694;
  t696 = 0.9e1 * t194 * t679;
  result[127] = -t696;
  t697 = 0.4e1 * t683;
  result[128] = -t697;
  result[129] = t696;
  result[130] = -0.10e2 * t683;
  result[131] = 0.12500000000000000000000000000000000e1 * t689 * t234 * r12dot;
  result[132] = -0.13750000000000000000000000000000000e2 * t694;
  result[133] = t697;
  result[134] = -0.4e1 * t228 * t107 * r12dot;
  result[135] = 0.3e1 * t240 * t679;
  t707 = t3 * t218 * r12dot;
  result[136] = -0.3e1 * t244 * t707;
  t710 = t380 * r12dot;
  t713 = t178 * omega;
  result[137] = -0.1e1 * t177 * t710 - 0.50000000000000000000000000000000000e0 * t177 * t713;
  t716 = t597 * t224;
  t719 = t601 * t224;
  t722 = t194 * result[1];
  t724 = result[0] * t686 * r12dot;
  result[138] = 0.3e1 * t194 * t716 - 0.3e1 * t194 * t719 - 0.15e2 * t722 * t724;
  t727 = t597 * t218;
  t730 = t601 * t218;
  t732 = t159 * t682;
  result[139] = -0.1e1 * t253 * t727 + t253 * t730 + 0.4e1 * t253 * t732;
  t735 = t194 * t727;
  t737 = t194 * t730;
  t739 = t262 * r12dot;
  t740 = t722 * t739;
  result[140] = 0.6e1 * t735 - 0.6e1 * t737 - 0.24e2 * t740;
  result[141] = -0.10e2 * t735 + 0.10e2 * t737 + 0.40e2 * t740;
  t745 = t597 * t215;
  t748 = t601 * t215;
  result[142] = 0.3e1 * t194 * t745 - 0.3e1 * t194 * t748 - 0.9e1 * t722 * t365 * r12dot;
  t754 = m2 * t43;
  t755 = omega * t224;
  t756 = t755 * t264;
  t759 = m2 * t41;
  t762 = t261 * result[0];
  result[143] = 0.50000000000000000000000000000000000e0 * t754 * t756 - 0.50000000000000000000000000000000000e0 * t759 * t756 - 0.25000000000000000000000000000000000e1 * t762 * t686 * t264 * r12dot;
  t767 = t755 * t2;
  t768 = t754 * t767;
  t770 = t759 * t767;
  t774 = t762 * t686 * t2 * r12dot;
  result[144] = -0.55000000000000000000000000000000000e1 * t768 + 0.55000000000000000000000000000000000e1 * t770 + 0.27500000000000000000000000000000000e2 * t774;
  result[145] = t272 * t727 - 0.1e1 * t272 * t730 - 0.4e1 * t272 * t732;
  result[146] = -0.50000000000000000000000000000000000e0 * t274 * t727 + 0.50000000000000000000000000000000000e0 * t274 * t730 + 0.2e1 * t274 * result[1] * t739;
  t791 = t159 * t679;
  t796 = t759 * t44 * t160 * omega;
  t797 = 0.50000000000000000000000000000000000e0 * t796;
  t799 = m2 * t42 * t598;
  t800 = 0.50000000000000000000000000000000000e0 * t799;
  t803 = t282 * t169 * t215 * r12dot;
  t804 = 0.33333333333333333333333333333333333e0 * t803;
  result[147] = t280 * t745 - 0.1e1 * t280 * t748 - 0.3e1 * t280 * t791 - t797 + t800 + t804;
  t811 = t289 * t294;
  result[148] = -0.50000000000000000000000000000000000e0 * t629 * t286 * r12dot - 0.50000000000000000000000000000000000e0 * t181 * t182 * omega - 0.25000000000000000000000000000000000e0 * t811 * t710 - 0.12500000000000000000000000000000000e0 * t811 * t713;
  result[149] = result[117];
  result[150] = -0.1e1 * t657 * result[1] * r12dot + 0.50000000000000000000000000000000000e0 * t205 * result[101] + 0.25000000000000000000000000000000000e0 * t304 * t745 - 0.25000000000000000000000000000000000e0 * t304 * t748 - 0.75000000000000000000000000000000000e0 * t304 * t791 + 0.25000000000000000000000000000000000e0 * t796 - 0.25000000000000000000000000000000000e0 * t799 - 0.16666666666666666666666666666666667e0 * t803;
  result[151] = result[119];
  result[152] = -result[118];
  result[153] = -0.37500000000000000000000000000000000e1 * t309 * t687;
  result[154] = t684;
  result[155] = 0.15000000000000000000000000000000000e1 * t313 * t707;
  result[156] = result[130];
  result[157] = result[129];
  result[158] = -0.26250000000000000000000000000000000e2 * t27 * t686 * r12dot + 0.16250000000000000000000000000000000e2 * t694;
  result[159] = -0.75000000000000000000000000000000000e1 * t694;
  result[160] = result[128];
  t851 = t289 * t215;
  t856 = t337 * omega;
  result[161] = -0.20e2 * t76 * t131 * r12dot + 0.25000000000000000000000000000000000e0 * t811 * t623 - 0.12500000000000000000000000000000000e0 * t811 * t626 - 0.25000000000000000000000000000000000e0 * t329 * t598 + 0.25000000000000000000000000000000000e0 * t329 * t602 + 0.50000000000000000000000000000000000e0 * t329 * t606 + 0.16666666666666666666666666666666667e0 * t851 * t170 * t337 * r12dot - 0.25000000000000000000000000000000000e0 * t332 * t609 * t856 + 0.25000000000000000000000000000000000e0 * t332 * t614 * t856 - 0.83333333333333333333333333333333333e-1 * t332 * t170 * (0.16e2 * t16 * r12dot - 0.2e1 * t87 * r12dot);
  result[162] = 0.2e1 * t597 * t422 - 0.4e1 * t722 * t710 - 0.2e1 * t759 * t208 * omega;
  result[163] = 0.75000000000000000000000000000000000e0 * t344 * t716 - 0.75000000000000000000000000000000000e0 * t344 * t719 - 0.37500000000000000000000000000000000e1 * t344 * result[1] * t724;
  result[164] = -0.75000000000000000000000000000000000e0 * t347 * t727 + 0.75000000000000000000000000000000000e0 * t347 * t730 + 0.3e1 * t347 * t732;
  result[165] = 0.50000000000000000000000000000000000e0 * t735 - 0.50000000000000000000000000000000000e0 * t737 - 0.2e1 * t740;
  result[166] = result[142];
  result[167] = -result[166];
  t894 = t755 * t352;
  result[168] = -0.25000000000000000000000000000000000e0 * t754 * t894 + 0.25000000000000000000000000000000000e0 * t759 * t894 + 0.12500000000000000000000000000000000e1 * t762 * t686 * t352 * r12dot;
  t903 = t755 * t358;
  result[169] = -0.25000000000000000000000000000000000e0 * t754 * t903 + 0.25000000000000000000000000000000000e0 * t759 * t903 + 0.12500000000000000000000000000000000e1 * t762 * t686 * t358 * r12dot;
  result[170] = -0.15000000000000000000000000000000000e1 * t768 + 0.15000000000000000000000000000000000e1 * t770 + 0.75000000000000000000000000000000000e1 * t774;
  t915 = t363 * t43;
  t916 = omega * t218;
  t917 = t916 * t369;
  t920 = t363 * t41;
  result[171] = -0.25000000000000000000000000000000000e0 * t915 * t917 + 0.25000000000000000000000000000000000e0 * t920 * t917 + t364 * t262 * t369 * r12dot;
  t926 = t916 * t376;
  result[172] = -0.25000000000000000000000000000000000e0 * t915 * t926 + 0.25000000000000000000000000000000000e0 * t920 * t926 + t364 * t262 * t376 * r12dot;
  t934 = omega * t215;
  t935 = t934 * t384;
  result[173] = 0.50000000000000000000000000000000000e0 * t915 * t935 - 0.50000000000000000000000000000000000e0 * t920 * t935 - 0.15000000000000000000000000000000000e1 * t364 * t365 * t384 * r12dot;
  t944 = t934 * t388;
  result[174] = 0.50000000000000000000000000000000000e0 * t915 * t944 - 0.50000000000000000000000000000000000e0 * t920 * t944 - 0.15000000000000000000000000000000000e1 * t364 * t365 * t388 * r12dot - t797 + t800 + t804;
  t966 = t406 * omega;
  t982 = t411 * t17;
  result[175] = 0.41666666666666666666666666666666667e-1 * t398 * t597 * t171 - 0.41666666666666666666666666666666667e-1 * t398 * t601 * t171 - 0.41666666666666666666666666666666667e-1 * t398 * t159 * t618 + 0.16666666666666666666666666666666667e0 * t851 * t170 * t406 * r12dot - 0.25000000000000000000000000000000000e0 * t332 * t609 * t966 + 0.25000000000000000000000000000000000e0 * t332 * t614 * t966 - 0.83333333333333333333333333333333333e-1 * t332 * t170 * (0.4e1 * t27 * r12 * r12dot - 0.32e2 * t87 * t459) + 0.50000000000000000000000000000000000e0 * t982 * t609 * omega - 0.50000000000000000000000000000000000e0 * t982 * t614 * omega + 0.41666666666666666666666666666666667e-1 * t363 * t42 * t44 * t43 * r12 * omega - 0.41666666666666666666666666666666667e-1 * t363 * t42 * t41 * t44 * r12 * omega + 0.83333333333333333333333333333333333e-2 * t363 * t418 * r12dot;
  result[176] = -t638 + t641 - 0.1e1 * t194 * t605;
  result[177] = result[151];
  result[178] = -0.12500000000000000000000000000000000e1 * t425 * t687;
  result[179] = -0.2e1 * t316 * r12dot;
  result[180] = result[154];
  result[181] = result[156];
  result[182] = result[157];
  result[183] = result[159];
  result[184] = 0.37500000000000000000000000000000000e1 * t689 * t428 * r12dot;
  result[185] = -0.4e1 * t228 * t308 * r12dot;
  result[186] = result[133];
  result[187] = -0.12500000000000000000000000000000000e1 * t432 * t686 * t528 + 0.25000000000000000000000000000000000e0 * t432 * t716 - 0.25000000000000000000000000000000000e0 * t432 * t719;
  result[188] = result[165];
  result[189] = -0.75000000000000000000000000000000000e1 * t735 + 0.75000000000000000000000000000000000e1 * t737 + 0.30e2 * t740;
  result[190] = result[170];
  result[191] = -0.75000000000000000000000000000000000e0 * t437 * t716 + 0.75000000000000000000000000000000000e0 * t437 * t719 + 0.37500000000000000000000000000000000e1 * t437 * result[1] * t724;
  result[192] = -0.50000000000000000000000000000000000e0 * t443 * t727 + 0.50000000000000000000000000000000000e0 * t443 * t730 + 0.2e1 * t443 * t732;
  result[193] = -t797 + t800 + t804;
  result[194] = m1 * omega;
  result[195] = result[194];
  result[196] = result[195];
  result[197] = result[196];
  result[198] = result[197];
  result[199] = result[198];
  cgret[0] = result[0];
  cgret[1] = result[1];
  cgret[2] = result[2];
  cgret[3] = result[3];
  cgret[4] = result[4];
  cgret[5] = result[5];
  cgret[6] = result[6];
  cgret[7] = result[7];
  cgret[8] = result[8];
  cgret[9] = result[9];
  cgret[10] = result[10];
  cgret[11] = result[11];
  cgret[12] = result[12];
  cgret[13] = result[13];
  cgret[14] = result[14];
  cgret[15] = result[15];
  cgret[16] = result[16];
  cgret[17] = result[17];
  cgret[18] = result[18];
  cgret[19] = result[19];
  cgret[20] = result[20];
  cgret[21] = result[21];
  cgret[22] = result[22];
  cgret[23] = result[23];
  cgret[24] = result[24];
  cgret[25] = result[25];
  cgret[26] = result[26];
  cgret[27] = result[27];
  cgret[28] = result[28];
  cgret[29] = result[29];
  cgret[30] = result[30];
  cgret[31] = result[31];
  cgret[32] = result[32];
  cgret[33] = result[33];
  cgret[34] = result[34];
  cgret[35] = result[35];
  cgret[36] = result[36];
  cgret[37] = result[37];
  cgret[38] = result[38];
  cgret[39] = result[39];
  cgret[40] = result[40];
  cgret[41] = result[41];
  cgret[42] = result[42];
  cgret[43] = result[43];
  cgret[44] = result[44];
  cgret[45] = result[45];
  cgret[46] = result[46];
  cgret[47] = result[47];
  cgret[48] = result[48];
  cgret[49] = result[49];
  cgret[50] = result[50];
  cgret[51] = result[51];
  cgret[52] = result[52];
  cgret[53] = result[53];
  cgret[54] = result[54];
  cgret[55] = result[55];
  cgret[56] = result[56];
  cgret[57] = result[57];
  cgret[58] = result[58];
  cgret[59] = result[59];
  cgret[60] = result[60];
  cgret[61] = result[61];
  cgret[62] = result[62];
  cgret[63] = result[63];
  cgret[64] = result[64];
  cgret[65] = result[65];
  cgret[66] = result[66];
  cgret[67] = result[67];
  cgret[68] = result[68];
  cgret[69] = result[69];
  cgret[70] = result[70];
  cgret[71] = result[71];
  cgret[72] = result[72];
  cgret[73] = result[73];
  cgret[74] = result[74];
  cgret[75] = result[75];
  cgret[76] = result[76];
  cgret[77] = result[77];
  cgret[78] = result[78];
  cgret[79] = result[79];
  cgret[80] = result[80];
  cgret[81] = result[81];
  cgret[82] = result[82];
  cgret[83] = result[83];
  cgret[84] = result[84];
  cgret[85] = result[85];
  cgret[86] = result[86];
  cgret[87] = result[87];
  cgret[88] = result[88];
  cgret[89] = result[89];
  cgret[90] = result[90];
  cgret[91] = result[91];
  cgret[92] = result[92];
  cgret[93] = result[93];
  cgret[94] = result[94];
  cgret[95] = result[95];
  cgret[96] = result[96];
  cgret[97] = result[97];
  cgret[98] = result[98];
  cgret[99] = result[99];
  cgret[100] = result[100];
  cgret[101] = result[101];
  cgret[102] = result[102];
  cgret[103] = result[103];
  cgret[104] = result[104];
  cgret[105] = result[105];
  cgret[106] = result[106];
  cgret[107] = result[107];
  cgret[108] = result[108];
  cgret[109] = result[109];
  cgret[110] = result[110];
  cgret[111] = result[111];
  cgret[112] = result[112];
  cgret[113] = result[113];
  cgret[114] = result[114];
  cgret[115] = result[115];
  cgret[116] = result[116];
  cgret[117] = result[117];
  cgret[118] = result[118];
  cgret[119] = result[119];
  cgret[120] = result[120];
  cgret[121] = result[121];
  cgret[122] = result[122];
  cgret[123] = result[123];
  cgret[124] = result[124];
  cgret[125] = result[125];
  cgret[126] = result[126];
  cgret[127] = result[127];
  cgret[128] = result[128];
  cgret[129] = result[129];
  cgret[130] = result[130];
  cgret[131] = result[131];
  cgret[132] = result[132];
  cgret[133] = result[133];
  cgret[134] = result[134];
  cgret[135] = result[135];
  cgret[136] = result[136];
  cgret[137] = result[137];
  cgret[138] = result[138];
  cgret[139] = result[139];
  cgret[140] = result[140];
  cgret[141] = result[141];
  cgret[142] = result[142];
  cgret[143] = result[143];
  cgret[144] = result[144];
  cgret[145] = result[145];
  cgret[146] = result[146];
  cgret[147] = result[147];
  cgret[148] = result[148];
  cgret[149] = result[149];
  cgret[150] = result[150];
  cgret[151] = result[151];
  cgret[152] = result[152];
  cgret[153] = result[153];
  cgret[154] = result[154];
  cgret[155] = result[155];
  cgret[156] = result[156];
  cgret[157] = result[157];
  cgret[158] = result[158];
  cgret[159] = result[159];
  cgret[160] = result[160];
  cgret[161] = result[161];
  cgret[162] = result[162];
  cgret[163] = result[163];
  cgret[164] = result[164];
  cgret[165] = result[165];
  cgret[166] = result[166];
  cgret[167] = result[167];
  cgret[168] = result[168];
  cgret[169] = result[169];
  cgret[170] = result[170];
  cgret[171] = result[171];
  cgret[172] = result[172];
  cgret[173] = result[173];
  cgret[174] = result[174];
  cgret[175] = result[175];
  cgret[176] = result[176];
  cgret[177] = result[177];
  cgret[178] = result[178];
  cgret[179] = result[179];
  cgret[180] = result[180];
  cgret[181] = result[181];
  cgret[182] = result[182];
  cgret[183] = result[183];
  cgret[184] = result[184];
  cgret[185] = result[185];
  cgret[186] = result[186];
  cgret[187] = result[187];
  cgret[188] = result[188];
  cgret[189] = result[189];
  cgret[190] = result[190];
  cgret[191] = result[191];
  cgret[192] = result[192];
  cgret[193] = result[193];
  cgret[194] = result[194];
  cgret[195] = result[195];
  cgret[196] = result[196];
  cgret[197] = result[197];
  cgret[198] = result[198];
  cgret[199] = result[199];
}

*/
/********
IZ_time_tidal_v0 is based on IZ_time_tidal_v0 at
./init_data/confcurvBBHID/maple/test_c-files/Profiling/Ricci_dr_v4a/IZ_time_tidal_v0.c
********/
/*
static void IZ_time_tidal_v0 (
  double m1,
  double m2,
  double r12,
  double r12dot,
  double phi,
  double omega,
  double b,
  double A2_Et[4][4],
  double A2_Etdot[4][4],
  double A2_Etdot0[4][4],
  double A3_Et[4][4][4],
  double A3_Ct[4][4][4],
  double A3_Ctdot[4][4][4],
  double A3_Ctdot0[4][4][4],
  double A4_Ct[4][4][4][4])
{
  double t1;
  double t2;
  double t3;
  double t4;
  double t5;
  double t6;
  double t7;
  double t8;
  double t9;
  double t10;
  double t11;
  double t12;
  double t13;
  double t14;
  double t15;
  double t16;
  double t17;
  double t18;
  double t19;
  double t20;
  double t21;
  double t24;
  double t25;
  double t27;
  double t28;
  double t29;
  double t30;
  double t31;
  double t32;
  double t35;
  double t40;



  A2_Et[0][0] = 0.0e0;
  A2_Et[0][1] = 0.0e0;
  A2_Et[0][2] = 0.0e0;
  A2_Et[0][3] = 0.0e0;
  A2_Et[1][0] = 0.0e0;
  t1 = 0.2e1 * b;
  t2 = cos(phi);
  t3 = t2 * t2;
  t8 = m1 + m2;
  t9 = t8 * t3;
  t11 = sin(phi);
  t12 = t11 * t11;
  t13 = t8 * t12;
  t17 = b * b;
  t18 = t17 * t17;
  t19 = 0.1e1 / t18;
  A2_Et[1][1] = -0.50000000000000000000000000000000000e0 * m2 * (-t1 + 0.6e1 * b * t3 - 0.3e1 * t3 * m2 + m2 - 0.4e1 * t9 + 0.5e1 * t13) * t19;
  A2_Et[1][2] = -0.15000000000000000000000000000000000e1 * m2 * t11 * t2 * (t1 - 0.4e1 * m2 - 0.3e1 * m1) * t19;
  A2_Et[1][3] = 0.0e0;
  A2_Et[2][0] = 0.0e0;
  A2_Et[2][1] = A2_Et[1][2];
  A2_Et[2][2] = -0.50000000000000000000000000000000000e0 * m2 * (-t1 + 0.6e1 * b * t12 - 0.3e1 * t12 * m2 + m2 - 0.4e1 * t13 + 0.5e1 * t9) * t19;
  A2_Et[2][3] = 0.0e0;
  A2_Et[3][0] = 0.0e0;
  A2_Et[3][1] = 0.0e0;
  A2_Et[3][2] = 0.0e0;
  A2_Et[3][3] = 0.50000000000000000000000000000000000e0 * m2 * (t1 + m1) * t19;

  A2_Etdot[0][0] = 0.0e0;
  A2_Etdot[0][1] = 0.0e0;
  A2_Etdot[0][2] = 0.0e0;
  A2_Etdot[0][3] = 0.0e0;
  A2_Etdot[1][0] = 0.0e0;
  t1 = cos(phi);
  t3 = sin(phi);
  t8 = 0.2e1 * b - 0.4e1 * m2 - 0.3e1 * m1;
  t10 = b * b;
  t11 = t10 * t10;
  t12 = 0.1e1 / t11;
  A2_Etdot[1][1] = 0.3e1 * m2 * t1 * t3 * omega * t8 * t12;
  t16 = t1 * t1;
  t17 = t3 * t3;
  A2_Etdot[1][2] = -0.15000000000000000000000000000000000e1 * m2 * omega * t8 * (t16 - 0.1e1 * t17) * t12;
  A2_Etdot[1][3] = 0.0e0;
  A2_Etdot[2][0] = 0.0e0;
  A2_Etdot[2][1] = A2_Etdot[1][2];
  A2_Etdot[2][2] = -A2_Etdot[1][1];
  A2_Etdot[2][3] = 0.0e0;
  A2_Etdot[3][0] = 0.0e0;
  A2_Etdot[3][1] = 0.0e0;
  A2_Etdot[3][2] = 0.0e0;
  A2_Etdot[3][3] = 0.0e0;

  A2_Etdot0[0][0] = 0.0e0;
  A2_Etdot0[0][1] = 0.0e0;
  A2_Etdot0[0][2] = 0.0e0;
  A2_Etdot0[0][3] = 0.0e0;
  A2_Etdot0[1][0] = 0.0e0;
  A2_Etdot0[1][1] = 0.0e0;
  t1 = b * b;
  t2 = t1 * t1;
  t7 = sqrt(m2 / b);
  t11 = sqrt((m1 + m2) / m2);
  A2_Etdot0[1][2] = -0.3e1 * m2 / t2 * t7 * t11;
  A2_Etdot0[1][3] = 0.0e0;
  A2_Etdot0[2][0] = 0.0e0;
  A2_Etdot0[2][1] = A2_Etdot0[1][2];
  A2_Etdot0[2][2] = 0.0e0;
  A2_Etdot0[2][3] = 0.0e0;
  A2_Etdot0[3][0] = 0.0e0;
  A2_Etdot0[3][1] = 0.0e0;
  A2_Etdot0[3][2] = 0.0e0;
  A2_Etdot0[3][3] = 0.0e0;

  A3_Et[0][0][0] = 0.0e0;
  A3_Et[0][0][1] = 0.0e0;
  A3_Et[0][0][2] = 0.0e0;
  A3_Et[0][0][3] = 0.0e0;
  A3_Et[0][1][0] = 0.0e0;
  A3_Et[0][1][1] = 0.0e0;
  A3_Et[0][1][2] = 0.0e0;
  A3_Et[0][1][3] = 0.0e0;
  A3_Et[0][2][0] = 0.0e0;
  A3_Et[0][2][1] = 0.0e0;
  A3_Et[0][2][2] = 0.0e0;
  A3_Et[0][2][3] = 0.0e0;
  A3_Et[0][3][0] = 0.0e0;
  A3_Et[0][3][1] = 0.0e0;
  A3_Et[0][3][2] = 0.0e0;
  A3_Et[0][3][3] = 0.0e0;
  A3_Et[1][0][0] = 0.0e0;
  A3_Et[1][0][1] = 0.0e0;
  A3_Et[1][0][2] = 0.0e0;
  A3_Et[1][0][3] = 0.0e0;
  A3_Et[1][1][0] = 0.0e0;
  t1 = cos(phi);
  t2 = m2 * t1;
  t3 = t1 * t1;
  t4 = b * t3;
  t6 = 0.3e1 * b;
  t7 = 0.9e1 * m2;
  t8 = t3 * m2;
  t10 = m1 + m2;
  t11 = t10 * t3;
  t13 = sin(phi);
  t14 = t13 * t13;
  t15 = t10 * t14;
  t16 = 0.4e1 * t15;
  t18 = b * b;
  t19 = t18 * t18;
  t21 = 0.1e1 / t19 / b;
  A3_Et[1][1][1] = 0.3e1 * t2 * (0.5e1 * t4 - t6 + t7 - 0.15e2 * t8 - 0.1e1 * t11 + t16) * t21;
  t24 = t13 * m2;
  A3_Et[1][1][2] = t24 * (0.15e2 * t4 - t6 + t7 - 0.45e2 * t8 - 0.11e2 * t11 + t16) * t21;
  A3_Et[1][1][3] = 0.0e0;
  A3_Et[1][2][0] = 0.0e0;
  A3_Et[1][2][1] = A3_Et[1][1][2];
  t30 = b * t14;
  t32 = t14 * m2;
  t35 = 0.4e1 * t11;
  A3_Et[1][2][2] = t2 * (0.15e2 * t30 - t6 + t7 - 0.45e2 * t32 - 0.11e2 * t15 + t35) * t21;
  A3_Et[1][2][3] = 0.0e0;
  A3_Et[1][3][0] = 0.0e0;
  A3_Et[1][3][1] = 0.0e0;
  A3_Et[1][3][2] = 0.0e0;
  t40 = (t6 - 0.8e1 * m2 + m1) * t21;
  A3_Et[1][3][3] = -0.1e1 * t2 * t40;
  A3_Et[2][0][0] = 0.0e0;
  A3_Et[2][0][1] = 0.0e0;
  A3_Et[2][0][2] = 0.0e0;
  A3_Et[2][0][3] = 0.0e0;
  A3_Et[2][1][0] = 0.0e0;
  A3_Et[2][1][1] = A3_Et[1][2][1];
  A3_Et[2][1][2] = A3_Et[1][2][2];
  A3_Et[2][1][3] = 0.0e0;
  A3_Et[2][2][0] = 0.0e0;
  A3_Et[2][2][1] = A3_Et[2][1][2];
  A3_Et[2][2][2] = 0.3e1 * t24 * (0.5e1 * t30 - t6 + t7 - 0.15e2 * t32 - 0.1e1 * t15 + t35) * t21;
  A3_Et[2][2][3] = 0.0e0;
  A3_Et[2][3][0] = 0.0e0;
  A3_Et[2][3][1] = 0.0e0;
  A3_Et[2][3][2] = 0.0e0;
  A3_Et[2][3][3] = -0.1e1 * t24 * t40;
  A3_Et[3][0][0] = 0.0e0;
  A3_Et[3][0][1] = 0.0e0;
  A3_Et[3][0][2] = 0.0e0;
  A3_Et[3][0][3] = 0.0e0;
  A3_Et[3][1][0] = 0.0e0;
  A3_Et[3][1][1] = 0.0e0;
  A3_Et[3][1][2] = 0.0e0;
  A3_Et[3][1][3] = A3_Et[1][3][3];
  A3_Et[3][2][0] = 0.0e0;
  A3_Et[3][2][1] = 0.0e0;
  A3_Et[3][2][2] = 0.0e0;
  A3_Et[3][2][3] = A3_Et[2][3][3];
  A3_Et[3][3][0] = 0.0e0;
  A3_Et[3][3][1] = A3_Et[3][1][3];
  A3_Et[3][3][2] = A3_Et[3][2][3];
  A3_Et[3][3][3] = 0.0e0;

  A3_Ct[0][0][0] = 0.0e0;
  A3_Ct[0][0][1] = 0.0e0;
  A3_Ct[0][0][2] = 0.0e0;
  A3_Ct[0][0][3] = 0.0e0;
  A3_Ct[0][1][0] = 0.0e0;
  A3_Ct[0][1][1] = 0.0e0;
  A3_Ct[0][1][2] = 0.0e0;
  A3_Ct[0][1][3] = 0.0e0;
  A3_Ct[0][2][0] = 0.0e0;
  A3_Ct[0][2][1] = 0.0e0;
  A3_Ct[0][2][2] = 0.0e0;
  A3_Ct[0][2][3] = 0.0e0;
  A3_Ct[0][3][0] = 0.0e0;
  A3_Ct[0][3][1] = 0.0e0;
  A3_Ct[0][3][2] = 0.0e0;
  A3_Ct[0][3][3] = 0.0e0;
  A3_Ct[1][0][0] = 0.0e0;
  A3_Ct[1][0][1] = 0.0e0;
  A3_Ct[1][0][2] = 0.0e0;
  A3_Ct[1][0][3] = 0.0e0;
  A3_Ct[1][1][0] = 0.0e0;
  A3_Ct[1][1][1] = 0.0e0;
  A3_Ct[1][1][2] = 0.0e0;
  A3_Ct[1][1][3] = 0.0e0;
  A3_Ct[1][2][0] = 0.0e0;
  t2 = m2 / b;
  t3 = sqrt(t2);
  t4 = t3 * t2;
  t5 = cos(phi);
  t7 = m1 + m2;
  t9 = t7 / m2;
  t10 = sqrt(t9);
  t20 = sqrt(m2 / t7);
  t24 = b * b;
  t27 = (-0.6e1 * t10 * b + 0.5e1 * m2 * t10 * t9 + 0.7e1 * m2 * t10 - 0.3e1 * t20 * m2) / t24 / b;
  A3_Ct[1][2][1] = 0.50000000000000000000000000000000000e0 * t4 * t5 * t27;
  t29 = sin(phi);
  A3_Ct[1][2][2] = 0.50000000000000000000000000000000000e0 * t4 * t29 * t27;
  A3_Ct[1][2][3] = 0.0e0;
  A3_Ct[1][3][0] = 0.0e0;
  A3_Ct[1][3][1] = 0.0e0;
  A3_Ct[1][3][2] = 0.0e0;
  A3_Ct[1][3][3] = -A3_Ct[1][2][2];
  A3_Ct[2][0][0] = 0.0e0;
  A3_Ct[2][0][1] = 0.0e0;
  A3_Ct[2][0][2] = 0.0e0;
  A3_Ct[2][0][3] = 0.0e0;
  A3_Ct[2][1][0] = 0.0e0;
  A3_Ct[2][1][1] = -A3_Ct[1][2][1];
  A3_Ct[2][1][2] = A3_Ct[1][3][3];
  A3_Ct[2][1][3] = 0.0e0;
  A3_Ct[2][2][0] = 0.0e0;
  A3_Ct[2][2][1] = 0.0e0;
  A3_Ct[2][2][2] = 0.0e0;
  A3_Ct[2][2][3] = 0.0e0;
  A3_Ct[2][3][0] = 0.0e0;
  A3_Ct[2][3][1] = 0.0e0;
  A3_Ct[2][3][2] = 0.0e0;
  A3_Ct[2][3][3] = A3_Ct[1][2][1];
  A3_Ct[3][0][0] = 0.0e0;
  A3_Ct[3][0][1] = 0.0e0;
  A3_Ct[3][0][2] = 0.0e0;
  A3_Ct[3][0][3] = 0.0e0;
  A3_Ct[3][1][0] = 0.0e0;
  A3_Ct[3][1][1] = 0.0e0;
  A3_Ct[3][1][2] = 0.0e0;
  A3_Ct[3][1][3] = -A3_Ct[2][1][2];
  A3_Ct[3][2][0] = 0.0e0;
  A3_Ct[3][2][1] = 0.0e0;
  A3_Ct[3][2][2] = 0.0e0;
  A3_Ct[3][2][3] = -A3_Ct[2][3][3];
  A3_Ct[3][3][0] = 0.0e0;
  A3_Ct[3][3][1] = 0.0e0;
  A3_Ct[3][3][2] = 0.0e0;
  A3_Ct[3][3][3] = 0.0e0;

  A3_Ctdot[0][0][0] = 0.0e0;
  A3_Ctdot[0][0][1] = 0.0e0;
  A3_Ctdot[0][0][2] = 0.0e0;
  A3_Ctdot[0][0][3] = 0.0e0;
  A3_Ctdot[0][1][0] = 0.0e0;
  A3_Ctdot[0][1][1] = 0.0e0;
  A3_Ctdot[0][1][2] = 0.0e0;
  A3_Ctdot[0][1][3] = 0.0e0;
  A3_Ctdot[0][2][0] = 0.0e0;
  A3_Ctdot[0][2][1] = 0.0e0;
  A3_Ctdot[0][2][2] = 0.0e0;
  A3_Ctdot[0][2][3] = 0.0e0;
  A3_Ctdot[0][3][0] = 0.0e0;
  A3_Ctdot[0][3][1] = 0.0e0;
  A3_Ctdot[0][3][2] = 0.0e0;
  A3_Ctdot[0][3][3] = 0.0e0;
  A3_Ctdot[1][0][0] = 0.0e0;
  A3_Ctdot[1][0][1] = 0.0e0;
  A3_Ctdot[1][0][2] = 0.0e0;
  A3_Ctdot[1][0][3] = 0.0e0;
  A3_Ctdot[1][1][0] = 0.0e0;
  A3_Ctdot[1][1][1] = 0.0e0;
  A3_Ctdot[1][1][2] = 0.0e0;
  A3_Ctdot[1][1][3] = 0.0e0;
  A3_Ctdot[1][2][0] = 0.0e0;
  t2 = m2 / b;
  t3 = sqrt(t2);
  t4 = t3 * t2;
  t5 = sin(phi);
  t7 = m1 + m2;
  t9 = t7 / m2;
  t10 = sqrt(t9);
  t20 = sqrt(m2 / t7);
  t25 = b * b;
  t28 = omega * (-0.6e1 * t10 * b + 0.5e1 * m2 * t10 * t9 + 0.7e1 * m2 * t10 - 0.3e1 * t20 * m2) / t25 / b;
  t30 = 0.50000000000000000000000000000000000e0 * t4 * t5 * t28;
  A3_Ctdot[1][2][1] = -t30;
  t31 = cos(phi);
  A3_Ctdot[1][2][2] = 0.50000000000000000000000000000000000e0 * t31 * t4 * t28;
  A3_Ctdot[1][2][3] = 0.0e0;
  A3_Ctdot[1][3][0] = 0.0e0;
  A3_Ctdot[1][3][1] = 0.0e0;
  A3_Ctdot[1][3][2] = 0.0e0;
  A3_Ctdot[1][3][3] = -A3_Ctdot[1][2][2];
  A3_Ctdot[2][0][0] = 0.0e0;
  A3_Ctdot[2][0][1] = 0.0e0;
  A3_Ctdot[2][0][2] = 0.0e0;
  A3_Ctdot[2][0][3] = 0.0e0;
  A3_Ctdot[2][1][0] = 0.0e0;
  A3_Ctdot[2][1][1] = t30;
  A3_Ctdot[2][1][2] = A3_Ctdot[1][3][3];
  A3_Ctdot[2][1][3] = 0.0e0;
  A3_Ctdot[2][2][0] = 0.0e0;
  A3_Ctdot[2][2][1] = 0.0e0;
  A3_Ctdot[2][2][2] = 0.0e0;
  A3_Ctdot[2][2][3] = 0.0e0;
  A3_Ctdot[2][3][0] = 0.0e0;
  A3_Ctdot[2][3][1] = 0.0e0;
  A3_Ctdot[2][3][2] = 0.0e0;
  A3_Ctdot[2][3][3] = A3_Ctdot[1][2][1];
  A3_Ctdot[3][0][0] = 0.0e0;
  A3_Ctdot[3][0][1] = 0.0e0;
  A3_Ctdot[3][0][2] = 0.0e0;
  A3_Ctdot[3][0][3] = 0.0e0;
  A3_Ctdot[3][1][0] = 0.0e0;
  A3_Ctdot[3][1][1] = 0.0e0;
  A3_Ctdot[3][1][2] = 0.0e0;
  A3_Ctdot[3][1][3] = -A3_Ctdot[2][1][2];
  A3_Ctdot[3][2][0] = 0.0e0;
  A3_Ctdot[3][2][1] = 0.0e0;
  A3_Ctdot[3][2][2] = 0.0e0;
  A3_Ctdot[3][2][3] = -A3_Ctdot[2][3][3];
  A3_Ctdot[3][3][0] = 0.0e0;
  A3_Ctdot[3][3][1] = 0.0e0;
  A3_Ctdot[3][3][2] = 0.0e0;
  A3_Ctdot[3][3][3] = 0.0e0;

  A3_Ctdot0[0][0][0] = 0.0e0;
  A3_Ctdot0[0][0][1] = 0.0e0;
  A3_Ctdot0[0][0][2] = 0.0e0;
  A3_Ctdot0[0][0][3] = 0.0e0;
  A3_Ctdot0[0][1][0] = 0.0e0;
  A3_Ctdot0[0][1][1] = 0.0e0;
  A3_Ctdot0[0][1][2] = 0.0e0;
  A3_Ctdot0[0][1][3] = 0.0e0;
  A3_Ctdot0[0][2][0] = 0.0e0;
  A3_Ctdot0[0][2][1] = 0.0e0;
  A3_Ctdot0[0][2][2] = 0.0e0;
  A3_Ctdot0[0][2][3] = 0.0e0;
  A3_Ctdot0[0][3][0] = 0.0e0;
  A3_Ctdot0[0][3][1] = 0.0e0;
  A3_Ctdot0[0][3][2] = 0.0e0;
  A3_Ctdot0[0][3][3] = 0.0e0;
  A3_Ctdot0[1][0][0] = 0.0e0;
  A3_Ctdot0[1][0][1] = 0.0e0;
  A3_Ctdot0[1][0][2] = 0.0e0;
  A3_Ctdot0[1][0][3] = 0.0e0;
  A3_Ctdot0[1][1][0] = 0.0e0;
  A3_Ctdot0[1][1][1] = 0.0e0;
  A3_Ctdot0[1][1][2] = 0.0e0;
  A3_Ctdot0[1][1][3] = 0.0e0;
  A3_Ctdot0[1][2][0] = 0.0e0;
  A3_Ctdot0[1][2][1] = 0.0e0;
  t1 = b * b;
  t2 = t1 * t1;
  t8 = 0.3e1 * m2 / t2 / b * (m1 + m2);
  A3_Ctdot0[1][2][2] = -t8;
  A3_Ctdot0[1][2][3] = 0.0e0;
  A3_Ctdot0[1][3][0] = 0.0e0;
  A3_Ctdot0[1][3][1] = 0.0e0;
  A3_Ctdot0[1][3][2] = 0.0e0;
  A3_Ctdot0[1][3][3] = t8;
  A3_Ctdot0[2][0][0] = 0.0e0;
  A3_Ctdot0[2][0][1] = 0.0e0;
  A3_Ctdot0[2][0][2] = 0.0e0;
  A3_Ctdot0[2][0][3] = 0.0e0;
  A3_Ctdot0[2][1][0] = 0.0e0;
  A3_Ctdot0[2][1][1] = 0.0e0;
  A3_Ctdot0[2][1][2] = -A3_Ctdot0[1][2][2];
  A3_Ctdot0[2][1][3] = 0.0e0;
  A3_Ctdot0[2][2][0] = 0.0e0;
  A3_Ctdot0[2][2][1] = 0.0e0;
  A3_Ctdot0[2][2][2] = 0.0e0;
  A3_Ctdot0[2][2][3] = 0.0e0;
  A3_Ctdot0[2][3][0] = 0.0e0;
  A3_Ctdot0[2][3][1] = 0.0e0;
  A3_Ctdot0[2][3][2] = 0.0e0;
  A3_Ctdot0[2][3][3] = 0.0e0;
  A3_Ctdot0[3][0][0] = 0.0e0;
  A3_Ctdot0[3][0][1] = 0.0e0;
  A3_Ctdot0[3][0][2] = 0.0e0;
  A3_Ctdot0[3][0][3] = 0.0e0;
  A3_Ctdot0[3][1][0] = 0.0e0;
  A3_Ctdot0[3][1][1] = 0.0e0;
  A3_Ctdot0[3][1][2] = 0.0e0;
  A3_Ctdot0[3][1][3] = -A3_Ctdot0[1][3][3];
  A3_Ctdot0[3][2][0] = 0.0e0;
  A3_Ctdot0[3][2][1] = 0.0e0;
  A3_Ctdot0[3][2][2] = 0.0e0;
  A3_Ctdot0[3][2][3] = 0.0e0;
  A3_Ctdot0[3][3][0] = 0.0e0;
  A3_Ctdot0[3][3][1] = 0.0e0;
  A3_Ctdot0[3][3][2] = 0.0e0;
  A3_Ctdot0[3][3][3] = 0.0e0;

  A4_Ct[0][0][0][0] = 0.0e0;
  A4_Ct[0][0][0][1] = 0.0e0;
  A4_Ct[0][0][0][2] = 0.0e0;
  A4_Ct[0][0][0][3] = 0.0e0;
  A4_Ct[0][0][1][0] = 0.0e0;
  A4_Ct[0][0][1][1] = 0.0e0;
  A4_Ct[0][0][1][2] = 0.0e0;
  A4_Ct[0][0][1][3] = 0.0e0;
  A4_Ct[0][0][2][0] = 0.0e0;
  A4_Ct[0][0][2][1] = 0.0e0;
  A4_Ct[0][0][2][2] = 0.0e0;
  A4_Ct[0][0][2][3] = 0.0e0;
  A4_Ct[0][0][3][0] = 0.0e0;
  A4_Ct[0][0][3][1] = 0.0e0;
  A4_Ct[0][0][3][2] = 0.0e0;
  A4_Ct[0][0][3][3] = 0.0e0;
  A4_Ct[0][1][0][0] = 0.0e0;
  A4_Ct[0][1][0][1] = 0.0e0;
  A4_Ct[0][1][0][2] = 0.0e0;
  A4_Ct[0][1][0][3] = 0.0e0;
  A4_Ct[0][1][1][0] = 0.0e0;
  A4_Ct[0][1][1][1] = 0.0e0;
  A4_Ct[0][1][1][2] = 0.0e0;
  A4_Ct[0][1][1][3] = 0.0e0;
  A4_Ct[0][1][2][0] = 0.0e0;
  A4_Ct[0][1][2][1] = 0.0e0;
  A4_Ct[0][1][2][2] = 0.0e0;
  A4_Ct[0][1][2][3] = 0.0e0;
  A4_Ct[0][1][3][0] = 0.0e0;
  A4_Ct[0][1][3][1] = 0.0e0;
  A4_Ct[0][1][3][2] = 0.0e0;
  A4_Ct[0][1][3][3] = 0.0e0;
  A4_Ct[0][2][0][0] = 0.0e0;
  A4_Ct[0][2][0][1] = 0.0e0;
  A4_Ct[0][2][0][2] = 0.0e0;
  A4_Ct[0][2][0][3] = 0.0e0;
  A4_Ct[0][2][1][0] = 0.0e0;
  A4_Ct[0][2][1][1] = 0.0e0;
  A4_Ct[0][2][1][2] = 0.0e0;
  A4_Ct[0][2][1][3] = 0.0e0;
  A4_Ct[0][2][2][0] = 0.0e0;
  A4_Ct[0][2][2][1] = 0.0e0;
  A4_Ct[0][2][2][2] = 0.0e0;
  A4_Ct[0][2][2][3] = 0.0e0;
  A4_Ct[0][2][3][0] = 0.0e0;
  A4_Ct[0][2][3][1] = 0.0e0;
  A4_Ct[0][2][3][2] = 0.0e0;
  A4_Ct[0][2][3][3] = 0.0e0;
  A4_Ct[0][3][0][0] = 0.0e0;
  A4_Ct[0][3][0][1] = 0.0e0;
  A4_Ct[0][3][0][2] = 0.0e0;
  A4_Ct[0][3][0][3] = 0.0e0;
  A4_Ct[0][3][1][0] = 0.0e0;
  A4_Ct[0][3][1][1] = 0.0e0;
  A4_Ct[0][3][1][2] = 0.0e0;
  A4_Ct[0][3][1][3] = 0.0e0;
  A4_Ct[0][3][2][0] = 0.0e0;
  A4_Ct[0][3][2][1] = 0.0e0;
  A4_Ct[0][3][2][2] = 0.0e0;
  A4_Ct[0][3][2][3] = 0.0e0;
  A4_Ct[0][3][3][0] = 0.0e0;
  A4_Ct[0][3][3][1] = 0.0e0;
  A4_Ct[0][3][3][2] = 0.0e0;
  A4_Ct[0][3][3][3] = 0.0e0;
  A4_Ct[1][0][0][0] = 0.0e0;
  A4_Ct[1][0][0][1] = 0.0e0;
  A4_Ct[1][0][0][2] = 0.0e0;
  A4_Ct[1][0][0][3] = 0.0e0;
  A4_Ct[1][0][1][0] = 0.0e0;
  A4_Ct[1][0][1][1] = 0.0e0;
  A4_Ct[1][0][1][2] = 0.0e0;
  A4_Ct[1][0][1][3] = 0.0e0;
  A4_Ct[1][0][2][0] = 0.0e0;
  A4_Ct[1][0][2][1] = 0.0e0;
  A4_Ct[1][0][2][2] = 0.0e0;
  A4_Ct[1][0][2][3] = 0.0e0;
  A4_Ct[1][0][3][0] = 0.0e0;
  A4_Ct[1][0][3][1] = 0.0e0;
  A4_Ct[1][0][3][2] = 0.0e0;
  A4_Ct[1][0][3][3] = 0.0e0;
  A4_Ct[1][1][0][0] = 0.0e0;
  A4_Ct[1][1][0][1] = 0.0e0;
  A4_Ct[1][1][0][2] = 0.0e0;
  A4_Ct[1][1][0][3] = 0.0e0;
  A4_Ct[1][1][1][0] = 0.0e0;
  A4_Ct[1][1][1][1] = 0.0e0;
  A4_Ct[1][1][1][2] = 0.0e0;
  A4_Ct[1][1][1][3] = 0.0e0;
  A4_Ct[1][1][2][0] = 0.0e0;
  A4_Ct[1][1][2][1] = 0.0e0;
  A4_Ct[1][1][2][2] = 0.0e0;
  A4_Ct[1][1][2][3] = 0.0e0;
  A4_Ct[1][1][3][0] = 0.0e0;
  A4_Ct[1][1][3][1] = 0.0e0;
  A4_Ct[1][1][3][2] = 0.0e0;
  A4_Ct[1][1][3][3] = 0.0e0;
  A4_Ct[1][2][0][0] = 0.0e0;
  A4_Ct[1][2][0][1] = 0.0e0;
  A4_Ct[1][2][0][2] = 0.0e0;
  A4_Ct[1][2][0][3] = 0.0e0;
  A4_Ct[1][2][1][0] = 0.0e0;
  t2 = m2 / b;
  t3 = sqrt(t2);
  t4 = t3 * t2;
  t8 = sqrt((m1 + m2) / m2);
  t9 = t4 * t8;
  t10 = cos(phi);
  t11 = t10 * t10;
  t14 = b * b;
  t16 = 0.1e1 / t14 / b;
  A4_Ct[1][2][1][1] = 0.15000000000000000000000000000000000e1 * t9 * (0.5e1 * t11 - 0.1e1) * t16;
  t19 = t16 * t4;
  t20 = sin(phi);
  A4_Ct[1][2][1][2] = 0.75000000000000000000000000000000000e1 * t19 * t8 * t20 * t10;
  A4_Ct[1][2][1][3] = 0.0e0;
  A4_Ct[1][2][2][0] = 0.0e0;
  A4_Ct[1][2][2][1] = A4_Ct[1][2][1][2];
  t24 = t20 * t20;
  A4_Ct[1][2][2][2] = 0.15000000000000000000000000000000000e1 * t9 * (0.5e1 * t24 - 0.1e1) * t16;
  A4_Ct[1][2][2][3] = 0.0e0;
  A4_Ct[1][2][3][0] = 0.0e0;
  A4_Ct[1][2][3][1] = 0.0e0;
  A4_Ct[1][2][3][2] = 0.0e0;
  A4_Ct[1][2][3][3] = -0.45000000000000000000000000000000000e1 * t19 * t8;
  A4_Ct[1][3][0][0] = 0.0e0;
  A4_Ct[1][3][0][1] = 0.0e0;
  A4_Ct[1][3][0][2] = 0.0e0;
  A4_Ct[1][3][0][3] = 0.0e0;
  A4_Ct[1][3][1][0] = 0.0e0;
  A4_Ct[1][3][1][1] = 0.0e0;
  A4_Ct[1][3][1][2] = 0.0e0;
  A4_Ct[1][3][1][3] = -A4_Ct[1][2][2][1];
  A4_Ct[1][3][2][0] = 0.0e0;
  A4_Ct[1][3][2][1] = 0.0e0;
  A4_Ct[1][3][2][2] = 0.0e0;
  A4_Ct[1][3][2][3] = -A4_Ct[1][2][2][2];
  A4_Ct[1][3][3][0] = 0.0e0;
  A4_Ct[1][3][3][1] = A4_Ct[1][3][1][3];
  A4_Ct[1][3][3][2] = A4_Ct[1][3][2][3];
  A4_Ct[1][3][3][3] = 0.0e0;
  A4_Ct[2][0][0][0] = 0.0e0;
  A4_Ct[2][0][0][1] = 0.0e0;
  A4_Ct[2][0][0][2] = 0.0e0;
  A4_Ct[2][0][0][3] = 0.0e0;
  A4_Ct[2][0][1][0] = 0.0e0;
  A4_Ct[2][0][1][1] = 0.0e0;
  A4_Ct[2][0][1][2] = 0.0e0;
  A4_Ct[2][0][1][3] = 0.0e0;
  A4_Ct[2][0][2][0] = 0.0e0;
  A4_Ct[2][0][2][1] = 0.0e0;
  A4_Ct[2][0][2][2] = 0.0e0;
  A4_Ct[2][0][2][3] = 0.0e0;
  A4_Ct[2][0][3][0] = 0.0e0;
  A4_Ct[2][0][3][1] = 0.0e0;
  A4_Ct[2][0][3][2] = 0.0e0;
  A4_Ct[2][0][3][3] = 0.0e0;
  A4_Ct[2][1][0][0] = 0.0e0;
  A4_Ct[2][1][0][1] = 0.0e0;
  A4_Ct[2][1][0][2] = 0.0e0;
  A4_Ct[2][1][0][3] = 0.0e0;
  A4_Ct[2][1][1][0] = 0.0e0;
  A4_Ct[2][1][1][1] = -A4_Ct[1][2][1][1];
  A4_Ct[2][1][1][2] = A4_Ct[1][3][3][1];
  A4_Ct[2][1][1][3] = 0.0e0;
  A4_Ct[2][1][2][0] = 0.0e0;
  A4_Ct[2][1][2][1] = A4_Ct[2][1][1][2];
  A4_Ct[2][1][2][2] = A4_Ct[1][3][2][3];
  A4_Ct[2][1][2][3] = 0.0e0;
  A4_Ct[2][1][3][0] = 0.0e0;
  A4_Ct[2][1][3][1] = 0.0e0;
  A4_Ct[2][1][3][2] = 0.0e0;
  A4_Ct[2][1][3][3] = -A4_Ct[1][2][3][3];
  A4_Ct[2][2][0][0] = 0.0e0;
  A4_Ct[2][2][0][1] = 0.0e0;
  A4_Ct[2][2][0][2] = 0.0e0;
  A4_Ct[2][2][0][3] = 0.0e0;
  A4_Ct[2][2][1][0] = 0.0e0;
  A4_Ct[2][2][1][1] = 0.0e0;
  A4_Ct[2][2][1][2] = 0.0e0;
  A4_Ct[2][2][1][3] = 0.0e0;
  A4_Ct[2][2][2][0] = 0.0e0;
  A4_Ct[2][2][2][1] = 0.0e0;
  A4_Ct[2][2][2][2] = 0.0e0;
  A4_Ct[2][2][2][3] = 0.0e0;
  A4_Ct[2][2][3][0] = 0.0e0;
  A4_Ct[2][2][3][1] = 0.0e0;
  A4_Ct[2][2][3][2] = 0.0e0;
  A4_Ct[2][2][3][3] = 0.0e0;
  A4_Ct[2][3][0][0] = 0.0e0;
  A4_Ct[2][3][0][1] = 0.0e0;
  A4_Ct[2][3][0][2] = 0.0e0;
  A4_Ct[2][3][0][3] = 0.0e0;
  A4_Ct[2][3][1][0] = 0.0e0;
  A4_Ct[2][3][1][1] = 0.0e0;
  A4_Ct[2][3][1][2] = 0.0e0;
  A4_Ct[2][3][1][3] = A4_Ct[1][2][1][1];
  A4_Ct[2][3][2][0] = 0.0e0;
  A4_Ct[2][3][2][1] = 0.0e0;
  A4_Ct[2][3][2][2] = 0.0e0;
  A4_Ct[2][3][2][3] = A4_Ct[1][2][2][1];
  A4_Ct[2][3][3][0] = 0.0e0;
  A4_Ct[2][3][3][1] = A4_Ct[2][3][1][3];
  A4_Ct[2][3][3][2] = A4_Ct[2][3][2][3];
  A4_Ct[2][3][3][3] = 0.0e0;
  A4_Ct[3][0][0][0] = 0.0e0;
  A4_Ct[3][0][0][1] = 0.0e0;
  A4_Ct[3][0][0][2] = 0.0e0;
  A4_Ct[3][0][0][3] = 0.0e0;
  A4_Ct[3][0][1][0] = 0.0e0;
  A4_Ct[3][0][1][1] = 0.0e0;
  A4_Ct[3][0][1][2] = 0.0e0;
  A4_Ct[3][0][1][3] = 0.0e0;
  A4_Ct[3][0][2][0] = 0.0e0;
  A4_Ct[3][0][2][1] = 0.0e0;
  A4_Ct[3][0][2][2] = 0.0e0;
  A4_Ct[3][0][2][3] = 0.0e0;
  A4_Ct[3][0][3][0] = 0.0e0;
  A4_Ct[3][0][3][1] = 0.0e0;
  A4_Ct[3][0][3][2] = 0.0e0;
  A4_Ct[3][0][3][3] = 0.0e0;
  A4_Ct[3][1][0][0] = 0.0e0;
  A4_Ct[3][1][0][1] = 0.0e0;
  A4_Ct[3][1][0][2] = 0.0e0;
  A4_Ct[3][1][0][3] = 0.0e0;
  A4_Ct[3][1][1][0] = 0.0e0;
  A4_Ct[3][1][1][1] = 0.0e0;
  A4_Ct[3][1][1][2] = 0.0e0;
  A4_Ct[3][1][1][3] = A4_Ct[2][3][3][2];
  A4_Ct[3][1][2][0] = 0.0e0;
  A4_Ct[3][1][2][1] = 0.0e0;
  A4_Ct[3][1][2][2] = 0.0e0;
  A4_Ct[3][1][2][3] = A4_Ct[1][2][2][2];
  A4_Ct[3][1][3][0] = 0.0e0;
  A4_Ct[3][1][3][1] = A4_Ct[3][1][1][3];
  A4_Ct[3][1][3][2] = A4_Ct[3][1][2][3];
  A4_Ct[3][1][3][3] = 0.0e0;
  A4_Ct[3][2][0][0] = 0.0e0;
  A4_Ct[3][2][0][1] = 0.0e0;
  A4_Ct[3][2][0][2] = 0.0e0;
  A4_Ct[3][2][0][3] = 0.0e0;
  A4_Ct[3][2][1][0] = 0.0e0;
  A4_Ct[3][2][1][1] = 0.0e0;
  A4_Ct[3][2][1][2] = 0.0e0;
  A4_Ct[3][2][1][3] = A4_Ct[2][1][1][1];
  A4_Ct[3][2][2][0] = 0.0e0;
  A4_Ct[3][2][2][1] = 0.0e0;
  A4_Ct[3][2][2][2] = 0.0e0;
  A4_Ct[3][2][2][3] = A4_Ct[2][1][2][1];
  A4_Ct[3][2][3][0] = 0.0e0;
  A4_Ct[3][2][3][1] = A4_Ct[3][2][1][3];
  A4_Ct[3][2][3][2] = A4_Ct[3][2][2][3];
  A4_Ct[3][2][3][3] = 0.0e0;
  A4_Ct[3][3][0][0] = 0.0e0;
  A4_Ct[3][3][0][1] = 0.0e0;
  A4_Ct[3][3][0][2] = 0.0e0;
  A4_Ct[3][3][0][3] = 0.0e0;
  A4_Ct[3][3][1][0] = 0.0e0;
  A4_Ct[3][3][1][1] = 0.0e0;
  A4_Ct[3][3][1][2] = 0.0e0;
  A4_Ct[3][3][1][3] = 0.0e0;
  A4_Ct[3][3][2][0] = 0.0e0;
  A4_Ct[3][3][2][1] = 0.0e0;
  A4_Ct[3][3][2][2] = 0.0e0;
  A4_Ct[3][3][2][3] = 0.0e0;
  A4_Ct[3][3][3][0] = 0.0e0;
  A4_Ct[3][3][3][1] = 0.0e0;
  A4_Ct[3][3][3][2] = 0.0e0;
  A4_Ct[3][3][3][3] = 0.0e0;

  return;
}
*/

/********
IZ_const_funcs_v0 is based on IZ_const_funcs_v0 at
./init_data/confcurvBBHID/maple/test_c-files/Profiling/Ricci_dr_v4a/IZ_const_funcs_v0.c
********/
/*
double IZ_const_funcs_v0 (double m1, double m2)
{
  double t1;
  t1 = m1 * m1;
  return(0.4e1 * t1);
}
*/

void initialize_metric_routines(char *name)
{
  hid_t file_id, header_id, grid_id;
  
  file_id  = myH5_Fopen(name,0);
  header_id = H5Gopen(file_id,"Header");
  grid_id = H5Gopen(header_id,"Grid");

  myH5_read_scalar2_orig(grid_id,"m_bh1",H5T_NATIVE_DOUBLE,&m_bh1);
  myH5_read_scalar2_orig(grid_id,"m_bh2",H5T_NATIVE_DOUBLE,&m_bh2);
  myH5_read_scalar2_orig(grid_id,"initial_bbh_separation",H5T_NATIVE_DOUBLE,&initial_bbh_separation);
  printf("Initializing metric routine variables m1 m2 a0 %e %e %e\n",m_bh1,m_bh2,initial_bbh_separation);
  return;
}
