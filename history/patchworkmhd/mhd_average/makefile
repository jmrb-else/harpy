.PHONY : failure all clean newrun vclean runit runit2 \
	emacs emn tags newtest archive redo testinterp



#***********************************************************************************
####  COMPILER :  0 (gcc)   1  (icc)    2  (pgcc)  
#***********************************************************************************
COMPILER = 0

#####  set MAKESDFS to 0 if you do not want to use the sdf libraries:
MAKESDFS = 0

#####  set MAKEHDFS to 0 if you do not want to use the hdf5 libraries:
MAKEHDFS = 1

#####  set USE_GSL to 0 if you do not want to use the GSL libraries:
USE_GSL = 0


#***********************************************************************************
#***********************************************************************************
# Basic compiler options and settings:
#
#  OPTIONS: Set the makefile variables that are to be sent to the
#            pre-compiler: If you add an option above, please add
#            it here so that you can use the flag as a
#            pre-compiler macro in the source code.
#
#***********************************************************************************
#***********************************************************************************
OPTIONS = 
OTHER_CFLAGS = -Wall -fopenmp
OTHER_LFLAGS = 
EXTRA_LIBS = -lm -fopenmp

#***********************************************************************************
# Intel compiler options
#***********************************************************************************
ifeq ($(COMPILER),1)
COMP       = icc
#CCFLAGS  = -g 
CCFLAGS = -O3 -DINTEL_HEADER 
OTHER_CFLAGS +=  -I$(INTEL_INC_DIR)
OTHER_LFLAGS +=  -L$(INTEL_LIB_DIR)
endif

#***********************************************************************************
# PGI  compiler options
#***********************************************************************************
ifeq ($(COMPILER),2)
COMP       = pgcc
endif

#***********************************************************************************
# gcc  compiler options
#***********************************************************************************
ifeq ($(COMPILER),0)
COMP       = gcc
CCFLAGS  = -O3
#CCFLAGS  = -pg
#CCFLAGS = -O0 -g
endif


#***********************************************************************************
# sdf libraries
#***********************************************************************************
ifeq ($(MAKESDFS),1)
OTHER_CFLAGS +=  -I${HOME}/local/include
OTHER_LFLAGS +=  -L${HOME}/local/lib 
EXTRA_LIBS += -lrnpl -lxvs -lsvs
endif

#***********************************************************************************
# Hdf5 libraries
#***********************************************************************************
ifeq ($(MAKEHDFS),1)

ifndef HDF_HOME
ifdef HDF_DIR
HDF_HOME=$(HDF_DIR)
else 
ifeq ($(USE_MODULE_ENVIRONMENT),0)
failure:
	@echo "HDF_HOME needs to be set in user's environment or in cluster-specific environment in the makefile"
endif
endif
endif

OPTIONS      += -DH5_USE_16_API  
EXTRA_LIBS   += -lz -lhdf5_hl -lhdf5
HDF_INCLUDE   = $(HDF_HOME)/include
HDF_LIB       = $(HDF_HOME)/lib
HDF_BIN       = $(HDF_HOME)/bin
OTHER_CFLAGS +=  -I$(HDF_INCLUDE)
OTHER_LFLAGS +=  -L$(HDF_LIB)

endif

#***********************************************************************************
# GSL libraries:  GSL_HOME needs to be set by environment
#***********************************************************************************
ifeq ($(USE_GSL),1)

ifndef GSL_HOME
ifdef GSL_DIR
GSL_HOME=$(GSL_DIR)
else 
ifeq ($(USE_MODULE_ENVIRONMENT),0)
failure:
	@echo "GSL_HOME needs to be set in user's environment or in cluster-specific environment in the makefile"
endif
endif
endif

OPTIONS      += -DUSE_GSL 
EXTRA_LIBS   += -lgsl -lgslcblas -lm
GSL_INCLUDE   = $(GSL_HOME)/include
GSL_LIB       = $(GSL_HOME)/lib
OTHER_CFLAGS += -I$(GSL_INCLUDE)
OTHER_LFLAGS += -L$(GSL_HOME)
endif



#***********************************************************************************
#***********************************************************************************
# Begin of compiler and option independent section
#***********************************************************************************
#***********************************************************************************
CCCFLAGS = $(OPTIONS) $(OTHER_CFLAGS)  -c
CCLFLAGS = $(OPTIONS) $(OTHER_LFLAGS)  

CC_COMPILE  = $(CC) $(CCFLAGS) $(CCCFLAGS) 
CC_LOAD     = $(CC) $(CCFLAGS) $(CCLFLAGS) 

EXE = averager
MACH = mach.list 

SRCS = main.c  dump_hdf.c coord.c utility.c metric_dyn_bbh_fullpn_nz_iz_2nd_v01.c
INCS = defs.h


BASE = $(basename $(SRCS) )
OBJS = $(addsuffix .o, $(BASE) )


#***********************************************************************************
# Dependencies and Make Rules
#***********************************************************************************
.c.o: 
	$(CC_COMPILE) $*.c

all: $(EXE) 


$(OBJS) : $(INCS) makefile

$(EXE): $(OBJS) 
	$(CC_LOAD) $(OBJS) $(EXTRA_LIBS) -o $(EXE)


clean:
	/bin/rm -f *.o 
	/bin/rm -f *.oo 
	/bin/rm -f *.il 
	/bin/rm -f TAGS
	/bin/rm -f $(EXE) 


newrun:
	/bin/rm -rf dumps
	/bin/rm -rf images
	/bin/rm -f  ener.out
	/bin/rm -f  *.sdf
	/bin/rm -rf history
	/bin/rm -f *.h5
	/bin/rm -f *.out 

vclean: 
	make newrun
	make clean

runit: 
	make newrun
	make clean
	make
	./harm3d > output_log.dat 2>&1 

runit2: 
	nohup mpirun -np 8 ./harm3d 2 4 1 256 128 1 > output_log.dat 2>&1 &


emacs: 
	emacs *.c *.h README CHANGES makefile  &

emn: 
	emacs -nw *.c *.h README CHANGES makefile

tags: $(INCS) $(SRCS)
	etags $(INCS) $(SRCS)

newtest:
	make newrun
	make clean
	make > make.out 2>&1
	./harm3d 1 1 1 32 32 32  > output_log.dat 2>&1  & 
	ln -s . dumps

archive:
	tar zcvf code.tgz *.c *.h makefile README* CHANGES 

redo:
	make newrun
	make clean
	make > make.out 2>&1
	cp ../cart/KDHARM0.000000.h5 rdump_start_other.h5 
	./harm3d  1 1 1 256 1 256 > output_log.dat  2>&1  & 
	ln -s . dumps


##  This is used as an example for setting up environment variable for interpolating calculation:
testinterp:
	cp ../../diag3/KDHARM0.000000.h5 ../diag3
	make newrun
	make clean
	make > make.out 2>&1
	export DEST_INTERP_FILENAME="../diag3/KDHARM0.000000.h5" ; mpirun -n 4 ./harm3d  2 1 2 64 1 64  > output_log.dat  2>&1  
	ln -s . dumps


#cleanmpi: 

