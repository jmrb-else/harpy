#!/usr/bin/env python

import sys

#path to python routine polp_new.py
#sys.path.append ("/home/dennis/local/python")
#sys.path.append ("/home/dennis/python/warp_coord")
#sys.path.append ("/home/dennis/local/python/bbhdisk")

#import numpy as np

import matplotlib
matplotlib.use('Agg')

import sys

args = sys.argv[1:]

from polp_new import polp

for it in args:
    print 'it =', it
    i = int(it)

    try:
        #polp(itime=i,dirname='.',funcname='ucon1',dirout='.',userad=0, plot_grid=0, plot_bhs_traj=0,savepng=1, show_plot=0, trajfile='traj/my_bbh_trajectory.out', rmax=100,visc_circles=0,plotcentre=None,fillcutout=0,ubh=False,nperplot=20,corotate=0,nticks=7,scale=None,spinning=False,background='black',fillwedge=1,dolog=0,ith='equator',iph=None,EXTEND_CENTRAL_CUTOUT=1,minf=-0.001,maxf=0.001)
        polp(itime=i,dirname='.',funcname='rho',dirout='.',userad=0, plot_grid=0, plot_bhs_traj=1,savepng=1, show_plot=0, trajfile='traj/my_bbh_trajectory.out', rmax=100,visc_circles=0,plotcentre=None,fillcutout=0,ubh=False,nperplot=20,corotate=0,nticks=7,scale=None,spinning=False,background='black',fillwedge=1,dolog=1,ith='equator',iph=None,EXTEND_CENTRAL_CUTOUT=1,minf=-8,maxf=-1)
    except KeyError:
        print "Error reading it =", i

    except ValueError:
        print "interpolation error? it =", i

    except IOError:
        print "error opening file at it =", i




