"""  

  merge_surface_h5.py: 
  ------------
    -- program to put collapse a series of surface data files into one 
       "history-file-like" format;
    -- looks for all the files that matches  "basename.N.h5"  where "N" is 
        a zero-padded  6-digit integer. 

"""

import numpy as np
import h5py

import os,sys
import glob 

###############################################################################
###############################################################################
###############################################################################

def find_list_of_h5files(base):
    """  
    Returns with the list of files matching 'base.N.h5' where 'N' is a
    zero-padded 6-digit integer.
    """

    globname = './' + base + ".[0-9]?????.h5"
    h5list = glob.glob(globname)

    if len(h5list) <= 0:
        print("Cannot find any files to merge with basename = ", base)
        sys.exit("Can no longer proceed, exiting....")

    list.sort(h5list)
    return h5list

###############################################################################
###############################################################################
###############################################################################

def merge_surface_h5(basename='KDHARM0'):
    """  
    Primary routine that calls all the functions and loops over the files
    and grid functions.
    """

    # Obtain list of files in current working directory to process
    surfacename = basename+'.surface'
    h5list = find_list_of_h5files(surfacename)
    n_h5files = len(h5list) 
    print(basename)
    print(surfacename)
    print(h5list)

    # Set output file name 
    destfilename = surfacename + '.all.h5'
    print(destfilename)
    sys.stdout.flush()
    
    #################
    # Read various details about datasets inside example file : 
    #################
    f = h5py.File(h5list[0],'r')

    group_names = ('/Bound', '/Unbound', '/Jet')
    n_groups = len(group_names)

    #  open group in root directory 
    grp = f[group_names[0]]           

    #  list of names of grid functions
    grid_funcs_all = grp.keys()     
    grid_funcs = [func for func in grid_funcs_all if "S_rhoav" in func]
    n_funcs = len(grid_funcs)
    print("matching grid_funcs = ", grid_funcs, n_funcs)
    if n_funcs <= 0:
        print("There are no functions matching the special function, n_funcs = ", n_funcs)
        print("Here are the list of possible grid functions: ", grid_funcs_all)
        sys.exit("No functions selected, can no longer proceed, exiting....")

    sys.stdout.flush()

    #  actual grid function data for first on the list:
    g0 = grp[grid_funcs[0]]     
    dims = (n_h5files, ) + g0.shape
    src_dims = g0.shape
    dset_type = g0.dtype 

    # Create all the datasets in the dest file: 
    fdest = h5py.File(destfilename,'w-')    # writing to it, fail if it exists
    f.copy('/Header',fdest)
    f.close()


    time_arr = np.zeros((n_h5files,),dtype=dset_type)
    gfunc_arr = np.zeros(src_dims,dtype=dset_type)

    for group_name in group_names:
        grp_dest = fdest.create_group(group_name)
        for gfunc in grid_funcs: 
            dataset = grp_dest.create_dataset(gfunc,dims,dtype=dset_type)
 
 
    # Now copy over directly each grid-function from each file to the
    # destination file's dataset:
    for i in range(n_h5files):
        print("Reading file ", h5list[i], "....")
        sys.stdout.flush()
        f = h5py.File(h5list[i],'r')
                
        # First copy over the time coordinate: 
        f['/Header/Grid/t'].read_direct(time_arr, np.s_[0:1], np.s_[i:i+1])
 
        # Now transfer the data for each group for all grid functions:
        for group_name in group_names:
            grp_src  = f[group_name]
            grp_dest = fdest[group_name]
 
            for gfunc_name in grid_funcs:
                gfunc_src = grp_src[gfunc_name]
                gfunc_dest = grp_dest[gfunc_name]
                gfunc_dest[i,:,:] = gfunc_src[:,:]
  
        f.close()
     
 
    dset = fdest.create_dataset('tout', (n_h5files,), dtype=dset_type, data=time_arr)

    ####################################################
    # Copy over radius coordinate array: 
    ####################################################
    gdumpname = basename + '.gdump.h5'
    print("Copying over the radius array from ", gdumpname)
    sys.stdout.flush()
    fgdump = h5py.File(gdumpname,'r')
    nr = fgdump['/Header/totalsize1'][0]
    rout = np.zeros((nr,),dtype=dset_type)
    fgdump['x1'].read_direct(rout, np.s_[0:nr,0,0], np.s_[0:nr])
    dset = fdest.create_dataset('rout', (nr,), dtype=dset_type, data=rout)
    fgdump.close()

    
    fdest.close()

    print("All done merging the ", n_h5files, " into the new file: ", destfilename)


###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python merge_surface_h5
# 
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) > 1 :
        merge_surface_h5(sys.argv[1])
    else:
        merge_surface_h5()

