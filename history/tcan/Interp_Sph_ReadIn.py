"""
interp_sph_grids.dat File Reader. Compatible with Python 2.7+ and 3.6+ at least.

Zachariah B. Etienne

Based on Python scripts written by Bernard Kelly:
https://bitbucket.org/zach_etienne/nrpy/src/master/mhd_diagnostics/

Find the latest version of this reader at the bottom of this Jupyter notebook:
https://github.com/zachetienne/nrpytutorial/blob/master/Tutorial-ETK_thorn-Interpolation_to_Spherical_Grids.ipynb

Usage instructions:

From the command-line, run via:
python Interp_Sph_ReadIn.py interp_sph_grids.dat [number of gridfunctions (58 or so)] [outfile]

Currently the last parameter "outfile" is required but not actually used.
"""
import numpy as np
import struct
import sys
import argparse
import h5py
import math

parser = argparse.ArgumentParser(description='Read file.')
parser.add_argument("datafile", help="main data file")
parser.add_argument("number_of_gridfunctions", help="number of gridfunctions")

parser.add_argument("outfileroot", help="root of output file names")

args = parser.parse_args()

datafile = args.datafile
outfileroot = args.outfileroot
number_of_gridfunctions = int(args.number_of_gridfunctions)

print("reading from "+str(datafile))

"""
read_char_array():
Reads a character array of size="size"
from a file (with file handle = "filehandle")
and returns the character array as a proper 
Python string.
"""
def read_char_array(filehandle,size):
    reached_end_of_string = False
    chartmp = struct.unpack(str(size)+'s', filehandle.read(size))[0]

    #https://docs.python.org/3/library/codecs.html#codecs.decode
    char_array_orig = chartmp.decode('utf-8',errors='ignore')

    char_array = ""
    for i in range(len(char_array_orig)):
        char = char_array_orig[i]
        # C strings end in '\0', which in Python-ese is '\x00'.
        #   As characters read after the end of the string will
        #   generally be gibberish, we no longer append 
        #   to the output string after '\0' is reached.
        if   sys.version_info[0]==3 and bytes(char.encode('utf-8')) == b'\x00':
            reached_end_of_string = True
        elif sys.version_info[0]==2 and char ==  '\x00':
            reached_end_of_string = True

        if reached_end_of_string == False:
            char_array += char
        else:
            pass # Continue until we've read 'size' bytes
    return char_array

"""
read_header()
Reads the header from a file.
"""
def read_header(filehandle):
    # This function makes extensive use of Python's struct.unpack
    # https://docs.python.org/3/library/struct.html
    # First store gridfunction name and interpolation order used:
    # fwrite(gf_name, 100*sizeof(char), 1, file);
    gf_name = read_char_array(filehandle,100)
    # fwrite(order, sizeof(CCTK_INT), 1, file);
    order = struct.unpack('i',filehandle.read(4))[0]

    # Then the radial grid parameters:
    # fwrite( & N0, sizeof(CCTK_INT), 1, file);
    N0 =    struct.unpack('i',filehandle.read(4))[0]
    # fwrite( & R0, sizeof(CCTK_REAL), 1, file);
    R0 =    struct.unpack('d',filehandle.read(8))[0]
    # fwrite( & Rin, sizeof(CCTK_REAL), 1, file);
    Rin =   struct.unpack('d',filehandle.read(8))[0]
    # fwrite( & Rout, sizeof(CCTK_REAL), 1, file);
    Rout =  struct.unpack('d',filehandle.read(8))[0]

    # Then the grid parameters related to the theta coordinate:
    # fwrite( & N1, sizeof(CCTK_INT), 1, file);
    N1           = struct.unpack('i', filehandle.read(4))[0]
    # fwrite( & x1_beg, sizeof(CCTK_REAL), 1, file);
    x1_beg       = struct.unpack('d', filehandle.read(8))[0]
    # fwrite( & theta_option, sizeof(CCTK_INT), 1, file);
    theta_option = struct.unpack('i', filehandle.read(4))[0]
    # fwrite( & th_c, sizeof(CCTK_REAL), 1, file);
    th_c         = struct.unpack('d', filehandle.read(8))[0]
    # fwrite( & xi, sizeof(CCTK_REAL), 1, file);
    xi           = struct.unpack('d', filehandle.read(8))[0]
    # fwrite( & th_n, sizeof(CCTK_INT), 1, file);
    th_n         = struct.unpack('i', filehandle.read(4))[0]

    # Then the grid parameters related to the phi coordinate:
    # fwrite( & N2, sizeof(CCTK_INT), 1, file);
    N2     = struct.unpack('i', filehandle.read(4))[0]
    # fwrite( & x2_beg, sizeof(CCTK_REAL), 1, file);
    x2_beg = struct.unpack('d', filehandle.read(8))[0]

    magic_number_check = 1.130814081305130e-21
    # fwrite( & magic_number, sizeof(CCTK_REAL), 1, file);
    magic_number = struct.unpack('d', filehandle.read(8))[0]
    if magic_number != magic_number_check:
        print("Error: Possible file corruption: Magic number mismatch. Found magic number = "+str(magic_number)+" . Expected "+str(magic_number_check))
        exit(1)
    # fwrite( & cctk_iteration, sizeof(CCTK_INT), 1, file);
    cctk_iteration = struct.unpack('i', filehandle.read(4))[0]
    # fwrite( & cctk_time, sizeof(CCTK_REAL), 1, file);
    cctk_time      = struct.unpack('d', filehandle.read(8))[0]
    

    return gf_name,order,N0,R0,Rin,Rout,N1,x1_beg,theta_option,th_c,xi,th_n,N2,x2_beg,cctk_iteration,cctk_time

###Create the names of the Data Header that is read in by Harm3D, the order of this
###array is important because it mimics the order of gf_name. 
harm3d_group_names = ['rho','uu','v1','v2','v3','B1','B2','B3','gcov00','gcov01','gcov02','gcov03','gcov11','gcov12',\
                          'gcov13','gcov22','gcov23','gcov33','4Christoffel000', '4Christoffel001', '4Christoffel002', '4Christoffel003',\
                          '4Christoffel011', '4Christoffel012', '4Christoffel013','4Christoffel022', '4Christoffel023', '4Christoffel033', \
                          '4Christoffel100', '4Christoffel101', '4Christoffel102', '4Christoffel103', '4Christoffel111', '4Christoffel112',\
                          '4Christoffel113', '4Christoffel122', '4Christoffel123', '4Christoffel133', '4Christoffel200', '4Christoffel201', \
                          '4Christoffel202', '4Christoffel203','4Christoffel211', '4Christoffel212', '4Christoffel213', '4Christoffel222', \
                          '4Christoffel223',  '4Christoffel233', '4Christoffel300', '4Christoffel301', '4Christoffel302', '4Christoffel303', \
                          '4Christoffel311', '4Christoffel312', '4Christoffel313', '4Christoffel322', '4Christoffel323','4Christoffel333']

  
# Now open the file and read all the data
with open(datafile,"rb") as f:
    # Main loop over all gridfunctions
    for i in range(number_of_gridfunctions):
        # Data are output in chunks, one gridfunction at a time, with metadata
        #    for each gridfunction stored at the top of each chunk
        # First read in the metadata:
        gf_name, order, N0, R0, Rin, Rout, N1, x1_beg, theta_option, th_c, xi, th_n, N2, x2_beg, cctk_iteration, cctk_time = read_header(f)
        print("\n\nReading gridfunction "+gf_name)
        data_chunk_size = N0*N1*N2*8 # 8 bytes per double-precision number
        # Next read in the full gridfunction data
        bytechunk = f.read(data_chunk_size)
        # Process the data using NumPy's frombuffer() function:
        #   https://docs.scipy.org/doc/numpy/reference/generated/numpy.frombuffer.html
        buffer_res = np.frombuffer(bytechunk)
        # Reshape the data into a 3D NumPy array:
        #   https://docs.scipy.org/doc/numpy/reference/generated/numpy.reshape.html
        this_data = buffer_res.reshape(N0,N1,N2)
        

        # Sanity check: Make sure the output in the "middle" of the grid looks reasonable.
        ii = int(N0/2)
        jj = int(N1/2)
        kk = int(N2/2)
        print(gf_name,this_data[kk,jj,ii])
        
        
        #Write the data and the corresponding group names to an H5 file in the format read in by Harm3D
        with h5py.File('rdump_start.h5', 'a') as t:
            tset = t.create_dataset(harm3d_group_names[i], data=this_data)
            

            
# Now open the file and read all the data
with open(datafile,"rb") as f:
    # First read in the metadata:
    gf_name, order, N0, R0, Rin, Rout, N1, x1_beg, theta_option, th_c, xi, th_n, N2, x2_beg, cctk_iteration, cctk_time = read_header(f)
    
    #the length of this_data
    file_size = 96
    
    #For converting the grid spacing to numerical coordinates read in by Harm3D
    #All of these relationships come directly from Zach's Jupyter Notebook
    delta_x0 = 1/N0*math.log((Rout-R0)/(Rin-R0))
    x0_beg = math.log(Rin - R0)
    x0 = np.zeros((file_size,file_size,file_size))
    r_i = np.zeros((file_size,file_size,file_size))
    
    delta_x1 = 1/N1
    x1 = np.zeros((file_size,file_size,file_size))
    theta_i = np.zeros((file_size,file_size,file_size))
    
    delta_x2 = 2*math.pi/N2
    x2 = np.zeros((file_size,file_size,file_size))
    phi_i = np.zeros((file_size,file_size,file_size))
    
    
    #Loop over the grid and create arrays that contain numerical coords (x0,x1,x2)
    #and physical coords (r_i,theta_i,phi_i)
    for i in range(0,len(this_data)):
        for j in range(0,len(this_data)):
            for k in range(0,len(this_data)):
                x0[i,j,k] = x0_beg + (i+1/2)*delta_x0
                x1[i,j,k] = x1_beg + (j+1/2)*delta_x1
                x2[i,j,k] = x2_beg + (k+1/2)*delta_x2
                
                r_i[i,j,k] = R0 + math.exp(x0[i,j,k])
                theta_i[i,j,k] = th_c + (math.pi - 2*th_c)*x1[i,j,k] + xi*math.sin(2*math.pi*x1[i,j,k])
                phi_i[i,j,k] = x2[i,j,k]
                
                
      
    #Initialize zeroed arrays          
    dx_dxp10, dx_dxp11, dx_dxp12, dx_dxp13, dx_dxp20, dx_dxp21, dx_dxp22, dx_dxp23, dx_dxp30, dx_dxp31, dx_dxp32, dx_dxp33 = (np.zeros((file_size,file_size,file_size)) for i in range(12)) 
    dx_dxp11 = x0
    dx_dxp22 = x1
    dx_dxp33 = x2

    #Write the arrays to the HDF5 file
    
    with h5py.File('rdump_start.h5', 'a') as t:
        tset = t.create_dataset('x1', data = r_i)
        tset = t.create_dataset('x2', data = theta_i)
        tset = t.create_dataset('x3', data = phi_i)
        tset = t.create_dataset('dx_dxp10', data = dx_dxp10)
        tset = t.create_dataset('dx_dxp11', data = dx_dxp11)
        tset = t.create_dataset('dx_dxp12', data = dx_dxp12)
        tset = t.create_dataset('dx_dxp13', data = dx_dxp13)
        tset = t.create_dataset('dx_dxp20', data = dx_dxp20)
        tset = t.create_dataset('dx_dxp21', data = dx_dxp21)
        tset = t.create_dataset('dx_dxp22', data = dx_dxp22)
        tset = t.create_dataset('dx_dxp23', data = dx_dxp23)
        tset = t.create_dataset('dx_dxp30', data = dx_dxp30)
        tset = t.create_dataset('dx_dxp31', data = dx_dxp31)
        tset = t.create_dataset('dx_dxp32', data = dx_dxp32)
        tset = t.create_dataset('dx_dxp33', data = dx_dxp33)
        
        #Read in Metric quantities
        gcov00 = t['gcov00'].value
        gcov01 = t['gcov01'].value
        gcov02 = t['gcov02'].value
        gcov03 = t['gcov03'].value
        gcov11 = t['gcov11'].value
        gcov12 = t['gcov12'].value
        gcov13 = t['gcov13'].value
        gcov22 = t['gcov22'].value
        gcov23 = t['gcov23'].value
        gcov33 = t['gcov33'].value
        
        #Initialize gdet and fill it inside the loop
        gdet = np.zeros((file_size,file_size,file_size))
        for i in range(0,len(this_data)):
            for j in range(0,len(this_data)):
                for k in range(0,len(this_data)):
                    g = np.array([[gcov00[i,j,k], gcov01[i,j,k], gcov02[i,j,k], gcov03[i,j,k]],\
                                  [0            , gcov11[i,j,k], gcov12[i,j,k], gcov13[i,j,k]],\
                                  [0            , 0            , gcov22[i,j,k], gcov23[i,j,k]],\
                                  [0            , 0            , 0            , gcov33[i,j,k]]])
                    gdet[i,j,k] = np.linalg.det(g)
        
        #write gdet to the hdf5 file
        tset = t.create_dataset('gdet', data = gdet)
        
        #Retrieving cartesian velocities that were previously written to the HDF5 file
        v1 = t['v1'].value
        v2 = t['v2'].value
        v3 = t['v3'].value
        
        #Retrieving the cartesian form of the physical coordinates written to the HDF5 file
        #Note the necessary conversion to cartesian coords in order to compute the velocity conversions
        #--scn  x = t['x1'].value * np.sin(t['x2'].value) * np.cos(t['x3'].value)
        #--scn  y = t['x1'].value * np.sin(t['x2'].value) * np.sin(t['x3'].value)
        #--scn  z = t['x1'].value * np.cos(t['x2'].value)
    
        x = r_i * np.sin(theta_i) * np.cos(phi_i)
        y = r_i * np.sin(theta_i) * np.sin(phi_i)
        z = r_i * np.cos(theta_i)


        ### This block is taken from polp_new.py ###
        
        # copied straight from Scott's idl set_metric_gen2.pro    
        gcon00 =  gcov11*gcov22*gcov33 - gcov11*gcov23*gcov23 - gcov12*gcov12*gcov33 + gcov12*gcov13*gcov23 + gcov13*gcov12*gcov23 - gcov13*gcov13*gcov22
        gcon01 = -gcov01*gcov22*gcov33 + gcov01*gcov23*gcov23 + gcov02*gcov12*gcov33 - gcov02*gcov13*gcov23 - gcov03*gcov12*gcov23 + gcov03*gcov13*gcov22
        gcon02 =  gcov01*gcov12*gcov33 - gcov01*gcov23*gcov13 - gcov02*gcov11*gcov33 + gcov02*gcov13*gcov13 + gcov03*gcov11*gcov23 - gcov03*gcov13*gcov12
        gcon03 = -gcov01*gcov12*gcov23 + gcov01*gcov22*gcov13 + gcov02*gcov11*gcov23 - gcov02*gcov12*gcov13 - gcov03*gcov11*gcov22 + gcov03*gcov12*gcov12
        gcon11 =  gcov00*gcov22*gcov33 - gcov00*gcov23*gcov23 - gcov02*gcov02*gcov33 + gcov02*gcov03*gcov23 + gcov03*gcov02*gcov23 - gcov03*gcov03*gcov22
        gcon12 = -gcov00*gcov12*gcov33 + gcov00*gcov23*gcov13 + gcov02*gcov01*gcov33 - gcov02*gcov03*gcov13 - gcov03*gcov01*gcov23 + gcov03*gcov03*gcov12
        gcon13 =  gcov00*gcov12*gcov23 - gcov00*gcov22*gcov13 - gcov02*gcov01*gcov23 + gcov02*gcov02*gcov13 + gcov03*gcov01*gcov22 - gcov03*gcov02*gcov12
        gcon22 =  gcov00*gcov11*gcov33 - gcov00*gcov13*gcov13 - gcov01*gcov01*gcov33 + gcov01*gcov03*gcov13 + gcov03*gcov01*gcov13 - gcov03*gcov03*gcov11
        gcon23 = -gcov00*gcov11*gcov23 + gcov00*gcov12*gcov13 + gcov01*gcov01*gcov23 - gcov01*gcov02*gcov13 - gcov03*gcov01*gcov12 + gcov03*gcov02*gcov11
        gcon33 =  gcov00*gcov11*gcov22 - gcov00*gcov12*gcov12 - gcov01*gcov01*gcov22 + gcov01*gcov02*gcov12 + gcov02*gcov01*gcov12 - gcov02*gcov02*gcov11
    
        det = gcov00*gcon00 + gcov01*gcon01 + gcov02*gcon02 + gcov03*gcon03
        det[det==0] += 1.e-10    # to not prevent further computations in case a
                                 # singular matrix is found at some points. this
                                 # happens for instance when using excision...
        gcon00[gcon00==0] += -1.e-10
    
        inv_det = 1.0 / det
        det = 0 
    
        gcon00 *= inv_det
        gcon01 *= inv_det
        gcon02 *= inv_det
        gcon03 *= inv_det
        gcon11 *= inv_det
        gcon12 *= inv_det
        gcon13 *= inv_det
        gcon22 *= inv_det
        gcon23 *= inv_det
        gcon33 *= inv_det
    
        inv_det = 0 
    
        alpha = np.sqrt(-1./gcon00)
        beta1 =  -gcon01/gcon00 
        beta2 =  -gcon02/gcon00 
        beta3 =  -gcon03/gcon00 
    
    
        # metric = {'gdet':gdet,
        #           'gcov00':gcov00, 'gcov01':gcov01, 'gcov02':gcov02, 'gcov03':gcov03,
        #           'gcov11':gcov11, 'gcov12':gcov12, 'gcov13':gcov13,
        #           'gcov22':gcov22, 'gcov23':gcov23,
        #           'gcov33':gcov33,
        #           'gcon00':gcon00, 'gcon01':gcon01, 'gcon02':gcon02, 'gcon03':gcon03,
        #           'gcon11':gcon11, 'gcon12':gcon12, 'gcon13':gcon13,
        #           'gcon22':gcon22, 'gcon23':gcon23,
        #           'gcon33':gcon33,
        #           'alpha':alpha, 'beta1':beta1, 'beta2':beta2, 'beta3':beta3 }
                  
    
        vsq = gcov11*v1*v1 + gcov22*v2*v2 + gcov33*v3*v3 + 2.*(gcov12*v1*v2 + gcov13*v1*v3 + gcov23*v2*v3)
        
        gamma = np.sqrt(1. + vsq)
        vsq   = 0 
        # alpha = metric['alpha']
        # beta1 = metric['beta1']
        # beta2 = metric['beta2']
        # beta3 = metric['beta3']
    
        ucon0 = gamma/alpha 
        #gamma = 0 
        ucon1 = v1 - ucon0 * beta1
        ucon2 = v2 - ucon0 * beta2
        ucon3 = v3 - ucon0 * beta3
        
        
        ### Block taken from polp_new.py ends here ###
        
        
        ### Write the Jacobian used for switching cartesian velocities to spherical velocities
        aa = x*x  + y*y
        bb = np.sqrt(aa)
        r = np.sqrt(aa + z**2.0)
        jacobian00 = np.zeros(np.shape(aa)); jacobian00.fill(1.)
        jacobian01 = np.zeros(np.shape(aa))
        jacobian02 = np.zeros(np.shape(aa))
        jacobian03 = np.zeros(np.shape(aa))
        jacobian10 = np.zeros(np.shape(aa))
        jacobian11 = x / r * jacobian00   # why have "* jacobian00"  here? 
        jacobian12 = y / r * jacobian00
        jacobian13 = z / r * jacobian00
        jacobian20 = np.zeros(np.shape(aa))
        jacobian21 = x*z/(r**2.0*bb) * jacobian00
        jacobian22 = y*z/(r**2.0*bb) * jacobian00
        jacobian23 = -bb/r**2.0 * jacobian00
        jacobian30 = np.zeros(np.shape(aa))
        jacobian31 = -y/aa * jacobian00
        jacobian32 = x/aa * jacobian00
        jacobian33 = np.zeros(np.shape(aa))

        
        ### Compute the spherical velocities by performing the transformation ###
        u_time = jacobian00*ucon0 + jacobian01*ucon1 + jacobian02*ucon2 + jacobian03*ucon3
        u_r = jacobian10*ucon0 + jacobian11*ucon1 + jacobian12*ucon2 + jacobian13*ucon3
        u_phi = jacobian30*ucon0 + jacobian31*ucon1 + jacobian32*ucon2 + jacobian33*ucon3
        u_theta = jacobian20*ucon0 + jacobian21*ucon1 + jacobian22*ucon2 + jacobian23*ucon3
        
        ### We now have to convert these 4-velocities to primitive velocities:
        ###  First read or set beta_[r,theta,phi]:

        if 1 : 
            # beta_r     =
            # beta_theta =
            # beta_phi   =
        
            v_r     =  u_r     + ucon0 * beta_r
            v_theta =  u_theta + ucon0 * beta_theta
            v_phi   =  u_phi   + ucon0 * beta_phi

        else :
            ### Or first translate to numerical coordinates and use the shift in numerical coordinates:
            # beta_1 =
            # beta_2 =
            # beta_3 =

            ### Either calculate dxp_dxNM from inverting dx_dxpNM from above, or use analytic expressions:
            ### For the current coordinate system, I think the only non-zero elemets are the diagonal ones:
            u_1 =  u_r * dxp_dx11 + u_theta * dxp_dx12 + u_phi * dxp_dx13
            u_2 =  u_r * dxp_dx21 + u_theta * dxp_dx22 + u_phi * dxp_dx33
            u_3 =  u_r * dxp_dx31 + u_theta * dxp_dx32 + u_phi * dxp_dx33
            v_1 =  u_1 + ucon0 * beta_1
            v_2 =  u_2 + ucon0 * beta_2
            v_3 =  u_3 + ucon0 * beta_3
        
        
        ### Once these are checked, there will be a write command inserted here to store the velocities###
        
