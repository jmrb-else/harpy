"""  

  mdot.py: 
  ------------
    -- plot accretion rate data from reduced data set:

M0                       Dataset {SCALAR}  = 
Mdot                     Dataset {182}
mass_lost                Dataset {182}
time                     Dataset {182}


"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################

def plot_mdot() :

    filein = 'mass_flux.h5'

    h5file = h5py.File(filein,'r')
    tout = h5file['time'].value
    mdot = h5file['Mdot'].value
    mass_acc = h5file['mass_lost'].value
    mall = h5file['M0'].value

    mdot /= mall
    mass_acc /= mall

    fontsize=10
    dirout = './'
    
    nwin=0
    fignames = [(dirout + 'mdot')]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [M]',fontsize=fontsize)
    ax.set_ylabel(r'$\dot{M}_{\rm cutout}$ / M(t=0)',fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    ax.plot(tout,mdot)


    mdot_num = np.zeros(len(tout)) + 0.03
    nwin+=1
    fignames = np.append(fignames,(dirout + 'mdot_raw'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [M]',fontsize=fontsize)
    ax.set_ylabel(r'$\dot{M}_{\rm cutout}$',fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    ax.plot(tout,mdot*mall)
    ax.plot(tout,mdot_num,'b.')


    nwin+=1
    fignames = np.append(fignames,(dirout + 'mdot_by_mdotnum'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [M]',fontsize=fontsize)
    ax.set_ylabel(r'$\dot{M}_{\rm cutout}/\dot{M}_{\rm num}$',fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    ax.plot(tout,mdot*mall/mdot_num)


    nwin+=1
    fignames = np.append(fignames,(dirout + 'mass_acc'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [M]',fontsize=fontsize)
    ax.set_ylabel(r'$M(<t) / M(t=0)$]',fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    ax.plot(tout,mass_acc)

    h5file.close()

    for i in np.arange(len(figs)) : 
        figs[i].savefig('%s.pdf' %fignames[i], dpi=300,bbox_inches='tight')
    
    print("All done ")
    sys.stdout.flush()
    
    return

    
###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python lum
# 
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)

    if( 1 ) :
        plot_mdot()
        
    
#     if len(sys.argv) > 1 :
#         make_series(sys.argv[1])
#     else:
#         lum()

    
