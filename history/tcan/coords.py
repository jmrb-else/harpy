###############################################################################
###############################################################################
#  Functions useful to generate different coordinate systems;
###############################################################################
###############################################################################

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re 

from IPython.core.debugger import Tracer

SMALL = 1e-14

###############################################################################
###############################################################################
def PERIODIC_SHIFT(x,x0,xn):

    xout = np.where( (x < x0), ( 1.+( (np.abs((x)-(x0))/((xn)-(x0))).astype(int) ) ), (-(  (np.abs((x)-(x0))/((xn)-(x0))).astype(int) )) )

    return xout 

###############################################################################
###############################################################################
def PERIODIC(x,x0,xn):

    return ( (x) + ((xn)-(x0))*(PERIODIC_SHIFT((x),(x0),(xn))) )


###############################################################################
###############################################################################
def seesaw(x, x0, xn): 

  xn2 = 2*xn - x0
  x1 = PERIODIC(x,x0,xn2)

  xout = np.where( (x1 < xn),  (x1-x0), (xn2-x1) )

  return ( xout ) 



###############################################################################
###############################################################################
#  ===========================================
#  set of functions for COORD_WARPED_SPHERICAL
#  ===========================================
# 
###############################################################################
###############################################################################
def sech2(x):
    y = np.cosh(x)
    return (1./( y*y ))

###############################################################################
# 
#  -- stepfunc 
#     --------
#     \sigma in LaTeX doc
#
###############################################################################
def stepfunc(x, x1, hh):
    return np.tanh(hh*(x - x1)) 


###############################################################################
# 
#  -- transfunc
#     --------
#     -- transition function between two values
#
###############################################################################
def transfunc(x, x1, hh, f1, f2):
    return (f1*np.ones(len(x)) + (f2-f1)*0.5*( stepfunc(x,x1,hh) + 1. ) )


###############################################################################
#  -- square
#     ------
#     \tau in latex doc.
#     Square function  with baseline = 0 , amplitude = 1
#     also called "tophat" in latex document, "square" in latex document is something else;
#
###############################################################################
def square( x,  x1,  h,  delta):
    return (0.5*(stepfunc(x,x1-delta,h) - stepfunc(x,x1+delta,h)) )


###############################################################################
# 
#  -- square_per
#     ----------
#     Periodic square function over  x \in [0,1]
#
###############################################################################
def square_per( x,  x1,  h,  delta) :
    return (square(x,x1   ,h,delta) +
            square(x,x1-1.,h,delta) +
            square(x,x1+1.,h,delta) )


###############################################################################
# 
#  -- stepfunc_int
#     ------------
#     \Sigma in LaTeX doc 
#
###############################################################################
def stepfunc_int( x,  x1,  h) :
    # //  y = x-x1 ;
    # // return ( log(cosh(h*(y-floor(y)-0.5)))/h ) ;
    # // return ( log(cosh(h*(y)))/h  - log(cosh(h*(-x1)))/h ) ;
    return ( np.log(np.cosh(h*(x-x1)))/h )

###############################################################################
# 
#  -- square_int
#     ----------
#     \Tau in LaTeX doc
#
###############################################################################
def square_int( x,  x1,  h,  delta) :
    return ( 0.5*(stepfunc_int(x,x1-delta,h) -
                  stepfunc_int(x,x1+delta,h)) )


###############################################################################
# 
#  -- square_int_per
#     --------------
#     \tilde \Tau in LaTeX doc
#
###############################################################################
def square_int_per( x,  x1,  h,  delta) :
    return ( square_int(x,x1   ,h,delta) +
             square_int(x,x1-1.,h,delta) +
             square_int(x,x1+1.,h,delta) )


###############################################################################
# 
#  -- dsquare
#     -----------
#     \tau' in latex doc
#
###############################################################################
def dsquare( x,  x1,  h,  delta) :
    return ( 0.5*h*( sech2(h*(x-x1+delta)) - sech2(h*(x-x1-delta)) ) )


def dsquare_per( x,  x1,  h,  delta) :
    return ( dsquare(x,x1   ,h,delta) +
             dsquare(x,x1-1.,h,delta) +
             dsquare(x,x1+1.,h,delta) ) 


###############################################################################
# 
#  -- d2square_per
#     -----------
#     \tilde \tau''
# 
###############################################################################
def d2square( x,  x1,  h,  delta) :
    return (h*h * ( sech2(h*(x - x1 - delta)) * np.tanh(h*(x - x1 - delta)) -
                    sech2(h*(x - x1 + delta)) * np.tanh(h*(x - x1 + delta))))


def d2square_per( x,  x1,  h,  delta) :
    return ( d2square(x,x1   ,h,delta) +
             d2square(x,x1-1.,h,delta) +
             d2square(x,x1+1.,h,delta) )


###############################################################################
# 
#  -- th_of_x2
#     -----------
#       pi * ( z  - a_z * ( Tau_{z1}(z) - Tau_{z1}(z1)  - (z-z1) * ( Tau_{z1}(1) - Tau_{z1}(0)  ) ) )
# 
#  Tau_{z1}(z)  = square_int_per( z,  z1,  h,  delta) 
###############################################################################
def th_of_x2( z, z1,  a_z, h,  delta) :

    return( np.pi * ( z - a_z * ( square_int_per(z ,z1,h,delta) -
                                  square_int_per(z1,z1,h,delta) - (z-z1) * (
                                  square_int_per(1.,z1,h,delta) -
                                  square_int_per(0 ,z1,h,delta) ))))


###############################################################################
# 
#  -- th_of_x2_two_regions
#     -----------
#       pi * ( z  - a_z * ( Tau_{z1}(z) - Tau_{z1}(z1)  - (z-z1) * ( Tau_{z1}(1) - Tau_{z1}(0)  ) ) )
# 
#  Tau_{z1}(z)  = square_int_per( z,  z1,  h,  delta) 
###############################################################################
def th_of_x2_two_regions( z, z1,  a_z_1, h_1,  delta_1, z2,  a_z_2, h_2,  delta_2) :

    return( np.pi * ( z
                      - a_z_1 * ( square_int_per(z ,z1,h_1,delta_1) -
                                  square_int_per(z1,z1,h_1,delta_1) - (z-z1) * (
                                  square_int_per(1.,z1,h_1,delta_1) -
                                  square_int_per(0 ,z1,h_1,delta_1) ))
                      - a_z_2 * ( square_int_per(z ,z2,h_2,delta_2) -
                                  square_int_per(z2,z2,h_2,delta_2) - (z-z2) * (
                                  square_int_per(1.,z2,h_2,delta_2) -
                                  square_int_per(0 ,z2,h_2,delta_2) ))))


###############################################################################
# 
#  -- find_az_from_dth
#     -----------
###############################################################################
def find_az_from_dth( N2, dth, delta_z ):

    return ( ( 1. - N2*(dth / np.pi) ) / ( 1. - 2.*delta_z ) )



###############################################################################
# 
#  -- find_az_from_dth_2
#     -----------
###############################################################################
def find_az_from_dth_2( N2, dth, delta_z ):

    return ( (  N2*(dth / np.pi) - 1. ) / ( 2.*delta_z ) )



###############################################################################
# 
#  -- find_delta_from_dths
#     -----------
#
#      -- returns with the value of delta to ensure that the maximum
#          dth reached is dth_max;
#
###############################################################################
def find_delta_from_dths( N2, dth_min, dth_max) :

    return ( 0.5*(dth_max - np.pi/N2) / (dth_max - dth_min) )


###############################################################################
# 
#  -- find_delta_from_dthmin_dph
#     -----------
#
#      -- returns with the value of delta such that the max. dth is such that
#         0.5*sin(th)*dphi  =  dth_min 
#        or that the minimum cell extents are the same in theta and phi;
#
###############################################################################
def find_delta_from_dthmin_dph( N2, dth_min, dph) :

    return ( 0.5*(1. - dph*(0.5*np.pi/(N2*dth_min))) / (1. - dph) )


###############################################################################
# 
#  -- find_xp1_0_of_hyperexp()
#     -----------
#
#      -- finds the coordinate xp1=xp1_0 where the grid transitions
#         from exponential to superexponential
#
#      -- solves
#              2*a*x**n - x**(n-1)  - 1  = 0     for x
#            where
#               a = log( (rmax - R0)/(rmin - R0) ) / log(r0 - R0)
#            where
#               Rmin, Rmax = min. and max. radii of grid;
#               r0         = radial location of transition
#               R0         = constant offset in radial grid, often is zero;
#  
#       -- "a" is typically ~ O(3)
#       -- note that we restrict  x in [0,1]
# 
###############################################################################
def find_xp1_0_of_hyperexp(r0, rmin, rmax, R0, n_exp1):

    coeff = np.zeros(n_exp1+1)
    a_1 = np.log( (rmax - R0)/(rmin - R0) ) / np.log(r0 - R0)
    
    coeff[0] = 2.*a_1
    coeff[1] = -1.
    coeff[-1] = -1.
    
    roots = np.roots(coeff)
    print("roots = ", roots)
    print("real roots = ", roots[np.isreal(roots)])
    sols = [ x for x in roots if np.isreal(x) and x > 0. and x < 1. ]

    final_sol = np.real(np.max(sols))
    
    return ( final_sol )


###############################################################################
# 
#  -- find_xp1_0_of_hyperexp_new()
#     -----------
#
#      -- for the new hyperexp grid, you can freely choose xp1_0,
#         which has the effect for larger values to increase the
#         gradient of the exponential gradient part so that it matches
#         the hyperexponential part at larger xp1.  Typically,
#         however, we want xp1_0 to be such that grid look like a
#         normal exponential grid at smaller xp1, and then transition
#         to a hyper-exp grid further out.  Or, in other words, the
#         linear part of the exponential should match the high-order
#         polynomial part when xp1=xp1_0.  Using this additional
#         constraint, we arrive at the following high-order polynomial
#         algebraic equation to solve for xp1_0, similar to the
#         old/errant hyper-exp grid we developed earlier.
#
#      -- this routine solves
#              2*a*x**n - x**(n-1)  - 1  = 0     for x
#            where
#                           log( (rmax - R0)/(rmin - R0) )
#               a = Bc/Ac = -------------------------------
#                           log( (r0 - R0)  /(rmin - R0) )
#            where
#               Rmin, Rmax = min. and max. radii of grid;
#               r0         = radial location of transition
#               R0         = constant offset in radial grid, often is zero;
#  
#       -- "a" is typically ~ O(3)
#       -- note that we restrict  x in [0,1]
# 
###############################################################################
def find_xp1_0_of_hyperexp_new(r0, rmin, rmax, R0, n_exp1):

    coeff = np.zeros(n_exp1+1)
    a_1 = np.log( (rmax - R0)/(rmin - R0) ) / np.log((r0 - R0)/(rmin - R0))
    
    coeff[0] = 2.*a_1
    coeff[1] = -1.
    coeff[-1] = -1.
    
    roots = np.roots(coeff)
    print("roots = ", roots)
    print("real roots = ", roots[np.isreal(roots)])
    sols = [ x for x in roots if np.isreal(x) and x > 0. and x < 1. ]

    final_sol = np.real(np.max(sols))
    
    return ( final_sol )


###############################################################################
# 
#  -- r_of_xp1_hyperexp()
#     -----------
#
#      -- hyper- and regular exponential radial grid 
#
#      -- r0 is approximately the radius at which the grid "goes
#         hyperexponential" which we contrive by judicious choice of
#         the coefficient on the xp1**n term;
#
#      -- this form of the equation arose from using the general form:
#          r(x) = R0 + R1*exp[ c1*( x + c2*x**n ) ]
#
#         and finding R1, c1, c2 for rmin, rmax, r0, where r0 is defined as
#          r0 = r(x0)  where  the two terms in the exponent are equal:
#             x0 - c2*x0**n = 0 
#
#      -- note that
#                    (d/dx) a^x =  a^x log(a)     for any constant "a"
#
###############################################################################
def r_of_xp1_hyperexp(xp1, r0, rmin, rmax, R0, n_exp1):

    if( n_exp1 <= 1 ) :
        sys.exit("n must be greater than 1 : n = ", n_exp1)
    
    xp1_0 = find_xp1_0_of_hyperexp(r0, rmin, rmax, R0, n_exp1)
    print("xp1_0 = ", xp1_0)
    c1 = 0.5*np.log(r0 - R0) / xp1_0

    xtmp = xp1 / xp1_0
    rtmp = ( R0 + (rmin-R0) * (r0-R0)**(0.5*xtmp*(1.+xtmp**(n_exp1-1))) )

    ftmp = np.log(r0-R0) * 0.5 * ( 1. + n_exp1*xtmp**(n_exp1-1) ) / xp1_0
    dr = (rtmp - R0) * ftmp
    dr2 = (rtmp - R0) * ftmp*ftmp   +  (rtmp - R0) * np.log(r0-R0) * 0.5 * n_exp1*(n_exp1-1)*xtmp**(n_exp1-2) / (xp1_0*xp1_0) 
    
    return xp1_0, rtmp, dr, dr2


###############################################################################
# 
#  -- r_of_xp1_hyperexp_new()
#     -----------
#
#      -- hyper- and regular exponential radial grid 
#
#      -- r0 is approximately the radius at which the grid "goes
#         hyperexponential" which we contrive by judicious choice of
#         the coefficient on the xp1**n term;
#
#      -- this form of the equation arose from using the general form:
#          r(x) = R0 + R1*exp[ c1*xtmp + c2*xtmp**n ) ]
#
#         where xtmp = x/x_0 
#         and finding R1, c1, c2 for rmin, rmax, r0, where r0 is defined as
#          r0 = r(x0) . 
#
###############################################################################
def r_of_xp1_hyperexp_new(xp1, r0, rmin, rmax, R0, n_exp1, xp1_0=None):

    if( n_exp1 <= 2 ) :
        sys.exit("n must be greater than 2 : n = ", n_exp1)

    Ac = np.log( (r0  -R0)/(rmin-R0) )
    Bc = np.log( (rmax-R0)/(rmin-R0) )

    x0_min = Ac/Bc
    x0_max = x0_min**(1./n_exp1)
    if( xp1_0 is None ) :
        xp1_0 = find_xp1_0_of_hyperexp_new(r0, rmin, rmax, R0, n_exp1)
        
    print("xp1_0  min max = ", xp1_0, x0_min, x0_max)
    if( (xp1_0 <= x0_min) or (xp1_0 >= x0_max) ):
        sys.exit("xp1_0 is out of bounds!!")
        
    x0_nm1 = xp1_0**(n_exp1-1)
    denom = 1. - x0_nm1
    c1 = (Ac - Bc*x0_nm1*xp1_0) / denom
    c2 = x0_nm1*( Bc*xp1_0 - Ac ) / denom

    print("Ac = ", Ac)
    print("Bc = ", Bc)
    print("c1 = ", c1)
    print("c2 = ", c2)

    xtmp = xp1 / xp1_0
    x_nm2 = xtmp**(n_exp1-2)
    x_nm1 = x_nm2*xtmp
    x_n   = x_nm1*xtmp
    rtmp = R0 + (rmin - R0)*np.exp( c1*xtmp + c2*x_n )
    ftmp = n_exp1*c2*x_nm2
    gtmp = ( c1 + xtmp * ftmp ) / xp1_0
    
    
    dr = (rtmp - R0) * gtmp
    dr2 = dr*gtmp  +  (rtmp - R0)*(n_exp1-1) * ftmp / (xp1_0*xp1_0)
    
    return rtmp, dr, dr2


###############################################################################
###############################################################################

def make_xp_array( Ncells, pos='CENTER', NG=3, GridLength=1., include_ghost_cells=True ):

    dxp2 = GridLength/Ncells
    
    if( include_ghost_cells ) :
        n2tot = Ncells + 2*NG
        xp2_beg = 0. - NG*dxp2
        xp2_end = 1. + NG*dxp2
    else :
        n2tot = Ncells
        xp2_beg = 0.
        xp2_end = 1.

    if( pos == 'FACE' or pos == 'CORNER' ) :
        xp2 = np.linspace( xp2_beg, xp2_end, n2tot+1 )
    elif( pos == 'CENTER' ) :
        xp2 = np.linspace( (xp2_beg+0.5*dxp2), (xp2_end - 0.5*dxp2), n2tot )

    return (xp2) 
    
