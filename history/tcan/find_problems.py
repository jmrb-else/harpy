
from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import h5py
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
from scipy.interpolate import interp1d
from coord_transforms import *
from harm import *
from matplotlib import ticker
from polp_lite import polp


######################################

######################################
##
## find_problems:
##
######################################
######################################

"""
  Usage:

    find_problems <filename>  [rel_divb_thresh]


  where
   <filename>        :   the name of the hdf5 file
   [rel_divb_thresh] : threshold of divb*dx/bsq above which cells are identified as problematic;
"""
def find_problems(filename,
                  rel_divb_thresh=1.e-12,
                  print_indices=False,
                  avoid_subdomain_boundaries=False) :


    h5file = h5py.File(filename,'r')
    pflag=h5file['pflag'][:]
    value_index = np.where(pflag != 0)
    n_pflag = len(value_index[0])

    print(" n_pflag = ", n_pflag)
    if( print_indices and (n_pflag > 0) ) :
        print("pflag bad indices = ", value_index)
        print("pflag bad values   = ", pflag[value_index])
        iloc, icounts = np.unique(value_index[0],return_counts=True)
        jloc, jcounts = np.unique(value_index[1],return_counts=True)
        kloc, kcounts = np.unique(value_index[2],return_counts=True)
        print(" i uniq  = ", iloc)
        print(" i count = ", icounts)
        print(" j uniq  = ", jloc)
        print(" j count = ", jcounts)
        print(" k uniq  = ", kloc)
        print(" k count = ", kcounts)


    pflag = 0

    dx1 = h5file['/Header/Grid/dx1'].value[0]
    dx2 = h5file['/Header/Grid/dx2'].value[0]
    dx3 = h5file['/Header/Grid/dx3'].value[0]
    dx = (dx1*dx2*dx3)**(1./3.)

    bsq = h5file['bsq'][:]
    divb = h5file['divb'][:]
    reldivb = np.multiply(np.divide(divb,bsq),dx)
    reldivb[0,:,:] = 0.
    reldivb[:,0,:] = 0.
    reldivb[:,:,0] = 0.


    if( avoid_subdomain_boundaries ) :
        n1 = h5file['/Header/Grid/N1'].value[0]
        n2 = h5file['/Header/Grid/N2'].value[0]
        n3 = h5file['/Header/Grid/N3'].value[0]
        reldivb[::n1,:,:] = 0.
        reldivb[:,::n2,:] = 0.
        reldivb[:,:,::n3] = 0.


    reldivb[~np.isfinite(reldivb)] = 0.
    loc_divb = np.where(np.abs(reldivb) > rel_divb_thresh)
    n_divb = len(loc_divb[0])

    print(" n_divb = ", n_divb)
    if( print_indices and (n_divb > 0) ) :
        print("divb     bad indices = ", loc_divb)
        print("bsq      bad values  = ", bsq[loc_divb])
        print("divb     bad values  = ", divb[loc_divb])
        print("rel_divb bad values  = ", reldivb[loc_divb])
        iloc, icounts = np.unique(loc_divb[0],return_counts=True)
        jloc, jcounts = np.unique(loc_divb[1],return_counts=True)
        kloc, kcounts = np.unique(loc_divb[2],return_counts=True)
        print(" i uniq  = ", iloc)
        print(" i count = ", icounts)
        print(" j uniq  = ", jloc)
        print(" j count = ", jcounts)
        print(" k uniq  = ", kloc)
        print(" k count = ", kcounts)

    bsq = 0
    divb = 0

    return


###############################################################################
# This allows the python module to be executed like:
#
#   prompt>  python find_problems.py  KDHARM0.000000
#
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) > 1 :
        if( len(sys.argv) == 2 ) :
            find_problems(sys.argv[1])
        if( len(sys.argv) == 3 ) :
            find_problems(sys.argv[1],rel_divb_thresh=np.float64(sys.argv[2]))
        if( len(sys.argv) == 4 ) :
            print_indices = False
            avoid_subdomain_boundaries = False
            if( int(sys.argv[3]) != 0 ) :
                print_indices=True
            find_problems(sys.argv[1],rel_divb_thresh=np.float64(sys.argv[2]),print_indices=print_indices)
        if( len(sys.argv) == 5 ) :
            print_indices = False
            if( int(sys.argv[3]) != 0 ) :
                print_indices=True
            avoid_subdomain_boundaries = False
            if( int(sys.argv[4]) != 0 ) :
                avoid_subdomain_boundaries=True
            find_problems(sys.argv[1],rel_divb_thresh=np.float64(sys.argv[2]),
                          print_indices=print_indices,
                          avoid_subdomain_boundaries=avoid_subdomain_boundaries)
    else:
        print("Usage:  python find_problems.py  <filename>  [rel_divb_thresh] [print_indices] [avoid_subdomain_boundaries] ")
        print(" ")
        print(" ")

        print("   where")
        print("    <filename>        :   the name of the hdf5 file")
        print("    [rel_divb_thresh] : threshold of divb*dx/bsq above which cells are identified as problematic;")
        print("    [print_indices]   : if true, prints out bad indices ;")
        print("    [avoid_subdomain_boundaries] : ignore cells along the subdomain boundaries in case you are writing data at intermediate steps;")
        print(" ")
        print(" ")
        sys.exit("Missing arguments...exiting")
