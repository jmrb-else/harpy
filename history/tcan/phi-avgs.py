
from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import h5py
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib.colors import Colormap
from scipy.interpolate import interp1d 
from coord_transforms import *
from harm import *
from polp_lite import *

######################################
# make time-avg
######################################
def phi_avgs_func(funcname,h5file,av_h5file):

    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    func = read_3d_hdf5_slice(funcname,slice_obj_tmp,h5file)
    n3 = func.shape[2]
    func_av = np.sum(func,axis=2)
    func_av /= n3
    dset = av_h5file.create_dataset(funcname+'_phavg', data=func_av)
    func = 0

    return func_av
    

######################################
# make A_phi 
######################################
def make_aphi(bx,by,h5file,av_h5file,gdump_h5file):
 
    dx1 = h5file['Header/Grid/dx1'].value[0] 
    dx2 = h5file['Header/Grid/dx1'].value[0] 

    half_dx1 = 0.5*dx1
    half_dx2 = 0.5*dx2
    
    gdet = gdump_h5file['gdet3'].value[:,:,0]
    gterm = gdet * np.sqrt(4.*np.pi)

    bx *= gterm
    by *= gterm
        
    psi = 0.*bx

    idim = bx.shape[0]
    kdim = bx.shape[1]
    kmid = int(kdim/2)

    print("idim = ", idim)
    print("kdim = ", kdim)

    j = 0
    for i in np.arange(idim-1) :
        psi[i+1,j] = psi[i,j] - half_dx1 * (by[i,j] + by[i+1,j])

    i = 0
    for j in np.arange(kdim-1):
        psi[i,j+1] = psi[i,j] + half_dx2 * (bx[i,j+1] + bx[i,j+1])

    j = kdim-1
    for i in np.arange(idim-1) :
        psi[i+1,j] = psi[i,j] - half_dx1 * (by[i,j] + by[i+1,j])

    i = idim-1
    for j in np.arange(kdim-1):
        psi[i,j+1] = psi[i,j] + half_dx2 * (bx[i,j+1] + bx[i,j+1])

#    for i in np.arange(1,idim-2):
#        for j in np.arange(kmid):
#            psi[i,j+1] = psi[i,j] + half_dx2 * (bx[i,j+1] + bx[i,j+1])
#
#        for j in np.arange(kdim-1,kmid,-1): 
#            psi[i,j-1] = psi[i,j] - half_dx2 * (bx[i,j-1] + bx[i,j])

    for i in np.arange(1,idim-2):
        for j in np.arange(kdim-1):
            psi[i,j+1] = psi[i,j] + half_dx2 * (bx[i,j+1] + bx[i,j+1])

            
    return psi

 
######################################
# make time-avg
######################################
def make_phi_avgs():

    file = 'KDHARM0.000800.h5'
    av_file = 'KDHARM0.000800.avg.h5'
    gdump_file = 'KDHARM0.gdump.h5'
    h5file   = h5py.File(file, 'r')
    av_h5file  = h5py.File(av_file, 'w')
    gdump_h5file  = h5py.File(gdump_file, 'r')

    phi_avgs_func('gamma',h5file,av_h5file)
    phi_avgs_func('rho',  h5file,av_h5file)
    phi_avgs_func('bsq',  h5file,av_h5file)
    bx = phi_avgs_func('B1',   h5file,av_h5file)
    by = phi_avgs_func('B2',   h5file,av_h5file)
    phi_avgs_func('uu',   h5file,av_h5file)
    
    aphi = make_aphi(bx,by,h5file,av_h5file,gdump_h5file)
    dset = av_h5file.create_dataset('Aphi', data=aphi)

    h5file.close()
    av_h5file.close()
    gdump_h5file.close()

    return
    

#######################################

if 1 :
    make_phi_avgs()

    
rhomin=-10.
rhomax=4.5

uumin = -9
uumax = -3

bsq_min = -9
bsq_max = -3

gammamin = 1.
gammamax = 2.

bor_min = -4.
bor_max = 4. 

i = 800
suf='-rmax2'
dirout='./plots'

nt = 6

if 0 : 
    rmax = 60.
    polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,usegdump=True)

    polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p2',xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True)

    polp(func0=gamma_av,itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-rmax,zmax=rmax,usegdump=True,minf=1.,maxf=3.,plottitle='gamma')

    polp(func0=bsq_o_rho_av,itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix=suf,xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,plottitle='bsq_o_rho',minf=-4,maxf=5.5)



if 1 :
    av_file = 'KDHARM0.000800.avg.h5'
    av_h5file  = h5py.File(av_file, 'r')

    bsq   = av_h5file['bsq_phavg'  ].value
    uu    = av_h5file['uu_phavg'   ].value
    rho   = av_h5file['rho_phavg'  ].value
    gamma = av_h5file['gamma_phavg'].value
    aphi  = av_h5file['Aphi'       ].value
    bsq_o_rho = bsq/rho

    av_h5file.close()

    nlines = 20
    
    rmax = 60.

#    polp(func0=rho,itime=i,dirname="./",show_plot=False,funcname='rho-av',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,usegdump=True,dirout=dirout)

#    polp(func0=rho,itime=i,dirname="./",show_plot=False,funcname='rho-av',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p2',xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout)

#    polp(func0=uu,itime=i,dirname="./",show_plot=False,funcname='uu-av',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,usegdump=True,dirout=dirout)

#    polp(func0=uu,itime=i,dirname="./",show_plot=False,funcname='uu-av',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p2',xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout)

#    polp(func0=bsq,itime=i,dirname="./",show_plot=False,funcname='bsq-av',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p2',xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout)

#    polp(func0=gamma,itime=i,dirname="./",show_plot=False,funcname='gamma-av',dolog=False,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p2',xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout,minf=1.,maxf=3.)

#    polp(func0=gamma,itime=i,dirname="./",show_plot=False,funcname='gamma-av',dolog=False,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-rmax,zmax=rmax,usegdump=True,minf=1.,maxf=3.,plottitle='gamma',dirout=dirout)

#    polp(func0=bsq_o_rho,itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho-av',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p2',xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout,minf=-4.,maxf=5.5)
    
#    polp(func0=bsq_o_rho,itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho-av',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix=suf,xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,plottitle='bsq_o_rho',minf=-4,maxf=5.5,dirout=dirout)

#     polp(func0=aphi,itime=i,dirname="./",show_plot=False,funcname='aphi',dolog=False,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-rmax,zmax=rmax,usegdump=True,plottitle='aphi',dirout=dirout)
# 
#     polp(func0=rho,itime=i,dirname="./",show_plot=False,funcname='rho-field',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p2',xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout,fieldlines=[nlines,aphi])
# 
#     polp(func0=rho,itime=i,dirname="./",show_plot=False,funcname='rho-field',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p1',xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,usegdump=True,dirout=dirout,fieldlines=[nlines,aphi])

 

    vsq = -1./(gamma*gamma) + 1. 
    vgamma = np.sqrt(vsq)*gamma


#     polp(func0=vgamma,itime=i,dirname="./",show_plot=False,funcname='vgamma-av',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p2',xmin=1e-6,xmax=40.,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout,minf=-1.,maxf=1.)
#     
#     polp(func0=vgamma,itime=i,dirname="./",show_plot=False,funcname='vgamma-field',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-p1',xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,usegdump=True,dirout=dirout,fieldlines=[nlines,aphi],minf=-1.,maxf=1.)

    polp(func0=rho,itime=i,dirname="./",show_plot=False,funcname='rho-field',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-rho-final',xmin=1e-6,xmax=50,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout,plottitle='notitle',fillaxis=True,fieldlines=[nlines,aphi],axis='off',plot_horizon=True)
    #fieldlines=[nlines,aphi],

    polp(func0=vgamma,itime=i,dirname="./",show_plot=False,funcname='vgamma-field',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,namesuffix='-vgamma-final',xmin=-50.,xmax=-1e-6,zmin=-rmax,zmax=rmax,usegdump=True,dirout=dirout,minf=-1.,maxf=1.,flip_horizontal=True,plottitle='notitle',fillaxis=True,fieldlines=[nlines,aphi],axis='off',plot_horizon=True)
    

    # make them side by side, mirror image
    # keep color scale
    # remove axes
    
    


