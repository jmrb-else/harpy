"""

  merge-bothros-runs2.py: 
  ------------

    -- merges a set of bothros files, each at a single time  but each file has all frequences stored inside; 


"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

import string as str

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################
#
#  Data formats of the source data files:
#
# funcs_to_copy: 
#    Header                   Group
#  DBL  pix_area                 Dataset {1, 800, 800}
#  DBL  pix_x                    Dataset {1, 800, 800}
#  DBL  pix_y                    Dataset {1, 800, 800}
#  INT  id                       Dataset {1, 800, 800}
#  DBL   units                    Dataset {100, 13}
# INT   end_state                Dataset {1, 800, 800}
# DBL   freq                     Dataset {100}
#
# other_funcs:
# DBL   times                    Dataset {100}
# DBL   I_image                  Dataset {100, 800, 800}
# DBL   I_image2                 Dataset {100, 800, 800}
#
#
#
##################################################################################################



# There is probably a nice way of doing this automatically using glob() and string parsing: 
ntimes = 182
#ntimes = 4

ntot = ntimes

files = sorted(glob.glob("light_curve_*.h5"))
nfiles = len(files)
print("files=", files)

if( nfiles != ntot ) :
    sys.exit("counts are off: ", ntimes, ntot, nfiles)

outfile="all_frames_lg.h5"
    
funcs_to_copy = ['Header','pix_area', 'pix_x', 'pix_y', 'id','freq', 'end_state']
n_funcs_to_copy = len(funcs_to_copy)

other_funcs = ['times', 'I_image', 'I_image2']
n_other_funcs = len(other_funcs)


#######################################################################
# Copy over those datasets that are independent of time and frequency:
#######################################################################
h5in  = h5py.File(files[0],'r')
h5out  = h5py.File(outfile,'w-')


for func in funcs_to_copy :
    print("doing func = ", func)
    print(" func = ", func )
    h5in.copy(func,h5out)

#######################################################################
# Create the datasets in the output file, automatically figuring out
# their dimensions and types from the source data sets:
#######################################################################
shape0 = (ntimes,)
for func in other_funcs :
    print("doing func = ", func)
    shape1 = h5in[func].shape
    ndim1 = len(shape1)
    if( ndim1 == 1 ) :  # there should only be one function meeting this criterion:
        shape2 = shape0
    else :
        shape2 = np.append(shape0,shape1)

    dset = h5out.create_dataset(func,shape2,dtype=h5in[func].dtype)

h5in.close()


#######################################################################
# Now read in each source data set and save it in the correct slot of
# the destination/merged data set.
#
#  -- assume for now that every input hdf5 file will only have one
#     entry;
#
#  -- each input file will have the filename format :
#        light_curve_${itime}_${ifreq}.h5
#
#######################################################################
for file in files :
    print("Processing  file = ", file)
    file_parts = file.split("_")
    time_str = file_parts[2]
    time_strings = file_parts[2].split('.')
    time_str = time_strings[0]
    itime = int(time_str)
    print(" itime ifreq = ", itime)

    h5in  = h5py.File(file,'r')

    for func in other_funcs :
        print("....Merging func = ", func)
        ndim1 = len(h5in[func].shape)
        print("shape in  = ", h5in[func].shape)
        print("shape out = ", h5out[func].shape)
        # There has to be a more elegant solution than this: 
        if( ndim1 == 1 ) :
            h5out[func][itime] = h5in[func][0]
        else : 
            h5out[func][itime,:] = h5in[func][:]
            
    h5in.close()
            

h5out.close()

print("Done with all files !")
