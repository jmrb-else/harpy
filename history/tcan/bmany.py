"""  

  bmany.py: 
  ------------
    -- make a row of 2-d color contours, each with a different color bar. 

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, NullFormatter


import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################

def make_multi_image(dirout='./', second_orbit=True, extend='both' ):
    """  
    """

    itimes = [15,24,30,72]
    ntimes = len(itimes)

    showplots=0
    savepng=1
    nwin=0
    
    cmap  = plt.cm.Spectral_r
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 
    
    if not showplots :
        plt.ioff()

    if( second_orbit ) : 
        filename = 'avg_frames_2nd.h5'
    else : 
        filename = 'avg_frames_3rd.h5'

    
    fignames = []
    figs     = []

    h5file = h5py.File(filename,'r')
    freqs = h5file['freq'].value[0,:]
    h5file.close()

    fignames = np.append(fignames,(dirout + 'freq-snapshots'))
    figs = np.append(figs,plt.figure(nwin,figsize=(13,3)))
    figs[-1].clf()
    a_form = "%g"
    ymajorFormatter = FormatStrFormatter('%1.1e')
    xmajorFormatter = FormatStrFormatter('%1.0f')

    vmaxes = [-1.,-1.,-1.,-3.]
    vmines = np.add(vmaxes,-4.)

    asep = 20.
    rmax = 50.


    j = 0 
    for itime in itimes :
        pix_x, pix_y, func = make_image(tout=0.,funcname='I_image_tavg',filename=filename,itime=itime,dolog=True,savepng=False,showplots=False)
        ax = figs[-1].add_subplot(1,ntimes,1+j)
        ax.set_xlim([-rmax,rmax])
        ax.set_ylim([-rmax,rmax])    
        ax.set_aspect('equal')
        ax.yaxis.set_major_formatter(NullFormatter())
        ax.xaxis.set_major_formatter(NullFormatter())
        title = str(r'$\nu$ = %4.1e Hz' %freqs[itime])
        ax.set_title(title)
        plt.subplots_adjust(wspace=0.2)
        CP = ax.pcolormesh(pix_x, pix_y,func,vmin=vmines[j],vmax=vmaxes[j],cmap=cmap)
        art1 = plt.Circle((0.,0.),   asep,alpha=0.5,color='white',fill=False)
        art2 = plt.Circle((0.,0.),2.*asep,alpha=0.5,color='white',fill=False)
        ax.add_artist(art1)
        ax.add_artist(art2)
   

        figs[-1].colorbar(CP,format='%3.0f',extend=extend,ticks=np.linspace(vmines[j],vmaxes[j],4))
        
            
        j += 1
        
    print("fignames[0] = ", fignames[0])
    plt.tight_layout()
    
    if savepng :
        i = 0 
        figs[i].savefig(str('%s.png' %fignames[i]), dpi=1000,bbox_inches='tight')
        #figs[i].savefig(str('%s.png' %fignames[i]), dpi=300,bbox_inches='tight')
        #figs[i].savefig(str('%s.pdf' %fignames[i]), dpi=300,bbox_inches='tight')
            
    print("All done ")
    sys.stdout.flush()

    return 

###############################################################################
###############################################################################

make_multi_image(second_orbit=True) 



