"""
interp_sph_grids.dat File Reader. Compatible with Python 2.7+ and 3.6+ at least.

Zachariah B. Etienne

Based on Python scripts written by Bernard Kelly:
https://bitbucket.org/zach_etienne/nrpy/src/master/mhd_diagnostics/

Find the latest version of this reader at the bottom of this Jupyter notebook:
https://github.com/zachetienne/nrpytutorial/blob/master/Tutorial-ETK_thorn-Interpolation_to_Spherical_Grids.ipynb

Usage instructions:

From the command-line, run via:
python Interp_Sph_ReadIn.py interp_sph_grids.dat [number of gridfunctions (58 or so)] [outfile]

Currently the last parameter "outfile" is required but not actually used.
"""
import numpy as np
import struct
import sys
import argparse
import h5py
import math
import matplotlib.pyplot as plt

RUNNING_FROM_HYDRO_ONLY = True

parser = argparse.ArgumentParser(description='Read file.')
parser.add_argument("datafile", help="main data file")
parser.add_argument("number_of_gridfunctions", help="number of gridfunctions")

parser.add_argument("outfileroot", help="root of output file names")

args = parser.parse_args()

datafile = args.datafile
outfileroot = args.outfileroot
number_of_gridfunctions = int(args.number_of_gridfunctions)

print("reading from "+str(datafile))

"""
read_char_array():
Reads a character array of size="size"
from a file (with file handle = "filehandle")
and returns the character array as a proper 
Python string.
"""
def read_char_array(filehandle,size):
    reached_end_of_string = False
    chartmp = struct.unpack(str(size)+'s', filehandle.read(size))[0]

    #https://docs.python.org/3/library/codecs.html#codecs.decode
    char_array_orig = chartmp.decode('utf-8',errors='ignore')

    char_array = ""
    for i in range(len(char_array_orig)):
        char = char_array_orig[i]
        # C strings end in '\0', which in Python-ese is '\x00'.
        #   As characters read after the end of the string will
        #   generally be gibberish, we no longer append 
        #   to the output string after '\0' is reached.
        if   sys.version_info[0]==3 and bytes(char.encode('utf-8')) == b'\x00':
            reached_end_of_string = True
        elif sys.version_info[0]==2 and char ==  '\x00':
            reached_end_of_string = True

        if reached_end_of_string == False:
            char_array += char
        else:
            pass # Continue until we've read 'size' bytes
    return char_array


"""
read_header()
Reads the header from a file.
"""
def read_header(filehandle):
    # This function makes extensive use of Python's struct.unpack
    # https://docs.python.org/3/library/struct.html
    # First store gridfunction name and interpolation order used:
    # fwrite(gf_name, 100*sizeof(char), 1, file);
    gf_name = read_char_array(filehandle,100)
    # fwrite(order, sizeof(CCTK_INT), 1, file);
    order = struct.unpack('i',filehandle.read(4))[0]

    # Then the radial grid parameters:
    # fwrite( & N0, sizeof(CCTK_INT), 1, file);
    N0 =    struct.unpack('i',filehandle.read(4))[0]
    # fwrite( & R0, sizeof(CCTK_REAL), 1, file);
    R0 =    struct.unpack('d',filehandle.read(8))[0]
    # fwrite( & Rin, sizeof(CCTK_REAL), 1, file);
    Rin =   struct.unpack('d',filehandle.read(8))[0]
    # fwrite( & Rout, sizeof(CCTK_REAL), 1, file);
    Rout =  struct.unpack('d',filehandle.read(8))[0]

    # Then the grid parameters related to the theta coordinate:
    # fwrite( & N1, sizeof(CCTK_INT), 1, file);
    N1           = struct.unpack('i', filehandle.read(4))[0]
    # fwrite( & x1_beg, sizeof(CCTK_REAL), 1, file);
    x1_beg       = struct.unpack('d', filehandle.read(8))[0]
    # fwrite( & theta_option, sizeof(CCTK_INT), 1, file);
    theta_option = struct.unpack('i', filehandle.read(4))[0]
    # fwrite( & th_c, sizeof(CCTK_REAL), 1, file);
    th_c         = struct.unpack('d', filehandle.read(8))[0]
    # fwrite( & xi, sizeof(CCTK_REAL), 1, file);
    xi           = struct.unpack('d', filehandle.read(8))[0]
    # fwrite( & th_n, sizeof(CCTK_INT), 1, file);
    th_n         = struct.unpack('i', filehandle.read(4))[0]

    # Then the grid parameters related to the phi coordinate:
    # fwrite( & N2, sizeof(CCTK_INT), 1, file);
    N2     = struct.unpack('i', filehandle.read(4))[0]
    # fwrite( & x2_beg, sizeof(CCTK_REAL), 1, file);
    x2_beg = struct.unpack('d', filehandle.read(8))[0]

    magic_number_check = 1.130814081305130e-21
    # fwrite( & magic_number, sizeof(CCTK_REAL), 1, file);
    magic_number = struct.unpack('d', filehandle.read(8))[0]
    if magic_number != magic_number_check:
        print("Error: Possible file corruption: Magic number mismatch. Found magic number = "+str(magic_number)+" . Expected "+str(magic_number_check))
        exit(1)
    # fwrite( & cctk_iteration, sizeof(CCTK_INT), 1, file);
    cctk_iteration = struct.unpack('i', filehandle.read(4))[0]
    # fwrite( & cctk_time, sizeof(CCTK_REAL), 1, file);
    cctk_time      = struct.unpack('d', filehandle.read(8))[0]
    

    return gf_name,order,N0,R0,Rin,Rout,N1,x1_beg,theta_option,th_c,xi,th_n,N2,x2_beg,cctk_iteration,cctk_time



##########################################################################################
### Create an array that contains the title of each specific gridfunction and make     ###
### sure that this list of strings mimics the order of the list of the gridfunction    ###
### names in gf_name output by the above function.                                     ### 
##########################################################################################
    

    
harm3d_group_names = ['rho','uu','ucon0','gam_v1','gam_v2','gam_v3','ucon1','ucon2', 'ucon3','ucon1_ucon0','ucon2_ucon0','ucon3_ucon0',\
                      'B1','B2','B3','gcov00','gcov01','gcov02','gcov03','gcov11','gcov12',\
                      'gcov13','gcov22','gcov23','gcov33','4Christoffel000', '4Christoffel001', '4Christoffel002', '4Christoffel003',\
                      '4Christoffel011', '4Christoffel012', '4Christoffel013','4Christoffel022', '4Christoffel023', '4Christoffel033', \
                      '4Christoffel100', '4Christoffel101', '4Christoffel102', '4Christoffel103', '4Christoffel111', '4Christoffel112',\
                      '4Christoffel113', '4Christoffel122', '4Christoffel123', '4Christoffel133', '4Christoffel200', '4Christoffel201', \
                      '4Christoffel202', '4Christoffel203','4Christoffel211', '4Christoffel212', '4Christoffel213', '4Christoffel222', \
                      '4Christoffel223',  '4Christoffel233', '4Christoffel300', '4Christoffel301', '4Christoffel302', '4Christoffel303', \
                      '4Christoffel311', '4Christoffel312', '4Christoffel313', '4Christoffel322', '4Christoffel323','4Christoffel333',\
                      '4ChristoffelSphere000', '4ChristoffelSphere001', '4ChristoffelSphere002', '4ChristoffelSphere003',\
                      '4ChristoffelSphere011', '4ChristoffelSphere012', '4ChristoffelSphere013','4ChristoffelSphere022', '4ChristoffelSphere023', '4ChristoffelSphere033', \
                      '4ChristoffelSphere100', '4ChristoffelSphere101', '4ChristoffelSphere102', '4ChristoffelSphere103', '4ChristoffelSphere111', '4ChristoffelSphere112',\
                      '4ChristoffelSphere113', '4ChristoffelSphere122', '4ChristoffelSphere123', '4ChristoffelSphere133', '4ChristoffelSphere200', '4ChristoffelSphere201', \
                      '4ChristoffelSphere202', '4ChristoffelSphere203','4ChristoffelSphere211', '4ChristoffelSphere212', '4ChristoffelSphere213', '4ChristoffelSphere222', \
                      '4ChristoffelSphere223',  '4ChristoffelSphere233', '4ChristoffelSphere300', '4ChristoffelSphere301', '4ChristoffelSphere302', '4ChristoffelSphere303', \
                      '4ChristoffelSphere311', '4ChristoffelSphere312', '4ChristoffelSphere313', '4ChristoffelSphere322', '4ChristoffelSphere323','4ChristoffelSphere333',
                      'gcovsph00','gcovsph01','gcovsph02','gcovsph03','gcovsph11','gcovsph12',\
                      'gcovsph13','gcovsph22','gcovsph23','gcovsph33','ucon1_ucon0sph','ucon2_ucon0sph','ucon3_ucon0sph',
                      'uconsph1','uconsph2', 'uconsph3','gamsph_v1','gamsph_v2','gamsph_v3','B1sph','B2sph','B3sph']




##########################################################################################
### This function will read in the group names specified above and effectively         ###
### multiply each function name by the number of interpolation orders inside the       ###
### IGM data.                                                                          ###
##########################################################################################


number_of_interpolation_orders = [1.0,2.0,4.0]
multi_harm3d_group_names = []


for j in range(0, len(harm3d_group_names)):
    for i in range(0, len(number_of_interpolation_orders)):
        multi = harm3d_group_names[j]
        multi_harm3d_group_names.append(multi)
        
        
##########################################################################################
### Now open the IGM file and read all of the data                                     ###
##########################################################################################


# Now open the file and read all the data
with open(datafile,"rb") as f:
    # Main loop over all gridfunctions
    for i in range(number_of_gridfunctions):
        # Data are output in chunks, one gridfunction at a time, with metadata
        #    for each gridfunction stored at the top of each chunk
        # First read in the metadata:
        gf_name, order, N0, R0, Rin, Rout, N1, x1_beg, theta_option, th_c, xi, th_n, N2, x2_beg, cctk_iteration, cctk_time = read_header(f)
        print("\n\nReading gridfunction "+gf_name)
        data_chunk_size = N0*N1*N2*8 # 8 bytes per double-precision number
        # Next read in the full gridfunction data
        bytechunk = f.read(data_chunk_size)
        # Process the data using NumPy's frombuffer() function:
        #   https://docs.scipy.org/doc/numpy/reference/generated/numpy.frombuffer.html
        buffer_res = np.frombuffer(bytechunk)
        # Reshape the data into a 3D NumPy array:
        #   https://docs.scipy.org/doc/numpy/reference/generated/numpy.reshape.html
        this_data = buffer_res.reshape(N0,N1,N2)


        # Sanity check: Make sure the output in the "middle" of the grid looks reasonable.
        ii = int(N0/2)
        jj = int(N1/2)
        kk = int(N2/2)
        print(gf_name,this_data[kk,jj,ii])
        
        
        # This will transpose the IGM data from (k,j,i) to (i,j,k) -> to mimic the input of Harm3D
        Harm_data = np.transpose(this_data, (2, 1, 0))
        
        
        
        # Create a dataset inside an hdf5 file named rdump_start_int_i. There will be 
        
        if order == 1.0:
            with h5py.File('rdump_start_int_1.h5', 'a') as t:
                t.create_dataset(multi_harm3d_group_names[i], data=Harm_data)
                print(1)
                t.close()
                
        if order == 2.0:
            with h5py.File('rdump_start_int_2.h5', 'a') as d:
                d.create_dataset(multi_harm3d_group_names[i], data=Harm_data)
                print(2)
                d.close()
               
        if order == 4.0:
            with h5py.File('rdump_start_int_4.h5', 'a') as g:
                g.create_dataset(multi_harm3d_group_names[i], data=Harm_data)
                print(4)
                g.close()
                

            
# Now open the file and read all the data 
with open(datafile,"rb") as f:
    # First read in the metadata:
    gf_name, order, N0, R0, Rin, Rout, N1, x1_beg, theta_option, th_c, xi, th_n, N2, x2_beg, cctk_iteration, cctk_time = read_header(f)
    
    # Initialize spacetime quantities at the same dimensionality as other gridfunctions.
    file_size = len(this_data)
        
    
    # For converting the grid spacing to numerical coordinates read in by Harm3D
    delta_x0 = 1/N0*math.log((Rout-R0)/(Rin-R0))
    x0_beg = math.log(Rin - R0)
    x0 = np.zeros((file_size,file_size,file_size))
    r_i = np.zeros((file_size,file_size,file_size))
    
    delta_x1 = 1/N1
    x1 = np.zeros((file_size,file_size,file_size))
    theta_i = np.zeros((file_size,file_size,file_size))
    
    delta_x2 = 2*math.pi/N2
    x2 = np.zeros((file_size,file_size,file_size))
    phi_i = np.zeros((file_size,file_size,file_size))

    x = np.zeros((file_size,file_size,file_size))
    y = np.zeros((file_size,file_size,file_size))
    z = np.zeros((file_size,file_size,file_size))
    
    
    # Loop over the grid and create arrays that contain numerical coords (x0,x1,x2)
    # and physical coords (r_i,theta_i,phi_i)
    for i in range(0,len(this_data)):
        for j in range(0,len(this_data)):
            for k in range(0,len(this_data)):
                x0[i,j,k] = x0_beg + (i+1/2)*delta_x0
                x1[i,j,k] = x1_beg + (j+1/2)*delta_x1
                x2[i,j,k] = x2_beg + (k+1/2)*delta_x2
                
                r_i[i,j,k] = R0 + math.exp(x0[i,j,k])
                theta_i[i,j,k] = th_c + (math.pi - 2*th_c)*x1[i,j,k] + xi*math.sin(2*math.pi*x1[i,j,k])
                phi_i[i,j,k] = x2[i,j,k]

                x[i,j,k] = r_i[i,j,k] * math.sin(theta_i[i,j,k]) * math.cos(phi_i[i,j,k])
                y[i,j,k] = r_i[i,j,k] * math.sin(theta_i[i,j,k]) * math.sin(phi_i[i,j,k])
                z[i,j,k] = r_i[i,j,k] * math.cos(theta_i[i,j,k])

        
##########################################################################################
### Write the spacetime arrays to the Harm Readable HDF5 file, multiply IGM's          ###
### pressure primitive by 3.0 to produce Harm3D's internal energy density              ###     
### primitive - derived from (gamma - 1)*uu = P. In this case, gamma = 4/3             ###
### and set the magnetic field components to B^i = 0 for hydro only testing.           ###
##########################################################################################
    
with h5py.File('rdump_start_int_1.h5', 'a') as t:
    t.create_dataset('xp1', data = x0)
    t.create_dataset('xp2', data = x1)
    t.create_dataset('xp3', data = x2)
    t.create_dataset('x1', data = r_i)
    t.create_dataset('x2', data = theta_i)
    t.create_dataset('x3', data = phi_i)
    
    uu_IGM = t['uu'].value
    uu_IGM = 3.0*uu_IGM
    del t['uu']
    t.create_dataset('uu', data = uu_IGM)
    
    if RUNNING_FROM_HYDRO_ONLY == True:
        t['B1'][:] = 0.0
        t['B2'][:] = 0.0
        t['B3'][:] = 0.0
    t.close()
        

with h5py.File('rdump_start_int_2.h5', 'a') as d:
    d.create_dataset('xp1', data = x0)
    d.create_dataset('xp2', data = x1)
    d.create_dataset('xp3', data = x2)
    d.create_dataset('x1', data = r_i)
    d.create_dataset('x2', data = theta_i)
    d.create_dataset('x3', data = phi_i)
    
    uu_IGM = d['uu'].value
    uu_IGM = 3.0*uu_IGM
    del d['uu']
    d.create_dataset('uu', data = uu_IGM)
    
    if RUNNING_FROM_HYDRO_ONLY == True:
        d['B1'][:] = 0.0
        d['B2'][:] = 0.0
        d['B3'][:] = 0.0
    d.close()
       
with h5py.File('rdump_start_int_4.h5', 'a') as g:
    g.create_dataset('xp1', data = x0)
    g.create_dataset('xp2', data = x1)
    g.create_dataset('xp3', data = x2)
    g.create_dataset('x1', data = r_i)
    g.create_dataset('x2', data = theta_i)
    g.create_dataset('x3', data = phi_i)
    
    uu_IGM = g['uu'].value
    uu_IGM = 3.0*uu_IGM
    del g['uu']
    g.create_dataset('uu', data = uu_IGM)
    
    if RUNNING_FROM_HYDRO_ONLY == True:
        g['B1'][:] = 0.0
        g['B2'][:] = 0.0
        g['B3'][:] = 0.0
    g.close()

    ### If you want to plot the data for visualization before running with Harm, 
    ### First, move the data to a  folder named 'dumps' and rename it KDHARM0.000000.h5, 
    ### then run polp_new.py --- Example command for command line:
    ### -> ipython
    ### -> run polp_new.py
    ### -> polp(0, funcname='rho', ith=None, dolog=0, show_plot=1, savepng=1, plottitle = 'notitle')

