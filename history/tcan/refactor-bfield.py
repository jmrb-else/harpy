"""

  change-bfield.py: 
  ------------

    -- divides the B-field by the gdet in the hdf5 file, so that we
       can use different metrics using the same snapshot data and same
       grid while not introducing monopoles;


"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import numpy as np
import os,sys
import h5py
import glob
import string as str

#######################################################################

print(sys.argv)
if len(sys.argv) == 3 :
    filename    = sys.argv[1]
    newgdetfile = sys.argv[2]
else : 
    sys.exit('Need a file name!!  Exitting!!')
    
#######################################################################
# Open the file in read/write access, will modify the data "in place" 
#######################################################################
h5in  = h5py.File(filename,'r+')
gdet = np.array(h5in['gdet'])
h5new = h5py.File(newgdetfile,'r')
gdetnew = np.array(h5new['gdet'])
bfactor = np.divide(gdet, gdetnew, out=np.zeros_like(gdet), where=gdetnew!=0.)

for func in ['B1','B2','B3'] : 
    print(" func = ", func )
    dat_tmp =  h5in[func] 
    dat_tmp[...] *= bfactor
    #h5in.copy(func,h5out)

h5in.close()

