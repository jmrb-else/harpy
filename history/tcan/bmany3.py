"""  

  bmany3.py: 
  ------------
    -- make a row of 2-d  color contours with a common color bar at the bottom


"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, NullFormatter
import matplotlib.gridspec as gridspec


import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################

def make_multi_image(dirout=None, extend='both', funcname='I_image', maxf=20. ):
    """  
    """

#    ithetas = [0, 39, 56, 71, 84, 90]
#    ithetas = [0, 39, 71, 84, 90]
    ithetas = [0, 71, 90]
    nthetas = len(ithetas)
    
    itimes = [103,108,113,118]
    ntimes = len(itimes)

    showplots=0
    savepng=1
    nwin=0
    
    cmap  = plt.cm.Spectral_r
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 
    
    if not showplots :
        plt.ioff()

        
    fignames = []
    figs     = []

    #h5file = h5py.File(filename,'r')
    #freqs = h5file['freq'].value[0,:]
    #h5file.close()

    if( funcname == 'I_image' ) :
        plotname = 'inu-snapshots-'
        vmaxes = -4.5*np.ones(ntimes)
        vmines = -8.*np.ones(ntimes)
        dolog = True
        factor = 1.
        
    else : 
        plotname = 'tau-snapshots-'
        vmaxes = maxf*np.ones(ntimes)
        vmines = np.zeros(ntimes)
        dolog = False
        factor = 50.
    

    fignames = np.append(fignames,'all-inu-snapshots')
    figs = np.append(figs,plt.figure(nwin,figsize=(13,9)))
    figs[-1].clf()
    a_form = "%g"
    ymajorFormatter = FormatStrFormatter('%1.1e')
    xmajorFormatter = FormatStrFormatter('%1.0f')


    asep = 20.

    i = 0 
    for ith in ithetas :
        th_str = str('%06d' %ith)
        dirout = 'th_'+th_str+'/'
        prefix = dirout+'light_curve_'+th_str+'_'

        if( ith == 90 ) :
            rmax = 30.
        else:
            rmax = 50.
        
        j = 0 
        for itime in itimes :
            filename = prefix+str('%06d' %itime)+'.h5'
            print("filename = ", filename)
            pix_x, pix_y, func = make_image(tout=0.,funcname=funcname,filename=filename,itime=0,dolog=dolog,savepng=False,showplots=False)
            func *= factor 
            ax = figs[-1].add_subplot(nthetas,ntimes,i*ntimes+j+1)
            ax.set_xlim([-rmax,rmax])
            ax.set_ylim([-rmax,rmax])    
            ax.set_aspect('equal')
            ax.yaxis.set_major_formatter(NullFormatter())
            ax.xaxis.set_major_formatter(NullFormatter())
            #title = str(r'$\nu$ = %4.1e Hz' %freqs[itime])
            #ax.set_title(title)
            plt.subplots_adjust(wspace=0.001,hspace=0.01)
            CP = ax.pcolormesh(pix_x, pix_y,func,vmin=vmines[j],vmax=vmaxes[j],cmap=cmap)
            #art1 = plt.Circle((0.,0.),   asep,alpha=0.5,color='white',fill=False)
            #art2 = plt.Circle((0.,0.),2.*asep,alpha=0.5,color='white',fill=False)
            #ax.add_artist(art1)
            #ax.add_artist(art2)
  
            #figs[-1].colorbar(CP,format='%3.0f',extend=extend,ticks=np.linspace(vmines[j],vmaxes[j],4))
            j += 1
        i += 1 

    figs[-1].subplots_adjust(bottom=0.05)
    cbar_ax = figs[-1].add_axes([0.2,0.0,0.6,0.03])
    figs[-1].colorbar(CP, orientation="horizontal",cax=cbar_ax,extend='both')

        
    print("fignames[0] = ", fignames[0])
    #plt.tight_layout()
    
    if savepng :
        i = 0 
        figs[i].savefig(str('%s.png' %fignames[i]), dpi=1000,bbox_inches='tight')
        #figs[i].savefig(str('%s.png' %fignames[i]), dpi=300,bbox_inches='tight')
        #figs[i].savefig(str('%s.pdf' %fignames[i]), dpi=300,bbox_inches='tight')
            
    print("All done ")
    sys.stdout.flush()

    return 

###############################################################################
###############################################################################



make_multi_image()

