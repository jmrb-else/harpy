"""  

  merge_volumes_h5.py: 
  ------------
    -- program to merge a set of volumes into one global volume/domain hdf5 file
    -- looks for all the files that matches  "basename.vol*.h5"  where "*" is 
        a zero-padded  5-digit integer usually

"""

import numpy as np
import h5py

import os,sys
import glob

VERBOSE = 0 

###############################################################################
###############################################################################
###############################################################################

def find_list_of_h5files(base):
    """  
    Returns with the list of files matching 'base.N.h5' where 'N' is a
    zero-padded 6-digit integer.
    """

    globname = './' + base + ".vol*.h5"
    h5list = glob.glob(globname)

    if len(h5list) <= 0:
        print("Cannot find any files to merge with basename = ", base)
        sys.exit("Can no longer proceed, exiting....")

    list.sort(h5list)
    return h5list

###############################################################################
###############################################################################
###############################################################################

def merge_volumes_h5(basename):
    """  
    Primary routine that calls all the functions and loops over the files
    and grid functions.
    """

    # Obtain list of files in current working directory to process
    h5list = find_list_of_h5files(basename)
    n_h5files = len(h5list) 
    if( VERBOSE ): 
        print(basename)
        print(h5list)

    # Set output file name 
    destfilename = basename + '.h5'
    if( VERBOSE ): 
        print(destfilename)
    sys.stdout.flush()
    
    #################
    # Read various details about datasets inside example file : 
    #################
    # Get a list of all the parameters in the hdf5 header file:  
    fh5    = h5py.File(h5list[0],'r')
    fh5new = h5py.File(destfilename,'w')

    all_h5_objs = []
    fh5.visit(all_h5_objs.append)

#    print(all_h5_objs)

    all_groups   = [ obj for obj in all_h5_objs if isinstance(fh5[obj],h5py.Group) ]
    all_datasets = [ obj for obj in all_h5_objs if isinstance(fh5[obj],h5py.Dataset) ]

    # Create all the groups: 
    for obj in all_groups : 
        fh5new.create_group(fh5[obj].name)

    if( VERBOSE ): 
        print("  ")

    funcnames = []

    # Copy over all datasets as is except for those in the Header, in which case 
    #  we copy over only the first element's worth of each array:
    for obj in all_datasets : 
        gd = fh5[obj]
        if '/Header' in gd.name  :
            if( VERBOSE ): 
                print(" Shrinking and Copying over :  " + gd.name)
            new_arr = np.zeros((1,), dtype=gd.dtype)
            gd.read_direct(new_arr, np.s_[0:1], np.s_[0:1])
            dnew = fh5new.create_dataset(gd.name, (1,) , dtype=gd.dtype, data=new_arr)

        else :
            if( VERBOSE ): 
                print(" Creating over :  " + gd.name)
            funcnames = np.append(funcnames,gd.name)
            shape1 = np.copy(gd.shape)
            shape1[0] *= n_h5files
            dset = fh5new.create_dataset(gd.name,shape1,dtype=fh5[obj].dtype)

    if( VERBOSE ) :
        print("funcnames = ", funcnames)
    
    n1 = fh5new['/Header/Grid/N1'][0]

    fh5.close()
    
    for file in h5list :
        fh5 = h5py.File(file,'r')
        ivol = fh5['/Header/Grid/cpupos1'][0]
        ibeg = n1*ivol
        iend = n1*(ivol+1)
        if( VERBOSE ) : 
            print("ibeg iend = ", ibeg, iend)
        for func in funcnames :
            fh5new[func][ibeg:iend,:,:] = fh5[func][:,:,:]

        fh5.close()
                
        
    fh5new.close()

    
###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python merge_volumes_h5.py  KDHARM0.000000
# 
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) > 1 :
        merge_volumes_h5(sys.argv[1])
    else:
        print("Usage: python merge_volumes_h5.py  <common-and-unique-prefix-of-files-to-merge>")
        sys.exit("User must specify the prefix string identifying the set of files to merger, exiting....")

