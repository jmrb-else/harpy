"""  

  disk_ecc.py: 
  ------------
   -- routines for calculating and plotting the eccentricity in disks; 
   

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.fftpack import fft
import scipy.signal as signal 
import numpy as np
import os,sys
import h5py

from get_sim_info import *
from readhist_h5_all import *
from harm import *
from myfft import my_fft
from mymath import *


###############################################################################
###############################################################################
###############################################################################

def make_ecc_from_3d_dump(itime=0,
                          outfilename='ecc_analysis.h5',
                          dump_coord=False,
                          runname='inject_disk'
                          ):
    """
    make_ecc_from_3d_dump():
    ------------

    -- calculates the eccentricity per annulus for each time slice,
       following Shi++2012 eq. (16) (but without the time average;

    -- compare to way we calculate it in make_ecc_from_surface_dump();

    -- writes the resultant datasets to the run's dump directory in
       "ecc_analysis.h5" unless otherwise specified;

    """


    plt.ioff()

    h_o_r = 0.1 
    
    #################
    # Read in simulation's information: 
    #################
    pnames = ['dirname', 'gdump_name', 'dump_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'rad_freq', 'dump_freq', 'surf_name']
    sim_info = get_sim_info(pnames, runname=runname)
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    rad_itime = int( (itime * sim_info['dump_freq'])  / sim_info['rad_freq'])
    timeid = '%06d' %itime    #harm HDF5 dumps
    rad_timeid = '%06d' %rad_itime   #harm HDF5 radflux dumps
    filename=sim_info['dump_name']+'.'+timeid+'.h5'
    radname = sim_info['dump_name']+'.RADFLUX.'+rad_timeid+'.h5'
    
    full_outfilename = sim_info['dirname']+'modes/'+outfilename

    #################
    # Get coordinates: 
    #################
    slall = slice(None,None,None)
    h5g = h5py.File(sim_info['gdump_name'],'r')
    # Assuming that phi is independent of r, theta : 
    slice_rr = (slall,       slice(0,1,1),slice(0,1,1))
    slice_th = (slice(0,1,1),slall,       slice(0,1,1))
    slice_ph = (slice(0,1,1),slice(0,1,1),slall       )
    rr    = read_3d_hdf5_slice("x1",slice_rr,h5g)
    theta = read_3d_hdf5_slice("x2",slice_th,h5g)
    phi   = read_3d_hdf5_slice("x3",slice_ph,h5g)
    h5g.close()
    
    #################
    # Read in 3-d data:
    #################
    # shape should be  (nr, nth, nphi) :
    th_beg = 0.5*np.pi - 2.*h_o_r
    th_end = 0.5*np.pi + 2.*h_o_r
    ith_beg, ith_end = find_bracket(th_beg,th_end,theta)

    r_beg = 2.*asep
    r_end = 6.*asep
    ir_beg, ir_end = find_bracket(r_beg,r_end,rr)

    r_out = rr[0:ir_end+1]
    nr = len(r_out)

    # shape should be  (nr, nth, nphi) :
    #slice_3d = (slice(0,ir_end+1,1),slice(ith_beg,ith_end+1,1),slall)
    slice_3d = (slice(0,ir_end+1,1),slall,slall)
    
    # Get regular dump quantities: 
    print("Reading in ", filename)
    sys.stdout.flush()
    h5f = h5py.File(filename,'r')
    h5r = h5py.File(radname,'r')

    gdet   = read_3d_hdf5_slice('gdet',slice_3d,h5f)
    gcov00 = read_3d_hdf5_slice('gcov00',slice_3d,h5f)
    gcov01 = read_3d_hdf5_slice('gcov01',slice_3d,h5f)
    gcov02 = read_3d_hdf5_slice('gcov02',slice_3d,h5f)
    gcov03 = read_3d_hdf5_slice('gcov03',slice_3d,h5f)
    gcov11 = read_3d_hdf5_slice('gcov11',slice_3d,h5f)
    gcov12 = read_3d_hdf5_slice('gcov12',slice_3d,h5f)
    gcov13 = read_3d_hdf5_slice('gcov13',slice_3d,h5f)
    gcov22 = read_3d_hdf5_slice('gcov22',slice_3d,h5f)
    gcov23 = read_3d_hdf5_slice('gcov23',slice_3d,h5f)
    gcov33 = read_3d_hdf5_slice('gcov33',slice_3d,h5f)
    
    gcon00 =  gcov11*(gcov22*gcov33 - gcov23*gcov23) - gcov12*(gcov12*gcov33 - gcov13*gcov23) + gcov13*(gcov12*gcov23 - gcov13*gcov22)
    gcon01 = -gcov01*(gcov22*gcov33 - gcov23*gcov23) + gcov02*(gcov12*gcov33 - gcov13*gcov23) - gcov03*(gcov12*gcov23 - gcov13*gcov22)
    gcon02 =  gcov01*(gcov12*gcov33 - gcov23*gcov13) - gcov02*(gcov11*gcov33 - gcov13*gcov13) + gcov03*(gcov11*gcov23 - gcov13*gcov12)
    gcon03 = -gcov01*(gcov12*gcov23 - gcov22*gcov13) + gcov02*(gcov11*gcov23 - gcov12*gcov13) - gcov03*(gcov11*gcov22 - gcov12*gcov12)

    det = gcov00*gcon00 + gcov01*gcon01 + gcov02*gcon02 + gcov03*gcon03
    gcon02 = gcov00 = gcov01 = gcov02 = gcov03 = gcov12 = gcov13 = gcov22 = gcov23 = 0
    det[det==0] += 1.e-10    # to not prevent further computations in case a
                             # singular matrix is found at some points. this
                             # happens for instance when using excision...
    inv_det = 1.0 / det
    det = 0 

    gcon00[gcon00==0] += -1.e-10
    gcon00 *= inv_det
    gcon01 *= inv_det
    gcon03 *= inv_det

    v1  =  read_3d_hdf5_slice("v1" ,slice_3d,h5r)
    v3  =  read_3d_hdf5_slice("v3" ,slice_3d,h5r)
    gamma =  read_3d_hdf5_slice("gamma" ,slice_3d,h5f)

    tmp = gamma / (np.sqrt(-gcon00))
    ucon1 = v1 - gcon01 * tmp
    ucon3 = v3 - gcon03 * tmp
    gcon00 = gcon01 = gcon03 = v1 = v3 = gamma = tmp = 0 

    #dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_3d,h5f)
    #dx_dxp11 = 0

    # This handles transformation to numerical coordinate basis automatically for diagonal metrics: 
    ucon1 *= np.sqrt(gcov11)
    ucon3 *= np.sqrt(gcov33)
    gcov11 = gcov33 = 0 
    
    rho =  read_3d_hdf5_slice("rho",slice_3d,h5r)

    h5f.close()
    h5r.close()

    rho *= gdet 
    func1 = rho * ucon1
    print("func1 shape = ", func1.shape)
    func1_th_int = np.sum( func1, axis=1 )
    func1 = 0

    func2 = rho * ucon3
    print("func2 shape = ", func2.shape)
    rho = 0 
    denom = np.sum( func2, axis=(1,2) )
    
    sin_phi = np.sin(phi)
    cos_phi = np.cos(phi)
    sin_2d_phi = np.outer( np.ones(nr), sin_phi )
    cos_2d_phi = np.outer( np.ones(nr), cos_phi )
    numer_a = np.sum( cos_2d_phi * func1_th_int, axis=1 )
    numer_b = np.sum( sin_2d_phi * func1_th_int, axis=1 )
    numer_abs = np.sqrt( numer_a**2 + numer_b**2 )

    eccentricity  = numer_abs / denom 

    print("Writing ", full_outfilename, "  ... ") 
    h5out = h5py.File(full_outfilename,'a')
    
    dset = h5out.create_dataset('ecc-'+timeid, data=eccentricity)
    if( dump_coord ) : 
        dset = h5out.create_dataset('r_out', data=r_out)

    h5out.close()
    print("..... done!")

    return




###############################################################################
###############################################################################
###############################################################################

def make_ecc_from_surface_dump(outfilename='ecc_analysis.h5',
                               runname='inject_disk'
                               ):
    """
    make_ecc_from_surface_dump():
    ------------

    -- calculates the eccentricity per annulus for each surface data
       time slice, following Shi++2012 eq. (16) (but without the time
       average;

    -- compare to way we calculate it in make_ecc_from_3d_dump();

    -- writes the resultant datasets to the run's dump directory in
       "ecc_analysis.h5" unless otherwise specified;

    """


    plt.ioff()
    
    #################
    # Read in simulation's information: 
    #################
    pnames = ['dirname', 'gdump_name', 'dump_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'rad_freq', 'dump_freq', 'surf_name']
    sim_info = get_sim_info(pnames, runname=runname)

    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    surf_filename=sim_info['surf_name']+'.all.h5'

    #################
    # Read in 3-d data:
    #################
    # shape should be  (nr, nth, nphi) :
    slall = slice(None,None,None)

    # Get coordinates: 
    h5g = h5py.File(sim_info['gdump_name'],'r')
    # Assuming that phi is independent of r, theta : 
    slice_1d = (slice(0,1,1),slice(0,1,1),slall)
    phi = read_3d_hdf5_slice("x3",slice_1d,h5g)
    h5g.close()

    # shape should be  (nr, nth, nphi) :
    slice_3d = (slall,slall,slall)
    print("Reading in ", surf_filename)
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    nt = len(t_out)
    nr = len(r_out)

    func1 =  read_3d_hdf5_slice("/Bound/S_hUx",slice_3d,h5f)
    tmp   =  read_3d_hdf5_slice("/Unbound/S_hUx",slice_3d,h5f)
    func1 += tmp
    tmp = 0 

    func2 =  read_3d_hdf5_slice("/Bound/S_hUz",slice_3d,h5f)
    tmp   =  read_3d_hdf5_slice("/Unbound/S_hUz",slice_3d,h5f)
    func2 += tmp
    tmp = 0 
    h5f.close()

    ##################################
    #  We want to calculate \sqrt{-g} rho * u^r \sqrt(g_{rr}) and
    #  would have to calculate g_rr from g_11 and dx_dxp11 but we do
    #  not have this data in the surface files, so let us approximate
    #  g_rr = 1 and  g_{phi phi} = r^2.  
    ###################################
    term2 = np.outer(np.ones(nt), r_out)
    denom = np.sum( func2, axis=2 )
    denom *= term2
    term2 = func2 = 0 

    sin_phi = np.sin(phi)
    cos_phi = np.cos(phi)
    sin_2d_phi = np.reshape(np.outer(np.ones(nt), np.outer( np.ones(nr), sin_phi )), func1.shape)
    cos_2d_phi = np.reshape(np.outer(np.ones(nt), np.outer( np.ones(nr), cos_phi )), func1.shape)
    sin_phi = cos_phi = 0
    numer_a = np.sum( cos_2d_phi * func1, axis=2 )
    numer_b = np.sum( sin_2d_phi * func1, axis=2 )
    numer_abs = np.sqrt( numer_a**2 + numer_b**2 )
    numer_a = numer_b = 0 

    eccentricity  = numer_abs / denom 

    print("Writing ", full_outfilename, "  ... ") 
    h5out = h5py.File(full_outfilename,'a')
    
    dset = h5out.create_dataset('ecc-surf-spacetime', data=eccentricity)

    h5out.close()
    print("..... done!")

    return

###############################################################################
###############################################################################
###############################################################################

def make_ecc_phase(outfilename='ecc_analysis2.h5',
                   runname='inject_disk'
                   ):
    """
    make_ecc_phase():
    ------------

    -- calculates the eccentricity's phase per annulus for each surface data
       time slice, following Shi++2012 eq. (16) (but without the time
       average;


    """

    plt.ioff()

    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    
    #################
    # Read in simulation's information: 
    #################
    pnames = ['dirname', 'gdump_name', 'dump_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'rad_freq', 'dump_freq', 'surf_name','t_lump_beg', 'tbeg', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)
    asep = sim_info['rsep0'][0]

    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    surf_filename=sim_info['surf_name']+'.all.h5'

    #################
    # Read in 3-d data:
    #################
    # shape should be  (nr, nth, nphi) :
    slall = slice(None,None,None)

    # Get coordinates: 
    h5g = h5py.File(sim_info['gdump_name'],'r')
    # Assuming that phi is independent of r, theta : 
    slice_1d = (slice(0,1,1),slice(0,1,1),slall)
    phi = read_3d_hdf5_slice("x3",slice_1d,h5g)
    h5g.close()

    # shape should be  (nr, nth, nphi) :
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_orig = h5f['rout'].value
    nt = len(t_out)

    r_beg = 2.*asep
    r_end = 4.*asep
    ir_beg, ir_end = find_bracket(r_beg,r_end,r_orig)

    r_out = r_orig[ir_beg:ir_end+1]
    nr = len(r_out)

    sl_r = slice(ir_beg, ir_end+1, None)
    slice_3d = (slall,sl_r,slall)
    print("Reading in ", surf_filename)

    func1 =  read_3d_hdf5_slice("/Bound/S_hUx",slice_3d,h5f)
    tmp   =  read_3d_hdf5_slice("/Unbound/S_hUx",slice_3d,h5f)
    func1 += tmp
    tmp = 0 

    func2 =  read_3d_hdf5_slice("/Bound/S_hUz",slice_3d,h5f)
    tmp   =  read_3d_hdf5_slice("/Unbound/S_hUz",slice_3d,h5f)
    func2 += tmp
    tmp = 0 
    h5f.close()

    ##################################
    #  We want to calculate \sqrt{-g} rho * u^r \sqrt(g_{rr}) and
    #  would have to calculate g_rr from g_11 and dx_dxp11 but we do
    #  not have this data in the surface files, so let us approximate
    #  g_rr = 1 and  g_{phi phi} = r^2.  
    ###################################
    term2 = np.outer(np.ones(nt), r_out)
    print("term2 shape = ", np.shape(term2))
    print("func2 shape = ", np.shape(func2))
    term2 *= np.sum( func2, axis=2 )
    denom = np.sum( term2, axis=1)
    term2 = func2 = 0 

    nphi = len(phi)
    sin_phi = np.sin(phi)
    cos_phi = np.cos(phi)
    #rtmp = np.reshape(np.outer(np.ones(nt), np.outer( r_out , np.ones(nphi) )), func1.shape)
    #func1 *= rtmp
    #rtmp = 0 
    func1 = np.sum(func1,axis=1)
    sin_2d_phi = np.reshape(np.outer(np.ones(nt), sin_phi ), func1.shape)
    cos_2d_phi = np.reshape(np.outer(np.ones(nt), cos_phi ), func1.shape)
    sin_phi = cos_phi = 0
    numer_a = np.sum( cos_2d_phi * func1, axis=1 )
    numer_b = np.sum( sin_2d_phi * func1, axis=1 )
    numer_abs = np.sqrt( numer_a**2 + numer_b**2 )

    numer_x = numer_a / denom
    numer_y = -numer_b / denom
    phase = np.arctan2(numer_y,numer_x)
    eccentricity = numer_abs / denom

    print("shape of ecc = ", np.shape(eccentricity))
    print("shape of phase = ", np.shape(phase))
    print("shape of t_out = ", np.shape(t_out))
    freq_tmp, power_fft_ecc, amp = my_fft(t_out, eccentricity)
    freq_fft_ecc  = freq_tmp[     freq_tmp > 0. ]
    power_fft_ecc = power_fft_ecc[freq_tmp > 0. ]
    
    freq_tmp, power_fft_phase, amp = my_fft(t_out, phase)
    freq_fft_phase  = freq_tmp[       freq_tmp > 0. ]
    power_fft_phase = power_fft_phase[freq_tmp > 0. ]
    
    
    numer_x = numer_y = numer_a = numer_b = 0 

    print("Writing ", full_outfilename, "  ... ") 
    h5out = h5py.File(full_outfilename,'a')
    
    dset = h5out.create_dataset('ecc-lump'            , data=eccentricity)
    dset = h5out.create_dataset('ecc-lump-fft-all'    , data=power_fft_ecc)
    dset = h5out.create_dataset('ecc-lump-freqfft-all', data=freq_fft_ecc)
    dset = h5out.create_dataset('ecc-lump-phase'      , data=phase)
    dset = h5out.create_dataset('ecc-lump-phase-fft'  , data=power_fft_phase)
    dset = h5out.create_dataset('tout', data=t_out)

    t_lump = sim_info['t_lump_beg']
    t_start = 3.e4
    if( t_lump is not None ) :
        t_start = t_lump
    tbeg = sim_info['tbeg']
    it_start = np.int( (t_start-tbeg) / sim_info['surf_freq'] )
    t_tmp = t_out[it_start:]
    e_tmp = eccentricity[it_start:]
    freq_ecc_tlump, power_fft_ecc_tlump, amp = my_fft(t_tmp, e_tmp)
    dset = h5out.create_dataset('ecc-lump-fft-tlump'      , data=power_fft_ecc_tlump)
    dset = h5out.create_dataset('ecc-lump-freqfft-tlump'  , data=freq_ecc_tlump)
    

    h5out.close()

    print("....finished!")
    return


###############################################################################
###############################################################################
###############################################################################

def plot_ecc_phase(outfilename='ecc_analysis2.h5',
                   savepng=True,
                   savepdf=False,
                   cmap  = plt.cm.Spectral_r,
                   runname='inject_disk',
                   tmax=None,
                   tag=''
                   ):
    """
    plot_ecc_phase():
    ------------

    -- calculates the eccentricity's phase per annulus for each surface data
       time slice, following Shi++2012 eq. (16) (but without the time
       average;


    """
    plt.ioff()

    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    
    #################
    # Read in simulation's information: 
    #################
    pnames = ['dirname', 'gdump_name', 'dump_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'rad_freq', 'dump_freq', 'surf_name','t_lump_beg', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    freq_bin = sim_info['freq_bin_orbit']
    binary_period = 1./freq_bin
    t_lump = sim_info['t_lump_beg']

    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    print(" Reading ecc data ...")
    h5f = h5py.File(full_outfilename,'r')
    ecc_mag          = h5f['ecc-lump'            ].value[:]
    ecc_phase        = h5f['ecc-lump-phase'      ].value[:]
    ecc_fft_all      = h5f['ecc-lump-fft-all'    ].value[:]
    ecc_freqfft_all  = h5f['ecc-lump-freqfft-all'].value[:]
    t_out            = h5f['tout'].value[:]

    if( t_lump is not None ) :
        ecc_fft_tlump      = h5f['ecc-lump-fft-all'    ].value[:]
        ecc_freqfft_tlump  = h5f['ecc-lump-freqfft-all'].value[:]
    
    h5f.close()

    if( tmax is None ) :
        tmax = np.max(t_out)
        
    ecc_phase = np.pi - ecc_phase

    smooth_period = binary_period
    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    ftmp = np.reshape(ecc_mag,(len(ecc_mag),1))
    ecc_mag_smooth = np.reshape(signal.convolve2d(ftmp,sm_window,mode='same',boundary='symm'),(len(ecc_mag)))

    omega2tmp = np.gradient(ecc_phase)
    omega2tmp[ omega2tmp <= 0. ] = np.nan
    omega2 = interp_over_bad_vals(omega2tmp,t_out)
    dt = np.gradient(t_out)
    omega2 = omega2tmp[:]/dt[:]
    
    maxf  = -1.
    minf  = -4.

    lsize = 15
    linew = 1.1
    alpha = 0.5
    nwin = 0
    fignames = []
    figs = []



    funcname = dirout + 'ecc_timeseries2'+tag
    nwin += 1
    fignames = np.append(fignames,funcname)
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax2 = ax.twiny()
    funcout = np.log10(np.abs(ecc_mag_smooth))
    funcout2 = np.log10(np.abs(ecc_mag))
    ax.set_ylabel(r'$\log_{10} \ e_\mathrm{lump}$',size=lsize)
    ax.set_xlabel(r'$t \ [t_\mathrm{bin}]$',size=lsize)
    ax.set_xlim(1e-10,tmax/binary_period)
    ax.set_ylim(minf,maxf)
    ax.plot(t_out/binary_period,funcout,'k',linewidth=linew)
    ax.plot(t_out/binary_period,funcout2,'k',linewidth=linew,alpha=0.4)
    if( t_lump is not None ) : 
        ax.plot([t_lump/binary_period,t_lump/binary_period],[-100,100],'k--')

    ax2.set_xlim(1e-10,tmax/1e4)
    ax2.plot(t_out/1e4,funcout)
    ax2.cla()
    ax2.set_xlabel(r'$t \ [10^4 M]$',size=lsize)

    #########

    funcname = dirout + 'ecc_phase'+tag
    nwin += 1
    fignames = np.append(fignames,funcname)
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax2 = ax.twiny()
    
    ax.set_ylabel(r'$\mathrm{Phase} \ \mathrm{of} e_\mathrm{lump}$',size=lsize)
    ax.set_xlabel(r'$t \ [t_\mathrm{bin}]$',size=lsize)
    ax.set_ylim(0.,2.1*np.pi)
    ax.set_xlim(1e-10,tmax/binary_period)
    ax.plot(t_out/binary_period,ecc_phase,'k',linewidth=linew)
    if( t_lump is not None ) : 
        ax.plot([t_lump/binary_period,t_lump/binary_period],[-100,100],'k--')

    ax2.set_xlim(1e-10,tmax/1e4)
    ax2.plot(t_out/1e4,ecc_phase)
    ax2.cla()
    ax2.set_xlabel(r'$t \ [10^4 M]$',size=lsize)

    #########

    funcname = dirout + 'ecc_omega'+tag
    nwin += 1
    fignames = np.append(fignames,funcname)
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax2 = ax.twiny()
    
    ax.set_ylabel(r'$\omega \ \mathrm{of} e_\mathrm{lump} [\Omega_\mathrm{bin}]$',size=lsize)
    ax.set_xlabel(r'$t \ [t_\mathrm{bin}]$',size=lsize)
    ax.set_ylim(1e-10,2.)
    ax.plot(t_out/binary_period,omega2/omega_bin,'k',linewidth=linew)
    if( t_lump is not None ) : 
        ax.plot([t_lump/binary_period,t_lump/binary_period],[-100,100],'k--')

    ax2.plot(t_out/1e4,omega2/omega_bin)
    ax2.cla()
    ax2.set_xlabel(r'$t \ [10^4 M]$',size=lsize)

    #########
    
    funcname = dirout + 'ecc_fft_all'+tag
    nwin += 1
    fignames = np.append(fignames,funcname)
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax2 = ax.twiny()
    
    ax.set_xlabel(r'$\nu [\nu_\mathrm{bin}]$',size=lsize)
    ax.set_ylabel(r'$\mathrm{PSD} \ \mathrm{of} \ e_\mathrm{lump}$',size=lsize)
    ax.loglog(ecc_freqfft_all/freq_bin,ecc_fft_all,'k',linewidth=linew)
    ax2.loglog(ecc_freqfft_all,ecc_fft_all)
    ax2.cla()
    ax2.set_xlabel(r'$\nu \ [M^{-1}]$',size=lsize)
    
    #########
    if( t_lump is not None ) :
        funcname = dirout + 'ecc_fft_tlump'+tag
        nwin += 1
        fignames = np.append(fignames,funcname)
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax2 = ax.twiny()
        
        ax.set_xlabel(r'$\nu [\nu_\mathrm{bin}$',size=lsize)
        ax.set_ylabel(r'$\mathrm{PSD} \ \mathrm{of}  \  e_\mathrm{lump}$',size=lsize)
        ax.loglog(ecc_freqfft_tlump/freq_bin,ecc_fft_tlump,'k',linewidth=linew)
        ax2.loglog(ecc_freqfft_tlump,ecc_fft_tlump)
        ax2.cla()
        ax2.set_xlabel(r'$\nu \ [M^{-1}]$',size=lsize)


    #########
    
    if savepng :
        for i in np.arange(len(figs)) : 
            #figs[i].savefig('%s.png' %fignames[i], dpi=300,bbox_inches='tight')
            figs[i].savefig('%s.png' %fignames[i], dpi=300)

    if(savepdf):
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.pdf' %fignames[i], dpi=300,bbox_inches='tight')


    return

###############################################################################
###############################################################################
###############################################################################

def plot_ecc_spacetime(outfilename='ecc_analysis.h5',
                       nticks=10,
                       savepng=1,
                       cmap  = plt.cm.Spectral_r,
                       runname='inject_disk'
                      ):
    """
    plot_ecc_spacetime():
    ------------
       -- plot the eccentricity as a function of r and t;

    """

    plt.ioff()

    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    
    #################
    # Read in simulation's information: 
    #################
    pnames = ['dirname', 'gdump_name', 'dump_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'rad_freq', 'dump_freq', 'surf_name', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    surf_filename=sim_info['surf_name']+'.all.h5'
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()

    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    print(" Reading ecc data ...")
    h5f = h5py.File(full_outfilename,'r')
    func = h5f['ecc-surf-spacetime'].value
    h5f.close()

    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 
    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fsmooth = signal.convolve2d(func,sm_window,mode='same',boundary='symm')
    

    r_out /= asep
    t_out *= 1e-4
    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)
    
    r_closeup = 5.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1

    func2 = np.log(np.abs(func))
    fsmooth2 = np.log(np.abs(fsmooth))
    #maxf2 = np.nanmax(func2)
    maxf2 = -1.
    minf2 = maxf2 - 4.
    maxf  = np.exp(-1.)
    minf  = np.exp(-5.)

    nwin = 0
    fignames = []
    figs = []
    funcname = dirout + 'ecc_spacetime'

    nwin+=1
    fignames = np.append(fignames,funcname + '-lin')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,funcname + '-smooth-lin')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,fsmooth,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,funcname + '-ln')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,funcname + '-smooth-ln')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,fsmooth2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,funcname + '-ln-closeup')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,funcname + '-smooth-ln-closeup')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,fsmooth2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,funcname + '-ln-closeup2')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,funcname + '-smooth-ln-closeup2')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,fsmooth2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)

    return


###############################################################################
###############################################################################
###############################################################################

def plot_ecc_comparison(itime=0,
                        outfilename='ecc_analysis.h5',
                        savepng=1,
                        cmap  = plt.cm.Spectral_r,
                        runname='inject_disk',tag=''
                        ):
    """
    plot_ecc_comparison():
    ------------
       -- make a plot to compare the two different ways we are calculating the eccentricity

    """

    plt.ioff()

    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    
    #################
    # Read in simulation's information: 
    #################
    pnames = ['dirname', 'gdump_name', 'dump_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'rad_freq', 'dump_freq', 'surf_freq', 'surf_name']
    sim_info = get_sim_info(pnames, runname=runname)
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    timeid = '%06d' %itime    #harm HDF5 dumps

    itime_surf = np.int( itime * sim_info['dump_freq'] / sim_info['surf_freq'] )
    
    surf_filename=sim_info['surf_name']+'.all.h5'
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()

    print("comparing at time = ", t_out[itime_surf])

    slc_t = slice(itime_surf,itime_surf+1,1)
    slc_a = slice(None,None,None)
    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    print(" Reading ecc data ...")
    h5f = h5py.File(full_outfilename,'r')
    func1 = h5f['ecc-'+timeid].value
    func2 = read_3d_hdf5_slice('ecc-surf-spacetime',(slc_t,slc_a),h5f)
    h5f.close()

    r_beg = 2.*asep
    r_end = 6.*asep
    ir_beg, ir_end = find_bracket(r_beg,r_end,r_out)
    r_out2 = r_out[0:ir_end+1]
    
    r_out /= asep
    r_min = np.min(r_out)
    r_max = 5.

    maxf  = -1.
    minf  = -5.

    alpha = 0.5
    nwin = 0
    fignames = []
    figs = []
    funcname = dirout + 'ecc_comparison-' +tag + timeid

    nwin += 1
    fignames = np.append(fignames,funcname + '-lin')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'eccentriciy')
    ax.set_xlabel(r'r [a]')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(np.exp(-5.),np.exp(-1.))
    ax.plot(r_out2,func1,label='3d dump',alpha=alpha)
    ax.plot(r_out,func2,label='surf dump',alpha=alpha)
    ax.legend(loc=0)

    nwin += 1
    fignames = np.append(fignames,funcname + '-ln')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'eccentriciy')
    ax.set_xlabel(r'r [a]')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(minf,maxf)
    ax.plot(r_out2,np.log(np.abs(func1)),label='3d dump',alpha=alpha)
    ax.plot(r_out,np.log(np.abs(func2)),label='surf dump',alpha=alpha)
    ax.legend(loc=0)

    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)


    return

###############################################################################
###############################################################################
###############################################################################

def plot_ecc_timeseries(outfilename='ecc_analysis.h5',
                        savepng=True,
                        savepdf=False,
                        cmap  = plt.cm.Spectral_r,
                        runname='inject_disk',
                        tmax=None,
                        tag=''
                        ):
    """
    plot_ecc_timeseries():
    ------------
       -- make a plot of eccentricity integrated over lump region vs. time

    """

    plt.ioff()

    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    
    #################
    # Read in simulation's information: 
    #################
    pnames = ['dirname', 'gdump_name', 'dump_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'rad_freq', 'dump_freq', 'surf_freq', 'surf_name','t_lump_beg']
    sim_info = get_sim_info(pnames, runname=runname)
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)
    binary_period = 1./sim_info['freq_bin_orbit']
    t_lump = sim_info['t_lump_beg']

    surf_filename=sim_info['surf_name']+'.all.h5'
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()

    if( tmax is None ) :
        tmax = np.max(t_out)
    
    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    print(" Reading ecc data ...")
    h5f = h5py.File(full_outfilename,'r')
    func2 = h5f['ecc-surf-spacetime'].value[:]
    h5f.close()

    r_beg = 2.*asep
    r_end = 4.*asep
    ir_beg, ir_end = find_bracket(r_beg,r_end,r_out)
    r_out /= asep

    ftmp = np.mean(func2[:,ir_beg:ir_end],axis=1)
    funcout = np.log10(np.abs(ftmp))

    maxf  = -1.
    minf  = -3.

    linew = 1.1
    alpha = 0.5
    nwin = 0
    fignames = []
    figs = []
    funcname = dirout + 'ecc_timeseries'+tag
    nwin += 1
    fignames = np.append(fignames,funcname)
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax2 = ax.twiny()
    
    ax.set_ylabel(r'$\log_{10} \ e_\mathrm{lump} (2a < r < 4a)$',size=15)
    ax.set_xlabel(r'$t \ [t_\mathrm{bin}]$',size=15)
    ax.set_ylim(minf,maxf)
    ax.set_xlim(1e-10,tmax/binary_period)
    ax.plot(t_out/binary_period,funcout,'k',linewidth=linew)
    if( t_lump is not None ) : 
        ax.plot([t_lump/binary_period,t_lump/binary_period],[-100,100],'k--')

    ax2.set_xlim(1e-10,tmax/1e4)
    ax2.plot(t_out/1e4,funcout)
    ax2.cla()
    ax2.set_xlabel(r'$t \ [10^4 M]$',size=15)
    
    if savepng :
        for i in np.arange(len(figs)) : 
            #figs[i].savefig('%s.png' %fignames[i], dpi=300,bbox_inches='tight')
            figs[i].savefig('%s.png' %fignames[i], dpi=300)

    if(savepdf):
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.pdf' %fignames[i], dpi=300,bbox_inches='tight')

    return
            
###############################################################################
###############################################################################
###############################################################################

def plot_ecc_avgs(outfilename='ecc_analysis.h5',
                  savepng=1,
                  cmap  = plt.cm.Spectral_r,
                  runname='inject_disk',
                  n_orbits_avg=(4./0.28),
                  n_avgs = 5
                  ):
    """
    plot_ecc_avgs():
    ------------
       -- make a plot showing averages of the eccentricity profile over time;

    """

    plt.ioff()

    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    
    #################
    # Read in simulation's information: 
    #################
    pnames = ['dirname', 'gdump_name', 'dump_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'rad_freq', 'dump_freq', 'surf_freq', 'surf_name', 't_lump_beg','tend','freq_bin_orbit']
    sim_info = get_sim_info(pnames, runname=runname)
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    surf_filename=sim_info['surf_name']+'.all.h5'
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()

    binary_period = 1./sim_info['freq_bin_orbit']
    tend = sim_info['tend']
    t_avg = n_orbits_avg * binary_period
    n_surf = len(t_out)
    dn_surf = np.int(t_avg / sim_info['surf_freq'])
    i_avg_beg = n_avgs * dn_surf
    if( i_avg_beg > n_surf ) :
        sys.exit("Need to reduce  n_orbits_avg or n_avgs: ", n_orbits_avg, n_avgs, i_avg_beg, n_surf )
    
    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    print(" Reading ecc data ...")
    h5f = h5py.File(full_outfilename,'r')
    func2 = h5f['ecc-surf-spacetime'].value[:]
    h5f.close()

    r_out /= asep
    #r_min = np.min(r_out)
    r_min = 0.75
    r_max = 5.

    maxf  = -1.
    minf  = -5.

    alpha = 0.5
    linew = 2.
    nwin = 0
    fignames = []
    figs = []
    funcname = dirout + 'ecc_avgs' 

    nwin += 1
    fignames = np.append(fignames,funcname + '-log')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\log_{10}$ eccentricity')
    ax.set_xlabel(r'r [a]')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(minf*np.log10(np.exp(1.)),maxf*np.log10(np.exp(1.)))

    alphas = np.linspace(0.1,1.,n_avgs)
    for i_avg in np.arange(n_avgs) :
        i1 = n_surf - i_avg_beg + i_avg*dn_surf
        i2 = i1 + dn_surf 
            
        #print("averaging over ", i1, i2)
        str2 = '[{:d}, {:d}] '.format(np.int(t_out[i1]/binary_period), np.int(t_out[i2-1]/binary_period))
        str3 =  r'$t_\mathrm{bin}$'
        tlabel = str2 + str3
        favg = np.mean(func2[i1:i2,:],axis=0)
        ax.plot(r_out,np.log10(np.abs(favg)),'k',alpha=alphas[i_avg],label=tlabel,linewidth=linew)

    ax.legend(loc=1)
        
    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)

################################################################################
################################################################################

def get_tmax_of_allruns(all_runs):

    tmax = -1.
    for run in all_runs :
        sim_info=get_sim_info(['tend'],runname=run)
        tmax = np.max([tmax,sim_info['tend']])


    return tmax


# ###############################################################################
# # This allows the python module to be executed like:   
# # 
# #   prompt>  python lump
# # 
# ###############################################################################
# if __name__ == "__main__":
#     import sys
#     print(sys.argv)
#     if len(sys.argv) > 1 :
#         lump(sys.argv[1])
#     else:
#         lump()
#

all_runs = ['longer2', 'proj1_q2', 'proj1_q5', 'proj1_q10']
tmax_qsurvey = get_tmax_of_allruns(all_runs)
all_runs = ['longer2', 'medium_disk','bigger_disk', 'inject_disk']
tmax_initsurvey = get_tmax_of_allruns(all_runs)
tmax_max = np.max([tmax_initsurvey,tmax_qsurvey])


all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']

#  Make the mode_analysis.h5 files: 
if 0 : 
    #all_times = np.arange(0,301,100)
    all_times = np.arange(0,380,1)
    
    for run in all_runs :
        make_ecc_from_surface_dump(runname=run)        

        #for itime in all_times : 
            #make_ecc_from_3d_dump(runname=run,itime=itime)

if 0: 
    for run in all_runs :
        plot_ecc_avgs(runname=run)

if 0 :
    all_runs = ['longer2', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    tag='-qsurvey'
    tmax=tmax_qsurvey
    for run in all_runs :
        plot_ecc_timeseries(runname=run,tag=tag,tmax=tmax)
        plot_ecc_phase(runname=run,tag=tag,tmax=tmax)

    all_runs = ['longer2', 'medium_disk','bigger_disk', 'inject_disk']
    tag='-initsurvey'
    tmax = tmax_initsurvey
    for run in all_runs :
        plot_ecc_timeseries(runname=run,tag=tag,tmax=tmax)
        plot_ecc_phase(runname=run,tag=tag,tmax=tmax)
        
        
if 0 :
    for run in all_runs:
        make_ecc_phase(runname=run)

if 0 :
    for run in all_runs :
        plot_ecc_phase(runname=run)

                    
if 1 : 
    #all_runs = ['longer2']
    all_times = np.arange(0,301,100)
    #all_times = np.arange(0,380,1)
    
    for run in all_runs :
        #plot_ecc_spacetime(runname=run)
        for itime in all_times : 
            plot_ecc_comparison(runname=run,itime=itime,tag='-all')


