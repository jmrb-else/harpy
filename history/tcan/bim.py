"""  

  bim.py: 
  ------------
    -- display image from bothros calculation

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################
###############################################################################

# def lum(runname='longer2',savepng=0):
"""  
Primary routine for analyzing light curve. 
"""

showplots=0
savepng=1
nwin=0
dolog=0

nlev = 256
nticks=10

cmap  = plt.cm.Spectral_r
darkgrey = '#202020'
purple   = '#800080'
darkblue = '#0000A0'
Colormap.set_under(cmap,color=darkgrey) 
Colormap.set_over(cmap,color='w') 

if not showplots :
    plt.ioff()


# dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
dirout ='./'
#dumpfile = 'BOTHROS.merged.053.h5'
dumpfile='BOTHROS.AVERAGED.080.100x100.h5'
itime = 0

fdest = h5py.File(dirout+dumpfile,'r')
t_dat = fdest['times'].value 
image = fdest['I_image2'].value
idout = fdest['id'].value
imgout = image[itime,:]
tout = t_dat[itime]
pix_x = fdest['pix_x'].value[itime,:]
pix_y = fdest['pix_y'].value[itime,:]
r1outer = fdest['Header/r1outer'].value 
r_cam = fdest['Header/r_cam'].value
th_cam = fdest['Header/th_cam'].value
th_cam *= 180. / np.pi 

#length_scale = r_cam 
length_scale = 1.
pix_x  *= length_scale
pix_y  *= length_scale

#imgout = idout[itime,:]

if (dolog == 1 ) : 
    func = np.log10(np.abs(imgout))
    maxf = np.nanmax(func) 
    minf = maxf - 10. 
else : 
    func = imgout 
    maxf = np.nanmax(func) 
    minf = 1.e-10* maxf 

    

if( len(func.shape) == 1 ) : 
    nx = 2 
    npts = func.shape[0]
    if( (npts % nx) == 0 ) : 
        ny = npts / nx 
        func = np.reshape(func,(-1,2))
        pix_x = np.reshape(pix_x,(-1,2))
        pix_y = np.reshape(pix_y,(-1,2))
    else : 
        print("this is weird")
        stop


# header_dn1, geodata = read_geo_data('georay-dn-4040.dat')
# header_dn2, geodata = read_geo_data('georay-dn-4041.dat')
# header_dn3, geodata = read_geo_data('georay-dn-4140.dat')
# header_dn4, geodata = read_geo_data('georay-dn-4141.dat')
# header_up1, geodata = read_geo_data('georay-up-5840.dat')
# header_up2, geodata = read_geo_data('georay-up-5841.dat')
# header_up3, geodata = read_geo_data('georay-up-5940.dat')
# header_up4, geodata = read_geo_data('georay-up-5941.dat')
  

header1, geodata = read_geo_data('georay-1025.dat')
header2, geodata = read_geo_data('georay-1095.dat')
header3, geodata = read_geo_data('georay-8010.dat')
header4, geodata = read_geo_data('georay-8060.dat')


nwin+=1
tstring = str('%0.1f' %tout)
incstring = str('%03d' %th_cam)
fignames = [dirout + 'image'+'-'+tstring+'-'+incstring]
print( "fignames = ", fignames)
figs = [plt.figure(nwin)]
figs[-1].clf()
ax = figs[-1].add_subplot(111)
ax.set_title(r'Image'+'   t='+tstring)
ax.set_xlabel(r'x [M]')
ax.set_ylabel(r'y [M]')

# plt.axis([1.e-4,sim_info['tend']*1e-4,0.0,6.])

levels = np.linspace(minf,maxf,nlev)
rticks = np.linspace(minf,maxf,nticks)

pix_x *= -1.
pix_y *= -1.

CP = ax.contourf(pix_x, pix_y,func,levels,cmap=cmap,extend='both')

ax.autoscale(False)
ax.plot(-header1['x_cen'],-header1['y_cen'],'ro')
ax.plot(-header2['x_cen'],-header2['y_cen'],'yo')
ax.plot(-header3['x_cen'],-header3['y_cen'],'go')
ax.plot(-header4['x_cen'],-header4['y_cen'],'bo')

figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

fdest.close()

if savepng :
    for i in np.arange(len(figs)) : 
        figs[i].savefig('%s.png' %fignames[i], dpi=300)
        
if showplots : 
    plt.show()
        
        
print("All done ")
sys.stdout.flush()

#  
#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
