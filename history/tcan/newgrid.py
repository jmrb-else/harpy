###############################################################################
###############################################################################
#  exploring grid setups
###############################################################################
###############################################################################

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re 

from IPython.core.debugger import Tracer

SMALL = 1e-14

###############################################################################
###############################################################################
def PERIODIC_SHIFT(x,x0,xn):

    xout = np.where( (x < x0), ( 1.+( (np.abs((x)-(x0))/((xn)-(x0))).astype(int) ) ), (-(  (np.abs((x)-(x0))/((xn)-(x0))).astype(int) )) )

    return xout 

###############################################################################
###############################################################################
def PERIODIC(x,x0,xn):

    return ( (x) + ((xn)-(x0))*(PERIODIC_SHIFT((x),(x0),(xn))) )


###############################################################################
###############################################################################
def seesaw(x, x0, xn): 

  xn2 = 2*xn - x0
  x1 = PERIODIC(x,x0,xn2)

  xout = np.where( (x1 < xn),  (x1-x0), (xn2-x1) )

  return ( xout ) 


###############################################################################
###############################################################################

def diag3_x2( xp2, h_slope, th_cutout, diag3_exponent ):

    diag3_factor   = 1. - 2.*th_cutout/np.pi - h_slope
    
    ftmp = 2 * seesaw(xp2,0.,1.) - 1.
    x2  = 0.5*np.pi*( 1. + h_slope*ftmp  + diag3_factor*( ftmp**diag3_exponent) )

    return (x2) 
    

###############################################################################
###############################################################################

def make_xp_array( Ncells, pos='CENTER', NG=3, GridLength=1., include_ghost_cells=True ):

    dxp2 = GridLength/Ncells
    
    if( include_ghost_cells ) :
        n2tot = Ncells + 2*NG
        xp2_beg = 0. - NG*dxp2
        xp2_end = 1. + NG*dxp2
    else :
        n2tot = Ncells
        xp2_beg = 0.
        xp2_end = 1.

    if( pos == 'FACE' or pos == 'CORNER' ) :
        xp2 = np.linspace( xp2_beg, xp2_end, n2tot+1 )
    elif( pos == 'CENTER' ) :
        xp2 = np.linspace( (xp2_beg+0.5*dxp2), (xp2_end - 0.5*dxp2), n2tot )

    return (xp2) 
    

###############################################################################
###############################################################################
def setup_grid(N2=160,h_slope=0.13,diag3_exponent=9,th_cutout=1.e-15,hor=0.3,
               r_hor=1.5,cour=0.45,
               t_sim=7.2e4,ncells_per_core=(20.*20.*32.),plot_ghost_cells=True,
               zc_rate=(1.15*8.e4),tag=''):


    xp2_corners = make_xp_array(N2,pos='CORNER',include_ghost_cells=plot_ghost_cells)
    xp2_centers = make_xp_array(N2,pos='CENTER',include_ghost_cells=plot_ghost_cells)

    x2_corners = diag3_x2(xp2_corners, h_slope, th_cutout, diag3_exponent) 
    x2_centers = diag3_x2(xp2_centers, h_slope, th_cutout, diag3_exponent)

    x2_2 = np.roll(x2_corners,1)
    dx2 = x2_corners - x2_2
    dx2_centers = dx2[1:]
    print("dx2 contrast = ", np.max(dx2)/np.min(dx2))
    print("dx2 min max = ", np.min(dx2),np.max(dx2))

    print("len(dx2_centers) = ", len(dx2_centers))
    print("len(xp2_centers) = ", len(xp2_centers))
    print("len(x2_centers) = ", len(x2_centers))
    print("len(x2_corners) = ", len(x2_corners))

    nwin = 0
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,x2_centers,'.')
    ax.plot([0.,0.],[0,np.pi])
    ax.plot([1.,1.],[0,np.pi])
    ax.plot([0,1],[0.,0.])
    ax.plot([0,1],[np.pi,np.pi])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\theta$')
    fig.savefig(tag+"x2.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,np.log10(dx2_centers))
    ax.plot([0.,0.],[-30.,1.])
    ax.plot([1.,1.],[-30.,1.])
    ax.set_ylim([-6,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\log \Delta \theta$')
    fig.savefig(tag+"dx2-log.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,(dx2_centers))
    ax.plot([0.,0.],[0.,1.])
    ax.plot([1.,1.],[0.,1.])
    #ax.set_ylim([-6,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\theta$')
    fig.savefig(tag+"dx2-lin.png", dpi=300)

    return

###############################################################################
###############################################################################
#  int_step():
#  ------------------
#  -- calculates the integral of step() and x*step()   used for COORD_DIAGONAL2
#  -- assumes that the step function is   atan(s*(x/x0-1))/np.pi + 0.5
###############################################################################
def int_step(xtmp, x01, s1):

    x1 = PERIODIC(xtmp,xi_diag2[0],xi_diag2[-1]) 


    intstep = (s*(np.pi*x + 2*t1*t4) - x0*np.log(t7*t1*t1 + t6))*t5
  
    intstepx = (2*t4*(t6 + t7*t1*t2) + s*(t1*(-2*x0 + np.pi*s*t2) -
                                          2*t6*np.log(1. + t3*t3)))*t5*t5*np.pi
  
    return intstep, instepx 



###############################################################################
###############################################################################
#   x2_of_xp2_diag2(): 
#  ------------------
#   -- Returns the non-normalized value of the coordinate function for COORD_DIAGONAL2
#   -- makes sure that the discretization is periodic without making x2 periodic
#   -- assumes equatorial symmetry
#   -- most *_diag2 arrays have length  n_diag2_lines except xi_diag2 has length (n_diag2_lines+1)
###############################################################################
def x2_of_xp2_diag2(xp2o, ai_diag2, bi_diag2, si_diag2, xi_diag2, len_diag2):

    
    xp2 = PERIODIC(xp2o,xi_diag2[0],xi_diag2[-1])

    nx = len(xp2)
    ns = len(si_diag2)
    
    x0 = np.outer(np.ones(nx), xi_diag2[0:-1]   )
    s  = np.outer(np.ones(nx), si_diag2   )
    ai = np.outer(np.ones(nx), ai_diag2   )
    bi = np.outer(np.ones(nx), bi_diag2   )
    x  = np.outer(        xp2, np.ones(ns))

    t1 = x - x0
    t2 = x + x0
    t3 = s*t1/x0
    t4 = np.arctan(t3)
    t5 = 0.5/(np.pi*s)
    t6 = x0*x0
    t7 = s*s
    
    intstep = (s*(np.pi*x + 2*t1*t4) - x0*np.log(t7*t1*t1 + t6))*t5
  
    intstepx = (2*t4*(t6 + t7*t1*t2) 
	        + s*(t1*(-2*x0 + np.pi*s*t2) - 2*t6*np.log(1. + t3*t3))
    )*t5*t5*np.pi


    ftmp = ai*intstepx + bi*intstep
    fout = np.sum(ftmp,axis=1)
    fout -=  len_diag2 * PERIODIC_SHIFT(xp2o,xi_diag2[0],xi_diag2[-1]) 
    
    return fout

###############################################################################
###############################################################################
#  calc_ai_bi_diag2()
# ------------------
#  --  Calculates the coefficients on the step functions that 
#       result in a step-wise set of continuous linear curves. 
#       (see comments in setup_diag2() for more details)
#
#  --  We need only the slopes (ci), the locations of the 
#       intersections (xi), and the value of the function 
#       at x=xi[0]  (d0).   Calculates  ai,bi,di  
#   
#  -- Assumes that all memory has been allocated
#
################################################################################
def calc_ai_bi_diag2(ci_diag2, di_diag2): 

    nd = len(ci_diag2)
    ai_diag2 = np.zeros(nd)
    bi_diag2 = np.copy(ai_diag2)

    eij = np.zeros((nd,nd))
    eij[range(nd),range(nd)] = 1.
    eij[range(1,nd),range(0,nd-1)] = -1.

    ai_diag2 = np.dot(eij, ci_diag2)
    bi_diag2 = np.dot(eij, di_diag2)

    return ai_diag2, bi_diag2 

###############################################################################
###############################################################################
#  calc_ai_bi_di_diag2()
# ------------------
#  --  Calculates the coefficients on the step functions that 
#       result in a step-wise set of continuous linear curves. 
#       (see comments in setup_diag2() for more details)
#
#  --  We need only the slopes (ci), the locations of the 
#       intersections (xi), and the value of the function 
#       at x=xi[0]  (d0).   Calculates  ai,bi,di  
#   
#  -- Assumes that all memory has been allocated
#
###############################################################################
def calc_ai_bi_di_diag2(d0_diag2, ci_diag2, xi_diag2, di_diag2=None):


    if( di_diag2 is None ) : 
        di_diag2 = np.zeros(len(ci_diag2))

        # Adjust y-intercept to x0 
        di_diag2[0] = d0_diag2 - ci_diag2[0]*xi_diag2[0]  

        # Set di[] so that the lines are continuous at xi
        di_diag2[1:] = di_diag2[0:-1] + xi_diag2[1:-1]*(ci_diag2[0:-1] - ci_diag2[1:])

        

    ai_diag2, bi_diag2 = calc_ai_bi_diag2(ci_diag2, di_diag2)


    return ai_diag2, bi_diag2, di_diag2 


###############################################################################
###############################################################################
#  setup_diag2():
# -----------------------
#  -- sets auxiliary constants needed for COORD_DIAGONAL2
#  -- The coordinates x2 are defined by their discretization (dx2) as a function of xp2,
#  -- dx2 is a set of C^\infinity piecewise-linear functions that are joined by a 
#     continuous representation of the step function (to make the function differentiable)
#  -- There can be n_diag2_lines, with slope ci_diag2[] and "y-intercepts" di_diag2[] 
#  -- ai_diag2[] and bi_diag2[] are derived quantities that are just the coefficients on the 
#      linear and constant parts of each segment.  In other words, are desired function is 
# 
#      f(x) =   c_i x + d_i    if   x_i < x < x_{i+1} 
#
#      and we derive a differentiable version of this this function by matching to 
# 
#      g(x) = \sum_i  (a_i x + b_i)*step(x,x_i,s_i)  
#
#      where step(x,x_i,s_i) is our differentiable representation of the step function 
#        going from 0 to 1  at x=x_i with "strength" s_i  that is, the larger s_i 
#        is the more like a step function step() is 
#    
#   -- ai,bi are easily found to be linear combinations of ci,di respectively 
#        (see calc_ai_bi_di_diag2())
#  
###############################################################################
def setup_diag2(d0_diag2, ci_diag2, xi_diag2, si_diag2, th_length, di_diag2=None): 


    ai_diag2, bi_diag2, di_diag2  = calc_ai_bi_di_diag2(d0_diag2, ci_diag2, xi_diag2, di_diag2)

    # /* Need to renormalize coordinates to desired range : 
    # -- move boundaries just within true boundaries since we remap xp2 to always lie within 
    #  x0 and xn
    len_diag2 = 0.
    
    beg_diag2 = x2_of_xp2_diag2([xi_diag2[ 0]*(1.+SMALL)], ai_diag2, bi_diag2, si_diag2, xi_diag2, len_diag2)
    end_diag2 = x2_of_xp2_diag2([xi_diag2[-1]*(1.-SMALL)], ai_diag2, bi_diag2, si_diag2, xi_diag2, len_diag2)
    len_diag2 = end_diag2 - beg_diag2
    norm_diag2 = th_length/len_diag2

    #/* Because of how the step function is defined, we need to make sure that none of the 
    #  transition locations are at  x=0   */

    xi_diag2[np.abs(xi_diag2) < SMALL ] = SMALL

    print("xi_diag2 = ", xi_diag2)
    print("ai_diag2 = ", ai_diag2)
    print("bi_diag2 = ", bi_diag2)
    print("ci_diag2 = ", ci_diag2)
    print("di_diag2 = ", di_diag2)

    return xi_diag2, ai_diag2, bi_diag2, di_diag2, len_diag2, beg_diag2, end_diag2, norm_diag2


###############################################################################
###############################################################################
def x2_diag2(xp2o, ai_diag2, bi_diag2, si_diag2, xi_diag2, len_diag2, beg_diag2, norm_diag2):

    x2_tmp = x2_of_xp2_diag2(xp2o, ai_diag2, bi_diag2, si_diag2, xi_diag2, len_diag2)

    x2_final = (x2_tmp - beg_diag2) * norm_diag2

    return x2_final


###############################################################################
###############################################################################
def setup_grid2(N2=160,th_cutout=1.e-15,hor=0.3,th_length=np.pi, 
               r_hor=1.5,cour=0.45,
               t_sim=7.2e4,ncells_per_core=(20.*20.*32.),plot_ghost_cells=True,
               zc_rate=(1.15*8.e4),tag=''):


    if 1 : 
        #/* Collection of fewer straight lines with smoother transitions: 
        #  (si[1] was tuned so that the dtheta is approximately symmetric about equator) */
        n_diag2_lines = 4  # /* Number of segments */

        xi_diag2 = np.zeros(n_diag2_lines+1)
        ci_diag2 = np.zeros(n_diag2_lines  )
        di_diag2 = np.zeros(n_diag2_lines  )
        si_diag2 = np.zeros(n_diag2_lines  )
        
        #/* Locations of transitions : (assume xp2 goes from 0 to 1 , use th_length to scale thinsg) */
        xi_diag2[0] = SMALL
        xi_diag2[1] = (4./N2) 
        xi_diag2[2] = 0.5 
        xi_diag2[3] = 2*xi_diag2[2] - xi_diag2[1]
        xi_diag2[4] = 2*xi_diag2[2] - xi_diag2[0]

        # /* Constant values of d^2 th / dx2^2   */
        ci_diag2[0] =  0.
        ci_diag2[1] = -0.25
        ci_diag2[2] = -ci_diag2[1]
        ci_diag2[3] = -ci_diag2[0]

        di_diag2[0] =  1.4   
        di_diag2[1] =  0.25  
        di_diag2[2] =  di_diag2[1]+2*xi_diag2[2]*ci_diag2[1]  
        di_diag2[3] =  di_diag2[0]
        
        si_diag2[0] =  1.e4
        si_diag2[1] =  4.893617
        si_diag2[2] = si_diag2[1]
        si_diag2[3] = si_diag2[2]

        d0_diag2      = 0.

    if 0 : 
        n_diag2_lines = 6      # /* Number of segments */
        d0_diag2      = 0.085  # /* Sets the floating scale of the discretization, similar to the value of dx2 at xp2=xi_xp2[0] */

        xi_diag2 = np.zeros(n_diag2_lines+1)
        ci_diag2 = np.zeros(n_diag2_lines  )
        di_diag2 = np.zeros(n_diag2_lines  )
        si_diag2 = np.zeros(n_diag2_lines  )

        #/* Locations of transitions : (assume xp2 goes from 0 to 1 , use th_length to scale thinsg) */
        xi_diag2[0] = SMALL
        xi_diag2[1] = (2./N2) 
        xi_diag2[2] = 4. * xi_diag2[1]
        xi_diag2[3] = 0.5 
        xi_diag2[4] = 2*xi_diag2[3] - xi_diag2[2]
        xi_diag2[5] = 2*xi_diag2[3] - xi_diag2[1]
        xi_diag2[6] = 2*xi_diag2[3] - xi_diag2[0]
        
        # /* Constant values of d^2 th / dx2^2   */
        ci_diag2[0] =  0.
        ci_diag2[1] = -2.
        ci_diag2[2] = -0.02 
        ci_diag2[3] = -ci_diag2[2]
        ci_diag2[4] = -ci_diag2[1]
        ci_diag2[5] = -ci_diag2[0] 

        si_diag2[:] = 1.e4 

        di_diag2 = None




    xi_diag2, ai_diag2, bi_diag2, di_diag2, len_diag2, beg_diag2, end_diag2, norm_diag2 = setup_diag2(d0_diag2, ci_diag2, xi_diag2, si_diag2, th_length, di_diag2)

      
    xp2_corners = make_xp_array(N2,pos='CORNER',include_ghost_cells=plot_ghost_cells)
    xp2_centers = make_xp_array(N2,pos='CENTER',include_ghost_cells=plot_ghost_cells)

    x2_corners = x2_diag2(xp2_corners, ai_diag2, bi_diag2, si_diag2, xi_diag2, len_diag2, beg_diag2, norm_diag2)
    x2_centers = x2_diag2(xp2_centers, ai_diag2, bi_diag2, si_diag2, xi_diag2, len_diag2, beg_diag2, norm_diag2)


    x2_2 = np.roll(x2_corners,1)
    dx2 = x2_corners - x2_2
    dx2_centers = dx2[1:]
    print("dx2 contrast = ", np.max(dx2)/np.min(dx2))
    print("dx2 min max = ", np.min(dx2),np.max(dx2))

    print("len(dx2_centers) = ", len(dx2_centers))
    print("len(xp2_centers) = ", len(xp2_centers))
    print("len(x2_centers) = ", len(x2_centers))
    print("len(x2_corners) = ", len(x2_corners))

    nwin = 0
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,x2_centers,'.')
    ax.plot([0.,0.],[0,np.pi])
    ax.plot([1.,1.],[0,np.pi])
    ax.plot([0,1],[0.,0.])
    ax.plot([0,1],[np.pi,np.pi])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\theta$')
    fig.savefig(tag+"x2.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,np.log10(dx2_centers))
    ax.plot([0.,0.],[-30.,1.])
    ax.plot([1.,1.],[-30.,1.])
    ax.set_ylim([-6,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\log \Delta \theta$')
    fig.savefig(tag+"dx2-log.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,(dx2_centers))
    ax.plot([0.,0.],[0.,1.])
    ax.plot([1.,1.],[0.,1.])
    #ax.set_ylim([-6,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\theta$')
    fig.savefig(tag+"dx2-lin.png", dpi=300)

    return




###########################

#setup_grid(h_slope=0.1,diag3_exponent=21,N2=180,hor=0.3,tag='diag3-')

setup_grid2(N2=180,tag='diag2-', plot_ghost_cells=False)


