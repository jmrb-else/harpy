"""  

  ball.py: 
  ------------
    -- script for making a series of images from a MERGED bothros file; 

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################



file ="../all_frames.h5"

# F_circumbinary           Dataset {182, 100}
# F_minidisks              Dataset {182, 100}
# F_tot                    Dataset {182, 100}
# Header                   Group
# I_image                  Dataset {182, 100, 500, 500}
# I_image2                 Dataset {182, 100, 500, 500}
# freq                     Dataset {182, 100}
# id                       Dataset {1, 500, 500}
# pix_area                 Dataset {1, 500, 500}
# pix_x                    Dataset {1, 500, 500}
# pix_y                    Dataset {1, 500, 500}
# snap_id                  Dataset {182, 100}

ifreq = 72
nt = 181

min_im=1e-8
max_im=1e-3

min_tau=1e-10
max_tau=2.


if 1 : 
    for itime in np.arange(nt) : 
#    for itime in [60,100,181] : 
        make_image(filename=file,itime=itime,dolog=True,tag='-emiss',ifreq=ifreq,minf=min_im,maxf=max_im)
        make_image(funcname='I_image2',filename=file,itime=itime,dolog=False,tag='-tau',ifreq=ifreq,minf=min_tau,maxf=max_tau)



#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
