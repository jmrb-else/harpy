"""  

  debug.py: 
  ------------
    -- make plots from ascii data;

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import string 
#import h5py

from astropy.io import ascii

# from IPython.core.debugger import Tracer

###############################################################################
###############################################################################
def make_cont(xout,yout,fout,
              filename='imgout',xlabel='x [M]',ylabel='y [M]',nlev=256,nticks=10,
              dolog=False,maxf=None,minf=None, cmap=plt.cm.Spectral_r,
              use_imshow=False,
              overlay_data=None,
              bbox=None,
              title=None,
              line1=None,
              line2=None,
              line3=None,
              line4=None) :


    if (dolog) : 
        func = np.log10(np.abs(fout))
        if maxf is None : 
            maxf = np.nanmax(fout) 
        else :
            maxf = np.log10(maxf)

        if minf is None :
            minf = maxf - 6. 
        else :
            minf = np.log10(minf)

        loc = np.logical_not(np.isfinite(fout))
        func[loc] = minf

        if( overlay_data is not None ) :
            func2 = np.log10(np.abs(overlay_data[2]))
            loc = np.logical_not(np.isfinite(func2))
            func2[loc] = minf

    else : 
        func = fout
        if maxf is None : 
            maxf = np.nanmax(fout) 
        if minf is None :
            minf = 1.e-6* maxf 

        if( overlay_data is not None ) :
            func2 = overlay_data[2]
    

    nwin=1
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    levels = np.linspace(minf,maxf,nlev)
    rticks = np.linspace(minf,maxf,nticks)

    ax.set_aspect('equal')

    if( bbox is not None ):
        ax.set_xlim(bbox[0],bbox[2])
        ax.set_ylim(bbox[1],bbox[3])
        

    if( use_imshow ) :
        CP = ax.imshow(np.transpose(func),vmin=np.min(levels),vmax=np.max(levels),cmap=cmap,
                       interpolation='none',origin='lower',
                       extent=[np.min(xout),np.max(xout),np.min(yout),np.max(yout)])
        
    else :
        CP = ax.contourf(xout, yout,func,levels,cmap=cmap,extend='both')


    if( overlay_data is not None ) :
        CP2 = ax.contourf(overlay_data[0],overlay_data[1],overlay_data[2],levels,cmap=cmap,extend='both')
        
    if( line1 is not None ) :
        print("making line1" )
        ax.plot(line1[0],line1[1],linewidth=0.1)

    if( line2 is not None ) :
        print("making line2" )
        ax.plot(line2[0],line2[1],linewidth=0.1)

    if( line3 is not None ) :
        print("making line3" )
        ax.plot(line3[0],line3[1],linewidth=0.1)

    if( line4 is not None ) :
        print("making line4" )
        ax.plot(line4[0],line4[1],linewidth=0.1)

    if title is not None : 
        ax.set_title('%s' %title)
        
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    figs[-1].savefig('%s.png' %filename, dpi=300)



    return

###############################################################################
###############################################################################
###############################################################################

def plotframe(itime=0,funcname='rho',minf=None,maxf=None,dolog=False,plotcflag=False):
    """  
    Primary routine for analyzing light curve. 
    """
    
    indexname_out = ['k','j']
    prefix = 'SOD.'
    dirout ='./plots/'
    dirdumps = './dumps/'
    n_patches = 2
    NG = 3
    indout = [10+NG,5+NG]
    
    n1_0 = 100+2*NG
    n2_0 = 100+2*NG
    n1_1 = 100+2*NG
    n2_1 = 100+2*NG
    
    plt.ioff()
    
    
    itime_str = str('%06d' %itime)
    
    i_patch_strs = []
    dumpfiles = []
    patchfile_pre = 'patch_'
    
    for i in np.arange(n_patches) :
        i_patch_strs = np.append(i_patch_strs,str('%04d' %i))
        dumpfiles = np.append(dumpfiles,(dirdumps+prefix+i_patch_strs[i]+'.'+itime_str+'.dat'))
    
   
    print("dumpfiles = ", dumpfiles)
    
    dumpdata0 = ascii.read(dumpfiles[0],format='commented_header')
    dumpdata1 = ascii.read(dumpfiles[1],format='commented_header')

    print('Finished reading data')
    print('shape of dumpdata0 = ', len(dumpdata0))
    print('shape of dumpdata1 = ', len(dumpdata1))
    print('shape x0 = ', dumpdata0['x'].shape)
    print('shape x1 = ', dumpdata1['x'].shape)
    #print('x0 = ', dumpdata0['x'])
    #print('x1 = ', dumpdata1['x'])
    
    figname = dirout + funcname + '-' + itime_str
    ipatch=0
    loc0 = (dumpdata0[indexname_out[ipatch]]==indout[ipatch]).flat
    
    ipatch=1                
    loc1 = (dumpdata1[indexname_out[ipatch]]==indout[ipatch]).flat
    
    ipatch=0
    xout0 = dumpdata0['x'].compress(loc0).reshape(n1_0,n2_0)
    yout0 = dumpdata0['y'].compress(loc0).reshape(n1_0,n2_0)
    func0 = dumpdata0[funcname].compress(loc0).reshape(n1_0,n2_0)
    
    ipatch=1
    xout1 = dumpdata1['x'].compress(loc1).reshape(n1_1,n2_1)
    yout1 = dumpdata1['y'].compress(loc1).reshape(n1_1,n2_1)
    func1 = dumpdata1[funcname].compress(loc1).reshape(n1_1,n2_1)
    
    print('xout0 shape = ', xout0.shape)
    print('yout0 shape = ', yout0.shape)
    print('func0 shape = ', func0.shape)
    
    print('xout1 shape = ', xout1.shape)
    print('yout1 shape = ', yout1.shape)
    print('func1 shape = ', func1.shape)

    delta = 0.2*(np.max(xout1) - np.min(xout1))
    bbox = [np.min(xout1)-delta,np.min(yout1)-delta,np.max(xout1)+delta,np.max(yout1)+delta]
    
    make_cont(xout0,yout0,func0,filename=figname+'-p0',dolog=dolog,minf=minf,maxf=maxf,
              bbox=bbox,
              line1=[xout1[0,:],yout1[0,:]],
              line2=[xout1[NG,:],yout1[NG,:]],
              line3=[xout1[-NG,:],yout1[-NG,:]],
              line4=[xout1[-1,:],yout1[-1,:]])

    make_cont(xout0,yout0,func0,filename=figname+'-both',dolog=dolog,minf=minf,maxf=maxf,
              bbox=bbox,
              overlay_data=[xout1,yout1,func1],
              line1=[xout1[0,:],yout1[0,:]],
              line2=[xout1[NG,:],yout1[NG,:]],
              line3=[xout1[-NG,:],yout1[-NG,:]],
              line4=[xout1[-1,:],yout1[-1,:]])

    make_cont(xout1,yout1,func1,filename=figname+'-p1',dolog=dolog,
              minf=minf,maxf=maxf,bbox=bbox)

    
    if( plotcflag ) :
#        cflagfile = dirdumps+prefix+'cover_flags.'+itime_str+'.dat'
        funcname='cover_flags'
        cflagdata = dumpdata0
        figname = dirout + funcname + '-' + itime_str
        ipatch=0
        n1_c = n1_0
        n2_c = n2_0
        loc0 = (cflagdata[indexname_out[ipatch]]==indout[ipatch]).flat
        xout0 = cflagdata['x'].compress(loc0).reshape(n1_c,n2_c)
        yout0 = cflagdata['y'].compress(loc0).reshape(n1_c,n2_c)
        func0 = cflagdata[funcname].compress(loc0).reshape(n1_c,n2_c)
        make_cont(xout0,yout0,func0,filename=figname+'-p0',dolog=0,minf=-1.,maxf=3.,
                  line1=[xout1[0,:],yout1[0,:]],
                  line2=[xout1[NG,:],yout1[NG,:]],
                  line3=[xout1[-NG,:],yout1[-NG,:]],
                  line4=[xout1[-1,:],yout1[-1,:]],use_imshow=True)
    
    
    
    print("All done ")
    sys.stdout.flush()
    return

#  

#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#

ibeg = 41
iend = 185
istep = 1

minf=1.25e4
maxf=1.e5
funcname='rho'
dolog=True
#for i  in np.arange(0,10) : 
for i  in np.arange(ibeg,iend,istep) : 
    plotframe(itime=i,funcname=funcname,minf=minf,maxf=maxf,dolog=dolog,plotcflag=True)


minf=-3.e-4
maxf=4.e-3
funcname='ucon1'
dolog=False
for i  in np.arange(ibeg,iend,istep) : 
    plotframe(itime=i,funcname=funcname,minf=minf,maxf=maxf,dolog=dolog)

