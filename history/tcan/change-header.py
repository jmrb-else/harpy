"""

  change-bfield.py: 
  ------------

    -- divides the B-field by the gdet in the hdf5 file, so that we
       can use different metrics using the same snapshot data and same
       grid while not introducing monopoles;


"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import numpy as np
import os,sys
import h5py
import glob
import string as str

#######################################################################

print(sys.argv)
if len(sys.argv) > 2 :
    file_patch0 = sys.argv[1]
    file_patch1 = sys.argv[2]
else : 
    sys.exit('Need a file names !!  Exitting!!')
    
#######################################################################
# Open the file in read/write access, will modify the data "in place" 
#######################################################################
patch0  = h5py.File(file_patch0,'r+')
patch1  = h5py.File(file_patch1,'r+')

funclist_from_p0 = ['/Header/Grid/bh1_traj1',
                    '/Header/Grid/bh1_traj2',
                    '/Header/Grid/bh2_traj1',
                    '/Header/Grid/bh2_traj2',
                    '/Header/Grid/dx0',
                    '/Header/IO/DT_out0',
                    '/Header/IO/DT_out1',
                    '/Header/IO/DT_out2',
                    '/Header/IO/DT_out3',
                    '/Header/IO/DT_out4',
                    '/Header/IO/DT_out5',
                    '/Header/IO/DT_out6',
                    '/Header/IO/DT_out7',
                    '/Header/IO/DT_out8',
                    '/Header/IO/DT_out9',
                    '/Header/IO/T_out0',
                    '/Header/IO/T_out1',
                    '/Header/IO/T_out2',
                    '/Header/IO/T_out3',
                    '/Header/IO/T_out4',
                    '/Header/IO/T_out5',
                    '/Header/IO/T_out6',
                    '/Header/IO/T_out7',
                    '/Header/IO/T_out8',
                    '/Header/IO/T_out9',
                    '/Header/IO/N_out0',
                    '/Header/IO/N_out1',
                    '/Header/IO/N_out2',
                    '/Header/IO/N_out3',
                    '/Header/IO/N_out4',
                    '/Header/IO/N_out5',
                    '/Header/IO/N_out6',
                    '/Header/IO/N_out7',
                    '/Header/IO/N_out8',
                    '/Header/IO/N_out9']


funclist_from_p1 = [ '/Header/IO/DT_out10',
                     '/Header/IO/DT_out11',
                     '/Header/IO/T_out10',
                     '/Header/IO/T_out11',
                     '/Header/IO/N_out10',
                     '/Header/IO/N_out11',
                     '/Header/IO/N_OUT_TYPES']


for func in funclist_from_p0 :
    print(" func = ", func )
    patch1[func][...] = patch0[func]

for func in funclist_from_p1 :
    print(" func = ", func )
    patch0[func][...] = patch1[func]


patch0.close()
patch1.close()

