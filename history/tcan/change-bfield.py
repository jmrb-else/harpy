"""

  change-bfield.py: 
  ------------

    -- divides the B-field by the gdet in the hdf5 file, so that we
       can use different metrics using the same snapshot data and same
       grid while not introducing monopoles;


"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import numpy as np
import os,sys
import h5py
import glob
import string as str

#######################################################################

print(sys.argv)
if len(sys.argv) > 1 :
    filename = sys.argv[1]
else : 
    sys.exit('Need a file name!!  Exitting!!')
    
#######################################################################
# Open the file in read/write access, will modify the data "in place" 
#######################################################################
h5in  = h5py.File(filename,'r+')
gdet = h5in['gdet']

for func in ['B1','B2','B3'] : 
    print(" func = ", func )
    dat_tmp =  h5in[func] 
    dat_tmp[...] *= gdet 
    #h5in.copy(func,h5out)

h5in.close()

