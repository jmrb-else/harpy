import numpy as np
import os,sys
import h5py
from harm import *

###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python search_h5_for_nans.py  [filename] 
# 
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) > 1 :
        filename = sys.argv[1]
    else:
        filename = 'rdump_start.h5'

    fh5 = h5py.File(filename, 'r')
    search_all_datasets_for_nans(fh5)
    fh5.close()
