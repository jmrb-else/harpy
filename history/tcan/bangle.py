"""  

  bangle.py: 
  ------------
    -- script for plotting dL/d\cos(\theta)dphi

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal
from scipy.interpolate import interp1d

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################

def make_dl_dth(second_orbit=True):

    dirout = './'

    if( second_orbit ) :
        file='light_curve_2nd_merged.h5'
        tag='_2nd'
    else : 
        file='light_curve_3rd_merged.h5'
        tag='_3rd'


    h5file = h5py.File(file,'r')
    flux_tot = h5file['flux_tot'].value
    phi      = h5file['phi'].value
    theta    = h5file['theta'].value
    r_cam    = h5file['/Header/r_cam'].value[0]
    freq     = h5file['freq'].value[0]
    tout     = h5file['times'].value
    h5file.close()

    n_theta = len(theta)
    costh = np.cos(theta*np.pi/180.)
    dcosth = np.zeros(n_theta)
    
    for i in np.arange(1,n_theta-1) :
        dcosth[i] = 0.5*( costh[i+1]  - costh[i-1] )

    i = 0 
    dcosth[i] = 0.5*( costh[i+1]  - costh[i] )
    i = n_theta-1
    dcosth[i] = 0.5*( costh[i]  - costh[i-1] )

    dcosth = np.abs(dcosth)
    print("int dcosth = ", np.sum(dcosth) )
    
    dphi = phi[1] - phi[0]
    dphi *= 2.*np.pi/360. 
    dl_dth = np.sum(flux_tot,axis=1) * dphi

    print("dphi = ", dphi)
    print("flux shape = ", flux_tot.shape)

    #dl_dth /= dcosth
    C_G      =  (6.6742e-8                   ) #/* Gravitational constant */
    C_c      =  (2.99792458e10               ) #/* speed of light */
    C_mSUN   =  (1.99e33                     ) #/* solar mass */
    C_h      =  (6.6260693e-27               ) #/* Planck constant */

    Theta_kt = 0.2
    mbh = 10.**6 * C_mSUN
    rscale = r_cam * mbh * C_G / (C_c**2)
    area_factor = rscale**2

    h_nu_Theta = C_h * freq / Theta_kt 
    W_nu = h_nu_Theta**(-0.5) * np.exp(-h_nu_Theta)
    A_factor = Theta_kt * np.sqrt(np.pi) / C_h 
    bol_factor = A_factor / W_nu

    print("rscale = ", rscale)
    print("area_factor = ", area_factor)

    dl_dth *= area_factor * bol_factor

    print("dl shape = ", dl_dth.shape)
    nt = dl_dth.shape[1]
    
    cm = plt.get_cmap('gist_rainbow',lut=nt)

    nwin=0
    fignames = [dirout + 'dl_dcosth'+tag]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'$\theta_\mathrm{cam}$ [deg.]')
    ax.set_ylabel(r'dL / d$\cos(\theta)$  [erg s$^{-1}$ rad$^{-1}$]')
    ax.set_ylim(0.42e43,0.85e43)
    for i in range(nt):
        lines = ax.plot(theta, dl_dth[:,i])
        lines[0].set_color(cm(i))
        lines[0].set_label(str('t =%5.0f M' %np.int(tout[i])))
        #lines = ax.plot(np.arange(10)*(i+1))
        #lines[0].set_linewidth(i%3 + 1)

    ax.legend(loc=0)
    
    #plt.colorbar()

    
    for i in np.arange(len(figs)) : 
        figs[i].savefig('%s.pdf' %fignames[i], dpi=300,bbox_inches='tight')
    
    print("All done ")
    sys.stdout.flush()
    
    return


###############################################################################
###############################################################################

def make_dl_domega(second_orbit=True,itime=5):

    
    dirout = './'

    if( second_orbit ) :
        file='light_curve_2nd_merged.h5'
        tag='_2nd'
    else : 
        file='light_curve_3rd_merged.h5'
        tag='_3rd'


    h5file = h5py.File(file,'r')
    flux_tot = h5file['flux_tot'].value[:,:,itime]
    phi      = h5file['phi'].value
    theta    = h5file['theta'].value
    r_cam    = h5file['/Header/r_cam'].value[0]
    freq     = h5file['freq'].value[0]
    tout     = h5file['times'].value
    h5file.close()

    n_theta = len(theta)
    costh = np.cos(theta*np.pi/180.)
    
    C_G      =  (6.6742e-8                   ) #/* Gravitational constant */
    C_c      =  (2.99792458e10               ) #/* speed of light */
    C_mSUN   =  (1.99e33                     ) #/* solar mass */
    C_h      =  (6.6260693e-27               ) #/* Planck constant */

    Theta_kt = 0.2
    mbh = 10.**6 * C_mSUN
    rscale = r_cam * mbh * C_G / (C_c**2)
    area_factor = rscale**2

    h_nu_Theta = C_h * freq / Theta_kt 
    W_nu = h_nu_Theta**(-0.5) * np.exp(-h_nu_Theta)
    A_factor = Theta_kt * np.sqrt(np.pi) / C_h 
    bol_factor = A_factor / W_nu

    print("rscale = ", rscale)
    print("area_factor = ", area_factor)

    dl_domega = area_factor * bol_factor * flux_tot

    print("dl shape = ", dl_domega.shape)
    
    cm = plt.get_cmap('gist_rainbow',lut=n_theta)

    nwin=0
    tstr = str('-%06d' %itime)
    fignames = [dirout + 'dl_domega'+tag+tstr]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'$\phi$ [deg.]')
    ax.set_ylabel(r'dL / d$\Omega$  [erg s$^{-1}$ sterad$^{-1}$]')
    ax.set_ylim(0.55e42,2.25e42)
    for i in range(n_theta):
        lines = ax.plot(phi, dl_domega[i,:])
        lines[0].set_color(cm(i))
        lines[0].set_label(str(r'$\theta$=%2d${}^\circ$' %np.int(theta[i])))
        #lines = ax.plot(np.arange(10)*(i+1))
        #lines[0].set_linewidth(i%3 + 1)

    ax.legend(loc=2)
    
    #plt.colorbar()

    
    for i in np.arange(len(figs)) : 
        figs[i].savefig('%s.pdf' %fignames[i], dpi=300)
    
    print("All done ")
    sys.stdout.flush()
    
    return

###############################################################################
###############################################################################

def make_dl_domega2(second_orbit=True,itime=5):

    
    dirout = './'

    if( second_orbit ) :
        file='light_curve_2nd_merged.h5'
        tag='_2nd'
    else : 
        file='light_curve_3rd_merged.h5'
        tag='_3rd'

    # approximate value of omega_bin
    trajfile='my_bbh_trajectory.out'

    tbh_traj, phase_traj = np.loadtxt(trajfile,usecols=(0,24),unpack=True) #old metrics

    phasebh1 = interp1d(tbh_traj, phase_traj, kind='linear')

    #print(" phase_traj = ", phase_traj)

    h5file = h5py.File(file,'r')
    flux_tot = h5file['flux_tot'].value[:,:,itime]
    phi      = h5file['phi'].value
    theta    = h5file['theta'].value
    r_cam    = h5file['/Header/r_cam'].value[0]
    freq     = h5file['freq'].value[0]
    tout     = h5file['times'].value
    h5file.close()

    phi *= 2.*np.pi / 360. 
    tplot = tout[itime]
    phbh = 0.5*np.pi 
    phase0 = phasebh1(tplot) + phbh  - 2.*np.pi*np.int((phbh+phasebh1(tplot))/(2.*np.pi))

    phiout = phi - phase0  
    loc = phiout < 0. 
    phiout[loc] += 2.*np.pi 
    phiout *= 360./(2.*np.pi)

    print("time phase = ", tplot, phase0)
    #print("phiout = ",  phiout)
    #print("loc = ",  loc)
    
    
    n_theta = len(theta)
    costh = np.cos(theta*np.pi/180.)
    
    C_G      =  (6.6742e-8                   ) #/* Gravitational constant */
    C_c      =  (2.99792458e10               ) #/* speed of light */
    C_mSUN   =  (1.99e33                     ) #/* solar mass */
    C_h      =  (6.6260693e-27               ) #/* Planck constant */

    Theta_kt = 0.2
    mbh = 10.**6 * C_mSUN
    rscale = r_cam * mbh * C_G / (C_c**2)
    area_factor = rscale**2

    h_nu_Theta = C_h * freq / Theta_kt 
    W_nu = h_nu_Theta**(-0.5) * np.exp(-h_nu_Theta)
    A_factor = Theta_kt * np.sqrt(np.pi) / C_h 
    bol_factor = A_factor / W_nu

    print("rscale = ", rscale)
    print("area_factor = ", area_factor)

    dl_domega = area_factor * bol_factor * flux_tot

    print("dl shape = ", dl_domega.shape)
    
    cm = plt.get_cmap('gist_rainbow',lut=n_theta)

    newshape = list(np.copy(dl_domega.shape))
    #print("newshape = ", newshape)
    newshape[0] += 1
    #print("newshape = ", newshape)
    tmp_data = np.ndarray(tuple(newshape))
    #print("tmp shape = ", tmp_data.shape)
    #print("phiout shape = ", phiout.shape)
    tmp_data[0,:] = phiout[:]
    tmp_data[1:,:] = dl_domega[:,:]
    #print("tmp_data = ", tmp_data[0,:], tmp_data[1,:])
    #print("size sorted = ", tmp_data.shape)

    #print("argsort = ", np.argsort(tmp_data[0,:]))
    tmp2 = np.transpose(tmp_data)
    sorted_data = np.transpose(tmp2[np.argsort(tmp2[:,0])])
    #print("sorted_data = ", sorted_data[0,:], sorted_data[1,:])

    nphi = 200
    newphi = np.linspace(0.,360.,num=nphi)
    interp_out = np.ndarray((n_theta,nphi))
    
    nwin=0
    tstr = str('-%06d' %itime)
    fignames = [dirout + 'dl_domega'+tag+tstr]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'$\phi$ [deg.]')
    ax.set_ylabel(r'dL / d$\Omega$  [erg s$^{-1}$ sterad$^{-1}$]')
    ax.set_ylim(0.55e42,2.25e42)
    ax.set_xlim(0.,360.)
    for i in range(n_theta):
        #lines = ax.plot(phiout, dl_domega[i,:])
        xtmp = sorted_data[0,:]
        ytmp = sorted_data[i+1,:]
        xtmp2 = np.zeros(len(xtmp)+2)
        ytmp2 = np.zeros(len(xtmp)+2)
        xtmp2[0]    = xtmp[-1]-360.
        xtmp2[1:-1] = xtmp[:]
        xtmp2[-1]   = xtmp[0]+360.
        ytmp2[0]    = ytmp[-1]
        ytmp2[1:-1] = ytmp[:]
        ytmp2[-1]   = ytmp[0]
        #print("xtmp2 = ", xtmp2)
        #print("ytmp2 = ", ytmp2)
        finterp = interp1d(xtmp2,ytmp2,kind='nearest')
        yout = finterp(newphi)
        #print("orig phi = ", sorted_data[0,:])
        #print("newphi = ", newphi)
        #print("finterp = ", finterp(newphi))
        lines = ax.plot(sorted_data[0,:], sorted_data[i+1,:],'.')
        lines2 = ax.plot(newphi,yout)
        lines[0].set_color(cm(i))
        lines[0].set_label(str(r'$\theta_\mathrm{cam}$=%2d${}^\circ$' %np.int(theta[i])))
        lines2[0].set_color(cm(i))
        #lines = ax.plot(np.arange(10)*(i+1))
        #lines[0].set_linewidth(i%3 + 1)
        interp_out[i,:] = yout
        

    ax.legend(loc=2)
    
    #plt.colorbar()

    
    for i in np.arange(len(figs)) : 
        figs[i].savefig('%s.pdf' %fignames[i], dpi=300,bbox_inches='tight')
    
    print("All done ")
    sys.stdout.flush()
    
    return theta, newphi, interp_out


###############################################################################
# 
###############################################################################

def check_units():

    C_G      =  (6.6742e-8                   ) #/* Gravitational constant */
    C_c      =  (2.99792458e10               ) #/* speed of light */
    C_mSUN   =  (1.99e33                     ) #/* solar mass */
    C_h      =  (6.6260693e-27               ) #/* Planck constant */

    freq =  1.09647819614318e+19
    r_cam = 1e3
    Theta_kt = 0.2
    mbh = 10.**6 * C_mSUN
    lscale = mbh * C_G / (C_c**2)
    rscale = r_cam * mbh * C_G / (C_c**2)
    area_factor = rscale**2

    h_nu_Theta = C_h * freq / Theta_kt 
    W_nu = h_nu_Theta**(-0.5) * np.exp(-h_nu_Theta)
    A_factor = Theta_kt * np.sqrt(np.pi) / C_h 
    bol_factor = A_factor / W_nu

    print("r_cam [cm]  = ", rscale)
    lengthScale =     1.477783751818498840e+11 
    print("lengthScale bothros = ", lengthScale )
    print("lscale              = ", lscale)
    print("bol_factor = ", bol_factor)

    print("total factor = ", bol_factor * rscale**2 ) 
    
    return


###############################################################################
#
###############################################################################
def find_interp_data(second_orbit=True): 

    nt = 12 
    for itime in np.arange(nt) : 
        th_tmp, ph_tmp, int_tmp = make_dl_domega2(second_orbit=second_orbit,itime=itime)
        if( itime == 0 ):
            nth = len(th_tmp)
            nph = len(ph_tmp)
            all_data = np.ndarray(tuple((nt,nth,nph)))
            thout = th_tmp
            phout = ph_tmp

        all_data[itime,:,:] = int_tmp


    return nt, thout, phout, all_data 

    

###############################################################################
#
###############################################################################
def make_tavg_plot(second_orbit=True):

    nt, theta, phout, all_data  = find_interp_data(second_orbit=second_orbit)

    std_data = np.std(all_data,axis=0)
    avg_data = np.mean(all_data,axis=0) 

    print("shape of avg = ", std_data.shape, avg_data.shape)

    n_theta = len(theta)

    if( second_orbit ) :
        tag = '-2nd'
    else :
        tag = '-3rd'
        

    nwin=0
    dirout = './'

    cm = plt.get_cmap('gist_rainbow',lut=n_theta)
    
    tstr = '-tavg'
    fignames = [dirout + 'dl_domega'+tag+tstr]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'$\phi_\mathrm{cam}$ [deg.]')
    ax.set_ylabel(r'dL / d$\Omega$  [erg s$^{-1}$ sterad$^{-1}$]')
    #ax.set_ylim(0.55e42,1.5e42)
    #ax.set_ylim(0.55e42,6.5e42)
    ax.set_ylim(0.,6.e42)
    ax.set_xlim(0.,360.)
    for i in np.arange(n_theta):
        offset = 0.5e42*(n_theta-1-i)
        yout = avg_data[i,:] + offset 
        lines = ax.plot(phout, yout,alpha=1)
        lines[0].set_color(cm(i))
        lines[0].set_label(str(r'$\theta_\mathrm{cam}$=%2d${}^\circ$' %np.int(theta[i])))
        ymin = yout - std_data[i,:] 
        ymax = yout + std_data[i,:] 
        ax.fill_between(phout, ymin, ymax, color = cm(i), alpha = 0.4)
        #ax.plot(x_data, y_data, lw = 1, color = '#539caf', alpha = 1, label = 'Fit')
        # Shade the confidence interval
        #ax.fill_between(sorted_x, low_CI, upper_CI, color = '#539caf', alpha = 0.4, label = '95% CI')
        
        
    #ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    ax.legend(loc=1)
    
    #plt.colorbar()

    
    for i in np.arange(len(figs)) : 
        figs[i].savefig('%s.pdf' %fignames[i], dpi=300, bbox_inches='tight')
    
    print("All done ")
    sys.stdout.flush()


    return 

    

###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python lum
# 
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)

    make_dl_dth(second_orbit=True)
    make_dl_dth(second_orbit=False)

    #for itime in np.arange(12) : 
    #    make_dl_domega(second_orbit=True,itime=itime)
    #    make_dl_domega(second_orbit=False,itime=itime)

    #for itime in np.arange(12) : 
    #    make_dl_domega2(second_orbit=True,itime=itime)
    #    make_dl_domega2(second_orbit=False,itime=itime)

            
    #itime=0
    #make_dl_domega2(second_orbit=True,itime=itime)

    # make_tavg_plot(second_orbit=True)
    # make_tavg_plot(second_orbit=False)


#    check_units()
    
#     if len(sys.argv) > 1 :
#         make_series(sys.argv[1])
#     else:
#         lum()

    
