"""  

  gencont.py: 
  ------------

    -- generic contour plot, not for doing sophisticated operations
       but for quickly viewing 2-d slices of 3-d data;

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re 

from harm import * 

from IPython.core.debugger import Tracer

###############################################################################
###############################################################################

def gencont(funcname,
            filename='*000000*.h5',
            dolog=False,
            nticks=10,
            nlev=256,
            minf=None, 
            maxf=None,
            xmax=None,
            ymax=None,
            xmin=None,
            ymin=None,
            zmax=None,
            zmin=None,
            savepng=True,
            showplots=False,
            dirout='./',
            slice=[-1,-1,-1],
            plottitle=None,
            cmap=plt.cm.Spectral_r,
            tag=None,
            all_labels=None
            ):

    """  
    Routine for making quick color contours of data slices. 
    """
    prog_name="gencont"


# Note that the "filename" can be a wildcard expression, just put it in quotes. 
    files = glob.glob(filename)

    print("files = ", files) 

    if( len(files) < 1 ) : 
        sys.exit(prog_name+"():  No files matching the expression!  Exitting...", filename)
    
    if( len(files) > 1 ) : 
        sys.exit(prog_name+"():  Need to be more specific with the wildcard expression so we only choose one file!  Exitting...", filename)
    
        
    print(prog_name+"():  Reading : ", files[0])

# Now figure out the dimensions:      
    fh5 = h5py.File(files[0],'r')
    n1 = get_val('totalsize1',fh5)
    n2 = get_val('totalsize2',fh5)
    n3 = get_val('totalsize3',fh5)
    tout = get_val('t',fh5)
    
    print("grid function dimensions = ", n1[0], n2[0], n3[0])
    
    #  Check slice: 
    slice = np.array(slice)
    ndims = np.array([n1[0],n2[0],n3[0]])
    plot_dims = np.array([ (n1[0] > 1), (n2[0] > 1), (n3[0] > 1) ])

    print("plot_dims1 = ", plot_dims) 

    print("slice = ",slice)

    if( len(slice) != 3 ) : 
        sys.exit(prog_name+"():  Problem with length of slice: ", slice)
        
    plot_dims[ slice >= 0 ] = False   # We do not plot along dimensions we are slicing through

    print("plot_dims2 = ", plot_dims) 
    
    if( any( np.greater_equal(slice, ndims) ) ):
        sys.exit(prog_name+"():  We are trying to slice at an index out of bounds:  slice = ", slice, "  ndims = ", ndims )
    
    if( any( plot_dims ) ) : 
        print(prog_name+"():  We are plotting  func = ", funcname, "  in dimensions = ", plot_dims)
    else : 
        sys.exit(prog_name+"():  No real way to plot a point of data!!! ")
    
    
    if( all(plot_dims) ) : 
        print(prog_name+"():  User did not specify a valid slice for full 3-d data, so we will use the default slice through the third dimension at k=0")
        slice[2] = 0
        plot_dims[2] = False
    
    do_1d = ( np.sum(plot_dims) == 1 )

    if( all_labels is None ) : 
        all_labels = np.array(['xp1','xp2','xp3'])

    plot_labels = all_labels[plot_dims]
    nout = ndims[plot_dims]
    func_slice, xout, yout  = get_harm_gridfunction(funcname,plot_labels,slice,plot_dims,nout,do_1d,fh5)

        
    fh5.close()
       
    xmin,xmax,ymin,ymax  = [np.min(xout),np.max(xout), np.min(yout), np.max(yout)]
    
    if dolog:
        func_slice[func_slice == 0] = 1.e-25       # so that there is no log of zero...
        func_slice = np.log10(np.abs(func_slice))
    
    
    if minf is None:
        minf = np.min(func_slice)
    if maxf is None:
        maxf = np.max(func_slice)
    
    
    tstring = str('%0.1f' %tout)
    
    figname = funcname
    if dolog:
        figname = 'log10|' + figname + '|'
    
    if plottitle is None:
        plottitle = figname
    
    if plottitle is 'notitle':        
        title = 't =  ' + tstring
    else: 
        title = plottitle + '  t =  ' + tstring

    if tag is None : 
        tag = '-'
    
    
    xlabel = plot_labels[0]
    if do_1d  : 
        ylabel = figname
    else: 
        ylabel = plot_labels[1]
    
    suffix = '-slice{}-{}-{}'.format(slice[0],slice[1],slice[2])
    title += '  slice = [{},{},{}]'.format(slice[0],slice[1],slice[2])

    nwin=0
    fignames = [dirout + figname + '-'+ylabel+'-vs-'+xlabel+'-'+tstring+suffix+tag]
    print( "fignames = ", fignames)
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_title('%s' %title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    levels = np.linspace(minf,maxf,nlev)
    rticks = np.linspace(minf,maxf,nticks)
    
    if do_1d : 
        ax.axis([xmin,xmax,ymin,ymax])
        ax.plot(xout, yout)
    else:  
        CP = ax.contourf(xout, yout,func_slice,levels,cmap=cmap,extend='both')
        figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)
            
    if showplots : 
        plt.show()
            
    print("All done ")
    sys.stdout.flush()
        
    
    return


###########################################################################
##  Routine to loop over times:
###########################################################################
def gc_alltimes() : 
    
    filenames = "SOD.patch_0*" 
    tag = "_patch0"

    for file in glob.glob(filenames):
        gencont('rho',filename=file,tag=tag)
        gencont('uu' ,filename=file,tag=tag)
        
    
    filenames = "SOD.patch_1*" 
    tag = "_patch1"
    for file in glob.glob(filenames):
        gencont('rho',filename=file,tag=tag)
        gencont('uu' ,filename=file,tag=tag)

        
    return

###########################################################################
##  Routine to loop over times:
###########################################################################
def pspec() :
    
    filenames = "dumps/K*100.h5"

    slice = [200,-1,-1]
    labels = np.array(['x1','x2','x3'])

    for file in glob.glob(filenames):
        gencont('r',filename=file, slice=slice,all_labels=labels )
        gencont('poynting_flux', filename=file, slice=slice,all_labels=labels )
        
    
    return

    
#  
#   ###############################################################################
#   # This allows the python module to be executed like:   
#   # 
#   #   prompt>  python lum
#   # 
#   ###############################################################################
#   if __name__ == "__main__":
#       import sys
#       print(sys.argv)
#       if len(sys.argv) > 1 :
#           gencont(sys.argv[1])
#       else:
#           gencont()

#gencont('rho',filename='SOD*.h5')

