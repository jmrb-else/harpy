"""  

  lump.py: 
  ------------
   -- program that creates various analysis plots regarding the m=1 
       overdensity feature, aka "lump". 

   -- reads in the merged "*.surface.all.h5" file and analyzes it. 


"""

from __future__ import division
from __future__ import print_function

from scipy.fftpack import fft
import scipy.signal as signal 
import matplotlib.pyplot as plt
import numpy as np
import os,sys
import h5py

from get_sim_info import *
from readhist_h5_all import *

from IPython.core.debugger import Tracer

###############################################################################
###############################################################################
###############################################################################

# def lump(filename='KDHARM0.surface.all.h5',savepng=0):
"""  
Primary routine for lump analysis. 
"""


#runnames = ['longer2', 'medium_disk', 'bigger_disk', 'inject_disk']
runnames = ['bigger_disk']
#runnames = ['longer2']
#runnames = ['inject_disk']

runname=runnames[0]

showplots=1
savepng=0
phi_order = 1 


pnames = ['dirname', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 't_lump_beg', 't_lump_end', 'tend', 'rsep0']
sim_info = get_sim_info(pnames, runname=runname)
filename=sim_info['surf_name']+'.all.h5'

dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'


#################
# Read various details about datasets inside example file : 
#################
print("Reading in ", filename)
sys.stdout.flush()

f = h5py.File(filename,'r')

#  open group in root directory 
name = 'S_rhoav' 
func1 =  f['/Bound/S_rhoav'].value
t_out = f['tout'].value
r_out = f['rout'].value
f.close()

# should be  nt, nr, nphi
print("func1 shape", func1.shape)
sys.stdout.flush()

nt   = len(t_out)
nr   = len(r_out)
nphi = func1.shape[2]

xtmp = np.ones(nt*nr)
phi_out = np.linspace(0.,2.*np.pi,nphi)
base_func = np.sin(np.multiply(phi_out,phi_order))
base_factor = np.reshape(np.outer(xtmp,base_func),(nt,nr,nphi))
power = np.sum(np.multiply(func1,base_factor),axis=2)
dphi = 2./nphi
power = np.multiply(power,dphi)


nwin=0
fignames = 'power'
figs = [plt.figure(nwin)]
figs[-1].clf()
ax = figs[-1].add_subplot(111)
ax = figs[-1].add_subplot(111)
ax.set_ylabel(r'r [a]')
ax.set_xlabel(r't [$10^4$ M]')
xtmp = np.ones(nr)
t_out = np.multiply(t_,out,1e-4)
tplot = np.outer(t_out,xtmp)
xtmp = np.ones(nt)
ftmp = 1./sim_info['rsep0']
r_out = np.multiply(r_out,ftmp)
rplot = np.outer(xtmp,r_out)
plt.pcolormesh(tplot,rplot,power,cmap='jet')
plt.colorbar()

if(savepng):
    iph=10
    figname = 'testfig-' + '%04d' %iph
    fig1.savefig('%s.png' %figname, dpi=300)

plt.show()


print("All done ")
sys.stdout.flush()


# ###############################################################################
# # This allows the python module to be executed like:   
# # 
# #   prompt>  python lump
# # 
# ###############################################################################
# if __name__ == "__main__":
#     import sys
#     print(sys.argv)
#     if len(sys.argv) > 1 :
#         lump(sys.argv[1])
#     else:
#         lump()
# 
