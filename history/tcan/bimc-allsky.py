"""  

  bim.py: 
  ------------
    -- display image from bothros calculation

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################

def detrend(xfunc,yfunc) : 
    
#    pfit = np.poly1d(np.polyfit(xfunc,yfunc,deg=5))(xfunc)
    nlen = len(xfunc)
    pfit = np.poly1d(np.polyfit([xfunc[nlen-1],xfunc[0]],[yfunc[nlen-1],yfunc[0]],deg=1))(xfunc)
    y_detrend1 = yfunc - pfit
    y_final = y_detrend1 / np.max(y_detrend1)
    
    return y_final


###############################################################################
###############################################################################

def flux_conv(dirname='res_0300',
              dirout='./'
              ):
    """  
    lPrimary routine for analyzing light curve. 
    """

    showplots=0
    savepng=1
    nwin=0
    
    cmap  = plt.cm.Spectral_r
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 
    
    if not showplots :
        plt.ioff()
    
    files = sorted(glob.glob("res_*/light_curve_00.h5"))
    nfiles = len(files)

    print("files=", files)

    funcs = ['F_minidisks','F_circumbinary','F_cavity','F_tot']
    nfuncs = len(funcs)

    labels = ['300x300','500x500','800x800','1100x1100','1400x1400']

    for i in np.arange(nfiles) : 
        h5file = h5py.File(dirout+files[i])
        if( i == 0 ): 
            times =  h5file['times'].value
            nt = len(times)
            all_data = np.ndarray((nfiles,nfuncs,nt))
            
        for j in np.arange(nfuncs) : 
            all_data[i,j,:] = h5file[funcs[j]].value[:]
            
        h5file.close()


    figs = []
    fignames = []

    linetypes = ['k-','b-','r-','g-','k.','b.','r.','g.']

    for i in np.arange(nfuncs):
        fignames = np.append(fignames,(dirout + funcs[i]+'-conv'))
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r't [M]')
        ax.set_ylabel(r'Flux')
        for j in np.arange(nfiles) :
            ax.plot(times, all_data[j,i,:], linetypes[j], label=labels[j])
        ax.legend(loc=0)
        nwin+=1
    
    
    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)
            
    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################

import sys
print(sys.argv)
if len(sys.argv) > 1 :
    prefix=sys.argv[1]
else : 
    prefix="light_curve_*.h5"

files = sorted(glob.glob(prefix))

rcam=1e3
rmax=15.
xmax = rmax/rcam
xmin =-xmax
#bbox = np.multiply([-1.,-0.5,1.,0.5],np.pi)
#bbox = np.multiply([(1.-3./8.),-0.25,1.,0.25],np.pi)
bbox = np.multiply([-1.,-0.5,1.,0.5],np.pi)
lscale = 1.
aratio='equal'

w=7.2
h=3.6
dpi=300



if 1 : 
    i = 0 
    for file in files :
        for itime in np.arange(1) :
            file_parts = file.split("light_curve_")
            time_strings = file_parts[1].split('.')
            time_str = time_strings[0]
            print("time_str = ", time_str)
            
            tag = "-"+time_str+"-"

            make_image(filename=file,itime=itime,dolog=False,tag=tag+'-pix-x',funcname='pix_x',length_scale=lscale,bbox=bbox,aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=False,tag=tag+'-pix-y',funcname='pix_y',length_scale=lscale,bbox=bbox,aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=False,tag=tag+'-ngeod',funcname='n_geod',length_scale=lscale,bbox=bbox,aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=False,tag=tag+'-id',funcname='id',length_scale=lscale,bbox=bbox,aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=True,tag=tag+'-all',minf=1e-9,maxf=2e-4,length_scale=lscale,bbox=bbox,aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=True,tag=tag+'-zoom',length_scale=lscale,bbox=bbox,aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=True,tag=tag+'-image2',funcname='I_image2',length_scale=lscale,bbox=bbox,aspect_ratio=aratio,maxf=5.,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=False,tag=tag+'-redshift',funcname='redshift_infinity',length_scale=lscale,bbox=bbox,minf=False,aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=False,tag=tag+'-th',funcname='th_infinity',length_scale=lscale,bbox=bbox,minf=1e-12,maxf=(np.pi),aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=False,tag=tag+'-ph',funcname='ph_infinity',length_scale=lscale,bbox=bbox,minf=1e-12,maxf=(2.*np.pi),aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)
            make_image(filename=file,itime=itime,dolog=False,tag=tag+'-end_state',funcname='end_state',length_scale=lscale,bbox=bbox,aspect_ratio=aratio,width=w,height=h,dpi=dpi,image_only=True)

        i += 1 

    
#flux_conv()

    



#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
