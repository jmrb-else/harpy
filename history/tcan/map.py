import os,sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import matplotlib.image as mpimg
from scipy import ndimage
from scipy import misc

import numpy as np
import string 
import h5py


###############################################################################

def angle_index_map(angles,period,npix) :

    loctmp = np.copy(angles)
    loctmp = loctmp % (period)
    loctmp *= npix/period
    indices = loctmp.astype(int)
    loctmp = 0 

    return indices

###############################################################################

def write_plain_image(img,
                      nwin=0,
                      width=2.,
                      height=1.,
                      imgsuffix='png',
                      dpi=300,
                      figname='plain-image'
                      ) :

    fig = plt.figure(nwin,frameon=False)
    fig.set_size_inches(width,height)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(img)
    figstr = figname+'.'+imgsuffix
    fig.savefig(figstr, dpi=dpi)

    return 

###############################################################################

dpi=300
nwin=0

print(" sys.argv = ", sys.argv)

if( len(sys.argv) < 5 ) :
    sys.exit('Need more arguments')

            
iframe=int(sys.argv[1])
nframes=int(sys.argv[2])
imgfile=sys.argv[3]
warp_map = sys.argv[4]
f=mpimg.imread(imgfile)

# f = misc.face()
# misc.imsave('face.png', f) # uses the Image module (PIL)
#f=mpimg.imread('WISE2012-003-B.jpg')
#f=mpimg.imread('gaia.png')

allparts=imgfile.split('.')
figname_prefix = 'warped-' + allparts[0]
#figname_prefix='new-wise'

print("shape = ", f.shape)
print("type = ", f.dtype)
ftype = f.dtype

fdest = h5py.File(warp_map,'r')
end_state = fdest['end_state'].value[0,:,:]
horizon_loc = np.where(end_state != 1)
nhor = len(horizon_loc[0])
end_state=0

ph_in0 = fdest['ph_infinity'].value[0,:,:]
th_in = fdest['th_infinity'].value[0,:,:]
newimg = np.zeros((th_in.shape[0], th_in.shape[1], f.shape[2]), dtype=ftype)
th_map = angle_index_map(th_in,(np.pi),f.shape[0])

height=np.float(th_in.shape[0])/np.float(dpi)
width=np.float(th_in.shape[1])/np.float(dpi)
print("width height = ", width, height)

# phase loop:
ph_shift = 2.*np.pi/nframes
print("ph_shift = ", ph_shift)
#for i in np.arange(nframes) :
for i in [iframe] :
    ph_in = ph_in0 + i*ph_shift
    print("ph_in = ", ph_in[0,0], ph_in0[0,0] )
    ph_map = angle_index_map(ph_in,(2.*np.pi),f.shape[1])
    newimg[:,:,:] = f[th_map[:,:],ph_map[:,:],:]
    if( nhor > 0 ) :
        print("vals = ", newimg[horizon_loc[0][:],horizon_loc[1][:],:]) 
        newimg[horizon_loc[0][:],horizon_loc[1][:],0:3] = 0
        print("vals = ", newimg[horizon_loc[0][:],horizon_loc[1][:],:]) 
    figname=figname_prefix+'-'+str('%06d' %i)
    print("writing ", figname) 
    write_plain_image(newimg,width=width,height=height,figname=figname)
    ph_map = 0 
    ph_in = 0 

#And plot the image if you want
#  
# plt.imshow(newimg)
# plt.show()
