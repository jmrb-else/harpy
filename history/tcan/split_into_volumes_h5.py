"""  

  split_into_volumes_h5..py: 
  ------------
    -- program to split a merged dump into volumes;

    -- works with the given file and the number of volumes to be made,
       which needs to be an integer factor of the totalsize1;

"""

import numpy as np
import h5py

import os,sys
import glob

VERBOSE = 0 

###############################################################################
###############################################################################
###############################################################################

def find_list_of_h5files(base):
    """  
    Returns with the list of files matching 'base.N.h5' where 'N' is a
    zero-padded 6-digit integer.
    """

    globname = './' + base + ".vol*.h5"
    h5list = glob.glob(globname)

    if len(h5list) <= 0:
        print("Cannot find any files to merge with basename = ", base)
        sys.exit("Can no longer proceed, exiting....")

    list.sort(h5list)
    return h5list

###############################################################################
###############################################################################
###############################################################################

def split_into_volumes_h5(orig_name,n_volumes):
    """  
    Primary routine that calls all the functions and loops over the files
    and grid functions.
    """

    if( VERBOSE ) : 
        print("orig_name = ", orig_name, "    n_volumes = ", n_volumes)

        
    if( n_volumes < 0 ) : 
        print("n_volumes must be positive, exiting....,  n_volumes = ", n_volumes)
        sys.exit("Exiting")

    prefix = orig_name.split('.h5')[0]

    if( VERBOSE ) : 
        print("prefix = ", prefix)

    sys.stdout.flush()
    
    #################
    # Read various details about datasets inside example file : 
    #################
    # Get a list of all the parameters in the hdf5 header file:  
    fh5    = h5py.File(orig_name,'r')

    totalsize1 = fh5['/Header/Grid/totalsize1'][0]

    if( (totalsize1 % n_volumes) != 0 ) :
        fh5.close()
        print(" volumes must all be the same size, so the number of volumes must be a divisor of the totalsize1 ",  totalsize1, n_volumes)
        sys.exit("Exiting")

    if( (totalsize1 < n_volumes) ) :
        fh5.close()
        print(" volumes need to have integer size greater or equal to one :  ",  totalsize1, n_volumes)
        sys.exit("Exiting")

 
    all_h5_objs = []
    fh5.visit(all_h5_objs.append)

    all_groups   = [ obj for obj in all_h5_objs if isinstance(fh5[obj],h5py.Group) ]
    all_datasets = [ obj for obj in all_h5_objs if isinstance(fh5[obj],h5py.Dataset) ]


    if( VERBOSE ): 
        print("  ")
    
    n1 = int(totalsize1 / n_volumes)

    for ivol in np.arange(n_volumes) :
        file = str(prefix+".vol%05d.h5" %ivol)
        if( VERBOSE ) : 
            print("Creating file = ", file)
        fh5new = h5py.File(file,'w')

        # Create all the groups: 
        for obj in all_groups : 
            fh5new.create_group(fh5[obj].name)

        for obj in all_datasets : 
            gd = fh5[obj]
            if '/Header' in gd.name  :
                if( VERBOSE ): 
                    print(" Shrinking and Copying over :  " + gd.name)
                dnew = fh5new.create_dataset(gd.name, data=gd[:])

            else :
                if( VERBOSE ): 
                    print(" Creating over :  " + gd.name)
                ibeg = n1*ivol
                iend = n1*(ivol+1)
                shape1 = np.copy(gd.shape)
                shape1[0] /= n_volumes
                dset = fh5new.create_dataset(gd.name,shape1,dtype=fh5[obj].dtype)
                dset[:,:,:] = gd[ibeg:iend,:,:]

        fh5new['/Header/Grid/N1'][:] = n1
        fh5new['/Header/Grid/cpupos1'][:] = ivol
        fh5new.close()
    
        
    fh5.close()
    

    
###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python split_into_volumes_h5.py  <file.h5> <n_volumes>
# 
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) > 2 :
        split_into_volumes_h5(sys.argv[1],int(sys.argv[2]))
    else:
        print("Usage:  split_into_volumes_h5.py  <file.h5> <n_volumes>  ")
        sys.exit("User must specify the prefix string identifying the set of files to merger, exiting....")

