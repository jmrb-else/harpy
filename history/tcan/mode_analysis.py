import sys
import numpy as np
import matplotlib.pyplot as plt
import h5py
from scipy.interpolate import interp1d, griddata
from scipy.fftpack import fft, fftfreq
import numpy.polynomial.polynomial as poly 

from polp_new import get_metric, get_dx_dxp
from coord_transforms import get_ginverse, transform_gcon, transform_tensordensity, get_dxc_dxs, metric2BL, coordsIZ_of_coordsNZ, coordsBL_of_coordsIZ

'''
  Fourrier Series Coefficients for generic 2D function 
   on evenly sampled, regular coordinates (x1,x3)
'''
def zero_mode(func,x1,x3,dx1,dx3,detg):
    return np.sum(func * detg ) * dx1 * dx3

def an(func,x1,x3,dx1,dx3,detg,m):
    return np.sum(func * np.cos(m*x3) * detg ) * dx1 * dx3

def bn(func,x1,x3,dx1,dx3,detg,m):
    return np.sum( func * np.sin(m*x3) * detg ) * dx1 * dx3

def get_metric_modes(tindex,dirname='.',trajfile=None,RMAX=0.35,RMIN=None):
    '''
    This function calculates the binary botential via the static-weak field approximation
    in a local Boyer-Lindquist frame centered on the primary BH using harm hdf5 output

    Assumptions:
      Removes axisymmetric GM1/r1 from potential before caluclating modes
    Inputs:    
      tindex: Index in dumps to read in for potential calculation
      trajfile: trajectory data
      RMAX:   Fraction of binary separation to integrate out to in the mode calculation
      RMIN:   Fraction of binary separation to integrate in  to in the mode calculation (if None assumes horizon)
    Returns (all modes normalized to m0):
      r12, m1, m2
    '''

    #Setup and read the coordinates from Harm output
    timeid   = '%06d' %tindex
    fullname = dirname + '/dumps/KDHARM0.' + timeid +  '.h5'
    print(fullname) #debugging
    h5file   = h5py.File(fullname,'r')
    RRNZ     = h5file['x1'].value
    THNZ     = h5file['x2'].value
    PHNZ     = h5file['x3'].value
    tloc     = h5file['Header/Grid/t'].value[0]
    h5file.close()
    
    if trajfile is None:
        trajfile = dirname + '/traj/my_bbh_trajectory.out'

    #Read and calculate the BH trajectory information at this time-slice
    tbh_traj, phi_bin_traj, omega_bin_traj, r12_traj = np.loadtxt(trajfile,usecols=(0,23,24,25),unpack=True) #old metrics

    fr12       = interp1d(tbh_traj,r12_traj,kind='linear')
    r12        = fr12(tloc)
    fomega_bin = interp1d(tbh_traj, omega_bin_traj, kind='linear')            
    omega_bin  = fomega_bin(tloc)
    fphi_bin   = interp1d(tbh_traj, phi_bin_traj,kind='linear')
    phase      = fphi_bin(tloc)
    
    #Renormalize to 2pi
    norb       = int(phase / (2. * np.pi))
    print("Norm fac phase, norb, newphase ", phase,  norb, (phase - 2.*np.pi*norb)/(np.pi) ) #debugging
    phase     -= 2. * np.pi * norb


    #Calculate the BL coordinates around the primary (non-rotated frame)
    TTNZ = np.zeros((np.shape(RRNZ))); TTNZ.fill(tloc)
    TTIZ, XXIZ, YYIZ, ZZIZ = coordsIZ_of_coordsNZ(trajfile,'primary',TTNZ,RRNZ*np.sin(THNZ)*np.cos(PHNZ),RRNZ*np.sin(THNZ)*np.sin(PHNZ),RRNZ*np.cos(THNZ))
    TTBL, RRBL, THBL, PHBL = coordsBL_of_coordsIZ(0.5,0,TTIZ,XXIZ,YYIZ,ZZIZ)

    #Read and transform the metric to BL coordinates centered on the primary
    dx_dxp = get_dx_dxp(fullname)
    metric = get_metric(fullname)
    metric = metric2BL('primary',trajfile,TTNZ,RRNZ,THNZ,PHNZ,dx_dxp,metric)
    gcon   = { '00':metric['gcon00'], '01':metric['gcon01'], '02':metric['gcon02'], '03':metric['gcon03'],
               '11':metric['gcon11'], '12':metric['gcon12'], '13':metric['gcon13'],
               '22':metric['gcon22'], '23':metric['gcon23'],
               '33':metric['gcon33'] }
    gdet   = metric['gdet']
    '''
    rewrite dx_dxp to be jacobian to corotating frame 
    dx_dxp = |   1    0 0 0 |
             |   0    1 0 0 |
             |   0    0 1 0 |
             |  omega 0 0 1 |
    '''
    dx_dxp['00'].fill(1.);         dx_dxp['01'].fill(0.); dx_dxp['02'].fill(0.); dx_dxp['03'].fill(0.)
    dx_dxp['10'].fill(0.);         dx_dxp['11'].fill(1.); dx_dxp['12'].fill(0.); dx_dxp['13'].fill(0.)
    dx_dxp['20'].fill(0.);         dx_dxp['21'].fill(0.); dx_dxp['22'].fill(1.); dx_dxp['23'].fill(0.)
    dx_dxp['30'].fill(omega_bin);  dx_dxp['31'].fill(0.); dx_dxp['32'].fill(0.); dx_dxp['33'].fill(1.)
    
    #Transform gcon and gdet to the rotating BL frame 
    gcon    = transform_gcon( dx_dxp,  gcon) # Corotating Frame ( spherical coords )
    gdet    = transform_tensordensity(0,-2,dx_dxp,gdet)
    
    #Calculate the new BL-azimuthal coordinate in this frame normalized to (0,2pi)
    PHBL   += phase #corotate the coords
    PHBL[PHBL > 2.*np.pi] -= 2.*np.pi; PHBL[PHBL < 0.] += 2.*np.pi; #Normalize to (0,2pi)

    #Transform gcon and gdet to the rotating Cartesian frame
    dxc_dxs = get_dxc_dxs(RRBL,THBL,PHBL)
    gcon    = transform_gcon( dxc_dxs, gcon) #Cartesian PN Coords (Physical)
    gdet    = transform_tensordensity(0,-2,dxc_dxs,gdet)

    #Get gcov from gcon and calculate the Binary Potential from g_{tt}
    gcov    = get_ginverse(gcon) #Get gcov
    pot     = -0.5 * ( gcov['00'] + 1. ) #Binary Potential

    #Subtract azimuthally symmetric primary newtonian potential to look at perturber only
    pot    += 0.5 / RRBL #GM1/r1


    '''
      Code gets uglier here:
      IMPORTANT HACK EXPLAINED: we zero out the value at phi=2pi so we don't double count the point in the mode calculation
        Need to interpolate onto a regular grid so we can easily integrate for mode calculation
          Regular grid uses 1500 * (r12/20) radial points and 1500 azimuthal points
          rr1d : Flattened BL radial    coordinate array which contains only elements to integrate over in mode calculation
          ph1d : Flattened BL azimuthal coordinate array which contains only elements to integrate over in mode calculation
         pot1d : Flattened BL potential            array which contains only elements to integrate over in mode calculation
        detg1d : Flattened BL \sqrt{-g}            array which contains only elements to integrate over in mode calculation
         rrnew : 2D        BL radial    coordinate array regular grid to interpolate onto and integrate in (spacing drnew)
         phnew : 2D        BL azimuthal coordinate array regular grid to interpolate onto and integrate in (spacing dphnew)
    '''
    if( RMIN is None ):
        RMIN = 1.
    else:
        RMIN *= r12
    RMAX *= r12
    #Flatten arrays
    rr1d    = RRBL[(RRBL < RMAX) & (RRBL > RMIN)].flatten()
    ph1d    = PHBL[(RRBL < RMAX) & (RRBL > RMIN)].flatten()
    pot1d   = pot[(RRBL < RMAX) & (RRBL > RMIN)].flatten()
    detg1d  = gdet[(RRBL < RMAX) & (RRBL > RMIN)].flatten()
    #Initialize new regular grid to work on
    rrnew   = np.linspace(RMIN,RMAX,1500*(r12/20))
    phnew   = np.linspace(0,2.*np.pi,1500)
    rrnew, phnew = np.meshgrid(rrnew,phnew,indexing='ij')
    drnew   = rrnew[1,0] - rrnew[0,0]; dphnew = phnew[0,1] - phnew[0,0]
    #Interpolate the potential and detg onto the new regular grid
    potnew  = griddata((rr1d,ph1d),pot1d, (rrnew,phnew),fill_value=0,method='cubic')
    detgnew = griddata((rr1d,ph1d),detg1d,(rrnew,phnew),fill_value=0,method='cubic')


    #potnew.fill(1.); detgnew.fill(1) #uncomment to fill arrays with constant value to test mode calculation (should give zero)

    #Zero out pot/detg at phi = 2pi so we don't double count the point in mode calculation
    potnew[ phnew == 2. * np.pi] = 0.
    detgnew[phnew == 2. * np.pi] = 0.

    #Calculate the Fourrier series coefficients
    m0_pot = zero_mode(potnew,rrnew,phnew,drnew,dphnew,detgnew)
    a1_pot = an(potnew,rrnew,phnew,drnew,dphnew,detgnew,1)
    a2_pot = an(potnew,rrnew,phnew,drnew,dphnew,detgnew,2)
    b1_pot = bn(potnew,rrnew,phnew,drnew,dphnew,detgnew,1)
    b2_pot = bn(potnew,rrnew,phnew,drnew,dphnew,detgnew,2)

    #Calculate mode strengths normalized to m0
    m1pot = np.sqrt(a1_pot*a1_pot + b1_pot*b1_pot)/m0_pot
    m2pot = np.sqrt(a2_pot*a2_pot + b2_pot*b2_pot)/m0_pot
    #print("Potential modes 1 , 2 ", m1pot,m2pot, m1pot/m2pot)

    return r12, m1pot, m2pot
    #return np.array([r12,m1pot,m2pot]) #pack within array if desired


def get_Newtonian_metric_modes(r12,RMAX=0.35,RMIN=None):
    '''
    This function calculates the binary botential via the static-weak field approximation
    in a local Boyer-Lindquist frame centered on the primary BH using harm hdf5 output

    Assumptions:
      potential = -GM1/r - GM2/|r - D| + GM2 (r \cdot D) / |D|^3
      D         = (-r12+0.5,0,0)
      r         = (xBL,yBL,0)
      r2        = r - D
      Removes GM1/r1 axisymmetric portion of potential before mode analysis
    Inputs/Assumptions:
      r12 :   Binary Separation to use for Newtonian calculation
      RMAX:   Fraction of binary separation to integrate out to in the mode calculation
      RMIN:   Fraction of binary separation to integrate in  to in the mode calculation (if None assumes horizon)
    Returns (all modes normalized to m0):
      r12,m1_newt,m2_newt #where Newtonian is the potential used in Newtonian studies with indirect term 

    '''
    #Set Default Behavior and convert to actual distances
    if( RMIN is None ):
        RMIN = 1.
    else:
        RMIN *= r12
    RMAX *= r12
    #Initialize location of the secondary in BL coordinates
    xbh2 = -(r12 + 0.5); ybh2 = 0. #BHs on x axis
    #ybh2 = -r12 + 0.5; xbh2 = 0.  #BHs on y axis
    
    #Initialize BL grid to calculate on in spherical coords
    rrnew = np.linspace(RMIN,RMAX,1500*(r12/20))
    phnew = np.linspace(0,2.*np.pi,1500)
    rrnew,phnew = np.meshgrid(rrnew,phnew,indexing='ij')
    #Calculate Cartesian Coordinates
    xxnew = rrnew*np.cos(phnew); yynew = rrnew*np.sin(phnew)    

    #Calculate r2 and r1 \cdot r2
    r2     = np.sqrt( (xxnew - xbh2)*(xxnew - xbh2) + (yynew - ybh2)*(yynew - ybh2) )
    r1dotD = xxnew*xbh2 + yynew*ybh2
    magD   = r12+0.5

    #Calculate ONLY perturbing potential
    pot_newt = 0.5 * ( -1./r2 + r1dotD/np.power(magD,3) )
    pot_newt[ phnew == 2. * np.pi] = 0. #cause we need the modes to not double count first wedge

    #2D problem in cylindrical coords (\sqrt{-g} = r)
    newtg = np.zeros((np.shape(pot_newt))); newtg = rrnew

    #Calculate Fourrier series coefficients
    m0_pot = zero_mode(pot_newt,rrnew,phnew,drnew,dphnew,newtg)
    a1_pot = an       (pot_newt,rrnew,phnew,drnew,dphnew,newtg,1)
    a2_pot = an       (pot_newt,rrnew,phnew,drnew,dphnew,newtg,2)
    b1_pot = bn       (pot_newt,rrnew,phnew,drnew,dphnew,newtg,1)
    b2_pot = bn       (pot_newt,rrnew,phnew,drnew,dphnew,newtg,2)

    #Calculate mode strength
    m1pot_newt = np.sqrt(a1_pot*a1_pot + b1_pot*b1_pot)/m0_pot
    m2pot_newt = np.sqrt(a2_pot*a2_pot + b2_pot*b2_pot)/m0_pot
    #print("sep ",r12, " Newtonian Potential modes 1 , 2 ratio ", m1pot_newt,m2pot_newt, m1pot_newt/m2pot_newt)

    return r12, m1pot_newt, m2pot_newt
    #return np.array([r12, m1pot_newt, m2pot_newt]) #pack within an array if desired





def get_disk_modes(dirname='.',trajfile=None,funcname='sigma',TMIN=0,TMAX=None,RMIN=None,RMAX=0.35):
    '''
     Calculates the Fourrier mode analysis for the disk around the primary
      Reads in the history of func[tindex,rrindex,phindex] from 'alpha_history.h5'      

    returns:
       tt,m1,m2,phi1,phi2 -> time, m=1/2 mode strength, m=1/2 angular positions
    '''
    #Read in data
    h5file = h5py.File(dirname + '/alpha_history.h5','r')
    tt     = h5file['t'].value
    rr     = h5file['rrBL'].value
    ph     = h5file['phBL'].value
    sep    = h5file['sep'].value #binary separation
    func   = h5file[funcname].value
    h5file.close()

    #Set default behavior
    if TMAX is None:
        TMAX = np.max(tt)
    if trajfile is None:
        trajfile = dirname + '/traj/my_bbh_trajectory.out'
    
    #Spacing of the grid for mode analysis
    dr  = rr[1,0] - rr[0,0]
    dph = ph[0,1] - ph[0,0]

    #sigma.fill(1) #; rr.fill(1) #Uncomment to fill everything with const value to test mode analysis

    #Zero out ph = 2.pi to avoid double counting
    func[:,ph == 2. * np.pi] = 0.

    #Remove elements not within our time-sample and radial sample (creates 1d arrays to integrate on)
    print(np.shape(RMIN))
    sep  = sep[ (TMIN <= tt) & (tt <= TMAX)                            ]
    func = func[(TMIN <= tt) & (tt <= TMAX),:]
    tt   = tt[  (TMIN <= tt) & (tt <= TMAX)                            ]

    #Set Radial Integration Bounds
    RMAX *= sep
    if RMIN is None:
        RMIN = np.zeros((np.shape(sep))); RMIN.fill(1.)
    else:
        RMIN *= sep
    
    #Calculate Fourrier Series Coefficients (approximate detg as rr)
    #Pass only portions of the arrays in integration bounds for each time-slice (recast to numpy arrays)
    m0 = np.array([zero_mode(func[tindex,(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],ph[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],dr,dph,rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])] ) for tindex in xrange(0,len(tt))])
    a1 = np.array([an(func[tindex,(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],ph[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],dr,dph,rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],1  ) for tindex in xrange(0,len(tt))])
    a2 = np.array([an(func[tindex,(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],ph[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],dr,dph,rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])], 2  ) for tindex in xrange(0,len(tt))])
    b1 = np.array([bn(func[tindex,(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],ph[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],dr,dph,rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])], 1  ) for tindex in xrange(0,len(tt))])
    b2 = np.array([bn(func[tindex,(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],ph[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])],dr,dph,rr[(RMIN[tindex] <= rr) & (rr <= RMAX[tindex])], 2  ) for tindex in xrange(0,len(tt))])

    #Calculate Mode Strength
    m1 = np.sqrt(a1*a1 + b1*b1) / m0
    m2 = np.sqrt(a2*a2 + b2*b2) / m0

    #Calculate Mode Positions (normalize for visualization ease)
    phi1 = np.arctan2(b1,a1); phi1[phi1 < 0.] += 2. * np.pi
    phi2 = np.arctan2(b2,a2); #phi2[phi2 < 0.] += 2. * np.pi

    return tt, m1, m2, phi1, phi2


def main():
    #tt, m1, m2, phi1, phi2 = get_disk_modes(dirname='.',trajfile=None,funcname='sigma',TMIN=0,TMAX=None,RMIN=None,RMAX=0.35)
    a, m1pot, m2pot = get_metric_modes(tindex=0,RMAX=0.4)
    print("Sep r12 m1pot m2pot m1pot/m2pot ", a, m1pot, m2pot,m1pot/m2pot)
    a, m1pot, m2pot = get_metric_modes(tindex=0,dirname='../a100_hydrostatic_eq_data_large_disks',RMAX=0.4)
    print("Sep r12 m1pot m2pot m1pot/m2pot ", a, m1pot, m2pot,m1pot/m2pot)
    #fig = plt.figure(0)
    #ax  = fig.gca()
    #ax.plot(tt,m1)
    #ax.plot(tt,m2)
    #fig = plt.figure(1)
    #ax = fig.gca()
    #ax.plot(tt,phi1)
    #ax.plot(tt,phi2)
    #plt.show()
if __name__ == '__main__':
    main()
#result = np.array([get_metric_modes(tindex) for tindex in xrange(0,2)])
#print(result[0,0])
#print(result[1,1])






#plt.show()



