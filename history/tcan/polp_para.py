from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import h5py
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib.colors import Colormap
from scipy.interpolate import interp1d 
from coord_transforms import *
from harm import *
from multiprocessing import Pool


###############################################################################
###############################################################################
def polp(itime=0, dirname=None, funcname=None, func0=None,
         irad=None, ith=None, iph=0,
         rmin=None, rmax=None, minf=None, maxf=None,
         xmax=None,ymax=None,xmin=None,ymin=None,zmax=None,zmin=None,
         nlev=256, nticks=10, ticks_range='minmax',
         use_num_coord=False,cart_coords=False,
         plot_grid=False, 
         nperplot=20, savepng=False, dirout=None, dolog=False, show_plot=True,
         plottitle=None, userad=False,
         plot_bhs_traj=False, trajfile=None, plot_horizon=False,
         nwin=0, background='black', namesuffix='', plotcentre=None,
         ax=None, axislabels=True, xticks=None, yticks=None, yticksloc='left', retfunc=True, fillwedge=True,
         ubh=False,visc_circles=False, fillcutout=False,corotate=False,get_time=False,
         use_stat2=False,stat2_factor=0,use_conserved_vars=False,
         scale=True,axis=None,take_ratio=False,relerror=False,usegdump=False,compare_runs=False,compare_dir=None,spinning=False,
         R1T0=None, plot_potential=False,pminf=None,pmaxf=None,pnlev=10,pfontsize=8,pot_labels=True,plot_slosh_box=False,normalize_func=False,
         EXTEND_CENTRAL_CUTOUT=False,freescale=False,
         slice_obj=None,with_gdet=False,just_get_data=False,rho_cutout_value=None,
         dumpfile=None,radfile=None,gdumpfile=None,run_tag='KDHARM0',other_data=None,move_patch=False,
         artists=None,make_bbox=False
     ):

    # nticks    -> number of ticks for the colorbar
    # plot_grid ->  plot numerical grid?

    slice_obj_all = (slice(None,None,None),slice(None,None,None),slice(None,None,None))

    if show_plot is False:
        # Turn interactive plotting off
        plt.ioff()

    if use_num_coord:
        plot_horizon  = False
        plot_bhs_traj = False
        plot_grid     = False

#-scn ok
#     if ith is None:
#         plot_bhs_traj = False        

    if background is 'black':
        grid_colour = 'white'
    else:
        grid_colour = 'black'

    if plotcentre is not None:
         plot_bhs_traj=True

    if dirname is None:
        dirname = os.getcwd()  # should tell me my current working directory,
                               # *not* where this script is located

    if trajfile is None:
        if ubh is False:
            trajfile = dirname + '/dumps/KDHARM0_bbh_trajectory.out'
	    #trajfile = dirname + '/my_bbh_trajectory.out'
    if ubh is True:
        ubh_horizon = True
    else:
        ubh_horizon = False
    
    if irad is not None and corotate : 
        print(" corotate is not implemented for th-phi plots yet!! ")
        return

    timeid = '%06d' %itime #harm HDF5 dumps
    #timeid = '%05d' %itime #monopole cleaner dumps

    if use_stat2:
        timestat2 = '%06d' %(itime - stat2_factor)
        stat2file = dirname + '/dumps/' + run_tag + '.STAT2.' + timestat2 + '.h5'

    if dumpfile is None: 
        dumpfile  = dirname + '/dumps/' + run_tag + '.'  +  timeid + '.h5'

    if radfile is None: 
        radfile   = dirname + '/dumps/' + run_tag + '.RADFLUX.' +  timeid + '.h5'

    if gdumpfile is None: 
        gdumpfile = dirname + '/dumps/' + run_tag + '.gdump.h5'


    fullname = dumpfile

    h5file   = h5py.File(fullname, 'r')

    idim, jdim, kdim, xp1, xp2, xp3 = get_grid(h5file)
    ndims = np.array([idim,jdim,kdim])


    # we get the time level:
    time = h5file['Header/Grid/t'][0]
#    time = 5e-3
    print("time = ", time)

    if ith is 'equator':
        ith = int(ndims[1]/2)
        if ndims[1] == 1:
            EQUATORIAL_RUN = True
        if funcname == 'dM' or funcname == 'dMdt':
            SUM_THETA = True
#    if ith is None and (corotate or iph is None):
    if corotate and ith is None and iph is not None:
        if( spinning ):
            tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,1,2,4,5),unpack=True) #spinning
        else:
            tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,4,5,6,7),unpack=True) #old metrics

        fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
        fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
        fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
        fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

        xbh1  = fxbh1(time)#/np.sin(theta)
        ybh1  = fybh1(time)#/np.sin(theta)
        xbh2  = fxbh2(time)#/np.sin(theta)
        ybh2  = fybh2(time)#/np.sin(theta)

        phase = np.arctan2(ybh1,xbh1)
        #renormalize phase s.t phase \in (0,2pi)
        if phase < 0.:
            phase = 2. * np.pi - np.abs(phase)
        dxp3 = h5file['Header/Grid/dx3'].value[0]
        kshift = int( (phase) / ( 2. * np.pi * dxp3 ) - 0. + 0.5 )
        phasediffmin = 2.e6
        print("kdim = ", kdim)
        slice_obj_tmp = (slice(0,1,1),slice(0,1,1),slice(None,None,None))
        ph_tmp   = read_3d_hdf5_slice('x3',slice_obj_tmp,h5file)
        for index in range(kdim):
            phasediff = np.abs(ph_tmp[index] - phase)
            if phasediffmin > phasediff:
                phasediffmin = phasediff
                indexcrit = index
        print("Expected shift found shift ",kshift,indexcrit)
        iph = kshift + 1

    # set grid coordinate functions
    if( slice_obj is None ) : 
        slice_obj = calc_slice(irad,ith,iph,ndims)

    rr   = read_3d_hdf5_slice('x1',slice_obj,h5file)   # 'x1' is the name of the dataset inside the hdf5 file
    th   = read_3d_hdf5_slice('x2',slice_obj,h5file)
    ph   = read_3d_hdf5_slice('x3',slice_obj,h5file)


    print("##################################")
    print(" rr.shape()  = ", np.shape(rr))

    rr_slice = rr
    ph_slice = ph
    th_slice = th

    if rmin is None:
        rmin = np.min(rr)
    if rmax is None:
        rmax = np.max(rr)

#     if userad:
#         fullname = radfile
#         h5file.close()
#         h5file = h5py.File(fullname, 'r')

    if usegdump:
        fullname = gdumpfile
        h5file.close()
        h5file = h5py.File(fullname, 'r')

    if use_stat2:
        #fullname = stat2file
        #h5file.close()
        #h5file = h5py.File(stat2file,'r')
        h5file = h5py.File(fullname,'r')
        h5stat2 = h5py.File(stat2file,'r')

    if userad:
        h5file2 = h5py.File(radfile, 'r')
    else:
        h5file2 = h5py.File(dumpfile, 'r')

    if irad is not None : 
        print("r of slice = ", rr_slice[0,0])



    # if we've specified something for func0 we will plot it instead.
    if func0 is not None:
        func = func0
    elif funcname is 'uconrr':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
        h5file2.close()
        #uconrr, uconth, uconph = calc_uconKS(slice_obj, h5file, v1, v2, v3) #assumes diagonal metric to get velocities
        ucon0, uconrrp, uconthp, uconphp = calc_ucon(slice_obj, h5file, v1, v2, v3)  #get ucon wrt numerical coordinates
        #non-dynamic coords
        #uconrr, uconth, uconph = uconp2ucon(slice_obj,h5file,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
        #dynamic coordinates
        ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(slice_obj, h5file, ucon0,uconrrp,uconthp,uconphp)
        if met_type == 2:
            ucon0, uconrr, uconth, uconph =  ks2bl_con(slice_obj, h5file,uconrr,uconth,uconph) #convert from ks to bl if necessary...
       

        uconth = 0
        uconph = 0
        func   = uconrr  / ucon00 #should be uconrr dennis
    elif funcname is 'uconrrbh' or funcname is 'vrbh':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
        h5file2.close()

        ucon0, uconrrp, uconthp, uconphp = calc_ucon(slice_obj, h5file,v1,v2,v3) #get ucon wrt numerical coordinates
        #dynamic coordinates
        ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(slice_obj, h5file,ucon0,uconrrp,uconthp,uconphp)
        
        r1max = 45.
        r2max = 45.
        tt = np.zeros((np.shape(rr))); tt.fill(time)
        uconttb, uconrrb, uconthb, uconphb = rank1con2BH1(trajfile,tt,rr,th,ph,ucon00,uconrr,uconth,uconph,dx_dxp=None,SETPHASE=None)
       
        if funcname is 'uconrrbh':
            func = uconrrb
        elif funcname is 'vrbh':
            func = uconrrb/uconttb

        #Normalize to adot
        tbh_traj, r12_traj = np.loadtxt(trajfile,usecols=[0,23],unpack=True)
        adot_traj          = np.gradient(r12_traj,tbh_traj[1] - tbh_traj[0])
        fadot              = interp1d(tbh_traj,adot_traj,kind='linear')
        print(fadot(time), "Normalizing vr by adot")
        func              /= fadot(time)
    elif funcname is 'uconth':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
        h5file2.close()
        #uconrr, uconth, uconph = calc_uconKS(slice_obj, h5file, v1, v2, v3) #assumes diagonal metric to get velocities
        ucon0, uconrrp, uconthp, uconphp = calc_ucon(slice_obj, h5file,v1,v2,v3) #get ucon wrt numerical coordinates
        #non-dynamic coords
        #uconrr, uconth, uconph = uconp2ucon(slice_obj, h5file,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
        #dynamic coordinates
        ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(slice_obj, h5file,ucon0,uconrrp,uconthp,uconphp)
        if met_type == 2:
            ucon0, uconrr, uconth, uconph =  ks2bl_con(slice_obj, h5file,uconrr,uconth,uconph) #convert from ks to bl if necessary...

        uconrr = 0
        uconph = 0
        func   = uconth
    elif funcname is 'uconph':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
        h5file2.close()
        #uconrr, uconth, uconph = calc_uconKS(slice_obj, h5file, v1, v2, v3) #assumes diagonal metric to get physical ucon
        ucon0, uconrrp, uconthp, uconphp = calc_ucon(slice_obj, h5file,v1,v2,v3) #returns ucon wrt numerical coordinates
        #non-dynamic coords
        #uconrr, uconth, uconph = uconp2ucon(slice_obj,h5file,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
        #dynamic coordinates
        ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(slice_obj, h5file,ucon0,uconrrp,uconthp,uconphp)
        if met_type == 2:
            ucon0, uconrr, uconth, uconph =  ks2bl_con(slice_obj, h5file,uconrr,uconth,uconph) #convert from ks to bl if necessary...

        uconth = 0
        uconrr = 0
        func   = uconph #should be uconph dennis here
    elif funcname is 'uconph_bh' or funcname is 'omega_bh':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
        h5file2.close()

        ucon0, uconrrp, uconthp, uconphp = calc_ucon(slice_obj,h5file,v1,v2,v3) #get ucon wrt numerical coordinates
        #dynamic coordinates
        ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(slice_obj,h5file,ucon0,uconrrp,uconthp,uconphp)
        
#         uconCM = np.zeros((uconrr.shape[0],uconrr.shape[1],uconrr.shape[2],4))
#         uconCM[:,:,:,0] = ucon00[:,:,:]
#         uconCM[:,:,:,1] = uconrr[:,:,:]
#         uconCM[:,:,:,2] = uconth[:,:,:]
#         uconCM[:,:,:,3] = uconph[:,:,:]
        r1max = 50.
        r2max = 50.
        #uconttb,uconrrb,uconthb,uconphb = ucon2bhframes(trajfile, uconCM,rr,ph,r1max,r2max,time)
        dx_dxp = get_dx_dxp(slice_obj,h5file)
        TNZ = np.zeros(np.shape(rr)); TNZ = TNZ.fill(time)
        uconttb,uconrrb,uconthb,uconphb = rank1con2BH1(trajfile,TNZ,rr,th,ph,ucon00,uconrr,uconth,uconph)
        #print(np.min(ucon0),np.min(ucon00),np.min(uconttb))
        #print(ucon00 - ucon0)
        if funcname is 'uconph_bh':
            func = uconphb
        elif funcname is 'omega_bh':
            func = uconrrb/uconttb #DENNIS WRONG
    elif funcname is 'ucon0_special':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        h5file2.close()
        ucon0, ucon1, ucon2, ucon3 = calc_ucon(slice_obj, h5file, v1, v2, v3)
        ucon1 = 0
        ucon2 = 0
        ucon3 = 0
        func  = ucon0
    elif funcname is 'ucon1_special':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        h5file2.close()
        ucon0, ucon1, ucon2, ucon3 = calc_ucon(slice_obj,h5file, v1, v2, v3)
        ucon0 = 0
        ucon2 = 0
        ucon3 = 0
        func  = ucon1
    elif funcname is 'ucon2_special':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        h5file2.close()
        ucon0, ucon1, ucon2, ucon3 = calc_ucon(slice_obj,h5file, v1, v2, v3)
        ucon0 = 0
        ucon1 = 0
        ucon3 = 0
        func  = ucon2
    elif funcname is 'ucon3_special':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        h5file2.close()
        ucon0, ucon1, ucon2, ucon3 = calc_ucon(slice_obj,h5file, v1, v2, v3)
        ucon0 = 0
        ucon1 = 0
        ucon2 = 0
        func  = ucon3

    elif funcname is 'ucov0':
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        h5file2.close()
        metric = get_metric(slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3 = calc_ucon(slice_obj,h5file, v1, v2, v3)
        ucov0, ucov1, ucov2, ucov3 = lower2(metric,ucon0,ucon1,ucon2,ucon3)
        func  = ucov0

    elif funcname is 'temp':
        USE_SIMPLE_EOS = h5file['Header/Macros/USE_SIMPLE_EOS'].value[0]
        if( USE_SIMPLE_EOS ):
            print("Using ISOTHERMAL EOS")
            pressure = read_3d_hdf5_slice( 'B1',slice_obj,h5file)
            rho      = read_3d_hdf5_slice('rho',slice_obj,h5file)
            #assume Isothermal EOS
            func = pressure / rho
        else:
            print("Using GAMMA-LAW EOS")
            gam = h5file['Header/Grid/gam'].value[0]
            uu  = read_3d_hdf5_slice( 'uu',slice_obj,h5file)
            rho = read_3d_hdf5_slice('rho',slice_obj,h5file)
            func = (gam - 1.)*uu/rho
    elif funcname is 'dtmin1':
        dx1 = h5file['Header/Grid/dx1'].value[0]
        vmax = vchar(fullname,1)
        func = np.abs(dx1 / vmax)
        print("dtmin1 found to be ",np.min(func), " at coords (r,th,ph) = ",rr[func == np.min(func)],th[func == np.min(func)],ph[func == np.min(func)])
    elif funcname is 'dtmin2':
        dx2 = h5file['Header/Grid/dx2'].value[0]
        vmax = vchar(fullname,2)
        func = np.abs(dx2 / vmax)
        print("dtmin2 found to be ",np.min(func), " at coords (r,th,ph) = ",rr[func == np.min(func)],th[func == np.min(func)],ph[func == np.min(func)])
    elif funcname is 'dtmin3':
        dx3 = h5file['Header/Grid/dx3'].value[0]
        vmax = vchar(fullname,3)
        func = np.abs(dx3 / vmax)
        print("dtmin3 found to be ",np.min(func), " at coords (r,th,ph) = ",rr[func == np.min(func)],th[func == np.min(func)],ph[func == np.min(func)])
        print( np.where(func == func.min()) )
    elif funcname is 'cs':
        #sqrt{gam p / rho}
        gam = h5file['Header/Grid/gam'].value[0]
        uu  = read_3d_hdf5_slice( 'uu',slice_obj,h5file)
        rho = read_3d_hdf5_slice('rho',slice_obj,h5file)
        p   = (gam - 1.)*uu
        #func = np.sqrt(gam) * p / rho 
        func = np.sqrt(gam * p / rho)
    elif funcname is 'cooling':
        gam = h5file['Header/Grid/gam'].value[0]
        uu  = read_3d_hdf5_slice( 'uu',slice_obj,h5file)
        rho = read_3d_hdf5_slice('rho',slice_obj,h5file)
        S0  = 0.01
        S   = (gam - 1.)*uu / np.power(rho,gam)
        Sterm = (S - S0)/S0
        #calculate BL
        TNZ = np.zeros((np.shape(rr))); TNZ.fill(time)
        XNZ = rr * np.sin(th) * np.cos(ph)
        YNZ = rr * np.sin(th) * np.sin(ph)
        ZNZ = rr * np.cos(th)

        if( spinning ):
            tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,1,2,4,5),unpack=True) #spinning
        else:
            tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,4,5,6,7),unpack=True) #old metrics

        fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
        fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
        fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
        fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

        theta = th_slice[0,0]

        xbh1  = fxbh1(time)/np.sin(theta)
        ybh1  = fybh1(time)/np.sin(theta)
        xbh2  = fxbh2(time)/np.sin(theta)
        ybh2  = fybh2(time)/np.sin(theta)
        sep   = np.sqrt( (xbh1 - xbh2)*(xbh1 - xbh2) + (ybh1 - ybh2)*(ybh1 - ybh2) )
        
        #Assume that q = 1 ( so m1 = m2 = 0.5 )
        rBL = rr + 1.
        rBL[rBL < 1.5 * sep] = 1.5 * sep
        r1  = np.sqrt( (XNZ - xbh1)*(XNZ - xbh1) + (YNZ - ybh1)*(YNZ - ybh1) + (ZNZ)*(ZNZ) ) + 0.5
        r2  = np.sqrt( (XNZ - xbh2)*(XNZ - xbh2) + (YNZ - ybh2)*(YNZ - ybh2) + (ZNZ)*(ZNZ) ) + 0.5
        #Reset to ISCO inside ISCO
        r1[ r1 <= 3. ] = 3.
        r2[ r2 <= 3. ] = 3.

        Tcool = 2. * np.pi * np.power( rBL, 1.5 )
        Tcool[r1 < 0.45 * sep] = 2. * np.pi * np.power( r1[r1 < 0.45 * sep], 1.5 ) / np.sqrt(0.5)
        Tcool[r2 < 0.45 * sep] = 2. * np.pi * np.power( r2[r2 < 0.45 * sep], 1.5 ) / np.sqrt(0.5)

        func = (uu/Tcool) * ( Sterm + np.abs(Sterm) ) #/ rho #divide by rho to get cooling per unit mass

    elif funcname is 'entropy':
        rho = read_3d_hdf5_slice('rho',slice_obj,h5file)
        uu  = read_3d_hdf5_slice( 'uu',slice_obj,h5file)
        gam = h5file['Header/Grid/gam'].value
        p   = (gam - 1.)*uu
        func = p / np.power(rho,gam)    

    elif funcname is 'beta1':
        metric = get_metric(slice_obj,h5file)
        func = metric['beta1']

    elif funcname is 'beta2':
        metric = get_metric(slice_obj,h5file)
        func = metric['beta2']

    elif funcname is 'beta3':
        metric = get_metric(slice_obj,h5file)
        func = metric['beta3']

    elif funcname is 'alpha':
        metric = get_metric(slice_obj,h5file)
        func = metric['alpha']

    elif funcname is 'ncon0':
        metric = get_metric(slice_obj,h5file)
        func = 1./metric['alpha']
    
    elif funcname is 'gcon00':
        metric = get_metric(slice_obj,h5file)
        func = metric['gcon00']

    elif funcname is 'potential':
        transformBL = False #whether to plot wrt BL or PN coords
        scalesep    = True
        metric = get_metric(slice_obj,h5file)
        gcon = { '00':metric['gcon00'], '01':metric['gcon01'], '02':metric['gcon02'], '03':metric['gcon03'],
             '11':metric['gcon11'], '12':metric['gcon12'], '13':metric['gcon13'],
             '22':metric['gcon22'], '23':metric['gcon23'],
             '33':metric['gcon33'] }
        if( transformBL ):
            ttNZ   = np.zeros((np.shape(rr)))
            ttNZ.fill(time)
            dx_dxp = get_dx_dxp(slice_obj,h5file)
            metricBL = metric2BL(1, trajfile, ttNZ, rr, th, ph, dx_dxp, metric)
            func = -0.5 * ( metric['gcov00'] + 1. )
        else:
            dx_dxp  = get_dx_dxp(slice_obj,h5file)
            dxc_dxs = get_dxc_dxs( rr, th, ph )
            gcon    = transform_gcon( dx_dxp, gcon ) #Spherical PN Coords (Physical)
            if( corotate ):
                '''
                rewrite dx_dxp to be jacobian to corotating frame 
                  dx_dxp = |   1    0 0 0 |
                           |   0    1 0 0 |
                           |   0    0 1 0 |
                           | -omega 0 0 1 |
                '''
                if( spinning ):
                    tbh_traj, omega_bin_traj = np.loadtxt(trajfile,usecols=(0,16),unpack=True) #spinning
                else:
                    tbh_traj, omega_bin_traj = np.loadtxt(trajfile,usecols=(0,24),unpack=True) #old metrics

                fomega_bin = interp1d(tbh_traj, omega_bin_traj, kind='linear')            
                omega_bin = fomega_bin(time)
                dx_dxp['00'].fill(1.);         dx_dxp['01'].fill(0.); dx_dxp['02'].fill(0.); dx_dxp['03'].fill(0.)
                dx_dxp['10'].fill(0.);         dx_dxp['11'].fill(1.); dx_dxp['12'].fill(0.); dx_dxp['13'].fill(0.)
                dx_dxp['20'].fill(0.);         dx_dxp['21'].fill(0.); dx_dxp['22'].fill(1.); dx_dxp['23'].fill(0.)
                dx_dxp['30'].fill(-omega_bin); dx_dxp['31'].fill(0.); dx_dxp['32'].fill(0.); dx_dxp['33'].fill(1.)
                gcon    = transform_gcon( dx_dxp,  gcon) # Corotating Frame ( spherical coords )
            gcon    = transform_gcon( dxc_dxs, gcon) #Cartesian PN Coords (Physical)
            #Get gcov from gcon
            gcov    = get_ginverse(gcon)            
            func    = -0.5 * ( gcov['00'] + 1. )
            if( scalesep ):
                sep = h5file['Header/Grid/initial_bbh_separation'].value[0]
                func *= sep
    elif funcname is 'geom_alpha':
        metric = get_metric(slice_obj,h5file)
        func = metric['alpha']
    elif funcname is 'D':
        rho = read_3d_hdf5_slice(   'rho',slice_obj,h5file)
        gam = read_3d_hdf5_slice( 'gamma',slice_obj,h5file)
        func = rho * gam
    elif funcname is 'mass_flux':
        #mass flux wrt numerical coordinates
        v1      = read_3d_hdf5_slice('v1',slice_obj,h5file2)
        v2      = read_3d_hdf5_slice('v2',slice_obj,h5file2)
        v3      = read_3d_hdf5_slice('v3',slice_obj,h5file2)
        h5file2.close()
        #uconrr, uconth, uconph = calc_uconKS(slice_obj, h5file, v1, v2, v3) #assumes diagonal metric to get velocities
        ucon0, uconrrp, uconthp, uconphp = calc_ucon(slice_obj,h5file,v1,v2,v3) #get ucon wrt numerical coordinates
        
        rho  = read_3d_hdf5_slice(  'rho',slice_obj,h5file)
        gdet = read_3d_hdf5_slice( 'gdet',slice_obj,h5file)

        func   = uconrrp * rho * gdet
    elif funcname is 'dg_dxp1' or funcname is 'dg_dxp2' or funcname is 'dg_dxp3':
        gdet = read_3d_hdf5_slice('gdet',slice_obj,h5file)
        dxp1 = h5file['Header/Grid/dx1'].value[0]
        dxp2 = h5file['Header/Grid/dx2'].value[0]
        dxp3 = h5file['Header/Grid/dx3'].value[0]
        if EQUATORIAL_RUN:
            dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
            func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
        else:
            dg_dxp1, dg_dxp2, dg_dxp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
        if funcname is 'dg_dxp1':
            if EQUATORIAL_RUN:
                func[:,0,:] = dg_dxp1[:,:]                
            else:
                func = dg_dxp1
        elif funcname is 'dg_dxp2':
            func = dg_dxp2
        elif funcname is 'dg_dxp3':
            if EQUATORIAL_RUN:
                func[:,0,:] = dg_dxp3[:,:]
            else:
                func = dg_dxp3
    elif funcname is 'd2g_dxp11' or funcname is 'd2g_dxp12' or funcname is 'd2g_dxp13':
        gdet = read_3d_hdf5_slice('gdet',slice_obj,h5file)
        dxp1 = h5file['Header/Grid/dx1'].value[0]
        dxp2 = h5file['Header/Grid/dx2'].value[0]
        dxp3 = h5file['Header/Grid/dx3'].value[0]
        if EQUATORIAL_RUN:
            dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
            d2g_dxp11, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp3)            
            func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
            if funcname is 'd2g_dxp11':
                func[:,0,:] = d2g_dxp11[:,:]
            elif funcname is 'd2g_dxp13':
                func[:,0,:] = d2g_dxp13[:,:]
        else:
            dg_dxp1, dg_xp2, dg_xp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
            d2g_dxp11, d2g_dxp12, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp2,dxp3)
            if funcname is 'd2g_dxp11':
                func = d2g_dxp11
            elif funcname is 'd2g_dxp12':
                func = d2g_dxp12
            elif funcname is 'd2g_dxp13':
                func = d2g_dxp13
    elif funcname is 'd2g_dxp21' or funcname is 'd2g_dxp22' or funcname is 'd2g_dxp23':
        gdet = read_3d_hdf5_slice('gdet',slice_obj,h5file)
        dxp1 = h5file['Header/Grid/dx1'].value[0]
        dxp2 = h5file['Header/Grid/dx2'].value[0]
        dxp3 = h5file['Header/Grid/dx3'].value[0]        
        dg_dxp1, dg_xp2, dg_xp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
        d2g_dxp21, d2g_dxp22, d2g_dxp23 = np.gradient(dg_dxp2,dxp1,dxp2,dxp3)
        if funcname is 'd2g_dxp21':
            func = d2g_dxp21
        elif funcname is 'd2g_dxp22':
            func = d2g_dxp22
        elif funcname is 'd2g_dxp23':
            func = d2g_dxp23
    elif funcname is 'd2g_dxp31' or funcname is 'd2g_dxp32' or funcname is 'd2g_dxp33':
        gdet = read_3d_hdf5_slice('gdet',slice_obj,h5file)
        dxp1 = h5file['Header/Grid/dx1'].value[0]
        dxp2 = h5file['Header/Grid/dx2'].value[0]
        dxp3 = h5file['Header/Grid/dx3'].value[0]
        if EQUATORIAL_RUN:
            dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
            d2g_dxp31, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp3)            
            func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
            if funcname is 'd2g_dxp31':
                func[:,0,:] = d2g_dxp31[:,:]
            elif funcname is 'd2g_dxp33':
                func[:,0,:] = d2g_dxp33[:,:]
        else:
            dg_dxp1, dg_xp2, dg_xp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
            d2g_dxp31, d2g_dxp32, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp2,dxp3)
            if funcname is 'd2g_dxp31':
                func = d2g_dxp31
            elif funcname is 'd2g_dxp32':
                func = d2g_dxp32
            elif funcname is 'd2g_dxp33':
                func = d2g_dxp33
    elif funcname is 'gradg_rat_x1' or funcname is 'gradg_rat_x2' or funcname is 'gradg_rat_x3':
        gdet = read_3d_hdf5_slice('gdet',slice_obj,h5file)
        dxp1 = h5file['Header/Grid/dx1'].value[0]
        dxp2 = h5file['Header/Grid/dx2'].value[0]
        dxp3 = h5file['Header/Grid/dx3'].value[0]
        if EQUATORIAL_RUN:
            dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
            d2g_dxp11, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp3)
            d2g_dxp31, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp3)            
            func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
            if funcname is 'gradg_rat_x1':
                func[:,0,:] = dxp1 * d2g_dxp11[:,:] / dg_dxp1[:,:]
            elif funcname is 'gradg_rat_x3':
                func[:,0,:] = dxp3 * d2g_dxp33[:,:] / dg_dxp3[:,:]
        else:
            dg_dxp1, dg_xp2, dg_xp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
            d2g_dxp11, d2g_dxp12, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp2,dxp3)
            d2g_dxp21, d2g_dxp22, d2g_dxp32 = np.gradient(dg_dxp2,dxp1,dxp2,dxp3)
            d2g_dxp31, d2g_dxp32, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp2,dxp3)
            if funcname is 'gradg_rat_x1':
                func = dxp1 * d2g_dxp11 / dg_dxp1
            elif funcname is 'gradg_rat_x2':
                func = dxp2 * d2g_dxp22 / dg_dxp2
            elif funcname is 'gradg_rat_x3':
                func = dxp3 * d2g_dxp33 / dg_dxp3
    elif funcname is 'dr':
        func = np.diff(read_3d_hdf5_slice('x1',slice_obj,h5file),axis=0)
        zerrarr = np.zeros((1,np.shape(func)[1],np.shape(func)[2]))
        zerrarr[0,:,:] = func[0,:,:]
        func = np.append(func,   zerrarr,   axis=0)

    elif funcname is 'dth':
        x1 = read_3d_hdf5_slice('x1',slice_obj,h5file)
        x2 = read_3d_hdf5_slice('x2',slice_obj,h5file)
        x2 = np.diff(x2,axis=1)
        zerrarr = np.zeros((np.shape(x2)[0],1,np.shape(x2)[2]))
        zerrarr[:,0,:] = x2[:,0,:]
        x2 = np.append(x2,   zerrarr,   axis=1)
        func = x1 * x2
    elif funcname is 'dph':
        x1 = read_3d_hdf5_slice('x1',slice_obj,h5file)
        x3 = read_3d_hdf5_slice('x3',slice_obj,h5file)
        x3 = np.diff(x3,axis=2)
        print("need to fix this option")
        zerrarr = np.zeros((np.shape(x3)[0],np.shape(x3)[1],1))
        zerrarr[:,:,0] = x3[:,:,0]
        x3 = np.append(x3,   zerrarr,   axis=2)
        func = x1 * x3
    elif funcname is 'testclean':        
        print("Testing Cleaner Status target is 1.e-3")
        print("READ GFUNCS")
        dx1  = h5file['Header/Grid/dx1'].value[0]
        dx2  = h5file['Header/Grid/dx2'].value[0]
        dx3  = h5file['Header/Grid/dx3'].value[0] 
        divb = read_3d_hdf5_slice('divb',slice_obj,h5file)
        B1   = read_3d_hdf5_slice(  'B1',slice_obj,h5file)
        B2   = read_3d_hdf5_slice(  'B2',slice_obj,h5file)
        B3   = read_3d_hdf5_slice(  'B3',slice_obj,h5file)
        v1   = read_3d_hdf5_slice(  'v1',slice_obj,h5file)
        v2   = read_3d_hdf5_slice(  'v2',slice_obj,h5file)
        v3   = read_3d_hdf5_slice(  'v3',slice_obj,h5file)
        #Try reading gcov to do better on memory
        gcov00 = read_3d_hdf5_slice('gcov00',slice_obj,h5file)
        gcov01 = read_3d_hdf5_slice('gcov01',slice_obj,h5file)
        gcov02 = read_3d_hdf5_slice('gcov02',slice_obj,h5file)
        gcov03 = read_3d_hdf5_slice('gcov03',slice_obj,h5file)
        gcov11 = read_3d_hdf5_slice('gcov11',slice_obj,h5file)
        gcov12 = read_3d_hdf5_slice('gcov12',slice_obj,h5file)
        gcov13 = read_3d_hdf5_slice('gcov13',slice_obj,h5file)
        gcov22 = read_3d_hdf5_slice('gcov22',slice_obj,h5file)
        gcov23 = read_3d_hdf5_slice('gcov23',slice_obj,h5file)
        gcov33 = read_3d_hdf5_slice('gcov33',slice_obj,h5file)

        #print("get metric")
        #metric = get_metric(slice_obj,h5file)
        #Get min dx^i
        print("Get min dx^i")        
        dxmin = min(min(dx1,dx3),min(dx2,dx3))
        #print(dx1,dx2,dx3,dxmin)
        #Get bsq
        print("calc_ucon")
        ucon0, ucon1, ucon2, ucon3 = calc_ucon(fullname,v1,v2,v3)
        print("lower ucon")
        #ucov0, ucov1, ucov2, ucov3 = lower2(metric,ucon0,ucon1,ucon2,ucon3)
        ucov0, ucov1, ucov2, ucov3 = lower(gcov00,gcov01,gcov02,gcov03,gcov11,gcov12,gcov13,gcov22,gcov23,gcov33,ucon0,ucon1,ucon2,ucon3)
        print("bcon_calc")
        bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
        print("lower bcon")
        #bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)
        bcov0, bcov1, bcov2, bcov3 = lower(gcov00,gcov01,gcov02,gcov03,gcov11,gcov12,gcov13,gcov22,gcov23,gcov33,bcon0,bcon1,bcon2,bcon3)
        print("bsq calc")
        bsq = bcov0*bcon0 + bcov1*bcon1 + bcov2*bcon2 + bcov3*bcon3
        #avoid bsq == 0
        #print(bsq)
        bsq[bsq <= 1.e-10] = divb[bsq <= 1.e-10]
        #print(bsq)
        print("return func")
        #Use cleaning target
        func = divb * dxmin / np.sqrt(bsq)
        print("Cleaned cells ", np.shape(func[func <= 1.e-3]), "Total Cells ",np.shape(func))
        #rewrite when divb is just tiny because no Bfield
        #func[divb <= 1.e-10] = 1.e-40
        #func[bsq <= 1.e-10]  = 1.e-40
        #print(func)

    elif funcname is 'rho_gamma': 
        rho   = read_3d_hdf5_slice(  'rho',slice_obj,h5file)
        gamma = read_3d_hdf5_slice('gamma',slice_obj,h5file)
        func = rho*gamma

    elif funcname is 'bsq_o_rho': 
        rho   = read_3d_hdf5_slice(  'rho',slice_obj,h5file)
        bsq   = read_3d_hdf5_slice(  'bsq',slice_obj,h5file)
        func = bsq/rho

    elif funcname is 'total_flux_r': 
        v1     = read_3d_hdf5_slice('v1',slice_obj,h5file)
        v2     = read_3d_hdf5_slice('v2',slice_obj,h5file)
        v3     = read_3d_hdf5_slice('v3',slice_obj,h5file)
        rho    = read_3d_hdf5_slice('rho',slice_obj,h5file)
        uu     = read_3d_hdf5_slice( 'uu',slice_obj,h5file)
        B1     = read_3d_hdf5_slice( 'B1',slice_obj,h5file)
        B2     = read_3d_hdf5_slice( 'B2',slice_obj,h5file)
        B3     = read_3d_hdf5_slice( 'B3',slice_obj,h5file)
        bsq    = read_3d_hdf5_slice('bsq',slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
        bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)

        gam = h5file['Header/Grid/gam'].value
        energy = -(rho + gam*uu)*ucov0
        
        bsq = bsq * ucov0 

        #  EM flux =  - T^r_t
        em_flux_1t  = - ucon1 * bsq  + bcon1 * bcov0
        em_flux_2t  = - ucon2 * bsq  + bcon2 * bcov0
        em_flux_3t  = - ucon3 * bsq  + bcon3 * bcov0
        hyd_flux_1t = energy * ucon1 
        hyd_flux_2t = energy * ucon2 
        hyd_flux_3t = energy * ucon3 
        mass_flux_1 =  rho * ucon1
        mass_flux_2 =  rho * ucon2
        mass_flux_3 =  rho * ucon3
        total_flux_1t  = hyd_flux_1t + em_flux_1t - mass_flux_1
        total_flux_2t  = hyd_flux_2t + em_flux_2t - mass_flux_2
        total_flux_3t  = hyd_flux_3t + em_flux_3t - mass_flux_3

        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)

        func = total_flux_1t * dx_dxp11  + total_flux_2t * dx_dxp12  + total_flux_3t * dx_dxp13
        if with_gdet : 
            func *= metric['gdet']

    elif funcname is 'poynting_flux_r': 
        v1     = read_3d_hdf5_slice('v1',slice_obj,h5file)
        v2     = read_3d_hdf5_slice('v2',slice_obj,h5file)
        v3     = read_3d_hdf5_slice('v3',slice_obj,h5file)
        rho    = read_3d_hdf5_slice('rho',slice_obj,h5file)
        B1     = read_3d_hdf5_slice( 'B1',slice_obj,h5file)
        B2     = read_3d_hdf5_slice( 'B2',slice_obj,h5file)
        B3     = read_3d_hdf5_slice( 'B3',slice_obj,h5file)
        bsq    = read_3d_hdf5_slice('bsq',slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
        bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)
        
        bsq = bsq * ucov0 

        #  EM flux =  - T^r_t
        em_flux_1t = - ucon1 * bsq  + bcon1 * bcov0
        em_flux_2t = - ucon2 * bsq  + bcon2 * bcov0
        em_flux_3t = - ucon3 * bsq  + bcon3 * bcov0

        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)

        func = em_flux_1t * dx_dxp11  + em_flux_2t * dx_dxp12  + em_flux_3t * dx_dxp13
        if with_gdet : 
            func *= metric['gdet']

    elif funcname is 'hydro_flux_r': 
        v1     = read_3d_hdf5_slice('v1',slice_obj,h5file)
        v2     = read_3d_hdf5_slice('v2',slice_obj,h5file)
        v3     = read_3d_hdf5_slice('v3',slice_obj,h5file)
        rho    = read_3d_hdf5_slice('rho',slice_obj,h5file)
        uu     = read_3d_hdf5_slice( 'uu',slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        
        gam = h5file['Header/Grid/gam'].value
        energy = -(rho + gam*uu)*ucov0

        #  flux =  - T^r_t - rho u^r
        hyd_flux_1t = (energy - rho) * ucon1 
        hyd_flux_2t = (energy - rho) * ucon2 
        hyd_flux_3t = (energy - rho) * ucon3 

        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)

        func = hyd_flux_1t * dx_dxp11  + hyd_flux_2t * dx_dxp12  + hyd_flux_3t * dx_dxp13
        if with_gdet : 
            func *= metric['gdet']

    elif funcname is 'mass_flux_r': 
        v1     = read_3d_hdf5_slice('v1',slice_obj,h5file)
        v2     = read_3d_hdf5_slice('v2',slice_obj,h5file)
        v3     = read_3d_hdf5_slice('v3',slice_obj,h5file)
        rho    = read_3d_hdf5_slice('rho',slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        
        #  mass flux =  rho u^r
        mass_flux_1 =  rho * ucon1
        mass_flux_2 =  rho * ucon2
        mass_flux_3 =  rho * ucon3

        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)

        func = mass_flux_1 * dx_dxp11  + mass_flux_2 * dx_dxp12  + mass_flux_3 * dx_dxp13
        if with_gdet : 
            func *= metric['gdet']

    elif funcname is 'total_flux_z': 
        v1     = read_3d_hdf5_slice('v1',slice_obj,h5file)
        v2     = read_3d_hdf5_slice('v2',slice_obj,h5file)
        v3     = read_3d_hdf5_slice('v3',slice_obj,h5file)
        rho    = read_3d_hdf5_slice('rho',slice_obj,h5file)
        uu     = read_3d_hdf5_slice( 'uu',slice_obj,h5file)
        B1     = read_3d_hdf5_slice( 'B1',slice_obj,h5file)
        B2     = read_3d_hdf5_slice( 'B2',slice_obj,h5file)
        B3     = read_3d_hdf5_slice( 'B3',slice_obj,h5file)
        bsq    = read_3d_hdf5_slice('bsq',slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
        bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)

        gam = h5file['Header/Grid/gam'].value
        energy = -(rho + gam*uu)*ucov0
        
        bsq = bsq * ucov0 

        #  EM flux =  - T^z_t
        em_flux_1t  = - ucon1 * bsq  + bcon1 * bcov0
        em_flux_2t  = - ucon2 * bsq  + bcon2 * bcov0
        em_flux_3t  = - ucon3 * bsq  + bcon3 * bcov0
        hyd_flux_1t = energy * ucon1 
        hyd_flux_2t = energy * ucon2 
        hyd_flux_3t = energy * ucon3 
        mass_flux_1 =  rho * ucon1
        mass_flux_2 =  rho * ucon2
        mass_flux_3 =  rho * ucon3
        total_flux_1t  = em_flux_1t - mass_flux_1
        total_flux_2t  = em_flux_2t - mass_flux_2
        total_flux_3t  = em_flux_3t - mass_flux_3

        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
        dx_dxp21 = read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
        dx_dxp22 = read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
        dx_dxp23 = read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)

        func_rr = total_flux_1t * dx_dxp11  + total_flux_2t * dx_dxp12  + total_flux_3t * dx_dxp13
        func_th = total_flux_1t * dx_dxp21  + total_flux_2t * dx_dxp22  + total_flux_3t * dx_dxp23
        # x^z =  x^r dz/dr  + x^th dz/dth =  x^r cos(th)  - x^th * r * sin(th)
        func = func_rr * np.cos(th_slice) - func_th * np.sin(th_slice) * rr_slice 
        if with_gdet : 
            func *= metric['gdet']

    elif funcname is 'poynting_flux_z': 
        v1     = read_3d_hdf5_slice('v1',slice_obj,h5file)
        v2     = read_3d_hdf5_slice('v2',slice_obj,h5file)
        v3     = read_3d_hdf5_slice('v3',slice_obj,h5file)
        rho    = read_3d_hdf5_slice('rho',slice_obj,h5file)
        B1     = read_3d_hdf5_slice( 'B1',slice_obj,h5file)
        B2     = read_3d_hdf5_slice( 'B2',slice_obj,h5file)
        B3     = read_3d_hdf5_slice( 'B3',slice_obj,h5file)
        bsq    = read_3d_hdf5_slice('bsq',slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
        bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)
        
        bsq = bsq * ucov0 

        #  EM flux =  - T^r_t
        em_flux_1t = - ucon1 * bsq  + bcon1 * bcov0
        em_flux_2t = - ucon2 * bsq  + bcon2 * bcov0
        em_flux_3t = - ucon3 * bsq  + bcon3 * bcov0

        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
        dx_dxp21 = read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
        dx_dxp22 = read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
        dx_dxp23 = read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)

        func_rr = em_flux_1t * dx_dxp11  + em_flux_2t * dx_dxp12  + em_flux_3t * dx_dxp13
        func_th = em_flux_1t * dx_dxp21  + em_flux_2t * dx_dxp22  + em_flux_3t * dx_dxp23
        # x^z =  x^r dz/dr  + x^th dz/dth =  x^r cos(th)  - x^th * r * sin(th)
        func = func_rr * np.cos(th_slice) - func_th * np.sin(th_slice) * rr_slice 
        if with_gdet : 
            func *= metric['gdet']

    elif funcname is 'hydro_flux_z': 
        v1     = read_3d_hdf5_slice('v1',slice_obj,h5file)
        v2     = read_3d_hdf5_slice('v2',slice_obj,h5file)
        v3     = read_3d_hdf5_slice('v3',slice_obj,h5file)
        rho    = read_3d_hdf5_slice('rho',slice_obj,h5file)
        uu     = read_3d_hdf5_slice( 'uu',slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        
        gam = h5file['Header/Grid/gam'].value
        energy = -(rho + gam*uu)*ucov0

        #  flux =  - T^r_t - rho u^r
        hyd_flux_1t = (energy - rho) * ucon1 
        hyd_flux_2t = (energy - rho) * ucon2 
        hyd_flux_3t = (energy - rho) * ucon3 

        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
        dx_dxp21 = read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
        dx_dxp22 = read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
        dx_dxp23 = read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)

        func_rr = hyd_flux_1t * dx_dxp11  + hyd_flux_2t * dx_dxp12  + hyd_flux_3t * dx_dxp13
        func_th = hyd_flux_1t * dx_dxp21  + hyd_flux_2t * dx_dxp22  + hyd_flux_3t * dx_dxp23
        # x^z =  x^r dz/dr  + x^th dz/dth =  x^r cos(th)  - x^th * r * sin(th)
        func = func_rr * np.cos(th_slice) - func_th * np.sin(th_slice) * rr_slice 
        if with_gdet : 
            func *= metric['gdet']

    elif funcname is 'mass_flux_z': 
        v1     = read_3d_hdf5_slice('v1',slice_obj,h5file)
        v2     = read_3d_hdf5_slice('v2',slice_obj,h5file)
        v3     = read_3d_hdf5_slice('v3',slice_obj,h5file)
        rho    = read_3d_hdf5_slice('rho',slice_obj,h5file)
        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        
        #  mass flux =  rho u^r
        mass_flux_1 =  rho * ucon1
        mass_flux_2 =  rho * ucon2
        mass_flux_3 =  rho * ucon3

        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
        dx_dxp21 = read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
        dx_dxp22 = read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
        dx_dxp23 = read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)

        func_rr = mass_flux_1 * dx_dxp11  + mass_flux_2 * dx_dxp12  + mass_flux_3 * dx_dxp13
        func_th = mass_flux_1 * dx_dxp21  + mass_flux_2 * dx_dxp22  + mass_flux_3 * dx_dxp23
        # x^z =  x^r dz/dr  + x^th dz/dth =  x^r cos(th)  - x^th * r * sin(th)
        func = func_rr * np.cos(th_slice) - func_th * np.sin(th_slice) * rr_slice 
        if with_gdet : 
            func *= metric['gdet']

    elif funcname is 'coolfunc_rho_mask': 
        rho      = read_3d_hdf5_slice('rho',slice_obj,h5file2)
        coolfunc = read_3d_hdf5_slice('coolfunc',slice_obj,h5file2)

        if( rho_cutout_value is None ): 
            rho_cutout_value = 1e-4 
            
        func = coolfunc
        func[ rho < rho_cutout_value ] = 1e-30

    else:
        if use_stat2:
            func = h5stat2[funcname]
        else:
            if( take_ratio ):
                func1 = read_3d_hdf5_slice(funcname[0],slice_obj,h5file)
                func2 = read_3d_hdf5_slice(funcname[1],slice_obj,h5file)
                func2[func2 == 0] = 1.e-40
                func = func1 / func2
                funcname = funcname[0] + '/' + funcname[1]
            elif( relerror ):
                func1 = read_3d_hdf5_slice(funcname[0],slice_obj,h5file)
                func2 = read_3d_hdf5_slice(funcname[1],slice_obj,h5file)
                func = np.abs(func1 - func2) / func2
                func[func1 == 0] = 1.e-40
                funcname = 'relative error (' + funcname[0] + ')'
            elif( compare_runs ):
                if( compare_dir is None ):
                    h5file2n = '/home/dennis/runs/mass_sharing/a100_hydrostatic_eq_data/conn-dumps/dumps/KDHARM0.' + timeid + '.h5'
                else:
                    h5file2n = compare_dir + '/dumps/KDHARM0.' + timeid + '.h5'
                h5file2.close()
                h5file2 = h5py.File(h5file2n,'r')
                func1 = read_3d_hdf5_slice(funcname,slice_obj,h5file)
                func2 = read_3d_hdf5_slice(funcname,slice_obj,h5file2)
                #mask1 = read_3d_hdf5_slice('evmask',slice_obj,h5file)
                #mask2 = read_3d_hdf5_slice('evmask',slice_obj,h5file2)
                #func1[mask1 != 2] = 1.e-40
                #func2[mask2 != 2] = 1.e-40
                #func = np.abs( func1 - func2) / np.abs(func1)
                func1[func1 == 0] += 1.e-40
                func2[func2 == 0] += 1.e-40
                func = np.abs( 2. * ( func1 - func2 ) / ( func1 + func2 ) )
                #func[mask1 != 2] = 0
                #func[func1 == 0] = 1.e-40
                funcname = 'relative-error-' + funcname
                h5file2.close()
            else:
                func = read_3d_hdf5_slice(funcname,slice_obj,h5file2)
                #mask = read_3d_hdf5_slice('evmask',slice_obj,h5file)
                #func[mask != 2] = 1.e20

    #EXTEND_CENTRAL_CUTOUT
    if(EXTEND_CENTRAL_CUTOUT):
        mask = read_3d_hdf5_slice('evmask',slice_obj,h5file)
        func[mask == 3] = 0. #set the values to 0 in the mask

    #potential overplot
    if( plot_potential ):
        metric = get_metric(slice_obj,h5file)
        gcon = { '00':metric['gcon00'], '01':metric['gcon01'], '02':metric['gcon02'], '03':metric['gcon03'],
                 '11':metric['gcon11'], '12':metric['gcon12'], '13':metric['gcon13'],
                 '22':metric['gcon22'], '23':metric['gcon23'],
                 '33':metric['gcon33'] }
        dx_dxp  = get_dx_dxp(slice_obj,h5file)
        dxc_dxs = get_dxc_dxs( rr, th, ph )
        gcon    = transform_gcon( dx_dxp, gcon ) #Spherical PN Coords (Physical)
        if( corotate ):
            '''
            rewrite dx_dxp to be jacobian to corotating frame 
            dx_dxp = |   1    0 0 0 |
            |   0    1 0 0 |
            |   0    0 1 0 |
            | -omega 0 0 1 |
            '''
            if( spinning ):
                tbh_traj, omega_bin_traj = np.loadtxt(trajfile,usecols=(0,16),unpack=True) #spinning
            else:
                tbh_traj, omega_bin_traj = np.loadtxt(trajfile,usecols=(0,24),unpack=True) #old metrics

            fomega_bin = interp1d(tbh_traj, omega_bin_traj, kind='linear')            
            omega_bin = fomega_bin(time)
            dx_dxp['00'].fill(1.);         dx_dxp['01'].fill(0.); dx_dxp['02'].fill(0.); dx_dxp['03'].fill(0.)
            dx_dxp['10'].fill(0.);         dx_dxp['11'].fill(1.); dx_dxp['12'].fill(0.); dx_dxp['13'].fill(0.)
            dx_dxp['20'].fill(0.);         dx_dxp['21'].fill(0.); dx_dxp['22'].fill(1.); dx_dxp['23'].fill(0.)
            dx_dxp['30'].fill(-omega_bin); dx_dxp['31'].fill(0.); dx_dxp['32'].fill(0.); dx_dxp['33'].fill(1.)
            gcon    = transform_gcon( dx_dxp,  gcon) # Corotating Frame ( spherical coords )
        gcon    = transform_gcon( dxc_dxs, gcon) #Cartesian PN Coords (Physical)
        #Get gcov from gcon
        gcov    = get_ginverse(gcon)
        pot_func    = -0.5 * ( gcov['00'] + 1. )
        # function to plot
        pot_slice = pot_func


    #dennis modifying
    #func /= np.max(func[rr < 100.])
            
    # function to plot
    func_slice = func 

    # Get BH masses if plotting trajs before closing file
    if(plot_bhs_traj):
        m_bh1 = h5file['Header/Grid/m_bh1'].value[0]
        m_bh2 = h5file['Header/Grid/m_bh2'].value[0]
        if( m_bh1 != 0.5 ):
            print('Unequal Masses')
            print('m_bh1',m_bh1)
            print('m_bh2',m_bh2)

   
    if(fillwedge and ith is not None and not cart_coords ):
        rr_slice   = np.append(rr_slice,   np.array([rr_slice[:,0]]).T,   axis=1)
        ph_slice   = np.append(ph_slice,   np.array([ph_slice[:,0]]).T,   axis=1)
        th_slice   = np.append(th_slice,   np.array([th_slice[:,0]]).T,   axis=1)
        func_slice = np.append(func_slice, np.array([func_slice[:,0]]).T, axis=1)
        if( plot_potential ):
            pot_slice = np.append(pot_slice, np.array([pot_slice[:,0]]).T, axis=1)


    if(fillcutout and ith is not None and not cart_coords ):
        zerarr = np.zeros((1,rr_slice.shape[1]))
        phval = np.linspace(0,2.*np.pi,rr_slice.shape[1])
        rr_slice = np.append(rr_slice, zerarr, axis=0)
        zerarr[0,:] = phval[:]
        ph_slice = np.append(ph_slice, zerarr, axis=0)
        th_slice = np.append(th_slice, zerarr, axis=0)
        zerarr[0,:] = np.average(func_slice[0,:])
        #print(zerarr)
        func_slice = np.append(func_slice, zerarr,axis=0)
        #roll so index is 0 of func_slice
        rr_slice   = np.roll(rr_slice,1,axis=0)
        ph_slice   = np.roll(ph_slice,1,axis=0)
        th_slice   = np.roll(th_slice,1,axis=0)
        func_slice = np.roll(func_slice,1,axis=0)
        if( plot_potential ):
            zerarr[0,:] = np.average(pot_slice[0,:])
            pot_slice = np.append(pot_slice, zerarr, axis=0)
            pot_slice = np.roll(pot_slice,1,axis=0)
            

    #Test equator first
    print("rr shape = ", np.shape(rr_slice))
    print("th shape = ", np.shape(th_slice))
    print("ph shape = ", np.shape(ph_slice))
    
    if cart_coords : 
        xx_slice = rr_slice
        yy_slice = th_slice

    else : 
        if ith  == int(jdim/2):
            xx_slice = rr_slice*np.cos(ph_slice)   #*np.sin(th_slice)
            yy_slice = rr_slice*np.sin(ph_slice)   #*np.sin(th_slice)    
        elif ith is not None:
            xx_slice = rr_slice*np.cos(ph_slice)   *np.sin(th_slice)
            yy_slice = rr_slice*np.sin(ph_slice)   *np.sin(th_slice)
        else:
            if iph is None: 
                xx_slice = th_slice
                yy_slice = ph_slice
            else :
                xx_slice = rr_slice*np.sin(th_slice)   *np.cos(ph_slice)
                yy_slice = rr_slice*np.sin(th_slice)   *np.sin(ph_slice)
                xx_slice = np.sqrt(xx_slice*xx_slice + yy_slice*yy_slice) #really r     
                yy_slice = rr_slice*np.cos(th_slice) #really z
        #dennis hack here for cartesian coords (long-box testing)
        #xx_slice = rr_slice
        #yy_slice = th_slice

    if plottitle is None:
        plottitle = funcname
        if dolog:
            plottitle = 'log10|' + plottitle + '|'

    ########################## set default parameters ##########################

    if xmax is None:
        if use_num_coord:
            xmax = len(func_slice[:,0])
        else:
            if irad is not None: 
                xmax = np.pi 
            else : 
                xmax = rmax

    if ymax is None:
        if use_num_coord:
            ymax = len(func_slice[0,:])
        else:
            if irad is not None: 
                ymax = 2.*np.pi 
            else : 
                ymax = rmax

    if zmax is None:
        if use_num_coord:
            zmax = len(func_slice[0,:])
        else:
            zmax = 0.5*rmax

    if xmin is None:
        if use_num_coord:
            xmin = 0
        else:
            if ith is not None:
                xmin = -rmax
            else:
                xmin = 0 

    if ymin is None:
        if use_num_coord:
            ymin = 0
        else:
            if irad is not None: 
                ymin = 0.
            else : 
                ymin = -rmax

    if zmin is None:
        if use_num_coord:
            zmin = 0
        else:
            zmin = -0.5*rmax

    if dirout is None:
        dirout = os.getcwd()



    ############################################################################
    
    if(plot_bhs_traj):
        if( spinning ):
            tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,1,2,4,5),unpack=True) #spinning
        else:
            tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,4,5,6,7),unpack=True) #old metrics

        fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
        fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
        fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
        fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

        theta = th_slice[0,0]

        zbh1  = 0.
        zbh2  = 0.

        xbh1  = fxbh1(time)
        ybh1  = fybh1(time)
        xbh2  = fxbh2(time)
        ybh2  = fybh2(time)
        
        if ith  != int(jdim/2) and ith is not None:
            xbh1  /= np.sin(theta)
            ybh1  /= np.sin(theta)
            xbh2  /= np.sin(theta)
            ybh2  /= np.sin(theta)

	#xbh1 = 10.
	#ybh1 = 0.
	#xbh2 = -10.
	#ybh2 = 0.
        #print('corotate ?', corotate)
        if (corotate):
            phase = np.arctan2(ybh1,xbh1)
            #renormalize phase s.t phase \in (0,2pi)
            if phase < 0.:
                phase = 2. * np.pi - np.abs(phase)
            print('adjusting phase by ', phase * 180. / np.pi, ' degrees')
            #we now need to subtract this angle off from all angle arrays
            ph_slice = ph_slice - phase
            print(" phase = ", phase)
            print("shape( rr_slice ) = " , np.shape(rr_slice))
            print("shape( th_slice ) = " , np.shape(th_slice))
            print("shape( ph_slice ) = " , np.shape(ph_slice))
            
            #reset the x,y slices
            if ith  == int(jdim/2):
                xx_slice = rr_slice*np.cos(ph_slice)   #*np.sin(th_slice)
                yy_slice = rr_slice*np.sin(ph_slice)   #*np.sin(th_slice)
            elif ith is not None:
                xx_slice = rr_slice*np.cos(ph_slice)   *np.sin(th_slice)
                yy_slice = rr_slice*np.sin(ph_slice)   *np.sin(th_slice)
            else:
                if iph is None: 
                    xx_slice = th_slice
                    yy_slice = ph_slice
                else :
                    xx_slice = rr_slice*np.sin(th_slice)   *np.cos(ph_slice)
                    xx_slice = np.abs(xx_slice)
                    yy_slice = rr_slice*np.cos(th_slice)
            #move BHs back to x axis according to phase
            xbh1 = np.sqrt( xbh1*xbh1 + ybh1*ybh1 + zbh1*zbh1)
            ybh1 = 0.
            xbh2 = -np.sqrt( xbh2*xbh2 + ybh2*ybh2 + zbh2*zbh2 )
            ybh2 = 0.

        if plotcentre is None:
            pass
        elif plotcentre is 'BH1':
            xmin = xbh1 - rmax
            xmax = xbh1 + rmax
            ymin = ybh1 - rmax
            ymax = ybh1 + rmax
            zmin = zbh1 - rmax
            zmax = zbh1 + rmax
        elif plotcentre is 'BH2':
            xmin = xbh2 - rmax
            xmax = xbh2 + rmax
            ymin = ybh2 - rmax
            ymax = ybh2 + rmax
            zmin = zbh2 - rmax
            zmax = zbh2 + rmax
        else:
            sys.exit('invalid plotcentre option')

    if normalize_func is True:
        func_slice /= np.max(func_slice)


    if dolog:
        func_slice[func_slice == 0] += 1.e-40       # so that there is no log of zero...
        func_slice = np.log10(np.abs(func_slice))

    if move_patch: 
        xx_center = h5file['Header/Grid/centx_cart1'].value[0]
        yy_center = h5file['Header/Grid/centx_cart2'].value[0]
        zz_center = h5file['Header/Grid/centx_cart3'].value[0]
        print("moving patch to new center = ", xx_center, yy_center, zz_center)
        xx_slice += xx_center
        yy_slice += yy_center
    else : 
        xx_center = 0.
        yy_center = 0.
        zz_center = 0.


    h5file.close()

    if make_bbox : 
        if cart_coords : 
            other_bbox = [np.min(xx_slice),np.max(xx_slice),np.min(yy_slice),np.max(yy_slice)]
            out_artists = [matplotlib.patches.Rectangle((other_bbox[0], other_bbox[2]), (other_bbox[1]-other_bbox[0]), (other_bbox[3]-other_bbox[2]),fill=False,color='k',linestyle='dashed')]
        else :
            rmin = np.min(rr_slice)
            rmax = np.max(rr_slice)
            inner_circle = plt.Circle((xx_center,yy_center),rmin,color='black',fill=False,linestyle='dashed')
            outer_circle = plt.Circle((xx_center,yy_center),rmax,color='black',fill=False,linestyle='dashed')
            out_artists = [inner_circle,outer_circle]
        

    if just_get_data : 
        if use_num_coord:
            if make_bbox : 
                return out_artists, func_slice 
            else : 
                return func_slice 
                
        else : 
            if make_bbox : 
                return out_artists, xx_slice, yy_slice, func_slice 
            else : 
                return xx_slice, yy_slice, func_slice 


    if minf is None:
        minf = np.min(func_slice)
    if maxf is None:
        maxf = np.max(func_slice)

   
    if( plot_potential ):
        if pminf is None:
            pminf = np.min(pot_slice)
        if pmaxf is None:
            pmaxf = np.max(pot_slice)


    # we can now do the plotting

    fig1    = plt.figure(nwin)
    fig1.clf()

    if ax is None:
        if freescale :
            ax   = fig1.add_subplot(111, axisbg=background) 
        else :
            ax   = fig1.add_subplot(111, aspect='equal',axisbg=background) 

    if axislabels is True:
        if use_num_coord:
            if ith is not None:
                ax.set_xlabel('x3')
                ax.set_ylabel('x1')
            else:
                if iph is not None:
                    ax.set_xlabel('x2')
                    ax.set_ylabel('x1')
                else :
                    ax.set_xlabel('x2')
                    ax.set_ylabel('x3')
        else:
            ax.set_xlabel('')
            ax.set_ylabel('')

    
    ax.set_autoscalex_on(False)
    ax.set_autoscaley_on(False)

    if ith is not None:
        ax.axis([xmin,xmax,ymin,ymax])
    else:
        if iph is None: 
            ax.axis([xmin,xmax,ymin,ymax])
        else : 
            ax.axis([xmin,xmax,zmin,zmax])
            

    if xticks is not None:
        ax.set_xticks(xticks)
    if yticks is not None:
        ax.set_yticks(yticks)
    if yticksloc is 'right':
        ax.yaxis.tick_right()

    cmap  = plt.cm.Spectral_r
    #cmap = plt.cm.gray
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 

    print(" min max xx_slice = ", np.min(xx_slice),np.max(xx_slice))
    print(" min max yy_slice = ", np.min(yy_slice),np.max(yy_slice))
    print(" ith = ", ith)
    print(" iph = ", iph)

    print("minf maxf = ", minf, maxf)

    levels = np.linspace(minf,maxf,nlev)
    if use_num_coord:
        CS = ax.contourf(func_slice,levels, cmap=cmap, extend='both')
    else:
        CS = ax.contourf(xx_slice,yy_slice,func_slice,levels, cmap=cmap, extend='both')
        
        
    if artists is not None: 
        for artist in artists :
            ax.add_artist(artist)
        
    if other_data is not None: 
        if use_num_coord:
            CS_other = ax.contourf(other_data[0],levels, cmap=cmap, extend='both')
        else:
            CS_other = ax.contourf(other_data[0],other_data[1],other_data[2],levels, cmap=cmap, extend='both')
        

    if( plot_potential ):
        matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
        plevels = np.linspace(pminf,pmaxf,pnlev)
        CS_pot = ax.contour(xx_slice,yy_slice,pot_slice,plevels,colors='k',extend='both')
        if( pot_labels ):
            plt.clabel(CS_pot, fontsize=pfontsize)

    if(plot_grid):
        for i in range(len(xx_slice[0,:]))[::nperplot]:
            ax.plot(xx_slice[:,i],yy_slice[:,i], color=grid_colour)
            
        for i in range(len(xx_slice[:,0]))[::nperplot]:
            ax.plot(xx_slice[i,:],yy_slice[i,:], color=grid_colour)

    if plot_horizon : 
        if(ubh_horizon):
            rhor = 2.
            iscor = 6.
            bh = plt.Circle((0,0),rhor,color='k')
            isco = plt.Circle((0,0),iscor,color='k',linestyle='dashed',fill=False)
            ax.add_artist(bh)
            ax.add_artist(isco)

    if(plot_bhs_traj):
        green_led = '#5DFC0A'
        #rhor = 0.5 #only for equal mass rat
        rhor1 = m_bh1
        rhor2 = m_bh2
        #actually plot the BHS
        ybh1_bak = ybh1
        ybh2_bak = ybh2
        xbh1_bak = xbh1
        xbh2_bak = xbh2

        if ith is None:
            if iph is None: 
                phase1 = np.arctan2(ybh1,xbh1)
                phase2 = np.arctan2(ybh2,xbh2)
        #renormalize phase s.t phase \in (0,2pi)
                if phase1 < 0.:
                    phase1 = 2. * np.pi - np.abs(phase1)
                if phase2 < 0.:
                    phase2 = 2. * np.pi - np.abs(phase2)
                    
                xbh1 = 0.5*np.pi
                xbh2 = 0.5*np.pi
                ybh1 = phase1
                ybh2 = phase2
                rhor1 = 0.025*np.pi
                rhor2 = rhor1
            else :
                ybh1 = zbh1 
                ybh2 = zbh2


        
        bh1 = plt.Circle((xbh1,ybh1), rhor1, color=green_led)
        bh2 = plt.Circle((xbh2,ybh2), rhor2, color=green_led)
        #bh1 = plt.Circle((xbh1,ybh1), rhor1, color='k')
        #bh2 = plt.Circle((xbh2,ybh2), rhor1, color='k')
        #bh1 = plt.Circle((xbh1,ybh1), rhor1, color='k',fill=False)
        #bh2 = plt.Circle((xbh2,ybh2), rhor2, color='k',fill=False)

        ax.add_artist(bh1)
        ax.add_artist(bh2)

        if(visc_circles):
            sep = np.sqrt( (xbh1 - xbh2)*(xbh1 - xbh2) + (ybh1 - ybh2)*(ybh1 - ybh2) )
            print("sep = ",sep)
            #isco1 = plt.Circle((xbh1,ybh1),3.,color=green_led,fill=False)
            #isco2 = plt.Circle((xbh2,ybh2),3.,color=green_led,fill=False)
            #isco1  = plt.Circle((xbh1,ybh1),5.*m_bh1,color=grid_colour,fill=False)
            #isco2  = plt.Circle((xbh2,ybh2),5.*m_bh2,color=grid_colour,fill=False)
            #tidal1 = plt.Circle((xbh1,ybh1),0.3*np.power(m_bh2/m_bh1,-0.3)*sep,color=grid_colour,fill=False,linestyle='dashed')
            #tidal2 = plt.Circle((xbh2,ybh2),0.3*np.power(m_bh2/m_bh1, 0.3)*sep,color=grid_colour,fill=False,linestyle='dashed')

            isco1  = plt.Circle((xbh1,ybh1),5.*m_bh1,color='k',fill=False,linestyle='solid')
            isco2  = plt.Circle((xbh2,ybh2),5.*m_bh2,color='k',fill=False,linestyle='solid')
            #dsig   = plt.Circle((xbh1,ybh1),0.22*sep,color='white',fill=False,linestyle='solid')
            tidal1 = plt.Circle((xbh1,ybh1),0.3*np.power(m_bh2/m_bh1,-0.3)*sep,color='k',fill=0,linestyle='dashed')
            tidal2 = plt.Circle((xbh2,ybh2),0.3*np.power(m_bh2/m_bh1, 0.3)*sep,color='k',fill=0,linestyle='dashed')

            #cool1 = plt.Circle((xbh1,ybh1),0.45*np.power(m_bh2/m_bh1,-0.3)*sep,color='k',fill=False,linestyle='solid')
            #cool2 = plt.Circle((xbh2,ybh2),0.45*np.power(m_bh2/m_bh1, 0.3)*sep,color='k',fill=False,linestyle='solid')

          
            #Keep these
            ax.add_artist(isco1)
            ax.add_artist(isco2)
            ax.add_artist(tidal1)
            ax.add_artist(tidal2)
            #ax.add_artist(dsig)
            #ax.add_artist(cool1)
            #ax.add_artist(cool2)
            
            if R1T0 is not None:
                Transinner1 = plt.Circle((xbh1,ybh1),R1T0,color='black',fill=False,linestyle='dashdot')
                Transinner2 = plt.Circle((xbh2,ybh2),R1T0,color='black',fill=False,linestyle='dashdot')

                ax.add_artist(Transinner1); ax.add_artist(Transinner2)            
            
            #FUDGE1 = plt.Circle((xbh1,ybh1),0.1*m_bh1,color='white',fill=False,linestyle='dashed')
            #FUDGE2 = plt.Circle((xbh2,ybh2),0.1*m_bh2,color='white',fill=False,linestyle='dashed')
            #ax.add_artist(FUDGE1)
            #ax.add_artist(FUDGE2)

        xbh1 = xbh1_bak
        xbh2 = xbh2_bak
        ybh1 = ybh1_bak
        ybh2 = ybh2_bak

            
    if(plot_horizon):
        yellow_neon = '#F3F315'
        r0          = 2.

        if ith is not None:
            phi0   = np.linspace(0, 2*np.pi, 100)
            xh1    = r0 * np.cos(phi0)
            yh1    = r0 * np.sin(phi0)
            ax.plot( xh1, yh1, color=yellow_neon)
        else:
            theta = np.linspace(np.pi*0.5, -np.pi*0.5, 100)
            ax.plot(r0*np.cos(theta), r0*np.sin(theta), color=yellow_neon)

    if(plot_slosh_box):
        if not corotate:
            print("Must Corotate for sloshing patches")
        else:
            sep = np.sqrt( (xbh1 - xbh2)*(xbh1 - xbh2) + (ybh1 - ybh2)*(ybh1 - ybh2) )
            slosh = matplotlib.patches.Rectangle((0-0.2*sep,0-0.3*sep),0.4*sep,0.6*sep,fill=False,color='k',linestyle='dashed')
            ax.add_artist(slosh)

    if plottitle is not 'notitle':        
        title = plottitle + '  t =  ' + str('%0.1f' %time)
        ax.set_title('%s' %title)

    # ticks for the colorbar
    if ticks_range is 'best':
        rticks = MaxNLocator(nticks+1)
    elif ticks_range is 'minmax':
        rticks = np.linspace(minf,maxf,nticks)

    if scale is not 'off':
        fig1.colorbar(CS,ticks=rticks,format='%1.4g') #colorbar removal dennis here
    if axis is 'off':
        ax.set_axis_off()#axis off here dennis


    if ith is not None:
        figname = dirout+'/'+plottitle + namesuffix + '_it='+timeid + '_ith='+ '%04d' %ith
    else:
        if iph is not None:
            figname = dirout+'/'+plottitle + namesuffix + '_it='+timeid + '_iph='+ '%04d' %iph
        else: 
            figname = dirout+'/'+plottitle + namesuffix + '_it='+timeid + '_irad='+ '%04d' %irad

    if(savepng):
        fig1.savefig('%s.png' %figname, dpi=200,bbox_inches='tight')

    if ith is not None : 
        print("th = ", th_slice[0,0])

    if iph is not None : 
        print("ph = ", ph_slice[0,0])

    if irad is not None : 
        print("r  = ", rr_slice[0,0])


    if show_plot:
        plt.show()

    if (get_time):
        if retfunc:
            return time,func_slice
        else:
            return time,CS
    else:
        if retfunc:
            return func_slice
        else:
            return CS


######################################################
######################################################
def runitall(ibeg,iend):

    dirout = './movies'
#    ibeg = 181
#    iend = 182
#iend = 51

    iph = 0

    rhomin=-7
    rhomax=-1.5
    
    uumin = -9
    uumax = -3
    
    bsqmin = -9
    bsqmax = -3
    
    rhogammin = 1.
    rhogammax = 1.
    
    gammamin = 1.
    gammamax = 2.

    bor_min = -4.
    bor_max = 4. 


    for i in np.arange(ibeg,iend) :
        suf='-rmax1'
        rmax = 50.
#        polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=rhomin,maxf=rhomax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=rhomin,maxf=rhomax,dirout=dirout)
 
#        polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=bsqmin,maxf=bsqmax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=bsqmin,maxf=bsqmax,dirout=dirout)
 
#        polp(itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=bor_min,maxf=bor_max,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=bor_min,maxf=bor_max,dirout=dirout)
 
#        polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=uumin,maxf=uumax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=uumin,maxf=uumax,dirout=dirout)
        
#        polp(itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=gammamin,maxf=gammamax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=gammamin,maxf=gammamax,dirout=dirout)
     

        suf='-rmax2'
        rmax=200.

#        polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=rhomin,maxf=rhomax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=rhomin,maxf=rhomax,dirout=dirout)
 
#        polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=bsqmin,maxf=bsqmax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=bsqmin,maxf=bsqmax,dirout=dirout)
 
#        polp(itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=bor_min,maxf=bor_max,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=bor_min,maxf=bor_max,dirout=dirout)
 
#        polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=uumin,maxf=uumax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=uumin,maxf=uumax,dirout=dirout)

#        polp(itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=gammamin,maxf=gammamax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=gammamin,maxf=gammamax,dirout=dirout)
     
    return


    
######################################
def runitall2(ibeg,iend):

    dirout='./r-mov'
    irads = [400,500,590]
    xmin=0.2
    xmax=np.pi-xmin

    for i in np.arange(ibeg,iend) :
        for irad in irads : 
            #    polp(itime=i,dirname="./",show_plot=False,funcname='poynting_flux_z',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf,dirout=dirout,with_gdet=True)
            #    polp(itime=i,dirname="./",show_plot=False,funcname='total_flux_z',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf   ,dirout=dirout,with_gdet=True)
            #    polp(itime=i,dirname="./",show_plot=False,funcname='hydro_flux_z',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf   ,dirout=dirout,with_gdet=True)
            #    polp(itime=i,dirname="./",show_plot=False,funcname='mass_flux_z',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf    ,dirout=dirout,with_gdet=True)
            polp(itime=i,dirname="./",show_plot=False,funcname='poynting_flux_r',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf,dirout=dirout,with_gdet=True,xmin=xmin,xmax=xmax,minf=-4,maxf=-0.1)
            polp(itime=i,dirname="./",show_plot=False,funcname='total_flux_r',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf   ,dirout=dirout,with_gdet=True,xmin=xmin,xmax=xmax,minf=-4,maxf=3)
            polp(itime=i,dirname="./",show_plot=False,funcname='hydro_flux_r',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf   ,dirout=dirout,with_gdet=True,xmin=xmin,xmax=xmax,minf=-4,maxf=-0.1)
            polp(itime=i,dirname="./",show_plot=False,funcname='mass_flux_r',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf    ,dirout=dirout,with_gdet=True,xmin=xmin,xmax=xmax,minf=-4,maxf=3)

    return
    
######################################
def flux_integrate(ibeg,iend):
    
    nt = iend - ibeg 

    dirout='./'
    dumpbase = 'KDHARM0'
    #timeid = '%05d' %itime #monopole cleaner dumps

    irad = 590
    slice_obj=(slice(irad,irad+1,1),slice(2,14,1),slice(None,None,None))

    id = 0
    for itime in np.arange(ibeg,iend) :
        timeid = '%06d' %itime #harm HDF5 dumps
        dumpfile  = dirout + 'dumps/' + 'KDHARM0.' +  timeid + '.h5'
        h5file   = h5py.File(dumpfile, 'r')

        if itime == ibeg : 
            idim, jdim, kdim, xp1, xp2, xp3  = get_grid(h5file)
            total_out = np.zeros((nt,kdim))
            mflux_out = np.zeros((nt,kdim))
            hyd_out   = np.zeros((nt,kdim))
            em_out    = np.zeros((nt,kdim))
            t_out     = np.zeros(nt)
            ph_tmp    = read_3d_hdf5_slice('x3',slice_obj,h5file)
            ph_out    = np.squeeze(ph_tmp[0,:])
            ph_tmp    = 0 

        t_out[id] = h5file['/Header/Grid/t'].value[0]
        total_r, mflux_r, hyd_flux_r, em_flux_r =  calc_fluxes(slice_obj, h5file)
        total_out[id,:] = np.sum(total_r   ,axis=0)
        mflux_out[id,:] = np.sum(mflux_r   ,axis=0)
        hyd_out[  id,:] = np.sum(hyd_flux_r,axis=0)
        em_out[   id,:] = np.sum(em_flux_r ,axis=0)

        id += 1 
        h5file.close()


    outh5 = h5py.File('fluxes.h5','w')
    dset = outh5.create_dataset('total_flux', data=total_out)
    dset = outh5.create_dataset('mflux_flux', data=mflux_out)
    dset = outh5.create_dataset('hydro_flux', data=hyd_out)
    dset = outh5.create_dataset(   'em_flux', data=em_out)
    dset = outh5.create_dataset(   'phi'    , data=ph_out)
    dset = outh5.create_dataset(   'times'  , data=t_out)
    outh5.close()

    return

######################################
def plot_fluxes() : 

    dirout = './plots/'

    h5file = h5py.File('fluxes_590.h5','r')
    xout = h5file['phi'].value
    yout = h5file['times'].value

    total_out = h5file['total_flux'].value
    mflux_out = h5file['mflux_flux'].value
    hyd_out   = h5file['hydro_flux'].value
    em_out    = h5file[   'em_flux'].value

    trajfile = './dumps/my_bbh_trajectory.out'
    tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,4,5,6,7),unpack=True) #old metrics
    phase1 = np.arctan2(ybh1_traj,xbh1_traj)
    phase2 = np.arctan2(ybh2_traj,xbh2_traj)

    loc = phase1 < 0. 
    phase1[loc] = 2.*np.pi - np.abs(phase1[loc])
    loc = phase2 < 0. 
    phase2[loc] = 2.*np.pi - np.abs(phase2[loc])


    [xmin,xmax,ymin,ymax] = [np.min(xout),np.max(xout),np.min(yout),np.max(yout)]
    
    dolog = False

    minf = 0.1
    maxf = 4

    if dolog : 
        minf = np.log10(minf)
        maxf = np.log10(maxf)

    nticks = 10. 
    nlev = 256.
    nwin=0
    savepng = 1
    cmap=plt.cm.Spectral_r

    func = total_out
    if dolog:
        func[func == 0] += 1.e-40       # so that there is no log of zero...
        func = np.log10(np.abs(func))
    funcname = 'total_flux'
    fignames = [dirout + funcname + '-'+'phi-vs-t']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
#    ax.set_title('%s' %title)
    ax.set_xlabel('phi')
    ax.set_ylabel('t [M]')
    levels = np.linspace(minf,maxf,nlev)
    rticks = np.linspace(minf,maxf,nticks)
    ax.axis([xmin,xmax,ymin,ymax])
    ax.plot(phase1,tbh_traj,'w,')
    ax.plot(phase2,tbh_traj,'w,')
    CP = ax.contourf(xout, yout,func,levels,cmap=cmap,extend='both')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    func = hyd_out
    if dolog:
        func[func == 0] += 1.e-40       # so that there is no log of zero...
        func = np.log10(np.abs(func))
    funcname = 'hydro_flux'
    fignames = np.append(fignames,dirout + funcname + '-'+'phi-vs-t')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
#    ax.set_title('%s' %title)
    ax.set_xlabel('phi')
    ax.set_ylabel('t [M]')
    levels = np.linspace(minf,maxf,nlev)
    rticks = np.linspace(minf,maxf,nticks)
    ax.axis([xmin,xmax,ymin,ymax])
    ax.plot(phase1,tbh_traj,'w,')
    ax.plot(phase2,tbh_traj,'w,')
    CP = ax.contourf(xout, yout,func,levels,cmap=cmap,extend='both')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    func = em_out
    if dolog:
        func[func == 0] += 1.e-40       # so that there is no log of zero...
        func = np.log10(np.abs(func))
    funcname = 'em_flux'
    fignames = np.append(fignames,dirout + funcname + '-'+'phi-vs-t')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
#    ax.set_title('%s' %title)
    ax.set_xlabel('phi')
    ax.set_ylabel('t [M]')
    levels = np.linspace(minf,maxf,nlev)
    rticks = np.linspace(minf,maxf,nticks)
    ax.axis([xmin,xmax,ymin,ymax])
    ax.plot(phase1,tbh_traj,'w,')
    ax.plot(phase2,tbh_traj,'w,')
    CP = ax.contourf(xout, yout,func,levels,cmap=cmap,extend='both')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    print( "fignames = ", fignames)

    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)
            
    print("All done ")
    sys.stdout.flush()
        

    return


######################################

def make_plots(itime) : 

    rhomin=0
    rhomax=4.5
    
    uumin = -9
    uumax = -3
    
    bsq_min = -9
    bsq_max = -3
    
    gammamin = 1.
    gammamax = 2.
    
    bor_min = -4.
    bor_max = 4. 
    
    i = 30
    suf='-rmax2'
    dirout='./plots'
    
    if 0 : 
        rmax = 40.
        polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,irad=None,iph=0,ith=None,plot_bhs_traj=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,minf=rhomin,maxf=rhomax)
        polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,irad=None,iph=0,ith=None,plot_bhs_traj=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,minf=uumin,maxf=uumax)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,irad=None,iph=0,ith=None,plot_bhs_traj=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,minf=bsq_min,maxf=bsq_max)
        polp(itime=i,dirname="./",show_plot=False,funcname='coolfunc',userad=True,dolog=True,savepng=True,irad=None,iph=0,ith=None,plot_bhs_traj=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,minf=-12,maxf=-8)
        polp(itime=i,dirname="./",show_plot=False,funcname='coolfunc_rho_mask',userad=True,dolog=True,savepng=True,irad=None,iph=0,ith=None,plot_bhs_traj=True,namesuffix=suf,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,minf=-12,maxf=-8)
    
    rmax = 20.
    dolog=False
    nt = 21
    ibeg = 0
    iend = 62
    istep = 1
    
    if 1 :
        minf=-1e-4
        maxf=3.1e-3
        for i in [itime]  : 
            run_tag = 'SOD.patch_1'
            artists, xx1, yy1, func1 = polp(itime=i,dirname="./",move_patch=True,show_plot=False,funcname='ucon1',irad=None,iph=5,ith=None,run_tag=run_tag,just_get_data=True,cart_coords=True,dolog=dolog,ubh=True,make_bbox=True)
    
            run_tag = 'SOD.patch_0'
            polp(itime=i,dirname="./",artists=artists,dirout=dirout,move_patch=True,show_plot=False,funcname='ucon1',dolog=dolog,savepng=True,irad=None,iph=9,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,other_data=[xx1,yy1,func1],minf=minf,maxf=maxf)
            polp(itime=i,dirname="./",dirout=dirout,move_patch=True,show_plot=False,funcname='ucon1',dolog=dolog,savepng=True,irad=None,iph=9,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,minf=minf,maxf=maxf,namesuffix='-glob-only')
    
    
    if 1 :
        minf=-1e-4
        maxf=3.1e-3
        for i in [itime]  : 
            run_tag = 'SOD.patch_1'
            artists, xx1, yy1, func1 = polp(itime=i,dirname="./",move_patch=True,show_plot=False,funcname='ucon2',irad=None,iph=5,ith=None,run_tag=run_tag,just_get_data=True,cart_coords=True,dolog=dolog,ubh=True,make_bbox=True)
    
            run_tag = 'SOD.patch_0'
            polp(itime=i,dirname="./",artists=artists,dirout=dirout,move_patch=True,show_plot=False,funcname='ucon2',dolog=dolog,savepng=True,irad=None,iph=9,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,other_data=[xx1,yy1,func1],minf=minf,maxf=maxf)
            polp(itime=i,dirname="./",dirout=dirout,move_patch=True,show_plot=False,funcname='ucon2',dolog=dolog,savepng=True,irad=None,iph=9,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,minf=minf,maxf=maxf,namesuffix='-glob-only')
    
    
    if 1 :
        minf=1.24e4*1.01
        maxf=1.1e5
        for i in [itime]  : 
            run_tag = 'SOD.patch_1'
            artists, xx1, yy1, func1 = polp(itime=i,dirname="./",move_patch=True,show_plot=False,funcname='rho',irad=None,iph=5,ith=None,run_tag=run_tag,just_get_data=True,cart_coords=True,dolog=dolog,ubh=True,make_bbox=True)
    
            run_tag = 'SOD.patch_0'
            polp(itime=i,dirname="./",artists=artists,dirout=dirout,move_patch=True,show_plot=False,funcname='rho',dolog=dolog,savepng=True,irad=None,iph=9,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,other_data=[xx1,yy1,func1],minf=minf,maxf=maxf)
            polp(itime=i,dirname="./",dirout=dirout,move_patch=True,show_plot=False,funcname='rho',dolog=dolog,savepng=True,irad=None,iph=9,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,minf=minf,maxf=maxf,namesuffix='-glob-only')
    
    
    if 1 :
        minf=0.24
        maxf=2.6
        for i in [itime]  : 
            run_tag = 'SOD.patch_1'
            artists, xx1, yy1, func1 = polp(itime=i,dirname="./",move_patch=True,show_plot=False,funcname='uu',irad=None,iph=5,ith=None,run_tag=run_tag,just_get_data=True,cart_coords=True,dolog=dolog,ubh=True,make_bbox=True)
            
            run_tag = 'SOD.patch_0'
            polp(itime=i,dirname="./",dirout=dirout,artists=artists,move_patch=True,show_plot=False,funcname='uu',dolog=dolog,savepng=True,irad=None,iph=9,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,other_data=[xx1,yy1,func1],minf=minf,maxf=maxf)
            polp(itime=i,dirname="./",dirout=dirout,move_patch=True,show_plot=False,funcname='uu',dolog=dolog,savepng=True,irad=None,iph=9,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,minf=minf,maxf=maxf,namesuffix='-glob-only')
    
        

    return
        
######################################
######################################
######################################

if __name__ == '__main__':

    # Set the number of dump files to process from the beginning index to the final one, inclusive:
    ibeg = int(sys.argv[1])
    iend = int(sys.argv[2])    
    iend += 1 
    n_dumps = iend - ibeg 

    # Set the number of processes to use:
    n_procs = int(sys.argv[3])

    # Define the dataset
    itimes = np.arange(ibeg,iend)

    # Output the parameters:
    print("n_dumps = ", n_dumps)
    print("n_procs = ", n_procs)
    print("itimes = ", itimes)

    # Run the plot script with the given number of processes 
    chunksize = int( n_dumps / n_procs )

    if( chunksize <= 0 ) :
        chunksize = 1
        
    p = Pool(n_procs)
    p.map(make_plots, itimes, chunksize)


    print("All done!")

