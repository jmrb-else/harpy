"""  


"""

from __future__ import division
from __future__ import print_function

from scipy.fftpack import fft
import scipy.signal as signal 
import matplotlib.pyplot as plt
import numpy as np
import os,sys
import h5py

from get_sim_info import *
from readhist_h5_all import *

from IPython.core.debugger import Tracer

###############################################################################
###############################################################################
###############################################################################

# def lump(filename='KDHARM0.surface.all.h5',savepng=0):
"""  
Primary routine for lump analysis. 
"""


nt=1000
nphi=160

xtmp = np.ones(nt) 
phi = np.outer(xtmp,np.linspace(0.,np.pi,nphi))
print("phi =", phi.shape)

xtmp = np.ones(nphi) 
tout=np.outer(np.linspace(1.,1.e5,nt),xtmp)
print("tout =", tout.shape)

phase = np.pi*0.3
omega = 2.*np.pi/600.
theta= np.multiply(tout,omega)
theta+=phi
theta+=phase
surf= np.cos(theta)

nwin=0
figs = [plt.figure(nwin)]
figs[-1].clf()
ax = figs[-1].add_subplot(111)
ax.set_ylabel(r'$\phi$')
ax.set_xlabel(r't [M]')
plt.pcolormesh(tout,phi,surf,cmap='jet')
plt.colorbar()

#power = np.absolute(np.sum(np.multiply(surf,np.sin(phi)),axis=1))
power = np.sum(np.multiply(surf,np.sin(phi)),axis=1)
dphi = 2./nphi   # divided by pi 
power = np.multiply(power,dphi)


nwin+=1
figs = np.append(figs,plt.figure(nwin))
figs[-1].clf()
ax = figs[-1].add_subplot(111)
ax.set_xlabel(r't [M]')
ax.set_ylabel(r'FPS of Luminosity')
ax.plot(tout[:,0], power, '-', tout[:,0],np.ones(nt)*np.mean(power),'+')

T = (tout[1,0]-tout[0,0])
N = nt
if (N % 2) : 
    N -= 1 
y = power
from scipy.signal import blackmanharris
#w = blackmanharris(N)
#ywf = fft(y*w)
ywf = fft(y)
fps_sq = (np.abs(ywf[0:N/2]) * 2.0/N)**2
fps = np.sqrt(fps_sq)
freq = np.linspace(0.0, 1.0/(2.0*T), N/2)
ftmp = 1./(omega/(2.*np.pi))
freq = np.multiply(freq,ftmp)


nwin+=1
figs = np.append(figs,plt.figure(nwin))
figs[-1].clf()
ax = figs[-1].add_subplot(111)
ax.set_xlabel(r'$\omega$ [$\Omega$]')
ax.set_ylabel(r'FPS of Luminosity')
ax.plot(freq, fps)


plt.show()
