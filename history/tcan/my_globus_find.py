"""

  my_globus_find.py:
  ------------

    -- uses the CLI globus tool "globus" with its "globus ls" feature
       to search for files in a globus endpoint;


"""

from __future__ import division
from __future__ import print_function
import os,sys
import numpy as np
import h5py
import subprocess


###############################################################################
###############################################################################
###############################################################################

def test_subprocess_parsing(argv):
    """  
    """
    ##############################################################################
    # First test the arguments:
    ##############################################################################
    for iarg in np.arange(len(argv)) : 
        print("argv[", iarg, "] = ", argv[iarg])


    ps = subprocess.Popen(argv[1:],stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    b_std_output, b_std_error = ps.communicate(timeout=15)
    if( b_std_output ) : 
        std_output = b_std_output.decode("utf-8",errors="ignore")
        print(" stdout = ", std_output)
    if( b_std_error ) : 
        std_error  =  b_std_error.decode("utf-8",errors="ignore")
        print(" stderr = ", std_error)

    print(" stdout = ", std_output)
    print("##############")
    print(" stderr = ", std_error)

    #print([  f for f in std_output.split('\n') if 'Pictures' in f ])
    match_lines = [  f for f in std_output.split('\n') if f.endswith('/') ]
    for line in match_lines :
        print(line)

    #  globus ls $ep1':~/project.baqi/gk5/snoble/warp-zeus/run008'

    

    return

###############################################################################
###############################################################################
###############################################################################

def ls_glob_path(endpoint, path, verbose=False):
    """  
    """

    ##############################################################################
    # First test the arguments:
    ##############################################################################
    arg_orig = endpoint+':'+path
    cmds = ['globus', 'ls', arg_orig ]
    if( verbose ) :
        print("cmds = ", cmds)

    good_status = True

    ps = subprocess.Popen(cmds,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)

    try:
        b_std_output, b_std_error = ps.communicate(timeout=15)
    except subprocess.TimeoutExpired:
        ps.kill()
        good_status = False

    if( good_status ) : 
        if( b_std_output ) : 
            std_output = b_std_output.decode("utf-8",errors="ignore")
            if( verbose ) : 
                print(" stdout = ", std_output)
        if( b_std_error ) : 
            std_error  =  b_std_error.decode("utf-8",errors="ignore")
            if( verbose) : 
                print(" stderr = ", std_error)

        dir_list  = [  f for f in std_output.split('\n') if f.endswith('/') ]
        if( verbose ) : 
            print("dirlist = ", dir_list)
        file_list = [  f for f in std_output.split('\n') if not f.endswith('/') ]
        if( verbose ) : 
            print("filelist = ", file_list)
    else : 
        file_list = ['']
        dir_list = ['']


    return file_list, dir_list, good_status

###############################################################################
###############################################################################
###############################################################################

def count_files_in_dir(endpoint,path):
    """  
    """

    file_list, dir_list, good_status  = ls_glob_path(endpoint,path)

    print("#########################################")
    if( good_status ) : 
        print("ENDPOINT = ", endpoint)
        print("PATH = ", path)
        print("NUMBER OF FILES = ", len(file_list))
        print("DIRLIST = ", dir_list)
    else : 
        print("BAD READ: ", endpoint, path)

    return

###############################################################################
###############################################################################
###############################################################################

def find_timeout_reads(endpoint,path):
    """  
    """

    file_list, dir_list, good_status  = ls_glob_path(endpoint,path)

    sys.stdout.flush()

    if( good_status ) : 
        print("GOOD READ: ", endpoint, path, len(file_list), len(dir_list))
        for dir in dir_list : 
            newpath = path+dir
            find_timeout_reads(endpoint,newpath)
    else : 
        print("BAD READ: ", endpoint, path)

    sys.stdout.flush()

    return

###############################################################################
###############################################################################
###############################################################################

def my_globus_find(argv):
    """  
    """

    ###########################################
    #  Example:
    # 
    #  >  globus ls $ep1':~/project.baqi/gk5/snoble/warp-zeus/run008'
    #
    #  where $ep1 is an environment variable containing an endpoint identifier like
    #    d599008e-6d04-11e5-ba46-22000b92c6ec
    #
    ###########################################

    
    endpoint = argv[1]
    dir      = argv[2]
    arg_orig = endpoint+':'+dir
    
    cmds = ['globus', 'ls', arg_orig ]
    ##############################################################################
    # First test the arguments:
    ##############################################################################
    print("cmds = ", cmds)


    ps = subprocess.Popen(cmds,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    b_std_output, b_std_error = ps.communicate(timeout=15)
    if( b_std_output ) : 
        std_output = b_std_output.decode("utf-8",errors="ignore")
        print(" stdout = ", std_output)
    if( b_std_error ) : 
        std_error  =  b_std_error.decode("utf-8",errors="ignore")
        print(" stderr = ", std_error)

    #print([  f for f in std_output.split('\n') if 'Pictures' in f ])
    print("str = ", std_output )
    match_lines = [  f for f in std_output.split('\n') if f.endswith('/') ]
    print("match_lines = ", match_lines)
    for line in match_lines :
        print(line)

    

    return

###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python lum
# 
###############################################################################
if __name__ == "__main__":
    import sys

    #test_subprocess_parsing([argv[0],'ls', '-F'])
    
    print(sys.argv)
    if len(sys.argv) > 1 :
        #my_globus_find(sys.argv)
        #count_files_in_dir(sys.argv[1],sys.argv[2])
        find_timeout_reads(sys.argv[1],sys.argv[2])
    else:
        sys.exit("need args")

