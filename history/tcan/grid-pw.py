###############################################################################
###############################################################################
#  exploring grid setups
###############################################################################
###############################################################################

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re 

from IPython.core.debugger import Tracer

###############################################################################
###############################################################################
def setup_grid(N2=160,h_slope=0.13,diag3_exponent=9,hor=0.3,
               r_hor=1.5,cour=0.45,
               t_sim=7.2e4,ncells_per_core=(20.*20.*20.),
               zc_rate=(1.3*1.15*10000.), th_cutout=np.pi/4., coarsen_factor=10.):

    
    #print(" i_hr_lo, i_hr_hi diff = ", ibeg, iend, (iend - ibeg) )
    beta = 100.
    Qphi = 25.
    Nphi = np.int(1000.*(0.1/hor)*np.sqrt(beta/100.)*(Qphi/10.))
    dphi_eq = 2.*np.pi / Nphi
    #dphi    = dphi_eq * np.sin(th_cutout+0.5*dth)

    # at start of sim:
    beta = 1000.
    vasq_o_vazsq=2.
    Qz=20.
    Nz = np.int(16.*np.sqrt(beta/100.)*np.sqrt((vasq_o_vazsq))*(Qz/10.))
    #print("Ncells within hor = ", n_within_hor)
    print("Nz start = ", Nz)
    dth = hor / Nz 
 
    beta = 10.
    vasq_o_vazsq=100.
    Qz=20.
    Nz_2 = np.int(16.*np.sqrt(beta/100.)*np.sqrt((vasq_o_vazsq))*(Qz/10.))
    print("Nz turb = ", Nz_2)
 
    #Nphi = np.int(3.*np.sqrt(0.1)*1000.*(0.1/hor))
    dphi = 2.*np.pi / Nphi
    shape_factor = 4.
    dth_match = dphi / shape_factor

    ##
    dr_o_r = 2.*np.pi / Nphi
    Nr = np.int(np.log(t_sim/r_hor) / dr_o_r)
    print("Nr = ", Nr)

    N1 = 1472
    N3 = 840

    
    #patch1:
    N1_1 = N1
    N3_1 = N3
    dth_1 = dth
    #N2_1 = np.int( (np.pi - 2.*th_cutout)/dth_1 )
    # assume graded grid: 
    N2_1 = 160  
    N_1 = N1_1*N2_1*N3_1
    
    #patch2:
    N1_2 = N1
    dth_2 = coarsen_factor*dth
    N2_2 = np.int( 2.*th_cutout/dth_2 )
    N3_2 = N2_2 
    N_2 = N1_2*N2_2*N3_2
    
    #patch3:
    N1_3 = N1_2
    N2_3 = N2_2
    N3_3 = N3_2
    N_3 = N_2

    dphi = 2.*np.pi / N3_1
    print("dth dphi = ", dth, dphi)

    dt_phi = cour*r_hor*dphi
    dt_th = cour*r_hor*dth
    dt_min = np.min([dt_phi,dt_th])
    dt_min = dt_phi
    t_wall1 = t_sim * ncells_per_core/(dt_min * zc_rate  )  / (3600.)
    print("Nphi = ", Nphi)
    print("dt_phi dt_th  dt_min = ", dt_phi, dt_th, dt_min)
    print("t_wall (hours) = ", t_wall1)
    print("t_wall (days) = ", t_wall1/24.)
    
    Ncells = N_1 + N_2 + N_3 
    n_cores = Ncells / ncells_per_core
    n_cores_per_node = 56.
    n_nodes = n_cores / n_cores_per_node 
    print("N1 N2 N3 1  = ", N1_1,N2_1,N3_1 )
    print("N1 N2 N3 2  = ", N1_2,N2_2,N3_2 )
    print("N1 N2 N3 3  = ", N1_3,N2_3,N3_3 )
    print("Ncells = ", Ncells )
    print("Ncores = ", n_cores)
    print("Nnodes = ", n_nodes)
    print("Frontera SUs = ", t_wall1*n_nodes)

    n_cores_1 = N_1 / ncells_per_core
    n_cores_2 = N_2 / ncells_per_core
    n_cores_3 = N_3 / ncells_per_core

    print("Ncores Patch1 = ", n_cores_1)
    print("Ncores Patch2 = ", n_cores_2)
    print("Ncores Patch3 = ", n_cores_3)
    
    
    # rho uu p vi bi  = 8
    # gamma  bsq  radflux Ye  = 3  
    Nfuncs = 13
    hdf5_cadence = 1.

    # 8 bytes per number:
    Nbytes_per_snapshot = Ncells*Nfuncs*8.
    Nbytes_per_snapshot_TB = Nbytes_per_snapshot/1.e12
    Ndumps = t_sim / hdf5_cadence
    Sim_Footprint_TB = Ndumps * Nbytes_per_snapshot_TB
    print(" Nbytes_per_snapshot_TB = ", Nbytes_per_snapshot_TB)
    print(" Sim_Footprint_TB       = ", Sim_Footprint_TB )
    

    return



###########################

setup_grid(h_slope=0.1,diag3_exponent=21,N2=180,hor=0.3)



