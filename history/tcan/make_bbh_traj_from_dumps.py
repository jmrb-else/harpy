"""  

  make_bbh_traj_from_dumps.py: 
  --------------------------

    -- extract bbh_traj data from individual 3-d dump files; 

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *
from mymath import * 

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################

def make_traj():

    files = sorted(glob.glob("./trajdumps/KDHARM0.0*.h5"))
    nfiles = len(files)
    
    h5out  = h5py.File("bbh_traj.h5",'w-')

    in_prefix = '/Header/Grid/'

    funcs = ['bh1_traj0',
             'bh1_traj1',
             'bh1_traj2',
             'bh1_traj3',
             'bh2_traj0',
             'bh2_traj1',
             'bh2_traj2',
             'bh2_traj3']

    funcs_to_copy = ['Header']

    ################################################################
    #  Assume all the datasets are scalars while we create each one:
    ################################################################
    shape0 = (nfiles,)
    h5in = h5py.File(files[0],'r')

    # copy over static scalars:
    for func in funcs_to_copy :
        h5in.copy(func,h5out)
    
    # create datasets for the time-dependent ones: 
    for func in funcs : 
        dset = h5out.create_dataset(func,shape0,dtype=h5in[in_prefix+func].dtype)
    h5in.close()

    ################################################################
    #  Assume all the datasets are scalars while we create each one:
    ################################################################
    ifile = 0 
    for file in files :
        print("Doing file = ", file) 
        h5in = h5py.File(file,'r')
        for func in funcs : 
            h5out[func][ifile] = h5in[in_prefix+func].value[0]
        h5in.close()
        ifile += 1 

    h5out.close()
    
    return


###############################################################################
###############################################################################

def make_asep():

    h5out  = h5py.File("bbh_traj.h5",'r+')
    func = 'bh1_traj1'
    dset = h5out.create_dataset('asep',h5out[func].shape,dtype=h5out[func].dtype)
    x1 = h5out['bh1_traj1'][:]
    y1 = h5out['bh1_traj2'][:]
    x2 = h5out['bh2_traj1'][:]
    y2 = h5out['bh2_traj2'][:]
    h5out['asep'][:] = np.sqrt( (x2-x1)**2 + (y2-y1)**2 )
    h5out.close()

    return

    
###############################################################################
###############################################################################

def make_omega():

    h5out  = h5py.File("bbh_traj.h5",'r+')
    t1 = h5out['bh1_traj0'][:]
    x1 = h5out['bh1_traj1'][:]
    y1 = h5out['bh1_traj2'][:]
    x2 = h5out['bh2_traj1'][:]
    y2 = h5out['bh2_traj2'][:]

    phi0, phi1, phi2, dphi, dt, omega, Porb, i_orbit = orbital_functions(x1,y1,t1)

    i_orbit += 3 

    func = 'bh1_traj1'
    dset = h5out.create_dataset('omega_bin',data=omega)
    dset = h5out.create_dataset('P_bin',data=Porb)
    dset = h5out.create_dataset('phase1',data=phi2)
    dset = h5out.create_dataset('phi1',data=phi1)
    dset = h5out.create_dataset('i_orbit',data=i_orbit)
    h5out.close()

    return

    
###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python lum
# 
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)

    #make_traj()
    #make_asep()
    make_omega()
    
#     if len(sys.argv) > 1 :
#         make_series(sys.argv[1])
#     else:
#         lum()

    
