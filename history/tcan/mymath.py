###############################################################################
###############################################################################
#  Useful mathematical operations 
###############################################################################
###############################################################################

import os,sys
from astropy.io import ascii
import numpy as np
import string 

  
#################################################################################
#################################################################################
"""

  orbital_functions():
  -------------

   -- given the x,y coordinates of an orbit, calculates the phase
      (phi), angular frequency (omega), and orbital period (Porb)
      over the course of the orbit; 

   -- handles difficulties with taking derivatives of functions 
      that traverse a closed (periodic) range of values. 

"""
def orbital_functions(x,y,times):

    phi0 = np.arctan2(y,x)
    phi1 = np.copy(phi0)
    nphi = len(phi0)
    i_orbit = np.ndarray((nphi,))

    #####################################################
    # I cannot think of a more elegant solution than this: Need to
    # count the orbits as we make sure that the phase is set to be the
    # cumulative angle in the orbit;
    ######################################################
    phi1[ phi1 < 0. ] += 2. * np.pi 
    phi2 = np.copy(phi1)

    n_orbits = 0 
    i_orbit[0] = 0
    for i in  np.arange(nphi-1) : 
      if( phi1[i+1] < phi1[i] ) :  
        n_orbits += 1 
      i_orbit[i+1] = n_orbits
        
    phi2 += 2.*np.pi*i_orbit
    
    dphi = np.gradient(phi2)
    dt = np.gradient(times)
    omega = dphi/dt
    Porb = 2.*np.pi / omega 

    return phi0, phi1, phi2, dphi, dt, omega, Porb, i_orbit



###############################################################################
# Return with the index of a point in an array: 
###############################################################################
def find_array_index(rpt,r_out) : 
    SMALL = 1e-6
    
    nr = len(r_out)
    ipt = np.abs(r_out-rpt).argmin()

    
    dr = np.abs(np.gradient(r_out))
    ma = np.ma.masked_equal(dr, 0.0, copy=False)
    dmin = ma.min()
    
    if( (ipt <= 0) or (ipt >= (nr-1)) ) :
        if( np.abs(r_out[ipt] - rpt) > dmin ) :
            print("ipt rpt rmin, rmax r_nearest = ", ipt, rpt, np.min(r_out), np.max(r_out), r_out[ipt])
            sys.exit("Bad value of ipt = ")

    return ipt


###############################################################################
# Return with the indices of two points in an array: 
###############################################################################
def find_bracket(rbeg,rend,r_out) : 

    nr = len(r_out)

    if( rbeg is None ) :
        ibeg = 0
    else : 
        ibeg = find_array_index(rbeg,r_out)

    if( rend is None ):
        iend = nr
    else :
        iend = find_array_index(rend,r_out) 

    return ibeg, iend 


###############################################################################
# Return with the zero crossings of a function:
#
#  -- always use the lower index to indicate transit, so transit
#     occurs between i and (i+1).
#  -- code copied from StackExchange:
#  https://stackoverflow.com/questions/3843017/efficiently-detect-sign-changes-in-python
# 
###############################################################################
def find_zero_crossings(data) :
    pos = data > 0
    npos = ~pos
    return ((pos[:-1] & npos[1:]) | (npos[:-1] & pos[1:])).nonzero()[0]


###############################################################################
# Return with the indices places where a function crosses a threshold
# value: 
#
#  -- always use the lower index to indicate transit, so transit
#     occurs between i and (i+1).
#
###############################################################################
def find_threshold_transits(func, thresh) : 

    data = func - thresh

    return find_zero_crossings(data)

###############################################################################
# Interpolate in 1-d over non-finite values in an array:
# value: 
#
###############################################################################
def interp_over_bad_vals(data,coordinates) :

    is_good = np.isfinite(data)
    is_bad  = np.logical_not(is_good)
    if( len(is_good) < 2 ) :
        print("interp_over_bad_vals(): too few good points to interpolate:  ", len(is_good))
        
    if( (len(is_bad) > 0) ) :
        good_pts = np.where(is_good)
        bad_pts  = np.where(is_bad)
        xgood = coordinates[is_good]
        data_good = data[is_good]
        xbad  = coordinates[is_bad]
        data_fixed = np.interp(xbad, xgood, data_good)
        data[is_bad] = data_fixed
    
    return data

###############################################################################
# Rotate ph so that the locations of two black holes are always along
# a line at angle phi_0 from the x-axis
###############################################################################
def corotate_phi(ph,bhloc,phi_0=0.):

    xbh1, ybh1, zbh1, xbh2, ybh2, zbh2 = bhloc
    
    phase = np.arctan2(ybh1,xbh1)
    #renormalize phase s.t phase \in (0,2pi)
    if( phase < 0. ):
        phase = 2. * np.pi - np.abs(phase)

    ph_new = ph - phase + phi_0
        
    xbh1_new = np.sqrt( xbh1*xbh1 + ybh1*ybh1 + zbh1*zbh1)
    ybh1_new = 0.
    zbh1_new = zbh1
    xbh2_new = -np.sqrt( xbh2*xbh2 + ybh2*ybh2 + zbh2*zbh2 )
    ybh2_new = 0.
    zbh2_new = zbh2

    bhloc_new = [xbh1_new, ybh1_new, zbh1_new, xbh2_new, ybh2_new, zbh2_new]

    return ph_new, bhloc_new
