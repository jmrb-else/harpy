"""  

  all_lump.py: 
  ------------
   -- various routines for the lump analysis

   -- reads in the merged "*.surface.all.h5" file and analyzes it. 


"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.fftpack import fft
import scipy.signal as signal
from scipy.interpolate import griddata
import numpy as np
from os.path import expanduser
import os,sys
import shutil
import h5py

from get_sim_info import *
from readhist_h5_all import *
from harm import *
from myfft import my_fft,my_psd
from mymath import *
from plotting import * 
from plot_history import * 


from matplotlib.colors import Colormap
from matplotlib.ticker import FormatStrFormatter, NullFormatter, MultipleLocator, MaxNLocator,ScalarFormatter

USE_NEW_VERSION = True

###############################################################################
###############################################################################
###############################################################################

def make_mode_power_dataset(funcname = 'rhoav', 
                            outfilename='mode_analysis.h5',
                            savepng=1,
                            rbeg_lump=2.,
                            rend_lump=4., 
                            dump_coords=False,
                            runname='inject_disk',
                            use_all_matter=False
                            ):
    """  
    make_mode_power_dataset(): 
    ------------
    -- calculates power spectrum of m=1,... modes 
    -- reads in the merged "*.surface.all.h5" file and analyzes it. 
    -- writes the resultant datasets to the run's dump directory in "mode_analysis.h5" unless otherwise specified
    
    """


    plt.ioff()
    #runnames = ['longer2', 'medium_disk', 'bigger_disk', 'inject_disk']

    if( use_all_matter ) : 
        outfilename='mode_analysis-all.h5'
    
    pnames = ['dirname', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit']
    sim_info = get_sim_info(pnames, runname=runname)
    filename=sim_info['surf_name']+'.all.h5'
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    
    #################
    # Read various details about datasets inside example file : 
    #################
    print("Reading in ", filename)
    sys.stdout.flush()

    h5f = h5py.File(filename,'r')
    
    #  open group in root directory 
    
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value

    nt   = len(t_out)
    nr   = len(r_out)

    # find the (inclusive) region of the lump: 
    rbeg = rbeg_lump * asep
    rend = rend_lump * asep

    ibeg, iend = find_bracket(rbeg,rend,r_out)

    # shape should be  (nt, nr, nphi) :
    slall = slice(None,None,None)
    slrad = slice(ibeg,iend+1,None)
    lump_slice = (slall,slrad,slall)

    # Handle certain functions specially (i.e. adding multiple ones together, etc. ): 

    if( funcname == 'Mxz' ) :  # Maxwell Stress: 
        func1 =  read_3d_hdf5_slice("/Bound/S_Txzb",lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Bound/S_Txzc",lump_slice,h5f)
        func1 += tmp
        if( use_all_matter ) : 
            tmp   =  read_3d_hdf5_slice("/Unbound/S_Txzc",lump_slice,h5f)
            func1 += tmp
            tmp   =  read_3d_hdf5_slice("/Unbound/S_Txzb",lump_slice,h5f)
            func1 += tmp
        tmp = 0

    elif( funcname == 'dTdr_m' ) :  # torque density of MHD terms 
        func1 =  read_3d_hdf5_slice("/Bound/S_dTdr_3",lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_5",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_6",lump_slice,h5f)
        func1 += tmp
        if( use_all_matter ) : 
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_3",lump_slice,h5f)
            func1 += tmp
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_5",lump_slice,h5f)
            func1 += tmp
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_6",lump_slice,h5f)
            func1 += tmp
        tmp = 0

    elif( funcname == 'dTdr_h' ) :  # torque density of hydro terms 
        func1 =  read_3d_hdf5_slice("/Bound/S_dTdr_2",lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_4",lump_slice,h5f)
        func1 += tmp
        if( use_all_matter ) : 
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_2",lump_slice,h5f)
            func1 += tmp
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_4",lump_slice,h5f)
            func1 += tmp
        tmp = 0

    elif( funcname == 'dTdr_tot' ) :  # torque density of hydro terms 
        func1 =  read_3d_hdf5_slice("/Bound/S_dTdr_2",lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_3",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_4",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_5",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_6",lump_slice,h5f)
        func1 += tmp
        if( use_all_matter ) : 
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_2",lump_slice,h5f)
            func1 += tmp
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_3",lump_slice,h5f)
            func1 += tmp
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_4",lump_slice,h5f)
            func1 += tmp
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_5",lump_slice,h5f)
            func1 += tmp
            tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_6",lump_slice,h5f)
            func1 += tmp
        tmp = 0

    else : 
        func1 =  read_3d_hdf5_slice("/Bound/S_"+funcname,lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Unbound/S_"+funcname,lump_slice,h5f)
        func1 += tmp
        tmp = 0

    h5f.close()
    
    # should be  nt, (nr-iend+ibeg-1), nphi
    #print("func1 shape", func1.shape)
    sys.stdout.flush()

    func1_t_phi = np.sum(func1,axis=1)

    nphi = func1.shape[-1]
    ph_tmp = np.linspace(0.,1.,nphi)
    #print("before fft")
    freq, power, amp = my_fft(ph_tmp, func1_t_phi, axis=1)
    #print("after fft")

    #n_modes = nphi/2
    n_modes = 10 

    phase0 = np.ndarray((nt,n_modes),dtype=power.dtype)
    phase1 = np.copy(phase0)
    phase2 = np.copy(phase0)
    omega  = np.copy(phase0)
    omega2 = np.copy(phase0)
    Porb   = np.copy(phase0)
    i_orbit= np.copy(phase0)

    #print("shape of phase0 = ", phase0.shape)

    for i_mode in np.arange(n_modes) : 
        phase0tmp, phase1tmp, phase2tmp, dphi, dt, omegatmp, Porbtmp, i_orbittmp = orbital_functions(amp.real[:,i_mode], -amp.imag[:,i_mode], t_out)
        phase0[:,i_mode] = phase0tmp[:] 
        phase1[:,i_mode] = phase1tmp[:] 
        phase2[:,i_mode] = phase2tmp[:] 
        omega[:,i_mode]  = omegatmp[:]  
        Porb[:,i_mode]   = Porbtmp[:]   
        i_orbit[:,i_mode]= i_orbittmp[:]
        omega2tmp = np.gradient(phase1tmp)
        if 0 : 
            if( i_mode == 1 ) : 
                print(" phase1tmp = ", phase1tmp[2000:2100])
                print(" omega2tmp = ", omega2tmp[2000:2100])
        omega2tmp[ omega2tmp < 0. ] += 2.*np.pi/2.   # because it's a 2nd-order symmetric difference..
        omega2[:,i_mode] = omega2tmp[:]/dt[:]
        

    #print("power shape = ", power.shape)
    n_modes_out = np.int(power.shape[1]/2)
    #print("phase2[0] = ", phase2[100:120,1])
    #print("phase1[0] = ", phase1[100:120,1])
    #print("iorbit[] = ", i_orbit[100:120,1])
    #print("t_out  = ",t_out[100:120])

    print("Writing ", full_outfilename, "  ... ") 
    h5out = h5py.File(full_outfilename,'a')
    
    if dump_coords : 
        dset = h5out.create_dataset('rout', data=r_out)
        dset = h5out.create_dataset('tout', data=t_out)

    dset = h5out.create_dataset(funcname+'_total', data=func1_t_phi)
    dset = h5out.create_dataset(funcname+'_power', data=power[:,:n_modes_out])  # FFT of a real function only has N/2 unique fourier modes
    dset = h5out.create_dataset(funcname+'_phase', data=phase1)
    dset = h5out.create_dataset(funcname+'_omega', data=omega2)

    h5out.close()
    print("..... done!")

    return



###############################################################################
###############################################################################
###############################################################################

def make_mode_power_spacetime_dataset(funcname = 'rhoav', 
                                      outfilename='mode_spacetime_analysis.h5',
                                      savepng=1,
                                      rbeg_lump=2.,
                                      rend_lump=4., 
                                      dump_coords=False,
                                      runname='inject_disk',
                                      use_all_matter=False
                                      ):
    """  
    make_mode_power_spacetime_dataset(): 
    ------------
    -- calculates power of m=1-4 modes as a function of r,t
    -- reads in the merged "*.surface.all.h5" file and analyzes it. 
    -- writes the resultant datasets to the run's dump directory in "mode_analysis.h5" unless otherwise specified
    
    """


    plt.ioff()
    #runnames = ['longer2', 'medium_disk', 'bigger_disk', 'inject_disk']

    if( use_all_matter ) : 
        outfilename='mode_spacetime_analysis-all.h5'

    
    pnames = ['dirname', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit','gdump_name']
    sim_info = get_sim_info(pnames, runname=runname)
    filename=sim_info['surf_name']+'.all.h5'
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    full_outfilename = sim_info['dirname']+'modes/'+outfilename
    
    #################
    # Read various details about datasets inside example file : 
    #################
    print("Reading in ", filename)
    sys.stdout.flush()

    h5f = h5py.File(filename,'r')
    
    #  open group in root directory 
    
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value

    nt   = len(t_out)
    nr   = len(r_out)

    # find the (inclusive) region of the lump: 
    rbeg = rbeg_lump * asep
    rend = rend_lump * asep

    ibeg, iend = find_bracket(rbeg,rend,r_out)

    # shape should be  (nt, nr, nphi) :
    slall = slice(None,None,None)
    slrad = slall
    lump_slice = (slall,slrad,slall)

    # Handle certain functions specially (i.e. adding multiple ones together, etc. ): 

    print("now reading in ", funcname)

    if( funcname == 'Mxz' ) :  # Maxwell Stress: 
        func1 =  read_3d_hdf5_slice("/Bound/S_Txzb",lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Bound/S_Txzc",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_Txzc",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_Txzb",lump_slice,h5f)
        func1 += tmp
        tmp = 0
        
    elif( funcname == 'dTdr_m' ) :  # torque density of MHD terms 
        func1 =  read_3d_hdf5_slice("/Bound/S_dTdr_3",lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_5",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_6",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_3",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_5",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_6",lump_slice,h5f)
        func1 += tmp
        tmp = 0

    elif( funcname == 'dTdr_h' ) :  # torque density of hydro terms 
        func1 =  read_3d_hdf5_slice("/Bound/S_dTdr_2",lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_4",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_2",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_4",lump_slice,h5f)
        func1 += tmp
        tmp = 0

    elif( funcname == 'dTdr_tot' ) :  # torque density of hydro terms 
        func1 =  read_3d_hdf5_slice("/Bound/S_dTdr_2",lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_3",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_4",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_5",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Bound/S_dTdr_6",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_2",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_3",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_4",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_5",lump_slice,h5f)
        func1 += tmp
        tmp   =  read_3d_hdf5_slice("/Unbound/S_dTdr_6",lump_slice,h5f)
        func1 += tmp
        tmp = 0

    elif( funcname == 'testdata' ) :  # torque density of hydro terms
        h5g = h5py.File(sim_info['gdump_name'],'r')
        nphi = h5g['/Header/totalsize3'].value[0]
        h5g.close()
        phi0 = 2.*np.pi*np.linspace(0.,1.,nphi)
        phiall = np.outer(omega_bin*t_out,np.ones(len(phi0))) + np.outer(np.ones(len(t_out)),phi0)
        func1 = np.zeros((nt,nr,nphi))
        print("nphi = ", nphi) 
        for i_r in np.arange(nr) :
            func1[:,i_r,:]  =  r_out[i_r] * ( np.cos(phiall) + 1.2 ) 
        
    else : 
        func1 =  read_3d_hdf5_slice("/Bound/S_"+funcname,lump_slice,h5f)
        tmp   =  read_3d_hdf5_slice("/Unbound/S_"+funcname,lump_slice,h5f)
        func1 += tmp
        tmp = 0
        

    # should be  (nr-iend+ibeg-1), nt,  nphi
    #print("func1 shape", func1.shape)
    sys.stdout.flush()

    nphi = func1.shape[-1]
    ph_tmp = np.linspace(0.,1.,nphi)
    #print("before fft")
    freq, power, amp = my_fft(ph_tmp, func1[:], axis=2)
    #print("after fft")
    print("shape of amp = ", np.shape(amp))
    
    #n_modes = nphi/2
    n_modes = 5

    phase1 = np.ndarray((nt,nr,n_modes),dtype=power.dtype)
    omega2 = np.copy(phase1)

    for i_r in np.arange(nr) :
        for i_mode in np.arange(n_modes) : 
            phase0tmp, phase1tmp, phase2tmp, dphi, dt, omegatmp, Porbtmp, i_orbittmp = orbital_functions(amp.real[:,i_r,i_mode], -amp.imag[:,i_r,i_mode], t_out)
            phase1[:,i_r,i_mode] = phase1tmp[:] 
            omega2tmp = np.gradient(phase1tmp)
            omega2tmp[ omega2tmp < 0. ] += 2.*np.pi/2.   # because it's a 2nd-order symmetric difference..
            omega2[:,i_r,i_mode] = omega2tmp[:]/dt[:]
        

    print("Writing ", full_outfilename, "  ... ") 
    h5out = h5py.File(full_outfilename,'a')
    
    if dump_coords : 
        dset = h5out.create_dataset('rout', data=r_out)
        dset = h5out.create_dataset('tout', data=t_out)

    #dset = h5out.create_dataset(funcname+'_spacetime_total', data=func1[:])
    dset = h5out.create_dataset(funcname+'_spacetime_power', data=power[:,:,:n_modes])  # FFT of a real function only has N/2 unique fourier modes
    dset = h5out.create_dataset(funcname+'_spacetime_phase', data=phase1)
    dset = h5out.create_dataset(funcname+'_spacetime_omega', data=omega2)

    h5f.close()
    
    h5out.close()
    print("..... done!")

    return



###############################################################################
###############################################################################
###############################################################################

def plot_mode_power(funcname = 'rhoav', 
                    mode_filename='mode_analysis.h5',
                    savepng=1,
                    runname='inject_disk',
                    ):
    """  
    plot_mode_power_dataset(): 
    ------------
    -- plot functions associated with the mode analysis, whose data is
       stored in "mode_analysis.h5" unless otherwise specified;
    
    """
    plt.ioff()

    max_order = 5

    pnames = ['dirname', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    h5f.close()

    h5mode = h5py.File(full_mode_filename,'r')
    power  =  h5mode[funcname+'_power'].value[:]
    phase1 =  h5mode[funcname+'_phase'].value[:]
    omega2 =  h5mode[funcname+'_omega'].value[:]
    h5mode.close()

    t_out *= 1e-4
    alpha = 0.5

    nwin=0
    fignames = [dirout+'power-'+funcname+'-lin']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1e0)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,power[:,n_order]/power[:,0],label=order_label,alpha=alpha)
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log1'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e1)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,power[:,n_order]/power[:,0],label=order_label,alpha=alpha)
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e4)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,power[:,n_order]/power[:,0],label=order_label,alpha=alpha)
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-longlog2'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e4)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,power[:,n_order]/power[:,0],label=order_label,alpha=alpha)
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-zoom'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,power[:,n_order]/power[:,0],label=order_label,alpha=alpha)
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log-zoom'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    ax.set_yscale('log')
    ax.set_ylim(1e-4,1e0)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,power[:,n_order]/power[:,0],label=order_label,alpha=alpha)
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-phase-'+funcname+'-close'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\phi_m$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,phase1[:,n_order],label=order_label,marker='.',linestyle='None')
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-phase-'+funcname+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\phi_m$')
    ax.set_xlabel(r't [$10^4$ M]')
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,phase1[:,n_order],label=order_label,marker='.',linestyle='None')
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-close'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        if( n_order == 1 ) : 
            ax.plot(t_out,omega2[:,n_order]/omega_bin,label=order_label,alpha=1.)
        else :
            ax.plot(t_out,omega2[:,n_order]/omega_bin,label=order_label,alpha=0.3)
    ax.legend(loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        if( n_order == 1 ) : 
            ax.plot(t_out,omega2[:,n_order]/omega_bin,label=order_label,alpha=1.)
        else :
            ax.plot(t_out,omega2[:,n_order]/omega_bin,label=order_label,alpha=0.3)
    ax.legend(loc=0)


    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-longall'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order) :
        order_label = str('m=%1d' %n_order)
        if( n_order == 1 ) : 
            ax.plot(t_out,omega2[:,n_order]/omega_bin,label=order_label,alpha=1.)
        else :
            ax.plot(t_out,omega2[:,n_order]/omega_bin,label=order_label,alpha=0.3)
    ax.legend(loc=0)


    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_mode_power2(mode_filename='mode_analysis.h5',
                     savepng=1,
                     runname='inject_disk'
                     ):
    """  
    plot_mode_power2():
    ------------
    -- plot all the functions together for m=1,2 modes
    
    """
    plt.ioff()

    max_order = 2

    pnames = ['dirname', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'combo-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    all_funcs = ['rhoav','Mxz','Txza','bsq','pavg','ddot','lrho','Lut','dTdr_tot','dTdr_h','dTdr_m']
    plot_colors = { 'rhoav'   : 'black', 
                    'Mxz'     : 'red',
                    'Txza'    : 'lime',
                    'bsq'     : 'magenta',
                    'pavg'    : 'goldenrod',
                    'ddot'    : 'grey',
                    'lrho'    : 'brown',
                    'Lut'     : 'yellow',
                    'dTdr_tot': 'blue',
                    'dTdr_h'  : 'cyan',
                    'dTdr_m'  : 'purple'}

    linesty = ['.','-',':','--','-.']
    if( len(linesty) < max_order ) : 
        sys.exit("need more linestyles")

    n_funcs = len(all_funcs)

    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    h5f.close()

    power  = []
    phase1 = []
    omega2 = []

    h5mode = h5py.File(full_mode_filename,'r')
    for funcname in all_funcs : 
        power.append( h5mode[funcname+'_power'].value[:])
        phase1.append(h5mode[funcname+'_phase'].value[:])
        omega2.append(h5mode[funcname+'_omega'].value[:])

    h5mode.close()

    t_out *= 1e-4
    alpha = 0.5

    nwin=0
    fignames = [dirout+'power-'+funcname+'-lin']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1e0)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log1'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e1)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e4)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-longlog2'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e4)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'powerratio-'+funcname+'-longlog2'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_2$ / $A_1$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e2)
    n_order = 1
    for ifunc in np.arange(n_funcs) : 
        ax.plot(t_out,power[ifunc][:,1]/power[ifunc][:,2],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'monepower-'+funcname+'-longlog2'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_2$ / $A_1$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e4)
    n_order = 1
    for ifunc in np.arange(n_funcs) : 
        ax.plot(t_out,power[ifunc][:,1]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-zoom'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log-zoom'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    ax.set_yscale('log')
    ax.set_ylim(1e-4,1e0)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-phase-'+funcname+'-close'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\phi_m$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,phase1[ifunc][:,n_order],marker='.',linestyle='None',color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-phase-'+funcname+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\phi_m$')
    ax.set_xlabel(r't [$10^4$ M]')
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,phase1[ifunc][:,n_order],marker='.',linestyle='None',color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-close'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(7.,7.+0.6420)
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            if( n_order == 1 ) : 
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=1.,color=plot_colors[all_funcs[ifunc]])
            else :
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=0.3,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            if( n_order == 1 ) : 
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=1.,color=plot_colors[all_funcs[ifunc]])
            else :
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=0.3,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)


    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-longall'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            if( n_order == 1 ) : 
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=1.,color=plot_colors[all_funcs[ifunc]])
            else :
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=0.3,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)


    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_mode_power3(mode_filename='mode_analysis.h5',
                     savepng=1,
                     runname='inject_disk'
                     ):
    """  
    plot_mode_power3():
    ------------
    -- like plot_mode_power3() but without dTdr;
    
    """
    plt.ioff()

    max_order = 2

    pnames = ['dirname', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'combo2-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    all_funcs = ['rhoav','Mxz','Txza','bsq','pavg','ddot','lrho','Lut']
    plot_colors = { 'rhoav'   : 'black', 
                    'Mxz'     : 'red',
                    'Txza'    : 'lime',
                    'bsq'     : 'magenta',
                    'pavg'    : 'goldenrod',
                    'ddot'    : 'grey',
                    'lrho'    : 'brown',
                    'Lut'     : 'yellow'}

    linesty = ['.','-',':','--','-.']
    if( len(linesty) < max_order ) : 
        sys.exit("need more linestyles")

    n_funcs = len(all_funcs)

    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    h5f.close()

    power  = []
    phase1 = []
    omega2 = []

    h5mode = h5py.File(full_mode_filename,'r')
    for funcname in all_funcs : 
        power.append( h5mode[funcname+'_power'].value[:])
        phase1.append(h5mode[funcname+'_phase'].value[:])
        omega2.append(h5mode[funcname+'_omega'].value[:])

    h5mode.close()

    t_out *= 1e-4
    alpha = 0.5

    nwin=0
    fignames = [dirout+'power-'+funcname+'-lin']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1e0)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log1'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e1)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e4)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-longlog2'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e4)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'powerratio-'+funcname+'-longlog2'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_2$ / $A_1$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e2)
    n_order = 1
    for ifunc in np.arange(n_funcs) : 
        ax.plot(t_out,power[ifunc][:,1]/power[ifunc][:,2],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'monepower-'+funcname+'-longlog2'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_2$ / $A_1$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e4)
    n_order = 1
    for ifunc in np.arange(n_funcs) : 
        ax.plot(t_out,power[ifunc][:,1]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-zoom'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-'+funcname+'-log-zoom'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    ax.set_yscale('log')
    ax.set_ylim(1e-4,1e0)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,power[ifunc][:,n_order]/power[ifunc][:,0],linestyle=linesty[n_order],alpha=alpha,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-phase-'+funcname+'-close'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\phi_m$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(5.,5.+0.6420)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,phase1[ifunc][:,n_order],marker='.',linestyle='None',color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-phase-'+funcname+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\phi_m$')
    ax.set_xlabel(r't [$10^4$ M]')
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            ax.plot(t_out,phase1[ifunc][:,n_order],marker='.',linestyle='None',color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-close'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(7.,7.+0.6420)
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            if( n_order == 1 ) : 
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=1.,color=plot_colors[all_funcs[ifunc]])
            else :
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=0.3,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)

    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            if( n_order == 1 ) : 
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=1.,color=plot_colors[all_funcs[ifunc]])
            else :
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=0.3,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)


    nwin+=1 
    fignames = np.append(fignames,(dirout+'power-omega-'+funcname+'-longall'))
    figs = np.append(figs,plt.figure(nwin,figsize=(24,6)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega_m$ / $\Omega_{bin}$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1.5)
    for n_order in np.arange(1,max_order+1) :
        for ifunc in np.arange(n_funcs) : 
            if( n_order == 1 ) : 
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=1.,color=plot_colors[all_funcs[ifunc]])
            else :
                ax.plot(t_out,omega2[ifunc][:,n_order]/omega_bin,linestyle=linesty[n_order],alpha=0.3,color=plot_colors[all_funcs[ifunc]])
    ax.legend(all_funcs,loc=0)


    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
# version that plots FPS of various different quantities together like Bowen++2018 
###############################################################################

def plot_mode_power4(mode_filename='mode_analysis.h5',
                     savepng=1,
                     runname='inject_disk',dpi=300
                     ):
    """  
    plot_mode_power4():
    ------------
    -- like plot_mode_power4() 
    
    """
    plt.ioff()

    max_order = 2

    pnames = ['dirname', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit','t_lump_beg','t_lump_end']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'combo4-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    h5f.close()


    power  = []
    phase1 = []
    omega2 = []

    h5mode = h5py.File(full_mode_filename,'r')

    rho_lump_m1_power_vs_t = h5mode['rhoav_power'].value[:,1]
    Lut_lump_m0_power_vs_t     = h5mode['Lut_power'].value[:,0]
    Lut_lump_m1_power_vs_t     = h5mode['Lut_power'].value[:,1]

    phase_lump_m1 = h5mode['rhoav_phase'].value[:,1]
    phase_bin = omega_bin * t_out
    phase_bin = phase_bin % (2.*np.pi)
    phase_out  = phase_lump_m1 - phase_bin
    phase_out[ phase_out < 0. ] += 2. * np.pi
    phase_out /= 8.*np.pi
    phase_out += 0.5

    # bounds of a small segment of time series:
    
    if( sim_info['t_lump_end'] is None ) :
        t_lump_end = t_out[-1]
    else :
        t_lump_end = sim_info['t_lump_end']

    if( sim_info['t_lump_beg'] is None ) :
        t_lump_beg = t_lump_end - 2.5e4
    else :
        t_lump_beg = sim_info['t_lump_beg']

    omega_lump = 0.25*omega_bin
    period_lump = 2.*np.pi/omega_lump
    tend_plot = t_lump_end 
    tbeg_plot = tend_plot - 1.5*period_lump
    tbeg_plot *= 1e-4
    tend_plot *= 1e-4

    print("phase_out =", phase_out)

    omega_lump_m1 = h5mode['rhoav_omega'].value[:,1]
    
    h5mode.close()

    plot_colors = { 'rhoav'   : 'black', 
                    'Mxz'     : 'red',
                    'Txza'    : 'lime',
                    'bsq'     : 'magenta',
                    'pavg'    : 'goldenrod',
                    'ddot'    : 'grey',
                    'lrho'    : 'brown',
                    'Lut'     : 'yellow'}
    

    f_sin1 = np.sin(3.*omega_bin*t_out)
    funcs_out = [rho_lump_m1_power_vs_t,Lut_lump_m0_power_vs_t,Lut_lump_m1_power_vs_t,phase_out,omega_lump_m1,f_sin1]
    norms_out = [np.max(rho_lump_m1_power_vs_t),np.max(Lut_lump_m0_power_vs_t), np.max(Lut_lump_m1_power_vs_t), 1., omega_bin,1.]
    print("funcs_out shape = ", np.shape(funcs_out))
    print("t_out shape = ", np.shape(t_out))
    print(" max of phase_out", np.max(phase_out))
    nfuncs = len(funcs_out)
    colors_out = [plot_colors['rhoav'],'green','orange','blue','red','cyan']
    linesty = ['solid','solid','solid','solid','solid','solid']
    func_names = [r'$\rho_{m=1}$',r'$\mathcal{L}_{m=0}$',r'$\mathcal{L}_{m=1}$',r'$\left|\phi_{\mathrm{lump}} - \phi_{\mathrm{bin}}\right|$',r'$\omega_\mathrm{lump}$', r'$\sin(3 \Omega_{\mathrm{bin}} t)$']
    t_out *= 1e-4
    alphas = np.linspace(0.3,1.,nfuncs)
    linewidths = np.linspace(3.,0.5,nfuncs)

    nwin=0
    figname = dirout+'power-'+'-lin'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r'$f(t) \ [f_0] $')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1e0)
    for ifunc in np.arange(len(func_names)-1) :
        print(" ifunc = ", ifunc) 
        ax.plot(t_out,funcs_out[ifunc]/norms_out[ifunc],linestyle=linesty[ifunc],alpha=alphas[ifunc],color=colors_out[ifunc],linewidth=linewidths[ifunc])
    ax.legend(func_names,loc=0)
    fig.savefig('%s.png' %figname, dpi=dpi,bbox_inches='tight')

    nwin=0
    figname = dirout+'power-'+'-lin2'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r'$f(t) \ [f_0]$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(0.,1e0)
    ax.set_xlim(tbeg_plot,tend_plot)
    for ifunc in np.arange(len(func_names)) :
        print(" ifunc = ", ifunc) 
        ax.plot(t_out,funcs_out[ifunc]/norms_out[ifunc],linestyle=linesty[ifunc],alpha=alphas[ifunc],color=colors_out[ifunc],linewidth=linewidths[ifunc])
    ax.legend(func_names,loc=0)
    fig.savefig('%s.png' %figname, dpi=dpi,bbox_inches='tight')

    
    # FFT:
    detrend_order = 5
    max_freq = 3.5*omega_bin/(2.*np.pi)
    n_new_freq = 2000
    new_freqs = np.linspace(0.,max_freq,n_new_freq)
    t_out *= 1e4
    t_fft = t_out[(t_out > t_lump_beg)*(t_out < t_lump_end)]
    fourier_factor = np.exp( - 1j*2.*np.pi*np.outer(new_freqs,t_fft))

    fft_out = np.ndarray((len(func_names),int(len(t_fft)/2)))
    fft_out2 = np.ndarray((len(func_names),n_new_freq))
    fmod_out = np.ndarray((len(func_names),len(t_fft)))
    for ifunc in np.arange(len(func_names)) :
        print(" ifunc = ", ifunc)
        ftmp = funcs_out[ifunc][(t_out > t_lump_beg)*(t_out < t_lump_end)]
        print("shape of ftmp = ", np.shape(ftmp))
        print("shape of t-fft = ", np.shape(t_fft))
        f_fit = np.poly1d(np.polyfit(t_fft,ftmp,deg=detrend_order))(t_fft)
        print("shape of f_fit = ", np.shape(f_fit))
        ftmp2  = ftmp - f_fit
        fmod = ftmp2/np.std(ftmp2)
        #freq, power, amp = my_fft(t_fft, fmod)
        #nfreq = int(len(freq)/2)
        #freq_out = freq[0:nfreq]*2.*np.pi/omega_bin
        #fft_out[ifunc,:] = power[0:nfreq]
        freq, power, amp = my_psd(t_fft, fmod, fourier_type='welch', make_uniform=True)
        if( ifunc == 0 ) :
            fft_out = np.ndarray((len(func_names),int(len(power))))
            
        freq_out = freq*2.*np.pi/omega_bin
        fft_out[ifunc,:] = power[:]
        fmod_out[ifunc,:] = fmod[:]


    new_freqs *= 2.*np.pi / omega_bin
    max_freq  *= 2.*np.pi / omega_bin
    
    print("shape of fft_out = ", np.shape(fft_out))
    print("shape of power = ", np.shape(power))
    print("shape of power = ", np.shape([power]))
    print("shape of freq_out = ", np.shape(freq_out))

    nwin=0
    figname = dirout+'power-'+'-fft-lin2'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel('PSD[f]')
    ax.set_xlabel(r'$\omega \ [\Omega_\mathrm{bin}]$')
    #ax.set_ylim(0.,1e-3)
    ax.set_xlim(0.,max_freq)
    for ifunc in np.arange(len(func_names)) :
        print(" ifunc = ", ifunc)
        ax.plot(freq_out,fft_out[ifunc],linestyle=linesty[ifunc],alpha=alphas[ifunc],color=colors_out[ifunc],linewidth=linewidths[ifunc])
    ax.legend(func_names,loc=0)
    fig.savefig('%s.png' %figname, dpi=dpi,bbox_inches='tight')
    
    nwin=0
    figname = dirout+'power-'+'-fft-log'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel('PSD[f]')
    ax.set_xlabel(r'$\omega \ [\Omega_\mathrm{bin}]$')
    ax.set_ylim(-3.5,0.5)
    ax.set_xlim(0.,max_freq)
    for ifunc in np.arange(len(func_names)) :
        print(" ifunc = ", ifunc)
        ax.plot(freq_out,np.log10(np.abs(fft_out[ifunc])),linestyle=linesty[ifunc],alpha=alphas[ifunc],color=colors_out[ifunc],linewidth=linewidths[ifunc])
    ax.legend(func_names,loc=0)
    fig.savefig('%s.png' %figname, dpi=dpi,bbox_inches='tight')
    
    nwin=0
    figname = dirout+'power-'+'-fmod-lin'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r'$f(t)$ Detrended')
    ax.set_xlabel(r't [$10^4$ M]')
    for ifunc in np.arange(len(func_names)) :
        print(" ifunc = ", ifunc) 
        ax.plot(t_fft*1e-4,fmod_out[ifunc],linestyle=linesty[ifunc],alpha=alphas[ifunc],color=colors_out[ifunc],linewidth=linewidths[ifunc])
    ax.legend(func_names,loc=0)
    fig.savefig('%s.png' %figname, dpi=dpi,bbox_inches='tight')
    
    nwin=0
    figname = dirout+'power-'+'-fmod-lin2'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r'$f(t)$ Detrended')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_xlim(tbeg_plot,tend_plot)
    ax.set_ylim(-4.,4.)
    for ifunc in np.arange(len(func_names)) :
        print(" ifunc = ", ifunc) 
        ax.plot(t_fft*1e-4,fmod_out[ifunc],linestyle=linesty[ifunc],alpha=alphas[ifunc],color=colors_out[ifunc],linewidth=linewidths[ifunc])
    ax.legend(func_names,loc=0)
    fig.savefig('%s.png' %figname, dpi=dpi,bbox_inches='tight')
    
    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_stress(mode_filename='mode_analysis.h5',
                     savepng=1,
                     rbeg_lump=2.,
                     rend_lump=4., 
                     runname='inject_disk',
                     funcname='rhoav'
                     ):
    """  
    plot_lump_stress():
    ------------
    -- makes plots regarding the stress per unit mass in the lump region
    
    """
    max_order = 2
    plt.ioff()

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'lump-stress-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    all_funcs = ['rhoav','Mxz','Txza','bsq','pavg','ddot','lrho','Lut']

    linesty = ['.','-',':','--','-.']
    if( len(linesty) < max_order ) : 
        sys.exit("need more linestyles")


    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    power = h5mode[funcname+'_power'].value[:]
    h5mode.close()
    print(" .....done ")


    # History data to get stress per unit mass in the lump: 
    print(" Reading history data ...")
    Txzb_hist_b, t_hist = readhist_h5_all('/Bound/H_Txzb' ,sim_info['hist_name'])
    Txzb_hist_u, t_hist = readhist_h5_all('/Unbound/H_Txzb' ,sim_info['hist_name'])
    Txzb_hist = Txzb_hist_u + Txzb_hist_b
    Txzb_hist_u = Txzb_hist_b = 0 
    Txzc_hist_b, t_hist = readhist_h5_all('/Bound/H_Txzc' ,sim_info['hist_name'])
    Txzc_hist_u, t_hist = readhist_h5_all('/Unbound/H_Txzc' ,sim_info['hist_name'])
    Txzc_hist = Txzc_hist_u + Txzc_hist_b
    Txzc_hist_u = Txzc_hist_b = 0 
    rho_hist_b, t_hist = readhist_h5_all('/Bound/H_rhoav' ,sim_info['hist_name'])
    rho_hist_u, t_hist = readhist_h5_all('/Unbound/H_rhoav' ,sim_info['hist_name'])
    rho_hist = rho_hist_b + rho_hist_u
    rho_hist_b = rho_hist_u = 0 
    mag_stress_hist = Txzb_hist + Txzc_hist
    v_alf_sq = mag_stress_hist / rho_hist 
    print(" .... done ")

    rbeg = rbeg_lump * asep
    rend = rend_lump * asep

    ibeg, iend = find_bracket(rbeg,rend,r_out)
    print("averaging over : ", ibeg, iend, r_out[ibeg],r_out[iend])
    v_alf_sq_lumpavg1 = np.mean(v_alf_sq[:,ibeg:iend+1],axis=1)

    ibeg, iend = find_bracket((3.*asep),(5.*asep),r_out)
    print("averaging over : ", ibeg, iend, r_out[ibeg],r_out[iend])
    v_alf_sq_lumpavg2 = np.mean(v_alf_sq[:,ibeg:iend+1],axis=1)

    ibeg, iend = find_bracket((5.*asep),(7.*asep),r_out)
    print("averaging over : ", ibeg, iend, r_out[ibeg],r_out[iend])
    v_alf_sq_lumpavg3 = np.mean(v_alf_sq[:,ibeg:iend+1],axis=1)

    ibeg, iend = find_bracket((7.*asep),(9.*asep),r_out)
    print("averaging over : ", ibeg, iend, r_out[ibeg],r_out[iend])
    v_alf_sq_lumpavg4 = np.mean(v_alf_sq[:,ibeg:iend+1],axis=1)

    t_out *= 1e-4
    t_hist *= 1e-4
    alpha = 0.5

    print(" Plotting data....")
    nwin=0
    fignames = [dirout+'mag-stress-over-rho-'+funcname+'-log']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$<M_{r \phi}> / <\rho> $')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-4,2e-2)
    ax.plot(t_hist,v_alf_sq_lumpavg1)
    ax.plot(t_hist,v_alf_sq_lumpavg2)
    ax.plot(t_hist,v_alf_sq_lumpavg3)
    ax.plot(t_hist,v_alf_sq_lumpavg4)
    ax.legend(["2a < r < 4a", "3a < r < 5a", "5a < r < 7a", "7a < r < 9a"], loc=0)
    

    nwin+=1 
    fignames = np.append(fignames,(dirout+'mag-stress-over-rho-'+funcname+'-lin'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$<M_{r \phi}> / <\rho> $')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylim(1e-4,1.5e-2)
    ax.plot(t_hist,v_alf_sq_lumpavg1)
    ax.plot(t_hist,v_alf_sq_lumpavg2)
    ax.plot(t_hist,v_alf_sq_lumpavg3)
    ax.plot(t_hist,v_alf_sq_lumpavg4)
    ax.legend(["2a < r < 4a", "3a < r < 5a", "5a < r < 7a", "7a < r < 9a"], loc=0)


    nwin+=1 
    fignames = np.append(fignames,(dirout+'modes-'+funcname+'-log'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$A_m$ / $A_0$')
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_yscale('log')
    ax.set_ylim(1e-3,1e0)
    for n_order in np.arange(1,max_order+1) :
        order_label = str('m=%1d' %n_order)
        ax.plot(t_out,power[:,n_order]/power[:,0],label=order_label,linestyle=linesty[n_order],alpha=alpha,color='k')
    ax.legend(loc=0)


    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_power_spacetime(mode_filename='mode_analysis.h5',
                              mode_spacetime_filename='mode_spacetime_analysis.h5',
                              savepng=1,
                              runname='inject_disk',
                              funcname='rhoav',
                              nlev=256,
                              nticks=10,
                              dolog=True,
                              cmap  = plt.cm.Spectral_r,
                              m_order=1,
                              maxf=None,
                              minf=None,
                              maxf2=None,
                              minf2=None,
                              maxf3=None,
                              minf3=None
                              ):
    """  
    plot_lump_power_spacetime():
    ------------
    -- makes plots regarding the lump's power oer r and t
    
    """
    plt.ioff()

    
    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-power-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    smooth_period = (1./sim_info['freq_bin_orbit']) / 0.28 

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    power = h5mode[funcname+'_spacetime_power'].value[:]
    h5mode.close()
    print(" .....done ")

    t_out *= 1e-4
    r_out /= asep
    
    alpha = 0.5

    fshape = power.shape
    if( m_order > fshape[2] ) :
        sys.exit("m_order  needs to be smaller:  ", m_order, fshape[2])

    rtmp = np.outer(np.ones(len(t_out)), r_out)
    
    func0_2 = power[:,:,m_order] / rtmp

    ftmp = power[:,:,0]
    
    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fconv = signal.convolve2d(ftmp,sm_window,mode='same',boundary='symm')
    #print("fconv = ", fconv)
    #print("sm_window = ", sm_window)
    print("n_smooth = ", n_smooth)
    
    if( m_order == 0 ) :
        func0   = power[:,:,m_order]
        func0_3   = power[:,:,m_order]
    else :
        func0 = power[:,:,m_order] / fconv
        func0_3 = power[:,:,m_order] / power[:,:,0]
            

    func0_3_conv = signal.convolve2d(func0_3,sm_window,mode='same',boundary='symm')
        
    if( dolog ) : 
        func = np.log10(np.abs(func0))
        func2 = np.log10(np.abs(func0_2))
        func3 = np.log10(np.abs(func0_3))
        func4 = np.log10(np.abs(func0_3_conv))
        fconvout = np.log10(np.abs(fconv))
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( maxf2 is None ) : 
            maxf2 = np.nanmax(func2) 
        if( maxf3 is None ) : 
            maxf3 = 1e-10
        if( minf is None ) :
            minf = maxf - 3.
        if( minf2 is None ) :
            minf2 = maxf2 - 3.
        if( minf3 is None ) :
            minf3 = maxf3 - 3.
    else : 
        func = func0
        func2 = func0_2
        func3 = func0_3
        func4 = func0_3_conv
        fconvout = fconv
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( maxf2 is None ) : 
            maxf2 = np.nanmax(func2) 
        if( maxf3 is None ) : 
            maxf3 = 1.
        if( minf is None ) :
            minf = maxf * 1.e-3
        if( minf2 is None ) :
            minf2 = maxf2 * 1.e-3
        if( minf3 is None ) :
            minf3 = maxf3 * 1.e-3


    order_label = str('m=%1d' %m_order)

    print("data shape = ", func.shape)

    if( dolog ) :
        suffix = '-log'
    else :
        suffix = '-lin'

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)
    r_closeup = 5.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1
    
    print(" Plotting data....")
    nwin=0
    fignames = [dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    #ax.set_yscale('log')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf3,maxf3,nticks)
    CP = ax.pcolormesh(r_out, t_out,func3,vmin=minf3,vmax=maxf3,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm2-smooth'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf3,maxf3,nticks)
    CP = ax.pcolormesh(r_out, t_out,func4,vmin=minf3,vmax=maxf3,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    #ax.set_yscale('log')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+'conv-func-'+funcname+suffix))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    #ax.set_yscale('log')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,fconvout,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm2-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf3,maxf3,nticks)
    CP = ax.pcolormesh(r_out, t_out,func3,vmin=minf3,vmax=maxf3,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm2-smooth-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf3,maxf3,nticks)
    CP = ax.pcolormesh(r_out, t_out,func4,vmin=minf3,vmax=maxf3,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm2-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf3,maxf3,nticks)
    CP = ax.pcolormesh(r_out, t_out,func3,vmin=minf3,vmax=maxf3,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm2-smooth-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf3,maxf3,nticks)
    CP = ax.pcolormesh(r_out, t_out,func4,vmin=minf3,vmax=maxf3,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+'conv-func-'+funcname+suffix+'-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    #ax.set_yscale('log')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,fconvout,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min2,t_closeup_max)
    rticks = np.linspace(minf2,maxf2,nticks)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf2,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-norm-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min2,t_closeup_max)
    rticks = np.linspace(minf3,maxf3,nticks)
    CP = ax.pcolormesh(r_out, t_out,func3,vmin=minf3,vmax=maxf3,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    
    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_power_spacetime_special(mode_filename='mode_analysis.h5',
                                      mode_spacetime_filename='mode_spacetime_analysis.h5',
                                      savepng=1,
                                      runname='inject_disk',
                                      funcname='rhoav',
                                      nlev=256,
                                      nticks=10,
                                      dolog=True,
                                      cmap  = "hawley",
                                      m_order=1,
                                      maxf=None,
                                      minf=None,
                                      maxf2=None,
                                      minf2=None,
                                      maxf3=None,
                                      minf3=None,
                                      t_plot_max=None,
                                      tag=None
                                      ):
    """  
    plot_lump_power_spacetime_special():
    ------------
    -- plot m=1 over m=2 power only 
    
    """
    plt.ioff()

    if( cmap == "hawley" ) :
        cmap = get_hawley_cmap()
    
    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    
    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-power-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    power = h5mode[funcname+'_spacetime_power'].value[:]
    h5mode.close()
    print(" .....done ")

    t_out *= 1e-4
    r_out /= asep
    
    alpha = 0.5

    fshape = power.shape
    if( m_order > fshape[2] ) :
        sys.exit("m_order  needs to be smaller:  ", m_order, fshape[2])

    func0 = power[:,:,m_order] / power[:,:,2]
    func0_2 = power[:,:,m_order] / power[:,:,0]

    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    ftmp1 = signal.convolve2d(power[:,:,m_order],sm_window,mode='same',boundary='symm')
    ftmp2 = signal.convolve2d(power[:,:,2],sm_window,mode='same',boundary='symm')
    ftmp0 = signal.convolve2d(power[:,:,0],sm_window,mode='same',boundary='symm')
    fconv =  ftmp1 / ftmp2 
    fconv2 = ftmp1 / ftmp0 
    
    if( dolog ) : 
        func = np.log10(np.abs(func0))
        func_2 = np.log10(np.abs(func0_2))
        func2 = np.log10(np.abs(fconv))
        func3 = np.log10(np.abs(fconv2))
        if( maxf is None ) : 
            maxf = np.log10(10.)
        if( maxf2 is None ) : 
            maxf2 = np.log10(1.)
        if( minf is None ) :
            minf = -2.
    else : 
        func = func0
        func_2 = func0_2
        func2 = fconv
        func3 = fconv2
        if( maxf is None ) : 
            maxf = 10.
        if( maxf2 is None ) : 
            maxf2 = 1.
        if( minf is None ) :
            minf = 1.e-2


    order_label = str('m=%1d' %m_order)

    print("data shape = ", func.shape)

    if( dolog ) :
        suffix = '-log'
    else :
        suffix = '-lin'

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)

    if( t_plot_max is None ):
        t_plot_max = t_max
    
    r_closeup = 5.
    r_fixed   = 10.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1

    namebase = funcname+suffix+'-m1_o_m2'

    if( tag is not None ) :
        namebase += tag
        
    rticks = np.linspace(minf,maxf,nticks)
    print(" Plotting data....")
    nwin=0
    fignames = [dirout+'lump-'+order_label+'-power-'+namebase]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    #ax.set_yscale('log')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_plot_max)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-smooth'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_plot_max)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_plot_max)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-smooth-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_plot_max)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-fixed'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_fixed)
    ax.set_ylim(t_min,t_plot_max)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-smooth-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-smooth-fixed'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_fixed)
    ax.set_ylim(t_min,t_plot_max)
    CP = ax.pcolormesh(r_out, t_out,func2,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    # 
    namebase = funcname+suffix+'-m1_o_m0'
    rticks = np.linspace(minf,maxf2,nticks)
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-fixed'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_fixed)
    ax.set_ylim(t_min,t_plot_max)
    CP = ax.pcolormesh(r_out, t_out,func_2,vmin=minf,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+namebase+'-smooth-fixed'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_fixed)
    ax.set_ylim(t_min,t_plot_max)
    CP = ax.pcolormesh(r_out, t_out,func3,vmin=minf,vmax=maxf2,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    
    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_power_spacetime_lines(mode_spacetime_filename='mode_spacetime_analysis.h5',
                                    savepng=1,
                                    runname='inject_disk',
                                    funcname='rhoav',
                                    nlev=256,
                                    nticks=10,
                                    dolog=True,
                                    cmap="hawley",
                                    m_order=1,
                                    maxf=None,
                                    minf=None,
                                    maxf2=None,
                                    minf2=None,
                                    maxf3=None,
                                    minf3=None
                                    ):
    """  
    plot_lump_power_spacetime_lines():
    ------------
    -- plot m=1 over m=2 power only 
    
    """
    plt.ioff()

    if( cmap == "hawley" ) :
        cmap = get_hawley_cmap()
        
    
    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    
    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-power-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    power = h5mode[funcname+'_spacetime_power'].value[:]
    h5mode.close()
    print(" .....done ")

    t_out *= 1e-4
    r_out /= asep

    rbeg = 2.
    rend = 2.5
    ibeg, iend = find_bracket(rbeg,rend,r_out)
    
    alpha = 0.4

    fshape = power.shape

    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fsmooth_m0 = signal.convolve2d(power[:,:,0],sm_window,mode='same',boundary='symm')
    fsmooth_m1 = signal.convolve2d(power[:,:,1],sm_window,mode='same',boundary='symm')
    fsmooth_m2 = signal.convolve2d(power[:,:,2],sm_window,mode='same',boundary='symm')
    fsmooth_m3 = signal.convolve2d(power[:,:,3],sm_window,mode='same',boundary='symm')

    fsmooth_m1_o_m2 = fsmooth_m1 / fsmooth_m2
    fsmooth_m1_o_m0 = fsmooth_m1 / fsmooth_m0
    
    order_label = 'lines'

    if( dolog ) :
        func_m1_o_m2 = np.log10(fsmooth_m1_o_m2)
        func_m1_o_m0 = np.log10(fsmooth_m1_o_m0)
        suffix = '-log'
        if( maxf is None ) : 
            maxf = np.log10(5.)
        if( maxf2 is None ) : 
            maxf2 = np.log10(1.)
        if( minf is None ) :
            minf = -1.
        if( minf2 is None ) :
            minf2 = -2.
    else : 
        func_m1_o_m2 = fsmooth_m1_o_m2
        func_m1_o_m0 = fsmooth_m1_o_m0
        suffix = '-lin'
        if( maxf is None ) : 
            maxf = 5.
        if( maxf2 is None ) : 
            maxf2 = 1.
        if( minf is None ) :
            minf = maxf * 1.e-3
        if( minf2 is None ) :
            minf2 = maxf2 * 1.e-3
    

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)
    r_closeup = 5.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1
    
    nwin=0
    fignames = []
    figs = []
    
    print(" Plotting data....")
    nwin+=1 
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-m1_o_m2-smooth'))
    figs = np.append(figs,(plt.figure(nwin)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [$10^4$ M] ')
    ax.set_ylim(minf,maxf)
    ax.plot(t_out,func_m1_o_m2[:,ibeg],label='r=2.0a',alpha=1)
    ax.plot(t_out,func_m1_o_m2[:,iend],label='r=2.5a',alpha=1)
    ax.legend(loc=0)
    
    nwin+=1 
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-m1_o_m0-smooth'))
    figs = np.append(figs,(plt.figure(nwin)))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [$10^4$ M] ')
    ax.set_ylim(minf2,maxf2)
    ax.plot(t_out,func_m1_o_m0[:,ibeg],label='r=2.0a',alpha=1)
    ax.plot(t_out,func_m1_o_m0[:,iend],label='r=2.5a',alpha=1)
    ax.legend(loc=0)
    

    if( not dolog ): 
        nwin+=1 
        fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-m0-smooth'))
        figs = np.append(figs,(plt.figure(nwin)))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r't [$10^4$ M] ')
        ax.plot(t_out,fsmooth_m0[:,ibeg],'b' ,label='r=2.0a',alpha=1)
        ax.plot(t_out,fsmooth_m0[:,iend],'k',label='r=2.5a',alpha=1)
        ax.plot(t_out,power[:,ibeg,0],   'b' ,alpha=alpha)
        ax.plot(t_out,power[:,iend,0],   'k',alpha=alpha)
        ax.legend(loc=0)
    
        nwin+=1 
        fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-m1-smooth'))
        figs = np.append(figs,(plt.figure(nwin)))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r't [$10^4$ M] ')
        ax.plot(t_out,fsmooth_m1[:,ibeg],'b' ,label='r=2.0a',alpha=1)
        ax.plot(t_out,fsmooth_m1[:,iend],'k',label='r=2.5a',alpha=1)
        ax.plot(t_out,power[:,ibeg,1],   'b' ,alpha=alpha)
        ax.plot(t_out,power[:,iend,1],   'k',alpha=alpha)
        ax.legend(loc=0)
    
        nwin+=1 
        fignames = np.append(fignames,(dirout+'lump-'+order_label+'-power-'+funcname+suffix+'-m2-smooth'))
        figs = np.append(figs,(plt.figure(nwin)))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r't [$10^4$ M] ')
        ax.plot(t_out,fsmooth_m2[:,ibeg],'b' ,label='r=2.0a',alpha=1)
        ax.plot(t_out,fsmooth_m2[:,iend],'k',label='r=2.5a',alpha=1)
        ax.plot(t_out,power[:,ibeg,2],   'b' ,alpha=alpha)
        ax.plot(t_out,power[:,iend,2],   'k',alpha=alpha)
        ax.legend(loc=0)
    
 
    
    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_criteria(mode_spacetime_filename='mode_spacetime_analysis.h5',
                       savepng=True,
                       savepdf=False,
                       runname='inject_disk',
                       funcname='rhoav',
                       nlev=256,
                       nticks=10,
                       dolog=True,
                       cmap  = plt.cm.Spectral_r,
                       m_order=1,
                       maxf=None,
                       minf=None,
                       maxf2=None,
                       minf2=None,
                       maxf3=None,
                       minf3=None,
                       m1_o_m2_thresh = 2.5,
                       m1_o_m0_thresh = 0.2,
                       dpi=300
                       ):
    """  
    plot_lump_criteria():
    ------------
    -- make plots showing how each run satisfies the t_lump criteria; 
    
    """
    plt.ioff()

    
    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq',  't_lump_beg']
    sim_info = get_sim_info(pnames, runname=runname)

    
    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-power-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    t_lump     = sim_info['t_lump_beg']
    if( t_lump is not None ) : 
        t_lump *= 1e-4

    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    power = h5mode[funcname+'_spacetime_power'].value[:]
    h5mode.close()
    print(" .....done ")

    t_out *= 1e-4
    r_out /= asep

    rbeg = 2.
    rend = 2.5
    ibeg, iend = find_bracket(rbeg,rend,r_out)
    
    alpha = 0.4

    fshape = power.shape

    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fsmooth_m0 = signal.convolve2d(power[:,:,0],sm_window,mode='same',boundary='symm')
    fsmooth_m1 = signal.convolve2d(power[:,:,1],sm_window,mode='same',boundary='symm')
    fsmooth_m2 = signal.convolve2d(power[:,:,2],sm_window,mode='same',boundary='symm')
    fsmooth_m3 = signal.convolve2d(power[:,:,3],sm_window,mode='same',boundary='symm')

    fsmooth_m1_o_m2 = fsmooth_m1 / fsmooth_m2
    fsmooth_m1_o_m0 = fsmooth_m1 / fsmooth_m0
    
    order_label = 'criteria-amp-'

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)
    r_closeup = 5.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1

    x_hor_line = [np.min(t_out),np.max(t_out)]
    y_hor_line1 = [m1_o_m0_thresh,m1_o_m0_thresh]
    y_hor_line2 = [m1_o_m2_thresh,m1_o_m2_thresh]

    if( t_lump is not None ) : 
        x_vert_line = [t_lump,t_lump]
        y_vert_line = [-1e6,1e6]
    
    #ymajorFormatter = FormatStrFormatter('%1.1e')
    ymajorFormatter = FormatStrFormatter('%1.0f')
    xmajorFormatter = FormatStrFormatter('%1.0f')


    nwin=0
    fignames = []
    figs = []
    
    print(" Plotting data....")
    nwin+=1 
    fignames = np.append(fignames,(dirout+'lump-'+order_label+funcname))
    figs = np.append(figs,(plt.figure(nwin)))
    figs[-1].clf()

    alpha1 = 0.5
    alpha2 = 1.0

    loc = 2
    
    linew=2.
    ax = figs[-1].add_subplot(3,1,1)
    ax.set_ylabel(r'$\bar{A_1} / \bar{A_0}$')
    ax.set_ylim(1e-10,1.)
    ax.plot(t_out,fsmooth_m1_o_m0[:,ibeg],'k',label='r=2.0a',alpha=alpha1,linewidth=linew)
    ax.plot(t_out,fsmooth_m1_o_m0[:,iend],'k',label='r=2.5a',alpha=alpha2,linewidth=linew)
    ax.plot(x_hor_line,y_hor_line1 ,'k--')
    if( t_lump is not None ) : 
        ax.plot(x_vert_line,y_vert_line,'k--')
    # ax.yaxis.set_major_formatter(ymajorFormatter)
    # ax.xaxis.set_major_formatter(xmajorFormatter)
    #ax.set_xlabel(r't [$10^4$ M] ')
    ax.xaxis.set_major_formatter(NullFormatter())
    ax.legend(loc=loc)
    
    ax = figs[-1].add_subplot(3,1,2)
    ax.set_ylim(1e-10,5.)
    ax.set_ylabel(r'$\bar{A_1} / \bar{A_2}$')
    ax.plot(t_out,fsmooth_m1_o_m2[:,ibeg],'k',label='r=2.0a',alpha=alpha1,linewidth=linew)
    ax.plot(t_out,fsmooth_m1_o_m2[:,iend],'k',label='r=2.5a',alpha=alpha2,linewidth=linew)
    ax.plot(x_hor_line,y_hor_line2 ,'k--')
    if( t_lump is not None ) : 
        ax.plot(x_vert_line,y_vert_line,'k--')
    ax.xaxis.set_major_formatter(NullFormatter())
    # ax.yaxis.set_major_formatter(ymajorFormatter)
    # ax.xaxis.set_major_formatter(xmajorFormatter)
    #ax.set_xlabel(r't [$10^4$ M] ')
    ax.legend(loc=loc)

    linew=None
    ax = figs[-1].add_subplot(3,1,3)
    funcout = np.log10(np.abs(power))
    ymax = np.max(funcout[:,iend,0]) + 0.5
    ymin = ymax - 3.3
    ax.set_ylim(ymin,ymax)
    ax.set_ylabel(r'$\log_{10} A_m$')
    ax.plot(t_out,funcout[:,iend,0],'k', label='m=0',alpha=alpha2,linewidth=linew)
    ax.plot(t_out,funcout[:,ibeg,0],'k', label=None ,alpha=alpha1,linewidth=linew)
    ax.plot(t_out,funcout[:,iend,1],'b', label='m=1',alpha=alpha2,linewidth=linew)
    ax.plot(t_out,funcout[:,ibeg,1],'b', label=None ,alpha=alpha1,linewidth=linew)
    ax.plot(t_out,funcout[:,iend,2],'r', label='m=2',alpha=alpha2,linewidth=linew)
    ax.plot(t_out,funcout[:,ibeg,2],'r', label=None ,alpha=alpha1,linewidth=linew)
    if( t_lump is not None ) : 
        ax.plot(x_vert_line,y_vert_line,'k--')
    #ax.yaxis.set_major_formatter(ymajorFormatter)
    #ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.set_xlabel(r't [$10^4$ M] ')
    ax.legend(loc=loc)
    figs[-1].subplots_adjust(hspace=0.)

    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=dpi,bbox_inches='tight')

    if(savepdf):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.pdf' %fignames[ifig], dpi=dpi,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_criteria2(mode_spacetime_filename='mode_spacetime_analysis.h5',
                       savepng=True,
                       savepdf=False,
                       runname='inject_disk',
                       funcname='rhoav',
                       nlev=256,
                       nticks=10,
                       dolog=True,
                       cmap  = plt.cm.Spectral_r,
                       m_order=1,
                       maxf=None,
                       minf=None,
                       maxf2=None,
                       minf2=None,
                       maxf3=None,
                       minf3=None,
                       m1_o_m2_thresh = 2.5,
                       m1_o_m0_thresh = 0.2,
                        dpi=300,
                        tmax=None,
                        tag=''
                       ):
    """  
    plot_lump_criteria2():
    ------------
    -- make plots showing how each run satisfies the t_lump criteria and the new candidate single criterion; 
    
    """
    plt.ioff()

    
    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq', 't_lump_beg']
    sim_info = get_sim_info(pnames, runname=runname)

    
    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-power-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    t_lump     = sim_info['t_lump_beg']

    binary_period = 1./sim_info['freq_bin_orbit']
    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    power = h5mode[funcname+'_spacetime_power'].value[:]
    h5mode.close()
    print(" .....done ")

    t_out *= 1e-4
    r_out /= asep

    rbeg = 2.
    rend = 2.5
    ibeg, iend = find_bracket(rbeg,rend,r_out)
    
    alpha = 0.4

    fshape = power.shape

    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fsmooth_m0 = signal.convolve2d(power[:,:,0],sm_window,mode='same',boundary='symm')
    fsmooth_m1 = signal.convolve2d(power[:,:,1],sm_window,mode='same',boundary='symm')
    fsmooth_m2 = signal.convolve2d(power[:,:,2],sm_window,mode='same',boundary='symm')
    fsmooth_m3 = signal.convolve2d(power[:,:,3],sm_window,mode='same',boundary='symm')

    fsmooth_m1_o_m2 = fsmooth_m1 / fsmooth_m2
    fsmooth_m1_o_m0 = fsmooth_m1 / fsmooth_m0


    rbeg2 = 2.
    rend2 = 3.
    ibeg2, iend2 = find_bracket(rbeg2,rend2,r_out)
    fsmooth_lump_m0 = np.sum(fsmooth_m0[:,ibeg2:iend2],axis=1) 
    fsmooth_lump_m1 = np.sum(fsmooth_m1[:,ibeg2:iend2],axis=1) 
    fsmooth_lump_m2 = np.sum(fsmooth_m2[:,ibeg2:iend2],axis=1) 
    fsmooth_lump_m1_o_m2 = fsmooth_lump_m1 / fsmooth_lump_m2
    fsmooth_lump_m1_o_m0 = fsmooth_lump_m1 / fsmooth_lump_m0

    thresh_factor = (2.5/0.2)
    fsmooth_lump_m1_o_m0 *= thresh_factor

    fint_m = []
    fint_m = np.append(fint_m,np.sum(power[:,ibeg2:iend2,0],axis=1))
    fint_m = np.append(fint_m,np.sum(power[:,ibeg2:iend2,1],axis=1))
    fint_m = np.append(fint_m,np.sum(power[:,ibeg2:iend2,2],axis=1))
    fint_m = np.reshape(fint_m,(3,len(t_out)))
    print("fint_m shape = ", np.shape(fint_m))
    
    order_label = 'criteria-amp2-'

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)
    r_closeup = 5.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1

    x_hor_line  = np.array([np.min(t_out),np.max(t_out)])
    y_hor_line1 = np.array([m1_o_m0_thresh,m1_o_m0_thresh])
    y_hor_line2 = np.array([m1_o_m2_thresh,m1_o_m2_thresh])

    if( t_lump is not None ) : 
        x_vert_line = np.array([t_lump,t_lump])
        y_vert_line = np.array([-1e6,1e6])
    
    #ymajorFormatter = FormatStrFormatter('%1.1e')
    ymajorFormatter = FormatStrFormatter('%1.0f')
    xmajorFormatter = FormatStrFormatter('%1.0f')


    nwin=0
    fignames = []
    figs = []
    
    lsize = 15
    alpha1 = 0.5
    alpha2 = 1.0

    loc = 2

    print(" Plotting data....")

    if 0 : 
        nwin+=1 
        fignames = np.append(fignames,(dirout+'lump-'+order_label+funcname))
        figs = np.append(figs,(plt.figure(nwin)))
        figs[-1].clf()
        
        linew=2.
        ax = figs[-1].add_subplot(4,1,1)
        ax.set_ylabel(r'$\bar{A_1} / \bar{A_0}$',size=lsize)
        ax.set_ylim(1e-10,1.)
        ax.plot(t_out,fsmooth_m1_o_m0[:,ibeg],'k',label='r=2.0a',alpha=alpha1,linewidth=linew)
        ax.plot(t_out,fsmooth_m1_o_m0[:,iend],'k',label='r=2.5a',alpha=alpha2,linewidth=linew)
        ax.plot(x_hor_line,y_hor_line1 ,'k--')
        if( t_lump is not None ) : 
            ax.plot(x_vert_line/1e4,y_vert_line,'k--')
            # ax.yaxis.set_major_formatter(ymajorFormatter)
            # ax.xaxis.set_major_formatter(xmajorFormatter)
            #ax.set_xlabel(r't [$10^4$ M] ')
        ax.xaxis.set_major_formatter(NullFormatter())
        ax.legend(loc=loc)
        
        ax = figs[-1].add_subplot(4,1,2)
        ax.set_ylim(1e-10,5.)
        ax.set_ylabel(r'$\bar{A_1} / \bar{A_2}$',size=lsize)
        ax.plot(t_out,fsmooth_m1_o_m2[:,ibeg],'k',label='r=2.0a',alpha=alpha1,linewidth=linew)
        ax.plot(t_out,fsmooth_m1_o_m2[:,iend],'k',label='r=2.5a',alpha=alpha2,linewidth=linew)
        ax.plot(x_hor_line,y_hor_line2 ,'k--')
        if( t_lump is not None ) : 
            ax.plot(x_vert_line/1e4,y_vert_line,'k--')
            ax.xaxis.set_major_formatter(NullFormatter())
            # ax.yaxis.set_major_formatter(ymajorFormatter)
            # ax.xaxis.set_major_formatter(xmajorFormatter)
            #ax.set_xlabel(r't [$10^4$ M] ')
        ax.legend(loc=loc)
        
        ax = figs[-1].add_subplot(4,1,3)
        ax.set_ylim(1e-10,5.)
        ax.set_ylabel(r'$\int_{\Delta r_{gap}}\bar{A_1} dr / \int_{\Delta r_{gap}} \bar{A_i} dr$',size=lsize)
        ax.plot(t_out,fsmooth_lump_m1_o_m2[:],'k',label='i=2',alpha=alpha1,linewidth=linew)
        ax.plot(t_out,fsmooth_lump_m1_o_m0[:],'k',label='i=0 (x12.5)',alpha=alpha2,linewidth=linew)
        ax.plot(x_hor_line,y_hor_line2 ,'k--')
        if( t_lump is not None ) : 
            ax.plot(x_vert_line/1e4,y_vert_line,'k--')
            ax.xaxis.set_major_formatter(NullFormatter())
            # ax.yaxis.set_major_formatter(ymajorFormatter)
            # ax.xaxis.set_major_formatter(xmajorFormatter)
            #ax.set_xlabel(r't [$10^4$ M] ')
        ax.legend(loc=loc)
        
        linew=None
        ax = figs[-1].add_subplot(4,1,4)
        funcout = np.log10(np.abs(power))
        ymax = np.max(funcout[:,iend,0]) + 0.5
        ymin = ymax - 3.3
        ax.set_ylim(ymin,ymax)
        ax.set_ylabel(r'$\log_{10} A_m$',size=lsize)
        ax.plot(t_out,funcout[:,iend,0],'k', label='m=0',alpha=alpha2,linewidth=linew)
        ax.plot(t_out,funcout[:,ibeg,0],'k', label=None ,alpha=alpha1,linewidth=linew)
        ax.plot(t_out,funcout[:,iend,1],'b', label='m=1',alpha=alpha2,linewidth=linew)
        ax.plot(t_out,funcout[:,ibeg,1],'b', label=None ,alpha=alpha1,linewidth=linew)
        ax.plot(t_out,funcout[:,iend,2],'r', label='m=2',alpha=alpha2,linewidth=linew)
        ax.plot(t_out,funcout[:,ibeg,2],'r', label=None ,alpha=alpha1,linewidth=linew)
        if( t_lump is not None ) : 
            ax.plot(x_vert_line/1e4,y_vert_line,'k--')
            #ax.yaxis.set_major_formatter(ymajorFormatter)
            #ax.xaxis.set_major_formatter(xmajorFormatter)
        ax.set_xlabel(r't [$10^4$ M] ',size=lsize)
        ax.legend(loc=loc)
        figs[-1].subplots_adjust(hspace=0.)


    ################################
    if( tmax is None ) :
        tmax = np.max(t_out)*1e4

    t_out2 = t_out*1e4/binary_period
    fsmooth_lump_m1_o_m0 /= thresh_factor

    
    nwin+=1
    order_label = 'criteria-amp3-'+tag
    x_hor_line  = np.array([np.min(t_out),np.max(t_out)])
    x_hor_line2 = np.array([np.min(t_out2),np.max(t_out2)])
    y_hor_line1 = np.array([m1_o_m0_thresh,m1_o_m0_thresh])
    
    fignames = np.append(fignames,(dirout+'lump-'+order_label+funcname))
    figs = np.append(figs,(plt.figure(nwin)))
    figs[-1].clf()
    
    linew=2.
    funcout=fsmooth_lump_m1_o_m0[:]
    ax = figs[-1].add_subplot(2,1,1)
    ax2 = ax.twiny()
    ax.set_ylim(1e-10,0.8)
    ax.set_xlim(1e-10,tmax/binary_period)
    ax.set_ylabel(r'$\int_{2a}^{3a}\bar{A_1} dr / \int_{2a}^{3a} \bar{A_0} dr$',size=lsize)
    ax.plot(t_out2,funcout,'k',alpha=alpha2,linewidth=linew)
    ax.plot(x_hor_line2,y_hor_line1 ,'k--')
    if( t_lump is not None ) : 
        ax.plot(x_vert_line/binary_period,y_vert_line,'k--')
    ax.xaxis.set_major_formatter(NullFormatter())
    # ax.yaxis.set_major_formatter(ymajorFormatter)
    # ax.xaxis.set_major_formatter(xmajorFormatter)
    #ax.set_xlabel(r't [$10^4$ M] ')
    #ax.legend(loc=loc)
    ax2.set_xlim(1e-10,tmax/1e4)
    ax2.plot(t_out,funcout)
    ax2.cla()
    ax2.set_xlabel(r'$t \ [10^4 M]$',size=lsize)

    linew=None
    ax = figs[-1].add_subplot(2,1,2)
    funcout = np.log10(np.abs(power))
    ymax = np.max(funcout[:,iend,0]) + 0.5
    ymin = ymax - 3.3
    ax.set_ylim(ymin,ymax)
    ax.set_xlim(1e-10,tmax/binary_period)
    ax.set_ylabel(r'$\log_{10} A_m$',size=lsize)
    ax.plot(t_out2,funcout[:,iend,0],'k', label='m=0',alpha=alpha2,linewidth=linew)
    ax.plot(t_out2,funcout[:,ibeg,0],'k', label=None ,alpha=alpha1,linewidth=linew)
    ax.plot(t_out2,funcout[:,iend,1],'b', label='m=1',alpha=alpha2,linewidth=linew)
    ax.plot(t_out2,funcout[:,ibeg,1],'b', label=None ,alpha=alpha1,linewidth=linew)
    ax.plot(t_out2,funcout[:,iend,2],'r', label='m=2',alpha=alpha2,linewidth=linew)
    ax.plot(t_out2,funcout[:,ibeg,2],'r', label=None ,alpha=alpha1,linewidth=linew)
    if( t_lump is not None ) : 
        ax.plot(x_vert_line/binary_period,y_vert_line,'k--')
    #ax.yaxis.set_major_formatter(ymajorFormatter)
    #ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.set_xlabel(r'$t \ [t_\mathrm{bin}]$ ',size=lsize)
    ax.legend(loc=loc)
    figs[-1].subplots_adjust(hspace=0.)

    ######################

    nwin+=1
    order_label = 'criteria-amp4-'+tag
    
    fignames = np.append(fignames,(dirout+'lump-'+order_label+funcname))
    figs = np.append(figs,(plt.figure(nwin)))
    figs[-1].clf()
    
    linew=2.
    funcout=fsmooth_lump_m1_o_m0[:]
    ax = figs[-1].add_subplot(2,1,1)
    ax2 = ax.twiny()
    ax.set_ylim(1e-10,0.8)
    ax.set_xlim(1e-10,tmax/binary_period)
    ax.set_ylabel(r'$\int_{2a}^{3a}\bar{A_1} dr / \int_{2a}^{3a} \bar{A_0} dr$',size=lsize)
    ax.plot(t_out2,funcout,'k',alpha=alpha2,linewidth=linew)
    ax.plot(x_hor_line2,y_hor_line1 ,'k--')
    if( t_lump is not None ) : 
        ax.plot(x_vert_line/binary_period,y_vert_line,'k--')
    ax.xaxis.set_major_formatter(NullFormatter())
    # ax.yaxis.set_major_formatter(ymajorFormatter)
    # ax.xaxis.set_major_formatter(xmajorFormatter)
    #ax.set_xlabel(r't [$10^4$ M] ')
    #ax.legend(loc=loc)
    ax2.set_xlim(1e-10,tmax/1e4)
    ax2.plot(t_out,funcout)
    ax2.cla()
    ax2.set_xlabel(r'$t \ [10^4 M]$',size=lsize)

    linew=None
    ax = figs[-1].add_subplot(2,1,2)
    funcout = np.log10(fint_m)
    ymax = np.max(funcout) + 0.5
    ymin = ymax - 3.3
    ax.set_ylim(ymin,ymax)
    ax.set_xlim(1e-10,tmax/binary_period)
    ax.set_ylabel(r'$\log_{10} \int_{2a}^{3a} A_m dr$',size=lsize)
    ax.plot(t_out2,funcout[0,:],'k', label='m=0',alpha=alpha2,linewidth=linew)
    ax.plot(t_out2,funcout[1,:],'b', label='m=1',alpha=alpha2,linewidth=linew)
    ax.plot(t_out2,funcout[2,:],'r', label='m=2',alpha=alpha2,linewidth=linew)
    if( t_lump is not None ) : 
        ax.plot(x_vert_line/binary_period,y_vert_line,'k--')
    #ax.yaxis.set_major_formatter(ymajorFormatter)
    #ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.set_xlabel(r'$t \ [t_\mathrm{bin}]$ ',size=lsize)
    ax.legend(loc=loc)
    figs[-1].subplots_adjust(hspace=0.)


    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=dpi,bbox_inches='tight')

    if(savepdf):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.pdf' %fignames[ifig], dpi=dpi,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def find_tlump(mode_spacetime_filename='mode_spacetime_analysis.h5',
               runname='inject_disk',
               funcname='rhoav',
               m1_o_m2_thresh = 2.5,
               m1_o_m0_thresh = 0.2,
               tlump1_estimate = 1.e4,
               tlump2_estimate = 1.e4
              ):
    """  
    find_tlump():
    ------------
    -- return with the time at which the simulation reaches the criteria that indicates when we say a lump is present; 
    
    """
    plt.ioff()

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-power-'

    asep = sim_info['rsep0'][0]

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    power = h5mode[funcname+'_spacetime_power'].value[:]
    h5mode.close()
    print(" .....done ")

    r_out /= asep

    rbeg = 2.
    rend = 2.5
    ibeg, iend = find_bracket(rbeg,rend,r_out)

    # itlump1_beg, itlump1_end = find_bracket(tlump1_estimate*0.8,tlump1_estimate*1.2,t_out)
    # itlump2_beg, itlump2_end = find_bracket(tlump2_estimate*0.8,tlump2_estimate*1.2,t_out)

    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 
    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fsmooth_m0 = signal.convolve2d(power[:,:,0],sm_window,mode='same',boundary='symm')
    fsmooth_m1 = signal.convolve2d(power[:,:,1],sm_window,mode='same',boundary='symm')
    fsmooth_m2 = signal.convolve2d(power[:,:,2],sm_window,mode='same',boundary='symm')

    fsmooth_m1_o_m2 = fsmooth_m1 / fsmooth_m2
    fsmooth_m1_o_m0 = fsmooth_m1 / fsmooth_m0

    ##  Looking at spacetime-lump-power-lump-lines-power-rhoav-lin-m1_o_m2-smooth.png
    # threshold 1 :
    i_thresh1_1 = find_threshold_transits(fsmooth_m1_o_m2[:,ibeg],m1_o_m2_thresh)
    i_thresh1_2 = find_threshold_transits(fsmooth_m1_o_m2[:,iend],m1_o_m2_thresh)

    ##  Looking at spacetime-lump-power-lump-lines-power-rhoav-lin-m1_o_m0-smooth.png
    # threshold 2 :
    #it2_2 = np.abs(fsmooth_m1_o_m0[:,iend] - m1_o_m0_thresh).argmin()

    i_thresh2_1 = find_threshold_transits(fsmooth_m1_o_m0[:,ibeg],m1_o_m0_thresh)
    i_thresh2_2 = find_threshold_transits(fsmooth_m1_o_m0[:,iend],m1_o_m0_thresh)
    
    print("i_thresh1_1 = ", i_thresh1_1 )
    print("i_thresh1_2 = ", i_thresh1_2 )
    print("i_thresh2_1 = ", i_thresh2_1 )
    print("i_thresh2_2 = ", i_thresh2_2 )

    print("All done ")
    sys.stdout.flush()

    return t_out[np.array([i_thresh1_1])], t_out[np.array([i_thresh1_2])], t_out[np.array([i_thresh2_1])], t_out[np.array([i_thresh2_2])]


###############################################################################
###############################################################################
###############################################################################

def find_tlump2(mode_spacetime_filename='mode_spacetime_analysis.h5',
               runname='inject_disk',
               funcname='rhoav',
               m1_o_m2_thresh = 2.5,
               m1_o_m0_thresh = 0.2,
               tlump1_estimate = 1.e4,
               tlump2_estimate = 1.e4
              ):
    """  
    find_tlump2():
    ------------
    -- one using the dr_lump integrated amplitudes 
    -- return with the time at which the simulation reaches the criteria that indicates when we say a lump is present; 
    
    """
    plt.ioff()

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-power-'

    asep = sim_info['rsep0'][0]

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    power = h5mode[funcname+'_spacetime_power'].value[:]
    h5mode.close()
    print(" .....done ")

    r_out /= asep

    rbeg = 2.
    rend = 2.5
    ibeg, iend = find_bracket(rbeg,rend,r_out)

    # itlump1_beg, itlump1_end = find_bracket(tlump1_estimate*0.8,tlump1_estimate*1.2,t_out)
    # itlump2_beg, itlump2_end = find_bracket(tlump2_estimate*0.8,tlump2_estimate*1.2,t_out)

    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 
    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fsmooth_m0 = signal.convolve2d(power[:,:,0],sm_window,mode='same',boundary='symm')
    fsmooth_m1 = signal.convolve2d(power[:,:,1],sm_window,mode='same',boundary='symm')
    fsmooth_m2 = signal.convolve2d(power[:,:,2],sm_window,mode='same',boundary='symm')

    rbeg2 = 2.
    rend2 = 3.
    ibeg2, iend2 = find_bracket(rbeg2,rend2,r_out)
    fsmooth_lump_m0 = np.sum(fsmooth_m0[:,ibeg2:iend2],axis=1) 
    fsmooth_lump_m1 = np.sum(fsmooth_m1[:,ibeg2:iend2],axis=1) 
    fsmooth_lump_m2 = np.sum(fsmooth_m2[:,ibeg2:iend2],axis=1) 
    fsmooth_lump_m1_o_m2 = fsmooth_lump_m1 / fsmooth_lump_m2
    fsmooth_lump_m1_o_m0 = fsmooth_lump_m1 / fsmooth_lump_m0
    

    ##  Looking at spacetime-lump-power-lump-lines-power-rhoav-lin-m1_o_m0-smooth.png
    # threshold 2 :

    i_thresh = find_threshold_transits(fsmooth_lump_m1_o_m0[:],m1_o_m0_thresh)
    
    print("i_thresh = ", i_thresh )

    print("All done ")
    sys.stdout.flush()

    return t_out[np.array([i_thresh])]


###############################################################################
###############################################################################
###############################################################################

def plot_lump_power_avgs(mode_filename='mode_analysis.h5',
                         mode_spacetime_filename='mode_spacetime_analysis.h5',
                         savepng=1,
                         runname='inject_disk',
                         funcnames=['rhoav','Lut','bsq','pavg'],
                         nlev=256,
                         nticks=10,
                         dolog=True,
                         max_order=3,
                         maxf=1.,
                         minf=1e-10,
                         norbits_avg=20,
                         r_min=None,
                         r_max=10.
                         ):
    """  
    plot_lump_power_avgs():
    ------------
    -- makes plots regarding the lump's power oer r and t
    
    """
    plt.ioff()

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-power-'

    nfunc = len(funcnames)

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    period_bin = 2.*np.pi / omega_bin
    n_surf_avg = np.int(norbits_avg * period_bin / sim_info['surf_freq'] )
    #print("omega_bin = ", omega_bin)
    
    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')

    nr = len(r_out)

    
    ifunc = 0 
    for funcname in funcnames : 
        power = h5mode[funcname+'_spacetime_power'].value[:]
        if( ifunc == 0 ) :
            fshape = power.shape
            n_modes = fshape[-1]
            func0 = np.ndarray((nfunc,nr,n_modes),dtype=power.dtype)
            rtmp = np.outer(r_out,np.ones(n_modes))

        func0_avg = np.mean(power[-n_surf_avg:,:,:],axis=0) / rtmp
        func0[ifunc,:,:] = func0_avg[:,:] 
        ifunc += 1 
        
    h5mode.close()
    print(" .....done ")


    r_out /= asep
    
    alpha = 0.5

    fshape = power.shape
    if( (max_order+1) > fshape[2] ) :
        sys.exit("max_order  needs to be smaller:  ", max_order, fshape[2])

    if( dolog ) : 
        suffix = '-log'
        if( maxf is None ) : 
            maxf = np.log10(1.)
        if( minf is None ) :
            minf = maxf - 3.
    else :
        suffix = '-lin'

    if( r_min is None ) : 
        r_min = np.min(r_out)
    if( r_max is None ) : 
        r_max = np.max(r_out)

    print(" Plotting data....")
    nwin=0

    ylabels=[r'$\bar{A_0}/\max(\bar{A_0})$',r'$\bar{A_1}/\max(\bar{A_1})$',r'$\bar{A_2}/\max(\bar{A_2})$',r'$\bar{A_3}/\max(\bar{A_3})$']

    for m_order in np.arange(0,max_order+1) : 
        order_label = str('m=%1d' %m_order)
        fignames = [dirout+'lump-'+order_label+'-power-avg-'+suffix+'-norm']
        figs = [plt.figure(nwin)]
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r'r [a] ')
        ax.set_ylabel(ylabels[m_order])
        ax.set_xlim(r_min,r_max)
        ax.set_ylim(minf,maxf)
        for ifunc in np.arange(len(funcnames)) : 
            fplot = func0[ifunc,:,m_order]
            ax.plot(r_out,fplot/np.max(fplot),label=funcnames[ifunc],alpha=alpha)
        ax.legend(loc=0)
        

        if(savepng):
            for ifig in np.arange(len(figs)) : 
                figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_phases_spacetime(mode_filename='mode_analysis.h5',
                              mode_spacetime_filename='mode_spacetime_analysis.h5',
                              savepng=1,
                              runname='inject_disk',
                              funcname='rhoav',
                              nlev=256,
                              nticks=10,
                              dolog=False,
                              cmap  = plt.cm.Spectral_r,
                              m_order=1,
                              maxf=None,
                              minf=None
                              ):
    """  
    plot_lump_phases_spacetime():
    ------------
    -- makes plots regarding the lump's phase over r and t
    
    """
    plt.ioff()

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-phase-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    phase = h5mode[funcname+'_spacetime_phase'].value[:]
    h5mode.close()
    print(" .....done ")

    t_out *= 1e-4
    r_out /= asep
    
    alpha = 0.5

    fshape = phase.shape
    if( m_order > fshape[2] ) :
        sys.exit("m_order  needs to be smaller:  ", m_order, fshape[2])

    rtmp = np.outer(np.ones(len(t_out)), r_out)
    
    func0 = phase[:,:,m_order]
    
    if( dolog ) : 
        func = np.log10(np.abs(func0))
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = maxf - 3.
    else : 
        func = func0
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = 1.e-3* maxf 

    rticks = np.linspace(minf,maxf,nticks)

    order_label = str('m=%1d' %m_order)

    print("data shape = ", func.shape)

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)
    r_closeup = 5.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1
    
    print(" Plotting data....")
    nwin=0
    fignames = [dirout+'lump-'+order_label+'-phase-'+funcname+'-lin']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-phase-'+funcname+'-lin-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-phase-'+funcname+'-lin-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-phase-'+funcname+'-lin-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min2,t_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    
    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_phase_fft_spacetime(mode_filename='mode_analysis.h5',
                              mode_spacetime_filename='mode_spacetime_analysis.h5',
                              savepng=1,
                              runname='inject_disk',
                              funcname='rhoav',
                              nlev=256,
                              nticks=10,
                              dolog=False,
                              cmap  = plt.cm.Spectral_r,
                              m_order=1,
                              maxf=None,
                              minf=None
                              ):
    """  
    plot_lump_phase_fft_spacetime():
    ------------
    -- makes plots regarding the lump's phase over r and t
    
    """
    plt.ioff()

    # Set the time offset from the beginning of each run to start the FFT from: 

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq', 't_lump_beg', 'tbeg']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-phase-'

    t_start = 3.e4
    if( sim_info['t_lump_beg'] is not None ) :
        t_start = sim_info['t_lump_beg']

    tbeg = sim_info['tbeg']
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)
    it_start = np.int( (t_start-tbeg) / sim_info['surf_freq'] )

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value[it_start:]
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    phase = h5mode[funcname+'_spacetime_phase'].value[it_start:,:,:]
    h5mode.close()
    print(" .....done ")

    r_out /= asep
    
    alpha = 0.5

    fshape = phase.shape
    if( m_order > fshape[2] ) :
        sys.exit("m_order  needs to be smaller:  ", m_order, fshape[2])

    func0 = np.cos(phase[:,:,m_order])

    freq, power, amp = my_fft(t_out, func0, axis=0)
    nfreq = len(freq)/2
    func = power[0:nfreq,:]
    freq_bin = omega_bin / (2. * np.pi)
    freq_out = freq[0:nfreq] / freq_bin 
    fft_phase = np.angle(amp[0:nfreq])
    fft_phase[ fft_phase < 0. ] += 2. * np.pi 

    t_out *= 1e-4
    
    if( dolog ) : 
        suffix = '-log'
        func = np.log10(func)
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = maxf - 3.
    else : 
        suffix = '-lin'
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = 1.e-3* maxf 

    rticks = np.linspace(minf,maxf,nticks)

    order_label = str('m=%1d' %m_order)

    print("data shape = ", func.shape)

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    freq_min = np.min(freq_out)
    freq_max = np.max(freq_out)
    r_closeup = 5.
    freq_closeup_max = 3.
    freq_closeup_max2 = 0.5

    print(" Plotting data....")
    nwin=0
    name_base = dirout+'lump-'+order_label+'-phasefft-power-'+funcname+suffix
    fignames = [name_base]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_closeup_max2)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    if( dolog ) :
        return
    
    # phases of FFT of phases: ....ugh
    nwin=0
    name_base = dirout+'lump-'+order_label+'-phasefft-phase-'+funcname+suffix
    fignames = [name_base]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,fft_phase,vmin=1e-10,vmax=(2.*np.pi),cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,fft_phase,vmin=1e-10,vmax=(2.*np.pi),cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,fft_phase,vmin=1e-10,vmax=(2.*np.pi),cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_closeup_max2)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,fft_phase,vmin=1e-10,vmax=(2.*np.pi),cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    
    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_power_fft_spacetime(mode_filename='mode_analysis.h5',
                              mode_spacetime_filename='mode_spacetime_analysis.h5',
                              savepng=1,
                              runname='inject_disk',
                              funcname='rhoav',
                              nlev=256,
                              nticks=10,
                              dolog=False,
                              cmap  = plt.cm.Spectral_r,
                              m_order=1,
                              maxf=None,
                              minf=None
                              ):
    """  
    plot_lump_power_fft_spacetime():
    ------------
    -- makes plots regarding the fft of mode power amplitude over r and t
    
    """
    plt.ioff()


    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq', 't_lump_beg', 'tbeg']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-modeamp-'

    # Set the time offset from the beginning of each run to start the FFT from: 
    t_start = 3.e4
    if( sim_info['t_lump_beg'] is not None ) :
        t_start = sim_info['t_lump_beg']

    tbeg = sim_info['tbeg']
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)
    it_start = np.int( (t_start-tbeg) / sim_info['surf_freq'] )

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value[it_start:]
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    modeamp = h5mode[funcname+'_spacetime_power'].value[it_start:,:,:]
    h5mode.close()
    print(" .....done ")

    r_out /= asep
    
    alpha = 0.5

    fshape = modeamp.shape
    if( m_order > fshape[2] ) :
        sys.exit("m_order  needs to be smaller:  ", m_order, fshape[2])

    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 
    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fsmooth_m0 = signal.convolve2d(modeamp[:,:,0],sm_window,mode='same',boundary='symm')
    
    func0 = modeamp[:,:,m_order]

    freq, power, amp = my_fft(t_out, signal.detrend(func0,axis=0), axis=0)
    nfreq = np.int(len(freq)/2)
    func = power[0:nfreq,:]
    freq_bin = omega_bin / (2. * np.pi)
    freq_out = freq[0:nfreq] / freq_bin 
    fft_modeamp = np.angle(amp[0:nfreq])
    fft_modeamp[ fft_modeamp < 0. ] += 2. * np.pi 

    t_out *= 1e-4
    
    if( dolog ) : 
        suffix = '-log'
        func = np.log10(func)
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = maxf - 3.
    else : 
        suffix = '-lin'
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = 1.e-3* maxf 

    rticks = np.linspace(minf,maxf,nticks)

    order_label = str('m=%1d' %m_order)

    print("data shape = ", func.shape)

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    freq_min = np.min(freq_out)
    freq_max = np.max(freq_out)
    r_closeup = 5.
    freq_closeup_max = 3.
    freq_closeup_max2 = 0.5

    print(" Plotting data....")
    nwin=0
    name_base = dirout+'lump-'+order_label+'-modeampfft-power-'+funcname+suffix
    fignames = [name_base]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_closeup_max2)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    if( dolog ) :
        return
    
    # phases of FFT of phases: ....ugh
    nwin=0
    name_base = dirout+'lump-'+order_label+'-modeampfft-phase-'+funcname+suffix
    fignames = [name_base]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,fft_modeamp,vmin=1e-10,vmax=(2.*np.pi),cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,fft_modeamp,vmin=1e-10,vmax=(2.*np.pi),cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,fft_modeamp,vmin=1e-10,vmax=(2.*np.pi),cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_closeup_max2)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,fft_modeamp,vmin=1e-10,vmax=(2.*np.pi),cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    
    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_lump_omegas_spacetime(mode_filename='mode_analysis.h5',
                              mode_spacetime_filename='mode_spacetime_analysis.h5',
                              savepng=1,
                              runname='inject_disk',
                              funcname='rhoav',
                              nlev=256,
                              nticks=10,
                              dolog=False,
                              cmap  = plt.cm.Spectral_r,
                              m_order=1,
                              maxf=None,
                              minf=None
                              ):
    """  
    plot_lump_omegas_spacetime():
    ------------
    -- makes plots regarding the lump's orbital frequency  oer r and t
    
    """
    plt.ioff()

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-omega-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    omega = h5mode[funcname+'_spacetime_omega'].value[:]
    h5mode.close()
    print(" .....done ")

    t_out *= 1e-4
    r_out /= asep
    
    alpha = 0.5

    fshape = omega.shape
    if( m_order > fshape[2] ) :
        sys.exit("m_order  needs to be smaller:  ", m_order, fshape[2])

    rtmp = np.outer(np.ones(len(t_out)), r_out)
    
    func0 = omega[:,:,m_order]
    
    if( dolog ) : 
        func = np.log10(np.abs(func0))
        if( maxf is None ) : 
            maxf = np.log10(2.*omega_bin)
        if( minf is None ) :
            minf = maxf - 3.
    else : 
        func = func0
        if( maxf is None ) : 
            maxf = 2.*omega_bin
        if( minf is None ) :
            minf = 1.e-3 * maxf 


    order_label = str('m=%1d' %m_order)

    print("data shape = ", func.shape)

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)
    r_closeup = 5.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1
    
    print(" Plotting data....")
    nwin=0
    fignames = [dirout+'lump-'+order_label+'-omega-'+funcname+'-lin']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-omega-'+funcname+'-lin-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-omega-'+funcname+'-lin-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-omega-'+funcname+'-lin-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min2,t_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    
    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def compare_hist_surface_data(mode_filename='mode_analysis.h5',
                              mode_spacetime_filename='mode_spacetime_analysis.h5',
                              savepng=1,
                              runname='inject_disk',
                              funcname='rhoav',
                              nlev=256,
                              nticks=10,
                              dolog=True,
                              cmap  = plt.cm.Spectral_r,
                              m_order=1,
                              maxf=None,
                              minf=None
                              ):
    """  
    plot_lump_phases_spacetime():
    ------------
    -- makes plots regarding the lump's power oer r and t
    
    """
    plt.ioff()

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    dirout += 'spacetime-lump-phase-'

    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    phase = h5mode[funcname+'_spacetime_phase'].value[:]
    h5mode.close()
    print(" .....done ")

    t_out *= 1e-4
    r_out /= asep
    
    alpha = 0.5

    fshape = phase.shape
    if( m_order > fshape[2] ) :
        sys.exit("m_order  needs to be smaller:  ", m_order, fshape[2])

    rtmp = np.outer(np.ones(len(t_out)), r_out)
    
    func0 = phase[:,:,m_order]
    
    if( dolog ) : 
        func = np.log10(np.abs(func0))
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = maxf - 3.
    else : 
        func = func0
        if( maxf is None ) : 
            #maxf = np.nanmax(func) 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = 1.e-10


    order_label = str('m=%1d' %m_order)

    print("data shape = ", func.shape)

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    t_min = np.min(t_out)
    t_max = np.max(t_out)
    r_closeup = 5.
    t_closeup_max = t_max
    t_closeup_min = t_max - 1.2
    t_closeup_min2 = t_max - 0.1
    
    print(" Plotting data....")
    nwin=0
    fignames = [dirout+'lump-'+order_label+'-phase-'+funcname+'-log']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-phase-'+funcname+'-log-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_min,t_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-phase-'+funcname+'-log-closeup2'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min,t_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(dirout+'lump-'+order_label+'-phase-'+funcname+'-log-closeup3'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(t_closeup_min2,t_closeup_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    
    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################
###############################################################################

def plot_all_ffts(mode_filename='mode_analysis.h5',
                  mode_spacetime_filename='mode_spacetime_analysis.h5',
                  savepng=1,
                  runname='inject_disk',
                  funcname='rhoav',
                  nlev=256,
                  nticks=10,
                  dolog=False,
                  cmap  = plt.cm.Spectral_r,
                  m_order=1,
                  maxf=None,
                  minf=None
                  ):
    """  
    plot_all_ffts():
    ------------
    -- puts on the same plot FFTs of various quantities of interest
    -- for all times and with only post-lump times;
    --  e_lump(t), rhoav_lump, Lut_lump, bsq_lump, pgas_lump, A_{i} from rhoav integrated over lump region and not, 
    -- Mdot_horizon,  lum_tot(t), 
    
    """
    plt.ioff()





    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'surf_freq', 't_lump_beg', 'tbeg']
    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    full_mode_filename = sim_info['dirname']+'modes/'+mode_spacetime_filename
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/ffts/'
    dirout += 'spacetime-lump-modeamp-'

    # Set the time offset from the beginning of each run to start the FFT from: 
    t_start = 3.e4
    if( sim_info['t_lump_beg'] is not None ) :
        t_start = sim_info['t_lump_beg']

    tbeg = sim_info['tbeg']
    asep = sim_info['rsep0'][0]
    omega_bin = sim_info['omega_bin_orbit']
    #print("omega_bin = ", omega_bin)
    it_start = np.int( (t_start-tbeg) / sim_info['surf_freq'] )

    # Get data for mode strength: 
    print(" Reading mode data ...")
    h5f = h5py.File(surf_filename,'r')
    t_out = h5f['tout'].value[it_start:]
    r_out = h5f['rout'].value
    h5f.close()
    h5mode = h5py.File(full_mode_filename,'r')
    modeamp = h5mode[funcname+'_spacetime_power'].value[it_start:,:,:]
    h5mode.close()
    print(" .....done ")

    r_out /= asep
    
    alpha = 0.5

    fshape = modeamp.shape
    if( m_order > fshape[2] ) :
        sys.exit("m_order  needs to be smaller:  ", m_order, fshape[2])

    smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 
    n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
    sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
    fsmooth_m0 = signal.convolve2d(modeamp[:,:,0],sm_window,mode='same',boundary='symm')
    
    func0 = modeamp[:,:,m_order]

    freq, power, amp = my_fft(t_out, signal.detrend(func0,axis=0), axis=0)
    nfreq = np.int(len(freq)/2)
    func = power[0:nfreq,:]
    freq_bin = omega_bin / (2. * np.pi)
    freq_out = freq[0:nfreq] / freq_bin 
    fft_modeamp = np.angle(amp[0:nfreq])
    fft_modeamp[ fft_modeamp < 0. ] += 2. * np.pi 

    t_out *= 1e-4
    
    if( dolog ) : 
        suffix = '-log'
        func = np.log10(func)
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = maxf - 3.
    else : 
        suffix = '-lin'
        if( maxf is None ) : 
            maxf = np.nanmax(func) 
        if( minf is None ) :
            minf = 1.e-3* maxf 

    rticks = np.linspace(minf,maxf,nticks)

    order_label = str('m=%1d' %m_order)

    print("data shape = ", func.shape)

    r_min = np.min(r_out)
    r_max = np.max(r_out)
    freq_min = np.min(freq_out)
    freq_max = np.max(freq_out)
    r_closeup = 5.
    freq_closeup_max = 3.
    freq_closeup_max2 = 0.5

    print(" Plotting data....")
    nwin=0
    name_base = dirout+'lump-'+order_label+'-modeampfft-power-'+funcname+suffix
    fignames = [name_base]
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_max)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
    
    nwin+=1
    fignames = np.append(fignames,(name_base+'-closeup'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_ylabel(r'$\Omega$ [$\Omega_{bin}$] ')
    ax.set_xlabel(r'r [a] ')
    ax.set_xlim(r_min,r_closeup)
    ax.set_ylim(freq_min,freq_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, freq_out,func,vmin=minf,vmax=maxf,cmap=cmap)
    ax.plot(r_out,m_order*((r_out*asep)**(-1.5)/omega_bin), color='white')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    if(savepng):
        for ifig in np.arange(len(figs)) : 
            figs[ifig].savefig('%s.png' %fignames[ifig], dpi=300,bbox_inches='tight')


    print("All done ")
    sys.stdout.flush()


    

################################################################################
################################################################################

def get_tmax_of_allruns(all_runs):

    tmax = -1.
    for run in all_runs :
        sim_info=get_sim_info(['tend'],runname=run)
        tmax = np.max([tmax,sim_info['tend']])


    return tmax





################################################################################
################################################################################

def plot_curl_b(runname,itime):

    from polp_lite import polp

    pnames = ['dirname', 'dump_name', 'traj_file']
    sim_info = get_sim_info(pnames, runname=runname)
    rmax = 100.
    dirout = './'
    dolog=True
    minf=-3.
    maxf = 3.
    coolfminf=-12.
    coolfmaxf =-6.
    rhomin = -7.
    rhomax = -1.
    bsqmin = -10.
    bsqmax = -4.
    
    curl_b_1, curl_b_2, curl_b_3, curl_b_mag = polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_b_1',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],minf=minf,maxf=maxf,blackout_cutout=True)
    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_b_2',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],func0=curl_b_2,retfunc=False,minf=minf,maxf=maxf,blackout_cutout=True)
    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_b_3',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],func0=curl_b_3,retfunc=False,minf=minf,maxf=maxf,blackout_cutout=True)
    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_b_mag',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],func0=curl_b_mag,retfunc=False,minf=minf,maxf=maxf,blackout_cutout=True)
    logbsqorig = polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='bsq',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],minf=bsqmin,maxf=bsqmax,blackout_cutout=True)
    bsqorig = 10.**logbsqorig
    bshape = np.copy(bsqorig.shape)
    bshape[1] -= 1 
    bsq = np.empty(bshape)
    bsq[:,:] = bsqorig[:,:-1]
    bmag = np.sqrt(bsq)
    funcnew = curl_b_mag / bmag

    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_b_mag_over_bmag',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],func0=funcnew,retfunc=False,minf=0.,maxf=8.,blackout_cutout=True)

    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='rho',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],retfunc=False,minf=rhomin,maxf=rhomax,userad=True,blackout_cutout=True)
    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='coolfunc',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],retfunc=False,minf=coolfminf,maxf=coolfmaxf,userad=True,blackout_cutout=True)

    print("min max curl/bmag = ", np.min(funcnew), np.max(funcnew)) 
    print("min max bsq  = ", np.min(bsq), np.max(bsq)) 
    print("min max bmag  = ", np.min(bmag), np.max(bmag)) 
    
    return


################################################################################
################################################################################

def plot_curl_v(runname,itime):

    from polp_lite import polp

    pnames = ['dirname', 'dump_name', 'traj_file']
    sim_info = get_sim_info(pnames, runname=runname)
    rmax = 100.
    dirout = './curl'
    dolog=True
    minf=0.
    maxf =6.
    coolfminf=-12.
    coolfmaxf =-6.
    rhomax = -1.
    rhomin = -7.
    
    curl_v_1, curl_v_2, curl_v_3, curl_v_mag = polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_v_1',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],minf=minf,maxf=maxf,blackout_cutout=True,userad=True)
    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_v_2',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],func0=curl_v_2,retfunc=False,minf=minf,maxf=maxf,blackout_cutout=True,userad=True)
    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_v_3',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],func0=curl_v_3,retfunc=False,minf=minf,maxf=maxf,blackout_cutout=True,userad=True)
    polp(itime=itime,dirname=sim_info['dirname'],show_plot=False,funcname='curl_v_mag',dolog=dolog,savepng=True,ith=79,iph=None,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,dirout=dirout,usegdumpcoord=True,trajfile=sim_info['traj_file'],func0=curl_v_mag,retfunc=False,minf=minf,maxf=maxf,blackout_cutout=True,userad=True)

    return

################################################################################
def write_int_func(h5out,gdet,func,funcname) : 
    func_int  = np.sum(func * gdet, axis=1)
    name_int = funcname+'_int'
    dset = h5out.create_dataset(name_int,   data=func_int)
    return
    

################################################################################
################################################################################
#
# integrate_curl_b():
# ------------------
#   -- different filename exploring different integration ranges; 
#   -- Calculates all components of the curl of B^i over a volume specified by [rmin,rmax] and
#      [thmin,thmax], then integrates it along theta. 
#
################################################################################
def integrate_curl_b(runname,ibeg,iend,istep=1,rmin=-1.e100,rmax=1.e100,thmin=0.,thmax=np.pi,tag='',dump_name=None) :
  
    pnames = ['dirname', 'dump_name', 'traj_file', 'gdump_name']
    sim_info = get_sim_info(pnames, runname=runname)

    if( dump_name is None ) :
        dump_name = sim_info['dump_name']
        
    print("integrate_curl_b(): working on run = ", runname)
    
    ########################################################################
    # Determine slice we want:
    ########################################################################
    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    gfile  = h5py.File(sim_info['gdump_name'], 'r')

    #  Assuming a diagonal coordinate system:
    slice_rr = (slc_all,slice(0,1,1),slice(0,1,1))
    slice_th = (slice(0,1,1),slc_all,slice(0,1,1))
    rr = read_3d_hdf5_slice('x1',slice_rr,gfile)
    th = read_3d_hdf5_slice('x2',slice_th,gfile)

    ind_matching,  = np.where( (rr >= rmin) & (rr <= rmax) )
    slice_rr = slice(np.min(ind_matching),np.max(ind_matching)+1,1)

    ind_matching,  = np.where( (th >= thmin) & (th <= thmax) )
    slice_th = slice(np.min(ind_matching),np.max(ind_matching)+1,1)
    
    slice_3d = (slice_rr, slice_th, slc_all)
    
    dx2 = get_val('dx2',gfile)
    gfile.close()
    
    ########################################################################
    # Iterate over time sequence:
    #   -- assume the slicing does not change; 
    ########################################################################
    print("integrate_curl_b(): Starting on time iteration")
    for itime in np.arange(ibeg,iend,istep) : 
        timeid = '%06d' %itime
        filename = dump_name + '.' + timeid + '.h5'
        curl_filename = dump_name + '.curl_int'+tag+'.' + timeid + '.h5'
        h5file   = h5py.File(filename, 'r')
        curl_h5file   = h5py.File(curl_filename, 'w')

        timeout = get_val('t',h5file)
        print("Processing dumps ", filename, curl_filename, "at time = ", timeout)

        curl_1, curl_2, curl_3, curl_mag = calc_curl_vec(slice_3d, h5file)

        curl_abs1 = np.abs(curl_1)
        curl_abs2 = np.abs(curl_2)
        curl_abs3 = np.abs(curl_3)
        gdet  = read_3d_hdf5_slice('gdet',slice_3d,h5file)
        tmpones = np.ones_like(gdet)
        gdet *= dx2

        # Integrate over height/theta :
        write_int_func(curl_h5file,gdet,curl_1,'curl_b_1')
        write_int_func(curl_h5file,gdet,curl_2,'curl_b_2')
        write_int_func(curl_h5file,gdet,curl_3,'curl_b_3')
        write_int_func(curl_h5file,gdet,curl_abs1,'curl_b_abs1')
        write_int_func(curl_h5file,gdet,curl_abs2,'curl_b_abs2')
        write_int_func(curl_h5file,gdet,curl_abs3,'curl_b_abs3')
        write_int_func(curl_h5file,gdet,curl_mag,'curl_b_mag')
        write_int_func(curl_h5file,gdet,tmpones,'vol')
        curl_1 = 0
        curl_2 = 0
        curl_3 = 0
        curl_abs1 = 0
        curl_abs2 = 0
        curl_abs3 = 0
        bsq   = read_3d_hdf5_slice('bsq', slice_3d,h5file)
        bmag = np.sqrt(bsq)
        write_int_func(curl_h5file,gdet,bsq,'bsq')
        write_int_func(curl_h5file,gdet,bmag,'bmag')
        bsq =0
        bmag = 0 

        dset = curl_h5file.create_dataset('t',data=[timeout])
        
        h5file.close()
        curl_h5file.close()
  
  
    return

################################################################################
################################################################################
#
# integrate_curl_b_add_funcs():
# ------------------
#
#   -- performs the same operation as done in integrate_curl_b() but
#      for more grid functions;
#
################################################################################
def integrate_curl_b_add_funcs(runname,ibeg,iend,istep=1,rmin=-1.e100,rmax=1.e100,thmin=0.,thmax=np.pi,tag='',dump_name=None,rad_name=None,radtime_offset=None) :
  
    pnames = ['dirname', 'dump_name', 'radflux_name', 'traj_file', 'gdump_name']
    sim_info = get_sim_info(pnames, runname=runname)

    print("integrate_curl_b_add_funcs(): working on run = ", runname)

    if( dump_name is None ) :
        dump_name = sim_info['dump_name']
    
    ########################################################################
    # Determine slice we want:
    ########################################################################
    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    gfile  = h5py.File(sim_info['gdump_name'], 'r')

    #  Assuming a diagonal coordinate system:
    slice_rr = (slc_all,slice(0,1,1),slice(0,1,1))
    slice_th = (slice(0,1,1),slc_all,slice(0,1,1))
    rr = read_3d_hdf5_slice('x1',slice_rr,gfile)
    th = read_3d_hdf5_slice('x2',slice_th,gfile)

    ind_matching,  = np.where( (rr >= rmin) & (rr <= rmax) )
    slice_rr = slice(np.min(ind_matching),np.max(ind_matching)+1,1)

    ind_matching,  = np.where( (th >= thmin) & (th <= thmax) )
    slice_th = slice(np.min(ind_matching),np.max(ind_matching)+1,1)
    
    slice_3d = (slice_rr, slice_th, slc_all)
    
    dx2 = get_val('dx2',gfile)
    gfile.close()
    
    ########################################################################
    # Iterate over time sequence:
    #   -- assume the slicing does not change; 
    ########################################################################
    print("integrate_curl_b(): Starting on time iteration")
    irad = 0 
    for itime in np.arange(ibeg,iend,istep) : 
        timeid = '%06d' %itime
        
        filename = dump_name + '.' + timeid + '.h5'
        curl_filename = dump_name + '.curl_int'+tag+'.' + timeid + '.h5'
        h5file   = h5py.File(filename, 'r')

        if( radtime_offset is None ) : 
            rad_freq = get_val('DT_out7',h5file)[0]
            dump_freq = get_val('DT_out3',h5file)[0]
            rad_step = int( dump_freq / rad_freq ) 
            itimerad = rad_step*itime
        else :
            itimerad = itime + radtime_offset
            
        radtime = '%06d' %itimerad  #harm HDF5 dumps
            
        if( rad_name is None ) : 
            rad_name = sim_info['radflux_name']

        radftmp = rad_name + '.' + radtime + '.h5'
        radfile   = h5py.File(radftmp, 'r')
        
        timeout = get_val('t',h5file)
        print("Processing dumps ", filename, curl_filename, "at time = ", timeout)

        gdet  = read_3d_hdf5_slice('gdet',slice_3d,h5file)
        uu    = read_3d_hdf5_slice('uu', slice_3d,h5file)
        gamma = read_3d_hdf5_slice('gamma', slice_3d,h5file)
        B1    = read_3d_hdf5_slice('B1', slice_3d,h5file)
        B2    = read_3d_hdf5_slice('B2', slice_3d,h5file)
        B3    = read_3d_hdf5_slice('B3', slice_3d,h5file)
        gcov11    = read_3d_hdf5_slice('gcov11', slice_3d,h5file)
        gcov22    = read_3d_hdf5_slice('gcov22', slice_3d,h5file)
        gcov33    = read_3d_hdf5_slice('gcov33', slice_3d,h5file)
        B1 *= np.sqrt(gcov11)
        B2 *= np.sqrt(gcov22)
        B3 *= np.sqrt(gcov33)
        gcov11 = 0 
        gcov22 = 0 
        gcov33 = 0 

        rho      = read_3d_hdf5_slice('rho'     , slice_3d,radfile)
        coolfunc = read_3d_hdf5_slice('coolfunc', slice_3d,radfile)

        kinetic = rho * (gamma**2 - 1.)
        gamma = 0 
        
        gdet *= dx2
        h5file.close()
        radfile.close()

        # Integrate over height/theta : 
        curl_h5file   = h5py.File(curl_filename, 'a')
        write_int_func(curl_h5file,gdet,B1,'B1')
        write_int_func(curl_h5file,gdet,B2,'B2')
        write_int_func(curl_h5file,gdet,B3,'B3')
        write_int_func(curl_h5file,gdet,rho,'rho')
        write_int_func(curl_h5file,gdet,coolfunc,'coolfunc')
        write_int_func(curl_h5file,gdet,uu,'uu')
        write_int_func(curl_h5file,gdet,kinetic,'kinetic')
        
        curl_h5file.close()
  
  
    return

################################################################################
################################################################################
#
# integrate_curl_b_add_funcs2():
# ------------------
#
#   -- adds v[1-3] 
#
################################################################################
def integrate_curl_b_add_funcs2(runname,ibeg,iend,istep=1,rmin=-1.e100,rmax=1.e100,thmin=0.,thmax=np.pi,tag='',dump_name=None,rad_name=None,radtime_offset=None) :
  
    pnames = ['dirname', 'dump_name', 'radflux_name', 'traj_file', 'gdump_name']
    sim_info = get_sim_info(pnames, runname=runname)

    print("integrate_curl_b_add_funcs2(): working on run = ", runname)

    if( dump_name is None ) :
        dump_name = sim_info['dump_name']
    
    ########################################################################
    # Determine slice we want:
    ########################################################################
    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    gfile  = h5py.File(sim_info['gdump_name'], 'r')

    #  Assuming a diagonal coordinate system:
    slice_rr = (slc_all,slice(0,1,1),slice(0,1,1))
    slice_th = (slice(0,1,1),slc_all,slice(0,1,1))
    rr = read_3d_hdf5_slice('x1',slice_rr,gfile)
    th = read_3d_hdf5_slice('x2',slice_th,gfile)

    ind_matching,  = np.where( (rr >= rmin) & (rr <= rmax) )
    slice_rr = slice(np.min(ind_matching),np.max(ind_matching)+1,1)

    ind_matching,  = np.where( (th >= thmin) & (th <= thmax) )
    slice_th = slice(np.min(ind_matching),np.max(ind_matching)+1,1)
    
    slice_3d = (slice_rr, slice_th, slc_all)
    
    dx2 = get_val('dx2',gfile)
    gfile.close()
    
    ########################################################################
    # Iterate over time sequence:
    #   -- assume the slicing does not change; 
    ########################################################################
    print("integrate_curl_b(): Starting on time iteration")
    irad = 0 
    for itime in np.arange(ibeg,iend,istep) : 
        timeid = '%06d' %itime
        
        filename = dump_name + '.' + timeid + '.h5'
        curl_filename = dump_name + '.curl_int'+tag+'.' + timeid + '.h5'
        h5file   = h5py.File(filename, 'r')

        if( radtime_offset is None ) : 
            rad_freq = get_val('DT_out7',h5file)[0]
            dump_freq = get_val('DT_out3',h5file)[0]
            rad_step = int( dump_freq / rad_freq ) 
            itimerad = rad_step*itime
        else :
            itimerad = itime + radtime_offset
            
        radtime = '%06d' %itimerad  #harm HDF5 dumps
            
        if( rad_name is None ) : 
            rad_name = sim_info['radflux_name']

        radftmp = rad_name + '.' + radtime + '.h5'
        radfile   = h5py.File(radftmp, 'r')
        
        timeout = get_val('t',h5file)
        print("Processing dumps ", filename, curl_filename, "at time = ", timeout)

        gdet      = read_3d_hdf5_slice('gdet',   slice_3d,h5file)
        gcov11    = read_3d_hdf5_slice('gcov11', slice_3d,h5file)
        gcov22    = read_3d_hdf5_slice('gcov22', slice_3d,h5file)
        gcov33    = read_3d_hdf5_slice('gcov33', slice_3d,h5file)
        v1        = read_3d_hdf5_slice('v1',     slice_3d,radfile)
        v2        = read_3d_hdf5_slice('v2',     slice_3d,radfile)
        v3        = read_3d_hdf5_slice('v3',     slice_3d,radfile)
        v1 *= np.sqrt(gcov11)
        v2 *= np.sqrt(gcov22)
        v3 *= np.sqrt(gcov33)
        gcov11 = 0 
        gcov22 = 0 
        gcov33 = 0 

        gdet *= dx2
        h5file.close()
        radfile.close()

        # Integrate over height/theta : 
        curl_h5file   = h5py.File(curl_filename, 'a')
        write_int_func(curl_h5file,gdet,v1,'v1')
        write_int_func(curl_h5file,gdet,v2,'v2')
        write_int_func(curl_h5file,gdet,v3,'v3')
        
        curl_h5file.close()
  
  
    return

################################################################################
################################################################################
def aux_plot_int_curl(xx_slice,yy_slice,funcout,figname,title,nticks,cmap,
                      rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
                      dolog=True,vmax=None,vmin=None,fieldlines=None,stream_data=None):
    
    green_led = '#5DFC0A'
    if( vmax is None ): 
        vmax = np.max(funcout)

    if( vmin is None ) :
        if( dolog ) : 
            vmin = vmax - 3.
        else :
            vmin = np.min(funcout)
        
    nwin=0
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r'y [M] ')
    ax.set_xlabel(r'x [M] ')
    ax.set_title('%s' %title)
    rticks = np.linspace(vmin,vmax,nticks)
    ax.set_aspect('equal')
    rplot = 70.
    xmin = np.min(xx_slice)
    xmax = np.max(xx_slice)
    ymin = np.min(yy_slice)
    ymax = np.max(yy_slice)
    ax.set_xlim(-rplot,rplot)
    ax.set_ylim(-rplot,rplot)
    CP = ax.pcolormesh(xx_slice, yy_slice,funcout,vmin=vmin,vmax=vmax,cmap=cmap)

    cutout= plt.Circle((0,0),rcutout,color='k')
    ax.add_artist(cutout)
                
    bh1 = plt.Circle((xbh1,ybh1), rhor1, color=green_led)
    bh2 = plt.Circle((xbh2,ybh2), rhor2, color=green_led)
    ax.add_artist(bh1)
    ax.add_artist(bh2)

    if( fieldlines is not None ) :
        if( stream_data is None ): 
            print("size of fieldlines = ", np.shape(fieldlines))
            print("size of fieldlines[0] = ", np.shape(fieldlines[0]))
            print("size of fieldlines[1] = ", np.shape(fieldlines[1]))
            print("size of xx, yy, ff  = ", np.shape(xx_slice), np.shape(yy_slice), np.shape(funcout))
            # Need to interpolate onto uniform grid to make the streamplot:
            npts = np.max(np.shape(xx_slice))
            xlin = np.linspace(xmin,xmax,npts)
            ylin = np.linspace(ymin,ymax,npts)
            xs,ys = np.meshgrid(xlin,ylin)
            # grid the data.
            xx0 = xx_slice.flatten()
            yy0 = yy_slice.flatten()
            points = np.array([xx0,yy0]).T
            v_x = griddata(points,fieldlines[0].flatten(),(xs,ys))
            v_y = griddata(points,fieldlines[1].flatten(),(xs,ys))
            mask = np.zeros(v_x.shape, dtype=bool)
            mask[:] = ( np.sqrt( xs**2 + ys**2 ) < rcutout )
            v_x = np.ma.array(v_x,mask=mask)
            f_mag = griddata(points,fieldlines[2].flatten(),(xs,ys))
            print("npts =  ", npts)
            print("size of points = ", np.shape(points))
            print("size of fieldlines = ", np.shape(fieldlines))
            print("size of xs, ys = ", np.shape(xs), np.shape(ys))
            print("size of v_x, v_y = ", np.shape(v_x), np.shape(v_y))
            print("xs ys min max = ", np.min(xs), np.max(xs), np.min(ys), np.max(ys))
            print("f_mag min max = ", np.nanmin(f_mag), np.nanmax(f_mag))
            lw = 2.*f_mag / np.nanmax(f_mag)
            stream_data = [xs,ys,v_x,v_y,lw]

    if( stream_data is not None ) :
        xs  = stream_data[0]
        ys  = stream_data[1]
        v_x = stream_data[2]
        v_y = stream_data[3]
        lw  = stream_data[4]
        ax.streamplot(xs,ys,v_x,v_y,  color='black', linewidth=lw, density=3, arrowstyle='->', arrowsize=4)
        

    if( dolog ) : 
        fig.colorbar(CP,ticks=rticks,format='%4.1f')
    else :
        fig.colorbar(CP,ticks=rticks,format='%3.1e')

    fig.savefig('%s.png' %figname, dpi=300)#,bbox_inches='tight')

    
    if( fieldlines is not None ) :
        #return xs, ys, v_x, v_y, f_mag
        return stream_data
    else:
        return
    

################################################################################
################################################################################
def azimuthal_avg_rel(func): 

    azi_avg = np.mean(func,axis=1)
    azi_std = np.std(func,axis=1)
    azi_avg_2d = np.outer(azi_avg, np.ones(func.shape[1]))
    azi_std_2d = np.outer(azi_std, np.ones(func.shape[1]))
    diff_func = (func - azi_avg_2d)
    rel_avg_func = diff_func/azi_std_2d 
    
    return diff_func, rel_avg_func 
    
################################################################################
################################################################################
def zero_norm_correlation(f1, f2) :

    if( (f1.shape != f2.shape) and (len(f1.shape) != 2 ) ) :
        sys.exit("problem with arrays, need to be 2-d")

    ftmp, f1_azi = azimuthal_avg_rel(f1)
    ftmp, f2_azi = azimuthal_avg_rel(f2)
    
    f_corr = f1_azi * f2_azi 

    return f_corr


################################################################################
################################################################################
#
# plot_integrated_curl_b():
# ------------------
#
#   -- plots the data generated in integrate_curl_b()
#
#   -- needs the rmin, rmax combo used for the calculation to read in the
#      right part of the r grid.
################################################################################
def plot_integrated_curl_b(runname,itime,
                           rmin=-1.e100,rmax=1.e100,
                           cmap  = plt.cm.Spectral_r,
                           nticks=10,
                           time_unit_str='M',
                           tag='',
                           dump_name=None,
                           dirout=None
                          ) :
  
    pnames = ['dirname', 'dump_name', 'traj_file', 'gdump_name', 'header_file']
    sim_info = get_sim_info(pnames, runname=runname)

    if( dump_name is None ) :
        dump_name = sim_info['dump_name']

    if( dirout is None ) : 
        dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/curl/'
    
    timeid = '%06d' %itime
    curl_filename = dump_name + '.curl_int'+tag+'.' + timeid + '.h5'
    print("plot_integrated_curl_b(): working on run = ", runname, curl_filename)
    suffix = tag+'-'+timeid

    # Get coordinates:
    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    gfile  = h5py.File(sim_info['gdump_name'], 'r')

    slice_rr = (slc_all,slice(0,1,1),slice(0,1,1))
    rr_orig = read_3d_hdf5_slice('x1',slice_rr,gfile)

    ind_matching,  = np.where( (rr_orig >= rmin) & (rr_orig <= rmax) )
    rr = rr_orig[np.min(ind_matching):np.max(ind_matching)+1]

    slice_ph = (slice(0,1,1),slice(0,1,1),slc_all)
    ph       = read_3d_hdf5_slice('x3',slice_ph,gfile)

    gfile.close()

    rr_2d = np.outer(rr,np.ones_like(ph))
    ph_2d = np.outer(np.ones_like(rr),ph)

    xx_slice_tmp = rr_2d * np.cos(ph_2d)
    yy_slice_tmp = rr_2d * np.sin(ph_2d)

    xx_slice = viz_identify_symmetry_boundary(xx_slice_tmp)
    yy_slice = viz_identify_symmetry_boundary(yy_slice_tmp)

    rcutout = np.min(rr)

    ###########################################################
    # Open curl file: 
    ###########################################################
    h5file = h5py.File(curl_filename,'r')
    
    
    # Get bbh positions:
    gfile  = h5py.File(sim_info['header_file'], 'r')
    m_bh1 = get_val('m_bh1',gfile)
    m_bh2 = get_val('m_bh2',gfile)
    rhor1 = m_bh1
    rhor2 = m_bh2
    gam = get_val('gam',gfile)
    gfile.close() 
    xbh1, ybh1, zbh1, xbh2, ybh2, zbh2 = get_bbh_pos(h5file['t'][0],runname=runname,spinning=False)

    ###########################################################

    # functions  in the denominator: 
    # norm_funcs = ['vol','bmag','bsq','rho']
    #norm_funcs = ['bmag']
    norm_funcs = ['vol']
    #norm_funcs = ['sqrtrho','sqrtenergy']

    # available functions: 
    all_funcnames = ['bmag',
                     'bsq',
                     'curl_b_1',
                     'curl_b_2',   
                     'curl_b_3',   
                     'curl_b_mag',
                     'B1',
                     'B2',
                     'B3',
                     'rho',
                     'coolfunc',
                     'uu',
                     'kinetic']

    # denominator : numerator   pairs : 
    #'bmag' : ('curl_b_1','curl_b_2','curl_b_3','curl_b_mag','B1','B2','B3'), 
    #    func_map = { 'vol': {'curl_b_mag'},   #all_funcnames ,
    func_map = { 'vol': {'curl_b_1','curl_b_2','curl_b_3','curl_b_abs1','curl_b_abs2','curl_b_abs3','curl_b_mag','rho','uu','coolfunc','bsq','bmag','B1','B2','B3'},
                 'bmag' : {'curl_b_mag'}, 
                 'bsq'  : {'rho','coolfunc','uu','kinetic','curl_b_mag'},
                 'rho'  : {'bsq','bmag','coolfunc','uu','kinetic','curl_b_mag'},
                 'energy' : {'kinetic'} ,
                 'sqrtrho' : {'curl_b_mag','sqrtenergy'} ,
                 'sqrtenergy' : {'curl_b_mag'} }

    title = 't =  ' + str('%7.0f' %h5file['t'][0])+time_unit_str

    print(" Plotting data....")
    ifunc = 0
    sf = '_int'

    for norm_func in norm_funcs :
        fname0 = norm_func + sf
        print("norm_func name = ", fname0)
        if( norm_func == 'energy' ) :
            f0tmp = h5file['rho'+sf][:] + h5file['uu'+sf][:]*gam + h5file['bsq'+sf][:]
        elif( norm_func == 'sqrtrho' ) :
            f0tmp = np.sqrt(h5file['rho'+sf][:])
        elif( norm_func == 'sqrtenergy' ) :
            f0tmp = np.sqrt(h5file['rho'+sf][:] + h5file['uu'+sf][:]*gam + h5file['bsq'+sf][:])
        else :
            f0tmp = h5file[fname0][:]
        f0 = viz_identify_symmetry_boundary(f0tmp)
        
        for func in func_map[norm_func] :

            fname1 = func + sf
            print("func name = ", fname1)
            f1tmp = h5file[fname1][:]
            f1 = viz_identify_symmetry_boundary(f1tmp)
            funcout_lin = f1 / np.abs(f0) 
            funcout = np.log10( np.abs(funcout_lin) )
            funcout_diff, funcout_reldiff = azimuthal_avg_rel(funcout_lin)
            funcout_reldiff_log = np.log10(np.abs(funcout_reldiff))
            
            figname = dirout+'intx2-'+func+'-over-'+norm_func+suffix

            aux_plot_int_curl(xx_slice,yy_slice,funcout,figname,
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2)

            aux_plot_int_curl(xx_slice,yy_slice,funcout_lin,figname+'-lin',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,dolog=False)

            aux_plot_int_curl(xx_slice,yy_slice,funcout_reldiff_log,figname+'-reldiff-log',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,vmax=0.8,vmin=-1.)

            aux_plot_int_curl(xx_slice,yy_slice,funcout_reldiff,figname+'-reldiff-lin',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,dolog=False,vmin=-2.5,vmax=4.5)

            aux_plot_int_curl(xx_slice,yy_slice,funcout_diff,figname+'-diff-lin',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,dolog=False)

            
            
    h5file.close()

    return


################################################################################
################################################################################
#
# plot_ratios_integrated_curl_b():
# ------------------
#
#   -- plots ratios of the data generated in integrate_curl_b()
#
#   -- needs the rmin, rmax combo used for the calculation to read in the
#      right part of the r grid.
################################################################################
def plot_ratios_integrated_curl_b(runname,itime,
                                  rmin=-1.e100,rmax=1.e100,
                                  cmap  = plt.cm.Spectral_r,
                                  nticks=10,
                                  time_unit_str='M',
                                  dump_name=None,
                                  dirout=None
                                  ) :
  
    pnames = ['dirname', 'dump_name', 'traj_file', 'gdump_name', 'header_file']
    sim_info = get_sim_info(pnames, runname=runname)

    if( dump_name is None ) :
        dump_name = sim_info['dump_name']

    if( dirout is None ) : 
        dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/curl/'
    
    timeid = '%06d' %itime
    curl_filename = dump_name + '.curl_int.' + timeid + '.h5'
    print("plot_ratios_curl_b(): working on run = ", runname, curl_filename)
    suffix = '-'+timeid

    # Get coordinates:
    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    gfile  = h5py.File(sim_info['gdump_name'], 'r')

    slice_rr = (slc_all,slice(0,1,1),slice(0,1,1))
    rr_orig = read_3d_hdf5_slice('x1',slice_rr,gfile)

    ind_matching,  = np.where( (rr_orig >= rmin) & (rr_orig <= rmax) )
    rr = rr_orig[np.min(ind_matching):np.max(ind_matching)+1]

    slice_ph = (slice(0,1,1),slice(0,1,1),slc_all)
    ph       = read_3d_hdf5_slice('x3',slice_ph,gfile)

    gfile.close()

    rr_2d = np.outer(rr,np.ones_like(ph))
    ph_2d = np.outer(np.ones_like(rr),ph)

    xx_slice_tmp = rr_2d * np.cos(ph_2d)
    yy_slice_tmp = rr_2d * np.sin(ph_2d)

    xx_slice = viz_identify_symmetry_boundary(xx_slice_tmp)
    yy_slice = viz_identify_symmetry_boundary(yy_slice_tmp)

    rcutout = np.min(rr)

    ###########################################################
    # Open curl file: 
    ###########################################################
    h5file = h5py.File(curl_filename,'r')
    
    
    # Get bbh positions:
    gfile  = h5py.File(sim_info['header_file'], 'r')
    m_bh1 = get_val('m_bh1',gfile)
    m_bh2 = get_val('m_bh2',gfile)
    rhor1 = m_bh1
    rhor2 = m_bh2
    gam = get_val('gam',gfile)
    gfile.close() 
    xbh1, ybh1, zbh1, xbh2, ybh2, zbh2 = get_bbh_pos(h5file['t'][0],runname=runname,spinning=False)


    ###########################################################

    title = 't =  ' + str('%7.0f' %h5file['t'][0])+time_unit_str

    print(" Plotting data....")
    sf = '_int'

    for index in ['1','2','3'] : 
        func = 'alfven_'+index+'_'
        f0tmp = np.sqrt(h5file['uu'+sf][:]*gam + h5file['rho'+sf][:] + h5file['bsq'+sf][:])
        f0 = viz_identify_symmetry_boundary(f0tmp)
        f1 = viz_identify_symmetry_boundary(h5file['B'+index+sf][:])
        funcout_lin = f1/f0
        funcout = np.log10(np.abs(funcout_lin))
        funcout_diff, funcout_reldiff = azimuthal_avg_rel(funcout_lin)
        funcout_reldiff_log = np.log10(np.abs(funcout_reldiff))
        figname = dirout+'intx2-'+func+suffix
        aux_plot_int_curl(xx_slice,yy_slice,funcout,figname,
                          title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2)
        aux_plot_int_curl(xx_slice,yy_slice,funcout_lin,figname+'-lin',
                          title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,dolog=False)
        aux_plot_int_curl(xx_slice,yy_slice,funcout_reldiff_log,figname+'-reldiff-log',
                          title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2)
        aux_plot_int_curl(xx_slice,yy_slice,funcout_reldiff,figname+'-reldiff-lin',
                          title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,dolog=False)
        aux_plot_int_curl(xx_slice,yy_slice,funcout_diff,figname+'-diff-lin',
                          title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,dolog=False)


            
    h5file.close()

    return


################################################################################
################################################################################
#
# plot_corr_integrated():
# ------------------
#
#   -- plots spatial correlations between functions in the integrated data sets;
################################################################################
def plot_corr_integrated(runname,itime,
                         rmin=-1.e100,rmax=1.e100,
                         cmap  = plt.cm.Spectral_r,
                         nticks=10,
                         time_unit_str='M',
                         dump_name=None,
                         dirout=None
                         ) :
  
    pnames = ['dirname', 'dump_name', 'traj_file', 'gdump_name', 'header_file']
    sim_info = get_sim_info(pnames, runname=runname)

    if( dump_name is None ) :
        dump_name = sim_info['dump_name']

    if( dirout is None ) : 
        dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/curl2/'
    
    timeid = '%06d' %itime
    curl_filename = dump_name + '.curl_int.' + timeid + '.h5'
    print("plot_corr_integrated(): working on run = ", runname, curl_filename)
    suffix = '-'+timeid

    # Get coordinates:
    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    gfile  = h5py.File(sim_info['gdump_name'], 'r')

    slice_rr = (slc_all,slice(0,1,1),slice(0,1,1))
    rr_orig = read_3d_hdf5_slice('x1',slice_rr,gfile)

    ind_matching,  = np.where( (rr_orig >= rmin) & (rr_orig <= rmax) )
    rr = rr_orig[np.min(ind_matching):np.max(ind_matching)+1]

    slice_ph = (slice(0,1,1),slice(0,1,1),slc_all)
    ph       = read_3d_hdf5_slice('x3',slice_ph,gfile)

    gfile.close()

    rr_2d = np.outer(rr,np.ones_like(ph))
    ph_2d = np.outer(np.ones_like(rr),ph)

    xx_slice_tmp = rr_2d * np.cos(ph_2d)
    yy_slice_tmp = rr_2d * np.sin(ph_2d)

    xx_slice = viz_identify_symmetry_boundary(xx_slice_tmp)
    yy_slice = viz_identify_symmetry_boundary(yy_slice_tmp)

    rcutout = np.min(rr)

    ###########################################################
    # Open curl file: 
    ###########################################################
    h5file = h5py.File(curl_filename,'r')
    
    
    # Get bbh positions:
    gfile  = h5py.File(sim_info['header_file'], 'r')
    m_bh1 = get_val('m_bh1',gfile)
    m_bh2 = get_val('m_bh2',gfile)
    rhor1 = m_bh1
    rhor2 = m_bh2
    gfile.close() 
    xbh1, ybh1, zbh1, xbh2, ybh2, zbh2 = get_bbh_pos(h5file['t'][0],runname=runname,spinning=False)

    ###########################################################

    norm_funcs = ['vol']

    #all_funcnames = ['bsq',
    #                 'bmag',
    #                 'curl_b_mag',
    #                 'curlb_sq_o_bsq',
    #                 'alfven_1',
    #                 'alfven_2',
    #                 'alfven_3',
    #                 'bsq_o_rho',
    #                 'beta',
    #                 'B1',
    #                 'B2',
    #                 'B3',
    #                 'rho',
    #                 'coolfunc',
    #                 'uu',
    #                 'kinetic']

    #all_funcnames = ['curlb_sq_o_bsq']
    all_funcnames = ['curl_b_mag']

    ### CURRENTLY INCOMPLETE:  NEED TO FINISH THE MAPPING: 
    corr_map = { 'bsq'      : {'coolfunc','rho','uu','kinetic' },
                 'bmag'     : {'B1','B2','B3','curl_b_mag'},
                 'alfven_1' : {'curl_b_mag','beta'}    ,
                 'curlb_sq_o_bsq': {'rho','uu','coolfunc'},
                 'curl_b_mag': {'rho','uu','coolfunc'}   }
    

    title = 't =  ' + str('%7.0f' %h5file['t'][0])+time_unit_str

    print(" Plotting data....")
    ifunc = 0

    vmin = -2.2
    vmax = 2.2

    for ifunc in np.arange(len(all_funcnames)) :
        func = all_funcnames[ifunc]
        if func == 'curlb_sq_o_bsq' :
            f1tmp = h5file['curl_b_mag_int'][:]
            f1tmp *= f1tmp
            f1tmp /= h5file['bsq_int'][:]
            fname1 = func 
        else :
            fname1 = func + '_int'
            f1tmp = h5file[fname1][:]
            
        f1 = viz_identify_symmetry_boundary(f1tmp)
        
        #for icorr in np.arange(ifunc+1,len(all_funcnames)) :
        for corr_func in corr_map[func] :
            fname2 = corr_func + '_int'
            print("func names = ", fname1,fname2)
            f2tmp = h5file[fname2][:]
            f2 = viz_identify_symmetry_boundary(f2tmp)
            
            f_corr = zero_norm_correlation(f1,f2)
            funcout_diff, funcout_reldiff = azimuthal_avg_rel(f_corr)
            funcout_reldiff_log = np.log10(np.abs(funcout_reldiff))
            
            figname = dirout+'intx2-'+'corr-'+func+'-'+corr_func+suffix
            
            #aux_plot_int_curl(xx_slice,yy_slice,f_corr,figname,
            #                  title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
            #                  vmin=-1.,vmax=np.log10(np.abs(vmax)))

            #aux_plot_int_curl(xx_slice,yy_slice,f_corr,figname+'-log2',
            #                  title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
            #                  vmin=-2.,vmax=2.)

            aux_plot_int_curl(xx_slice,yy_slice,f_corr,figname+'-lin',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
                              dolog=False,vmin=vmin,vmax=vmax)

            #aux_plot_int_curl(xx_slice,yy_slice,funcout_diff,figname+'-diff-lin',
            #                  title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
            #                  dolog=False,vmin=-1,vmax=1)
            

            
    h5file.close()

    return


################################################################################
################################################################################
#
# plot_corr2_integrated():
# ------------------
#
#   -- plots spatial correlations between functions in the integrated data sets;
################################################################################
def plot_corr2_integrated(runname,itime,
                          rmin=-1.e100,rmax=1.e100,
                          cmap  = plt.cm.Spectral_r,
                          nticks=10,
                          time_unit_str='M',
                          dump_name=None,
                          dirout=None
                          ) :
  
    pnames = ['dirname', 'dump_name', 'traj_file', 'gdump_name', 'header_file']
    sim_info = get_sim_info(pnames, runname=runname)

    if( dump_name is None ) :
        dump_name = sim_info['dump_name']
        
    if( dirout is None ) : 
        dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/curl/'
    
    timeid = '%06d' %itime
    curl_filename = sim_info['dump_name'] + '.curl_int.' + timeid + '.h5'
    print("plot_corr2_integrated(): working on run = ", runname, curl_filename)
    suffix = '-'+timeid

    # Get coordinates:
    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    gfile  = h5py.File(sim_info['gdump_name'], 'r')

    slice_rr = (slc_all,slice(0,1,1),slice(0,1,1))
    rr_orig = read_3d_hdf5_slice('x1',slice_rr,gfile)

    ind_matching,  = np.where( (rr_orig >= rmin) & (rr_orig <= rmax) )
    rr = rr_orig[np.min(ind_matching):np.max(ind_matching)+1]

    slice_ph = (slice(0,1,1),slice(0,1,1),slc_all)
    ph       = read_3d_hdf5_slice('x3',slice_ph,gfile)

    gfile.close()

    rr_2d = np.outer(rr,np.ones_like(ph))
    ph_2d = np.outer(np.ones_like(rr),ph)

    xx_slice_tmp = rr_2d * np.cos(ph_2d)
    yy_slice_tmp = rr_2d * np.sin(ph_2d)

    xx_slice = viz_identify_symmetry_boundary(xx_slice_tmp)
    yy_slice = viz_identify_symmetry_boundary(yy_slice_tmp)

    rcutout = np.min(rr)

    ###########################################################
    # Open curl file: 
    ###########################################################
    h5file = h5py.File(curl_filename,'r')
    
    
    # Get bbh positions:
    gfile  = h5py.File(sim_info['header_file'], 'r')
    m_bh1 = get_val('m_bh1',gfile)
    m_bh2 = get_val('m_bh2',gfile)
    rhor1 = m_bh1
    rhor2 = m_bh2
    gfile.close() 
    xbh1, ybh1, zbh1, xbh2, ybh2, zbh2 = get_bbh_pos(h5file['t'][0],runname=runname,spinning=False)

    ###########################################################

    norm_funcs = ['vol']

    #all_funcnames = ['bsq',
    #                 'bmag',
    #                 'curl_b_mag',
    #                 'curlb_sq_o_bsq',
    #                 'alfven_1',
    #                 'alfven_2',
    #                 'alfven_3',
    #                 'bsq_o_rho',
    #                 'beta',
    #                 'B1',
    #                 'B2',
    #                 'B3',
    #                 'rho',
    #                 'coolfunc',
    #                 'uu',
    #                 'kinetic']

    all_funcnames = ['curlb_sq_o_bsq']

    ### CURRENTLY INCOMPLETE:  NEED TO FINISH THE MAPPING: 
    corr_map = { 'bsq'      : {'coolfunc','rho','uu','kinetic' },
                 'bmag'     : {'B1','B2','B3','curl_b_mag'},
                 'alfven_1' : {'curl_b_mag','beta'}    ,
                 'curlb_sq_o_bsq': {'rho'}   }
    

    title = 't =  ' + str('%7.0f' %h5file['t'][0])+time_unit_str

    print(" Plotting data....")
    ifunc = 0

    vmin = -10.
    vmax = 10.

    for ifunc in np.arange(len(all_funcnames)) :
        func = all_funcnames[ifunc]
        if func == 'curlb_sq_o_bsq' :
            f1tmp = h5file['curl_b_mag_int'][:]
            f1tmp *= f1tmp
            f1tmp /= h5file['bsq_int'][:]
            fname1 = func 
        else :
            fname1 = func + '_int'
            f1tmp = h5file[fname1][:]
            
        f1 = viz_identify_symmetry_boundary(f1tmp)
        
        #for icorr in np.arange(ifunc+1,len(all_funcnames)) :
        for corr_func in corr_map[func] :
            fname2 = corr_func + '_int'
            print("func names = ", fname1,fname2)
            f2tmp = h5file[fname2][:]
            f2 = viz_identify_symmetry_boundary(f2tmp)
            
            f_corr = zero_norm_correlation(f1,f2)
            funcout_diff, funcout_reldiff = azimuthal_avg_rel(f_corr)
            funcout_reldiff_log = np.log10(np.abs(funcout_reldiff))
            
            figname = dirout+'intx2-'+'corr4-'+func+'-'+corr_func+suffix
            
            aux_plot_int_curl(xx_slice,yy_slice,f_corr,figname,
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
                              vmin=-1.,vmax=2)

            aux_plot_int_curl(xx_slice,yy_slice,f_corr,figname+'-lin',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
                              dolog=False,vmin=-1.,vmax=1.)

            aux_plot_int_curl(xx_slice,yy_slice,funcout_diff,figname+'-diff-lin',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
                              dolog=False,vmin=-3,vmax=3)
            
            aux_plot_int_curl(xx_slice,yy_slice,funcout_reldiff_log,figname+'-reldiff',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
                              dolog=False,vmin=-2,vmax=1)

            aux_plot_int_curl(xx_slice,yy_slice,funcout_reldiff,figname+'-reldiff-lin',
                              title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,rhor1,rhor2,
                              dolog=False,vmin=-3,vmax=3)
            

            
    h5file.close()

    return


################################################################################
################################################################################
def plot_mdot(runname,tbeg,tend) :

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/curl/'

    sim_info = get_sim_info(pnames, runname=runname)

    surf_filename=sim_info['surf_name']+'.all.h5'
    h5f = h5py.File(surf_filename,'r')
    r_out = h5f['rout'].value
    h5f.close()
    
    mdot_hist_b, t_hist = readhist_h5_all('/Bound/H_ddot' ,sim_info['hist_name'],tbeg=tbeg,tend=tend)
    mdot_hist_u, t_hist = readhist_h5_all('/Unbound/H_ddot' ,sim_info['hist_name'],tbeg=tbeg,tend=tend)
    mdot_hist = mdot_hist_u + mdot_hist_b
    mdot_avg = np.average(mdot_hist,axis=0)

    mdot_avg *= -1.

    print("times = ", t_hist)
    
    nwin = 0 
    figname = dirout+'mdot-special'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r'r [M]')
    ax.set_xlabel(r'Mdot')
    ax.set_xlim(10.,80.)
    ax.plot(r_out,mdot_avg)

    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')
    
    return

################################################################################
################################################################################
def make_all_mdot(runnames) : 

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit',
              'gdump_name', 'sigma_0', 'tbeg', 'tend',
              'rsep0','omega_bin_orbit','paper_name1', 'paper_name2']


    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        gfile = h5py.File(sim_info['gdump_name'],'r')
        r_out = gfile['x1'][:,0,0]
        dr_out = np.gradient(r_out)
        gfile.close()

        h5f = h5py.File(sim_info['dirname']+'paper/mdot.h5')

        mdot, t_out = readhist_h5_all_suffixes(['/Bound/','/Unbound/'],'H_ddot',sim_info['hist_name'])
        mdot *= -1.
        inv_dr = 1./dr_out
        inv_dr_new = np.outer(np.ones_like(t_out),inv_dr)
        mdot *= inv_dr_new
        dnew = h5f.create_dataset('mdot',data=mdot)
        dnew = h5f.create_dataset('r_out',data=r_out)
        dnew = h5f.create_dataset('dr_out',data=dr_out)
        dnew = h5f.create_dataset('t_out',data=t_out)
        dnew = h5f.create_dataset('00README',data="This is the accretion rate of all matter, bound and unbound")
        h5f.close()
    
    return

################################################################################
#
################################################################################

def aux_plot_timeseries(dicts,tend,binary_period,figname,ylabel,ftags=[''],yscale='linear',
                        ylims=None,xlims=None,xlabel=None,xlabel2=None,xformatter=None,ntags=None):

    nfuncs = len(dicts)
    if( ntags is None ) :
        ntags = len(ftags)
        
    xmajorFormatter, ymajorFormatter = set_many_curves_in_one_plot_style(nfuncs,ntags=ntags)

    #If you want to plot more than one set of curves for each dataset, then specify the string tags to add to "func"
    
    nwin = 0 
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax2 = ax.twiny()
    tscale = 1e4
    if( xlabel is None ) : 
        ax.set_xlabel(r'$t \ \ [10^4 M]$')
    else :
        ax.set_xlabel(xlabel)
        
    if( xlims ) :
        ax.set_xlim(xlims[0]/tscale,xlims[1]/tscale)
    else :
        ax.set_xlim(0., tend/tscale)
        
    ax.set_ylabel(ylabel)
    if( ylims ) : 
        ax.set_ylim(ylims[0],ylims[1])
    ax.yaxis.set_major_formatter(ymajorFormatter)
    if( xformatter is None ) : 
        ax.xaxis.set_major_formatter(xmajorFormatter)
    else :
        ax.xaxis.set_major_formatter(xformatter)
        
    ax.set_yscale(yscale)
    for ftag in ftags :
        for data_dict  in dicts :
            ax.plot(data_dict['t_out']/tscale, data_dict['func'+ftag],label=data_dict['label'+ftag])

    tscale = binary_period
    if( xlims ) :
        ax2.set_xlim(xlims[0]/tscale,xlims[1]/tscale)
    else :
        ax2.set_xlim(1e-10, tend/tscale)

    ax2.set_yscale(yscale)
    ax2.yaxis.set_major_formatter(ymajorFormatter)
    ax2.xaxis.set_major_formatter(xmajorFormatter)
    for ftag in ftags :
        for data_dict in dicts :
            ax2.plot(data_dict['t_out']/tscale, data_dict['func'+ftag])
    ax2.cla()
    if( xlabel2 is None ) : 
        ax2.set_xlabel(r'$t \ \ [t_\mathrm{bin}]$')
    else :
        ax2.set_xlabel(xlabel2)
        
    
    ax.legend(loc=0)
    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

################################################################################
#
################################################################################

def aux_plot_timeseries_split(dicts,tend,binary_period,figname,ylabels,ftags=[''],yscale='linear',
                              ylims=None,xlims=None,xlabel=None,xlabel2=None,xformatter=None,ntags=None,
                              vlines=None, hlines=None):

    nfuncs = len(dicts)
    if( ntags is None ) :
        ntags = len(ftags)
    #xmajorFormatter, ymajorFormatter = set_many_curves_in_one_plot_style2(nfuncs,ntags=ntags)
    xmajorFormatter, ymajorFormatter = set_linear_plots_in_row_plot_style4(nfuncs)


    #If you want to plot more than one set of curves for each dataset, then specify the string tags to add to "func"
    
    nwin = 0
    fig, axs = plt.subplots(nrows=2,ncols=nfuncs)

    # First row:
    for ax, data_dict, icol in zip(axs[0,:], dicts,np.arange(len(axs[0,:]))):
        ax2 = ax.twiny()
        tscale = 10.*binary_period
        if( xlabel is None ) : 
            ax2.set_xlabel(r'$t \ \ [10 t_\mathrm{bin}]$')
        else :
            ax2.set_xlabel(xlabel)

        ax2.set_autoscalex_on(False)
        ax2.set_autoscaley_on(False)
        ax2.set_yscale(yscale)
        if( xlims ) :
            ax2.set_xlim(xlims[0]/tscale,xlims[1]/tscale)
        else :
            ax2.set_xlim(0., tend/tscale)
        
        if( ylims ) : 
            ax2.set_ylim(ylims[0][0],ylims[0][1])

        if( icol == 0 ) : 
            ax2.yaxis.set_minor_locator(MultipleLocator(5))
            #ax2.yaxis.set_major_formatter(ymajorFormatter)
            ax.set_ylabel(ylabels[0])
        else :
            ax2.yaxis.set_major_formatter(NullFormatter())

        if( xformatter is None ) : 
            ax2.xaxis.set_major_formatter(xmajorFormatter)
        else :
            ax2.xaxis.set_major_formatter(xformatter)
        
        ax.xaxis.set_major_formatter(NullFormatter())

        for ftag in [ftags[0]] :
            ax2.plot(data_dict['t_out']/tscale, data_dict['func'+ftag],label=data_dict['label'+ftag])

        if( hlines is not None ) :
            for hval in hlines :
                if( hval is not None ) :
                    fout = np.full_like(data_dict['t_out'],hval)
                    ax2.axhline(hval,color='black', linestyle='--',alpha=0.5)

        if( vlines is not None ) :
            vval = vlines[icol]
            if( vval is not None ) :
                vval /= tscale
                ax2.axvline(vval,color='black', linestyle=':',alpha=0.5)

    # Second row: 
    for ax, data_dict, icol in zip(axs[1,:], dicts,np.arange(len(axs[1,:]))):
        tscale = 1e4

        ax.set_autoscalex_on(False)
        ax.set_autoscaley_on(False)
        if( xlabel is None ) : 
            ax.set_xlabel(r'$t \ \ [10^4 M]$')
        else :
            ax.set_xlabel(xlabel)
        if( xlims ) :
            ax.set_xlim(xlims[0]/tscale,xlims[1]/tscale)
        else :
            ax.set_xlim(0., tend/tscale)
        
        ax.set_yscale(yscale)
        if( ylims ) : 
            ax.set_ylim(ylims[1][0],ylims[1][1])

        if( icol == 0 ) : 
            ax.yaxis.set_minor_locator(MultipleLocator(5))
            #ax.yaxis.set_major_formatter(ymajorFormatter)
            ax.set_ylabel(ylabels[1])
        else :
            ax.yaxis.set_major_formatter(NullFormatter())
        
        if( xformatter is None ) : 
            ax.xaxis.set_major_formatter(xmajorFormatter)
        else :
            ax.xaxis.set_major_formatter(xformatter)

        for ftag in ftags[1:] :
            ax.plot(data_dict['t_out']/tscale, data_dict['func'+ftag],label=data_dict['label'+ftag])

        if( vlines is not None ) :
            vval = vlines[icol]
            if( vval is not None ) :
                vval /= tscale
                ax.axvline(vval,color='black', linestyle=':',alpha=0.5)

        if( icol == 0 ) : 
            ax.legend(loc=0)
        
    fig.subplots_adjust(wspace=0.025, hspace=0.02)
    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

################################################################################
#
################################################################################

def aux_plot_timeseries_four_inarow(dicts,tend,binary_period,figname,ylabel=None,ftags=[''],yscale='linear',
                                    tscales=None,xlabels=None,                                    
                                    ylims=None,xlims=None,xformatter=None,ntags=None,
                                    vlines=None, hlines=None,prune1=None,prune2=None):

    if( ntags is None ) :
        ntags = len(ftags)

    nruns = len(dicts)
    nfuncs = ntags
        
    #xmajorFormatter, ymajorFormatter = set_many_curves_in_one_plot_style2(nfuncs,ntags=ntags)
    xmajorFormatter, ymajorFormatter = set_linear_plots_in_row_plot_style4(ntags)


    if( xlabels is None ) :
        xlabels = [None,None]
        
    if( tscales is None ) :
        tscales = [None,None]

    if( ylabel is None ) :
        ylabel = 'f(t)' 
        

    #If you want to plot more than one set of curves for each dataset, then specify the string tags to add to "func"

    handles = []
    first_plot = True
    nwin = 0
    fig, axs = plt.subplots(nrows=1,ncols=nruns)

    # First row:
    for ax, data_dict, icol in zip(axs[:], dicts,np.arange(len(axs[:]))):
        ax2 = ax.twiny()

        if( tscales[0] is None ) : 
            tscale = 1e4
        else :
            tscale = tscales[0]

        ax.set_autoscalex_on(False)
        ax.set_autoscaley_on(False)
        if( xlabels[0] is None ) : 
            ax.set_xlabel(r'$t \ \ [10^4 M]$')
        else :
            ax.set_xlabel(xlabels[0])

        if( data_dict['xlims'] is not None ) :
            ax.set_xlim(data_dict['xlims'][0]/tscale,data_dict['xlims'][1]/tscale)
        else :
            if( xlims is not None ) :
                ax.set_xlim(xlims[0]/tscale,xlims[1]/tscale)
            else :
                ax.set_xlim(0., tend/tscale)
        
        ax.set_yscale(yscale)
        if( ylims ) : 
            ax.set_ylim(ylims[0],ylims[1])

        ax.yaxis.set_major_formatter(NullFormatter())
        #ax.xaxis.set_major_locator(MultipleLocator(5))
        ax.xaxis.set_major_locator(MaxNLocator(nbins=5,prune=prune1))
        if( xformatter is None ) : 
            ax.xaxis.set_major_formatter(xmajorFormatter)
        else :
            ax.xaxis.set_major_formatter(xformatter)

        for ftag in ftags :
            ptmp = ax.plot(data_dict['t_out']/tscale, data_dict['func'+ftag],label=data_dict['label'+ftag])
            if( first_plot ) : 
                handles = np.append(handles,ptmp)

            
        # if( vlines is not None ) :
        #     vval = vlines[icol]
        #     if( vval is not None ) :
        #         vval /= tscale
        #         ax.axvline(vval,color='black', linestyle=':',alpha=0.5)

        # Top part 
        if( tscales[1] is None ) : 
            tscale = 10.*binary_period
        else :
            tscale = tscales[1]
            
        if( xlabels[1] is None ) :
            ax2.set_xlabel(r'$t \ \ [10 t_\mathrm{bin}]$')
        else :
            ax2.set_xlabel(xlabels[1])

        ax2.set_autoscalex_on(False)
        ax2.set_autoscaley_on(False)
        ax2.set_yscale(yscale)
        if( data_dict['xlims'] is not None ) :
            ax2.set_xlim(data_dict['xlims'][0]/tscale,data_dict['xlims'][1]/tscale)
        else :
            if( xlims is not None ) :
                ax2.set_xlim(xlims[0]/tscale,xlims[1]/tscale)
            else :
                ax2.set_xlim(0., tend/tscale)
        
        if( ylims ) : 
            ax2.set_ylim(ylims[0],ylims[1])

        ax2.yaxis.set_major_formatter(NullFormatter())
        #ax2.xaxis.set_major_locator(MultipleLocator(5))
        ax2.xaxis.set_major_locator(MaxNLocator(nbins=5,prune=prune2))
        
        if( xformatter is None ) : 
            ax2.xaxis.set_major_formatter(xmajorFormatter)
        else :
            ax2.xaxis.set_major_formatter(xformatter)
        
        for ftag in ftags : 
            ax2.plot(data_dict['t_out']/tscale, data_dict['func'+ftag],label=data_dict['label'+ftag])

        if( hlines is not None ) :
            hval = hlines[icol]
            for val in hval : 
                ax2.axhline(val,color='black', linestyle='--',alpha=0.5)

        if( vlines is not None ) :
            vval = vlines[icol]
            for val in vval : 
                val /= tscale
                ax2.axvline(val,color='black', linestyle=':',alpha=0.5)

    labels  = []
    for ftag in ftags :
        labels = np.append(labels,data_dict['label'+ftag])

        
    fig.legend(handles,labels,loc='upper center',borderaxespad=0.1,ncol=nfuncs)

    #axs[0].yaxis.set_major_locator(MultipleLocator(5))
    axs[0].yaxis.set_major_formatter(ymajorFormatter)
    axs[0].set_ylabel(ylabel)
        
    fig.subplots_adjust(wspace=0.075,top=0.85)
    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

################################################################################
#
################################################################################

def aux_plot_fft_series(dicts,binary_omega,figname,ylabel,ftags=[''],yscale='linear',
                        ylims=None,xlims=None,xlabel=None,xlabel2=None,xformatter=None,ntags=None):

    nfuncs = len(dicts)
    if( ntags is None ) :
        ntags = len(ftags)
        
    xmajorFormatter, ymajorFormatter = set_many_curves_in_one_plot_style(nfuncs,ntags=ntags)

    #If you want to plot more than one set of curves for each dataset, then specify the string tags to add to "func"
    
    nwin = 0 
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    tscale = 1e-4
    if( xlabel is None ) : 
        ax.set_xlabel(r'$\omega \ \ [10^{-4} M^{-1}]$')
    else :
        ax.set_xlabel(xlabel)
        
    if( xlims ) :
        ax.set_xlim(xlims[0],xlims[1])
        
    ax.set_ylabel(ylabel)
    if( ylims ) : 
        ax.set_ylim(ylims[0],ylims[1])
    ax.yaxis.set_major_formatter(ymajorFormatter)
    if( xformatter is None ) : 
        ax.xaxis.set_major_formatter(xmajorFormatter)
    else :
        ax.xaxis.set_major_formatter(xformatter)
        
    ax.set_yscale(yscale)
    for ftag in ftags :
        for data_dict  in dicts :
            ax.plot(data_dict['t_out']/tscale, data_dict['func'+ftag],label=data_dict['label'+ftag])

    tscale = binary_omega
    if( xlims ) :
        ax2.set_xlim(xlims[0]/tscale,xlims[1]/tscale)
    else :
        ax2.set_xlim(1e-10, tend/tscale)

    ax2.set_yscale(yscale)
    ax2.yaxis.set_major_formatter(ymajorFormatter)
    ax2.xaxis.set_major_formatter(xmajorFormatter)
    for ftag in ftags :
        for data_dict in dicts :
            ax2.plot(data_dict['t_out']/tscale, data_dict['func'+ftag])
    ax2.cla()
    if( xlabel2 is None ) : 
        ax2.set_xlabel(r'$\omega \ \ [\Omega_\mathrm{bin}]$')
    else :
        ax2.set_xlabel(xlabel2)
        
    
    ax.legend(loc=0)
    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

################################################################################
################################################################################

def aux_plot_vs_radius(dicts,asep,figname,ylabel,ylims=None,ftags=['']):

    nfuncs = len(dicts)
    xmajorFormatter, ymajorFormatter = set_many_curves_in_one_plot_style(nfuncs,ntags=len(ftags))

    nwin = 0 
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_xlabel(r'$r \ \ [a]$')
    ax.set_ylabel(ylabel)
    ax.set_xlim(0.75,5.)
    if( ylims is not None ) :
        ax.set_ylim(ylims[0],ylims[1])
    ax.yaxis.set_major_formatter(ymajorFormatter)
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.xaxis.set_minor_locator(MultipleLocator(5))
    plt.locator_params(axis='x', nbins=5)
    for ftag in ftags : 
        for data_dict  in dicts : 
            ax.plot(data_dict['r_out']/asep, data_dict['func'+ftag],label=data_dict['label'+ftag])

    # for data_dict  in dicts : 
    #     ax.plot(data_dict['r_out']/asep, data_dict['func2'],linestyle='--')
        
    ax.legend(loc=0)
    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

################################################################################
################################################################################

def aux_plot_vs_radius_four_inarow(dicts,asep,figname,ylabel,ylims=None,ftags=[''],
                                   yscale='linear',dicts2=None,plotstyle=None,
                                   use_horizontal_legend=False):

    nfuncs = len(ftags)
    nplots = len(dicts)
    print("nfuncs nplots = ", nfuncs, nplots)
    
    if( plotstyle is None ): 
        xmajorFormatter, ymajorFormatter = set_linear_plots_in_row_plot_style(nfuncs)
    else :
        if( dicts2 is None ) : 
            xmajorFormatter, ymajorFormatter = set_linear_plots_in_row_plot_style2(nfuncs)
        else :
            xmajorFormatter, ymajorFormatter = set_linear_plots_in_row_plot_style3(nfuncs)
            

    if( dicts2 is None ) :
        dicts2 = len(dicts)*[None]

    handles = [] 
    first_plot = True

    nwin = 0
    fig, axs = plt.subplots(nrows=1,ncols=nplots)
    
    for ax, data_dict, data_dict2 in zip(axs.flatten(), dicts, dicts2) :
        ax.set_xlabel(r'$r \ \ [a]$')
        ax.set_autoscalex_on(False)
        ax.set_xlim(0.75,5.)
        if( ylims is not None ) :
            ax.set_autoscaley_on(False)
            ax.set_ylim(ylims[0],ylims[1])
        ax.xaxis.set_major_formatter(xmajorFormatter)
        ax.xaxis.set_major_locator(MultipleLocator(1))
        ax.yaxis.set_major_formatter(NullFormatter())
        for ftag in ftags :
            ptmp = ax.plot(data_dict['r_out']/asep, data_dict['func'+ftag],label=data_dict['label'+ftag])
            if( first_plot ) : 
                handles = np.append(handles,ptmp)
        if( data_dict2 is not None ) : 
            for ftag in ftags :
                #ax.plot(data_dict2['r_out']/asep, data_dict2['func'+ftag],label=None,linestyle='--')
                ax.plot(data_dict2['r_out']/asep, data_dict2['func'+ftag],label=None,ls='--')

        if( not use_horizontal_legend ): 
            ax.legend(loc=0)

        first_plot = False
        

    labels  = []
    for ftag in ftags :
        labels = np.append(labels,data_dict['label'+ftag])
            
    if( use_horizontal_legend ): 
        #axs[-1].legend(loc=2,borderaxespad=0.,bbox_to_anchor=(1.05,1.))
        #axs[-1].legend(loc=3,borderaxespad=0.,bbox_to_anchor=(0.5-nplots,1.05),ncol=nfuncs)
        #fig.legend(handles,labels,loc='lower center',borderaxespad=0.,bbox_to_anchor=(0.25,1.05),ncol=nfuncs)
        #fig.legend(handles,labels,loc='lower center',borderaxespad=0.,ncol=nfuncs)
        fig.legend(handles,labels,loc='upper center',borderaxespad=0.,ncol=nfuncs)
        
    axs[0].yaxis.set_major_formatter(ymajorFormatter)
    axs[0].set_ylabel(ylabel)
    fig.subplots_adjust(wspace=0.075, hspace=0.)
    
    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

################################################################################
################################################################################

def aux_plot_snapshot_grid(dicts,asep,figname,ftags=None,xlims=None,ylims=None,func_lims=None,nlev=256,nticks=10,
                           use_contourf=False,cmap='hawley',extend=True,show_cbar=True,xlabel=None,ylabel=None,
                           label_color='white'):

    if( cmap == 'hawley' ) :
        cmap = get_hawley_cmap()

    green_led = '#5DFC0A'

    if( ftags is None ) :
        ftags = ['']
        
    ncols = len(ftags)
    nrows = len(dicts)

    xmajorFormatter, ymajorFormatter = set_contour_plots_in_grid_style(nrows,ncols)

    if( func_lims is not None ) :
        fmin, fmax = func_lims
    else :
        fmin = 1e100
        fmax = -1e100

    nwin = 0 
    fig, axs = plt.subplots(nrows=nrows,ncols=ncols)
    print("shape of axs = ", np.shape(axs))

    for irow in np.arange(nrows) : 
        data_dict = dicts[irow]
        for icol in np.arange(ncols) :
            ftag = ftags[icol]
            ax = axs[irow,icol] 
            ax.set_aspect('equal')

            func_out = data_dict['func'+ftag][:]
            if( func_lims is  None ) :
                fmin = np.nanmin([fmin,np.min(func_out)])
                fmax = np.nanmax([fmax,np.max(func_out)])
                func_lims = [fmin,fmax]

            levels = np.linspace(fmin,fmax,nlev)
            rcutout = np.min(data_dict['r_out'][:]/asep)
            if( 'th_out' in data_dict.keys() ) : 
                r_out  = np.outer(data_dict['r_out'][:]/asep,np.ones_like(data_dict['th_out'][:]))
                th_out = np.outer(np.ones_like(data_dict['r_out'][:]),data_dict['th_out'][:])
                xx_slice = r_out*np.sin(th_out)
                yy_slice = r_out*np.cos(th_out)
                r_out = 0
                th_out = 0
            else : 
                r_out  = np.outer(data_dict['r_out'][:]/asep,np.ones_like(data_dict['ph_out'+ftag][:]))
                ph_out = np.outer(np.ones_like(data_dict['r_out'][:]),data_dict['ph_out'+ftag][:])
                xx = r_out*np.cos(ph_out)
                yy = r_out*np.sin(ph_out)
                xx_slice = viz_identify_symmetry_boundary(xx)
                yy_slice = viz_identify_symmetry_boundary(yy)
                xx = 0
                yy = 0
                r_out = 0
                ph_out = 0
                
            if( xlims is None ) :
                xmin = np.min(xx_slice)
                xmax = np.max(xx_slice)
            else :
                xmin, xmax = xlims

            if( ylims is None ) :
                ymin = np.min(yy_slice)
                ymax = np.max(yy_slice)
            else :
                ymin, ymax = ylims
                
            ax.set_xlim(xmin,xmax)
            ax.set_ylim(ymin,ymax)

            CS = ax.pcolormesh(xx_slice, yy_slice,func_out,vmin=fmin,vmax=fmax,cmap=cmap)

            cutout= plt.Circle((0,0),rcutout,color='white')
            ax.add_artist(cutout)
            if( 'bhloc' in data_dict.keys() ) : 
                bhloc = data_dict['bhloc'+ftag]
                bh1 = plt.Circle((bhloc[0]/asep,bhloc[1]/asep), data_dict['rhor1']/asep, color='black')
                bh2 = plt.Circle((bhloc[3]/asep,bhloc[4]/asep), data_dict['rhor2']/asep, color='black')
                ax.add_artist(bh1)
                ax.add_artist(bh2)

            ax.text(0.95,0.85,data_dict['frame_name'+ftag],transform=ax.transAxes,horizontalalignment='right',
                    fontdict={'size':10, 'color':label_color})


    #for ax,data_dict in zip(axs[:,0].flatten(),dicts) : 
    #    if( ylabel is not None ) :
    #        ax.set_ylabel(ylabel)
    #    else:
    #        ax.set_ylabel(data_dict['run_name'])

    for ax in axs.flatten() :
        ax.yaxis.set_major_formatter(NullFormatter())
        
    for ax in axs[0::2,0].flatten() :
        ax.yaxis.set_major_formatter(ymajorFormatter)
        ax.yaxis.set_major_locator(MultipleLocator(2))

    for ax in axs[-1,:].flatten() :  
        if( xlabel is not None ) :
            ax.set_xlabel(xlabel)

    for ax in axs.flatten() :
        ax.xaxis.set_major_formatter(NullFormatter())

    for ax in axs[-1,0::2].flatten() :  
        ax.xaxis.set_major_formatter(xmajorFormatter)
        ax.xaxis.set_major_locator(MultipleLocator(2))

    
    #fig.subplots_adjust(wspace=0.075, hspace=0.075)
    fig.subplots_adjust(wspace=0.01, hspace=0.0)
    
    # ticks for the colorbar
    if( show_cbar ):
        #cbar = fig.colorbar(CS, ax=axs[-1])
        cbar = fig.colorbar(CS, ax=axs.ravel().tolist())
        cbar.set_ticks(np.linspace(fmin,fmax,6))

    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

    


################################################################################
################################################################################

def aux_plot_spacetime_contour(dicts,asep,figname,ylims=None,func_lims=None,nlev=256,nticks=10,
                               use_contourf=False,cmap='hawley',extend=True,show_cbar=True):

    if( cmap == 'hawley' ) :
        cmap = get_hawley_cmap()
    
    nfuncs = len(dicts)
    xmajorFormatter, ymajorFormatter = set_contour_plots_in_row_plot_style()

    nwin = 0 
    fig, axs = plt.subplots(nrows=1,ncols=nfuncs)

    for ax, data_dict in zip(axs.flatten(), dicts) :
        ax.set_xlabel(r'$r \ \ [a]$')
        ax.set_autoscalex_on(False)
        ax.set_xlim(0.75,5.)
        if( ylims is not None ) :
            ax.set_autoscaley_on(False)
            ax.set_ylim(ylims[0],ylims[1])
        ax.xaxis.set_major_formatter(xmajorFormatter)
        ax.xaxis.set_major_locator(MultipleLocator(1))
        ax.yaxis.set_major_formatter(NullFormatter())
        #plt.locator_params(axis='x', nbins=5)

        if( func_lims is not None ) :
            fmin = func_lims[0]
            fmax = func_lims[1] 
        else :
            fmin = np.min(data_dict['func'])
            fmax = np.max(data_dict['func'])
        levels = np.linspace(fmin,fmax,nlev)

        xx = data_dict['r_out'][:]/asep[0]
        yy = data_dict['t_out'][:]/1e4
        func_out = data_dict['func'][:]
        
        if( use_contourf )  :
            xx_slice = np.outer(xx,np.ones_like(yy))
            yy_slice = np.outer(np.ones_like(xx),yy)
            CS = ax.contourf(xx_slice,yy_slice,func_out,levels, cmap=cmap, extend=extend)
        else :
            #xx_out = make_pcolormesh_coordinates(xx)
            #yy_out = make_pcolormesh_coordinates(yy)
            # xx_slice = np.outer(xx,np.ones_like(yy))
            # yy_slice = np.outer(np.ones_like(xx),yy)
            # print("xx yy f shapes = ", np.shape(xx_slice), np.shape(yy_slice), np.shape(func_out))
            CS = ax.pcolormesh(xx,yy,func_out,vmin=np.min(levels),vmax=np.max(levels),cmap=cmap)

    axs[0].yaxis.set_major_formatter(ymajorFormatter)
    axs[0].set_ylabel(r'$t \ \ [10^4 M]$')

    fig.subplots_adjust(wspace=0.075, hspace=0.)
    # ticks for the colorbar
    if( show_cbar ):
        #cbar = fig.colorbar(CS, ax=axs[-1])
        cbar = fig.colorbar(CS, ax=axs.ravel().tolist())
        cbar.set_ticks(np.linspace(fmin,fmax,6))

    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

################################################################################
################################################################################
def plot_all_mdot(runnames,series=1) :

    pnames = ['dirname',  'freq_bin_orbit', 'tend','paper_name1', 'paper_name2','hist_freq','rsep0']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
        if( runname == 'longer2' ) :
            binary_period = 1./sim_info['freq_bin_orbit']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])
            h5f = h5py.File(sim_info['dirname']+'paper/mdot.h5','r')
            new_dict = dict([])
            tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
            t_out = h5f['t_out'][:]
            r_out = h5f['r_out'][:]
            iend = find_array_index(tend_out,t_out)
            ir_out = find_array_index(sim_info['rsep0'],r_out)
            new_dict['func'] = np.abs(h5f['mdot'][0:iend+1,ir_out]) + 2e-4
            new_dict['t_out'  ] = t_out[0:iend+1]
            new_dict['label'] = paper_name 
            h5f.close()
            dicts = np.append(dicts,new_dict)

    figname = dirout+'mdot-at-asep'+tag
    ylabel = r'$\dot{M}(t)$'
    aux_plot_timeseries(dicts,tend,binary_period,figname,ylabel)
    
    return

################################################################################
################################################################################
def plot_all_mdot_vs_r(runnames,series=1) :

    pnames = ['dirname', 'freq_bin_orbit', 'tend','paper_name1',
              'paper_name2', 'rsep0', 'gdump_name','hist_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tbeg = 4e4
    sim_info = get_sim_info(['tend','hist_freq'], runname='longer2')
    tend = (int(sim_info['tend'] / sim_info['hist_freq']) - 1) * sim_info['hist_freq']
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
        if( runname == 'longer2' ) :
            asep = sim_info['rsep0']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            gfile = h5py.File(sim_info['gdump_name'],'r')
            r_out = gfile['x1'][:,0,0]
            gfile.close()
            
            h5f = h5py.File(sim_info['dirname']+'paper/mdot.h5','r')
            new_dict = dict([])
            t_out = h5f['t_out'][:]
            itbeg, itend = find_bracket(tbeg,tend, t_out)
            new_dict['func'] = np.mean(h5f['mdot'][itbeg:itend+1,:], axis=0)
            tend_out = (int(sim_info['tend'] / sim_info['hist_freq']) - 1) * sim_info['hist_freq']
            itbeg, itend = find_bracket((sim_info['tend']-3e4),tend_out, t_out)
            new_dict['func2'] = np.mean(h5f['mdot'][itbeg:itend+1,:], axis=0)
            new_dict['r_out'] = r_out
            new_dict['label'] = paper_name
            new_dict['label2'] = None
            h5f.close()
            dicts = np.append(dicts,new_dict)

    figname = dirout+'mdot-vs-r'+tag
    ylabel = r'$\overline{\dot{M}(r,t)}$'
    aux_plot_vs_radius(dicts,asep,figname,ylabel,ylims=[0.,0.025],ftags=['','2'])
    
    return

################################################################################
################################################################################
#  plot_all_ecc():
# -----------------
#  taken from plot_all_mdot() in this file and plot_ecc_timeseries()
#  in disk_ecc.py
################################################################################
def plot_all_ecc(runnames,series=1) :

    pnames = ['dirname',  'freq_bin_orbit', 'tend','paper_name1', 'paper_name2', 'surf_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    ftags=['','2']
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
        if( runname == 'longer2' ) :
            binary_period = 1./sim_info['freq_bin_orbit']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])
            h5f = h5py.File(sim_info['dirname']+'modes/ecc_analysis2.h5','r')
            ecc_mag          = h5f['ecc-lump'            ].value[:]
            smooth_period = binary_period
            n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
            sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
            ftmp = np.reshape(ecc_mag,(len(ecc_mag),1))            
            ecc_mag_smooth = np.reshape(signal.convolve2d(ftmp,sm_window,mode='same',boundary='symm'),(len(ecc_mag)))
            funcout = np.log10(np.abs(ecc_mag_smooth))
            funcout2 = np.log10(np.abs(ecc_mag))
            
            new_dict = dict([])
            new_dict['func'] = funcout
            new_dict['func2'] = funcout2
            new_dict['t_out'  ] = h5f['tout'][:]
            new_dict['label'] = paper_name
            new_dict['label2'] = None
            h5f.close()
            dicts = np.append(dicts,new_dict)


    ylabel = r'$\log_{10} \ \overline{e_\mathrm{lump}}(t, \Delta t = t_\mathrm{bin})$'
    figname = dirout+'ecc-lump-smooth'+tag
    aux_plot_timeseries(dicts,tend,binary_period,figname,ylabel,ylims=[-4.,-1.],ftags=[''])
    
    ylabel = r'$\log_{10} \ e_\mathrm{lump}(t)$'
    figname = dirout+'ecc-lump-raw'+tag
    aux_plot_timeseries(dicts,tend,binary_period,figname,ylabel,ylims=[-4.,-1.],ftags=['2'])
    
    return

################################################################################
################################################################################
#  plot_all_ecc2():
# -----------------
#  -- close up version of plot_all_ecc()  of the last 3000 of evolution; 
################################################################################
def plot_all_ecc2(runnames,series=1) :

    pnames = ['dirname',  'freq_bin_orbit', 'tend','paper_name1', 'paper_name2', 'surf_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    ftags=['','2']
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
        if( runname == 'longer2' ) :
            binary_period = 1./sim_info['freq_bin_orbit']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])
            h5f = h5py.File(sim_info['dirname']+'modes/ecc_analysis2.h5','r')
            ecc_mag          = h5f['ecc-lump'            ].value[:]
            smooth_period = binary_period
            n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
            sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
            ftmp = np.reshape(ecc_mag,(len(ecc_mag),1))            
            ecc_mag_smooth = np.reshape(signal.convolve2d(ftmp,sm_window,mode='same',boundary='symm'),(len(ecc_mag)))
            funcout = np.log10(np.abs(ecc_mag_smooth))
            funcout2 = np.log10(np.abs(ecc_mag))
            
            new_dict = dict([])
            new_dict['func'] = funcout
            new_dict['func2'] = funcout2
            new_dict['t_out'  ] = h5f['tout'][:] - sim_info['tend'] 
            new_dict['label'] = paper_name
            new_dict['label2'] = paper_name
            print("tend ", paper_name, " = ", sim_info['tend'])
            if( runname is 'medium_disk' ) :
                print("tout = ", new_dict['t_out'])
                
            h5f.close()
            dicts = np.append(dicts,new_dict)

    xformatter = FormatStrFormatter('%2.1f')
    tminus = 10.*binary_period
    xlims = [-tminus,0.]
    xlabel2 = r'$t - t_\mathrm{end}  \ \ [t_\mathrm{bin}]$'
    xlabel = r'$t - t_\mathrm{end}  \ \ [10^4 M]$'
    
    ylabel = r'$\log_{10} \ \overline{e_\mathrm{lump}}(t, \Delta t = t_\mathrm{bin})$'
    figname = dirout+'ecc-lump2-smooth'+tag
    aux_plot_timeseries(dicts,tend,binary_period,figname,ylabel,ylims=[-4.,-1.],ftags=[''],xlims=xlims,xlabel=xlabel,xlabel2=xlabel2,xformatter=xformatter)
    
    ylabel = r'$\log_{10} \ e_\mathrm{lump}(t)$'
    figname = dirout+'ecc-lump2-raw'+tag
    aux_plot_timeseries(dicts,tend,binary_period,figname,ylabel,ylims=[-4.,-1.],ftags=['2'],xlims=xlims,xlabel=xlabel,xlabel2=xlabel2,xformatter=xformatter)
    
    return

################################################################################
################################################################################
def plot_all_ecc_vs_r(runnames,series=1) :

    pnames = ['dirname', 'freq_bin_orbit', 'tend','paper_name1',
              'paper_name2', 'rsep0', 'gdump_name','hist_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tbeg = 4e4
    sim_info = get_sim_info(['tend','hist_freq'], runname='longer2')
    tend = (int(sim_info['tend'] / sim_info['hist_freq']) - 1) * sim_info['hist_freq']
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
        if( runname == 'longer2' ) :
            asep = sim_info['rsep0']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            gfile = h5py.File(sim_info['gdump_name'],'r')
            r_out = gfile['x1'][:,0,0]
            gfile.close()
            
            h5f = h5py.File(sim_info['dirname']+'modes/ecc_analysis2.h5','r')
            t_out = h5f['tout'][:]
            h5f.close()
            
            h5f = h5py.File(sim_info['dirname']+'modes/ecc_analysis.h5','r')
            new_dict = dict([])
            tbeg_tmp = tbeg
            if( runname == 'inject_disk' ):
                tbeg_tmp = np.min(t_out)
                
            itbeg, itend = find_bracket(tbeg_tmp,tend, t_out)
            new_dict['func'] = np.log10(np.nanmean(h5f['ecc-surf-spacetime'][itbeg:itend+1,:], axis=0))
            tend_out = (int(sim_info['tend'] / sim_info['hist_freq']) - 1) * sim_info['hist_freq']
            itbeg, itend = find_bracket((sim_info['tend']-3e4),tend_out, t_out)
            new_dict['func2'] = np.log10(np.nanmean(h5f['ecc-surf-spacetime'][itbeg:itend+1,:], axis=0))
            new_dict['r_out'] = r_out
            new_dict['label'] = paper_name
            new_dict['label2'] = None
            h5f.close()
            dicts = np.append(dicts,new_dict)

    figname = dirout+'ecc-vs-r'+tag
    ylabel = r'$\log_{10} \ \overline{e(r,t)}$'
    aux_plot_vs_radius(dicts,asep,figname,ylabel,ylims=[-3,0.],ftags=['','2'])
    
    return

################################################################################
################################################################################
def plot_maxwell(runname,tbeg,tend,nticks=10, cmap='hawley'):


    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit', 'gdump_name', 't_lump_beg', 't_lump_end']


    if( cmap == 'hawley' ) :
        cmap = get_hawley_cmap()
    
    sim_info = get_sim_info(pnames, runname=runname)
    homedir = expanduser("~")
    dirout = homedir+'/tex/mass-ratio-paper/'+runname+'/'

    h5f = h5py.File(sim_info['gdump_name'],'r')
    r_out = h5f['x1'].value[:,0,0]
    h5f.close()

    t_lump = sim_info['t_lump_beg']
    if( t_lump is not None ) :
        t_lump *= 1e-4

    maxwell, t_hist = readhist_h5_all('/Bound/H_Txzd' ,sim_info['hist_name'],tbeg=tbeg,tend=tend)
    surfdens, t_hist = readhist_h5_all('/Bound/H_rhoav' ,sim_info['hist_name'],tbeg=tbeg,tend=tend)

    dr0 = np.gradient(r_out)
    drout = np.outer(np.ones_like(t_hist),dr0)
    rc = np.outer(np.ones_like(t_hist),r_out)

    maxwell  *= 1.39/rc
    
    # maxwell stress and surdens should be divided by the annular
    # area, but we don't need it because it'll divide out:

    # maxwell  /= (2.*np.pi*rc*drout)
    # surfdens /= (2.*np.pi*rc*drout)

    print("times = ", t_hist)

    print(" Plotting data....")
    t_hist *= 1e-4
    r_min = 0.75
    r_max = 5.
    r_out /= 20.
    func = maxwell / surfdens
    func_log = np.log10( np.abs(func))
    minf=-6.
    maxf=-1.
    nwin=0
    figname = dirout+'maxwell_o_surfdens-log'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    #ax.set_yscale('log')
    ax.set_xlim(r_min,r_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_hist,func_log,vmin=minf,vmax=maxf,cmap=cmap)
    if( t_lump is not None ) :
        ax.plot([r_min,r_max],[t_lump,t_lump],color='green',alpha=0.5,lw=2)
    fig.colorbar(CP,ticks=rticks,format='%1.4g')
    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')
    
    nwin=0
    minf=-2e-3
    maxf= 2e-3
    figname = dirout+'maxwell_o_surfdens-lin'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r't [$10^4$ M] ')
    ax.set_xlabel(r'r [a] ')
    #ax.set_yscale('log')
    ax.set_xlim(r_min,r_max)
    rticks = np.linspace(minf,maxf,nticks)
    CP = ax.pcolormesh(r_out, t_hist,func,vmin=minf,vmax=maxf,cmap=cmap)
    if( t_lump is not None ) :
        ax.plot([r_min,r_max],[t_lump,t_lump],color='green',alpha=0.5,lw=2)
    fig.colorbar(CP,ticks=rticks,format='%1.4g')
    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')
    
    return

################################################################################
################################################################################
def plot_lightcurve(runname,tbeg,tend) :

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit', 'hist_name', 'sigma_0', 'tend', 'rsep0','omega_bin_orbit','gdump_name']


    sim_info = get_sim_info(pnames, runname=runname)
    dirout = sim_info['dirname']+'plots/lightcurve/'

    os.mkdir(dirout)

    h5f = h5py.File(sim_info['gdump_name'],'r')
    r_out = h5f['x1'].value[:,0,0]
    h5f.close()
    
    lum_hist_b, t_hist = readhist_h5_all('/Bound/H_Lut' ,sim_info['hist_name'],tbeg=tbeg,tend=tend)
    lum_hist = lum_hist_b
    lum_avg = np.average(lum_hist,axis=0)

    print("times = ", t_hist)

    t_hist /= 1e4
    
    nwin = 0 
    figname = dirout+'lum'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r'Luminosity [$G M\ \Sigma_0 c^{-2}$]')
    ax.set_xlabel(r't [$10^4 M$]')
    ax.set_xlim(10.,80.)
    ax.plot(r_out,mdot_avg)

    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')

    return

################################################################################
################################################################################
def make_all_lightcurves(runnames) : 

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit',
              'gdump_name', 'sigma_0', 'tbeg', 'tend',
              'rsep0','omega_bin_orbit','paper_name1', 'paper_name2']


    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        h5f = h5py.File(sim_info['dirname']+'paper/lightcurve.h5')

        lum_all, t_out = readhist_h5_all_suffixes(['/Bound/'],'H_Lut',sim_info['hist_name'])
        lum = np.sum(lum_all,axis=1)
        dnew = h5f.create_dataset('lum',data=lum)
        dnew = h5f.create_dataset('t_out',data=t_out)
        dnew = h5f.create_dataset('00README',data="This is the integrated dL/dx1  over x1 (or integrated in radius) of all BOUND matter, which is what we use in the papers as the light curve of the simulation.")
        h5f.close()
    
    return

################################################################################
################################################################################
def plot_all_lightcurves(runnames,series=1) :

    pnames = ['dirname',  'freq_bin_orbit', 'tend','paper_name1', 'paper_name2',  'sigma_0']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
        if( runname == 'longer2' ) :
            binary_period = 1./sim_info['freq_bin_orbit']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])
            h5f = h5py.File(sim_info['dirname']+'paper/lightcurve.h5','r')
            new_dict = dict([])
            new_dict['func'] = -h5f['lum'][:] / sim_info['sigma_0']
            new_dict['t_out'  ] = h5f['t_out'][:]
            new_dict['label'] = paper_name 
            h5f.close()
            dicts = np.append(dicts,new_dict)

    figname = dirout+'lum'+tag
    ylabel = r'Luminosity [$G M\ \Sigma_0 c^{-2}$]'
    aux_plot_timeseries(dicts,tend,binary_period,figname,ylabel)
    
    return

################################################################################
# Mass enclosed datasets:
################################################################################
def make_all_massenc(runnames) : 

    pnames = ['dirname', 'hist_name', 'surf_name', 'freq_bin_orbit',
              'gdump_name', 'sigma_0', 'tbeg', 'tend',
              'rsep0','omega_bin_orbit','paper_name1', 'paper_name2']


    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        h5f = h5py.File(sim_info['dirname']+'paper/massenc.h5')
        mass, t_out = readhist_h5_all_suffixes(['/Bound/','/Unbound/'],'H_rhoav',sim_info['hist_name'])
        massenc = np.cumsum(mass,axis=1)
        dnew = h5f.create_dataset('massenc',data=massenc)
        dnew = h5f.create_dataset('t_out',data=t_out)
        dnew = h5f.create_dataset('00README',data="This is the cumulative sum of rest-mass density (rhoav)  over x1 (or integrated in radius) of ALL (bound+unbound) matter.")
        h5f.close()
    
    return

################################################################################
################################################################################
def plot_all_massenc(runnames,series=1) :

    pnames = ['dirname',  'freq_bin_orbit', 'tend','paper_name1', 'paper_name2',  'sigma_0', 'gdump_name','hist_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    rsamples = np.array([1.5, 2., 3., 4.])*20.
    ftags = []
    for iset in np.arange(len(rsamples)) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags) 
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
        if( runname == 'longer2' ) :
            binary_period = 1./sim_info['freq_bin_orbit']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            gfile = h5py.File(sim_info['gdump_name'],'r')
            r_out = gfile['x1'][:,0,0]
            gfile.close()

            isamples = []
            for rtry in rsamples :
                isamples = np.append(isamples,int(find_array_index(rtry,r_out)))

            isamples = isamples.astype('int')

            tend = np.max([tend,sim_info['tend']])
            h5f = h5py.File(sim_info['dirname']+'paper/massenc.h5','r')
            new_dict = dict([])

            tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
            t_out = h5f['t_out'][:]
            iend = find_array_index(tend_out,t_out)

            for ifunc in np.arange(len(rsamples)) : 
                new_dict['func'+ftags[ifunc]] = np.log10(h5f['massenc'][0:iend+1,isamples[ifunc]] / sim_info['sigma_0'])
                new_dict['label'+ftags[ifunc]] = None

            print("sigma0 = ", sim_info['sigma_0'])
            new_dict['label'] = paper_name 
                
            new_dict['t_out'  ] = t_out[0:iend+1]
            h5f.close()
            dicts = np.append(dicts,new_dict)

    figname = dirout+'mass-enclosed'+tag
    ylabel = r'$\log_{10} M(<r , t ) \left[\left(G M\right)^2 \Sigma_0 c^{-4}\right]$'
    aux_plot_timeseries(dicts,tend,binary_period,figname,ylabel,ftags=ftags,ylims=[0.,4.2],ntags=1)
    
    return

################################################################################
################################################################################
def make_all_spacetime_surfdens(runnames) :

    pnames = ['dirname',  'freq_bin_orbit', 'tend','paper_name1', 'paper_name2',  'sigma_0', 'gdump_name', 'hist_name']

    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
            
        gfile = h5py.File(sim_info['gdump_name'],'r')
        r_out = gfile['x1'][:,0,0]
        fshape = gfile['x1'].shape
        dphi = gfile['Header/dx3'][0]
        g_ph_ph = np.squeeze(gfile['gcov333'][:,int(fshape[1]/2),:])
        gfile.close()

        rho, t_out = readhist_h5_all_suffixes(['/Bound/','/Unbound/'],'H_rhoav',sim_info['hist_name'])

        dr_out = np.gradient(r_out)

        if( USE_NEW_VERSION ) : 
            # version "surfdens2.h5" 
            f0 = 1./(sim_info['sigma_0'] * dr_out * np.sum(np.sqrt(g_ph_ph)*dphi,axis=-1) )
            
        else : 
            # version "surfdens.h5" : 
            f0 = 1./(2.*np.pi * sim_info['sigma_0'] * r_out * dr_out)
        
        f1 = np.outer(np.ones_like(t_out),f0)
        func_out = f1 * rho
            
        if( USE_NEW_VERSION ) : 
            h5f = h5py.File(sim_info['dirname']+'paper/surfdens2.h5')
            dnew = h5f.create_dataset('g_ph_ph',data=g_ph_ph)
            dnew = h5f.create_dataset('00README',data="This is the surface density, or rhoav from surface file divided by dr*Sigma0*dphi*sqrt(g_ph_ph) of ALL (bound+unbound) matter.")                                            
        else :
            h5f = h5py.File(sim_info['dirname']+'paper/surfdens.h5')
            dnew = h5f.create_dataset('00README',data="This is the surface density, or rhoav from surface file divided by 2*pi*dr*r*Sigma0 of ALL (bound+unbound) matter.")                    
            
            
        dnew = h5f.create_dataset('surfdens',data=func_out)
        dnew = h5f.create_dataset('t_out',data=t_out)
        dnew = h5f.create_dataset('r_out',data=r_out)
        dnew = h5f.create_dataset('dr_out',data=dr_out)
        h5f.close()

    
    return

################################################################################
################################################################################
def plot_all_spacetime_surfdens(runnames,series=1) :

    pnames = ['dirname',  'tend','paper_name1', 'paper_name2', 'rsep0','hist_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    if( USE_NEW_VERSION ) :
        filename = 'surfdens2.h5'
    else :
        filename = 'surfdens.h5'

    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        if( runname == 'longer2' ) :
            asep = sim_info['rsep0']

        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])
            h5f = h5py.File(sim_info['dirname']+'paper/'+filename,'r')
            new_dict = dict([])
            new_dict['r_out'] = h5f['r_out'][:]
            tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
            t_out = h5f['t_out'][:]
            iend = find_array_index(tend_out,t_out)
            new_dict['t_out'] = t_out[0:iend+1]
            new_dict['func' ] = np.log10(np.abs(h5f['surfdens'][0:iend+1,:]))
            new_dict['label'] = paper_name 
            dicts = np.append(dicts,new_dict)
            h5f.close()

    figname = dirout+'surfdens-spacetime'+tag

    aux_plot_spacetime_contour(dicts,asep,figname,ylims=[0.,tend/1e4],func_lims=[-2.5,0.])
    
    return

################################################################################
################################################################################
def make_all_spacetime_beta(runnames) :

    pnames = ['dirname',  'freq_bin_orbit', 'tend','paper_name1', 'paper_name2',  'sigma_0', 'gdump_name', 'hist_name']

    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
            
        gfile = h5py.File(sim_info['gdump_name'],'r')
        r_out = gfile['x1'][:,0,0]
        fshape = gfile['x1'].shape
        gfile.close()

        pgas, t_out = readhist_h5_all_suffixes(['/Bound/'],'H_pavg',sim_info['hist_name'])
        pmag, t_out = readhist_h5_all_suffixes(['/Bound/'],'H_bsq',sim_info['hist_name'])
        pmag *= 0.5
        
        func_out = pgas / pmag
        
        h5f = h5py.File(sim_info['dirname']+'paper/beta.h5')
        dnew = h5f.create_dataset('beta',data=func_out)
        dnew = h5f.create_dataset('t_out',data=t_out)
        dnew = h5f.create_dataset('r_out',data=r_out)
        dnew = h5f.create_dataset('00README',data="This is beta, ratio of pgas to pmag using history data, of only bound matter.")
        h5f.close()

    
    return

################################################################################
################################################################################
def make_all_spacetime_stresses(runnames) :

    pnames = ['dirname',  'freq_bin_orbit', 'tend','paper_name1', 'paper_name2',  'sigma_0', 'gdump_name', 'hist_name','hist_freq']

    states = ['/Bound/','/Unbound/']

    # Functions stored in the history file: 
    hist_funcs = [ 'Lup',
                   'dTdr_2',
                   'dTdr_3',
                   'dTdr_4',
                   'dTdr_5',
                   'dTdr_6',
                   'Txza'  ,  #T^r_p Hydro
                   'Txzd'  ,  #T^r_p EM
                   'hUx'   ,
                   'lrho'  ,
                   'rhoav' ]

    # Functions to be taken derivatives of : 
    diffed_funcs = [ 'advect','reynolds','Txzd']
    del_funcs = [ ('del_'+x) for x in diffed_funcs ]
    
    # Functions we calculate from the history data : 
    aux_funcs = np.append(del_funcs, ['advect', 'reynolds','dTdr_tot','dTdr_m','dTdr_h','dJ_dtdr'] )
    all_funcs = np.append(hist_funcs, aux_funcs)
    all_funcs = np.append(all_funcs,['t_out','r_out','tbeg1','tend1','tbeg2','tend2'])
                                       
    tbeg = 4e4
    sim_info = get_sim_info(['tend','hist_freq'], runname='longer2')
    tend = (int(sim_info['tend'] / sim_info['hist_freq']) - 1) * sim_info['hist_freq']
    
    for runname in runnames :
        ndict = dict([])
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
            
        gfile = h5py.File(sim_info['gdump_name'],'r')
        r_out = gfile['x1'][:,0,0]
        gfile.close()
        
        dr_out = np.gradient(r_out)

        for func in hist_funcs : 
            ndict[func], t_out = readhist_h5_all_suffixes(     states,'H_'+func,sim_info['hist_name'])

        ndict['Lup' ], t_out = readhist_h5_all_suffixes(['/Bound/'],'H_Lup'   ,sim_info['hist_name'])

        # Need to remove the implicit "dr" in the definition of our
        # spherical shell integral:
        dr_st = np.outer(np.ones_like(t_out),dr_out)
        for func in hist_funcs : 
            ndict[func] /= dr_st
        

        tend2 = (int(sim_info['tend'] / sim_info['hist_freq']) - 1) * sim_info['hist_freq']
        tbeg2 = tend2 - 3.e4
        ibeg1, iend1 = find_bracket(tbeg ,tend ,t_out)
        ibeg2, iend2 = find_bracket(tbeg2,tend2,t_out)
        
        ndict['t_out'] = t_out
        ndict['r_out'] = r_out
        ndict['tbeg1'] = [tbeg]
        ndict['tend1'] = tend
        ndict['tbeg2'] = tbeg2
        ndict['tend2'] = tend2
        
        ndict['dTdr_h'] = ndict['dTdr_2'] + ndict['dTdr_4']
        ndict['dTdr_m'] = ndict['dTdr_3'] + ndict['dTdr_5'] + ndict['dTdr_6']
        ndict['dTdr_tot'] = ndict['dTdr_h'] + ndict['dTdr_m']
        
        ndict['advect'] = ndict['hUx'] * ndict['lrho'] / ndict['rhoav']
        ndict['reynolds'] = ndict['Txza'] - ndict['advect']

        for delfunc, func in zip( del_funcs, diffed_funcs ):
            ndict[delfunc] = np.gradient(ndict[func],axis=1) / dr_st

        ndict['dJ_dtdr'] = ndict['dTdr_tot'] - ndict['Lup'] - ndict['del_reynolds'] - ndict['del_advect'] - ndict['del_Txzd']
        
        avg_funcs = ['hUx','lrho','rhoav','advect','del_advect','del_reynolds','del_Txzd','Lup','dTdr_tot','dJ_dtdr']
        for func in avg_funcs :
            ndict[func+'_tavg1'] = np.nanmean(ndict[func][ibeg1:iend1+1,:],axis=0)
            ndict[func+'_tavg2'] = np.nanmean(ndict[func][ibeg2:iend2+1,:],axis=0)

        for tavg in ['_tavg1','_tavg2'] :
            ndict['advect'    +tavg+'_special'] = ndict['hUx'+tavg]*ndict['lrho'+tavg]/ndict['rhoav'+tavg]
            ndict['del_advect'+tavg+'_special'] = np.gradient(ndict['advect'+tavg+'_special']) / dr_out 
            ndict['dJ_dtdr'   +tavg+'_special'] = ndict['dTdr_tot'+tavg] - ndict['Lup'+tavg] - ndict['del_reynolds'+tavg] - ndict['del_advect'+tavg+'_special'] - ndict['del_Txzd'+tavg]

        h5f = h5py.File(sim_info['dirname']+'paper/stresses.h5')

        for func in all_funcs :
            dnew = h5f.create_dataset(func,data=ndict[func])
            
        for func in avg_funcs :
            for tavg in ['_tavg1','_tavg2'] :
                fout = func+tavg 
                dnew = h5f.create_dataset(fout,data=ndict[fout])
                
        for func in ['advect','del_advect','dJ_dtdr'] : 
            for tavg in ['_tavg1_special','_tavg2_special'] :
                fout = func+tavg 
                dnew = h5f.create_dataset(fout,data=ndict[fout])
                
        dnew = h5f.create_dataset('00README',data="These are various functions related to the different kinds of r-phi stresses  of both bound and unbound material.")
        h5f.close()

    
    return


################################################################################
################################################################################
def plot_all_spacetime_stresses(runnames,series=1) :

    pnames = ['dirname',  'tend','paper_name1', 'paper_name2', 'rsep0', 'sigma_0','hist_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'


    filename = 'stresses.h5'

    funcs = ['dJ_dtdr', 'dTdr_tot','Lup','del_reynolds','del_advect', 'del_Txzd']
    neg_funcs = ['Lup','del_reynolds','del_advect', 'del_Txzd']
    

    for func in funcs : 
        dicts = []
        tend = -100.
        for runname in runnames :
            print("Processing run = ", runname )
            sim_info = get_sim_info(pnames, runname=runname)

            if( runname == 'longer2' ) :
                asep = sim_info['rsep0']

            if( series == 1 ) :
                paper_name = sim_info['paper_name1']
                tag='-mass-ratio'
            else :
                paper_name = sim_info['paper_name2']
                tag='-init-data'

            if( paper_name is not None ) :
                tend = np.max([tend,sim_info['tend']])
                h5f = h5py.File(sim_info['dirname']+'paper/'+filename,'r')
                new_dict = dict([])
                new_dict['r_out'] = h5f['r_out'][:]
                new_dict['label'] = paper_name

                tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
                t_out = h5f['t_out'][:]
                iend = find_array_index(tend_out,t_out)
                new_dict['t_out'] = t_out[0:iend+1]

                factor = 1./sim_info['sigma_0']
                if( func in neg_funcs ) :
                    factor = -1./sim_info['sigma_0']

                new_dict['func'] = h5f[func][0:iend+1,:] * factor 
                dicts = np.append(dicts,new_dict)
                h5f.close()

                
        figname = dirout+func+'-spacetime'+tag
        cscale = 1e-1
        aux_plot_spacetime_contour(dicts,asep,figname,ylims=[0.,tend/1e4],func_lims=[-cscale,cscale])

    
    return

################################################################################
################################################################################
def plot_all_stress_avgs(runnames,series=1) :

    pnames = ['dirname',  'tend','paper_name1', 'paper_name2', 'rsep0', 'sigma_0','hist_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    dicts2 = []

    filename = 'stresses.h5'

    nfuncs = 6
    ftags = []
    for iset in np.arange(nfuncs) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags)

    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        if( runname == 'longer2' ) :
            asep = sim_info['rsep0']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            h5f = h5py.File(sim_info['dirname']+'paper/'+filename,'r')
            new_dict  = dict([])
            new_dict2 = dict([])
            r_out = h5f['r_out'][:]
            dr_out = np.gradient(r_out)
            t_out = h5f['t_out'][:]

            for tavg,ndict in zip(['_tavg1','_tavg2'],[new_dict,new_dict2]) :
                i = 0 
                ndict['func'+ftags[i]] = h5f['dJ_dtdr'+tavg+'_special'][:]
                ndict['label'+ftags[i]] = r'$d^2 J / dt dr$'
                i += 1 
                ndict['func'+ftags[i]] = -h5f['del_advect'+tavg+'_special'][:]
                ndict['label'+ftags[i]] = r'${A^r}_\phi$'
                i += 1 
                ndict['func'+ftags[i]] = -h5f['del_reynolds'+tavg][:]
                ndict['label'+ftags[i]] = r'${R^r}_\phi$'
                i += 1 
                ndict['func'+ftags[i]] = h5f['dTdr_tot'+tavg][:]
                ndict['label'+ftags[i]] = r'$dT / dr$'
                i += 1 
                ndict['func'+ftags[i]] = -h5f['del_Txzd'+tavg][:]
                ndict['label'+ftags[i]] = r'${M^r}_\phi$'
                i += 1
                ndict['func'+ftags[i]] = -h5f['Lup'+tavg][:]
                ndict['label'+ftags[i]] = r'$\mathcal{F}_\phi$'
                i += 1 

                for ftag in ftags :
                    ndict['func'+ftag] /= sim_info['sigma_0']


            new_dict[ 'r_out'] = h5f['r_out'][:]
            new_dict2['r_out'] = h5f['r_out'][:]

            dicts  = np.append(dicts ,new_dict )
            dicts2 = np.append(dicts2,new_dict2)
            h5f.close()

        
    ylims=[-0.05,0.05]
    ylabel=r'$d^2J/dtd(r/a) \ \ [ G M a \Sigma_0 ]$'

    figname = dirout+'dJdr-avgs'+tag
    aux_plot_vs_radius_four_inarow(dicts, asep, figname, ylabel,
                                   ylims=ylims,
                                   ftags=ftags,
                                   plotstyle='other',use_horizontal_legend=True)
    
    figname = dirout+'dJdr-avgs2'+tag
    aux_plot_vs_radius_four_inarow(dicts2, asep, figname, ylabel,
                                   ylims=ylims,
                                   ftags=ftags,
                                   plotstyle='other',use_horizontal_legend=True)
    
    return


################################################################################
################################################################################
def plot_all_spacetime_beta(runnames,series=1) :

    pnames = ['dirname',  'tend','paper_name1', 'paper_name2', 'rsep0','hist_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    filename = 'beta.h5'

    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        if( runname == 'longer2' ) :
            asep = sim_info['rsep0']

        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])
            h5f = h5py.File(sim_info['dirname']+'paper/'+filename,'r')
            new_dict = dict([])
            new_dict['r_out'] = h5f['r_out'][:]

            tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
            t_out = h5f['t_out'][:]
            iend = find_array_index(tend_out,t_out)
            new_dict['t_out'] = t_out[0:iend+1]
            new_dict['func' ] = np.log10(np.abs(h5f['beta'][0:iend+1,:]))
            
            new_dict['label'] = paper_name 
            dicts = np.append(dicts,new_dict)
            h5f.close()

    figname = dirout+'beta-spacetime'+tag

    aux_plot_spacetime_contour(dicts,asep,figname,ylims=[0.,tend/1e4],func_lims=[-1,1.])
    
    return

################################################################################
################################################################################
def plot_all_slices_surfdens(runnames,series=1) :

    pnames = ['dirname', 'tend','paper_name1', 'paper_name2',  'rsep0', 'hist_freq']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []

    dtavg = 2.e3
    sim_info = get_sim_info(['tend','hist_freq'], runname='longer2')
    tend_out = int(sim_info['tend'] / sim_info['hist_freq']) * sim_info['hist_freq']
    print("tend_out = ", tend_out)
    tsamples0  = np.linspace(4.5e4,(tend_out-0.5*dtavg-1.),5)
    ftags = []
    for iset in np.arange(len(tsamples0)+2) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags)

    if( USE_NEW_VERSION ) :
        filename = 'surfdens2.h5'
    else :
        filename = 'surfdens.h5'
    
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        if( runname == 'longer2' ) :
            asep = sim_info['rsep0']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            h5f = h5py.File(sim_info['dirname']+'paper/'+filename,'r')
            new_dict = dict([])
            t_out = h5f['t_out'][:]

            tend_out = int(sim_info['tend'] / sim_info['hist_freq']) * sim_info['hist_freq']
            tsamples = np.append(tsamples0,[tend_out-0.5*dtavg-1.])
            tsamples = np.append([t_out[0]],tsamples)
            print("tsamples = ", tsamples)
            first_time = True
            for t0,ftag in zip(tsamples,ftags) :
                new_dict['label'+ftag] = str('%1.1f' %(int(t0)/1e4)) 
                if( first_time ) :
                    new_dict['func'+ftag] = h5f['surfdens'][0,:]
                    first_time = False 
                else : 
                    tbeg = t0 - 0.5*dtavg
                    tend = t0 + 0.5*dtavg
                    ibeg, iend = find_bracket(tbeg,tend,t_out)
                    new_dict['func'+ftag] = np.nanmean(h5f['surfdens'][ibeg:iend,:],axis=0)
                    
                
            new_dict['r_out'] = h5f['r_out'][:]
            dicts = np.append(dicts,new_dict)
            h5f.close()

    figname = dirout+'surfdens-slices'+tag

    ylabel=r'$\Sigma \ \ [ \Sigma_0 ]$'
    aux_plot_vs_radius_four_inarow(dicts,asep,figname,ylabel,ylims=[0.,1.0],ftags=ftags)
    
    return

################################################################################
################################################################################
def make_all_snapshots_surfdens(runnames) :

    pnames = ['dirname', 'tend', 'rsep0', 'surf_name', 'surf_freq', 'gdump_name', 'sigma_0','header_file']

    sim_info = get_sim_info(['tend','rsep0','surf_freq'], runname='longer2')
    asep = sim_info['rsep0']
    tend_out = int(sim_info['tend'] / sim_info['surf_freq']) * sim_info['surf_freq']
    tsamples0  = np.linspace(4.e4,tend_out,3)
    rmax = 10.*asep

    ftags = []
    for iset in np.arange(len(tsamples0)+1) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags) 
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        gfile = h5py.File(sim_info['gdump_name'],'r')
        r_tmp  = gfile['x1'][:,0,0]
        ph_out = gfile['x3'][0,0,:]
        fshape = gfile['x1'].shape
        dphi = gfile['Header/dx3'][0]
        ind_r = find_array_index(rmax,r_tmp)
        ind_r += 1 
        r_out = r_tmp[0:ind_r]
        g_ph_ph = np.squeeze(gfile['gcov333'][0:ind_r,int(fshape[1]/2),:])
        gfile.close()

        hfile = h5py.File(sim_info['header_file'],'r')
        m_bh1 = get_val('m_bh1',hfile)
        m_bh2 = get_val('m_bh2',hfile)
        hfile.close()
        rhor1 = m_bh1
        rhor2 = m_bh2

        dr_out = np.gradient(r_out)

        if( USE_NEW_VERSION ) : 
            # version "surfdens2.h5" 
            f0 = 1./(dphi * sim_info['sigma_0'] * dr_out )
        else : 
            # version "surfdens.h5" : 
            f0 = 1./(2.*np.pi * sim_info['sigma_0'] * r_out * dr_out)

        f1 = np.outer(f0,np.ones_like(ph_out))

        if( USE_NEW_VERSION ) :
            f1 /= np.sqrt(g_ph_ph)
            
        f0 = 0 

        tend_out = int(sim_info['tend'] / sim_info['surf_freq']) * sim_info['surf_freq']
        tsamples = np.append(tsamples0,tend_out)
        
        if( USE_NEW_VERSION ) :
            h5fout = h5py.File(sim_info['dirname']+'paper/surfdens_snapshots2.h5')
            dnew = h5fout.create_dataset('g_ph_ph',data=g_ph_ph)
            dnew = h5fout.create_dataset('00README',data="This is the surface density, or rhoav from surface file divided by dr*Sigma0*dphi*sqrt(g_ph_ph) of ALL (bound+unbound) matter.")                                
        else :
            h5fout = h5py.File(sim_info['dirname']+'paper/surfdens_snapshots.h5')
            dnew = h5fout.create_dataset('00README',data="This is the surface density, or rhoav from surface file divided by 2*pi*dr*r*Sigma0 of ALL (bound+unbound) matter.")                    
            
        dnew = h5fout.create_dataset('rhor1',data=rhor1)
        dnew = h5fout.create_dataset('rhor2',data=rhor2)
        dnew = h5fout.create_dataset('r_out',data=r_out)
        dnew = h5fout.create_dataset('dr_out',data=dr_out)
        dnew = h5fout.create_dataset('tsamples',data=tsamples)
        
        for t0,ftag in zip(tsamples,ftags) :
            print("t0 = ", t0)
            # We are using the individual surface files rather than
            # the merged one so that we can make use of the OS links
            # for times that "injected-flux" run has to the "longer2"
            # files.

            # do not want to use nearest index because it won't work
            # for the last time as "tend" will usually exceed the time
            # of the last surface file
            surf_suf = '.%06d.h5' %(int( (t0 / sim_info['surf_freq'])))  
            print("opening ", sim_info['surf_name']+surf_suf)
            h5fin = h5py.File(sim_info['surf_name']+surf_suf,'r')
            bhloc = get_bbh_pos(t0,runname=runname,spinning=False)
            #get trajectory data, calculate phase and i_ph shift amount, perform shift, calculate new positions BHs
            dnew = h5fout.create_dataset('bhloc_orig'+ftag,data=bhloc)
            bhloc_new = np.copy(bhloc)
            ph, bhloc_new  = corotate_phi(ph_out,bhloc)
                
            dnew = h5fout.create_dataset('ph_out'+ftag, data=ph)
            dnew = h5fout.create_dataset('bhloc' +ftag, data=bhloc_new)
            dnew = h5fout.create_dataset('label' +ftag, data=str('%1.1g' %(int(t0)/1e4)))
            dnew = h5fout.create_dataset( 'time' +ftag, data=t0)
            f_b = h5fin['Bound/S_rhoav'][0:ind_r,:]
            f_u = h5fin['Unbound/S_rhoav'][0:ind_r,:]
            ftmp = (f_b + f_u) * f1
            dnew = h5fout.create_dataset('func'+ftag, data=ftmp)
                    
        
        h5fin.close()
        h5fout.close()
            
    
    return

################################################################################
################################################################################
def plot_all_snapshots_surfdens(runnames,series=1) :

    pnames = ['dirname', 'tend','paper_name1', 'paper_name2',  'rsep0', 'surf_name', 'gdump_name', 'sigma_0']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    sim_info = get_sim_info(['rsep0'], runname='longer2')
    asep = sim_info['rsep0']
    
    dicts = []
    ntimes = 4
    ftags = []
    for iset in np.arange(ntimes) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags)

    if( USE_NEW_VERSION ) :
        filename = 'surfdens_snapshots2.h5'
    else :
        filename = 'surfdens_snapshots.h5'

        
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            new_dict = dict([])
            new_dict['run_name'] = paper_name
            
            h5f = h5py.File(sim_info['dirname']+'paper/'+filename,'r')
            for dset in ['r_out','rhor1','rhor2']:
                new_dict[dset] = h5f[dset][:]
            
            for ftag in ftags :
                for dset in ['label' ,'time'] :
                    new_dict[dset+ftag] = h5f[dset+ftag]
                for dset in ['ph_out','bhloc','func'] :
                    new_dict[dset+ftag] = h5f[dset+ftag][:]
                new_dict['frame_name'+ftag] = str('%1.1f' %(int(h5f['time'+ftag].value)/1e4))
                
            h5f.close()
            dicts = np.append(dicts,new_dict)

            
    figname = dirout+'surfdens-snapshots'+tag
    rlims = [-4.,4.]
    aux_plot_snapshot_grid(dicts,asep,figname,ftags=ftags,func_lims=[0.,1.5],xlims=rlims,ylims=rlims)
    
    return

################################################################################
################################################################################
def make_all_aziavgs_beta(runnames) :

    pnames = ['dirname', 'tend', 'rsep0', 'dump_name', 'dump_freq', 'gdump_name', 'sigma_0','header_file']

    sim_info = get_sim_info(['tend','rsep0','dump_freq'], runname='longer2')
    asep = sim_info['rsep0']
    tend_out = int(sim_info['tend'] / sim_info['dump_freq']) * sim_info['dump_freq']
    tsamples0  = np.linspace(4.e4,tend_out,3)
    rmax = 10.*asep

    ftags = []
    for iset in np.arange(len(tsamples0)+1) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags) 
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        gfile = h5py.File(sim_info['gdump_name'],'r')
        th_out = gfile['x2'][0,:,0]
        r_tmp  = gfile['x1'][:,0,0]
        fshape = gfile['x1'].shape
        ind_r = find_array_index(rmax,r_tmp)
        ind_r += 1 
        r_out = r_tmp[0:ind_r]
        gfile.close()

        tend_out = int(sim_info['tend'] / sim_info['dump_freq']) * sim_info['dump_freq']
        tsamples = np.append(tsamples0,tend_out)
        isamples = tsamples / sim_info['dump_freq']
        isamples = isamples.astype(int)
        
        h5fout = h5py.File(sim_info['dirname']+'paper/beta_aziavgs.h5')
        dnew = h5fout.create_dataset('r_out',data=r_out)
        dnew = h5fout.create_dataset('th_out',data=th_out)
        dnew = h5fout.create_dataset('tsamples',data=tsamples)
        dnew = h5fout.create_dataset('isamples',data=isamples)
        dnew = h5fout.create_dataset('00README',data="This is beta, the ratio of pgas to pmag, using ALL (bound+unbound)  material, averaged azimuthally.")        
        for t0,ind_t,ftag in zip(tsamples,isamples,ftags) :
            print("t0 = ", t0)
            filename = sim_info['dump_name'] + str('.%06d' %ind_t) + '.h5'
            print("opening ", filename)
            h5fin = h5py.File(filename,'r')

            beta = h5fin['uu'][0:ind_r,:,:] / h5fin['bsq'][0:ind_r,:,:]
            beta_avg = np.nanmean(beta, axis=2)

            gam = get_val('gam',h5fin)
            beta_avg *= 2.*(gam-1.)

            dnew = h5fout.create_dataset('func'+ftag, data=beta_avg)
            dnew = h5fout.create_dataset('label' +ftag, data=str('%1.1g' %(int(t0)/1e4)))
            dnew = h5fout.create_dataset( 'time' +ftag, data=t0)
            h5fin.close()
            
        h5fout.close()
            
    
    return

################################################################################
################################################################################
def make_all_aziavgs_beta2(runnames) :

    pnames = ['dirname', 'tend', 'rsep0', 'dump_name', 'dump_freq', 'gdump_name', 'sigma_0','header_file']

    sim_info = get_sim_info(['tend','rsep0','dump_freq'], runname='longer2')
    asep = sim_info['rsep0']
    tend_out = int(sim_info['tend'] / sim_info['dump_freq']) * sim_info['dump_freq']
    tsamples0  = np.linspace(4.e4,tend_out,3)
    rmax = 10.*asep

    ftags = []
    for iset in np.arange(len(tsamples0)+1) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags) 
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        gfile = h5py.File(sim_info['gdump_name'],'r')
        th_out = gfile['x2'][0,:,0]
        r_tmp  = gfile['x1'][:,0,0]
        fshape = gfile['x1'].shape
        ind_r = find_array_index(rmax,r_tmp)
        ind_r += 1 
        r_out = r_tmp[0:ind_r]
        gfile.close()

        tend_out = int(sim_info['tend'] / sim_info['dump_freq']) * sim_info['dump_freq']
        tsamples = np.append(tsamples0,tend_out)
        isamples = tsamples / sim_info['dump_freq']
        isamples = isamples.astype(int)
        
        h5fout = h5py.File(sim_info['dirname']+'paper/beta_aziavgs2.h5')
        dnew = h5fout.create_dataset('r_out',data=r_out)
        dnew = h5fout.create_dataset('th_out',data=th_out)
        dnew = h5fout.create_dataset('tsamples',data=tsamples)
        dnew = h5fout.create_dataset('isamples',data=isamples)
        dnew = h5fout.create_dataset('00README',data="This is beta, the ratio of pgas to pmag, using ALL (bound+unbound)  material, averaged azimuthally.")        
        for t0,ind_t,ftag in zip(tsamples,isamples,ftags) :
            print("t0 = ", t0)
            filename = sim_info['dump_name'] + str('.%06d' %ind_t) + '.h5'
            print("opening ", filename)
            h5fin = h5py.File(filename,'r')

            beta = np.nanmean(h5fin['uu'][0:ind_r,:,:],axis=2) / np.nanmean(h5fin['bsq'][0:ind_r,:,:],axis=2)

            gam = get_val('gam',h5fin)
            beta *= 2.*(gam-1.)

            dnew = h5fout.create_dataset('func'+ftag, data=beta)
            dnew = h5fout.create_dataset('label' +ftag, data=str('%1.1g' %(int(t0)/1e4)))
            dnew = h5fout.create_dataset( 'time' +ftag, data=t0)
            h5fin.close()
            
        h5fout.close()
            
    
    return

################################################################################
################################################################################
def plot_all_aziavgs_beta(runnames,series=1,tag2='',func_lims=None) :

    pnames = ['dirname', 'tend','paper_name1', 'paper_name2',  'rsep0', 'surf_name', 'gdump_name', 'sigma_0']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    sim_info = get_sim_info(['rsep0'], runname='longer2')
    asep = sim_info['rsep0']
    
    dicts = []
    ntimes = 4
    ftags = []
    for iset in np.arange(ntimes) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags)

    filename = 'beta_aziavgs' + tag2 + '.h5'
        
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            new_dict = dict([])
            new_dict['run_name'] = paper_name
            
            h5f = h5py.File(sim_info['dirname']+'paper/'+filename,'r')
            for dset in ['r_out','th_out']:
                new_dict[dset] = h5f[dset][:]
            
            for ftag in ftags :
                for dset in ['label' ,'time'] :
                    new_dict[dset+ftag] = h5f[dset+ftag]
                for dset in ['func'] :
                    new_dict[dset+ftag] = np.log10(np.abs(h5f[dset+ftag][:]))
                new_dict['frame_name'+ftag] = str('%1.1f' %(int(h5f['time'+ftag].value)/1e4))
                
            h5f.close()
            dicts = np.append(dicts,new_dict)

            
    figname = dirout+'beta-aziavgs'+tag2+tag
    rlim = 4.
    if( func_lims is None ) :
        func_lims = [-1.5,1.5]
        
    aux_plot_snapshot_grid(dicts,asep,figname,ftags=ftags,func_lims=func_lims,
                           xlims=[0.,rlim],ylims=[-0.5*rlim,0.5*rlim],label_color='black')
    
    return

################################################################################
################################################################################
def plot_all_spacetime_lumpratio(runnames,series=1) :

    pnames = ['dirname','tend','paper_name1', 'paper_name2',
              'rsep0','hist_freq','surf_name','surf_freq',
              'omega_bin_orbit','freq_bin_orbit' ]

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    funcname = 'rhoav'
    filename = 'modes/mode_spacetime_analysis.h5'
    m_order = 1

    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        if( runname == 'longer2' ) :
            asep = sim_info['rsep0']

        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])

            h5f = h5py.File(sim_info['surf_name']+'.all.h5','r')
            t_out = h5f['tout'][:]
            r_out = h5f['rout'][:]
            h5f.close()
            
            tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
            iend = find_array_index(tend_out,t_out)
            
            h5f = h5py.File(sim_info['dirname']+filename,'r')
            power = h5f[funcname+'_spacetime_power'][0:iend+1,:,:]
            h5f.close()
            
            omega_bin = sim_info['omega_bin_orbit']
            smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 
            n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
            sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
            ftmp1 = signal.convolve2d(power[:,:,m_order],sm_window,mode='same',boundary='symm')
            ftmp0 = signal.convolve2d(power[:,:,0],sm_window,mode='same',boundary='symm')
            fconv2 = ftmp1 / ftmp0 
            #func0_2 = power[:,:,m_order] / power[:,:,0]

            new_dict = dict([])
            new_dict['t_out'] = t_out[0:iend+1]
            new_dict['r_out'] = r_out
            new_dict['func' ] = fconv2
            new_dict['label'] = paper_name
            
            dicts = np.append(dicts,new_dict)


    figname = dirout+'lumppower-spacetime'+tag

    aux_plot_spacetime_contour(dicts,asep,figname,ylims=[0.,tend/1e4],func_lims=[0.,1.])
    
    return

################################################################################
################################################################################
def plot_all_lumpcrit(runnames,series=1) :

    pnames = ['dirname','tend','paper_name1', 'paper_name2',
              'rsep0','hist_freq','surf_name','surf_freq',
              'omega_bin_orbit','freq_bin_orbit', 't_lump_beg',
              't_lump_end', 'header_file' ]

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    sim_info = get_sim_info(['rsep0','freq_bin_orbit'], runname='longer2')
    asep = sim_info['rsep0']
    binary_period = 1./sim_info['freq_bin_orbit']

    rbeg = 2.*asep
    rend = 3.*asep

    funcname = 'rhoav'
    filename = 'modes/mode_spacetime_analysis.h5'
    m_order = 1

    ftags = []
    nfuncs = 4
    for iset in np.arange(nfuncs) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags)

    vlines = []

    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)


        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])

            h5f = h5py.File(sim_info['surf_name']+'.all.h5','r')
            t_out = h5f['tout'][:]
            r_out = h5f['rout'][:]
            h5f.close()

            dr_out = np.gradient(r_out)
            dr = np.outer(np.ones_like(t_out),dr_out)
            
            tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
            it_end = find_array_index(tend_out,t_out)
            
            h5f = h5py.File(sim_info['dirname']+filename,'r')
            power = h5f[funcname+'_spacetime_power'][0:it_end+1,:,:]
            h5f.close()

            h5f = h5py.File(sim_info['header_file'],'r')
            dx1 = h5f['/Header/Grid/dx1'][0]
            h5f.close()
            
            omega_bin = sim_info['omega_bin_orbit']
            smooth_period = 2.*(1./sim_info['freq_bin_orbit']) / 0.28 
            n_smooth = np.int(smooth_period / sim_info['surf_freq'] )
            sm_window = np.reshape(np.ones(n_smooth)/n_smooth,(n_smooth,1))
            fsmooth_m0 = signal.convolve2d(power[:,:,0],sm_window,mode='same',boundary='symm')
            fsmooth_m1 = signal.convolve2d(power[:,:,1],sm_window,mode='same',boundary='symm')
            fsmooth_m2 = signal.convolve2d(power[:,:,2],sm_window,mode='same',boundary='symm')

            ibeg, iend = find_bracket(rbeg,rend,r_out)
            fsmooth_lump_m0 = np.sum(fsmooth_m0[:,ibeg:iend],axis=1) 
            fsmooth_lump_m1 = np.sum(fsmooth_m1[:,ibeg:iend],axis=1) 
            fsmooth_lump_m2 = np.sum(fsmooth_m2[:,ibeg:iend],axis=1)
            f_lump_m0 = np.sum(power[:,ibeg:iend,0],axis=1) 
            f_lump_m1 = np.sum(power[:,ibeg:iend,1],axis=1) 
            f_lump_m2 = np.sum(power[:,ibeg:iend,2],axis=1)

            fsmooth_lump_m1_o_m0 = fsmooth_lump_m1 / fsmooth_lump_m0

            # Because the FFT of \rho includes the \sqrt{-g}, there is
            # an implicty "dr/dxp1" in the FFT amplitude meaning that
            # all we need to do is multipy by dxp1 to get the right integrand for the dr integral:
            
            fsmooth_lump_m0 *= dx1
            fsmooth_lump_m1 *= dx1
            fsmooth_lump_m2 *= dx1
            f_lump_m0 *= dx1
            f_lump_m1 *= dx1
            f_lump_m2 *= dx1
            
            new_dict = dict([])
            new_dict['t_out'] = t_out[0:it_end+1]
            new_dict['func' +ftags[0]] = fsmooth_lump_m1_o_m0
            new_dict['label'+ftags[0]] = None
            new_dict['func' +ftags[1]] = np.log10(np.abs(f_lump_m0))
            new_dict['label'+ftags[1]] = r'$m=0$'
            new_dict['func' +ftags[2]] = np.log10(np.abs(f_lump_m1))
            new_dict['label'+ftags[2]] = r'$m=1$'
            new_dict['func' +ftags[3]] = np.log10(np.abs(f_lump_m2))
            new_dict['label'+ftags[3]] = r'$m=2$'
            vlines = np.append(vlines,sim_info['t_lump_beg'])
                                     
            dicts = np.append(dicts,new_dict)


    figname = dirout+'lumpcrit'+tag
    ylabels = [r'$\int_{2a}^{3a} A_1 dr / \int_{2a}^{3a} A_0 dr$',r'$\log_{10} \int_{2a}^{3a} A_m dr$']
    hlines = [0.2,None]
    aux_plot_timeseries_split(dicts,tend,binary_period,figname,ylabels,ylims=[[0.,0.6],[-4.1,-1.1]],
                              ftags=ftags, vlines=vlines, hlines=hlines)
    
    return

################################################################################
################################################################################
def make_all_mri_avgs(runnames) :

    pnames = ['dirname', 'tend', 'rsep0', 'dump_name', 'dump_freq',
              'gdump_name', 'radflux_name', 'sigma_0','header_file', 'rad_freq']

    sim_info = get_sim_info(['tend','rsep0','dump_freq'], runname='longer2')
    asep = sim_info['rsep0']
    tend_out = int(sim_info['tend'] / sim_info['dump_freq']) * sim_info['dump_freq']
    tsamples0  = np.linspace(4.e4,tend_out,3)

    slice_obj = (slice(None,None,None),slice(None,None,None),slice(None,None,None))

    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        gfile = h5py.File(sim_info['gdump_name'],'r')
        r_out  = gfile['x1'][:,0,0]
        th_out = gfile['x2'][0,:,0]
        ph_out = gfile['x3'][0,0,:]
        fshape = gfile['x1'].shape
        gam = get_val('gam',gfile)
        dx1 = get_val('dx1',gfile)
        dx2 = get_val('dx2',gfile)
        dx3 = get_val('dx3',gfile)
        gfile.close()

        omega_tmp = r_out**(-1.5)
        omega = np.reshape( np.outer(omega_tmp,np.ones(fshape[1:])), fshape)

        tend_out = int(sim_info['tend'] / sim_info['dump_freq']) * sim_info['dump_freq']
        tsamples = np.append(tsamples0,tend_out)
        isamples = tsamples / sim_info['dump_freq']
        isamples = isamples.astype(int)
            
        rad_factor = int( sim_info['dump_freq'] / sim_info['rad_freq'] )
        
        h5fout = h5py.File(sim_info['dirname']+'paper/mri_avgs.h5')
        dnew = h5fout.create_dataset('r_out',data=r_out)
        dnew = h5fout.create_dataset('th_out',data=th_out)
        dnew = h5fout.create_dataset('ph_out',data=ph_out)
        dnew = h5fout.create_dataset('tsamples',data=tsamples)
        dnew = h5fout.create_dataset('isamples',data=isamples)
        dnew = h5fout.create_dataset('00README',data="This is MRI quality factors (Q_MRI), the ratio of the MRI wavelength to the cell extent, as defined in Noble++2012, including all matter, averaged in x2 and x3.")

        if( runname == 'longer2' ):
            tsamples = tsamples[0:-1]
            isamples = isamples[0:-1]
            
        for t0,ind_t in zip(tsamples,isamples) :
            print("t0 = ", t0)
            ind_str = str('.%06d' %ind_t)
            filename = sim_info['dump_name'] + ind_str + '.h5'
            print("opening ", filename)
            h5fin = h5py.File(filename,'r')

            irad_str = str('.%06d' %(ind_t*rad_factor))
            filename = sim_info['radflux_name'] + irad_str + '.h5'
            print("opening ", filename)
            h5fin2 = h5py.File(filename,'r')

            print("time of dump and radump = ", h5fin['/Header/Grid/t'][0], h5fin2['/Header/t'][0])

            
            Qmri1, Qmri2, Qmri3, rho_gdet = calc_Qmri(slice_obj, h5fin, gam=gam, omega=omega, dx1=dx1, dx2=dx2, dx3=dx3, h5file2=h5fin2)

            h5fin.close()
            h5fin2.close()
            
            rho_gdet_avg2 = np.nansum( rho_gdet, axis=1 )
            rho_gdet_avg3 = np.nansum( rho_gdet, axis=2 )
            
            Qmri1_avg2 = np.nansum( Qmri1*rho_gdet, axis=1 ) / rho_gdet_avg2
            Qmri2_avg2 = np.nansum( Qmri2*rho_gdet, axis=1 ) / rho_gdet_avg2
            Qmri3_avg2 = np.nansum( Qmri3*rho_gdet, axis=1 ) / rho_gdet_avg2

            Qmri1_avg3 = np.nansum( Qmri1*rho_gdet, axis=2 ) / rho_gdet_avg3
            Qmri2_avg3 = np.nansum( Qmri2*rho_gdet, axis=2 ) / rho_gdet_avg3
            Qmri3_avg3 = np.nansum( Qmri3*rho_gdet, axis=2 ) / rho_gdet_avg3

            bhloc = get_bbh_pos(t0,runname=runname,spinning=False)
            #get trajectory data, calculate phase and i_ph shift amount, perform shift, calculate new positions BHs
            bhloc_new = np.copy(bhloc)
            ph, bhloc_new  = corotate_phi(ph_out,bhloc)
            
            dnew = h5fout.create_dataset('ph_out'+ind_str, data=ph)
            dnew = h5fout.create_dataset('bhloc'+ind_str, data=bhloc_new)
            dnew = h5fout.create_dataset('bhloc_orig'+ind_str,data=bhloc)
            dnew = h5fout.create_dataset('Qmri1'+ind_str, data=Qmri1)
            dnew = h5fout.create_dataset('Qmri2'+ind_str, data=Qmri2)
            dnew = h5fout.create_dataset('Qmri3'+ind_str, data=Qmri3)
            dnew = h5fout.create_dataset('Qmri1_avg2'+ind_str, data=Qmri1_avg2)
            dnew = h5fout.create_dataset('Qmri2_avg2'+ind_str, data=Qmri2_avg2)
            dnew = h5fout.create_dataset('Qmri3_avg2'+ind_str, data=Qmri3_avg2)
            dnew = h5fout.create_dataset('Qmri1_avg3'+ind_str, data=Qmri1_avg3)
            dnew = h5fout.create_dataset('Qmri2_avg3'+ind_str, data=Qmri2_avg3)
            dnew = h5fout.create_dataset('Qmri3_avg3'+ind_str, data=Qmri3_avg3)
            dnew = h5fout.create_dataset('label'+ind_str, data=str('%1.1g' %(int(t0)/1e4)))
            dnew = h5fout.create_dataset( 'time'+ind_str, data=t0)
            
            
        h5fout.close()
            
    
    return

################################################################################
################################################################################
def make_all_mri_avgs_alltimes(runnames,isamples0=None) :

    pnames = ['dirname', 'tbeg', 'tend', 'rsep0', 'dump_name', 'dump_freq',
              'gdump_name', 'radflux_name', 'sigma_0','header_file', 'rad_freq']

    sim_info = get_sim_info(['tend','rsep0','dump_freq'], runname='longer2')
    asep = sim_info['rsep0']

    slice_obj = (slice(None,None,None),slice(None,None,None),slice(None,None,None))

    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        gfile = h5py.File(sim_info['gdump_name'],'r')
        r_out  = gfile['x1'][:,0,0]
        th_out = gfile['x2'][0,:,0]
        ph_out = gfile['x3'][0,0,:]
        fshape = gfile['x1'].shape
        gam = get_val('gam',gfile)
        dx1 = get_val('dx1',gfile)
        dx2 = get_val('dx2',gfile)
        dx3 = get_val('dx3',gfile)
        gfile.close()

        omega_tmp = r_out**(-1.5)
        omega = np.reshape( np.outer(omega_tmp,np.ones(fshape[1:])), fshape)

        if( isamples0 is None ) : 
            tbeg_out = int(sim_info['tbeg'] / sim_info['dump_freq']) * sim_info['dump_freq']
            tend_out = int(sim_info['tend'] / sim_info['dump_freq']) * sim_info['dump_freq']
            ibeg_out = tbeg_out / sim_info['dump_freq']
            iend_out = tend_out / sim_info['dump_freq']
            isamples = np.arange(ibeg_out, iend_out+1, 1)
        else :
            isamples = np.copy(isamples0)
            
        rad_factor = int( sim_info['dump_freq'] / sim_info['rad_freq'] )
        
        h5fout = h5py.File(sim_info['dirname']+'paper/mri_avgs_alltimes.h5')
        dnew = h5fout.create_dataset('r_out',data=r_out)
        dnew = h5fout.create_dataset('th_out',data=th_out)
        dnew = h5fout.create_dataset('ph_out',data=ph_out)
        dnew = h5fout.create_dataset('isamples',data=isamples)
        dnew = h5fout.create_dataset('00README',data="This is MRI quality factors (Q_MRI), the ratio of the MRI wavelength to the cell extent, as defined in Noble++2012, including all matter, averaged in x2 and x3.")

        tsamples = []
        
        for ind_t in isamples :
            ind_str = str('.%06d' %ind_t)
            filename = sim_info['dump_name'] + ind_str + '.h5'
            print("opening ", filename)
            h5fin = h5py.File(filename,'r')

            irad_str = str('.%06d' %(ind_t*rad_factor))
            filename = sim_info['radflux_name'] + irad_str + '.h5'
            print("opening ", filename)
            h5fin2 = h5py.File(filename,'r')

            t0 = h5fin['/Header/Grid/t'][0]
            t1 = h5fin2['/Header/t'][0]
            if( np.abs((t0-t1)/(t0+1e-10)) > 1e-5 ):
                print("time mismatch in files ", t0, t1 )
                h5fout.close()
                sys.exit("Bad times")

            print("time of dump and radump = ", t0, t1 )

            Qmri1, Qmri2, Qmri3, rho_gdet = calc_Qmri(slice_obj, h5fin, gam=gam, omega=omega, dx1=dx1, dx2=dx2, dx3=dx3, h5file2=h5fin2)

            h5fin.close()
            h5fin2.close()
            
            rho_gdet_avg2 = np.nansum( rho_gdet, axis=1 )
            rho_gdet_avg3 = np.nansum( rho_gdet, axis=2 )
            rho_gdet_avg23 = np.nansum( rho_gdet_avg2, axis=1)
            
            Qmri1_avg2 = np.nansum( Qmri1*rho_gdet, axis=1 ) / rho_gdet_avg2
            Qmri2_avg2 = np.nansum( Qmri2*rho_gdet, axis=1 ) / rho_gdet_avg2
            Qmri3_avg2 = np.nansum( Qmri3*rho_gdet, axis=1 ) / rho_gdet_avg2

            Qmri1_avg3 = np.nansum( Qmri1*rho_gdet, axis=2 ) / rho_gdet_avg3
            Qmri2_avg3 = np.nansum( Qmri2*rho_gdet, axis=2 ) / rho_gdet_avg3
            Qmri3_avg3 = np.nansum( Qmri3*rho_gdet, axis=2 ) / rho_gdet_avg3

            Qmri1_avg23 = np.nansum( Qmri1_avg2, axis=1 ) / rho_gdet_avg23
            Qmri2_avg23 = np.nansum( Qmri2_avg2, axis=1 ) / rho_gdet_avg23
            Qmri3_avg23 = np.nansum( Qmri3_avg2, axis=1 ) / rho_gdet_avg23

            bhloc = get_bbh_pos(t0,runname=runname,spinning=False)
            #get trajectory data, calculate phase and i_ph shift amount, perform shift, calculate new positions BHs
            bhloc_new = np.copy(bhloc)
            ph, bhloc_new  = corotate_phi(ph_out,bhloc)
            
            dnew = h5fout.create_dataset('bhloc'+ind_str, data=bhloc_new)
            dnew = h5fout.create_dataset('bhloc_orig'+ind_str,data=bhloc)
            dnew = h5fout.create_dataset('Qmri1_avg2'+ind_str, data=Qmri1_avg2)
            dnew = h5fout.create_dataset('Qmri2_avg2'+ind_str, data=Qmri2_avg2)
            dnew = h5fout.create_dataset('Qmri3_avg2'+ind_str, data=Qmri3_avg2)
            dnew = h5fout.create_dataset('Qmri1_avg3'+ind_str, data=Qmri1_avg3)
            dnew = h5fout.create_dataset('Qmri2_avg3'+ind_str, data=Qmri2_avg3)
            dnew = h5fout.create_dataset('Qmri3_avg3'+ind_str, data=Qmri3_avg3)
            dnew = h5fout.create_dataset('Qmri1_avg23'+ind_str, data=Qmri1_avg23)
            dnew = h5fout.create_dataset('Qmri2_avg23'+ind_str, data=Qmri2_avg23)
            dnew = h5fout.create_dataset('Qmri3_avg23'+ind_str, data=Qmri3_avg23)
            dnew = h5fout.create_dataset('label'+ind_str, data=str('%1.1g' %(int(t0)/1e4)))
            dnew = h5fout.create_dataset( 'time'+ind_str, data=t0)

            tsamples = np.append(tsamples, t0)
            
        dnew = h5fout.create_dataset('tsamples',data=tsamples)
        h5fout.close()
            
    
    return

################################################################################
################################################################################
def plot_all_mri_avgs(runnames,series=1,tag2='', func_lims=None, funcname='Qmri1_avg2') :

    pnames = ['dirname', 'tend','paper_name1', 'paper_name2', 'rsep0',
              'surf_name', 'gdump_name', 'sigma_0', 'header_file']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    sim_info = get_sim_info(['rsep0'], runname='longer2')
    asep = sim_info['rsep0']
    
    dicts  = []
        
    funcnames = ['Qmri1_avg2','Qmri2_avg2','Qmri3_avg2','Qmri1_avg3','Qmri2_avg3','Qmri3_avg3']
    if( funcname not in funcnames ) :
        print("funcname is not in funcnames: ", funcname, funcnames2)
        sys.exit("funcname does not exist ")
        
    
    ntimes = 4
    ftags = []
    for iset in np.arange(ntimes) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("ftags = ", ftags)

    filename = 'mri_avgs.h5'
        
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            new_dict = dict([])
            new_dict['run_name'] = paper_name

            hfile = h5py.File(sim_info['header_file'],'r')
            m_bh1 = get_val('m_bh1',hfile)
            m_bh2 = get_val('m_bh2',hfile)
            hfile.close()
            rhor1 = m_bh1
            rhor2 = m_bh2
            
            h5f = h5py.File(sim_info['dirname']+'paper/'+filename,'r')
            
            for dset in ['r_out']:
                new_dict[ dset] = h5f[dset][:]

            if( 'avg3' in funcname ) :
                new_dict['th_out'] = h5f['th_out'][:]
                

            isamples = h5f['isamples'][:]
            tsamples = h5f['tsamples'][:]
            if( len(isamples) < ntimes ) :
                isamples = np.append(isamples, isamples[-1])
                tsamples = np.append(tsamples, tsamples[-1])

            new_dict['rhor1'] = rhor1[0]
            new_dict['rhor2'] = rhor2[0]
                
            for ftag, isample, tsample in zip(ftags,isamples,tsamples) :
                ind_str = str('.%06d' %isample)
                for dset in ['label', 'time'] :
                    new_dict[dset+ftag] = h5f[dset+ind_str].value
                for dset in ['ph_out', 'bhloc'] :
                    new_dict[dset+ftag] = h5f[dset+ind_str][:]

                new_dict['func'+ftag] = np.abs(h5f[funcname+ind_str][:])
                
                new_dict['frame_name'+ftag] = str('%1.1f' %(int(tsample)/1e4))
                #new_dict['ph_out'+ftag] = h5f['ph_out'][:]
                #new_dict['frame_name'+ftag] = h5f['label'+ind_str][:]

            h5f.close()

            dicts = np.append(dicts,new_dict)

                
    rlim = 5.
    if( func_lims is None ) :
        func_lims = [0,30.]

    figname = dirout+funcname+tag2+tag
    if( 'avg3' in funcname ) :
        aux_plot_snapshot_grid(dicts,asep,figname,ftags=ftags,func_lims=func_lims,
                               xlims=[0,rlim],ylims=[-0.5*rlim,0.5*rlim],label_color='black')
    else : 
        aux_plot_snapshot_grid(dicts,asep,figname,ftags=ftags,func_lims=func_lims,
                               xlims=[-rlim, rlim], ylims=[-rlim, rlim],label_color='black')
    
    return

################################################################################
################################################################################
#  plot_all_timeseries():
# -----------------
#  plot_mode_power4() above, but putting frames together: 
################################################################################
def plot_all_timeseries(runnames,series=1) :

    pnames = ['dirname', 'surf_name', 'surf_freq', 'freq_bin_orbit',
              'hist_name', 'sigma_0', 'tend',
              'rsep0','omega_bin_orbit','t_lump_beg','t_lump_end','paper_name1',
              'paper_name2','rsep0']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    sim_info = get_sim_info(pnames, runname='longer2')
    asep = sim_info['rsep0']
    binary_period = 1./sim_info['freq_bin_orbit']
    omega_bin = sim_info['omega_bin_orbit']
    omega_lump = 0.25*omega_bin
    period_lump = 2.*np.pi/omega_lump
    tzoom_offset =  - 1.5*period_lump
    omega_bin_0 = omega_bin

    dicts = []
    dicts2 = []
    dicts3 = []
    dicts4 = []
    tend = -100.

    #func_names = [r'$\rho_{m=1}$',r'$\mathcal{L}_{m=0}$',r'$\mathcal{L}_{m=1}$',r'$\left|\phi_{\mathrm{lump}} - \phi_{\mathrm{bin}}\right|$',r'$\omega_\mathrm{lump}$', r'$\log_{10} \ \overline{e(r,t)}$',  r'$\sin(3 \Omega_{\mathrm{bin}} t)$']
    func_names = [r'$\rho_{m=1}$',r'$\mathcal{L}_{m=0}$',r'$\mathcal{L}_{m=1}$',r'$\left|\phi_{\mathrm{lump}} - \phi_{\mathrm{bin}}\right|$',r'$\omega_\mathrm{lump}$', r'$\log_{10} \ \overline{e(r,t)}$']
            
    ftags = []
    for iset in np.arange(len(func_names)) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("nfuncs1 = ", len(func_names))
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])

            h5f = h5py.File(sim_info['dirname']+'modes/ecc_analysis2.h5','r')
            ecc_lump  = h5f['ecc-lump'][:]
            t_out    = h5f['tout'][:]
            h5f.close()

            h5mode = h5py.File(sim_info['dirname']+'modes/mode_analysis.h5','r')
            rho_lump_m1_power_vs_t = h5mode['rhoav_power'][:,1]
            Lut_lump_m0_power_vs_t = h5mode['Lut_power'][:,0]
            Lut_lump_m1_power_vs_t = h5mode['Lut_power'][:,1]
            phase_lump_m1          = h5mode['rhoav_phase'][:,1]
            omega_lump_m1          = h5mode['rhoav_omega'][:,1]
            h5mode.close()

            phase_bin = omega_bin * t_out
            phase_bin = phase_bin % (2.*np.pi)
            phase_out  = phase_lump_m1 - phase_bin
            phase_out[ phase_out < 0. ] += 2. * np.pi
            phase_out /= 8.*np.pi
            phase_out += 0.5

            if( sim_info['t_lump_end'] is None ) :
                t_lump_end = t_out[-1]
            else :
                t_lump_end = sim_info['t_lump_end']
            
            if( sim_info['t_lump_beg'] is None ) :
                t_lump_beg = t_lump_end - 2.5e4
            else :
                t_lump_beg = sim_info['t_lump_beg']

            omega_lump = 0.25*omega_bin
            period_lump = 2.*np.pi/omega_lump
            tend_plot = t_lump_end 
            tbeg_plot = tend_plot - 1.5*period_lump
            
            f_sin1 = np.sin(3.*omega_bin*t_out)
            #funcs_out = [rho_lump_m1_power_vs_t, Lut_lump_m0_power_vs_t, Lut_lump_m1_power_vs_t, phase_out, omega_lump_m1, ecc_lump, f_sin1]
            #norms_out = [np.max(rho_lump_m1_power_vs_t), np.max(Lut_lump_m0_power_vs_t), np.max(Lut_lump_m1_power_vs_t), 1., omega_bin, np.mean(ecc_lump), 1.]
            funcs_out = [rho_lump_m1_power_vs_t, Lut_lump_m0_power_vs_t, Lut_lump_m1_power_vs_t, phase_out, omega_lump_m1, ecc_lump]
            norms_out = [np.max(rho_lump_m1_power_vs_t), np.max(Lut_lump_m0_power_vs_t), np.max(Lut_lump_m1_power_vs_t), 1., omega_bin, np.max(ecc_lump)]

            print("nfuncs2 = ", len(funcs_out))
            print("nfuncs3 = ", len(norms_out))
            
            detrend_order = 5
            t_fft = t_out[(t_out > t_lump_beg)*(t_out < t_lump_end)]
            t_zoom = t_out[(t_out > tbeg_plot)*(t_out < tend_plot)]

            new_dict  = dict([])
            new_dict2 = dict([])
            new_dict3 = dict([])
            new_dict4 = dict([])
            new_dict[ 't_out'] = t_out
            new_dict2['t_out'] = t_fft
            new_dict3['t_out'] = t_zoom - tend_plot

            new_dict[ 'xlims'] = None
            new_dict2['xlims'] = [np.min(new_dict2['t_out']),np.max(new_dict2['t_out'])]
            new_dict3['xlims'] = [np.min(new_dict3['t_out']),np.max(new_dict3['t_out'])]
            new_dict4['xlims'] = None

         
            for ftag,func,fname,fnorm in zip(ftags, funcs_out, func_names, norms_out):
                ftmp = func[(t_out > t_lump_beg)*(t_out < t_lump_end)]
                ftmp2 = func[(t_out > tbeg_plot)*(t_out < tend_plot)]
                fit_func = np.poly1d(np.polyfit(t_fft,ftmp,deg=detrend_order))
                f_fit  = fit_func(t_fft)
                f_fit2 = fit_func(t_zoom)
                fmod  = (ftmp -f_fit )/np.std(ftmp)
                fmod2 = (ftmp2-f_fit2)/np.std(ftmp)
                freq, power, amp = my_psd(t_fft, fmod, fourier_type='welch', make_uniform=True)
                
                new_dict[ 'func'+ftag] = func/fnorm
                new_dict2['func'+ftag] = fmod
                new_dict3['func'+ftag] = fmod2
                new_dict4['func'+ftag] = power

            for ftag,fname in zip(ftags,func_names) : 
                new_dict[ 'label'+ftag] = fname
                new_dict2['label'+ftag] = fname
                new_dict3['label'+ftag] = fname
                new_dict4['label'+ftag] = fname

            new_dict4['t_out'] = freq

            dicts  = np.append(dicts ,new_dict )
            dicts2 = np.append(dicts2,new_dict2)
            dicts3 = np.append(dicts3,new_dict3)
            dicts4 = np.append(dicts4,new_dict4)
            

    ylims = [-0.1,1.1]
    figname = dirout+'timeseries-raw'+tag
    ylabel = r'f(t)'
    aux_plot_timeseries_four_inarow(dicts,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags)
    
    ylims = [-4,4]
    figname = dirout+'timeseries-full-fit'+tag
    ylabel = r'$\left[ f(t) - f_\mathrm{fit}(t)\right] / \sigma_f $'
    aux_plot_timeseries_four_inarow(dicts2,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags)
    
    ylims = [-2,2]
    figname = dirout+'timeseries-zoom-fit'+tag
    xlabels  = [r'$t - t_\mathrm{end} \ \ [10^3 M]$', r'$t - t_\mathrm{end} \ \ [t_\mathrm{bin}]$']
    xlims = [tzoom_offset,100.]
    tscales = [1e3, binary_period]
    aux_plot_timeseries_four_inarow(dicts3,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags,xlims=xlims,xlabels=xlabels,tscales=tscales)
    
    figname = dirout+'timeseries-fft'+tag
    ylabel='Fourier Power of '+r'$\left[ f(t) - f_\mathrm{fit}(t)\right] / \sigma_f $'
    xlabels = [r'$\nu \ \ [10^{-3} M^{-1}]$'  , r'$\omega \ \ [\Omega_\mathrm{bin}]$']
    tscales = [1e-3, (omega_bin_0/(2.*np.pi))]
    xlims = [0.,3.4*(omega_bin_0/(2.*np.pi))]
    aux_plot_timeseries_four_inarow(dicts4,tend,binary_period,figname,ylabel,ylims=[0.,1.4],ftags=ftags,xlims=xlims,xlabels=xlabels,tscales=tscales,prune2='upper',prune1='upper')
    
    return

################################################################################
################################################################################
#  plot_all_timeseries3():
# -----------------
#  plot_mode_power4() above, but putting frames together:
#  mostly lum. and mdot 
################################################################################
def plot_all_timeseries3(runnames,series=1) :

    pnames = ['dirname', 'surf_name', 'surf_freq', 'freq_bin_orbit',
              'hist_name', 'sigma_0', 'tend', 'hist_freq', 'omega_lump',
              'rsep0','omega_bin_orbit','t_lump_beg','t_lump_end','paper_name1',
              'paper_name2','rsep0']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    sim_info = get_sim_info(pnames, runname='longer2')
    asep = sim_info['rsep0']
    binary_period = 1./sim_info['freq_bin_orbit']
    omega_bin = sim_info['omega_bin_orbit']
    omega_lump_0 = sim_info['omega_lump']
    omega_lump = omega_lump_0
    period_lump = 2.*np.pi/omega_lump
    tzoom_offset =  - 1.5*period_lump
    omega_bin_0 = omega_bin

    dicts = []
    dicts2 = []
    dicts3 = []
    dicts4 = []
    vlines = []
    tend = -100.

    #func_names = [r'$\rho_{m=1}$',r'$\mathcal{L}_{m=0}$',r'$\mathcal{L}_{m=1}$',r'$\left|\phi_{\mathrm{lump}} - \phi_{\mathrm{bin}}\right|$',r'$\omega_\mathrm{lump}$', r'$\log_{10} \ \overline{e(r,t)}$',  r'$\sin(3 \Omega_{\mathrm{bin}} t)$']
    #func_names = [r'$\rho_{m=1}$',r'$\mathcal{L}_{m=0}$',r'$\mathcal{L}_{m=1}$',r'$\left|\phi_{\mathrm{lump}} - \phi_{\mathrm{bin}}\right|$',r'$\omega_\mathrm{lump}$', r'$\log_{10} \ \overline{e(r,t)}$']
    func_names = [r'$\mathcal{L}_{\mathrm{tot}}$',r'$\dot{M}(t)$']
            
    ftags = []
    for iset in np.arange(len(func_names)) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("nfuncs1 = ", len(func_names))
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])

            h5f = h5py.File(sim_info['dirname']+'paper/mdot.h5','r')
            tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
            t_tmp = h5f['t_out'][:]
            iend = find_array_index(tend_out,t_tmp)
            mdot = np.abs(h5f['mdot'][0:iend+1,0]) + 2e-4
            t_out = t_tmp[0:iend+1]
            t_tmp = 0
            h5f.close()

            h5f = h5py.File(sim_info['dirname']+'paper/lightcurve.h5','r')
            lum = -h5f['lum'][0:iend+1]
            h5f.close()

            if( sim_info['t_lump_end'] is None ) :
                t_lump_end = t_out[-1]
            else :
                t_lump_end = sim_info['t_lump_end']
            
            if( sim_info['t_lump_beg'] is None ) :
                t_lump_beg = t_lump_end - 2.5e4
            else :
                t_lump_beg = sim_info['t_lump_beg']

            if( sim_info['omega_lump'] is None ) :
                omega_lump = omega_lump_0
            else :
                omega_lump = sim_info['omega_lump']
                
            vval = [omega_lump, sim_info['omega_bin_orbit'], 2.*(sim_info['omega_bin_orbit'] - omega_lump)]
            
            period_lump = 2.*np.pi/omega_lump
            tend_plot = t_lump_end 
            tbeg_plot = tend_plot - 1.5*period_lump
            
            funcs_out = [ lum, mdot]
            norms_out = [np.max(lum), np.max(mdot) ]

            print("norms_out = ", norms_out)

            print("nfuncs2 = ", len(funcs_out))
            print("nfuncs3 = ", len(norms_out))
            
            detrend_order = 5
            t_fft = t_out[(t_out > t_lump_beg)*(t_out < t_lump_end)]
            t_zoom = t_out[(t_out > tbeg_plot)*(t_out < tend_plot)]

            new_dict  = dict([])
            new_dict2 = dict([])
            new_dict3 = dict([])
            new_dict4 = dict([])
            new_dict[ 't_out'] = t_out
            new_dict2['t_out'] = t_fft
            new_dict3['t_out'] = t_zoom - tend_plot

            new_dict[ 'xlims'] = None
            new_dict2['xlims'] = [np.min(new_dict2['t_out']),np.max(new_dict2['t_out'])]
            new_dict3['xlims'] = [np.min(new_dict3['t_out']),np.max(new_dict3['t_out'])]
            new_dict4['xlims'] = None

         
            for ftag,func,fname,fnorm in zip(ftags, funcs_out, func_names, norms_out):
                ftmp = func[(t_out > t_lump_beg)*(t_out < t_lump_end)]
                ftmp2 = func[(t_out > tbeg_plot)*(t_out < tend_plot)]
                fit_func = np.poly1d(np.polyfit(t_fft,ftmp,deg=detrend_order))
                f_fit  = fit_func(t_fft)
                f_fit2 = fit_func(t_zoom)
                #fmod  = (ftmp -f_fit )/np.std(ftmp)
                #fmod2 = (ftmp2-f_fit2)/np.std(ftmp)
                fmod  = (ftmp -f_fit )/np.std((ftmp -f_fit ))
                fmod2 = (ftmp2-f_fit2)/np.std((ftmp -f_fit ))
                freq, power, amp = my_psd(t_fft, fmod, fourier_type='welch', make_uniform=True)
                
                new_dict[ 'func'+ftag] = func/fnorm
                new_dict2['func'+ftag] = fmod
                new_dict3['func'+ftag] = fmod2
                new_dict4['func'+ftag] = power

            for ftag,fname in zip(ftags,func_names) : 
                new_dict[ 'label'+ftag] = fname
                new_dict2['label'+ftag] = fname
                new_dict3['label'+ftag] = fname
                new_dict4['label'+ftag] = fname

            new_dict4['t_out'] = freq

            dicts  = np.append(dicts ,new_dict )
            dicts2 = np.append(dicts2,new_dict2)
            dicts3 = np.append(dicts3,new_dict3)
            dicts4 = np.append(dicts4,new_dict4)

            vlines = np.append(vlines,vval)
            

    ylims = [-0.1,1.1]
    figname = dirout+'timeseries3-raw'+tag
    ylabel = r'f(t)'
    aux_plot_timeseries_four_inarow(dicts,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags)
    
    ylims = [-4,4]
    figname = dirout+'timeseries3-full-fit'+tag
    ylabel = r'$\left[ f(t) - f_\mathrm{fit}(t)\right] / \sigma_f $'
    aux_plot_timeseries_four_inarow(dicts2,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags)
    
    ylims = [-4,4]
    figname = dirout+'timeseries3-zoom-fit'+tag
    xlabels  = [r'$t - t_\mathrm{end} \ \ [10^3 M]$', r'$t - t_\mathrm{end} \ \ [t_\mathrm{bin}]$']
    xlims = [tzoom_offset,100.]
    tscales = [1e3, binary_period]
    aux_plot_timeseries_four_inarow(dicts3,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags,xlims=xlims,xlabels=xlabels,tscales=tscales)
    
    figname = dirout+'timeseries3-fft'+tag
    ylabel='Fourier Power of '+r'$\left[ f(t) - f_\mathrm{fit}(t)\right] / \sigma_f $'
    xlabels = [r'$\nu \ \ [10^{-3} M^{-1}]$'  , r'$\omega \ \ [\Omega_\mathrm{bin}]$']
    tscales = [1e-3, (omega_bin_0/(2.*np.pi))]
    xlims = [0.,3.4*(omega_bin_0/(2.*np.pi))]
    vlines /= (2.*np.pi)
    vlines = np.reshape(vlines,(4,len(vlines)/4))
    print("vlines = ", vlines)
    print("vlines shape = ", np.shape(vlines))
    aux_plot_timeseries_four_inarow(dicts4,tend,binary_period,figname,ylabel,ylims=[0.,1.],ftags=ftags,xlims=xlims,xlabels=xlabels,tscales=tscales,prune2='upper',prune1='upper',vlines=vlines)
    
    return

################################################################################
################################################################################
#  plot_all_timeseries2():
# -----------------
#  plot_mode_power4() above, but putting frames together:
# mostly the lump mass's quantities. 
################################################################################
def plot_all_timeseries2(runnames,series=1) :

    pnames = ['dirname', 'surf_name', 'surf_freq', 'freq_bin_orbit',
              'hist_name', 'sigma_0', 'tend','omega_lump',
              'rsep0','omega_bin_orbit','t_lump_beg','t_lump_end','paper_name1',
              'paper_name2','rsep0']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    sim_info = get_sim_info(pnames, runname='longer2')
    asep = sim_info['rsep0']
    binary_period = 1./sim_info['freq_bin_orbit']
    omega_bin = sim_info['omega_bin_orbit']
    omega_lump_0 = sim_info['omega_lump']
    omega_lump = omega_lump_0
    period_lump = 2.*np.pi/omega_lump
    tzoom_offset =  - 1.5*period_lump
    omega_bin_0 = omega_bin

    dicts = []
    dicts2 = []
    dicts3 = []
    dicts4 = []
    vlines = []
    
    tend = -100.

    #func_names = [r'$\rho_{m=1}$',r'$\mathcal{L}_{m=0}$',r'$\mathcal{L}_{m=1}$',r'$\left|\phi_{\mathrm{lump}} - \phi_{\mathrm{bin}}\right|$',r'$\omega_\mathrm{lump}$', r'$\log_{10} \ \overline{e(r,t)}$',  r'$\sin(3 \Omega_{\mathrm{bin}} t)$']
    #func_names = [r'$\rho_{m=1}$',r'$\mathcal{L}_{m=0}$',r'$\mathcal{L}_{m=1}$',r'$\left|\phi_{\mathrm{lump}} - \phi_{\mathrm{bin}}\right|$',r'$\omega_\mathrm{lump}$', r'$\log_{10} \ \overline{e(r,t)}$']
    func_names = [r'$\rho_{m=1}$',r'$\left|\phi_{\mathrm{lump}} - \phi_{\mathrm{bin}}\right|$',r'$\omega_\mathrm{lump}$', r'$\log_{10} \ \overline{e(r,t)}$']
            
    ftags = []
    for iset in np.arange(len(func_names)) :
        if( iset == 0 ) :
            ftags = np.append(ftags,'')
        else:
            ftags = np.append(ftags,str('%d' %iset))
    
    print("nfuncs1 = ", len(func_names))
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])

            h5f = h5py.File(sim_info['dirname']+'modes/ecc_analysis2.h5','r')
            ecc_lump  = h5f['ecc-lump'][:]
            t_out    = h5f['tout'][:]
            h5f.close()

            h5mode = h5py.File(sim_info['dirname']+'modes/mode_analysis.h5','r')
            rho_lump_m1_power_vs_t = h5mode['rhoav_power'][:,1]
            Lut_lump_m0_power_vs_t = h5mode['Lut_power'][:,0]
            Lut_lump_m1_power_vs_t = h5mode['Lut_power'][:,1]
            phase_lump_m1          = h5mode['rhoav_phase'][:,1]
            omega_lump_m1          = h5mode['rhoav_omega'][:,1]
            h5mode.close()

            phase_bin = omega_bin * t_out
            phase_bin = phase_bin % (2.*np.pi)
            phase_out  = phase_lump_m1 - phase_bin
            phase_out[ phase_out < 0. ] += 2. * np.pi
            phase_out /= 8.*np.pi
            #phase_out += 0.5

            if( sim_info['t_lump_end'] is None ) :
                t_lump_end = t_out[-1]
            else :
                t_lump_end = sim_info['t_lump_end']
            
            if( sim_info['t_lump_beg'] is None ) :
                t_lump_beg = t_lump_end - 2.5e4
            else :
                t_lump_beg = sim_info['t_lump_beg']

            if( sim_info['omega_lump'] is None ) :
                omega_lump = omega_lump_0
            else :
                omega_lump = sim_info['omega_lump']

            period_lump = 2.*np.pi/omega_lump
            tend_plot = t_lump_end 
            tbeg_plot = tend_plot - 1.5*period_lump

            vval = [omega_lump, sim_info['omega_bin_orbit'], 2.*(sim_info['omega_bin_orbit'] - omega_lump)]
            
            f_sin1 = np.sin(3.*omega_bin*t_out)
            #funcs_out = [rho_lump_m1_power_vs_t, Lut_lump_m0_power_vs_t, Lut_lump_m1_power_vs_t, phase_out, omega_lump_m1, ecc_lump, f_sin1]
            #norms_out = [np.max(rho_lump_m1_power_vs_t), np.max(Lut_lump_m0_power_vs_t), np.max(Lut_lump_m1_power_vs_t), 1., omega_bin, np.mean(ecc_lump), 1.]
            #funcs_out = [rho_lump_m1_power_vs_t, Lut_lump_m0_power_vs_t, Lut_lump_m1_power_vs_t, phase_out, omega_lump_m1, ecc_lump]
            #norms_out = [np.max(rho_lump_m1_power_vs_t), np.max(Lut_lump_m0_power_vs_t), np.max(Lut_lump_m1_power_vs_t), 1., omega_bin, np.max(ecc_lump)]
            funcs_out = [rho_lump_m1_power_vs_t, phase_out, omega_lump_m1, ecc_lump]
            norms_out = [np.max(rho_lump_m1_power_vs_t), 1., omega_bin, np.max(ecc_lump)]

            print("nfuncs2 = ", len(funcs_out))
            print("nfuncs3 = ", len(norms_out))
            
            detrend_order = 5
            t_fft = t_out[(t_out > t_lump_beg)*(t_out < t_lump_end)]
            t_zoom = t_out[(t_out > tbeg_plot)*(t_out < tend_plot)]

            new_dict  = dict([])
            new_dict2 = dict([])
            new_dict3 = dict([])
            new_dict4 = dict([])
            new_dict[ 't_out'] = t_out
            new_dict2['t_out'] = t_fft
            new_dict3['t_out'] = t_zoom - tend_plot

            new_dict[ 'xlims'] = None
            new_dict2['xlims'] = [np.min(new_dict2['t_out']),np.max(new_dict2['t_out'])]
            new_dict3['xlims'] = [np.min(new_dict3['t_out']),np.max(new_dict3['t_out'])]
            new_dict4['xlims'] = None

         
            for ftag,func,fname,fnorm in zip(ftags, funcs_out, func_names, norms_out):
                ftmp = func[(t_out > t_lump_beg)*(t_out < t_lump_end)]
                ftmp2 = func[(t_out > tbeg_plot)*(t_out < tend_plot)]
                fit_func = np.poly1d(np.polyfit(t_fft,ftmp,deg=detrend_order))
                f_fit  = fit_func(t_fft)
                f_fit2 = fit_func(t_zoom)
                #fmod  = (ftmp -f_fit )/np.std(ftmp)
                #fmod2 = (ftmp2-f_fit2)/np.std(ftmp)
                fmod  = (ftmp -f_fit )/np.std((ftmp -f_fit ))
                fmod2 = (ftmp2-f_fit2)/np.std((ftmp -f_fit ))
                freq, power, amp = my_psd(t_fft, fmod, fourier_type='welch', make_uniform=True)
                
                new_dict[ 'func'+ftag] = func/fnorm
                new_dict2['func'+ftag] = fmod
                new_dict3['func'+ftag] = fmod2
                new_dict4['func'+ftag] = power

            for ftag,fname in zip(ftags,func_names) : 
                new_dict[ 'label'+ftag] = fname
                new_dict2['label'+ftag] = fname
                new_dict3['label'+ftag] = fname
                new_dict4['label'+ftag] = fname

            new_dict4['t_out'] = freq

            dicts  = np.append(dicts ,new_dict )
            dicts2 = np.append(dicts2,new_dict2)
            dicts3 = np.append(dicts3,new_dict3)
            dicts4 = np.append(dicts4,new_dict4)
            
            vlines = np.append(vlines,vval)
            

    ylims = [-0.1,1.1]
    figname = dirout+'timeseries2-raw'+tag
    ylabel = r'f(t)'
    aux_plot_timeseries_four_inarow(dicts,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags)
    
    ylims = [-4,4]
    figname = dirout+'timeseries2-full-fit'+tag
    ylabel = r'$\left[ f(t) - f_\mathrm{fit}(t)\right] / \sigma_f $'
    aux_plot_timeseries_four_inarow(dicts2,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags)
    
    ylims = [-2,2]
    figname = dirout+'timeseries2-zoom-fit'+tag
    xlabels  = [r'$t - t_\mathrm{end} \ \ [10^3 M]$', r'$t - t_\mathrm{end} \ \ [t_\mathrm{bin}]$']
    xlims = [tzoom_offset,100.]
    tscales = [1e3, binary_period]
    aux_plot_timeseries_four_inarow(dicts3,tend,binary_period,figname,ylabel,ylims=ylims,ftags=ftags,xlims=xlims,xlabels=xlabels,tscales=tscales)
    
    figname = dirout+'timeseries2-fft'+tag
    ylabel='Fourier Power of '+r'$\left[ f(t) - f_\mathrm{fit}(t)\right] / \sigma_f $'
    xlabels = [r'$\nu \ \ [10^{-3} M^{-1}]$'  , r'$\omega \ \ [\Omega_\mathrm{bin}]$']
    tscales = [1e-3, (omega_bin_0/(2.*np.pi))]
    xlims = [0.,3.4*(omega_bin_0/(2.*np.pi))]
    vlines /= (2.*np.pi)
    vlines = np.reshape(vlines,(4,len(vlines)/4))
    aux_plot_timeseries_four_inarow(dicts4,tend,binary_period,figname,ylabel,ylims=[0.,1.4],ftags=ftags,xlims=xlims,xlabels=xlabels,tscales=tscales,prune2='upper',prune1='upper',vlines=vlines)
    
    return

################################################################################
################################################################################
#  plot_all_omegalump():
# -----------------
#  taken from plot_all_ecc() in this file 
#  
################################################################################
def plot_all_omegalump(runnames,series=1) :

    pnames = ['dirname', 'surf_name', 'surf_freq', 'freq_bin_orbit',
              'hist_name', 'sigma_0', 'tend', 'hist_freq',
              'rsep0','omega_bin_orbit','t_lump_beg','t_lump_end','paper_name1',
              'paper_name2','rsep0']

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    ftags=['','2']
    
    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)
        if( runname == 'longer2' ) :
            binary_period = 1./sim_info['freq_bin_orbit']
            
        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])
            omega_bin = sim_info['omega_bin_orbit']

            h5f = h5py.File(sim_info['dirname']+'modes/ecc_analysis2.h5','r')
            t_out    = h5f['tout'][:]
            h5f.close()

            h5mode = h5py.File(sim_info['dirname']+'modes/mode_analysis.h5','r')
            omega_lump = h5mode['rhoav_omega'][:,1]
            omega_lump /= omega_bin
            h5mode.close()
            
            if( sim_info['t_lump_end'] is None ) :
                t_lump_end = t_out[-1]
            else :
                t_lump_end = sim_info['t_lump_end']
            
            if( sim_info['t_lump_beg'] is None ) :
                t_lump_beg = t_lump_end - 2.5e4
            else :
                t_lump_beg = sim_info['t_lump_beg']

            favg = np.mean(omega_lump[(t_out > t_lump_beg)*(t_out < t_lump_end)])
            fstd = np.std(omega_lump[(t_out > t_lump_beg)*(t_out < t_lump_end)])
            print(" ", paper_name, "  omega_lump_avg = ", favg, " +/- ", fstd) 
            new_dict = dict([])
            new_dict['func'] = omega_lump
            new_dict['func2'] = np.full_like(t_out,favg)
            new_dict['t_out'  ] = t_out
            new_dict['label'] = paper_name
            new_dict['label2'] = None
            new_dict['xlims'] = None

            dicts = np.append(dicts,new_dict)


    ylabel = r'$\omega_\mathrm{lump}(t)$'
    figname = dirout+'omega-lump'+tag
    aux_plot_timeseries_four_inarow(dicts,tend,binary_period,figname,ylabel,ylims=[0.,0.5],ftags=[''])
    
    return

################################################################################
################################################################################
def plot_all_spacetime_maxstress(runnames,series=1) :

    pnames = ['dirname','tend','paper_name1', 'paper_name2',
              'rsep0','hist_freq','surf_name','surf_freq',
              'omega_bin_orbit','freq_bin_orbit' ]

    dirout = '/mnt/lustre/scn/home/tex/mass-ratio-paper/longer2/all/'

    dicts = []
    tend = -100.

    filename = 'paper/stresses.h5'
    m_order = 1

    for runname in runnames :
        print("Processing run = ", runname )
        sim_info = get_sim_info(pnames, runname=runname)

        if( runname == 'longer2' ) :
            asep = sim_info['rsep0']

        if( series == 1 ) :
            paper_name = sim_info['paper_name1']
            tag='-mass-ratio'
        else :
            paper_name = sim_info['paper_name2']
            tag='-init-data'

        if( paper_name is not None ) :
            tend = np.max([tend,sim_info['tend']])

            h5f = h5py.File(sim_info['dirname']+filename,'r')
            t_hist  = h5f['t_out'][:]
            tend_out = int(sim_info['tend'] / sim_info['hist_freq'] - 1) * sim_info['hist_freq']
            iend = find_array_index(tend_out,t_hist)
            t_out = t_hist[0:iend]
            surfdens = h5f['rhoav'][0:iend,:]
            maxwell = h5f['Txzd'][0:iend,:]
            r_out   = h5f['r_out'][:]
            h5f.close()

            rc = np.outer(np.ones_like(t_out),r_out)
            maxwell  *= 1.39/rc
 
            new_dict = dict([])
            new_dict['t_out'] = t_out[0:iend]
            new_dict['r_out'] = r_out
            new_dict['func' ] = np.log10(np.abs(maxwell/surfdens))
            new_dict['label'] = paper_name
            
            dicts = np.append(dicts,new_dict)


    figname = dirout+'maxwell-o-surfdens-spacetime'+tag

    aux_plot_spacetime_contour(dicts,asep,figname,ylims=[0.,tend/1e4],func_lims=[-7.,-1.])
    
    return

################################################################################
################################################################################
#
# plot_integrated_fieldlines():
# ------------------
#
#   -- plots magnetic field lines over a color contour of density; 
#
#   -- needs the rmin, rmax combo used for the calculation to read in the
#      right part of the r grid.
################################################################################
def plot_integrated_fieldlines(runname,itime,
                               rmin=-1.e100,rmax=1.e100,
                               cmap  = plt.cm.Spectral_r,
                               nticks=10,
                               time_unit_str='M',
                               tag='',
                               funcname='rho',
                               corotate=False,
                               dirout=None,
                               dump_name=None
                              ) :
  
    pnames = ['dirname', 'dump_name', 'traj_file', 'gdump_name', 'header_file']
    sim_info = get_sim_info(pnames, runname=runname)

    if( dump_name is None ) :
        dump_name = sim_info['dump_name']

    if( dirout is None ) : 
        dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/curl2/'
    
    timeid = '%06d' %itime
    
    curl_filename = dump_name + '.curl_int'+tag+'.' + timeid + '.h5'
    print("plot_integrated_fieldlines(): working on run = ", runname, curl_filename)
    suffix = tag+'-'+timeid

    # Get coordinates:
    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)
    gfile  = h5py.File(sim_info['gdump_name'], 'r')

    slice_rr = (slc_all,slice(0,1,1),slice(0,1,1))
    rr_orig = read_3d_hdf5_slice('x1',slice_rr,gfile)

    ind_matching,  = np.where( (rr_orig >= rmin) & (rr_orig <= rmax) )
    ir_min = np.min(ind_matching)
    ir_max = np.max(ind_matching)
    rr = rr_orig[ir_min:ir_max+1]
    slc_r = slice(ir_min, ir_max+1, 1)

    slice_ph = (slice(0,1,1),slice(0,1,1),slc_all)
    ph       = read_3d_hdf5_slice('x3',slice_ph,gfile)

    h5file  = h5py.File(sim_info['header_file'], 'r')
    idim, jdim, kdim, xp1, xp2, xp3 = get_grid(h5file)
    h5file.close()
    j_eq = int(jdim/2)
    slc_th_eq = slice(j_eq, j_eq+1, 1)
    slc_dx = (slc_r, slc_th_eq, slc_all)
    print("slc_r     = ", slc_r) 
    print("slc_th_eq = ", slc_th_eq) 
    print("slc_all   = ", slc_all) 
    dx_dxp11 = read_3d_hdf5_slice('dxdxp11',slc_dx,gfile)

    gcov11 = read_3d_hdf5_slice('gcov311', slc_dx,gfile)
    gcov33 = read_3d_hdf5_slice('gcov333', slc_dx,gfile)
    f0tmp = 1./np.sqrt(gcov11)
    B1_factor = viz_identify_symmetry_boundary(f0tmp)
    f0tmp = 1./np.sqrt(gcov33)
    B3_factor = viz_identify_symmetry_boundary(f0tmp)
    
    gfile.close()

    rr_2d = np.outer(rr,np.ones_like(ph))

    ###########################################################
    # Open curl file: 
    ###########################################################
    h5file = h5py.File(curl_filename,'r')
    
    
    # Get bbh positions:
    gfile  = h5py.File(sim_info['header_file'], 'r')
    m_bh1 = get_val('m_bh1',gfile)
    m_bh2 = get_val('m_bh2',gfile)
    rhor1 = m_bh1
    rhor2 = m_bh2
    gam = get_val('gam',gfile)
    gfile.close() 
    xbh1, ybh1, zbh1, xbh2, ybh2, zbh2 = get_bbh_pos(h5file['t'][0],runname=runname,spinning=False)

    
    if( corotate ) :
        bhloc = [xbh1, ybh1, zbh1, xbh2, ybh2, zbh2]
        ph, bhloc_new  = corotate_phi(ph,bhloc)
        xbh1, ybh1, zbh1, xbh2, ybh2, zbh2 = bhloc_new 

    ph_2d = np.outer(np.ones_like(rr),ph)

    dx_dr = np.cos(ph_2d)
    dy_dr = np.sin(ph_2d)
    xx_slice_tmp = rr_2d * dx_dr
    yy_slice_tmp = rr_2d * dy_dr

    xx_slice = viz_identify_symmetry_boundary(xx_slice_tmp)
    yy_slice = viz_identify_symmetry_boundary(yy_slice_tmp)

    rcutout = np.min(rr)

    dx_dxp1_tmp = dx_dxp11 * dx_dr
    dy_dxp1_tmp = dx_dxp11 * dy_dr
    dx_dxp1 = viz_identify_symmetry_boundary(dx_dxp1_tmp)
    dy_dxp1 = viz_identify_symmetry_boundary(dy_dxp1_tmp)
    
    dx_dphi = -yy_slice
    dy_dphi =  xx_slice


    ###########################################################

    # functions  in the denominator: 
    norm_funcs = ['vol']

    # available functions: 
    all_funcnames = ['bmag',
                     'bsq',
                     'curl_b_1',
                     'curl_b_2',   
                     'curl_b_3',   
                     'curl_b_mag',
                     'B1',
                     'B2',
                     'B3',
                     'rho',
                     'coolfunc',
                     'uu',
                     'kinetic']

    #funcnames = ['rho','B1','B2','B3','bsq','curl_b_mag','coolfunc','uu']
    funcnames = ['B3','rho','coolfunc','uu','bsq','curl_b_mag']

    # denominator : numerator   pairs : 
    func_map = { 'vol': {'curl_b_1','curl_b_2','curl_b_3','curl_b_abs1','curl_b_abs2','curl_b_abs3','curl_b_mag','rho','uu','coolfunc','bsq','bmag','B1','B2','B3'},
                 'bmag' : {'curl_b_mag'}, 
                 'bsq'  : {'rho','coolfunc','uu','kinetic','curl_b_mag'},
                 'rho'  : {'bsq','bmag','coolfunc','uu','kinetic','curl_b_mag'},
                 'energy' : {'kinetic'} ,
                 'sqrtrho' : {'curl_b_mag','sqrtenergy'} ,
                 'sqrtenergy' : {'curl_b_mag'} }

    title = 't =  ' + str('%7.0f' %h5file['t'][0])+time_unit_str

    print(" Plotting data....")
    ifunc = 0
    sf = '_int'

    stream_data = None
    
    for norm_func in norm_funcs :
        fname0 = norm_func + sf
        print("norm_func name = ", fname0)
        f0tmp = h5file[fname0][:]
        f0 = viz_identify_symmetry_boundary(f0tmp)

        # Code here assumes something like DIAGONAL3 coordinates: 
        fname1 = 'B1' + sf 
        fname2 = 'B3' + sf
        fname_mag = 'bmag' + sf
        print("func names = ", fname1, fname2)
        f1tmp = h5file[fname1][:]
        f1 = viz_identify_symmetry_boundary(f1tmp)
        vec_xp1 = f1 / np.abs(f0)
        vec_xp1 *= B1_factor 
        f1tmp = 0 
        f1 = 0
        f1tmp = h5file[fname2][:]
        f1 = viz_identify_symmetry_boundary(f1tmp)
        vec_phi = f1 / np.abs(f0)
        vec_phi *= B3_factor 
        f1tmp = 0 
        f1 = 0

        f1tmp = h5file[fname_mag][:]
        f_mag = viz_identify_symmetry_boundary(f1tmp)
        f_mag /= np.abs(f0)
        
        # test case: v^x = 1., v^y = 0. 
        if 0: 
            ftmp00 = xx_slice_tmp / rr_2d**2 
            vec_xp1 = viz_identify_symmetry_boundary(ftmp00)
            ftmp00 = -np.sin(ph_2d)/rr_2d
            vec_phi = viz_identify_symmetry_boundary(ftmp00)

            # test case: v^x = 0., v^y = 1. 
        if 0: 
            ftmp00 = yy_slice_tmp / rr_2d**2 
            vec_xp1 = viz_identify_symmetry_boundary(ftmp00)
            ftmp00 =  np.cos(ph_2d)/rr_2d
            vec_phi = viz_identify_symmetry_boundary(ftmp00)


        vec_x = dx_dxp1 * vec_xp1  +  dx_dphi * vec_phi
        vec_y = dy_dxp1 * vec_xp1  +  dy_dphi * vec_phi


        for funcname in funcnames : 
            fnameout = funcname + sf
            print(" funcout name = ", fnameout) 
            ftmp = h5file[fnameout][:]
            fout = viz_identify_symmetry_boundary(ftmp)
            funcout = fout / np.abs(f0)
        
            figname = dirout+'intx2-'+funcname+'-over-'+norm_func+'-fieldlines'+suffix

            if( stream_data is None  ) : 
                stream_data = aux_plot_int_curl(xx_slice,yy_slice,funcout,figname,
                                                title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,
                                                rhor1,rhor2,fieldlines=[vec_x,vec_y,f_mag],dolog=False)
            else :
                stream_data = aux_plot_int_curl(xx_slice,yy_slice,funcout,figname,
                                                title,nticks,cmap,rcutout,xbh1,ybh1,xbh2,ybh2,
                                                rhor1,rhor2,stream_data=stream_data,dolog=False)
                
            
    h5file.close()

    return


################################################################################
################################################################################
def test_corr() :

    nphi = 1000
    Amp1 = 1.
    Amp2 = 1.
    phi = np.linspace(0.,2.*np.pi, nphi)
    mode1 = 1.e-6
    mode2 = 5.
    phase1 = 0.1*np.pi
    phase2 = 0.93*np.pi
    ftmp1 = Amp1*np.cos(mode1*phi+phase1)
    ftmp2 = Amp2*np.cos(mode2*phi+phase2)
    func1 = ftmp1 - np.average(ftmp1)
    func2 = ftmp2 - np.average(ftmp2)

    std1 = np.std(func1)
    std2 = np.std(func2)

    #fcorr = func1*func2 / (std1*std2)
    fcorr = func2*func2 / (std2*std2)

    print("max of fcorr = ", np.max(fcorr) )
    
    nwin = 0 
    figname = 'test-corr'
    fig = plt.figure(nwin)
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r'y')
    ax.set_xlabel(r'$\phi$')
    ax.plot(phi,func1,label='f1')
    ax.plot(phi,func2,label='f2')
    ax.plot(phi,fcorr,label='corr')
    ax.legend(loc=0)

    fig.savefig('%s.png' %figname, dpi=300,bbox_inches='tight')
    
    return

################################################################################
# # This allows the python module to be executed like:   
# # 
# #   prompt>  python lump
# # 
# ###############################################################################
# if __name__ == "__main__":
#     import sys
#     print(sys.argv)
#     if len(sys.argv) > 1 :
#         lump(sys.argv[1])
#     else:
#         lump()
# 

#  Make the mode_analysis.h5 files: 
if 0 : 
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    all_funcs = ['rhoav','Mxz','Txza','bsq','pavg','ddot','lrho','Lut','dTdr_tot','dTdr_h','dTdr_m']
    
    for run in all_runs : 
        for func in all_funcs : 
            make_mode_power_dataset(funcname=func, runname=run)


if 0 : 
    all_runs = ['longer2', 'medium_disk', 'bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    #all_funcs = ['rhoav','Mxz','Txza','bsq','pavg','ddot','lrho','Lut','dTdr_tot','dTdr_h','dTdr_m']
    all_funcs = ['rhoav','Lut','bsq','pavg']   #'Mxz','Txza','dTdr_tot']
    
    for run in all_runs : 
        for func in all_funcs :
            make_mode_power_spacetime_dataset(funcname=func, runname=run)            


########################################################################################
# old way: 
if 0 : 
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    func = 'rhoav'
    tall1_1 = []
    tall1_2 = []
    tall2_1 = []
    tall2_2 = []
    
    for run in all_runs :
        t1_1, t1_2, t2_1, t2_2 = find_tlump(runname=run,funcname=func)
        tall1_1.append(t1_1)
        tall1_2.append(t1_2)
        tall2_1.append(t2_1)
        tall2_2.append(t2_2)

    i = 0 
    for run in all_runs : 
        print("results   ", run, "  tlumps = ",tall1_1[i], tall1_2[i], tall2_1[i], tall2_2[i])
        i += 1
        
########################################################################################
if 0 : 
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    func = 'rhoav'
    tall1_1 = []
    tall1_2 = []
    tall2_1 = []
    tall2_2 = []
    
    for run in all_runs :
        t1_1 = find_tlump2(runname=run,funcname=func)
        tall1_1.append(t1_1)

    i = 0 
    for run in all_runs : 
        print("results   ", run, "  tlumps = ",tall1_1[i])
        i += 1
        
########################################################################################
        
if 0 : 
    all_runs = ['longer2']
    #all_funcs = ['bsq']
    all_funcs = ['rhoav','Lut','bsq','pavg']
    #all_funcs = ['Mxz','Txza','dTdr_tot']
    
    for run in all_runs : 
        for func in all_funcs :
            #for m_order in [1,2,3]:
            for m_order in [0,1,2,3]:
            #for m_order in [0]:
                plot_lump_power_spacetime(funcname=func,runname=run,m_order=m_order)
                plot_lump_power_spacetime(funcname=func,runname=run,m_order=m_order,dolog=False)


if 0 : 
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    #all_runs = ['longer2']
    all_funcs = ['rhoav','Lut','bsq','pavg']
    #all_funcs = ['Mxz','Txza','dTdr_tot']
    
    for run in all_runs : 
        for func in all_funcs :
            for m_order in [0,1,2,3]:
                plot_lump_power_spacetime(funcname=func,runname=run,m_order=m_order)
                plot_lump_power_spacetime(funcname=func,runname=run,m_order=m_order,dolog=False)
                #plot_lump_phases_spacetime(funcname=func,runname=run,m_order=m_order)
                #plot_lump_omegas_spacetime(funcname=func,runname=run,m_order=m_order)
            for m_order in [1] : 
                plot_lump_power_spacetime_special(funcname=func,runname=run,m_order=m_order)

########################################################################################
all_runs = ['longer2', 'proj1_q2', 'proj1_q5', 'proj1_q10']
tmax_qsurvey = get_tmax_of_allruns(all_runs)
all_runs = ['longer2', 'medium_disk','bigger_disk', 'inject_disk']
tmax_initsurvey = get_tmax_of_allruns(all_runs)
tmax_max = np.max([tmax_initsurvey,tmax_qsurvey])
    

if 0 : 
    all_runs = ['longer2', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    all_funcs = ['rhoav','Lut','bsq','pavg']

    tag = '-qsurvey'
    tmax = tmax_qsurvey

    for run in all_runs :
        for func in all_funcs :
            for m_order in [1] : 
                plot_lump_power_spacetime_special(funcname=func,runname=run,m_order=m_order,dolog=False,tag=tag,t_plot_max=tmax)
                plot_lump_power_spacetime_special(funcname=func,runname=run,m_order=m_order,tag=tag,t_plot_max=tmax)



                
if 0 : 
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk']
    all_funcs = ['rhoav','Lut','bsq','pavg']

    tag = '-initsurvey'
    tmax = tmax_initsurvey

    for run in all_runs :
        for func in all_funcs :
            for m_order in [1] : 
                plot_lump_power_spacetime_special(funcname=func,runname=run,m_order=m_order,dolog=False,tag=tag,t_plot_max=tmax)
                plot_lump_power_spacetime_special(funcname=func,runname=run,m_order=m_order,tag=tag,t_plot_max=tmax)



                

if 0 : 
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    all_funcs = ['rhoav','Lut','bsq','pavg']
    
    for run in all_runs : 
        for func in all_funcs :
            plot_lump_power_spacetime_lines(funcname=func,runname=run)
            plot_lump_power_spacetime_lines(funcname=func,runname=run,dolog=False)

                                
if 0 : 
    #all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    all_runs = ['inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    #all_runs = ['longer2']
    all_funcs = ['rhoav','Lut','bsq','pavg']
    #all_funcs = ['rhoav']
    
    for run in all_runs : 
        for func in all_funcs :
            for m_order in [1,2,3]:
                #plot_lump_phases_spacetime(funcname=func,runname=run,m_order=m_order)
                #plot_lump_omegas_spacetime(funcname=func,runname=run,m_order=m_order)
                #plot_lump_power_spacetime(funcname=func,runname=run,m_order=m_order,dolog=False)
                # plot_lump_phase_fft_spacetime(funcname=func,runname=run,m_order=m_order,dolog=True)
                # plot_lump_phase_fft_spacetime(funcname=func,runname=run,m_order=m_order,dolog=False)
                plot_lump_power_fft_spacetime(funcname=func,runname=run,m_order=m_order,dolog=True,maxf=-3.)
                plot_lump_power_fft_spacetime(funcname=func,runname=run,m_order=m_order,dolog=False,maxf=1e-3)

if 0 : 
    #all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    all_runs = ['longer2','medium_disk','bigger_disk']
    for run in all_runs : 
        plot_lump_power_avgs(runname=run)

if 0 : 
    #all_funcs = ['rhoav','Lut','bsq','pavg']
    all_funcs = ['rhoav']

    all_runs = ['longer2','proj1_q2', 'proj1_q5', 'proj1_q10']
    tag = 'qsurvey-'
    tmax=tmax_qsurvey
    for run in all_runs : 
        for func in all_funcs :
            plot_lump_criteria2(runname=run,funcname=func,savepdf=True,tmax=tmax,tag=tag)


    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk']
    tag = 'initsurvey-'
    tmax=tmax_initsurvey
    for run in all_runs : 
        for func in all_funcs :
            plot_lump_criteria2(runname=run,funcname=func,savepdf=True,tmax=tmax,tag=tag)


if 0 : 
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk']
    all_funcs = ['rhoav','Lut','bsq','pavg']

    tag = '-initsurvey'
    tmax = get_tmax_of_allruns(all_runs)

            
                
if 0 : 
    all_runs = ['longer2']
    all_funcs = ['testdata']

    for run in all_runs : 
        for func in all_funcs : 
            make_mode_power_spacetime_dataset(funcname=func, runname=run)
    
    for run in all_runs : 
        for func in all_funcs :
            for m_order in [0,1,2,3]:
                if( m_order == 0  ): 
                    plot_lump_power_spacetime(funcname=func,runname=run,m_order=m_order)
                else : 
                    plot_lump_power_spacetime(funcname=func,runname=run,minf=-2.6,maxf=0.,m_order=m_order)

if 0 : 
    #all_runs = ['proj1_q2', 'proj1_q5', 'proj1_q10']
    all_runs = ['proj1_q5']
    all_funcs = ['dTdr_tot','dTdr_h','dTdr_m']
    
    for run in all_runs : 
        for func in all_funcs : 
            make_mode_power_dataset(funcname=func, runname=run)

if 0 : 
    #all_runs = ['proj1_q2', 'proj1_q5', 'proj1_q10']
    all_runs = ['proj1_q10']
    all_funcs = ['rhoav','Mxz','Txza','bsq','pavg','ddot','lrho','Lut','dTdr_tot','dTdr_h','dTdr_m']
    
    for run in all_runs : 
        for func in all_funcs : 
            make_mode_power_dataset(funcname=func, runname=run)


#  make the plots 
if 0 : 
    #all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    all_runs = ['proj1_q2', 'proj1_q5', 'proj1_q10']
    all_funcs = ['rhoav','Mxz','Txza','bsq','pavg','ddot','lrho','Lut','dTdr_tot','dTdr_h','dTdr_m']
    for run in all_runs : 
        for func in all_funcs : 
            plot_mode_power(funcname=func, runname=run)


if 0 : 
    #all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    #all_runs = ['proj1_q2', 'proj1_q5', 'proj1_q10']
    all_runs = ['longer2']
    for run in all_runs : 
        #plot_mode_power2(runname=run)
        #plot_mode_power3(runname=run)
        plot_lump_stress(runname=run)


#Redo runs with Bound and Unbound data: 

if 0 : 
    #all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    #all_runs = ['longer2']
    all_runs = ['inject_disk']
    for run in all_runs : 
        #plot_mode_power2(runname=run)
        #plot_mode_power3(runname=run)
        plot_mode_power4(runname=run)
        #plot_lump_stress(runname=run)

if 0 : 
    #all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    #all_runs = ['longer2','proj1_q2', 'proj1_q5', 'proj1_q10']
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk']
    plot_all_mdot(all_runs,series=2)


if 0 : 
    all_runs = ['longer2','medium_disk','bigger_disk', 'inject_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10']
    all_funcs = ['rhoav','Lut','bsq','pavg']   #'Mxz','Txza','dTdr_tot']
    for run in all_runs : 
        for func in all_funcs : 
            make_mode_power_spacetime_dataset(funcname=func, runname=run)
        


if 0 :
    for itimes in np.arange(200,380) :
        plot_curl_b('longer2',itimes)
    #plot_curl_v('longer2',300)
    #plot_curl_b('longer2',325)
    #plot_curl_b('longer2',350)
    #plot_curl_b('longer2',379)

if 0 :
    for itimes in np.arange(200,380) :
        plot_curl_v('longer2',itimes)
    #plot_curl_v('longer2',300)
    #plot_curl_b('longer2',325)
    #plot_curl_b('longer2',350)
    #plot_curl_b('longer2',379)



thmin=0.5*np.pi - 0.2
thmax=0.5*np.pi + 0.2

basedir='/mnt/lustre/scn/project1-prod-mhd-final-s0-longer2/dumps-high-cadence/series000/'
dump_name=basedir+'run000-dumps/KDHARM0'
rad_name=dump_name+'.RADFLUX'
dirout=basedir+'plots/'

it_beg=200
it_end=599
radtime_offset = it_beg*4 
    
if 0 :
    integrate_curl_b('longer2',it_beg,it_end,rmin=-1.e100,rmax=100.,thmin=thmin,thmax=thmax,tag='-abs3',dump_name=dump_name)
    integrate_curl_b_add_funcs('longer2',it_beg,it_end,rmin=-1.e100,rmax=100.,thmin=thmin,thmax=thmax,tag='-abs3',
                               dump_name=dump_name,rad_name=rad_name,radtime_offset=radtime_offset)
    for itime in np.arange(it_beg,it_end) :
        plot_integrated_fieldlines('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs3',dirout=dirout,dump_name=dump_name)

basedir='/mnt/lustre/scn/project1-prod-mhd-final-s0-longer2/dumps-high-cadence/series001/'
dump_name=basedir+'run000-dumps/KDHARM0'
rad_name=dump_name+'.RADFLUX'
dirout=basedir+'plots2/'

it_beg=325
it_end=799
#it_end=326
radtime_offset = it_beg*4 
    
if 0 :
    # integrate_curl_b('longer2',it_beg,it_end,rmin=-1.e100,rmax=100.,thmin=thmin,thmax=thmax,tag='-abs3',dump_name=dump_name)
    # integrate_curl_b_add_funcs('longer2',it_beg,it_end,rmin=-1.e100,rmax=100.,thmin=thmin,thmax=thmax,tag='-abs3',
    #                            dump_name=dump_name,rad_name=rad_name,radtime_offset=radtime_offset)
    integrate_curl_b_add_funcs2('longer2',it_beg,it_end,rmin=-1.e100,rmax=100.,thmin=thmin,thmax=thmax,tag='-abs3',
                                dump_name=dump_name,rad_name=rad_name,radtime_offset=radtime_offset)
    # for itime in np.arange(it_beg,it_end) :
    #     plot_integrated_fieldlines('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs3',dirout=dirout,dump_name=dump_name)

basedir='/mnt/lustre/scn/project1-prod-mhd-final-s0-longer2/dumps-high-cadence/series002/'
dump_name=basedir+'run000-dumps/KDHARM0'
rad_name=dump_name+'.RADFLUX'
dirout=basedir+'plots/'

it_beg=375
it_end=699

radtime_offset = it_beg*4 
    
if 0 :
    #integrate_curl_b('longer2',it_beg,it_end,rmin=-1.e100,rmax=100.,thmin=thmin,thmax=thmax,tag='-abs3',dump_name=dump_name)
    #integrate_curl_b_add_funcs('longer2',it_beg,it_end,rmin=-1.e100,rmax=100.,thmin=thmin,thmax=thmax,tag='-abs3',
    #                           dump_name=dump_name,rad_name=rad_name,radtime_offset=radtime_offset)
    integrate_curl_b_add_funcs2('longer2',it_beg,it_end,rmin=-1.e100,rmax=100.,thmin=thmin,thmax=thmax,tag='-abs3',
                               dump_name=dump_name,rad_name=rad_name,radtime_offset=radtime_offset)
    # for itime in np.arange(it_beg,it_end) :
    #     plot_integrated_fieldlines('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs3',dirout=dirout,dump_name=dump_name)

        
if 0 :
    thmin=0.5*np.pi - 0.2
    thmax=0.5*np.pi + 0.2
    for itime in np.arange(200,380) :
        plot_integrated_fieldlines('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs3',corotate=True)
    
 
if 0 :
    thmin=0.5*np.pi - 0.05
    thmax=0.5*np.pi + 0.05
    integrate_curl_b('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-abs1') 
    integrate_curl_b_add_funcs('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-abs1')
    thmin=0.5*np.pi - 0.1
    thmax=0.5*np.pi + 0.1
    integrate_curl_b('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-abs2') 
    integrate_curl_b_add_funcs('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-abs2') 
    thmin=0.5*np.pi - 0.2
    thmax=0.5*np.pi + 0.2
    integrate_curl_b('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-abs3') 
    integrate_curl_b_add_funcs('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-abs3') 
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    integrate_curl_b('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-abs4') 
    integrate_curl_b_add_funcs('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-abs4') 
 
if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    #integrate_curl_b('longer2',200,380,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax) 
    #integrate_curl_b_add_funcs('longer2',200,380,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax) 
    integrate_curl_b_add_funcs('longer2',200,379,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)

if 0 :
    thmin=0.5*np.pi - 0.05
    thmax=0.5*np.pi + 0.05
    integrate_curl_b('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-1') 
    integrate_curl_b('longer2',375,376,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-1') 
    integrate_curl_b('longer2',379,380,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-1') 

if 0 :
    thmin=0.5*np.pi - 0.1
    thmax=0.5*np.pi + 0.1
    integrate_curl_b('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-2') 
    integrate_curl_b('longer2',375,376,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-2') 
    integrate_curl_b('longer2',379,380,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-2') 

if 0 :
    thmin=0.5*np.pi - 0.2
    thmax=0.5*np.pi + 0.2
    integrate_curl_b('longer2',370,371,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-3') 
    integrate_curl_b('longer2',375,376,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-3') 
    integrate_curl_b('longer2',379,380,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax,tag='-3') 

if 0 :
    for itime in [370] : 
        plot_integrated_curl_b('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs1')
        plot_integrated_curl_b('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs2')
        plot_integrated_curl_b('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs3')
        plot_integrated_curl_b('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs4')

if 0 :
    for itime in [370] : 
        plot_integrated_fieldlines('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs1')
        plot_integrated_fieldlines('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs2')
        plot_integrated_fieldlines('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs3')
        plot_integrated_fieldlines('longer2',itime,rmin=-1.e100,rmax=100.,tag='-abs4')

if 0 :
    for itime in [370,375,379] : 
        plot_integrated_curl_b('longer2',itime,rmin=-1.e100,rmax=100.)
if 0 :
    #for itime in np.arange(370,371,1) : 
    #for itime in [370,375,379] : 
    for itime in [379] : 
        #plot_integrated_curl_b('longer2',itime,rmin=-1.e100,rmax=100.,tag='-1')
        plot_integrated_curl_b('longer2',itime,rmin=-1.e100,rmax=100.,tag='-3')

if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    for itime in np.arange(250,380,5) : 
        plot_corr2_integrated('longer2',itime,rmin=-1.e100,rmax=100.)

if 0 :
    for itime in np.arange(379,380,1) : 
        plot_corr_integrated('longer2',itime,rmin=-1.e100,rmax=100.)

        
if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    for itime in np.arange(379,380) : 
        plot_ratios_integrated_curl_b('longer2',itime,rmin=-1.e100,rmax=100.)


if 0 :
    tbeg = 5.5e4
    tend = tbeg + 50.
    plot_mdot('longer2',tbeg,tend)

    
if 0 :
    tbeg = -1.
    tend = 1e20
    runnames = ['longer2', 'medium_disk', 'bigger_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10','inject_disk']
    for runname in runnames : 
        plot_maxwell(runname,tbeg,tend)

if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    runnames = ['medium_disk']
    for runname in runnames :
        indlist = find_indices_of_dumps(runname)
        ibeg = int(np.min(indlist))
        iend = int(np.max(indlist))+1
        integrate_curl_b_add_funcs(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)

if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    runnames = ['bigger_disk']
    for runname in runnames :
        indlist = find_indices_of_dumps(runname)
        ibeg = int(np.min(indlist))
        iend = int(np.max(indlist))+1
        integrate_curl_b(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)
        integrate_curl_b_add_funcs(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)

        
if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    runnames = [ 'proj1_q2']
    for runname in runnames :
        indlist = find_indices_of_dumps(runname)
        ibeg = int(np.min(indlist))
        iend = int(np.max(indlist))+1
        integrate_curl_b_add_funcs(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)

if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    runnames = ['proj1_q5']
    for runname in runnames :
        indlist = find_indices_of_dumps(runname)
        ibeg = int(np.min(indlist))
        iend = int(np.max(indlist))+1
        integrate_curl_b(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)
        integrate_curl_b_add_funcs(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)

        
if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    runnames = [ 'proj1_q10']
    for runname in runnames :
        indlist = find_indices_of_dumps(runname)
        ibeg = int(np.min(indlist))
        iend = int(np.max(indlist))+1
        integrate_curl_b_add_funcs(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)

if 0 :
    thmin=0.5*np.pi - 0.3
    thmax=0.5*np.pi + 0.3
    runnames = [ 'inject_disk']    
    for runname in runnames :
        indlist = find_indices_of_dumps(runname)
        ibeg = int(np.min(indlist))
        iend = int(np.max(indlist))+1
        integrate_curl_b(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)
        integrate_curl_b_add_funcs(runname,ibeg,iend,rmin=-1.e100,rmax=100., thmin=thmin,thmax=thmax)


if 0 :
    test_corr()
        


if 0 :
    #plot_history('longer2',    'surfdens',total=True,dolog=True, t_max=33.e4,minf=-3,maxf=0.,r_max=100.)
    #plot_history('bigger_disk','surfdens',total=True,dolog=True, t_max=33.e4,minf=-3,maxf=0.,r_max=100.)
    #plot_history('medium_disk','surfdens',total=True,dolog=True, t_max=33.e4,minf=-3,maxf=0.,r_max=100.)
    #plot_history('longer2',    'surfdens',total=True,dolog=False,t_max=33.e4,maxf=1.,minf=1e-3,r_max=100.)
    #plot_history('bigger_disk','surfdens',total=True,dolog=False,t_max=33.e4,maxf=1.,minf=1e-3,r_max=100.)
    #plot_history('medium_disk','surfdens',total=True,dolog=False,t_max=33.e4,maxf=1.,minf=1e-3,r_max=100.)
    #plot_history('longer2',    'surfdens',total=True,dolog=False,t_max=33.e4,maxf=1.,minf=1e-3,tag='-fulldomain')
    #plot_history('bigger_disk','surfdens',total=True,dolog=False,t_max=33.e4,maxf=1.,minf=1e-3,tag='-fulldomain')
    #plot_history('medium_disk','surfdens',total=True,dolog=False,t_max=33.e4,maxf=1.,minf=1e-3,tag='-fulldomain')
    plot_history('longer2',    'surfdens',total=True,dolog=True,t_max=33.e4,maxf=0.,minf=-3,tag='-fulldomain')
    plot_history('bigger_disk','surfdens',total=True,dolog=True,t_max=33.e4,maxf=0.,minf=-3,tag='-fulldomain')
    plot_history('medium_disk','surfdens',total=True,dolog=True,t_max=33.e4,maxf=0.,minf=-3,tag='-fulldomain')

if 0 :
    runnames = ['longer2', 'medium_disk', 'bigger_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10','inject_disk']
    for run in runnames : 
        plot_history(run,'surfdens',total=True,dolog=False,t_max=33.e4,maxf=1.,minf=1e-3,tag='-fulldomain')
        plot_history(run,'surfdens',total=True,dolog=False,t_max=33.e4,maxf=1.,minf=1e-3,r_max=100.)
        plot_history(run,'surfdens',total=True,dolog=True, t_max=33.e4,maxf=0.,minf=-3,tag='-fulldomain')
        plot_history(run,'surfdens',total=True,dolog=True, t_max=33.e4,maxf=0.,minf=-3,r_max=100.)
    
    
if 0 :
    runnames = ['medium_disk', 'bigger_disk']
    ntype = 'default--Sigma0'
    for run in runnames : 
        plot_history(run,'Lut',bound=True,normalization_type=ntype,dolog=True,t_max=33.e4,maxf=0.,minf=-10,tag='-fulldomain')
        plot_history(run,'Lut',bound=True,normalization_type=ntype,dolog=True,t_max=33.e4,maxf=0.,minf=-10,r_max=100.)
        #plot_history(run,'Lut',bound=True,normalization_type=ntype,dolog=True, t_max=33.e4,maxf=0.,minf=-3,  tag='-fulldomain')
        #plot_history(run,'Lut',bound=True,normalization_type=ntype,dolog=True, t_max=33.e4,maxf=0.,minf=-3,  r_max=100.)
    

#TODO:
#
#  Mdot(t) (all on one? ) 
#  Mdot(r) over different periods (over RunSE SE period, over lump period, over sequence of periods)
#  Menc(r)
#  Sigma(r,t) 
#

if 0 :
    runnames = ['longer2', 'medium_disk', 'bigger_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10','inject_disk']
    pnames = ['dirname']
    for runname in runnames : 
        sim_info = get_sim_info(pnames, runname=runname)
        dirout = sim_info['dirname']+'paper/'
        dirtex = '/mnt/lustre/scn/home/tex/mass-ratio-paper/'+runname+'/'
        if( not os.path.exists(dirout) ) : 
            os.mkdir(dirout)
        #shutil.move(dirtex+'lcurve.h5',dirout)
        os.listdir(dirout)
        os.symlink(dirout+'lcurve.h5',dirtex+'lcurve.h5')

###########################################


runnames = ['longer2', 'medium_disk', 'bigger_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10','inject_disk']
if 0: 
    make_all_mdot(runnames)
        
if 0: 
    make_all_lightcurves(runnames)
        
if 0: 
    make_all_massenc(runnames)
        
if 0: 
    plot_all_mdot(runnames,series=1)
    plot_all_mdot(runnames,series=2)
        
if 0: 
    plot_all_lightcurves(runnames,series=1)
    plot_all_lightcurves(runnames,series=2)
        
if 0: 
    plot_all_mdot_vs_r(runnames,series=1)
    plot_all_mdot_vs_r(runnames,series=2)
        
if 0: 
    plot_all_massenc(runnames,series=1)
    plot_all_massenc(runnames,series=2)


####################################

if 0: 
    make_all_spacetime_surfdens(runnames)

if 0: 
    plot_all_spacetime_surfdens(runnames,series=1)
    plot_all_spacetime_surfdens(runnames,series=2)
        
if 0 : 
    plot_all_slices_surfdens(runnames,series=1)
    plot_all_slices_surfdens(runnames,series=2)
    
####################################
if 0 :
    #runnames2 = ['inject_disk']
    make_all_snapshots_surfdens(runnames)

if 0: 
    plot_all_snapshots_surfdens(runnames,series=1) 
    plot_all_snapshots_surfdens(runnames,series=2)

###############################################
if 0 :
    make_all_spacetime_beta(runnames) 
    
if 0: 
    plot_all_spacetime_beta(runnames,series=1)
    plot_all_spacetime_beta(runnames,series=2)

if 0 :
    make_all_aziavgs_beta(runnames)
    
if 0 :
    make_all_aziavgs_beta2(runnames)
    

if 0:  #  <beta>
    plot_all_aziavgs_beta(runnames,tag2='2',series=1)
    plot_all_aziavgs_beta(runnames,tag2='2',series=2)

if 0:  # <pgas>/<pmag>
    plot_all_aziavgs_beta(runnames,series=1,func_lims=[-3.,3.])
    plot_all_aziavgs_beta(runnames,series=2,func_lims=[-3.,3.])

###############################################

if 0 :
    runnamestmp = [ 'bigger_disk']
    #make_all_spacetime_stresses(runnames)
    make_all_spacetime_stresses(runnamestmp)

if 0 :
    plot_all_spacetime_stresses(runnames,series=1)
    plot_all_spacetime_stresses(runnames,series=2)

if 0 :
    #plot_all_stress_avgs(['longer2','inject_disk'],series=2)
    plot_all_stress_avgs(runnames,series=1)
    plot_all_stress_avgs(runnames,series=2)


###############################################

if 0 :
    plot_all_ecc(runnames,series=1)
    plot_all_ecc(runnames,series=2)

if 0 :
    plot_all_ecc2(runnames,series=1)
    plot_all_ecc2(runnames,series=2)

if 0 :
    plot_all_ecc_vs_r(runnames,series=1)
    plot_all_ecc_vs_r(runnames,series=2)

###############################################

if 0 :
    plot_all_spacetime_lumpratio(runnames,series=1)
    plot_all_spacetime_lumpratio(runnames,series=2)

if 0 :
    plot_all_lumpcrit(runnames,series=1)
    plot_all_lumpcrit(runnames,series=2)

###############################################

if 0 :
    #runnames = ['medium_disk', 'bigger_disk', 'proj1_q2', 'proj1_q5', 'proj1_q10','inject_disk']
    #runnames = ['longer2']
    make_all_mri_avgs(runnames)

###############################################

if 0 :
    funcnames = ['Qmri1_avg2','Qmri2_avg2','Qmri3_avg2','Qmri1_avg3','Qmri2_avg3','Qmri3_avg3']
    for func in funcnames : 
        plot_all_mri_avgs(runnames,series=1,funcname=func)
        plot_all_mri_avgs(runnames,series=2,funcname=func)

    
###############################################

if 0 :
    #runnames = ['longer2', 'medium_disk', 'bigger_disk']
    #runnames = ['proj1_q2', 'proj1_q5', 'proj1_q10','inject_disk']
    runnames = ['longer2']
    isamples0=np.arange(134, 380)
    make_all_mri_avgs_alltimes(runnames,isamples0=isamples0)


###############################################

if 0 :
    plot_all_timeseries(runnames,series=1)
    plot_all_timeseries(runnames,series=2)
    
if 1 :
    plot_all_timeseries2(runnames,series=1)
    plot_all_timeseries2(runnames,series=2)
    
if 1 :
    plot_all_timeseries3(runnames,series=1)
    plot_all_timeseries3(runnames,series=2)
    

###############################################

if 0 :
    plot_all_omegalump(runnames,series=1)
    plot_all_omegalump(runnames,series=2)
    
###############################################

if 0 :
    plot_all_spacetime_maxstress(runnames,series=1)
    plot_all_spacetime_maxstress(runnames,series=2)
    
############################################################
# USER NOTES:
# -----------
#    -- when there is a new run that you want to perform lump analysis for, do the following: 
#
#    1) Run make_mode_power_dataset(funcname=func, runname=run) to
#    make "mode_analysis.h5"
#
#    2) Run make_mode_power_spacetime_dataset(funcname=func,
#    runname=run) to make 'mode_spacetime_analysis.h5'
#
#    3) Run find_tlump() to collect criterion crossing times, then
#    select/reject times according to definitions in get_sim_info.py
#    and then assign the various "t_lump*" variables for the new
#    simulation's entry in get_sim_info.py;
#
#
#
############################################################
