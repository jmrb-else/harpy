"""  

  geo.py: 
  ------------
    -- display geodesic trajectory data from bothros's "write_geo_data()" 

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import string 
#import h5py

from astropy.io import ascii

from bothros import *

# from IPython.core.debugger import Tracer


###############################################################################
###############################################################################
###############################################################################
def plot_geo(geofile='geo-000000.dat',dirout='./plots/'):

    # def lum(runname='longer2',savepng=0):
    """  
    Primary routine for analyzing light curve. 
    """

    showplots=0
    savepng=1
    nwin=0
    dolog=0
    
    nlev = 256
    nticks=10
    
    cmap  = plt.cm.Spectral_r
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 
    
    if not showplots :
        plt.ioff()
    
    
    header_data, geodata = read_geo_data(geofile)
    
    rsize = 5000.
    
    nwin+=1
    fignames = [dirout + 'xy' + geofile]
    print( "fignames = ", fignames)
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    #ax.set_title(r'x-y plane')
    ax.set_xlabel(r'x [M]')
    ax.set_ylabel(r'y [M]')
    plt.axis([-rsize,rsize,-rsize,rsize])
    ax.plot(geodata['xcon1'],geodata['xcon2'])
    
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'yz' + geofile))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'y [M]')
    ax.set_ylabel(r'z [M]')
    plt.axis([-rsize,rsize,-rsize,rsize])
    ax.plot(geodata['xcon2'],geodata['xcon3'])
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'xz' + geofile))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'x [M]')
    ax.set_ylabel(r'z [M]')
    plt.axis([-rsize,rsize,-rsize,rsize])
    ax.plot(geodata['xcon1'],geodata['xcon3'])
    
    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)
            
    if showplots : 
        plt.show()
            
            
    print("All done ")
    sys.stdout.flush()
    return


###############################################################################
###############################################################################
###############################################################################
def plot_all_geo(geofileglob='geo*.dat',dirout='./'):

    # def lum(runname='longer2',savepng=0):
    """  
    Primary routine for analyzing light curve. 
    """

    showplots=0
    savepng=1
    nwin=0
    dolog=0
    
    nlev = 256
    nticks=10
    
    cmap  = plt.cm.Spectral_r
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 
    
    if not showplots :
        plt.ioff()
    
    rsize = 50.
    
    files = sorted(glob.glob(geofileglob))

    nwin+=1
    fignames = [dirout + 'xy'+'-all']
    print( "fignames = ", fignames)
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    #ax.set_title(r'x-y plane')
    ax.set_xlabel(r'x [M]')
    ax.set_ylabel(r'y [M]')
    ax.set_aspect('equal')
    plt.axis([-rsize,rsize,-rsize,rsize])
    for geofile in files :
        header_data, geodata = read_geo_data(geofile)
        ax.plot(geodata['xcon1'],geodata['xcon2'])
    
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'yz'+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'y [M]')
    ax.set_ylabel(r'z [M]')
    ax.set_aspect('equal')
    plt.axis([-rsize,rsize,-rsize,rsize])
    for geofile in files :
        header_data, geodata = read_geo_data(geofile)
        ax.plot(geodata['xcon2'],geodata['xcon3'])
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'xz'+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'x [M]')
    ax.set_ylabel(r'z [M]')
    ax.set_aspect('equal')
    plt.axis([-rsize,rsize,-rsize,rsize])
    for geofile in files :
        header_data, geodata = read_geo_data(geofile)
        ax.plot(geodata['xcon1'],geodata['xcon3'])
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'ph'+'-all'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'r [M]')
    ax.set_ylabel(r'ph [rad.]')
    #ax.set_aspect('equal')
    plt.axis([1e-10,rsize,-2.*np.pi,2.*np.pi])
    for geofile in files :
        header_data, geodata = read_geo_data(geofile)
        val = np.arctan2(geodata['xcon2'],geodata['xcon1'])
        valr = np.sqrt( geodata['xcon1']*geodata['xcon1'] + geodata['xcon2']*geodata['xcon2'] + geodata['xcon3']*geodata['xcon3'] )
        ax.plot(valr,val)
    
    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)
            
    if showplots : 
        plt.show()
            
            
    print("All done ")
    sys.stdout.flush()
    return


#  

#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  

files = sorted(glob.glob("geo-*.dat"))

# for file in files :
#     plot_geo(geofile=file)


plot_all_geo(geofileglob="geo-*.dat")

    
