###############################################################################
###############################################################################
#  Routines useful for making plots
###############################################################################
###############################################################################

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap,ListedColormap
from matplotlib import cm
from matplotlib.ticker import FormatStrFormatter, NullFormatter, MultipleLocator, MaxNLocator,ScalarFormatter
import matplotlib.pylab as pylab
from cycler import cycler
import numpy as np
import os,sys
import glob
from astropy.io import ascii


###############################################################################
###############################################################################
#  Reads in a PPM file containing RGB values of a color map, and returns a matplotlib color map.
###############################################################################

def read_ppm_rgbs(ppm_file,
                  ncolors=256,
                  alpha=1.
                 ):


    # Get the "bit" size of the color map

    rgbs = np.loadtxt(ppm_file,skiprows=3)
    ncols = len(rgbs)
    col_max = np.float(ncols-1)

    #print("rgbs = ", rgbs)

    max_rgb = np.max(rgbs)

    if( max_rgb > col_max ) :
        sys.exit("read_ppm_rgbs():  problem with scale of ppm color map." + ppm_file)

    if( ncolors < 2 ) :
        sys.exit("read_ppm_rgbs():  silly value of ncolors = ", ncolors)
        
    if( ncols < 2 ) :
        sys.exit("read_ppm_rgbs():  silly value of ncols = ", ncols)
        

    # Normalize RGB values to bit size, since matplotlib expects values over [0.,1.]
    rescale_rgb = rgbs / col_max

    # Store in matplotlib array
    new_rgb = np.ndarray((ncolors,4),dtype=float)
    if( ncolors != ncols ) :
        xorig = np.linspace(0.,1.,ncols)
        xnew  = np.linspace(0.,1.,ncolors)
        for icolor in [0,1,2] :
            new_rgb[:,icolor] = np.interp(xnew,xorig,rescale_rgb[:,icolor])

    else:
        new_rgb[:,0:3] = rescale_rgb[:,0:3]

    new_rgb[:,3] = alpha

    newcmp = ListedColormap(new_rgb)

    return newcmp


###############################################################################
###############################################################################
#  Returns with John Hawley's color map, as specified by a PPM file he gave
#  me a long time ago. 
###############################################################################
def get_hawley_cmap(filename=None) :
    from os.path import expanduser
    home = expanduser("~")
    
    if(filename is None) :
        cmap = read_ppm_rgbs(home+"/python/home/john.ppm")
    else :
        cmap = read_ppm_rgbs(filename)

    return cmap


###############################################################################
def test_hawley_cmap():
    
    cmap = get_hawley_cmap()
    cmap2 = read_ppm_rgbs("john.ppm",ncolors=512)

    np.random.seed(19680801)
    data = np.random.randn(30, 30)
    fig, axs = plt.subplots(1, 2, figsize=(6, 3), constrained_layout=True)
    psm = axs[0].pcolormesh(data, cmap=cmap, rasterized=True, vmin=-4, vmax=4)
    fig.colorbar(psm, ax=axs[0])
    
    psm2 = axs[1].pcolormesh(data, cmap=cmap2, rasterized=True, vmin=-4, vmax=4)
    fig.colorbar(psm2, ax=axs[1])
    plt.show()

    return



################################################################################
# Setup style for plot that has many curves
################################################################################
def set_many_curves_in_one_plot_style(nfuncs,ntags=1):

    mycolors = np.array(['black','orange','green','blue','red','cyan','yellow','brown']).astype(str)
    if( nfuncs > len(mycolors) ):
        sys.exit("need to increase the number of colors in mycolors")

    mylines = np.array(['-','--','-.']).astype(str)
    if( ntags> len(mylines) ):
        sys.exit("need to increase the number of linestyels ")

    lw_cyc = np.outer(np.linspace(0.5,2.5,nfuncs),np.ones(ntags))
    alpha_cyc = np.outer(np.linspace(1.,0.3,nfuncs),np.ones(ntags))

    # Note arrays of strings cannot use numpy.outer for some bullshit reason : 
    mycolors_cyc = []
    mylines_cyc = []
    for itag in np.arange(ntags) : 
        mycolors_cyc = np.append(mycolors_cyc, mycolors[0:nfuncs])
        for ifunc in np.arange(nfuncs) : 
            mylines_cyc  = np.append(mylines_cyc,mylines[itag])


    #new_cycler = (  cycler(color=mycolors[0:nfuncs]) +
    #                cycler(lw=np.linspace(0.5,2.5,nfuncs)) +
    #                cycler(alpha=np.linspace(1.,0.3,nfuncs)) +
    #                cycler(ls=mylines_cyc) )

    new_cycler = (  cycler(color=mycolors_cyc) +
                    cycler(lw=lw_cyc.flat) +
                    cycler(alpha=alpha_cyc.flat) +
                    cycler(ls=mylines_cyc) )
    
    params = {'figure.figsize': (6, 4),
              'axes.labelsize':  16,
              'axes.titlesize':  16,
              'xtick.labelsize': 16,
              'ytick.labelsize': 16,
              'legend.fontsize': 14,
              'legend.borderpad': 0.2,
              'legend.labelspacing': 0.2,
              'legend.handlelength': 1.5,
              'legend.handletextpad': 0.2,
              'legend.fancybox': False,
              'axes.prop_cycle':new_cycler,
    }
    
    ymajorFormatter = FormatStrFormatter('%1.1f')
    xmajorFormatter = FormatStrFormatter('%2.0f')

    pylab.rcParams.update(params)
    
    return xmajorFormatter, ymajorFormatter

    

################################################################################
# Setup style for plot that has many curves
################################################################################
def set_many_curves_in_one_plot_style2(nfuncs,ntags=1):

    mycolors = np.array(['black','orange','green','blue','red','cyan','yellow','brown']).astype(str)
    if( nfuncs > len(mycolors) ):
        sys.exit("need to increase the number of colors in mycolors")

    mylines = np.array(['-','-','-','-']).astype(str)
    if( ntags> len(mylines) ):
        sys.exit("need to increase the number of linestyels ")

    lw_cyc = np.outer(np.linspace(0.5,2.5,nfuncs),np.ones(ntags))
    alpha_cyc = np.outer(np.linspace(1.,0.3,nfuncs),np.ones(ntags))

    # Note arrays of strings cannot use numpy.outer for some bullshit reason : 
    mycolors_cyc = []
    mylines_cyc = []
    for itag in np.arange(ntags) : 
        mycolors_cyc = np.append(mycolors_cyc, mycolors[0:nfuncs])
        for ifunc in np.arange(nfuncs) : 
            mylines_cyc  = np.append(mylines_cyc,mylines[itag])


    #new_cycler = (  cycler(color=mycolors[0:nfuncs]) +
    #                cycler(lw=np.linspace(0.5,2.5,nfuncs)) +
    #                cycler(alpha=np.linspace(1.,0.3,nfuncs)) +
    #                cycler(ls=mylines_cyc) )

    new_cycler = (  cycler(color=mycolors_cyc) +
                    cycler(lw=lw_cyc.flat) +
                    cycler(alpha=alpha_cyc.flat) +
                    cycler(ls=mylines_cyc) )
    
    params = {'figure.figsize': (6, 4),
              'axes.labelsize':  16,
              'axes.titlesize':  16,
              'xtick.labelsize': 16,
              'ytick.labelsize': 16,
              'legend.fontsize': 14,
              'legend.borderpad': 0.2,
              'legend.labelspacing': 0.2,
              'legend.handlelength': 1.5,
              'legend.handletextpad': 0.2,
              'legend.fancybox': False,
              'axes.prop_cycle':new_cycler,
    }
    
    ymajorFormatter = FormatStrFormatter('%1.1f')
    xmajorFormatter = FormatStrFormatter('%2.0f')

    pylab.rcParams.update(params)
    
    return xmajorFormatter, ymajorFormatter

    

################################################################################
# Setup style for plot that has four contour plots in a row
################################################################################
def set_contour_plots_in_row_plot_style():

    params = {'figure.figsize': (8, 4),
              'axes.labelsize':  8,
              'axes.titlesize':  8,
              'xtick.labelsize': 8,
              'ytick.labelsize': 8,
              'legend.fontsize': 8,
              'legend.borderpad': 0.2,
              'legend.labelspacing': 0.2,
              'legend.handlelength': 1.5,
              'legend.handletextpad': 0.2,
              'legend.fancybox': False,
    }
    
    ymajorFormatter = FormatStrFormatter('%1.0f')
    xmajorFormatter = FormatStrFormatter('%1.0f')

    pylab.rcParams.update(params)
    
    return xmajorFormatter, ymajorFormatter


################################################################################
# Setup style for plot that has four contour plots in a row
################################################################################
def set_contour_plots_in_grid_style(nrows,ncols):

    xsize = 6.
    ysize = 0.775*(xsize*ncols)/nrows
    params = {'figure.figsize': (xsize, ysize),
              'axes.labelsize':  12,
              'axes.titlesize':  12,
              'xtick.labelsize': 12,
              'ytick.labelsize': 12,
              'legend.fontsize': 12,
              'legend.borderpad': 0.2,
              'legend.labelspacing': 0.2,
              'legend.handlelength': 1.5,
              'legend.handletextpad': 0.2,
              'legend.fancybox': False,
    }
    
    ymajorFormatter = FormatStrFormatter('%1.0f')
    xmajorFormatter = FormatStrFormatter('%1.0f')

    pylab.rcParams.update(params)
    
    return xmajorFormatter, ymajorFormatter


################################################################################
# Setup style for plot that has four contour plots in a row
################################################################################
def set_linear_plots_in_row_plot_style(nfuncs):

    xmajorFormatter, ymajorFormatter = set_contour_plots_in_row_plot_style()

    mycolors = ['black','orange','green','blue','red','cyan','black','yellow','brown']
    mylines = ['--','-','-','-','-','-','-','-']
    if( nfuncs > len(mycolors) ):
        sys.exit("need to increase the number of colors in mycolors")
        
    new_cycler = (  cycler(color=mycolors[0:nfuncs]) +
                    cycler(lw=np.linspace(0.5,2.5,nfuncs)) +
                    cycler(alpha=np.linspace(1.,0.3,nfuncs)) +
                    cycler(ls=mylines[0:nfuncs]) )
    
    params = {'figure.figsize': (18, 4),
              'axes.labelsize':  16,
              'axes.titlesize':  16,
              'xtick.labelsize': 16,
              'ytick.labelsize': 16,
              'legend.fontsize': 14,
              'legend.borderpad': 0.2,
              'legend.labelspacing': 0.2,
              'legend.handlelength': 1.5,
              'legend.handletextpad': 0.2,
              'legend.fancybox': False,
              'axes.prop_cycle':new_cycler,
    }
    
    ymajorFormatter = FormatStrFormatter('%1.1f')
    pylab.rcParams.update(params)
    
    return xmajorFormatter, ymajorFormatter


################################################################################
# Setup style for plot that has four contour plots in a row
#  -- version without the dashed line
################################################################################
def set_linear_plots_in_row_plot_style2(nfuncs):

    xmajorFormatter, ymajorFormatter = set_contour_plots_in_row_plot_style()

    mycolors = ['black','orange','green','blue','red','cyan','black','yellow','brown']
    mylines = ['-','-','-','-','-','-','-','-','-']
    if( nfuncs > len(mycolors) ):
        sys.exit("need to increase the number of colors in mycolors")
        
    new_cycler = (  cycler(color=mycolors[0:nfuncs]) +
                    cycler(lw=np.linspace(0.5,2.5,nfuncs)) +
                    cycler(alpha=np.linspace(1.,0.3,nfuncs)) +
                    cycler(ls=mylines[0:nfuncs]) )
    
    params = {'figure.figsize': (18, 4),
              'axes.labelsize':  16,
              'axes.titlesize':  16,
              'xtick.labelsize': 16,
              'ytick.labelsize': 16,
              'legend.fontsize': 14,
              'legend.borderpad': 0.2,
              'legend.labelspacing': 0.2,
              'legend.handlelength': 1.5,
              'legend.handletextpad': 0.2,
              'legend.fancybox': False,
              'axes.prop_cycle':new_cycler,
    }
    
    ymajorFormatter = FormatStrFormatter('%2.1f')
    pylab.rcParams.update(params)
    
    return xmajorFormatter, ymajorFormatter


################################################################################
# Setup style for plot that has four contour plots in a row
#  -- version without the dashed line
################################################################################
def set_linear_plots_in_row_plot_style3(nfuncs):

    xmajorFormatter, ymajorFormatter = set_contour_plots_in_row_plot_style()

    mycolors = ['black','orange','green','blue','red','cyan','black','yellow','brown']
    mylines = ['-','-','-','-','-','-','-','-','-']
    mylines2 = ['--','--','--','--','--','--','--','--','--']
    if( nfuncs > len(mycolors) ):
        sys.exit("need to increase the number of colors in mycolors")

    colors = np.append(mycolors[0:nfuncs],mycolors[0:nfuncs])
    lws = np.append(np.linspace(0.5,2.5,nfuncs),np.linspace(0.5,2.5,nfuncs))
    alphas = np.append(np.linspace(1.,0.3,nfuncs),np.linspace(1.,0.3,nfuncs))
    lss = np.append(mylines[0:nfuncs],mylines2[0:nfuncs])
        
    new_cycler = (  cycler(color=colors) +
                    cycler(lw=lws) +
                    cycler(alpha=alphas) +
                    cycler(ls=lss) )
    
    params = {'figure.figsize': (18, 4),
              'axes.labelsize':  16,
              'axes.titlesize':  16,
              'xtick.labelsize': 16,
              'ytick.labelsize': 16,
              'legend.fontsize': 14,
              'legend.borderpad': 0.2,
              'legend.labelspacing': 0.2,
              'legend.handlelength': 1.5,
              'legend.handletextpad': 0.2,
              'legend.fancybox': False,
              'axes.prop_cycle':new_cycler,
    }
    
    ymajorFormatter = FormatStrFormatter('%2.2f')
    pylab.rcParams.update(params)
    
    return xmajorFormatter, ymajorFormatter


################################################################################
# Setup style for plot that has four contour plots in a row
#  -- version without the dashed line
################################################################################
def set_linear_plots_in_row_plot_style4(nfuncs):

    xmajorFormatter, ymajorFormatter = set_contour_plots_in_row_plot_style()

    #mycolors = ['black','cyan','yellow','blue','orange','green','red','brown']
    mycolors = ['black','blue','orange','green','red','cyan','yellow','brown']
    mylines = ['-','-','-','-','-','-','-','-','-']
    if( nfuncs > len(mycolors) ):
        sys.exit("need to increase the number of colors in mycolors")
        
    new_cycler = (  cycler(color=mycolors[0:nfuncs]) +
                    cycler(lw=np.linspace(2.5,0.5,nfuncs)) +
                    cycler(alpha=np.linspace(0.25,1.0,nfuncs)) +
                    cycler(ls=mylines[0:nfuncs]) )
    
    params = {'figure.figsize': (18, 6),
              'axes.labelsize':  16,
              'axes.titlesize':  16,
              'xtick.labelsize': 16,
              'ytick.labelsize': 16,
              'legend.fontsize': 14,
              'legend.borderpad': 0.2,
              'legend.labelspacing': 0.2,
              'legend.handlelength': 1.5,
              'legend.handletextpad': 0.2,
              'legend.fancybox': False,
              'axes.prop_cycle':new_cycler,
    }
    
    ymajorFormatter = FormatStrFormatter('%2.1f')
    xmajorFormatter = ScalarFormatter()
    pylab.rcParams.update(params)
    
    return xmajorFormatter, ymajorFormatter


################################################################################
# Make coordinates for pcolormesh routine from cell-centered coordinates: 
################################################################################
def make_pcolormesh_coordinates(xx):

    var = np.abs(np.std(np.gradient(xx)/np.max(xx)))
    xx0 = np.append(xx,(2.*xx[-1]-xx[-2]))
    if( var > 1e-10 ) :
        xx1 = np.append((2.*xx[0]-xx[1]),xx)
        xxout = np.sqrt(np.abs(xx0*xx1))  # need to take geometric mean for coordinates that follow exponetial offsets
    else :
        xxout = xx0 - 0.5*(xx0[1]-xx0[0])

    return xxout
