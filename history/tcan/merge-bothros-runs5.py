"""

  merge-bothros-runs4.py: 
  ------------

    -- merges a set of bothros files, each at a single time and
       frequency but different "subframes" or sets of pixels per file.
    -- merges all the subframes into one frame; 

    -- should be able to merge subframes dividing the frame in either
       dimension, i.e. for the case where nx_frame and ny_frame are
       both greater than 1.

    -- now able to merge a set of frames with different numbers of iy pixels; 

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

import string as str

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################
#
#  Data formats of the source data files:
#
# funcs_to_copy: 
#    Header                   Group
# DBL  units                    Dataset {1, 13}
# DBL  freq                     Dataset {1}
# INT  snap_id                  Dataset {1}
# DBL  times                    Dataset {1}
#
# other_funcs to merge:
# DBL  I_image                  Dataset {1, 40, 80}
# DBL  I_image2                 Dataset {1, 40, 80}
# INT  end_state                Dataset {1, 40, 80}
# INT  id                       Dataset {1, 40, 80}
# INT  n_geod                   Dataset {1, 40, 80}
# DBL  ph_infinity              Dataset {1, 40, 80}
# DBL  pix_area                 Dataset {1, 40, 80}
# DBL  pix_x                    Dataset {1, 40, 80}
# DBL  pix_y                    Dataset {1, 40, 80}
# DBL  redshift_infinity        Dataset {1, 40, 80}
# DBL  th_infinity              Dataset {1, 40, 80}
#
#
#
##################################################################################################


import sys
print(sys.argv)
if len(sys.argv) > 1 :
    prefix = sys.argv[1]
else : 
    prefix = "light_curve"

    
# There is probably a nice way of doing this automatically using glob() and string parsing: 
#ntimes = 182
ntimes = 1

ntot = ntimes


files = sorted(glob.glob(prefix+"*.h5"))
nfiles = len(files)
#print("files=", files)

# file=files[0]
# file_parts = file.split(prefix)
# time_str = file_parts[2]
# time_strings = file_parts[2].split('.')
# time_str = time_strings[0]
# itime = int(time_str)
# print(" itime ifreq = ", itime)


outfile=prefix+".h5"
    
funcs_to_copy = ['Header', 'units', 'freq', 'snap_id', 'times']
n_funcs_to_copy = len(funcs_to_copy)

other_funcs = ['end_state','id','n_geod','pix_area','pix_x', 'pix_y','ph_infinity','th_infinity','redshift_infinity']
n_other_funcs = len(other_funcs)


#######################################################################
# Copy over those datasets that are independent of time and frequency:
#######################################################################
h5in  = h5py.File(files[0],'r')
h5out  = h5py.File(outfile,'w-')


for func in funcs_to_copy :
    # print("doing func = ", func)
    # print(" func = ", func )
    h5in.copy(func,h5out)

#######################################################################
# Read in the subframe decomposition and compare with the number of files
# given to make sure the merging operation makes sense. 
#######################################################################
nx = h5in['Header/nx_frame'].value[0]
ny = h5in['Header/ny_frame'].value[0]
nx_pix_tot = h5in['Header/nxi'].value[0]
ny_pix_tot = h5in['Header/npsi'].value[0]

n_subframes = nx*ny

if( n_subframes != nfiles ) :
    print("Mismatch between the number of frames and the number of files: ", n_subframes, nfiles)
    h5in.close()
    h5out.close()
    sys.exit('Exitting!!')

    
#######################################################################
# Create the datasets in the output file, from array size information
# stored in the header:
#######################################################################
shape2 = [ntimes,ny_pix_tot,nx_pix_tot]
for func in other_funcs :
    dset = h5out.create_dataset(func,shape2,dtype=h5in[func].dtype)


#print("nx/ny_pix = ", nx_pix, ny_pix)

h5in.close()


#######################################################################
# Now read in each source data set and save it in the correct slot of
# the destination/merged data set.
#
#  -- assume for now that every input hdf5 file will only have one
#     entry;
#
#  -- each input file will have the filename format :
#        light_curve_${itime}_${ifreq}.h5
#
#######################################################################


for file in files :
    print("Processing  file = ", file)

    h5in  = h5py.File(file,'r')

    i_psi_beg = h5in['Header/i_psi_beg'].value[0]
    i_psi_end = h5in['Header/i_psi_end'].value[0]
    i_xi_beg  = h5in['Header/i_xi_beg'].value[0]
    i_xi_end  = h5in['Header/i_xi_end'].value[0]

    for func in other_funcs :
        #print("....Merging func = ", func)
        ndim1 = len(h5in[func].shape)
        h5out[func][0,i_psi_beg:i_psi_end,i_xi_beg:i_xi_end] = h5in[func][0,:,:]

    h5in.close()
            

h5out.close()

print("Done with all files !")


#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
