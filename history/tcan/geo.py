"""  

  geo.py: 
  ------------
    -- display geodesic trajectory data from bothros's "write_geo_data()" 

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import string 
#import h5py

from astropy.io import ascii

from bothros import *

# from IPython.core.debugger import Tracer


###############################################################################
###############################################################################
###############################################################################

# def lum(runname='longer2',savepng=0):
"""  
Primary routine for analyzing light curve. 
"""

showplots=0
savepng=1
nwin=0
dolog=0

nlev = 256
nticks=10

cmap  = plt.cm.Spectral_r
darkgrey = '#202020'
purple   = '#800080'
darkblue = '#0000A0'
Colormap.set_under(cmap,color=darkgrey) 
Colormap.set_over(cmap,color='w') 

if not showplots :
    plt.ioff()


# dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
dirout ='./'

geofile='georay-dn-4040.dat'

header_data, geodata = read_geo_data(geofile)

rsize = 50.

nwin+=1
fignames = [dirout + 'xy' + geofile]
print( "fignames = ", fignames)
figs = [plt.figure(nwin)]
figs[-1].clf()
ax = figs[-1].add_subplot(111)
#ax.set_title(r'x-y plane')
ax.set_xlabel(r'x [M]')
ax.set_ylabel(r'y [M]')
plt.axis([-rsize,rsize,-rsize,rsize])
ax.plot(geodata['xcon1'],geodata['xcon2'])


nwin+=1
fignames = np.append(fignames,(dirout + 'yz' + geofile))
figs = np.append(figs,plt.figure(nwin))
figs[-1].clf()
ax = figs[-1].add_subplot(111)
ax.set_xlabel(r'y [M]')
ax.set_ylabel(r'z [M]')
plt.axis([-rsize,rsize,-rsize,rsize])
ax.plot(geodata['xcon2'],geodata['xcon3'])

nwin+=1
fignames = np.append(fignames,(dirout + 'xz' + geofile))
figs = np.append(figs,plt.figure(nwin))
figs[-1].clf()
ax = figs[-1].add_subplot(111)
ax.set_xlabel(r'x [M]')
ax.set_ylabel(r'z [M]')
plt.axis([-rsize,rsize,-rsize,rsize])
ax.plot(geodata['xcon1'],geodata['xcon3'])

if savepng :
    for i in np.arange(len(figs)) : 
        figs[i].savefig('%s.png' %fignames[i], dpi=300)
        
if showplots : 
    plt.show()
        
        
print("All done ")
sys.stdout.flush()

#  

#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
