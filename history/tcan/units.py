###############################################################################
###############################################################################
#  Routines for dealing with units
###############################################################################
###############################################################################

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re
import astropy.constants as consts
import astropy.units as u
#import scipy.constants as consts
from IPython.core.debugger import Tracer

#######################################################################

def keplerian_period(a_bh, m_bh, r) :

    period = 2.*np.pi*( r**(1.5) + a_bh*np.sqrt(m_bh) ) / np.sqrt(m_bh)

    return(period)


#######################################################################

def risco_func(a_bh, m_bh) :

    Z1 = 1. + ((1. - a_bh*a_bh)**(1.0/3.0))*( ((1. + a_bh)**(1.0/3.0)) + ((1. - a_bh)**(1.0/3.0)) )
    Z2 = np.sqrt( 3.*a_bh*a_bh + Z1*Z1 )
    
    ru = 3. + Z2 - np.sqrt( (3. - Z1)*(3. + Z1 + 2.*Z2) )
    rl = 3. + Z2 + np.sqrt( (3. - Z1)*(3. + Z1 + 2.*Z2) )

    if( a_bh >= 0. ) :
        risco = ru;
    else:
        risco = rl;

        risco *= m_bh;

    return risco

#######################################################################
    

#  print("c = ", consts.c)
#  print("Boltzmann = ", consts.Boltzmann)
#  print("c 2= ", consts.physical_constants['speed of light in vacuum'])
#  
#  C_G      =  (6.6742e-8                   ) #/* Gravitational constant */
#  C_c      =  (2.99792458e10               ) #/* speed of light */
#  C_mSUN   =  (1.99e33                     ) #/* solar mass */
#  C_h      =  (6.6260693e-27               ) #/* Planck constant */
#  
#  t_m = 600*1e6*C_mSUN * C_G / C_c**3 / 3600.
#  
#  print("time = ", t_m)
#  
#  print(" c3 = ", C_c**3)


print(" G = ", consts.G.cgs)
print(" GM_sun   = ", consts.GM_sun.cgs)
print(" GM_sun2  = ", consts.G.cgs*consts.M_sun.cgs)


asep = 20.
Porb = 2.*np.pi*asep**(1.5)
print("Porb  geom units = ", Porb)

t_m = consts.GM_sun / consts.c**3
l_m = t_m*consts.c
print("t_m = ", t_m)
print("l_m = ", l_m)
print("l_m.cgs = ",l_m.cgs)

movie_time = u.Quantity(60.+40., 's')

print("movie_time = ",movie_time)

n_Msun = movie_time / (Porb*t_m)
print("n Msun = ", n_Msun )
asep_movie = asep * n_Msun * l_m 
print("asep_movie = ", asep_movie.to('lightyear'), asep_movie.to('km'))

print(" ")
Porb_m6 = Porb*t_m*1e6
print("Porb_m6 = ", Porb_m6, Porb_m6.to('min'))
asep_m6 = asep * l_m * 1e6
print("asep_m6 = ", asep_m6.to('lightyear'), asep_m6.to('km'))

r = risco_func(-1., 1.)
print("a=-1. :  ", r, keplerian_period(-1.,1.,r))
r = risco_func(1., 1.)
print("a=1. :  ", r, keplerian_period(1.,1.,r))

