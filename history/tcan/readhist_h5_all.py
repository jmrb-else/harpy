"""  

  readhist_h5_all.py: 
  ------------

    -- Reads in a dataset from all the history file, appending along
    the way and being sure that the dataset is continuous and
    monotonically ordered in time.

    -- returns with the dataset and the time array: e.g., 

     fdata, tout = readhist_h5_all('/Bound/H_Lut','KDHARM0.history')
     fdata, tout = readhist_h5_all(func_name, hist_name)


    -- note that the "hist_name" must end before the suffixes so that this module can "glob" all the history files ala  

             hist_file+'*.h5'

    -- and "func_name" must be the full path of the grid function; 


"""

from __future__ import division
from __future__ import print_function
import os,sys
import numpy as np
import h5py 
import glob 

###############################################################################
###############################################################################
def check_ir_values(i_rbeg, i_rend, nr ) :

    if( i_rbeg is not None ) :
        if( (i_rbeg < 0) or (i_rbeg >= nr) ) :
            sys.exit("i_rbeg must be within [0,", (nr-1), "] " )
    if( i_rend is not None ) :
        if( (i_rend <= 0) or (i_rend > nr) ) :
            sys.exit("i_rbeg must be within [1,", (nr), "] " )

    if( i_rbeg is None ) :
        i_rbeg = 0

    if( i_rend is None ) :
        i_rend = nr

    return i_rbeg, i_rend
    

###############################################################################
###############################################################################
###############################################################################

def readhist_h5_all(func_name, hist_name, 
                    tbeg=None,
                    tend=None,
                    i_rbeg=None,
                    i_rend=None  ):
    """  
    Reads in a dataset from all the history file, appending along the
    way and being sure that the dataset is continuous and
    monotonically ordered in time.
    """

    t_thresh = 1e-10


           
    ####################################################################################
    # Read in the data :
    ####################################################################################
    hist_files = glob.glob(hist_name+"*.h5")

    if( len(hist_files) < 1 ) : 
        sys.exit("readhist_h5_all():  no history files exist!  Exitting...", hist_files)

    first = True
    t_dat = np.array([])
    for hfile in  hist_files : 
        fh5 = h5py.File(hfile,'r')
        t_dat = np.append(t_dat, fh5['/H_t'].value,axis=0)
        if( first ) : 
            i_rbeg, i_rend = check_ir_values(i_rbeg, i_rend, fh5[func_name].shape[1])
            f_dat = fh5[func_name].value[:,i_rbeg:i_rend]
            first = False
        else :
            f_dat = np.append(f_dat, fh5[func_name].value[:,i_rbeg:i_rend],axis=0)
        fh5.close()


    ####################################################################################
    # Make sure the arrays are consistent: 
    ####################################################################################
    if( len(t_dat.shape) != 1 ) : 
        print("readhist_h5_all():  time array is not 1-d,  ", t_dat.shape)
        sys.exit("readhist_h5_all(): Exitting..")

    if( len(f_dat.shape) != 2 ) : 
        print("readhist_h5_all():  time array is not 1-d,  ", t_dat.shape)
        sys.exit("readhist_h5_all():  Exitting..")

    if( t_dat.shape[0] != f_dat.shape[0] ) : 
        print("readhist_h5_all():  mismatch in dimensions,   ", t_dat.shape, f_dat.shape)
        sys.exit("readhist_h5_all():  Exitting..")


    # Mask out the points with missing data:
    if( tbeg is None ) :
        tbeg = np.min(t_dat)
    if( tend is None ) :
        tend = np.max(t_dat)
    
    mask = (( t_dat > t_thresh ) & (t_dat >= tbeg) & (t_dat <= tend) )

    # Mask out those points that are not monotonically increasing : 
    min_t = t_dat[0] - 10. 
    for i in np.arange(len(t_dat)) : 
        t_tmp = t_dat[i]
        if( t_tmp <= min_t ) : 
            mask[i] = False
        else : 
            min_t = t_tmp

    # Remove masked out points:
    loct = np.where(mask)
    t_dat = t_dat[loct[0]]
    f_dat = f_dat[loct[0],:]

    # We are done! 
    return f_dat, t_dat

###############################################################################
###############################################################################
###############################################################################
def readhist_h5_all_suffixes(suffixes,funcname,histname,
                             tbeg=None,
                             tend=None,
                             i_rbeg=None,
                             i_rend=None  ):

    first = True
    for suffix in suffixes :
        func_tmp, t_hist = readhist_h5_all(suffix+funcname,histname,tbeg=tbeg,tend=tend,i_rbeg=i_rbeg,i_rend=i_rend)
        if( first ) :
            func_out = func_tmp
            first = False
        else :
            func_out += func_tmp

    return func_out, t_hist 

