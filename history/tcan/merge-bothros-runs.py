"""

  merge-bothros-runs.py: 
  ------------

    -- merges a set of bothros files, each at a single time and
       frequency, into a merged data set.

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

import string 

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################
#
#  Data formats of the source data files:
#
# funcs_to_copy: 
# Header                   Group
# DBL   pix_area                 Dataset {1, 300, 300}
# DBL   pix_x                    Dataset {1, 300, 300}
# DBL   pix_y                    Dataset {1, 300, 300}
# INT   id                       Dataset {1, 300, 300}
#
# other_funcs:
# DBL   F_cavity                 Dataset {1}
# DBL   F_circumbinary           Dataset {1}
# DBL   F_minidisks              Dataset {1}
# DBL   F_tot                    Dataset {1}
# DBL   freq                     Dataset {1}
# DBL   times                    Dataset {1}
# INT   snap_id                  Dataset {1}
# DBL   units                    Dataset {1, 13}
# INT   end_state                Dataset {1, 300, 300}
# INT   n_geod                   Dataset {1, 300, 300}
# DBL   I_image                  Dataset {1, 300, 300}
# DBL   I_image2                 Dataset {1, 300, 300}
# DBL   I_image3                 Dataset {1, 300, 300}
#
#
# this is ignored because each frame will only integrate one frequency, we'll need to calculate this after the merged file is made:
## DBL   I_bolometric             Dataset {1, 300, 300}
#
##################################################################################################



# There is probably a nice way of doing this automatically using glob() and string parsing: 
ntimes = 182
nfreq  = 100
#ntimes = 2
#nfreq  = 2
# ntimes = 2
# nfreq  = 1
ntot = nfreq * ntimes

outfile="all_frames.h5"
h5out  = h5py.File(outfile,'w-')

files = sorted(glob.glob("../light_curve_*.h5"))
nfiles = len(files)
print("files=", files)

if( nfiles != ntot ) :
    sys.exit("counts are off: ", ntimes, nfreq, ntot, nfiles)

    
funcs_to_copy = ['Header','pix_area', 'pix_x', 'pix_y', 'id']
n_funcs_to_copy = len(funcs_to_copy)

other_funcs = ['F_minidisks','F_circumbinary','F_cavity','F_tot','freq','times', 'snap_id', 'units','I_image', 'I_image2']
n_other_funcs = len(other_funcs)


#######################################################################
# Copy over those datasets that are independent of time and frequency:
#######################################################################
h5in  = h5py.File(files[0],'r')

for func in funcs_to_copy :
    print("doing func = ", func)
    print(" func = ", func )
    h5in.copy(func,h5out)

#######################################################################
# Create the datasets in the output file, automatically figuring out
# their dimensions and types from the source data sets:
#######################################################################
shape0 = (ntimes, nfreq)
for func in other_funcs :
    print("doing func = ", func)
    shape1 = h5in[func].shape
    ndim1 = len(shape1)
    if( ndim1 >= 1 ) :
        shape2 = shape0
    if( ndim1 >= 2 ) :
        shape2 = np.append(shape2,shape1[1])
    if( ndim1 >= 3 ) :
        shape2 = np.append(shape2,shape1[2])
    if( ndim1 >= 4 ) :
        sys.exit("ndim >= 4 not supported: ", ndim1)

    dset = h5out.create_dataset(func,shape2,dtype=h5in[func].dtype)

h5in.close()


#######################################################################
# Now read in each source data set and save it in the correct slot of
# the destination/merged data set.
#
#  -- assume for now that every input hdf5 file will only have one
#     entry;
#
#  -- each input file will have the filename format :
#        light_curve_${itime}_${ifreq}.h5
#
#######################################################################
for file in files :
    print("Processing  file = ", file)
    file_parts = string.split(file,"_")
    time_str = file_parts[2]
    freq_strings = string.split(file_parts[3],'.')
    freq_str = freq_strings[0]
    itime = string.atoi(time_str)
    ifreq = string.atoi(freq_str)
    print(" itime ifreq = ", itime, ifreq) 

    h5in  = h5py.File(file,'r')

    for func in other_funcs :
        print("....Merging func = ", func)
        ndim1 = len(h5in[func].shape)
        print("shape in  = ", h5in[func].shape)
        print("shape out = ", h5out[func].shape)
        # There has to be a more elegant solution than this: 
        if( ndim1 == 1 ) :
            h5out[func][itime,ifreq] = h5in[func][0]
        if( ndim1 == 2 ) :
            h5out[func][itime,ifreq,:] = h5in[func][0,:]
        if( ndim1 == 3 ) :
            h5out[func][itime,ifreq,:,:] = h5in[func][0,:,:]
        if( ndim1 >= 4 ) :
            sys.exit("ndim >= 4 not supported: ", ndim1)
            
    h5in.close()
            

h5out.close()

print("Done with all files !")
