
from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import h5py
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from mpl_toolkits.axes_grid1 import make_axes_locatable
from coord_transforms import *
from harm import *
from polp_lite import *


###############################3
def gen_csv_files(file='KDHARM0.tavg.phavg.h5',func_suffix='_phavg_tavg'):

    
    gdumpfile = 'KDHARM0.gdump.h5'

    gfile   = h5py.File(gdumpfile, 'r')
    r  = gfile['x1'].value[:,:,0]
    th = gfile['x2'].value[:,:,0]
    x = r * np.sin(th)
    y = r * np.cos(th)
    gfile.close()
    
    h5file  = h5py.File(file,'r')
    rho     = h5file['rho'+func_suffix].value[:]
    sigma   = h5file['sigma_mag'+func_suffix].value[:]
    betainv = h5file['beta_inv'+func_suffix].value[:]
    h5file.close()
    
    file_out = 'nobleharm-phiaverages.npz'

    np.savez(file_out, x=x, y=y, sigma=sigma, rho=rho, betainv=betainv)
    #np.savez_compressed(file_out, x=x, y=y, sigma=sigma, rho=rho, betainv=betainv)

    return


###############################3
def plot_all(file='KDHARM0.000000.phavg.h5',func_suffix='_tavg'):

    funcs = ['Aphi','B1_phavg', 'B2_phavg', 'beta_inv_phavg', 'bsq_phavg',  'gamma_phavg','rho_phavg','sigma_mag_phavg','uu_phavg']

    gdf='KDHARM0.gdump.h5'
    suf='phavg'
    dirout='./plots/'

    rmax=50.

    h5file   = h5py.File(file, 'r')
    print("looking at file = ", file)
    
    for func in funcs :
        func += func_suffix
        print("   looking at func = ", func)
        
        func0 = h5file[func].value

        polp(func0=func0,itime=0,dirname="./",show_plot=False,funcname=func,dolog=True,savepng=True,iph=0,ith=None,corotate=False,namesuffix=suf,dirout=dirout,rmax=rmax,dumpfile=file,gdumpfile=gdf,usegdumpcoord=True)

        func0=0


    return


###############################3
def plot_spec(file='KDHARM0.000000.phavg.h5',func_suffix='_tavg'):

    funcs = ['rho_phavg','beta_inv_phavg', 'sigma_mag_phavg']
    #minvals = [1e-7,1e-2,1e-3]
    #maxvals = [1e0,1e3,1e3]
    minvals = [-7,-2,-3]
    maxvals = [0,3,3]

    gdf='KDHARM0.gdump.h5'
    suf='-special'
    dirout='./plots/'

    rmax=50.

    h5file   = h5py.File(file, 'r')
    print("looking at file = ", file)

    i = 0
    for func in funcs :
        func += func_suffix
        print("   looking at func = ", func)
        
        func0 = h5file[func].value

        polp(func0=func0,itime=0,dirname="./",show_plot=False,funcname=func,dolog=True,savepng=True,iph=0,ith=None,corotate=False,namesuffix=suf,dirout=dirout,rmax=rmax,dumpfile=file,gdumpfile=gdf,usegdumpcoord=True,minf=minvals[i],maxf=maxvals[i])

        func0=0
        i += 1


    return


##################################################
# Plot rho from NPZ data:
##################################################
def plot_npz_rho(file='nobleharm-phiaverages.npz'):

    data_npz = np.load(file)

    fig,((ax1))=plt.subplots(1,1,figsize=(3,6),dpi=300)
    fig.tight_layout()
    fig.subplots_adjust(wspace=.4)
    
    im=ax1.pcolormesh(data_npz['x'],data_npz['y'],np.log10(data_npz['rho']),vmin=-8,vmax=0, rasterized=True,cmap='jet')
    
    ax1.set_aspect('equal')
    ax1.set_xlim(0,50)
    ax1.set_ylim(-50,50)
    ax1.set_aspect('equal')
    ax1.set_title(r'$\log_{10}\langle \rho \rangle$')
    
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes("right", "4%", pad="6%")
    cbar=fig.colorbar(im,cax=cax)
    
    fig.savefig('npz-rho.pdf',dpi=300,bbox_inches='tight')

    return

##################################################
# Plot rho from NPZ data:
##################################################
def plot_npz_betainv(file='nobleharm-phiaverages.npz'):

    data_npz = np.load(file)

    fig,((ax1))=plt.subplots(1,1,figsize=(3,6),dpi=300)
    fig.tight_layout()
    fig.subplots_adjust(wspace=.4)
    
    im=ax1.pcolormesh(data_npz['x'],data_npz['y'],np.log10(data_npz['betainv']),vmin=-2,vmax=3, rasterized=True,cmap='plasma')
    
    ax1.set_aspect('equal')
    ax1.set_xlim(0,50)
    ax1.set_ylim(-50,50)
    ax1.set_aspect('equal')
    ax1.set_title(r'$\log_{10}\langle \beta^{-1} \rangle$')
    
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes("right", "4%", pad="6%")
    cbar=fig.colorbar(im,cax=cax)
    
    fig.savefig('npz-betainv.pdf',dpi=300,bbox_inches='tight')

    return

##################################################
# Plot rho from NPZ data:
##################################################
def plot_npz_sigma(file='nobleharm-phiaverages.npz'):

    data_npz = np.load(file)

    fig,((ax1))=plt.subplots(1,1,figsize=(3,6),dpi=300)
    fig.tight_layout()
    fig.subplots_adjust(wspace=.4)
    
    im=ax1.pcolormesh(data_npz['x'],data_npz['y'],np.log10(data_npz['sigma']),vmin=-3,vmax=3, rasterized=True,cmap='viridis')
    
    ax1.set_aspect('equal')
    ax1.set_xlim(0,50)
    ax1.set_ylim(-50,50)
    ax1.set_aspect('equal')
    ax1.set_title(r'$\log_{10}\langle \sigma \rangle$')
    
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes("right", "4%", pad="6%")
    cbar=fig.colorbar(im,cax=cax)
    
    fig.savefig('npz-sigma.pdf',dpi=300,bbox_inches='tight')

    return

###############################3
def time_avg_phi_avgs(file_list,file_out=None): 

    funcs = ['Aphi','B1_phavg', 'B2_phavg', 'beta_inv_phavg', 'bsq_phavg',  'gamma_phavg','rho_phavg','sigma_mag_phavg','uu_phavg']

    files = file_list 
    nfiles = len(files)

    if( nfiles < 1 ) :
        sys.exit('need more than one file: ', nfiles)


    if( file_out is None ) : 
        file_out = files[0].split('.')[0]+'.tavg.phavg.h5'

    h5out  = h5py.File(file_out, 'w')

    for func in funcs :
        print("Averaging  func = ", func)
        new_func = True
        
        for file in files :
            print("   with file = ", file)
            h5in  = h5py.File(file, 'r')

            if( new_func ) :
                favg = np.copy(h5in[func])
                new_func = False
            else :
                favg += h5in[func]

            h5in.close()


        favg /= nfiles
            
        #Create dataset in destination file:
        dset = h5out.create_dataset(func+'_tavg', data=favg)


    h5out.close()
    
    return


###############################3

if( False ) : 
    files = sorted(np.append(glob.glob("KDHARM0.000[5-9]??.h5"),"KDHARM0.001000.h5"))

    print("file list = ", files)

    for file in files :
        make_phi_avgs_eht( file=file )


    files2 = sorted(glob.glob("KDHARM0.??????.phavg.h5"))
    print("file list2 = ", files2)
    time_avg_phi_avgs(files2,file_out=None)


if( False ) :     
    plot_all(file='KDHARM0.tavg.phavg.h5')

if( False ) :     
    plot_spec(file='KDHARM0.tavg.phavg.h5')

    
if( True ) :     
    gen_csv_files()

if( True ) :
    plot_npz_rho() 
    plot_npz_betainv() 
    plot_npz_sigma() 

    
sys.exit('done')
    
 
