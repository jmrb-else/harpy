"""  

  lum.py: 
  ------------
    -- program to analyze the temporal energy spectrum of light curves from 
        various disk simulations#
    -- reads in data from the history files# 
    -- uses scipy methods for signal analysis#

"""

from __future__ import division
from __future__ import print_function

from scipy.fftpack import fft
import scipy.signal as signal 
import matplotlib.pyplot as plt
import numpy as np
import os,sys
import h5py

from get_sim_info import *
from readhist_h5_all import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################
###############################################################################

# def lum(runname='longer2',savepng=0):
"""  
Primary routine for analyzing light curve. 
"""

#wfuncs = ['lomb','blackmanharris','nuttall','hann','boxcar','blackman','fft']
wfuncs = ['fft']
#wfuncs = ['welch']
#wfuncs = ['lomb']
#wfuncs = ['blackmanharris', 'lomb', 'fft']
#wfuncs = ['blackmanharris']

nwfuncs = len(wfuncs)

showplots=1
savepng=1
write_lcurves=0
read_lcurves=1

runnames = ['longer2', 'medium_disk', 'bigger_disk', 'inject_disk']
#runnames = ['bigger_disk']
#runnames = ['longer2']
#runnames = ['inject_disk']

if not showplots :
    plt.ioff()


for runname in runnames :
    for i in np.arange(nwfuncs) : 
        winfunc = wfuncs[i]
        
        print("###############")
        print("Doing ", winfunc)
        print("###############")
        
        dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
#        dirout ='/home/scott/tex/mass-ratio-paper/'+runname+'/'
        
        pnames = ['dirname', 'freq_bin_orbit', 'hist_name', 'sigma_0', 't_lump_beg', 't_lump_end', 'tend']
        
        sim_info = get_sim_info(pnames, runname=runname)
        
        print(" back in lum") 
        print( sim_info )
        
        #     fs = 10e3
        #     N = 1e5
        #     amp = 2*np.sqrt(2)
        #     freq = 1270.0
        #     noise_power = 0.001 * fs / 2
        #     time = np.arange(N) / fs
        #     x = amp*np.sin(2*np.pi*freq*time)
        #    x += np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
            
        #    f, Pper_spec = signal.periodogram(x, fs, 'blackmanharris', scaling='spectrum')
        #    f, Pper_spec = signal.periodogram(x, fs, 'nuttall', scaling='spectrum')
        #    f, Pper_spec = signal.welch(x, fs, scaling='spectrum')
        #    f, Pper_spec = signal.welch(x, fs, window='nuttall',scaling='spectrum')
        #    yf = fft(y)

        if read_lcurves : 
            fdest = h5py.File(dirout+'lcurve.h5','r')
            t_dat = fdest['t_dat'].value 
            lcurve = fdest['lcurve'].value 
            fdest.close()
        else : 
            lum_dat, t_dat = readhist_h5_all('/Bound/H_Lut', sim_info['hist_name'])
            lcurve = np.sum(lum_dat,axis=1)


        if write_lcurves : 
            fdest = h5py.File(dirout+'lcurve.h5','w-')
            dset = fdest.create_dataset('t_dat',(len(t_dat),),dtype='float64',data=t_dat)
            dset = fdest.create_dataset('lcurve',(len(lcurve),),dtype='float64',data=lcurve)
            fdest.close()

        
        lcurve = np.divide(lcurve,-sim_info['sigma_0'])
        
        loct = np.where( (sim_info['t_lump_beg'] <= t_dat) & (t_dat <= sim_info['t_lump_end']) )
        t_win = t_dat[loct[0]]
        lcurve_win = lcurve[loct[0]]
        
        #lcurve_win_detrend = signal.detrend(lcurve_win,type='linear')
        
        pfit = np.poly1d(np.polyfit(t_win,lcurve_win,deg=5))(t_win)
        lcurve_win_detrend = lcurve_win - pfit
        
#        sampling_freq = 1./sim_info['hist_freq'][0] 
        sampling_freq = 1./10.
        
        if winfunc == 'fft' : 
            T = 1./sampling_freq
            N = len(t_win)
            if (N % 2) : 
                N -= 1 
            x = np.linspace(0.0, N*T, N)
            y = lcurve_win_detrend[0:N]
            from scipy.signal import blackmanharris
            w = blackmanharris(N)
            ywf = fft(y*w)
            fps_sq = (np.abs(ywf[0:N/2]) * 2.0/N)**2
            freq = np.linspace(0.0, 1.0/(2.0*T), N/2)

    #        print("y", len(y))
    #        print("w", len(w))
    #        print("freq ", len(freq))
    #        print("fps_sq", len(fps_sq))
    #        print("ywf", len(ywf))
    #        print("N", N)
    #        stop
        else : 
            if winfunc == 'lomb' :
                freq_min = 1./(t_win[-1] - t_win[0])
                freq_max = 0.45*freq_min*len(t_win)
                freq = np.linspace(2.*np.pi*freq_min, 2.*np.pi*freq_max, len(t_win))
                fps_sq  = signal.lombscargle(t_win,lcurve_win_detrend, freq)
                ftmp = 1./len(t_win)
                fps_sq = np.multiply(fps_sq,ftmp)
                ftmp = 1./(2.*np.pi)
                freq = np.multiply(freq,ftmp)
            else : 
                if winfunc == 'welch' :
                    freq, fps_sq  = signal.welch(lcurve_win_detrend, sampling_freq, scaling='spectrum')
                else : 
                    freq, fps_sq  = signal.periodogram(lcurve_win_detrend, sampling_freq, window=winfunc,scaling='spectrum')
    #            freq, fps_sq  = signal.periodogram(lcurve_win_detrend, window=winfunc,scaling='spectrum')
    
    
    
        fps = np.sqrt(fps_sq)
        
        t_dat = np.multiply(t_dat,1.e-4)
        t_win = np.multiply(t_win,1.e-4)
        
        ftmp = 1./sim_info['freq_bin_orbit']
        freq = np.multiply(freq,ftmp)
        
        tag = '-'+winfunc
        nwin=6
        fignames = dirout + 'lightcurve-full'+tag
        figs = [plt.figure(nwin)]
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r't [$10^4$ M]')
        ax.set_ylabel(r'Luminosity')
        ax.plot(t_dat, lcurve)
        
#        nwin+=1
#        fignames = np.append(fignames,(dirout + 'lightcurve-window'+tag))
#        figs = np.append(figs,plt.figure(nwin))
#        figs[-1].clf()
#        ax = figs[-1].add_subplot(111)
#        ax.set_xlabel(r't [$10^4$ M]')
#        ax.set_ylabel(r'Luminosity')
#        ax.plot(t_win, lcurve_win, '-', t_win, pfit, '--')
#    
#        nwin+=1
#        fignames = np.append(fignames,(dirout + 'lightcurve-window-detrend'+tag))
#        figs = np.append(figs,plt.figure(nwin))
#        figs[-1].clf()
#        ax = figs[-1].add_subplot(111)
#        ax.set_xlabel(r't [$10^4$ M]')
#        ax.set_ylabel(r'Luminosity')
#        ax.plot(t_win, lcurve_win_detrend)
    
        print("fps", len(fps))
        print("freq", len(freq))
    
#        nwin+=1
#        fignames = np.append(fignames,(dirout + 'fft-lum-logy'+tag))
#        figs = np.append(figs,plt.figure(nwin))
#        figs[-1].clf()
#        ax = figs[-1].add_subplot(111)
#        ax.set_xlabel(r'$\omega [\Omega_{bin}]$')
#        ax.set_ylabel(r'FPS of Luminosity')
#        plt.axis([5e-2,8.,1e-12,1.e-4])
#        ax.semilogy(freq, fps)
    
        nwin+=1
        fignames = np.append(fignames,(dirout + 'fft-lum-loglog'+tag))
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r'$\omega [\Omega_{bin}]$')
        ax.set_ylabel(r'FPS of Luminosity')
        plt.axis([5e-2,25.,1e-10,1.e-4])
        ax.loglog(freq, fps)
        
        nwin+=1
        fignames = np.append(fignames,(dirout + 'fft-lum-lin'+tag))
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r'$\omega [\Omega_{bin}]$')
        ax.set_ylabel(r'FPS of Luminosity')
        plt.axis([5e-2,4.,1e-12,1.e-4])
        ax.plot(freq, fps)

        nwin+=1
        fignames = np.append(fignames,(dirout + 'spectrogram-lum-lin'+tag))
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_title(r'Spectrogram of Luminosity')
        ax.set_xlabel(r't [$10^4$ M]')
        ax.set_ylabel(r'$\omega [\Omega_{bin}]$')
        freq, tout, spectr = signal.spectrogram(lcurve,sampling_freq,detrend='linear',scaling='spectrum')
        tout = np.multiply(tout,1e-4)
        ftmp = 1./sim_info['freq_bin_orbit']
        freq = np.multiply(freq,ftmp)
        plt.axis([1.e-4,sim_info['tend']*1e-4,0.0,6.])
        print("max = ", np.max(spectr))
        print("min = ", np.min(spectr))
        print("n_freq  = ",freq.shape)
        print("n_t     = ",tout.shape)
        print("n_spectr= ",spectr.shape)
        func=np.log10(spectr+1e-20)
        plt.pcolormesh(tout,freq,func,cmap='jet',vmin=-11.,vmax=-7)
        #plt.pcolormesh(tout,freq,spectr,cmap='jet',vmin=1e-12,vmax=1e-8)
        #plt.pcolormesh(tout,freq,spectr,cmap='jet')
        plt.colorbar()

        nwin+=1
        fignames = np.append(fignames,(dirout + 'spectrogram-lum-lin-wideband'+tag))
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_title(r'Spectrogram of Luminosity')
        ax.set_xlabel(r't [$10^4$ M]')
        ax.set_ylabel(r'$\omega [\Omega_{bin}]$')
        freq, tout, spectr = signal.spectrogram(lcurve,sampling_freq,detrend='linear',scaling='spectrum',nperseg=2048)
        tout = np.multiply(tout,1e-4)
        ftmp = 1./sim_info['freq_bin_orbit']
        freq = np.multiply(freq,ftmp)
        plt.axis([1.e-4,sim_info['tend']*1e-4,0.0,6.])
        print("max = ", np.max(spectr))
        print("min = ", np.min(spectr))
        func=np.log10(spectr+1e-20)
        plt.pcolormesh(tout,freq,func,cmap='jet',vmin=-11.,vmax=-7)
        #plt.pcolormesh(tout,freq,spectr,cmap='jet',vmin=1e-12,vmax=1e-8)
        #plt.pcolormesh(tout,freq,spectr,cmap='jet')
        plt.colorbar()

        nwin+=1
        fignames = np.append(fignames,(dirout + 'spectrogram-lum-lin-narrowband'+tag))
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_title(r'Spectrogram of Luminosity')
        ax.set_xlabel(r't [$10^4$ M]')
        ax.set_ylabel(r'$\omega [\Omega_{bin}]$')
        freq, tout, spectr = signal.spectrogram(lcurve,sampling_freq,detrend='linear',scaling='spectrum',nperseg=128)
        tout = np.multiply(tout,1e-4)
        ftmp = 1./sim_info['freq_bin_orbit']
        freq = np.multiply(freq,ftmp)
        plt.axis([1.e-4,sim_info['tend']*1e-4,0.0,6.])
        print("max = ", np.max(spectr))
        print("min = ", np.min(spectr))
        func=np.log10(spectr+1e-20)
        plt.pcolormesh(tout,freq,func,cmap='jet',vmin=-11.,vmax=-7)
        #plt.pcolormesh(tout,freq,spectr,cmap='jet',vmin=1e-12,vmax=1e-8)
        #plt.pcolormesh(tout,freq,spectr,cmap='jet')
        plt.colorbar()

        #nwin+=1
        #fignames = np.append(fignames,(dirout + 'lum-lin-astro-ls'+tag))
        #figs = np.append(figs,plt.figure(nwin))
        #figs[-1].clf()
        #ax = figs[-1].add_subplot(111)
        #ax.set_xlabel(r'$\omega [\Omega_{bin}]$')
        #ax.set_ylabel(r'FPS of Luminosity')
        ##plt.axis([5e-2,5.,1e-10,1.e-4])
        #t_ls = np.multiply(t_win,1e4)
        #from astropy.stats import LombScargle
        #fmin = 0.1*sim_info['freq_bin_orbit']
        #fmax = 10.*sim_info['freq_bin_orbit']
        #frequency, power = LombScargle(t_ls, lcurve_win_detrend).autopower(minimum_frequency=fmin,maximum_frequency=fmax,samples_per_peak=10,nyquist_factor=10)
        #ftmp = 1./sim_info['freq_bin_orbit']
        #frequency = np.multiply(frequency, ftmp)
        #power = np.abs(power)
        #ax.loglog(frequency, power)


        if savepng :
            for i in np.arange(len(figs)) : 
                figs[i].savefig('%s.png' %fignames[i], dpi=300)
        
        if showplots : 
            plt.show()
        
        
        print("All done ")
        sys.stdout.flush()

#  
#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
