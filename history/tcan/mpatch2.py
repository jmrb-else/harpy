"""  

  mpatch.py: 
  ------------

    -- generic contour plot, not for doing sophisticated operations
       but for quickly viewing 2-d slices of 3-d data;

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re 

from harm import * 

from IPython.core.debugger import Tracer

###############################################################################
#  
###############################################################################
def cyl_params(Rin, Rout, z_length, hor, n_cells_per_scaleheight, risco):

    
#    dz_min = hor * risco / n_cells_per_scaleheight
    dz_min = 0.0063
#    Nz = np.int(z_length / dz_min)
    Nz = 300

    #  r = r0 + exp(xp1),  r1-r0 = exp(xp1_0 + dx) - exp(xp1_0) = Rin*(exp(dx) - 1)
    Length1 = np.log10(Rout/Rin)
    dr = 2.*dz_min
    dxp1 = np.log(1. + dr/Rin)
    Nr = np.int(Length1 / dxp1)

    dphi = 4.*dz_min/risco 
    Nphi = np.int( 2.*np.pi / dphi )
    
    dx_min = np.array([dr, dz_min, risco*dphi ])
    Npts = np.array([Nr, Nz, Nphi])

    return Length1, dx_min, Npts


###############################################################################
#  
###############################################################################
def sphere_params(Rin, Rout, Npts, hor, n_cells_per_scaleheight, th_cut):

    Length = np.log10(Rout/Rin)
    dxp1 = Length / Npts[0]
    #  r = r0 + exp(xp1),  r1-r0 = exp(xp1_0 + dx) - exp(xp1_0) = Rin*(exp(dx) - 1)
    dx_min = np.array([Rin * (np.exp(dxp1) - 1.), 
                          Rin * hor / n_cells_per_scaleheight,
                          2.*np.pi*Rin*np.sin(th_cut)/Npts[2] ])

    return Length, dx_min 


###############################################################################
# Cartesian covering axis above and below the horizon 
###############################################################################
def cyl_axis_params(Rin, n_cells_per_hor, z_length):

    Length = 2.*Rin
    dxp1 = Length / n_cells_per_hor
    Nz   = np.int(z_length / dxp1)
    Nxy  = np.int(n_cells_per_hor)
    
    Npts  = np.array([Nxy, Nxy, Nz])
    dx_min = np.array([dxp1,dxp1,dxp1])

    return Length, dx_min, Npts


###############################################################################
#  
###############################################################################
def polar_cap_params(Rin, Rout, N0, th_cut, dx_min_0):

    Length = np.log10(Rout/Rin)
#    d_angle = np.min(dx_min_0) / Rin
#    d_angle = dx_min_0[1] / Rin
    d_angle = np.max(dx_min_0) / Rin
    dxp1 = Length / N0
    #  r = r0 + exp(xp1),  r1-r0 = exp(xp1_0 + dx) - exp(xp1_0) = Rin*(exp(dx) - 1)
    
    Npts  = np.array([N0, 
                      (int(th_cut/d_angle)), 
                      (int(th_cut/d_angle)) ])

    dx_min = np.array([Rin * (np.exp(dxp1) - 1.), 
                          Rin * th_cut / Npts[1], 
                          Rin * th_cut / Npts[2] ])


    return Length, dx_min, Npts

###############################################################################
#  Returns with the parameters for a spherical grid with top and bottom
#   polar (spherical) patches;
###############################################################################
def multi_sphere1_params(Rin, Rout, N_0, hor, n_cells_per_scaleheight, th_cut):

#-- Patch0 = lg. sphere
    # Larger  N1  here than Noble+2012 because it goes further in:
    Length_0, dx_min_0 = sphere_params(Rin, Rout, N_0, hor, n_cells_per_scaleheight, th_cut)
    Ntot_0 = np.prod(N_0)
    
#-- Patch1 = top polar cap (uniform in theta and phi) 
    ## !! will likely need to make polar caps slightly larger than th_cut 
    Length_1, dx_min_1, N_1 = polar_cap_params(Rin, Rout, N_0[0], th_cut, dx_min_0)
    Ntot_1 = np.prod(N_1)
    

#-- Patch2 = bottom polar cap (uniform in theta and phi) 
    ## !! will likely need to make polar caps slightly larger than th_cut 
    Length_2, dx_min_2, N_2 = polar_cap_params(Rin, Rout, N_0[0], th_cut, dx_min_0)
    Ntot_2 = np.prod(N_2)
    
    return dx_min_0, Ntot_0, dx_min_1, N_1, Ntot_1, dx_min_2, N_2, Ntot_2

###############################################################################
#  Returns with the parameters for a cylindrical grid with single Cartesian
#   patch covering axis;
###############################################################################
def multi_cyl1_params(Rin, Rout, z_length, risco, hor, n_cells_per_hor, n_cells_per_scaleheight):

#-- Patch0 = lg. cylinder
    Length_0, dx_min_0, N_0 = cyl_params(Rin, Rout, z_length, hor, n_cells_per_scaleheight, risco)
    Ntot_0 = np.prod(N_0)
    
#-- Patch1 = entire cartesian axis patch
    ## !! could break this into two if we moved the cylindrical one further into the
    ##    horizon, but I think this is easier. 
    Length_1, dx_min_1, N_1 = cyl_axis_params(Rin, n_cells_per_hor, z_length)
    Ntot_1 = np.prod(N_1)
    
    return dx_min_0, N_0, Ntot_0, dx_min_1, N_1, Ntot_1

###############################################################################
#      -- Patch00 = lg. sphere
#      -- Patch01 = top polar cap
#      -- Patch02 = bottom polar cap
#      -- Patch10 = Cart. patch at origin
#      -- Patch20 = sm. cylinder BH1, Rin_2 = 0.9 *  M1 / (M1+M2)  = 0.45 M 
#      -- Patch21 = BH1 Cartesian axis
#      -- Patch30 = sm. cylinder Bh2 
#      -- Patch31 = BH2 Cartesian axis
###############################################################################

def mpatch(th_cut_00, th_cut_20=None, th_cut_30=None,
           hor_00=0.1, 
           N1_00=300, 
           N2_00=160,
           N3_00=400,
           N1_20=200,
           N2_20=120,
           N3_20=200, 
           N1_30=200,
           N2_30=120,
           N3_30=200, 
           cour=0.45,
           n_cells_per_core=(20*16*20),
           t_length=1e4, 
           zc_per_sec_core=5800.
           ):

    """  
    Routine for making quick color contours of data slices. 
    """
    prog_name="mpatch"

    asep = 20.
    n_cells_per_scaleheight = 32
    n_cells_per_hor         = 32

    Mtot = 1.
    mass_ratio = 1. 
    M1 = Mtot / (1.+mass_ratio)
    M2 = Mtot * mass_ratio / (1.+mass_ratio)
    # mass ratio dependence of  r_minidisk? ??
    r_minidisk1 = 0.3 * asep * mass_ratio**(-0.3)
    r_minidisk2 = 0.3 * asep * mass_ratio**(+0.3)

    if th_cut_20 is None: 
        th_cut_20 = th_cut_00

    if th_cut_30 is None: 
        th_cut_30 = th_cut_00
    
    Rout_20 = r_minidisk1
    Rout_30 = r_minidisk2

    Length_10 = asep - Rout_20 - Rout_30 
    R_cart_10 = 0.5 * Length_10


#-- Patch00 = lg. sphere
#-- Patch01 = top polar cap (uniform in theta and phi) 
#-- Patch02 = bottom polar cap (uniform in theta and phi) 
    # Larger  N1  here than Noble+2012 because it goes further in:
    Rin_00 = R_cart_10
    Rout_00 = (13.*asep)
    # Calculate how many cells are required to extend the old log-r grid inward to Rin_00 using a constant dr equal to the smallest dr of the original Noble++2012 run:
    Rin_old = 0.75*asep
    dxp1_old = np.log10(Rout_00/Rin_old) / 300
    dr_old = Rin_old * (np.exp(dxp1_old) - 1.)
    N_extra = np.int((Rin_old - Rin_00) / dr_old)
    N_00 = np.array([N1_00+N_extra,N2_00,N3_00])
    dx_min_00, Ntot_00, dx_min_01, N_01, Ntot_01, dx_min_02, N_02, Ntot_02 = multi_sphere1_params(Rin_00, Rout_00, N_00, hor_00, n_cells_per_scaleheight, th_cut_00)
    # use the constant old dr: 
    dx_min_00[0] = dr_old
    Ntot_0 = np.long(Ntot_00 + Ntot_01 + Ntot_02)
    
    
    
#-- Patch10 = Cart. patch at origin (cube)
#    dx_10 = np.min(dx_min_00)
    dx_10 = dx_min_00[0]
    Ntmp = int(Length_10 / dx_10)
    dx_min_10 = np.array([dx_10,dx_10,dx_10])
    N_10 = np.array([Ntmp,Ntmp,Ntmp])
    Ntot_10 = np.prod([N_10])
    Ntot_1  = Ntot_10

    
#-- Patch20 = sm. cylinder BH1, Rin_2 = 0.9 *  M1 / (M1+M2)  = 0.45 M 
#-- Patch21 = BH1 Cartesian axis
    # using isotropic horizon definition:
    r_hor_20 = M1
    risco_20 = 5.*M1
    Rin_20   = 0.95 * r_hor_20
    Rout_20  = r_minidisk1
    z_length_20 = 2.*Rout_20 
    dx_min_20, N_20, Ntot_20, dx_min_21, N_21, Ntot_21 = multi_cyl1_params(Rin_20, Rout_20, z_length_20, risco_20, hor_00, n_cells_per_hor, n_cells_per_scaleheight)
    Ntot_2 = np.long(Ntot_20 + Ntot_21)
    
#-- Patch30 = sm. cylinder Bh2 
#-- Patch31 = BH2 Cartesian axis
    r_hor_30 = M2
    risco_30 = 5.*M2
    Rin_30   = 0.95 * r_hor_30
    Rout_30  = r_minidisk2
    z_length_30 = 2.*Rout_30 
    dx_min_30, N_30, Ntot_30, dx_min_31, N_31, Ntot_31 = multi_cyl1_params(Rin_30, Rout_30, z_length_30, risco_30, hor_00, n_cells_per_hor, n_cells_per_scaleheight)
    Ntot_3 = np.long(Ntot_30 + Ntot_31)
    
    print("Ntot_1", Ntot_1)
    print("Ntot_2", Ntot_2)
    print("Ntot_3", Ntot_3)

    min_dx_0 = np.min([dx_min_00,dx_min_01,dx_min_02])
    min_dx_1 = np.min([dx_min_10])
    min_dx_2 = np.min([dx_min_20,dx_min_21])
    min_dx_3 = np.min([dx_min_30,dx_min_31])

    print("Min_dx Patch0 = ", min_dx_0)
    print("Min_dx Patch1 = ", min_dx_1)
    print("Min_dx Patch2 = ", min_dx_2)
    print("Min_dx Patch3 = ", min_dx_3)

    # don't use the absolute min (from the dz in mini-disk patches) because the local char speed there is not that of light
#    min_min_dx = np.min([min_dx_0,min_dx_1,min_dx_2,min_dx_3])
    min_min_dx = np.min([min_dx_0])

    print("Min_min_dx  = ", min_min_dx)

    print("dx_00 = ", dx_min_00)
    print("dx_01 = ", dx_min_01)
    print("dx_02 = ", dx_min_02)

    print("dx_10 = ", dx_min_10)

    print("dx_20 = ", dx_min_20)
    print("dx_21 = ", dx_min_21)

    print("dx_30 = ", dx_min_30)
    print("dx_31 = ", dx_min_31)

    print("N_00 = ", N_00)
    print("N_01 = ", N_01)
    print("N_02 = ", N_02)

    print("N_10 = ", N_10)

    print("N_20 = ", N_20)
    print("N_21 = ", N_21)

    print("N_30 = ", N_30)
    print("N_31 = ", N_31)

    print("Rin_00", Rin_00)
    print("Rin_20", Rin_20)
    print("Rin_30", Rin_30)

    print("Rout_00", Rout_00)
    print("Rout_20", Rout_20)
    print("Rout_30", Rout_30)

    print("th_cut_00", th_cut_00)
    print("th_cut_20", th_cut_20)
    print("th_cut_30", th_cut_30)
    
    print("Ntot_0", Ntot_0)
    print("Ntot_1", Ntot_1)
    print("Ntot_2", Ntot_2)
    print("Ntot_3", Ntot_3)
          
    Ntot_tot = Ntot_0 +  Ntot_1 +  Ntot_2 +  Ntot_3
    print("Ntot_tot", Ntot_0)

    Ncores   = np.int( Ntot_tot / n_cells_per_core )
    Ncores_0 = np.int( Ntot_0 / n_cells_per_core   )
    Ncores_1 = np.int( Ntot_1 / n_cells_per_core   )
    Ncores_2 = np.int( Ntot_2 / n_cells_per_core   )
    Ncores_3 = np.int( Ntot_3 / n_cells_per_core   )
    print("Ncores Patch0 = ", Ncores_0)
    print("Ncores Patch1 = ", Ncores_1)
    print("Ncores Patch2 = ", Ncores_2)
    print("Ncores Patch3 = ", Ncores_3)
    print("Ncores Total  = ", Ncores  )

    print("Min_dt Patch0 = ", min_dx_0*cour)
    print("Min_dt Patch1 = ", min_dx_1*cour)
    print("Min_dt Patch2 = ", min_dx_2*cour)
    print("Min_dt Patch3 = ", min_dx_3*cour)
          
    dt = cour * min_min_dx 
    nt = t_length / dt 
    zone_cycles = Ntot_tot * nt
    n_core_sec =  zone_cycles /  zc_per_sec_core
    BW_SUs = n_core_sec / (16. * 3600.)

    PW_overhead = 1. + 0.3
    PW_hetero_speedup = 2.

    print("old  BW SUs = ", (6.1e7*2000./(4.6e-4*zc_per_sec_core*16.*3600.)))
    print("new  BW SUs = ", BW_SUs)
    print("new  BW SUs = ", BW_SUs*PW_overhead, "   (with PW overhead)")
    print("new  BW SUs = ", BW_SUs*PW_overhead/PW_hetero_speedup, "   (with PW overhead and PW hetergenous time step)")

    print("Wallclock time = ", (BW_SUs*PW_overhead/PW_hetero_speedup)/(Ncores/16)/24., "   (days)")


#     dirout = './'
# 
#     nwin=0
#     fignames = [dirout + 'plot1']
#     print( "fignames = ", fignames)
#     figs = [plt.figure(nwin)]
#     figs[-1].clf()
#     ax = figs[-1].add_subplot(111)
#     ax.set_title('%s' %title)
#     ax.set_xlabel(xlabel)
#     ax.set_ylabel(ylabel)
#     levels = np.linspace(minf,maxf,nlev)
#     rticks = np.linspace(minf,maxf,nticks)
#     
#     CP = ax.contourf(xout, yout,func_slice,levels,cmap=cmap,extend='both')
#     figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')
#     
#     if savepng :
#         for i in np.arange(len(figs)) : 
#             figs[i].savefig('%s.png' %fignames[i], dpi=300)
#             
    print("All done ")
    sys.stdout.flush()
        
    
    return


###########################################################################
##  Routine to run script:
###########################################################################
    
#th_cut = 0.042
th_cut = 0.55
print("th_cut = ", th_cut,  "rad" )
print("th_cut = ", th_cut*180./np.pi, "  deg  ")
mpatch(th_cut,th_cut_20=2.*th_cut,th_cut_30=2.*th_cut)


    
#  
#   ###############################################################################
#   # This allows the python module to be executed like:   
#   # 
#   #   prompt>  python lum
#   # 
#   ###############################################################################
#   if __name__ == "__main__":
#       import sys
#       print(sys.argv)
#       if len(sys.argv) > 1 :
#           gencont(sys.argv[1])
#       else:
#           gencont()

#gencont('rho',filename='SOD*.h5')

