"""  

  shrink_header_arrays():
  ------------
    -- Converts old 3-d dump files to work with new "gather" IO routines in harm3d.
    -- Explicitly, it shrinks the arrays found in "/Header" of length = "nprocs" to 
       length "1". 

"""

# from __future__ import print_function

import numpy as np
import h5py

import os,sys


###############################################################################
###############################################################################
###############################################################################

def shrink_header_arrays(h5file):
    """  
    Returns parameters used in a run, specified by the run's nickname and list of parameter names. 
    """

    h5newfile = 'fixed-'+h5file

    # Get a list of all the parameters in the hdf5 header file:  
    fh5    = h5py.File(h5file,'r')
    fh5new = h5py.File(h5newfile,'w')

    for group in {'/Header', '/Jet', '/Unbound'} : 
        fh5new[group] = fh5[group] 

    all_h5_params = []
#    fh5['/Header'].visit(all_h5_params.append)
    fh5['/Bound'].visit(all_h5_params.append)

    for key in all_h5_params :
        fh5new['/Bound'][key] = [fh5['/Bound'][key][0]]


    print(all_h5_params)

    ####################################################################################
    # We are done searching for parameters, now close up: 
    ####################################################################################
    fh5.close()
    fh5new.close()


###############################################################################
# This allows the python module to be executed like:
#
#   prompt>  python lump
#
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) > 1 :
        shrink_header_arrays(sys.argv[1])
    else:
        sys.exit("Need to specify the hdf5 file, Exiting ....")

