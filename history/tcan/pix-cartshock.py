
from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import h5py
import matplotlib 
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib.colors import Colormap
from scipy.interpolate import interp1d 
from coord_transforms import *
from harm import *
from matplotlib import ticker
from polp_lite import polp 


######################################

rhomin=1.e-10
rhomax=2.

yemin=-10.
yemax=0.5

bsq_min = 1e-10
bsq_max = 100.

gam=5./3.
uumin = 1./(gam-1.)
uumax = 30./(gam-1.)

divb_min = -20.
divb_max = 0.

k = 80
suf='-kslice'
dirout='./plots'

nt = 6
rmax = 15.
run_tag='KDHARM0'


if 1 : 
    kvals = np.arange(0,1)
    xmin = -0.5
    xmax =  0.5
    ymin = -0.5
    ymax =  0.5
    dirout = './plots'
    #for itime in np.arange(136,155) :
    for itime in np.arange(0,20,1) :
        for k in kvals : 
            suf = 'k='+str('%04d' %k)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=False,nperplot=1,use_contourf=False,
                 funcname='rho',minf=rhomin,maxf=rhomax,dolog=False)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=False,nperplot=1,use_contourf=False,
                 funcname='uu',minf=uumin,maxf=uumax,dolog=False)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=False,nperplot=1,use_contourf=False,
                 funcname='bsq',minf=bsq_min,maxf=bsq_max,dolog=False)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=False,nperplot=1,use_contourf=False,
                 funcname='Ye',minf=yemin,maxf=yemax,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=False,nperplot=1,use_contourf=False,
                 funcname='Ye',minf=1e-10,maxf=1.,dolog=False)


# 
