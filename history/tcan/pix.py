
from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import h5py
import matplotlib 
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib.colors import Colormap
from scipy.interpolate import interp1d 
from coord_transforms import *
from harm import *
from matplotlib import ticker
from polp_lite import polp 


######################################

rhomin=-8.
rhomax=-2.

bsq_min = -12.
bsq_max = -4.

uumin = bsq_min
uumax = bsq_max

divb_min = -20.
divb_max = 0.

k = 80
suf='-kslice'
dirout='./plots'

nt = 6
rmax = 15.
run_tag='BBH-DISK-CART-ORIGIN.patch_1'

if 0 :
    #itime = 135
    for itime in np.arange(148,161) : 
        polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,run_tag=run_tag,cart_coords=True,
             funcname='divb',minf=divb_min,maxf=divb_max,dolog=True)
        polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,run_tag=run_tag,cart_coords=True,
             funcname='mask',minf=-1,maxf=3,dolog=False)
        polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,run_tag=run_tag,cart_coords=True,
             funcname='rho',minf=rhomin,maxf=rhomax,dolog=True)
        polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,run_tag=run_tag,cart_coords=True,
             funcname='bsq',minf=bsq_min,maxf=bsq_max,dolog=True)



if 0 : 
    df='rdump_start.patch_1.h5'
    kvals = np.arange(74,83)
    xmin=5
    xmax=10
    ymin=5
    ymax=10
    dirout = './plots0'
    for k in kvals : 
        itime = 0 
        suf = 'k='+str('%04d' %k)+'-close-'
        polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,dumpfile=df,
             funcname='divb',minf=divb_min,maxf=divb_max,dolog=True)
        polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,dumpfile=df,
             funcname='mask',minf=-1,maxf=3,dolog=False)
        polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,dumpfile=df,
             funcname='rho',minf=rhomin,maxf=rhomax,dolog=True)
        polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,dumpfile=df,
             funcname='bsq',minf=bsq_min,maxf=bsq_max,dolog=True)

run_tag='BBH-DISK-CART-ORIGIN.patch_1'

if 0 : 
    kvals = np.arange(74,83)
    xmin=5
    xmax=10
    ymin=5
    ymax=10
    dirout = './plots2'
    for itime in np.arange(135,136) : 
        for k in kvals : 
            suf = 'k='+str('%04d' %k)+'-close-'
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='divb',minf=divb_min,maxf=divb_max,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='mask',minf=-1,maxf=3,dolog=False)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='rho',minf=rhomin,maxf=rhomax,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='bsq',minf=bsq_min,maxf=bsq_max,dolog=True)


if 1 : 
    kvals = np.arange(79,80)
    xmin=5
    xmax=10
    ymin=5
    ymax=10
    dirout = './plots6'
    #for itime in np.arange(136,155) :
    for itime in np.arange(154,155) :
        for k in kvals : 
            suf = 'k='+str('%04d' %k)+'-close-'
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=True,nperplot=1,use_contourf=False,
                 funcname='divb',minf=divb_min,maxf=divb_max,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=True,nperplot=1,use_contourf=False,
                 funcname='mask',minf=-1,maxf=3,dolog=False)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=True,nperplot=1,use_contourf=False,
                 funcname='rho',minf=rhomin,maxf=rhomax,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,plot_grid=True,nperplot=1,use_contourf=False,
                 funcname='bsq',minf=bsq_min,maxf=bsq_max,dolog=True)


if 0 : 
    kvals = np.arange(74,83)
    xmin=5
    xmax=10
    ymin=5
    ymax=10
    dirout = './plots2'
    for itime in np.arange(153,155) : 
        for k in kvals : 
            suf = 'k='+str('%04d' %k)+'-close-'
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='divb',minf=divb_min,maxf=divb_max,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='mask',minf=-1,maxf=3,dolog=False)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='rho',minf=rhomin,maxf=rhomax,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='bsq',minf=bsq_min,maxf=bsq_max,dolog=True)


if 0 : 
    kvals = np.array([156,157])
    kvals = np.append(kvals,np.arange(74,83))
    xmin=None
    xmax=None
    ymin=None
    ymax=None
    dirout = './plots3'
    for itime in np.arange(153,155) : 
        for k in kvals  : 
            suf = 'k='+str('%04d' %k)+'-all-'
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='divb',minf=divb_min,maxf=divb_max,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='mask',minf=-1,maxf=3,dolog=False)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='rho',minf=rhomin,maxf=rhomax,dolog=True)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,run_tag=run_tag,cart_coords=True,namesuffix=suf,
                 funcname='bsq',minf=bsq_min,maxf=bsq_max,dolog=True)




# 
