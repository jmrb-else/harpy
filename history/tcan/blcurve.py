"""

  blcurve.py: 
  ------------

    -- script for integrating frames at different angles and times to
       yield a light curve at each angle.

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################

def tally_fluxes(fileprefix='light_curve'):

    files = sorted(glob.glob(fileprefix+"*.h5"))
    nfiles = len(files)

    fileout = fileprefix+'.h5'
    h5out = h5py.File(fileout,'w')
    
    print("Doing files = ", files)

    itime = 0 
    first_time = True

    for file in files : 
        print("Processing file = ", file)

        h5in = h5py.File(file,'r')
        pix_x1, pix_y1, area1, tout1, img1 = get_bothros_data(h5in)
        flux_tot1 = np.sum(img1)
        flux_tot1 *= area1
        
        if( first_time ) :
            flux_out = np.zeros( (nfiles) )
            times_out = np.zeros( (nfiles) )
            first_time = False
            h5in.copy('/Header',h5out)
            h5in.copy('/freq',h5out)
            h5in.copy('/snap_id',h5out)
            h5in.copy('/units',h5out)

        h5in.close()
        flux_out[itime] = flux_tot1
        times_out[itime] = tout1
        itime += 1 

    dset = h5out.create_dataset('flux_tot',data=flux_out)
    dset = h5out.create_dataset('times',data=times_out)
        
    h5out.close()
        

    return


###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python lum
# 
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)

    if len(sys.argv) > 1 :
        tally_fluxes(sys.argv[1])
    else:
        sys.exit(" need a file prefix " )

