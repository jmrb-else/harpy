
from __future__ import division
from __future__ import print_function

import numpy as np
import h5py
import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib.colors import Colormap
from harm import *


import time 
#sys.path.append("/home/dennis/bin/truncation_analysis")
#sys.path.append("/home/dennis/local/python/bbhdisk")
#from coord_transforms import transform_rank1con
#sys.path.append("/home/dennis/bin")
#from my_write_hdf5 import my_write_hdf5

import pylab

TOP_CARTESIAN   = 0
TOP_SPHERICAL   = 1
TOP_CYLINDRICAL = 2
# fig_width_pt = 246.0
# inches_per_pt = 1.0/72.27
# golden_mean = (np.sqrt(5)-1.0)/2.0
# fig_width = fig_width_pt*inches_per_pt
# fig_height = fig_width*golden_mean
# fig_size =  [fig_width,fig_height]

# params = {'backend': 'ps',
#           'axes.labelsize': 9,
#           'font.size': 9,
#           'legend.fontsize': 9,
#           'legend.loc': "upper right",
#           'legend.frameon': False,
#           'xtick.labelsize': 9,
#           'ytick.labelsize': 9,
#           'text.usetex': True,
#           'figure.figsize': fig_size}

# pylab.rcParams.update(params)




# update_progress() : Displays or updates a console progress bar
## Accepts a float between 0 and 1. Any int will be converted to a float.
## A value under 0 represents a 'halt'.
## A value at 1 or bigger represents 100%
def update_progress(progress,show_time_estimate=True):
    global start
    global first_call
    barLength = 20 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    if(progress == 0):
        progress += 1.e-40
    if( show_time_estimate ):
        elapsedTime = time.time() - start
        remaining = -elapsedTime * (1 - 1. / progress)
    else:
        remaining = 0
    progress = round(progress,3)
    #text = "\rPercent: [{0}] {1}% {2} estimated remaining {3} min".format( "="*block + " "*(barLength-block), progress*100, status,remaining)
    if remaining >= 60.:
        text = "\rPercent: [{0}] {1}% estimated remaining {2}m".format( "="*block + " "*(barLength-block), progress*100, '%03d' % int(remaining/60))
    else:
        text = "\rPercent: [{0}] {1}% estimated remaining {2}s".format( "="*block + " "*(barLength-block), progress*100, '%03d' % int(remaining))
    if first_call:
        first_call = False
    else:
        sys.stdout.write(text)
        sys.stdout.flush()




def main(itime,funcname='rho',dirname='.',show_plot=True,savepdf=False,savepng=False,dolog=False,
         xmin=None,xmax=None,ymin=None,ymax=None,
         minfi=None,maxfi=None,
         minfj=None,maxfj=None,minfk=None,maxfk=None,nlev=256,nticks=10,normalize=False,nperplot=10,
         plot_grid=False,npatches=1,
         islice=0,jslice=0,kslice=None,only_local_patch=False,plot_error=False,dirout='.',myid=None,nwin=0):
    global start
    global first_call
    first_call = True
    start = time.time()
    cmap = plt.cm.viridis
    #cmap = plt.cm.Spectral_r
    #cmap = plt.cm.magma
    Colormap.set_under(cmap,color='k') 
    Colormap.set_over(cmap,color='w') 
    timeid = '%06d' % itime
    for patch in range(npatches):
        if( only_local_patch ):
            patch = patch + 1
        if(myid is None):
            #fullname = dirname + '/KDHARM0.NUMREL.patch_' + str(patch) + '.' + timeid + '.p0000' + str(patch) + '.h5'
            #fullname = dirname + '/KDHARM0.NUMREL.patch_' + str(patch) + '.' + timeid + '.h5'
            #fullname = dirname + '/KDHARM0.patch_' + str(patch) + '.' + timeid + '.h5'
            fullname = dirname + '/BBH-DISK-CART-ORIGIN.patch_' + str(patch) + '.' + timeid + '.h5'
        else:
            fullname = dirname + '/KDHARM0.NUMREL.patch_' + str(patch) + '.' + timeid + '.p0000' + str(myid) + '.h5'
        h5file = h5py.File(fullname,'r')
        dx1  = h5file['Header/Grid/dx1'][0]
        dx2  = h5file['Header/Grid/dx2'][0]
        dx3  = h5file['Header/Grid/dx3'][0]
        t    = h5file['Header/Grid/t'][0]

        if( funcname is not 'divb_calc' ) :
            sys.exit('invalid funcname = ', funcname)
            
        x1   = h5file['x1']
        x2   = h5file['x2']
        x3   = h5file['x3']
        cflag = h5file['cflag']

        slall = slice(None,None,None)

        if( patch == 0 ):
            TOP_TYPE_CHOICE = TOP_SPHERICAL
        else:
            TOP_TYPE_CHOICE = TOP_CARTESIAN
        #TOP_TYPE_CHOICE = h5file['Header/TOP_TYPE_CHOICE'][0]
        if( TOP_TYPE_CHOICE == TOP_SPHERICAL ):
            kslice = int(0.5 * len(x1[0,:,0]))
            x1_kslice   = x1[:,kslice,:]
            x2_kslice   = x2[:,kslice,:]
            #x3_kslice   = x3[:,kslice,:]

            sl_kslice = slice(kslice-1,kslice+1,1)
            slice_obj_tmp = (slall, sl_kslice, slall)
            divb = calc_divb(slice_obj_tmp,h5file)
            func_kslice = np.squeeze(divb[:,1,:])

            cflag_kslice = cflag[:,kslice,:]
            
            x1_kslice = np.append(x1_kslice, np.array([x1_kslice[:,0]]).T, axis=1)
            x2_kslice = np.append(x2_kslice, np.array([x2_kslice[:,0]]).T, axis=1)
            func_kslice = np.append(func_kslice, np.array([func_kslice[:,0]]).T, axis=1)
            cflag_kslice = np.append(cflag_kslice, np.array([cflag_kslice[:,0]]).T, axis=1)
            
        else:
            kslice = int(0.5 * len(x1[0,0,:]))
            x1_kslice   = x1[:,:,kslice]
            x2_kslice   = x2[:,:,kslice]
            #x3_kslice   = x3[:,:,kslice]

            sl_kslice = slice(kslice-1,kslice+1,1)
            slice_obj_tmp = (slall, slall, sl_kslice)
            divb = calc_divb(slice_obj_tmp,h5file)
            func_kslice = np.squeeze(divb[:,:,1])

            cflag_kslice = cflag[:,:,kslice]

        #if(TOP_TYPE_CHOICE == 2 and False):
        #    x1_kslice = x1[:,:,kslice] * np.cos(x2[:,:,kslice])
        #    x2_kslice = x1[:,:,kslice] * np.sin(x2[:,:,kslice])
        #    x3_kslice = x3[:,:,kslice]
        #else:
        #    x1_kslice   = x1[:,:,kslice]
        #    x2_kslice   = x2[:,:,kslice]
        #    x3_kslice   = x3[:,:,kslice]
        
        if(myid is not None):
            x1_1d = x1_kslice[:,0]
            x2_1d = x2_kslice[0,:]
            x1_1d = np.append(x1_1d,np.max(x1_1d) + dx1)
            x2_1d = np.append(x2_1d,np.max(x2_1d) + dx2)
            
            x1_kslice, x2_kslice = np.meshgrid(x1_1d,x2_1d,indexing='ij')

        if(plot_error):
            func_sol = h5file[funcname + '_sol'][:,:,kslice]
            #func_kslice = func_kslice + 2.
            #func_sol = func_sol + 2.
            #func_kslice = np.abs(2. * (func_kslice - func_sol) / (func_kslice + func_sol))
            if(funcname == 'phi'):
                func_kslice = np.abs( (func_kslice - func_sol) / func_sol )
            else:
                func_kslice = np.abs( func_kslice - func_sol )

        xbh1 = h5file['Header/Grid/bh1_traj1'][0]
        ybh1 = h5file['Header/Grid/bh1_traj2'][0]
        xbh2 = h5file['Header/Grid/bh2_traj1'][0]
        ybh2 = h5file['Header/Grid/bh2_traj2'][0]
        h5file.close()
       
        if(dolog):
            func_kslice[func_kslice == 0] += 1.e-12
            func_kslice = np.log10(np.abs(func_kslice))

        print("Largest smallest values of ", funcname, " on patch ", patch, np.min(func_kslice[cflag_kslice == 0]),np.max(func_kslice[cflag_kslice == 0]))
        #if(patch == 0):
        #    print("Index min max ", itime, np.min(func_kslice[cflag_kslice == 0]), np.max(func_kslice[cflag_kslice == 0]))
            
        #print(np.min(func_kslice),np.max(func_kslice))
        fig = plt.figure(nwin)#,figsize=(fig_width,fig_height))
        if patch == 0:
            if minfk is None:
                if funcname == 'cflag':
                    minfk = -1
                else:
                    minfk = np.min(func_kslice[cflag_kslice == 0])
            if maxfk is None:
                if funcname == 'cflag':
                    maxfk = 3
                else:
                    maxfk = np.max(func_kslice[cflag_kslice == 0])

            ax = fig.add_subplot(111,facecolor='k')
            CS = ax.pcolormesh(x1_kslice,x2_kslice,func_kslice,vmin=minfk,vmax=maxfk,shading='gouraud',rasterized=True,cmap=cmap)
            #CS = ax.pcolormesh(x1_kslice,x2_kslice,func_kslice,vmin=minfk,vmax=maxfk,shading='flat',rasterized=True,cmap=cmap)
            fig.colorbar(CS)#,ticks=rticks)#,format='%1.4g')
            if dolog:
                titlename = 'log10(' + funcname + ') t=' + str(t)
            else:
                titlename = funcname + ' ' + 't=' + str(t)
            ax.set_title(titlename)
            ax.set_xlabel('$x$')
            ax.set_ylabel('$y$')
            if( plot_grid ):
                #print("plotting grid patch ", patch)
                for i in range(len(x1_kslice[0,:]))[::nperplot]:
                    ax.plot(x1_kslice[:,i],x2_kslice[:,i],color='k')
                ax.plot(x1_kslice[:,len(x1_kslice[0,:])-1],x2_kslice[:,len(x1_kslice[0,:])-1],color='k')
                for i in range(len(x1_kslice[:,0]))[::nperplot]:
                    ax.plot(x1_kslice[i,:],x2_kslice[i,:],color='k')
                ax.plot(x1_kslice[len(x1_kslice[:,0])-1,:],x2_kslice[len(x1_kslice[:,0])-1,:],color='k')

        else:
            ax = fig.gca()
            if funcname !=  'cflag':
                CS = ax.pcolormesh(x1_kslice,x2_kslice,func_kslice,vmin=minfk,vmax=maxfk,shading='gouraud',rasterized=True,cmap=cmap)
                #CS = ax.pcolormesh(x1_kslice,x2_kslice,func_kslice,vmin=minfk,vmax=maxfk,shading='flat',rasterized=True,cmap=cmap)
                #fig.colorbar(CS)
            
            if( plot_grid ):
                #print("plotting grid patch ", patch)
                if funcname != 'cflag':
                    for i in range(len(x1_kslice[0,:]))[::nperplot]:
                        ax.plot(x1_kslice[:,i],x2_kslice[:,i],color='white')   
                else:
                    ax.plot(x1_kslice[:,0],x2_kslice[:,0],color='white')
                ax.plot(x1_kslice[:,len(x1_kslice[0,:])-1],x2_kslice[:,len(x1_kslice[0,:])-1],color='white')
                if funcname != 'cflag':
                    for i in range(len(x1_kslice[:,0]))[::nperplot]:
                        ax.plot(x1_kslice[i,:],x2_kslice[i,:],color='white')
                else:
                    ax.plot(x1_kslice[0,:],x2_kslice[0,:],color='white')
                ax.plot(x1_kslice[len(x1_kslice[:,0])-1,:],x2_kslice[len(x1_kslice[:,0])-1,:],color='white')
            else:
                #if don't plot grid then at least plot edge of patch
                ax.plot(x1_kslice[:,0],x2_kslice[:,0],color='white')
                ax.plot(x1_kslice[:,len(x1_kslice[0,:])-1],x2_kslice[:,len(x1_kslice[0,:])-1],color='white')
                ax.plot(x1_kslice[0,:],x2_kslice[0,:],color='white')
                ax.plot(x1_kslice[len(x1_kslice[:,0])-1,:],x2_kslice[len(x1_kslice[:,0])-1,:],color='white')

    bh1 = plt.Circle((xbh1,ybh1),0.5,fill=True,color='k')
    bh2 = plt.Circle((xbh2,ybh2),0.5,fill=True,color='k')

    ax.add_artist(bh1)
    ax.add_artist(bh2)
    if xmin is None:
        xmin = np.min(x1_kslice)
    if xmax is None:
        xmax = np.max(x1_kslice)
    if ymin is None:
        ymin = np.min(x2_kslice)
    if ymax is None:
        ymax = np.max(x2_kslice)

    ax.axis([xmin,xmax,ymin,ymax])
        
    if( show_plot ):
        plt.draw()
        #plt.show()

    if( savepng ):
        outname = dirout + '/' + funcname + '_t' + timeid + '_kslice_' + ('%04d' % kslice) + '.png'
        fig.savefig(outname,dpi=200)

    if( savepdf ):
        outname = dirout + '/' + funcname + '_t' + timeid + '_kslice_' + ('%04d' % kslice) + '.pdf'
        fig.savefig(outname,dpi=200)

if __name__ == '__main__':    
    index = sys.argv[1]
    if( len(sys.argv[:]) > 2 ):
        pid = sys.argv[2]
    else:
        pid = None
    #print(index)

    funcname = 'divb_calc'

    minf_def = -20.
    maxf_def =  0.

    main(int(index),funcname=funcname,dirname='.',show_plot=0,savepdf=0,savepng=1,dolog=1,
         xmin=-60,xmax=60,ymin=-60,ymax=60,
        minfi=None,maxfi=None,
        minfj=None,maxfj=None,minfk=minf_def,maxfk=maxf_def,nlev=256,nticks=10,normalize=False,nperplot=50,
        #minfj=None,maxfj=None,minfk=0.99,maxfk=3.01,nlev=256,nticks=10,normalize=False,nperplot=1,
        #minfj=None,maxfj=None,minfk=0,maxfk=5.e-4,nlev=256,nticks=10,normalize=False,nperplot=1,
        #minfj=None,maxfj=None,minfk=0,maxfk=0.02,nlev=256,nticks=10,normalize=False,nperplot=1,
        plot_grid=0,npatches=2,
        islice=None,jslice=None,kslice=None,only_local_patch=0,plot_error=0,dirout='.',myid=pid)
 #    if( funcname != 'cflag' and funcname != 'x1' ):  
#         main(int(index),funcname=funcname,dirname='.',show_plot=1,savepdf=0,savepng=0,dolog=0,
#              minfi=None,maxfi=None,
#              #minfj=None,maxfj=None,minfk=None,maxfk=None,nlev=256,nticks=10,normalize=False,nperplot=1,
#              #minfj=None,maxfj=None,minfk=0.99,maxfk=3.01,nlev=256,nticks=10,normalize=False,nperplot=1,
#              #minfj=None,maxfj=None,minfk=0,maxfk=5.e-4,nlev=256,nticks=10,normalize=False,nperplot=1,
#              minfj=None,maxfj=None,minfk=0,maxfk=None,nlev=256,nticks=10,normalize=False,nperplot=1,
#              plot_grid=0,npatches=2,
#              islice=None,jslice=None,kslice=None,only_local_patch=0,plot_error=1,dirout='.',myid=pid,nwin=100)
    plt.show()
    
