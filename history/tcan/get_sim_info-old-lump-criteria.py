"""get_sim_info.py: 
  ------------
    -- Module that provides the user easy access to parameters relevant to a 
       particular simulation without having;
    -- Provides a simple front-end to a simulation's data; 
    -- In order to add a simulation to the file, look for a similar 
       simulation and emulate its layout. 
    -- Each simulation has a "codename" or "runname" attached to it to look 
       it up easily. 



    LEGEND for various quantities used below:: 

    -- t_lump_all = [t1_1, t1_2, t2_1, t2_2] 
             -- let there be two Criteria:   
                     C1 = {A_1}/{A_0} > 0.2 
                     C2 = {A_1}/{A_2} > 2.5

                  where {A_m} is the amplitude of the m^{th} mode in
                  the theta-integrated density smoothed over 2 periods
                  of the lump's Keplerian orbit (at 1./0.28 times the
                  binary's orbit), as calculated in "find_tlump()" of
                  "all-lump.py".

             where t1_1 and t1_2 are when C1 is satisfied at r=2a and
             r=2.5a respectively, rejecting any times occurring in the
             first 15,000M of evolution (to avoid influence of
             transient artifacts), and selecting the earliest transit
             time of the criterion; t2_1 and t2_2 are when C2 is
             satisfied at r=2a and r=2.5a respectively, using the same
             rejection/selection criteria of t1_1 and t1_2 described
             above.

         -- t_lump_beg : is the maximum of the 3 smallest values of
            (t1_1,t1_2,t2_1,t2_2);

         -- t_lump_end : is always the end of the simulation since the
            lump never goes away once its formed; 

         -- t_lump_exit : is the time the simulation stops exhibiting
            the lump; so far this is only used in inject_disk since it
            was perturbed to get rid of the lump;

"""

from __future__ import division
from __future__ import print_function
import os,sys
import numpy as np
import h5py 

###############################################################################
###############################################################################
###############################################################################

def get_sim_info(param_names, runname='longer2'):
    """  
    Returns parameters used in a run, specified by the run's nickname and list of parameter names. 
    """
    
    ##############################################################################
    # First test the arguments:
    ##############################################################################
    n_params = len(param_names)
    if n_params <= 0 : 
        print("Too few parameters, ", n_params, "  parameters = ", param_names)
        sys.exit("Exiting....")
        

    # Default time unit is  M = M_total = M1 + M2

    hostname = os.environ['HOST']

    base_dirnames = { 'wormhole'            : '/opt/scn/ranger/', 
                      'newhorizons.cluster' : '/work/scn/ranger/', 
                      'gauss'               : '', 
                      'L171LX1'             : '',
                      'gs66-rai'            : '',
                      'bluesky.cluster'     : '/mnt/lustre/scn/' }

    if( hostname not in base_dirnames ) :
        print("Cannot hostname in our list, may update the list?   known hosts = ", keys(base_dirnames))
        sys.exit("Exiting....")

        
    dirname = base_dirnames[hostname]
    
    ##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ## Set parameters not found in dump header for groups of runs: 
    ##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    # Default values: 
    tbeg = 1e-10
    axi_sym_metric = 0
    mass_ratio = 1.0   #  m1 / m2
    density_normalization = 1.0

    tag = 'KDHARM0'
    header_file_name = 'dumps/'+tag+'.000000.h5'

    t_lump_beg = None
    t_lump_end = None
    t_lump_exit = None
    t_lump_exit_all = None
    

    ##:::::::::::::::::::::::::::::::::::::::::
    ## Set parameters not found in dump header for groups of runs: 
    ##:::::::::::::::::::::::::::::::::::::::::

    if( runname in ['orig', 'longer', 'longer2', 'longer3'] ) :
        tmax = 75890.015
        omega_bin_orbit =  .1049229035651877982177e-1
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720
        omega_max   = freq_max * 2.*np.pi
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity
        sigma_0 = 0.095606676
        roa_sigma_max = 2.5628741
        ubh = 0
        version_num = 2.0

    if( runname == 'orig' ) :
        dirname+='project1-prod-mhd-final-s0/'
        tend = 43973.5 
        t_static_beg = 2.e4
        t_static_end = 3.e4
        hor_rt_avg = 0.175     # approximate average of h/r over r=[2a0,5a0] over steady-state period

    if( runname ==  'longer' ) :
        dirname+='project1-prod-mhd-final-s0-longer/'
        tend = 5.39735e+04
        t_static_beg = 3.e4
        t_static_end = 4.e4
        hor_rt_avg = 0.15      # approximate average of h/r over r=[2a0,5a0] over steady-state period

    if( runname == 'longer2' ) :
        dirname+='project1-prod-mhd-final-s0-longer2/'
        tend = 75890.015
        t_static_beg = 4.e4
        t_static_end = tend
        t_lump_all = (49650.00019733, 48870.02768465, 46500.01858833, 51390.01652386)
        t_lump_beg = 49650.00019733
        t_lump_end = tend
        hor_rt_avg = 0.125     # approximate average of h/r over r=[2a0,5a0] over steady-state period

    if( runname == 'longer3' ) :
        dirname+='project1-prod-mhd-final-s0-longer3/'
        tend = 7.39735e+04
        t_static_beg = 4.e4
        t_static_end = 6.e4
        hor_rt_avg = 0.125     # approximate average of h/r over r=[2a0,5a0] over steady-state period

    if( runname == 'proj1_1pn' ) :
        dirname+='project1-1pn-order/'
        tend = 81690.0071313676
        t_static_beg = 4.e4     
        t_static_end = tend-50. 
        hor_rt_avg = 0.125     # UPDATE   
        tmax = 75890.015
        omega_bin_orbit =  .1049229035651877982177e-1
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 1.4408*freq_bin_orbit   #  from r_max_sigma  
        omega_max   = freq_max * 2.*np.pi
        r_max_var = 0.5*( 2.3*20. + 2.25*20. ) # average of value from FFTs of surfdens and luminosity  
        sigma_0 = 0.043358973
        roa_sigma_max = 2.6096567
        ubh = 0
        density_normalization = 0.095606676/sigma_0  # longer's sigma_0 divided by this run's 
        version_num = 2.0

    if( runname == 'proj1_1pn_new2' ) :
        dirname+='project1-1pn-order-new2/'
        tend = 88610.0033361882
        t_static_beg = 4.e4     
        t_static_end = tend-50. 
        hor_rt_avg = 0.125     # UPDATE   
        tmax = 75890.015
        omega_bin_orbit =  .1049229035651877982177e-1  
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi)    
        freq_max    = 1.4408*freq_bin_orbit   #  from r_max_sigma  UPDATE
        omega_max   = freq_max * 2.*np.pi
        r_max_var = 0.5*( 2.3*20. + 2.25*20. ) # average of value from FFTs of surfdens and luminosity   UPDATE
        sigma_0 = 0.095682847 
        roa_sigma_max = 2.6096567           # UPDATE
        ubh = 0
        density_normalization = 0.095606676/sigma_0  # longer's sigma_0 divided by this run's   UPDATE
        version_num = 2.0


    if( runname == 'proj1_q2' ) :
        dirname+='project1.1/q=1_o_2/'
        tend = 1.067528e+05
        t_static_beg = 4.e4     
        t_static_end = tend-50.
        t_lump_all = (68610.03590287, 59490.01708028, 64830.03121588, 76380.03758762)
        t_lump_beg = 68610.03590287
        t_lump_end = tend
        hor_rt_avg = 0.125      # UPDATE  
        mass_ratio = 1./2.
        tmax = 75890.015
        # tmax = 1.1e5
        omega_bin_orbit =  1.0478933822271228e-02
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720         # UPDATE
        omega_max   = freq_max * 2.*np.pi    # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity  # UPDATE
        sigma_0 = 0.082613745
        roa_sigma_max = 2.5628741
        ubh = 0
        version_num = 2.0

    if( runname == 'proj1_q5' ) :
        dirname+='project1.1/q=1_o_5/'
        tend = 9.4769888933956361e+04
        t_static_beg = 4.e4     
        t_static_end = tend-50.
        t_lump_all = (None,88200.01333604,None,None)
        t_lump_beg = None
        t_lump_end = tend
        hor_rt_avg = 0.125      # UPDATE  
        mass_ratio = 1./5.
        tmax = 75890.015
        # tmax = 1.1e5 
        omega_bin_orbit =  1.0438569448494870e-02
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720         # UPDATE
        omega_max   = freq_max * 2.*np.pi    # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity  # UPDATE
        sigma_0 = 0.082337298
        roa_sigma_max = 2.5628741  #UPDATE
        ubh = 0
        version_num = 2.0

    if( runname == 'proj1_q10' ) :
        dirname+='project1.1/q=1_o_10/'
        tend =  9.7474158737822378e+04
        t_static_beg = 4.e4     
        t_static_end = tend-50.
        t_lump_all = (None,None,None,None)
        t_lump_beg = None
        t_lump_end = tend
        hor_rt_avg = 0.125      # UPDATE  
        mass_ratio = 1./10.
        tmax = 75890.015
        # tmax = 1.1e5  
        omega_bin_orbit =  1.0411070568031828e-02
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi)  
        freq_max    = 0.0024543720         # UPDATE
        omega_max   = freq_max * 2.*np.pi    # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity  # UPDATE
        sigma_0 = 0.082138927
        roa_sigma_max = 2.5628741  #UPDATE
        ubh = 0
        version_num = 2.0

    if( runname == 'bigger_disk' ) :
        dirname+='project1-init-data/bigger-disk/'
        #  tend =  94860.0125513676
        tend =  1.577523e+05
        # t_static_beg = 4.e4     # UPDATE
        t_static_beg = 8.5e4     
        t_static_end = tend-50.
        t_lump_all = (130080.0177209, 124500.00946071, 110700.01004851, 131640.00838102)
        t_lump_beg = 130080.0177209
        t_lump_end = tend
        hor_rt_avg = 0.125      # UPDATE  
        #  tmax = 75890.015
        #  tmax =  94860.
        tmax =  1.577523e+05
        omega_bin_orbit =  .1049229035651877982177e-1
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720          # UPDATE
        omega_max   = freq_max * 2.*np.pi          # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity           # UPDATE
        sigma_0 = 0.087432780
        roa_sigma_max = 2.5628741           # UPDATE
        ubh = 0
        version_num = 2.0

    if( runname == 'medium_disk' ) :
        dirname+='project1-init-data/medium-disk/'
        tend =  142850.006256282
        t_static_beg = 7.e4     # UPDATE
        t_static_end = tend-50.
        t_lump_all = (76710.01967885, 49170.02574013, 73440.00927692, 90180.01404265)
        t_lump_beg = 76710.01967885
        t_lump_end = tend
        hor_rt_avg = 0.125      # UPDATE  
        tmax =  142850.006256282
        omega_bin_orbit =  .1049229035651877982177e-1
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720          # UPDATE
        omega_max   = freq_max * 2.*np.pi          # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity           # UPDATE
        sigma_0 = 0.084682412    
        roa_sigma_max = 2.5628741           # UPDATE
        ubh = 0
        version_num = 2.0


    if( runname == 'bigger_disk_medres' ) :
        dirname+='project1-init-data/bigger-disk-medres/'
        tend =  2.6803888092569923e+04
        t_static_beg = 100.      #UPDATE
        t_static_end = tend-50. 
        hor_rt_avg = 0.125      # UPDATE  
        tmax =  2.6803888092569923e+04
        omega_bin_orbit =  .1049229035651877982177e-1
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720          # UPDATE
        omega_max   = freq_max * 2.*np.pi          # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity           # UPDATE
        sigma_0 = 0.087432780    # UPDATE 
        roa_sigma_max = 2.5628741           # UPDATE
        ubh = 0
        version_num = 2.0

    if( runname == 'bigger_disk_hires' ) :
        dirname+='project1-init-data/bigger-disk-hires/half/'
        tend =  2.007968e+03
        t_static_beg = 100. # UPDATE 
        t_static_end = tend-50. 
        hor_rt_avg = 0.125      # UPDATE  
        tmax =  2.007968e+03
        omega_bin_orbit =  .1049229035651877982177e-1
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720          # UPDATE
        omega_max   = freq_max * 2.*np.pi          # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity           # UPDATE
        sigma_0 = 0.087432780    # UPDATE 
        roa_sigma_max = 2.5628741           # UPDATE
        ubh = 0
        version_num = 2.0

    if( runname == 'inject_disk' ) :
        dirname+='project1-init-data/inject-flux/'
        tbeg = 5.e4
        tend =   1.259300e+05
        t_static_beg = 8.0e4   
        t_static_end = tend-50.
        t_lump_all = (108390.01719859, 101460.01571152, 104070.00613571, 114870.00339985)
        t_lump_beg = 108390.01719859
        t_lump_end = tend
        t_lump_exit_all = (51900.00257942, 54780.01339766, 53970.0048454, 51720.0139199 )
        t_lump_exit = 53970.0048454
        hor_rt_avg = 0.125      # UPDATE  
#         tmax = 75890.015
#         tmax =   105850.
        tmax =   1.259300e+05
        omega_bin_orbit =  .1049229035651877982177e-1
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720
        omega_max   = freq_max * 2.*np.pi
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity
        sigma_0 = 0.095606676
        roa_sigma_max = 2.5628741
        ubh = 0
        version_num = 2.0

    if( runname == 'asep50' ) :
        dirname+='project1-init-data/asep-50/'
        tend =  4.570189e+05
        t_static_beg = 1.e5     # UPDATE
        t_static_end = tend-50. # UPDATE
        hor_rt_avg = 0.125      # UPDATE  
        tmax = 4.570189e+05
        omega_bin_orbit =  2.7543746714425651e-3
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720          # UPDATE
        omega_max   = freq_max * 2.*np.pi          # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 )*50./20. # average of value from FFTs of surfdens and luminosity           # UPDATE
        sigma_0 = 0.059350101               
        roa_sigma_max = 2.5628741           # UPDATE
        ubh = 0
        version_num = 2.0

    if( runname == 'asep100' ) :
        dirname+='project1-init-data/asep-100/'
        tend = 9.637382e+05
        t_static_beg = 1.e5     # UPDATE
        t_static_end = tend-50. # UPDATE
        hor_rt_avg = 0.125      # UPDATE  
        tmax = 9.637382e+05
        omega_bin_orbit =  9.8658656054412090e-4
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720          # UPDATE
        omega_max   = freq_max * 2.*np.pi          # UPDATE
        r_max_var = 0.5*( 45.847966 + 46.286004 )*100./20. # average of value from FFTs of surfdens and luminosity           # UPDATE
        sigma_0 = 0.042612190
        roa_sigma_max = 2.5628741           # UPDATE
        ubh = 0
        version_num = 2.0

    if( runname == 'bog1' ) :
        # First publishable black-holes-on-the-grid run 
        dirname+='bog/bog1/'
        tend = 88610.0033361882 # UPDATE
        t_static_beg = 4.e4     # UPDATE
        t_static_end = tend-50. # UPDATE
        hor_rt_avg = 0.1      # UPDATE  
        tmax = 75890.015  #UPDATE
        omega_bin_orbit =  .1049229035651877982177e-1    #UPDATE
        freq_bin_orbit = omega_bin_orbit / (2.*np.pi) 
        freq_max    = 0.0024543720              #UPDATE
        omega_max   = freq_max * 2.*np.pi      
        r_max_var = 0.5*( 45.847966 + 46.286004 ) # average of value from FFTs of surfdens and luminosity  #UPDATE
        sigma_0 = 0.095606676         #UPDATE
        roa_sigma_max = 2.5628741         #UPDATE
        ubh = 0
        version_num = 2.0

    if( runname in [ 'thickhr_a00'             ,  
                     'medlr_a00'               , 
                     'medhr_a00'               , 
                     'thinlr_a00'              , 
                     'thinhr_a00'              , 
                     'thinhr_a05'              , 
                     'thinhr_a05_reboot'       , 
                     'thinhr_a09'              , 
                     'thinhr_a099'             , 
                     'thinhr_a099_newcodetest' , 
                     'thinhr_a099_reboot'      , 
                     'thinhr_a00_coronacool'   ] ) : 
        tmax = 17106.
        ubh = 1 
        version_num = 1.0
        axi_sym_metric = 1


    if( runname == 'thickhr_a00'            ) : 
        dirname='/data1/rayleigh/harm3d/archive/3d/cooling/a=0.0/h_o_r=0.16/348x160x64_07282009/'
        tend = 13666.
        t_static_beg = 8000.
        t_static_end = tend
        hor_rt_avg = 0.17     # approximate average of h/r over steady-state period

    if( runname == 'medlr_a00'              ) : 
        dirname='/data1/rayleigh/harm3d/archive/3d/cooling/a=0.0/h_o_r=0.08/192x192x64_01082009/'
        tend = 15096.
        t_static_beg = 5.e3
        t_static_end = 8.e3
        hor_rt_avg = 0.091      # approximate average of h/r over steady-state period

    if( runname == 'medhr_a00'              ) : 
        dirname='/data1/rayleigh/harm3d/archive/3d/cooling/a=0.0/h_o_r=0.08/512x160x64_06102009/'
        tend = 12500.
        t_static_beg = 5.e3
        t_static_end = 12.5e3
        hor_rt_avg = 0.1      # approximate average of h/r over steady-state period

    if( runname == 'thinlr_a00'             ) : 
        dirname='/data1/rayleigh/harm3d/archive/3d/cooling/a=0.0/h_o_r=0.05/192x192x64_08252008/'
        tend = 12660.
        t_static_beg = 4.5e3
        t_static_end = 11.5e3
        hor_rt_avg = 0.085    # approximate average of h/r over steady-state period

    if( runname == 'thinhr_a00'             ) : 
        dirname='/data1/rayleigh/harm3d/archive/3d/cooling/a=0.0/h_o_r=0.05/912x160x64_05262009/'
        tend = 15223.
        t_static_beg = 10.e3
        t_static_end = 15.e3
        hor_rt_avg = 0.061    # approximate average of h/r over steady-state period

    if( runname == 'thinhr_a05'             ) : 
        dirname='/data2/rayleigh/harm3d/archive/3d/cooling/a=0.5/h_o_r=0.05/HR07_27_2011/'
        tend = 14698.4063104731
        t_static_beg = 10.e3  #UPDATE 
        t_static_end = tend   #UPDATE 
        hor_rt_avg = 0.06     #UPDATE approximate average of h/r over steady-state period
        header_file_name = 'dumps/KDHARM0.000200.h5'     # this run was restarted with a different number of cores

    if( runname == 'thinhr_a05_reboot'      ) : 
        dirname='/data2/rayleigh/harm3d/archive/3d/cooling/a=0.5/h_o_r=0.05/HR04_04_2013_reboot/'
        tend = 1.733527e+04
        t_static_beg = 10.e3  #UPDATE 
        t_static_end = tend   #UPDATE 
        hor_rt_avg = 0.06     #UPDATE approximate average of h/r over steady-state period
        header_file_name = 'dumps/KDHARM0.000400.h5'     # this run was restarted with a different number of cores

    if( runname == 'thinhr_a09'             ) : 
        dirname='/data4/rayleigh/harm3d/archive/3d/cooling/a=0.9/h_o_r=0.05/HR07_27_2011/'
        tend = 16815.
        t_static_beg = 10.e3  
        t_static_end = tend   
        hor_rt_avg = 0.06     #UPDATE approximate average of h/r over steady-state period

    if( runname == 'thinhr_a099'            ) : 
        dirname='/data3/rayleigh/harm3d/archive/3d/cooling/a=0.99/h_o_r=0.05/HR07_27_2011/'
        tend = 17106.
        t_static_beg = 10.e3  #UPDATE 
        t_static_end = tend   #UPDATE 
        hor_rt_avg = 0.06     #UPDATE approximate average of h/r over steady-state period
        header_file_name = 'dumps/KDHARM0.000200.h5'     # this run was restarted with a different number of cores

    if( runname == 'thinhr_a099_newcodetest') : 
        dirname='/data3/rayleigh/harm3d/archive/3d/cooling/a=0.99/h_o_r=0.05/HR06_12_2012/'
        tend = 9131.
        t_static_beg = 8.e3  #UPDATE 
        t_static_end = tend   #UPDATE 
        hor_rt_avg = 0.06     #UPDATE approximate average of h/r over steady-state period
        header_file_name = 'dumps/KDHARM0.000400.h5'     # this run was restarted with a different number of cores

    if( runname == 'thinhr_a099_reboot'     ) : 
        dirname='/data3/rayleigh/harm3d/archive/3d/cooling/a=0.99/h_o_r=0.05/HR06_01_2012/'
        tend = 16073.
        t_static_beg = 12.e3 
        t_static_end = 15.e3
        hor_rt_avg = 0.06     #UPDATE approximate average of h/r over steady-state period
        header_file_name = 'dumps/KDHARM0.000400.h5'     # this run was restarted with a different number of cores

    if( runname == 'thinhr_a00_coronacool'  ) : 
        # run with new corona cooling function, starts from thinhr_a00 snapshot
        dirname='/data1/rayleigh/harm3d/archive/3d/cooling/a=0.0/h_o_r=0.05/912x160x64_09062013/'
        tend = 16060.0012738601
        t_static_beg = 11.e3  #UPDATE 
        t_static_end = tend   #UPDATE 
        hor_rt_avg = 0.06     #UPDATE approximate average of h/r over steady-state period
        header_file_name = 'dumps/KDHARM0.000500.h5'     # this run was restarted with a different number of cores
        version_num = 2.0


    #####################################################
    # select file to read in header information:
    #####################################################
    header_file = dirname+header_file_name

    #####################################################
    # select name of BBH trajectory information:
    #####################################################
    traj_file=dirname+'KDHARM0_bbh_trajectory.out'

    #####################################################
    # DUMP or 3-d snapshot dump file name (without time index string):
    #####################################################
    dump_name=dirname+'dumps/'+tag

    #####################################################
    # GDUMP or geometry dump file name :
    #####################################################
    gdump_name=dump_name+'.gdump.h5'

    #####################################################
    # SURFACE DUMP  file name (without time index string):
    #####################################################
    surf_name=dump_name+'.surface'

    #####################################################
    # History file name :
    #####################################################
    hist_name=dirname+'history/'+tag+'.history'

    #####################################################
    # Default times for snapshots to be displayed:
    #####################################################

    n_special = 4l
    dttmp = (t_static_end-t_static_beg)/(n_special-1l)
    t_special1 = t_static_beg + dttmp * np.arange(n_special)

    dttmp2 = (tend-t_static_end)/(n_special-1l)
    t_special2 = t_static_end + dttmp2 * np.arange(n_special)

    dttmp2 = (tend-3e4)/(n_special-1l)
    t_special3 = 3e4 + dttmp2 * np.arange(n_special)


    # Exceptions:
    if( runname in ['longer2', 'proj1_1pn', 'proj1_1pn_new2', 'proj1_q2', 'proj1_q5', 'proj1_q10'] ) :
        tend_bak = tend
        tend = 75890.015
        t_static_end = tend
        dttmp = (t_static_end-t_static_beg)/(n_special-1l)
        t_special2 = t_static_beg + dttmp * np.arange(n_special)
        tend = 75890.015
        t_static_beg = 4.e4
        t_static_end = tend
        dttmp = (t_static_end-t_static_beg)/(n_special-1l)
        t_special1 = t_static_beg + dttmp * np.arange(n_special)
        tend=tend_bak

    if( runname in ['asep50', 'asep100'] ) :
        t_static_beg=0.66*tend
        t_static_end=tend
        t_special1=[0.5*tend,0.7*tend,0.85*tend,tend]
        t_special2=t_special1


    #####################################################
    # Dictionary of  name/value pairs that are extrinsic (post-processed) from a run:
    #   -- all of these should have been set already: 
    #####################################################
    pvals = { 
        'path'                     : dirname, 
        'dirname'                  : dirname, 
        'traj_file'                : traj_file, 
        'header_file'              : header_file,
        'dump_name'                : dump_name,
        'surf_name'                : surf_name,
        'gdump_name'               : gdump_name,
        'hist_name'                : hist_name,
        'tbeg'                     : tbeg, 
        'tend'                     : tend,
        't_static_beg'             : t_static_beg,
        't_static_end'             : t_static_end,
        't_lump_all'               : t_lump_all,
        't_lump_beg'               : t_lump_beg,
        't_lump_end'               : t_lump_end,
        't_lump_exit'              : t_lump_exit,
        't_lump_exit_all'          : t_lump_exit_all,
        'runname'                  : runname,
        'tmax'                     : tmax,
        'ubh'                      : ubh,
        't_special1'               : t_special1,
        't_special2'               : t_special2,
        'hor_rt_avg'               : hor_rt_avg,
        'sigma_0'                  : sigma_0,
        'hostname'                 : hostname,
        'omega_bin_orbit'          : omega_bin_orbit,
        'freq_bin_orbit'           : freq_bin_orbit,
        'freq_max'                 : freq_max,
        'omega_max'                : omega_max,
        'roa_sigma_max'            : roa_sigma_max,
        'r_max_var'                : r_max_var,
        't_special3'               : t_special3,
        'axi_sym_metric'           : axi_sym_metric,
        'density_normalization'    : density_normalization ,
    }

    #####################################################
    # Build output dictionary from the extrinsic dictionary:
    #   -- we return only the requested parameters;
    #####################################################

    param_names = set(param_names) 

    out_dict = dict([]) 

    for key in param_names.intersection(pvals.keys()) : 
        out_dict[key] = pvals[key]

        
    remaining_params = param_names - set(pvals.keys())

    #####################################################
    # Now read parameters stored in the header file : 
    #####################################################

    # List of Aliases: 
    #  -- aliases override parameter lookup so that requesting n1 will look 
    #     for totalsize1  instead of "N1" in the headerfile. 

    n_remain = len(remaining_params) 

    if (n_remain > 0) : 

        available_aliases = { 
            'hist_freq'  : 'DT_out0'               ,
            'ascii_freq' : 'DT_out1'               ,
            'sdf_freq'   : 'DT_out2'               ,
            'dump_freq'  : 'DT_out3'               ,
            'image_freq' : 'DT_out4'               ,
            'stat_freq'  : 'DT_out5'               ,
            'stat2_freq' : 'DT_out6'               ,
            'rad_freq'   : 'DT_out7'               ,
            'surf_freq'  : 'DT_out9'               ,
            'n0'         : 'totalsize0'            ,
            'n1'         : 'totalsize1'            ,
            'n2'         : 'totalsize2'            ,
            'n3'         : 'totalsize3'            ,
            'rmin'       : 'Rin'                   ,
            'rmax'       : 'Rout'                  ,
            'rsep0'      : 'initial_bbh_separation' }


        # Get a list of all the parameters in the hdf5 header file:  
        fh5 = h5py.File(header_file,'r')
        all_h5_params = []
        fh5.visit(all_h5_params.append) 

        # Allow the user to use aliases for certain parameters, perform mapping:
        aliases = dict([])
        for key in remaining_params.intersection(available_aliases.keys()) :
            aliases[key] = available_aliases[key]

        n_aliases = len(aliases)
        
        print("out_dict = ", out_dict) 
        print("available_aliases = ", available_aliases) 
        print("aliases = ", aliases) 
        print("n_aliases = ", n_aliases) 
        print("remaining_params = ", remaining_params) 
        print("intersection = ", remaining_params.intersection(available_aliases.keys())  )
        
        if (n_aliases > 0) : 
            # Mapping to parameter name to object's full path name:
            alias_to_objnames = dict([])
            for key,substring in aliases.items() : 
                f = [ x for x in all_h5_params if substring in x  ]
                alias_to_objnames[key] = f[0] 
                    
            # Make sure that all the values() in the aliases are available in the header file:
            if len(alias_to_objnames) != n_aliases : 
                print('Some aliase values are not existent in the header file : ', aliases.values(), all_h5_params)
                sys.exit("Exiting....")

            # Now add the aliases parameters to the dictionary to be returned:  
            for k in aliases.keys() : 
                out_dict[k] = fh5[alias_to_objnames[k]].value

            # Update remaining parameters: 
            remaining_params = remaining_params - set(aliases.keys())
            n_remain = len(remaining_params) 



        # This is a redundant check if there is no overlap with aliases in param_names, but better than repeating code:
        if (n_remain > 0) : 
            tmp_names = [ x for x in remaining_params if 'ncpu' in x  ]
            tmp_dict = dict([])
            if (len(tmp_names) > 0) : 
                needed_vars = ['totalsize1', 'totalsize2', 'totalsize3', 'N1', 'N2', 'N3']
                for k in needed_vars : 
                    varnames = [ x for x in all_h5_params if k in x ]
                    if (len(varnames) <= 0 ) : 
                        print('Missing parameters : ', k, all_h5_params)
                        sys.exit("Exiting....")
                        
                    tmp_dict[k] = fh5[varnames[0]].value

                out_dict['ncpux1'] =  tmp_dict['totalsize1'].item() / tmp_dict['N1'].item() 
                out_dict['ncpux2'] =  tmp_dict['totalsize2'].item() / tmp_dict['N2'].item() 
                out_dict['ncpux3'] =  tmp_dict['totalsize3'].item() / tmp_dict['N3'].item() 
                out_dict['ncpus']  = out_dict['ncpux1'].item() * out_dict['ncpux2'].item() * out_dict['ncpux3'].item() 
                
                new_names = needed_vars + ['ncpux1', 'ncpux2', 'ncpux3', 'ncpus']

                remaining_params = remaining_params - new_names
                n_remain = len(remaining_params) 


        # Again, this is a redundant check if the previous block was
        # not executed, but if there are remaining requested
        # parameters that have not been set, then we need to probe parameter file: 
        if (n_remain > 0) : 
            if( remaining_params.intersection(['roa_min', 'roa_max']) ) : 
                if( ubh == 0 ) :
                    if( 'Rin' in out_dict.keys() ) : 
                        Rin = out_dict['Rin'].item() 
                    else : 
                        Rin = [ x for x in all_h5_params if 'Rin' in x  ][0].value

                    if( 'Rout' in out_dict.keys() ) : 
                        Rout = out_dict['Rout'].item() 
                    else : 
                        Rout = [ x for x in all_h5_params if 'Rout' in x  ][0].value
                        out_dict['Rout'] = Rout

                    if( 'initial_bbh_separation' in out_dict.keys() ) : 
                        initial_bbh_separation = out_dict['initial_bbh_separation'].item() 
                    else : 
                        if( 'rsep0' in out_dict.keys() ) : 
                            initial_bbh_separation = out_dict['rsep0'].item() 
                        else : 
                            initial_bbh_separation = [ x for x in all_h5_params if 'initial_bbh_separation' in x  ][0].value
                        
                    out_dict['roa_min'] = Rin  / initial_bbh_separation
                    out_dict['roa_max'] = Rout / initial_bbh_separation
                    out_dict['Rin']     = Rin
                    out_dict['Rout']    = Rout
                    out_dict['initial_bbh_separation'] = initial_bbh_separation

                else : 
                    print('Invalid parameter request given  ubh=0 for  "roa_min" and "roa_max" : ')
                    sys.exit("Exiting....")

                remaining_params = remaining_params - ['roa_min', 'roa_max', 'Rin', 'Rout', 'initial_bbh_separataion']
                n_remain = len(remaining_params) 

        
        # The final lookup, searching through the available parameters in the header file: 
        if (n_remain > 0) : 
            for k in remaining_params :
                varnames = [ x for x in all_h5_params if k in x ]
                if( len(varnames) <= 0 ) : 
                    print('Parameter not found anywhere : ', k )
                else: 
                    out_dict[k] = varnames[0].value   # always chooose the first instance of the variable 

        ####################################################################################
        # We are done searching for parameters, now close up: 
        ####################################################################################
        fh5.close()

    return out_dict


