
from __future__ import division
from __future__ import print_function
from scipy.fftpack import fft,fftfreq,rfft,rfftfreq
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal
import scipy.interpolate as interp
import os,sys

####################################################################
####################################################################
####################################################################

def my_fft(t_out, f_out, axis=-1) : 
    """  
    my_fft(): 
    ------------
    -- wrapper to just get what you want from the fft with a time-dependent function;
    
    """

    nt = len(t_out)

    if( nt <= 1 ) : 
        sys.exit("my_fft(): Bad length of t_out = ", t_out)
        
    dt = (t_out[-1] - t_out[0]) / nt 
    if (nt % 2 == 0) : 
        nthalf = int(nt/2)
    else : 
        nthalf = int( (nt-1)/2 + 1 )

    #freq_out = np.linspace(0.,1./(2.*dt),nthalf)
    amps = rfft(f_out,axis=axis) 
    freq_out = rfftfreq(nt,d=dt)
    abs_amp = np.abs(amps) * 2. / nt 

    return freq_out, abs_amp, amps


####################################################################
####################################################################
# Calculates the temporal power spectrum of a time-series;
####################################################################

def my_psd(t_out, f_out, axis=-1, fourier_type='fft', make_uniform=False) :
    
    """  
    my_psd(): 
    ------------
    -- wrapper to just get what you want from the fft with a time-dependent function;
    
    """

    fbak = f_out
    tbak = t_out

    nt = len(t_out)

    if( nt <= 1 ) : 
        sys.exit("my_fft(): Bad length of t_out = ", t_out)
        

    # Check to see if time coordinate is uniform:
    var = np.abs(np.std(np.gradient(t_out)/np.max(t_out[-1])))
    if( var > 1e-12 ) :
        print("my_psd(): t_out is not quite uniform, var = ", var)
        if( make_uniform ) :
            print("my_psd(): Making it uniform....")
            t_out = np.linspace(tbak[0],tbak[-1],len(tbak))
            f_out = interp.interp1d(tbak,fbak,kind='cubic')(t_out)
        else:
            print("my_psd(): You may want to make the time series uniformly spaced else your PSD may be noisy (unless you're using Lomb-Scarlge ")
            
    nt = len(t_out)
    if( nt <= 1 ) : 
        sys.exit("my_fft(): Bad length of t_out = ", t_out)
    dt = (t_out[-1] - t_out[0]) / nt
    rate = 1./dt

    if (nt % 2 == 0) : 
        nthalf = int(nt/2)
    else : 
        nthalf = int( (nt-1)/2 + 1 )
            
    #freq_out = np.linspace(0.,1./(2.*dt),nthalf)
    if( (fourier_type.lower() == 'rfft') or (fourier_type.lower() == 'fft') ):
        print("Performing FFT.... ")
        amps = rfft(f_out,axis=axis) 
        freq_out = rfftfreq(nt,d=dt)
        abs_amp = np.abs(amps) * 2. / nt
        
    elif( fourier_type.lower() == 'periodogram' ) :
        print("Performing Periodogram... ")
        freq_out, abs_amp = signal.periodogram(f_out, rate, 'blackmanharris', scaling='spectrum')
        amps = np.sqrt(abs_amp*2)
        abs_amp = amps

    elif( fourier_type.lower()  == 'welch' ) :
        print("Performing Welch...")
        # This choice of nperseg/noverlap seems to give the highest resolution power spectrum:
        nperseg = nt
        #nperseg = 1024
        noverlap = nperseg-1
        #freq_out, abstmp = signal.welch(f_out, rate, window='hamming',scaling='spectrum',nperseg=nperseg,noverlap=noverlap)
        #freq_out, abstmp = signal.welch(f_out,rate,window='hamming', nperseg = nt//2, nfft=10*(nt*2+1), return_onesided=True, scaling='spectrum',detrend='linear')
        freq_out, abstmp = signal.welch(f_out,rate,window='hamming', nperseg=nperseg,noverlap=noverlap, nfft=10*(nt*2+1), return_onesided=True, scaling='spectrum',detrend='linear')
        abs_amp = np.sqrt(abstmp*2)
        amps = abs_amp 

    else :
        print("Performing Lomb-Scargle...")
        freq_out = rfftfreq(nt,d=dt)[1:-1]
        tlomb = t_out * 2.*np.pi
        abstmp = signal.lombscargle(tlomb, f_out, freq_out, normalize=False)
        abs_amp = np.sqrt(abstmp*4/nt)
        amps = abs_amp 

    f_out = fbak
    t_out = tbak
        
    return freq_out, abs_amp, amps


####################################################################
####################################################################
####################################################################

def test_my_fft() :

    print("testing my_fft ")

    if 0 : 
        fs = 10e3
        N = int(1e5)
        amp = 2*np.sqrt(2)
        freq = 1270.0
        noise_power = 0.001 * fs / 2
        time = np.arange(N) / fs
        y = amp*np.sin(2*np.pi*freq*time)

    if 1 : 
        # Number of sample points
        N = int(600)
        # sample spacing
        T = 1.0 / 800.0
        fs = T 
        x = np.linspace(0.0, N*T, N) + 1e-4*np.random.random_sample(N)
        y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)
        
    
    if( 1 ) : 
        yf = rfft(y)
        yf *= 2./N 
        #xf = fftfreq(0.0, 1.0/(2.0*T), N/2)
        xf = rfftfreq(N,d=(1./fs))
        fig = plt.figure(0)
        fig.clf()
        ax = fig.add_subplot(111)
        ax.set_xlim(0.,1.5e-4)
        ax.plot(xf, np.abs(yf))
        #plt.grid()
        fig.savefig('test1.png' , dpi=300,bbox_inches='tight')

    if( 1 ) :
#    x += np.random.normal(scale=np.sqrt(noise_power), size=time.shape)

        xf, yf = signal.periodogram(y, fs, 'blackmanharris', scaling='spectrum')
        #    f, Pper_spec = signal.periodogram(x, fs, 'nuttall', scaling='spectrum')
#    f, Pper_spec = signal.welch(x, fs, scaling='spectrum')
#    f, Pper_spec = signal.welch(x, fs, window='nuttall',scaling='spectrum')
    
        fig = plt.figure(1)
        fig.clf()
        #plt.semilogy(xf, yf)
        ax = fig.add_subplot(111)
        ax.set_xlim(0.,1.5e-4)
        ax.plot(xf, yf)
        ax.set_xlabel('frequency [Hz]')
        ax.set_ylabel('PSD')
        #plt.grid()
        #plt.show()
        fig.savefig('test2.png' , dpi=300,bbox_inches='tight')


####################################################################
####################################################################
####################################################################

def test_my_fft2() :

    print("testing my_fft ")

    if 0 : 
        fs = 10e3
        N = int(1e5)
        amp = 2*np.sqrt(2)
        freq = 1270.0
        noise_power = 0.001 * fs / 2
        time = np.arange(N) / fs
        y = amp*np.sin(2*np.pi*freq*time)
        t_out = time
        f_out = y 

    if 1 : 
        # Number of sample points
        N = int(2000)
        # sample spacing
        T = 1.0 / 800.0
        fs = T 
        w1 = 50.
        w2 = 80.
        freqmin, freqmax = [0.9*w1,1.1*w2]
        xorig = np.linspace(0.0, N*T, N) + 1e-2*np.random.random_sample(N)
        yorig = np.sin(w1 * 2.0*np.pi*xorig) + 0.5*np.sin(w2 * 2.0*np.pi*xorig)
        x = np.linspace(np.min(xorig),np.max(xorig),len(xorig))
        y = interp.interp1d(xorig,yorig,kind='cubic')(x)
        #xorig = np.linspace(0.0, N*T, N)
        print("vars = ", np.std(np.gradient(xorig)/np.max(xorig)), np.std(np.gradient(x)/np.max(x)))
        x=xorig
        y = np.sin(w1 * 2.0*np.pi*xorig) + 0.5*np.sin(w2 * 2.0*np.pi*xorig)
        t_out = x
        f_out = y 

        

    freq_out0, abs_amp0, amps = my_psd(t_out, f_out, make_uniform=True,fourier_type='fft')
    freq_out1, abs_amp1, amps = my_psd(t_out, f_out, make_uniform=True,fourier_type='periodogram')
    freq_out2, abs_amp2, amps = my_psd(t_out, f_out, make_uniform=True,fourier_type='welch')
    freq_out3, abs_amp3, amps = my_psd(t_out, f_out, make_uniform=True,fourier_type='lomb-scargle')
    print("len(freq0) = ",len(freq_out0))
    print("len(freq1) = ",len(freq_out1))
    print("len(freq2) = ",len(freq_out2))
    print("len(freq3) = ",len(freq_out3))

    print("freq amp 3  = ", freq_out3, abs_amp3)

    fig = plt.figure(1)
    fig.clf()
    #plt.semilogy(xf, yf)
    ax = fig.add_subplot(111)
    ax.set_xlim(freqmin,freqmax)
    ax.plot(freq_out0, abs_amp0,alpha=1.,label='rfft')
    ax.plot(freq_out1, abs_amp1,alpha=0.8,label='periodogram')
    ax.plot(freq_out2, abs_amp2,alpha=0.6,label='Welch')
    ax.plot(freq_out3, abs_amp3,alpha=0.4,label='Lomb-Scargle')
    ax.set_xlabel('frequency [Hz]')
    ax.set_ylabel('PSD')
    ax.legend(loc=0)
    #plt.grid()
    #plt.show()
    fig.savefig('test2.png' , dpi=300,bbox_inches='tight')
    
        
        
#test_my_fft2()
