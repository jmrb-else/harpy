
import os,sys
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')
from matplotlib.colors import Colormap,LinearSegmentedColormap
from astropy.io import ascii
import numpy as np
import string 

import h5py
import glob

from harm import read_3d_hdf5_slice 


#################################################################################
#################################################################################
"""

  make_monocolor_cmap():
  -------------

   -- given an RGB value and the colormap's name, create a color map 
       that goes from black to color to white. 
   
"""
def make_monocolor_cmap(cmap_name,rgb_value,
                        color_transition=None,
                        nlev=256,
                        white=(1.,1.,1.)): 

    black = (0.,0.,0.)
    midcolor=rgb_value
    if( color_transition is None ) :
        color_transition = 0.5
    locolor = np.multiply(midcolor,color_transition)
    colors=[black,locolor,midcolor,white]
    cmap = LinearSegmentedColormap.from_list(cmap_name, colors, N=nlev)

            
    return cmap


#################################################################################
#################################################################################
"""

  print_colormap():
  -------------

   -- print to stdout a ppm file for a given colormap in python 
   
"""
def print_colormap(cmap_name,nlev=256):

    from matplotlib import cm
    cmap = cm.get_cmap(cmap_name,nlev)
    ftmp = np.array(cmap(np.arange(nlev))*(nlev-1))
    itmp = ftmp.astype(int)
    rgbs = itmp[:,0:3]

    print("P3")
    print("16 16")
    print(nlev-1)
    for i in np.arange(nlev) :
        print("%d %d %d" %(rgbs[i,0],rgbs[i,1],rgbs[i,2]))
            
    return 


#################################################################################
#################################################################################
"""

  make_merged_cmap():
  -------------

   -- given an RGB value and the colormap's name, create a color map 
       that goes from black to color to midpoint on existing color map
   
"""
def make_merged_cmap(cmap_name,rgb_value,target_map,
                     target_start=0.4,
                     target_frac=0.5,
                     color_transition=None,
                     nlev=256):

    n1 = int( (1.-target_frac)*nlev )
    n2 = nlev - n1 
    colors2 = target_map(np.linspace(target_start, 1, n2))

    cmap1 = make_monocolor_cmap('tmpcmap',rgb_value,
                                color_transition=color_transition,
                                nlev=n1,
                                white=colors2[0])

    colors1 = cmap1(np.linspace(0, 1, n1))
    

    # combine them and build a new colormap
    colors_both = np.vstack((colors1, colors2))
    merged_cmap = LinearSegmentedColormap.from_list('cmap_name', colors_both)

    return merged_cmap


#################################################################################
#################################################################################
"""

  read_geo_data(): 
  -------------

   -- parses a file written by bothros via the "write_geo_info()" routine;
   -- returns with two tables:  header and body, each including column tables; 
   
"""
def read_geo_data(geofile,coord_type='spherical'): 
    fp = open(geofile, 'r')
    line1 = fp.readline()
    garb, line2 = string.split(fp.readline(),'#')
    header_lines = [line1,line2]
    fp.close()

    header_data = ascii.read(header_lines,format='commented_header')
    body_data = ascii.read(geofile,header_start=2,data_start=3,format='commented_header')

    if( coord_type is 'spherical'  ) :
        body_data['r']     =  body_data['xcon1']
        body_data['theta'] =  body_data['xcon2']
        body_data['phi']   =  body_data['xcon3']
        r  = body_data['r']     
        th = body_data['theta'] 
        ph = body_data['phi']
        body_data['x']     =  r*np.sin(th)*np.cos(ph)
        body_data['y']     =  r*np.sin(th)*np.sin(ph)
        body_data['z']     =  r*np.cos(th)
    elif( coord_type is 'cartesian' ) :
        body_data['x']     =  body_data['xcon1']
        body_data['y']     =  body_data['xcon2']
        body_data['z']     =  body_data['xcon3']
        x = body_data['x']
        y = body_data['y']
        z = body_data['z']
        r = np.sqrt(x*x + y*y + z*z) 
        th = np.arccos(z/r)
        ph = np.arctan2(y,x)
        body_data['r']     =  r
        body_data['theta'] =  th
        body_data['phi']   =  ph
    else :
        sys.exit('Unrecognized coord_type = ', coord_type)
        
        

    return header_data, body_data


###############################################################################
###############################################################################
# Assumes that the image frames have this shape:
#
#  I_image                  Dataset {12, 500, 500}
#
#  where  "12" is the number of the time steps and "500,500" is the image
#  resolution. 
###############################################################################

def get_bothros_data(fdest,funcname='I_image', frameslice=None):

    if( frameslice == None ) : 
        sltmp = slice(None,None,None)
        frameslice =  (sltmp,sltmp,sltmp)

    img = read_3d_hdf5_slice(funcname,frameslice,fdest)

    pix_x    = fdest['pix_x'   ].value[0,:]
    pix_y    = fdest['pix_y'   ].value[0,:]

    #Assuming uniform pixelation: 
    pix_area = fdest['pix_area'].value[0,0,0]

    tout = fdest['times'].value

    return pix_x, pix_y, pix_area, tout, img


#################################################################################
#################################################################################
"""

  find_image_overlap(): 
  -------------

   -- given two bothros data files each with a different field of view size, 
      this routine reads in pixel coordinates of each file and then returns with 
      an array representing the mask for the overlap region on the each frame. 

   -- the mask is set to 0. in those pixels that are overlapping, to 1 otherwise. 

   -- the masks will have the same dimensions as their respective files;

   -- assumes that each frame is Cartesian w.r.t. its coordinates, we can assumed
      that each frame can be described by two sets of coordinates representing the 
      lower-left and upper-right corners of their bounding box. 
   
"""
def find_image_overlap(h5file1=None,
                       h5file2=None,
                       pix_x1=None, 
                       pix_y1=None, 
                       pix_x2=None, 
                       pix_y2=None): 


    if( (h5file1 is None) and
        (pix_x1  is None) and 
        (pix_y1  is None) ) :
        sys.exit('We need either the file handle or the data')

    if( (h5file2 is None) and
        (pix_x2  is None) and 
        (pix_y2  is None) ) :
        sys.exit('We need either the file handle or the data')


    if( pix_x1 is None ) : 
        pix_x1 = h5file1['pix_x'].value[0,:]

    if( pix_y1 is None ) : 
        pix_y1 = h5file1['pix_y'].value[0,:]
    
    if( pix_x2 is None ) : 
        pix_x2 = h5file2['pix_x'].value[0,:]

    if( pix_y2 is None ) : 
        pix_y2 = h5file2['pix_y'].value[0,:]


    bbox1 = [np.min(pix_x1),np.max(pix_x1), np.min(pix_y1), np.max(pix_y1)]
    bbox2 = [np.min(pix_x2),np.max(pix_x2), np.min(pix_y2), np.max(pix_y2)]

    mask1 = np.ones(pix_x1.shape)
    mask2 = np.ones(pix_x2.shape)

    # print("bbox1 = ", bbox1)
    # print("bbox2 = ", bbox2)

    overlap1 = np.logical_and( np.logical_and( (pix_x1 >= bbox2[0]) , (pix_x1 <= bbox2[1]) ) , np.logical_and( (pix_y1 >= bbox2[2]) , (pix_y1 <= bbox2[3]) ) )
    mask1 *= np.logical_not(overlap1)

    overlap2 = np.logical_and( np.logical_and((pix_x2 >= bbox1[0]), (pix_x2 <= bbox1[1])) , np.logical_and( (pix_y2 >= bbox1[2]) , (pix_y2 <= bbox1[3]) ) )
    mask2 *= np.logical_not(overlap2)

    return mask1, mask2


#################################################################################
#################################################################################
"""

  make_image():
  -------------

   -- given image index and file name, reads in image data and plots it
   
"""
def make_image(filename='BOTHROS.000000.h5',
               funcname='I_image',
               dirout='./',
               dirplots='./',
               dolog=False,
               nlev=256,
               nticks=10,
               itime=0,
               tag=None,
               rmax=None,
               minf=None, maxf=None,
               title=None,
               imgout=None,
               pix_x=None,
               pix_y=None,
               tout=None,
               ifreq=None,
               dpi=300,
               other_data=None, 
               extend='both',
               clipmax=False,
               clipmin=False,
               showplots=False,
               savepng=True,
               nwin=0,
               use_contourf=False,
               length_scale=None,
               bbox=None,
               aspect_ratio=None,
               image_only=False,
               width=4,
               height=4,
               cmap_name='nipy_spectral',
               cmap_reversed=False,
               set_over_color=True,
               set_under_color=True,
               color_transition=None
               ):
    """  
    Make image of bothros slice
    """

    noplotting = True
    if( showplots or savepng ) :
        noplotting = False
        
    if( cmap_reversed is True ) :
        cmap_name += '_r'
        
    if( cmap_name is None ) :
        cmap = plt.cm.Spectral_r
    else :
        if( cmap_name ==  'myReds_r' ):
            cmap = make_monocolor_cmap(cmap_name,(1., 0, 0.),color_transition=color_transition,nlev=nlev)
        elif( cmap_name ==  'myGreens_r' ):
            cmap = make_monocolor_cmap(cmap_name,(0., 1., 0.),color_transition=color_transition,nlev=nlev)
        elif( cmap_name ==  'myBlues_r' ):
            cmap = make_monocolor_cmap(cmap_name,(0.,0.,1.),color_transition=color_transition,nlev=nlev)
        elif( cmap_name ==  'myIceyBlues_r' ):
            cmap = make_monocolor_cmap(cmap_name,(0.12,0.3,1.),color_transition=color_transition,nlev=nlev)
        elif( cmap_name ==  'myIceyBlueMagma_r' ):
            cmap_magma = plt.get_cmap('magma')
            rgbloc = np.multiply((0.12,0.3,1.),0.45)
            cmap = make_merged_cmap(cmap_name,rgbloc,cmap_magma,
                                    color_transition=color_transition,
                                    nlev=nlev)
            
            #rgb_magma = cmap_magma(1.)[0:3]
            #print("rgb magma = ", rgb_magma)
            #cmap = make_monocolor_cmap(cmap_name,(0.12,0.3,1.),color_transition=color_transition,nlev=nlev,white=rgb_magma)
        elif( cmap_name ==  'Violets_r' ):
            cmap = make_monocolor_cmap(cmap_name,(128./255., 0, 1.),color_transition=color_transition,nlev=nlev)
        else :
            cmap = plt.get_cmap(cmap_name)
        
#    cmap  = plt.cm.nipy_spectral
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    if( set_under_color ) : 
        Colormap.set_under(cmap,color=darkgrey) 
    if( set_over_color ) : 
        Colormap.set_over(cmap,color='w')


    if not showplots :
        plt.ioff()

    if( ifreq is not None ) : 
        frameslice = (slice(itime,itime+1,None),slice(ifreq,ifreq+1,None),slice(None,None,None),slice(None,None,None))
        vectorslice = (slice(itime,itime+1,None),slice(ifreq,ifreq+1,None))
    else :
        frameslice = (slice(itime,itime+1,None),slice(None,None,None),slice(None,None,None))
        vectorslice = (slice(itime,itime+1,None))

    if( tag is None ) : 
        tag = '-'

    if( ifreq is not None ) :
        tag = tag + str('-freq-%03d-' %ifreq) + str('it-%04d' %itime)
    else : 
        tag = tag + str('it-%04d' %itime)
    
    
    # dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'

    fdest = h5py.File(dirout+filename,'r')

    r1outer = fdest['Header/r1outer'].value[0]
    r_cam = fdest['Header/r_cam'].value[0]
    th_cam = fdest['Header/th_cam'].value[0]
    th_cam *= 180. / np.pi 

    if( length_scale is None ) : 
        length_scale = r_cam 
    
    if( rmax is None ) : 
        rmax = r1outer

    if( tout is None ) : 
        #tout = read_3d_hdf5_slice('times',vectorslice,fdest)
        tout = fdest['times'].value[itime]

    if( imgout is None ) : 
        imgout = read_3d_hdf5_slice(funcname,frameslice,fdest)

    if( pix_x is None ) : 
        pix_x = fdest['pix_x'].value[0,:]
        pix_x *= length_scale
        pix_x *= -1.

    if( pix_y is None ) : 
        pix_y = fdest['pix_y'].value[0,:]
        pix_y  *= length_scale
        pix_y *= -1.
    
    if (dolog) : 
        func = np.log10(np.abs(imgout))
        if maxf is None : 
            maxf = np.nanmax(func) 
        else :
            maxf = np.log10(maxf)

        
        if minf is None :
            minf = maxf - 6.
        elif minf is False :
            minf = np.log10(np.nanmin(func))
        else :
            minf = np.log10(minf)

        loc = np.logical_not(np.isfinite(func))
        func[loc] = minf - 1

    else : 
        func = np.copy(imgout)
        if maxf is None : 
            maxf = np.nanmax(func) 

        if minf is None :
            minf = 1.e-6* maxf 
        elif minf is False :
            minf = np.nanmin(func)
    
    if( clipmax ) : 
        loc = (func > maxf)
        func[loc] = maxf

    if( clipmin ) : 
        loc = (func < minf)
        func[loc] = minf

        
    if( len(func.shape) == 1 ) :
        nx = 2 
        npts = func.shape[0]
        if( (npts % nx) == 0 ) : 
            ny = npts / nx 
            func = np.reshape(func,(-1,2))
            pix_x = np.reshape(pix_x,(-1,2))
            pix_y = np.reshape(pix_y,(-1,2))
        else : 
            print("this is weird")
            stop

    fdest.close()

    print("min max func = ", np.min(func),np.max(func))

    if( noplotting ) :
        return pix_x, pix_y, func
    
    tstring = str('%0.1f' %tout)
    incstring = str('%03d' %th_cam)

    fignames = [dirplots + 'image'+'-'+incstring+tag]
    print( "figname = ", fignames)

    if( image_only is True ) : 
        figs = [plt.figure(nwin,frameon=False)]
        figs[-1].set_size_inches(width,height)
        ax = plt.Axes(figs[-1], [0., 0., 1., 1.])
        ax.set_axis_off()
        figs[-1].add_axes(ax)
        ax.imshow(func[:,::-1], aspect='auto',cmap=cmap,clim=(minf,maxf))
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=dpi)

        return func
    
    else:
        figs = [plt.figure(nwin)]
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        

    if( title is None ) : 
        ax.set_title(funcname+'   t='+tstring)
    else : 
        ax.set_title(title)

    ax.set_xlabel(r'x [M]')
    ax.set_ylabel(r'y [M]')

    levels = np.linspace(minf,maxf,nlev)
    rticks = np.linspace(minf,maxf,nticks)

    if( bbox is None ) :
        bbox = [-rmax,-rmax,rmax,rmax]

    ax.set_xlim([bbox[0],bbox[2]])
    ax.set_ylim([bbox[1],bbox[3]])

    if( aspect_ratio is not None ) : 
        ax.set_aspect(aspect_ratio)

    if 1 :
        if( use_contourf ) : 
            CP = ax.contourf(pix_x, pix_y,func,levels,cmap=cmap,extend=extend)
        else : 
            CP = ax.pcolormesh(pix_x, pix_y,func,vmin=np.min(levels),vmax=np.max(levels),cmap=cmap)

        if other_data is not None:
            print("using other_data")
            #            print(other_data[0])
            #            print(other_data[1])
            #            print(other_data[2])
            #            
            if( use_contourf ) : 
                CS_other = ax.contourf(other_data[0],other_data[1],other_data[2],levels, cmap=cmap, extend=extend)
            else : 
                CS_other = ax.pcolormesh(other_data[0],other_data[1],other_data[2],vmin=np.min(levels), vmax=np.max(levels), cmap=cmap)

        figs[-1].colorbar(CP,ticks=rticks,format='%4.1f',extend=extend)
            
    else : 
        CP = ax.imshow(func[:,::-1],extent=(-rmax,rmax,-rmax,rmax),cmap=cmap,clim=(-3.5,0.5),interpolation='nearest',origin='lower')
        figs[-1].colorbar(CP,format='%4.1f',extend=extend)

    # nwin+=1
    # fignames = np.append(fignames,(dirplots + 'image'+'-'+incstring+tag))
    # figs = np.append(figs,plt.figure(nwin))
    # 
    # figs[-1].clf()
    # ax = figs[-1].add_subplot(111)
    # CH = ax.hist(func.ravel(), bins=256, range=(-5, 2), fc='k', ec='k')


    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=dpi,bbox_inches='tight')
            
    if showplots : 
        plt.show()
            
    print("All done ")
    sys.stdout.flush()

    return pix_x, pix_y, func

