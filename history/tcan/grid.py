###############################################################################
###############################################################################
#  exploring grid setups
###############################################################################
###############################################################################

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re 

from IPython.core.debugger import Tracer

###############################################################################
###############################################################################
def setup_grid(N2=160,h_slope=0.13,diag3_exponent=9,th_cutout=1.e-15,hor=0.3,
               r_hor=1.5,cour=0.45,
               t_sim=7.2e4,ncells_per_core=(20.*20.*32.),
               zc_rate=(1.15*8.e4)):

    xp2_beg = 0.
    xp2_end = 1.
    gridlength2 = xp2_end - xp2_beg
    dxp2 = gridlength2/N2
    diag3_factor   = 1. - 2.*th_cutout/np.pi - h_slope
    xp2_corners = np.linspace(xp2_beg, xp2_end,N2)
    xp2_centers = xp2_corners[0:-1] + 0.5*dxp2
    ftmp = 2.*xp2_corners - 1. 
    x2_corners = 0.5*np.pi*( 1. + h_slope*ftmp  + diag3_factor*(ftmp**diag3_exponent) )
    ftmp = 2.*xp2_centers - 1. 
    x2_centers = 0.5*np.pi*( 1. + h_slope*ftmp  + diag3_factor*(ftmp**diag3_exponent) )
    x2_2 = np.roll(x2_corners,1)
    dx2 = x2_corners - x2_2
    dx2[0] = dx2[-1]
    dx2_centers = dx2[1:]
    print("dx2 contrast = ", np.max(dx2)/np.min(dx2))
    print("dx2 min max = ", np.min(dx2),np.max(dx2))

    #within_hor = np.abs(x2_centers - 0.5*np.pi) < 0.5*hor
    within_hor = np.abs(x2_centers - 0.5*np.pi) < hor
    n_within_hor = np.sum(within_hor)

    #resid = np.abs(x2_centers - 0.5*np.pi)  - 0.5*hor
    resid = np.abs(x2_centers - 0.5*np.pi)  - hor
    ibeg = -1
    iend = -1 
    for i in np.arange(len(resid) - 1)  :
        if( resid[i]*resid[i+1] < 0. ) :
            if( ibeg < 0 ) :
                ibeg = i
            else :
                iend = i

    #print(" i_hr_lo, i_hr_hi diff = ", ibeg, iend, (iend - ibeg) )
    beta = 100.
    Qphi = 25.
    dth = np.max(dx2)
    Nphi = np.int(1000.*(0.1/hor)*np.sqrt(beta/100.)*(Qphi/10.))
    dphi_eq = 2.*np.pi / Nphi
    dphi    = dphi_eq * np.sin(th_cutout+0.5*dth)

    dt_phi1 = cour*r_hor*dphi
    dt_phi2 = cour*r_hor*dphi_eq
    t_wall1 = t_sim * ncells_per_core/(dt_phi1 * zc_rate  )  / (3600.)
    print("Nphi = ", Nphi)
    print("dt_phi = ", dt_phi1, dt_phi2)
    print("t_wall (hours) = ", t_wall1)
    print("t_wall (days) = ", t_wall1/24.)
    

    # at start of sim:
    beta = 1000.
    vasq_o_vazsq=2.
    Qz=15.
    Nz = np.int(16.*np.sqrt(beta/100.)*np.sqrt((vasq_o_vazsq))*(Qz/10.))

    # dth_max threshold, which when above makes sure that dth_min is
    # the smallest angular extent: (2*dth_min / dphi), where dth_min
    # and dphi are set by the Hawley criteria):
    dth_max = (1.e2/(16.*np.pi)) * np.sqrt(1./vasq_o_vazsq) * (Qphi / Qz)
    
    print("Ncells within hor = ", n_within_hor)
    print("Nz start = ", Nz)
    print("dth_max start = ", dth_max)
 
    beta = 10.
    vasq_o_vazsq=100.
    Qz=15.
    Nz = np.int(16.*np.sqrt(beta/100.)*np.sqrt((vasq_o_vazsq))*(Qz/10.))
    dth_max = (1.e2/(16.*np.pi)) * np.sqrt(1./vasq_o_vazsq) * (Qphi / Qz)
    print("Nz turb = ", Nz)
    print("dth_max turb = ", dth_max)
 
    #Nphi = np.int(3.*np.sqrt(0.1)*1000.*(0.1/hor))
    dphi = 2.*np.pi / Nphi
    shape_factor = 4.
    dth_match = dphi / shape_factor

    ##
    dr_o_r = 2.*np.pi / Nphi
    Nr = np.int(np.log(t_sim/r_hor) / dr_o_r)
    print("Nr = ", Nr) 

    N1 = 1472
    N3 = 840
    Ncells = N1*N2*N3
    n_cores = Ncells / ncells_per_core
    n_cores_per_node = 56.
    n_nodes = n_cores / n_cores_per_node 
    print("N1 N2 N3 = ", N1, N2, N3)
    print("Ncells = ", Ncells )
    print("Ncores = ", n_cores)
    print("Nnodes = ", n_nodes)
    print("Frontera SUs = ", t_wall1*n_nodes)
    


    nwin = 0
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,x2_centers,'.')
    ax.plot([xp2_centers[ibeg],xp2_centers[ibeg]],[0,np.pi])
    ax.plot([xp2_centers[iend],xp2_centers[iend]],[0,np.pi])
    ax.plot([0,1],[x2_centers[ibeg],x2_centers[ibeg]])
    ax.plot([0,1],[x2_centers[iend],x2_centers[iend]])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\theta$')
    fig.savefig("x2.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,np.log10(dx2_centers))
    ax.plot([xp2_centers[ibeg],xp2_centers[ibeg]],[-30.,1.])
    ax.plot([xp2_centers[iend],xp2_centers[iend]],[-30.,1.])
    ax.plot([0,1],np.log10([dth_match,dth_match]))
    ax.set_ylim([-6,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\theta$')
    fig.savefig("dx2.png", dpi=300)

    return



###########################

setup_grid(h_slope=0.1,diag3_exponent=21,N2=180,hor=0.3)



