"""  

  sod.py: 
  ------------
    -- make 1-d plots for the sod test;

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, NullFormatter


import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################

def sod_exact_plots(ifile=0,
              dirout='./'
              ):
    """  
    """

    showplots=0
    savepng=1
    nwin=0
    
    cmap  = plt.cm.Spectral_r
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 
    
    if not showplots :
        plt.ioff()
    
    files = sorted(glob.glob("sod-*.dat"))
    nfiles = len(files)

    fignames = []
    figs     = []

    linetypes = ['k-','b-','r-','g-','k.','b.','r.','g.']
    funcs = ['density','pressure','velocity']
    nfuncs = len(funcs)

    fignames = np.append(fignames,(dirout + 'all-funcs'))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    a_form = "%g"
    ymajorFormatter = FormatStrFormatter('%1.1e')
    xmajorFormatter = FormatStrFormatter('%1.0f')

    for j in np.arange(nfiles) :
        data = ascii.read(files[j],data_start=2,format='commented_header')
        ax = figs[-1].add_subplot(3,3,1+j)
        ax.plot(data['x'], data['density'], 'k-',label='exact')
        ax.yaxis.set_major_formatter(ymajorFormatter)
        ax.xaxis.set_major_formatter(xmajorFormatter)
        if j == 0 : 
            ax.set_ylabel(r'$\rho$')
            ax.legend(loc=0)

        ax = figs[-1].add_subplot(3,3,4+j)
        ax.plot(data['x'], data['pressure'], 'k-',label='exact')
        ax.yaxis.set_major_formatter(ymajorFormatter)
        ax.xaxis.set_major_formatter(xmajorFormatter)
        if j == 0 : 
            ax.set_ylabel(r'$p$')
        if j == 1 :
            ax.set_xlabel(r'$x$')


        ax = figs[-1].add_subplot(3,3,7+j)
        ax.plot(data['x'], data['velocity'], 'k-',label='exact')
        ax.yaxis.set_major_formatter(ymajorFormatter)
        ax.xaxis.set_major_formatter(xmajorFormatter)
        if j == 0 : 
            ax.set_ylabel(r'$v_x$')
            

    nwin+=1

    print("fignames = ", fignames)
    print("fignames[0] = ", fignames[0])
    
    if savepng :
        i = 0 
        figs[i].savefig(str('%s.png' %fignames[i]), dpi=300)
            
    print("All done ")
    sys.stdout.flush()

    return 

###############################################################################
###############################################################################
###############################################################################

def sod_slice_plots(ifile=0,
              dirout='./'
              ):
    """  
    """

    showplots=0
    savepng=1
    nwin=0
    
    cmap  = plt.cm.Spectral_r
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 
    
    if not showplots :
        plt.ioff()
    
    ex_file = "sod-2500.dat"
    data = ascii.read(ex_file,data_start=2,format='commented_header')

    # h5 files:
    pfiles = sorted(glob.glob("dumps/SOD*.h5"))

    h5file0  = h5py.File(pfiles[0], 'r')
    gam = h5file0['Header/Grid/gam'].value[0]
    jj = 564
    kk = 9
    sl_obj = (slice(None,None,None),slice(jj,jj+1,1),slice(kk,kk+1,1))
    xcart0 = read_3d_hdf5_slice('x1'   ,sl_obj,h5file0)
    ycart0 = read_3d_hdf5_slice('x2'   ,sl_obj,h5file0)
    zcart0 = read_3d_hdf5_slice('x3'   ,sl_obj,h5file0)
    rho0   = read_3d_hdf5_slice('rho'  ,sl_obj,h5file0)
    pgas0  = read_3d_hdf5_slice('uu'   ,sl_obj,h5file0)
    vx0    = read_3d_hdf5_slice('ucon1',sl_obj,h5file0)
    pgas0 *= (gam-1.)
    print("ycart0 = ", ycart0[0])
    print("zcart0 = ", zcart0[0])
    h5file0.close()

    
    h5file1  = h5py.File(pfiles[1], 'r')
    jj = 4
    kk = 0
    sl_obj = (slice(None,None,None),slice(jj,jj+1,1),slice(kk,kk+1,1))
    rr1    = read_3d_hdf5_slice('x1'   ,sl_obj,h5file1)
    zcart1 = read_3d_hdf5_slice('x2'   ,sl_obj,h5file1)
    ph1    = read_3d_hdf5_slice('x3'   ,sl_obj,h5file1)
    rho1   = read_3d_hdf5_slice('rho'  ,sl_obj,h5file1)
    pgas1  = read_3d_hdf5_slice('uu'   ,sl_obj,h5file1)
    vx1    = read_3d_hdf5_slice('ucon1',sl_obj,h5file1)
    pgas1 *= (gam-1.)
    xp1 = 1.768174e+00
    yp1 = 8.231826e+00
    xcart1 = rr1 * np.cos(ph1) + xp1
    ycart1 = rr1 * np.sin(ph1) + yp1 
    print("ycart1 = ", ycart1[0])
    print("zcart1 = ", zcart1[0])

    kk = 499
    sl_obj = (slice(None,None,None),slice(jj,jj+1,1),slice(kk,kk+1,1))
    rr2    = read_3d_hdf5_slice('x1'   ,sl_obj,h5file1)
    zcart2 = read_3d_hdf5_slice('x2'   ,sl_obj,h5file1)
    ph2    = read_3d_hdf5_slice('x3'   ,sl_obj,h5file1)
    rho2   = read_3d_hdf5_slice('rho'  ,sl_obj,h5file1)
    pgas2  = read_3d_hdf5_slice('uu'   ,sl_obj,h5file1)
    vx2    = read_3d_hdf5_slice('ucon1',sl_obj,h5file1)
    pgas2 *= (gam-1.)
    xp2 = 1.768174e+00
    yp2 = 8.231826e+00
    xcart2 = rr2 * np.cos(ph2) + xp1
    ycart2 = rr2 * np.sin(ph2) + yp1 
    print("ycart2 = ", ycart2[0])
    print("zcart2 = ", zcart2[0])
    h5file1.close()

    fignames = []
    figs     = []

    linetypes = ['k-','b-','r-','g-','k.','b.','r.','g.']
    funcs = ['density','pressure','velocity']
    nfuncs = len(funcs)

    fignames = np.append(fignames,(dirout + 'all-slice'))
    figs = np.append(figs,plt.figure(nwin,figsize=(5,10)))
    figs[-1].clf()
    a_form = "%g"
    ymajorFormatter = FormatStrFormatter('%1.1e')
    xmajorFormatter = FormatStrFormatter('%1.0f')

    msizelocal=4
    msizeglobal=2
    ax = figs[-1].add_subplot(3,1,1)
    ax.plot(xcart1,rho1,'co',label='local',markersize=msizelocal)
    ax.plot(xcart2,rho2,'co',markersize=msizelocal)
    ax.plot(xcart0,rho0,'go',label='global',markersize=msizeglobal)
    ax.plot(data['x'], data['density'], 'k-',label='exact')
    ax.yaxis.set_major_formatter(ymajorFormatter)
#    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.xaxis.set_major_formatter(NullFormatter())
    ax.set_ylabel(r'$\rho$')
    ax.legend(loc=0,frameon=False)

    ax = figs[-1].add_subplot(3,1,2)
    ax.plot(xcart1,pgas1,'co',label='local',markersize=msizelocal)
    ax.plot(xcart2,pgas2,'co',markersize=msizelocal)
    ax.plot(xcart0,pgas0,'go',label='global',markersize=msizeglobal)
    ax.plot(data['x'], data['pressure'], 'k-',label='exact')
    ax.yaxis.set_major_formatter(ymajorFormatter)
#    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.xaxis.set_major_formatter(NullFormatter())
    ax.set_ylabel(r'$p$')


    ax = figs[-1].add_subplot(3,1,3)
    ax.plot(xcart1,vx1,'co',label='local',markersize=msizelocal)
    ax.plot(xcart2,vx2,'co',markersize=msizelocal)
    ax.plot(xcart0,vx0,'go',label='global',markersize=msizeglobal)
    ax.plot(data['x'], data['velocity'], 'k-',label='exact')
    ax.yaxis.set_major_formatter(ymajorFormatter)
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.set_ylabel(r'$v_x$')
    ax.set_xlabel(r'$x$')

    plt.subplots_adjust(left=0.2,right=0.95,top=0.95,bottom=0.05)

    nwin+=1

    print("fignames = ", fignames)
    print("fignames[0] = ", fignames[0])
    
    if savepng :
        i = 0 
        figs[i].savefig(str('%s.pdf' %fignames[i]))
#        figs[i].savefig(str('%s.png' %fignames[i]), dpi=500)
            
    print("All done ")
    sys.stdout.flush()

    return 

###############################################################################
###############################################################################

# sod_exact_plots()

sod_slice_plots()


    



#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
