###############################################################################
###############################################################################
#  exploring grid setups
###############################################################################
###############################################################################

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re 
from coords import *

from IPython.core.debugger import Tracer



###############################################################################
# One can show (Masa++2018) that jets have for z = R^1.6 or z=R^2
# where R = r * sin(th) and z = r*cos(th).  If z = R^m, then 
#  1 < m < 2.67    for general outflows, and one can show using the small 
# angle approximation that 
# 
#  th ~=  r^(1/m - 1)  = r^n2    where n2=(1/m - 1)
#  
#  for m = 1.5, 2 respectively,  n2 = -1/3, -1/2 respectively
#
###############################################################################

###############################################################################
###############################################################################
###############################################################################
def setup_grid2(N2=200,th_cutout=1.e-15,th_length=np.pi, 
                r_hor=1.5,
                cour=0.4,
                delta_z1=0.3,
                h_z=20.,
                h_o_r=0.2,
                z1=0.5,
                R0=0.,
                rmin=1.,
                rmax=7.2e4,
                r0=10000.,
                N1=1000,
                c1=1.,
                n_exp1=10,
                nth_stride=10,
                nr_stride=10,
                N3=800,
                r_jet=300.,
                n_exp2=(1./3.),
                h_jet=5.e-3,
                xp1_0=None,
                t_sim=7.2e4,ncells_per_core=(20.*20.*32.),plot_ghost_cells=True,
                zc_rate=(1.15*8.e4),tag=''):

    dph = 2.*np.pi/N3

    # r 
    xp1_corners = make_xp_array(N1,pos='CORNER',include_ghost_cells=plot_ghost_cells)
    xp1_centers = make_xp_array(N1,pos='CENTER',include_ghost_cells=plot_ghost_cells)
    dxp1 = xp1_corners[1] - xp1_corners[0]

    x1_corners, dr_corners, dr2_corners  = r_of_xp1_hyperexp_new(xp1_corners, r0, rmin, rmax, R0, n_exp1, xp1_0=xp1_0)
    x1_centers, dr_centers, dr2_centers  = r_of_xp1_hyperexp_new(xp1_centers, r0, rmin, rmax, R0, n_exp1, xp1_0=xp1_0)
    print("xp1_0 = ", xp1_0)
    x1_2 = np.roll(x1_corners,1)
    dx1 = x1_corners - x1_2
    dx1_centers = dx1[1:]
    x1_m1 = x1_2
    x1_p1 = np.roll(x1_corners,-1)
    dx1_2 = x1_p1 - 2.*x1_corners + x1_m1 
    dx1_2_corners = dx1_2[1:]

    dr_corners *= dxp1
    dr_centers *= dxp1
    dr2_corners *= dxp1*dxp1
    dr2_centers *= dxp1*dxp1

    # th
    dth_min1 = h_o_r / 32.
    if( delta_z1 is None ) : 
        delta_z1 = find_delta_from_dthmin_dph( N2, dth_min1, dph )
        print("delta_z1 = ", delta_z1)
        
    a_z = find_az_from_dth(N2, dth_min1, delta_z1)
    #dth_max1 = ( (np.pi/N2) - 2.*delta_z1*dth_min1 )/( 1. - 2.*delta_z1 )
    dth_max1 = 0.9*((np.pi/N2))
    print("a_z = ", a_z)
    
    dth_min2_centers = dth_max1*(x1_centers/r_jet)**(-n_exp2)
    dth_min2_corners = dth_max1*(x1_corners/r_jet)**(-n_exp2)
    delta_z1_r_centers = np.ones(len(xp1_centers))*delta_z1
    delta_z1_r_corners = np.ones(len(xp1_corners))*delta_z1

    #print("delta_z_2 centers = ", delta_z_2_r_centers)
    #print("delta_z_2 corners = ", delta_z_2_r_corners)
    trans_centers = transfunc(x1_centers, 0.9*r_jet, h_jet, 0., 1.)
    trans_corners = transfunc(x1_corners, 0.9*r_jet, h_jet, 0., 1.)
    
    a_z1_r_centers = find_az_from_dth_2(N2, dth_min2_centers, delta_z1_r_centers)
    a_z1_r_corners = find_az_from_dth_2(N2, dth_min2_corners, delta_z1_r_corners)

    a_z1_r_centers *= trans_centers
    a_z1_r_corners *= trans_corners

    a_z1_r_centers += (1.-trans_centers)*a_z
    a_z1_r_corners += (1.-trans_corners)*a_z


    xp2_centers = make_xp_array(N2,pos='CENTER',include_ghost_cells=plot_ghost_cells)
    xp2_corners = make_xp_array(N2,pos='CORNER',include_ghost_cells=plot_ghost_cells)
    xp2_centers_2d = np.outer(np.ones(len(xp1_centers)),xp2_centers )
    xp2_corners_2d = np.outer(np.ones(len(xp1_corners)),xp2_corners )

    delta_z1_centers = np.outer(delta_z1_r_centers,np.ones(len(xp2_centers)))
    delta_z1_corners = np.outer(delta_z1_r_corners,np.ones(len(xp2_corners)))
    a_z1_centers = np.outer(a_z1_r_centers,np.ones(len(xp2_centers)))
    a_z1_corners = np.outer(a_z1_r_corners,np.ones(len(xp2_corners)))
    
    x2_centers = th_of_x2(xp2_centers_2d, z1, a_z1_centers, h_z, delta_z1_centers)
    x2_corners = th_of_x2(xp2_corners_2d, z1, a_z1_corners, h_z, delta_z1_corners)

    #within_hor = np.abs(x2_centers - 0.5*np.pi) < h_o_r
    #n_within_hor = np.sum(within_hor)
    #print("n within hor = ", n_within_hor)

    x2_2 = np.roll(x2_corners,1,axis=1)
    dx2 = x2_corners - x2_2
    dx2_centers = dx2[1:,1:]
    print("dx2 contrast = ", np.max(dx2_centers)/np.min(dx2_centers))
    print("dx2 min max = ", np.min(dx2_centers),np.max(dx2_centers))
    print("dx3 eq  min = ", dph, np.sin(np.min(x2_centers))*dph)
    print("dx2_min / dx3_min = ", np.min(dx2_centers)/(np.sin(np.min(x2_centers))*dph))

    print("len(dx2_centers) = ", len(dx2_centers))
    print("len(xp2_centers) = ", len(xp2_centers))
    print("len(x2_centers) = ", len(x2_centers))
    print("len(x2_corners) = ", len(x2_corners))

    dth_min  = np.min(dx2_centers)
    r_hor = 2.
    ddth_max_vs_r_1 =  2.*dth_min*r_hor *x1_centers/ (x1_centers*dph)
    ddth_max_vs_r_2 = dth_max1 * x1_centers**(1.-1./2.)
    ddth_max_vs_r_3 = dth_max1 * x1_centers**(1.-1./3.)
    ddth_max_vs_r_4 = dth_max1 * x1_centers*(x1_centers/r_jet)**(-1./3.)
    ddth_max_vs_r_5 = dth_max1*x1_centers
    ddth_min  = dth_min * r_hor
    ddth_max  = dth_max1 * r_hor

    x1_out = np.outer(x1_centers,np.ones(N2))
    dr_out = np.outer(dr_centers,np.ones(N2))
    rdth_centers = x1_out * dx2_centers
    rdph_centers = x1_out * dph 

    
    ##################
    # x y
    nth = len(xp2_corners)
    nr = len(xp1_corners)

    rout = np.outer(x1_corners,np.ones(nth))
    thout= x2_corners
    
    zz = rout * np.cos(thout)
    xx = rout * np.sin(thout)

    zz_hor = x1_corners * np.cos(h_o_r+0.5*np.pi)
    xx_hor = x1_corners * np.sin(h_o_r+0.5*np.pi)

    i1 = 30
    i2 = np.int(0.9*N1)
    i3 = N1-2
    print("dth extract radii = ", x1_centers[i1],x1_centers[i2] )

    dth_base = (np.pi/N2)
    
    ########################
    # x2 plots :
    nwin = 0
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_corners,x2_corners[i1,:],'b.')
    ax.plot(xp2_corners,x2_corners[i2,:],'g.')
    ax.plot(xp2_corners,x2_corners[i3,:],'r.')
    ax.plot([0.,0.],[0,np.pi])
    ax.plot([1.,1.],[0,np.pi])
    ax.plot([0,1],[0.,0.])
    ax.plot([0,1],[np.pi,np.pi])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\theta$')
    fig.savefig(tag+"x2.png", dpi=300)
    # 
    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,np.log10(dx2_centers[i1,:]),'b.')
    ax.plot(xp2_centers,np.log10(dx2_centers[i2,:]),'g.')
    ax.plot(xp2_centers,np.log10(dx2_centers[i3,:]),'r.')
    ax.plot([0.,0.],[-30.,1.])
    ax.plot([1.,1.],[-30.,1.])
    ax.plot([0.,1.],np.log10([dth_min1,dth_min1]),'y--' )
    ax.plot([0.,1.],np.log10([dth_max1,dth_max1]),'o--')
    ax.plot([0.,1.],np.log10([dth_base,dth_base]),'w--')
    ax.set_ylim([-5,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\log \Delta \theta$')
    fig.savefig(tag+"dx2-log.png", dpi=300)
    # 
    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,(dx2_centers[i1,:]),'b.')
    ax.plot(xp2_centers,(dx2_centers[i2,:]),'g.')
    ax.plot(xp2_centers,(dx2_centers[i3,:]),'r.')
    ax.plot([0.,0.],[0.,1.])
    ax.plot([1.,1.],[0.,1.])
    ax.plot([0.,1.],[dth_min1,dth_min1],'y--' )
    ax.plot([0.,1.],[dth_max1,dth_max1],'o-.')
    ax.plot([0.,1.],[dth_base,dth_base],'w--')
    #ax.set_ylim([-6,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\Delta \theta$')
    fig.savefig(tag+"dx2-lin.png", dpi=300)

    
    nwin += 1 
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp1_centers,np.log10(x1_centers),'.')
    ax.plot([0.,1.],np.log10([rmin,rmin]))
    ax.plot([0.,1.],np.log10([rmax,rmax]))
    ax.plot([0.,1.],np.log10([r0,r0]))
    ax.set_ylim([-2,6])
    ax.set_xlabel(r'$xp^1$')
    ax.set_ylabel(r'$\log r$')
    fig.savefig(tag+"x1-log.png", dpi=300)
    
    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp1_centers,np.log10(dx1_centers),'-',label='dx1',alpha=1.)
    ax.plot(xp1_corners,np.log10(dr_corners),'--',label='dr_corn',alpha=0.7)
    ax.plot(xp1_centers,np.log10(dr_centers),':',label='dr_cen',alpha=0.4)
    ax.set_ylim([-4,6])
    ax.set_xlabel(r'$xp^1$')
    ax.set_ylabel(r'$\log \Delta r$')
    ax.legend(loc=0)
    fig.savefig(tag+"dx1-log.png", dpi=300)
    
    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp1_centers,np.log10(dx1_2_corners),'-',label='dx1',alpha=1.)
    ax.plot(xp1_corners,np.log10(dr2_corners),'--',label='dr_corn',alpha=0.7)
    ax.plot(xp1_centers,np.log10(dr2_centers),':',label='dr_cen',alpha=0.4)
    ax.set_ylim([-8,6])
    ax.set_xlabel(r'$xp^1$')
    ax.set_ylabel(r'$\log \Delta r$')
    ax.legend(loc=0)
    fig.savefig(tag+"dx1_2-log.png", dpi=300)
    # 
    # nwin += 1
    # fig = plt.figure(nwin,figsize=(8,6))
    # fig.clf()
    # ax = fig.add_subplot(111)
    # ax.plot(xp1_centers,(dx1_centers))
    # ax.plot([0.,0.],[0.,1.])
    # ax.plot([1.,1.],[0.,1.])
    # ax.plot([0.,1.],[dth_min1,dth_min1])
    # ax.plot([0.,1.],[dth_max1,dth_max1])
    # #ax.set_ylim([-6,-0])
    # ax.set_xlabel(r'$xp^1$')
    # ax.set_ylabel(r'$\Delta r$')
    # fig.savefig(tag+"dx1-lin.png", dpi=300)
    # 
    # nwin += 1
    # fig = plt.figure(nwin,figsize=(8,6))
    # fig.clf()
    # ax = fig.add_subplot(111)
    # ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_1)))
    # ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_2)))
    # ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_3)))
    # ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_4)))
    # ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_5)))
    # ax.plot(np.log10([1.,1000.]),np.log10([ddth_min,ddth_min]))
    # ax.plot(np.log10([1.,1000.]),np.log10([ddth_max,ddth_max]))
    # ax.plot(np.log10([r_jet,r_jet]),[-4,4])
    # #ax.set_xlim([0.5,10])
    # ax.set_xlabel(r'log $r$')
    # ax.set_ylabel(r'log $r \Delta \theta$')
    # fig.savefig(tag+"dx2-vs-r.png", dpi=300)

    nwin += 1
    rout = 20.
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111,aspect='equal')
    ax.set_ylim([-rout,rout])
    ax.set_xlim([-rout,rout])
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$z$')
    ax.plot(xx_hor, zz_hor,'r--',alpha=0.6)
    ax.plot(xx_hor,-zz_hor,'r--',alpha=0.6)
    for ith in np.arange(0,nth,nth_stride) :
        ax.plot(xx[:,ith],zz[:,ith],'w-')
        ax.plot(-xx[:,ith],zz[:,ith],'w-')
    for ir in np.arange(0,nr,nr_stride) :
        ax.plot(xx[ir,:],zz[ir,:],'w-')
        ax.plot(-xx[ir,:],zz[ir,:],'w-')
        
    fig.savefig(tag+"grid.png", dpi=300)

    nwin += 1
    rout = r_jet*1.2
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111,aspect='equal')
    ax.set_ylim([-rout,rout])
    ax.set_xlim([-rout,rout])
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$z$')
    ax.plot(xx_hor, zz_hor,'r--',alpha=0.6)
    ax.plot(xx_hor,-zz_hor,'r--',alpha=0.6)
    for ith in np.arange(0,nth,nth_stride) :
        ax.plot(xx[:,ith],zz[:,ith],'w-')
        ax.plot(-xx[:,ith],zz[:,ith],'w-')
    for ir in np.arange(0,nr,nr_stride) :
        ax.plot(xx[ir,:],zz[ir,:],'w-')
        ax.plot(-xx[ir,:],zz[ir,:],'w-')
        
    fig.savefig(tag+"grid2.png", dpi=300)

    nwin += 1
    rout = 2000.
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111,aspect='equal')
    ax.set_ylim([-rout,rout])
    ax.set_xlim([-rout,rout])
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$z$')
    ax.plot(xx_hor, zz_hor,'r--',alpha=0.6)
    ax.plot(xx_hor,-zz_hor,'r--',alpha=0.6)
    for ith in np.arange(0,nth,nth_stride) :
        ax.plot(xx[:,ith],zz[:,ith],'w-')
        ax.plot(-xx[:,ith],zz[:,ith],'w-')
    for ir in np.arange(0,nr,nr_stride) :
        ax.plot(xx[ir,:],zz[ir,:],'w-')
        ax.plot(-xx[ir,:],zz[ir,:],'w-')
        
    fig.savefig(tag+"grid3.png", dpi=300)

    igm_grid_extents = [3840,1920,960,168,96,48,18,12,6,3]
    igm_grid_resolutions = [16, 8,4,2, 1,  0.5,  0.25,  0.125, 0.0625, 0.03125]

    nwin += 1
    rout = 2000.
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.set_ylim([-3,4])
    ax.set_xlabel(r'$\log_{10} \ r$')
    ax.set_ylabel(r'$\log_{10} \ dx$')
    ax.plot(np.log10(x1_out.flatten()), np.log10(      dr_out.flatten()),'bo',markersize=1,label='dr ')
    ax.plot(np.log10(x1_out.flatten()), np.log10(rdth_centers.flatten()),'ro',markersize=1,label='r dth ')
    ax.plot(np.log10(x1_out.flatten()), np.log10(rdph_centers.flatten()),'go',markersize=1,label='r dph ')
    for i in np.arange(len(igm_grid_extents)) :
        if( i == 0 ) :
            labelout='IGM dx'
        else:
            labelout=None
        ax.plot(np.log10([1e-1,igm_grid_extents[i]]), np.log10([igm_grid_resolutions[i],igm_grid_resolutions[i]]), '-w',alpha=0.4,label=labelout)

    ax.legend(loc=0)
    fig.savefig(tag+"resolutions.png", dpi=300)
    
    return




###########################

#setup_grid(h_slope=0.1,diag3_exponent=21,N2=180,hor=0.3,tag='diag3-')

setup_grid2(N2=200,tag='mixed7-', plot_ghost_cells=False,h_jet=1e-2)

# ###############################################################################
# # This allows the python module to be executed like:   
# # 
# #   prompt>  python lum
# # 
# ###############################################################################
# if __name__ == "__main__":
#     import sys
#     print(sys.argv)
#     if len(sys.argv) > 1 :
#         lum(sys.argv[1])
#     else:
#         lum()
# 

