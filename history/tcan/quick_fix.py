"""  

  quick_fix()
  ------------
    -- adds a missing data set to all groups in a hdf5 history file. 

"""

from __future__ import print_function

import numpy as np
import h5py

import os,sys


###############################################################################
###############################################################################
###############################################################################

def quick_fix(h5file):
    """  
    Adds missing history dataset in an existing history file 
    """

    # Get a list of all the parameters in the hdf5 header file:  
    fh5 = h5py.File(h5file,'r+')
    gd  = fh5['/Bound/H_Lut']
    new_arr = np.zeros(gd.shape, dtype=gd.dtype)
    dnew = fh5.create_dataset('/Bound/H_Lup', gd.shape, dtype=gd.dtype, data=new_arr)
    dnew = fh5.create_dataset('/Unbound/H_Lup', gd.shape, dtype=gd.dtype, data=new_arr)
    dnew = fh5.create_dataset('/Jet/H_Lup', gd.shape, dtype=gd.dtype, data=new_arr)
    fh5.close()


###############################################################################
# This allows the python module to be executed like:
#
#   prompt>  python quick_fix  <h5file> 
#
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) > 1 :
        quick_fix(sys.argv[1])
    else:
        sys.exit("Need to specify the hdf5 history file, Exiting ....")

