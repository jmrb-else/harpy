###############################################################################
###############################################################################
#  exploring grid setups
###############################################################################
###############################################################################

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py
import glob
import re 
from coords import *

from IPython.core.debugger import Tracer



###############################################################################
# One can show (Masa++2018) that jets have for z = R^1.6 or z=R^2
# where R = r * sin(th) and z = r*cos(th).  If z = R^m, then 
#  1 < m < 2.67    for general outflows, and one can show using the small 
# angle approximation that 
# 
#  th ~=  r^(1/m - 1)  = r^n2    where n2=(1/m - 1)
#  
#  for m = 1.5, 2 respectively,  n2 = -1/3, -1/2 respectively
#
###############################################################################

###############################################################################
###############################################################################
###############################################################################
def setup_grid2(N2=180,th_cutout=1.e-15,hor=0.3,th_length=np.pi, 
                r_hor=1.5,
                cour=0.4,
                delta_z1=0.3,
                h_z=50.,
                dth_min1=(0.2/32.),
                h_o_r=0.2,
                z1=0.5,
                R0=0.,
                rmin=1.,
                rmax=7.2e4,
                r0=400.,
                N1=1000,
                c1=1.,
                n_exp1=10,
                nth_stride=10,
                nr_stride=10,
                N3=800,
                r_jet=10.,
                t_sim=7.2e4,ncells_per_core=(20.*20.*32.),plot_ghost_cells=True,
                zc_rate=(1.15*8.e4),tag=''):

    dph = 2.*np.pi/N3
    
    if( delta_z1 is None ) : 
        delta_z1 = find_delta_from_dthmin_dph( N2, dth_min1, dph )
        print("delta_z1 = ", delta_z1)
        
    a_z = find_az_from_dth(N2, dth_min1, delta_z1)
    dth_max1 = ( (np.pi/N2) - 2.*delta_z1*dth_min1 )/( 1. - 2.*delta_z1 )
    print("a_z = ", a_z)

    xp2_corners = make_xp_array(N2,pos='CORNER',include_ghost_cells=plot_ghost_cells)
    xp2_centers = make_xp_array(N2,pos='CENTER',include_ghost_cells=plot_ghost_cells)

    x2_corners = th_of_x2(xp2_corners, z1, a_z, h_z, delta_z1)
    x2_centers = th_of_x2(xp2_centers, z1, a_z, h_z, delta_z1)

    within_hor = np.abs(x2_centers - 0.5*np.pi) < h_o_r
    n_within_hor = np.sum(within_hor)
    print("n within hor = ", n_within_hor)

    x2_2 = np.roll(x2_corners,1)
    dx2 = x2_corners - x2_2
    dx2_centers = dx2[1:]
    print("dx2 contrast = ", np.max(dx2_centers)/np.min(dx2_centers))
    print("dx2 min max = ", np.min(dx2_centers),np.max(dx2_centers))
    print("dx3 eq  min = ", dph, np.sin(np.min(x2_centers))*dph)
    print("dx2_min / dx3_min = ", np.min(dx2_centers)/(np.sin(np.min(x2_centers))*dph))

    print("len(dx2_centers) = ", len(dx2_centers))
    print("len(xp2_centers) = ", len(xp2_centers))
    print("len(x2_centers) = ", len(x2_centers))
    print("len(x2_corners) = ", len(x2_corners))


    xp1_corners = make_xp_array(N1,pos='CORNER',include_ghost_cells=plot_ghost_cells)
    xp1_centers = make_xp_array(N1,pos='CENTER',include_ghost_cells=plot_ghost_cells)
    dxp1 = xp1_corners[1] - xp1_corners[0]

    xp1_0_1, x1_corners, dr_corners, dr2_corners  = r_of_xp1_hyperexp(xp1_corners, r0, rmin, rmax, R0, n_exp1)
    xp1_0_2, x1_centers, dr_centers, dr2_centers  = r_of_xp1_hyperexp(xp1_centers, r0, rmin, rmax, R0, n_exp1)
    print("xp1_0 = ", xp1_0_1, xp1_0_2)
    x1_2 = np.roll(x1_corners,1)
    dx1 = x1_corners - x1_2
    dx1_centers = dx1[1:]
    x1_m1 = x1_2
    x1_p1 = np.roll(x1_corners,-1)
    dx1_2 = x1_p1 - 2.*x1_corners + x1_m1 
    dx1_2_corners = dx1_2[1:]
    
    dr_corners *= dxp1
    dr_centers *= dxp1
    dr2_corners *= dxp1*dxp1
    dr2_centers *= dxp1*dxp1

    dth_min  = np.min(dx2_centers)
    r_hor = 2.
    ddth_max_vs_r_1 =  2.*dth_min*r_hor *x1_centers/ (x1_centers*dph)
    ddth_max_vs_r_2 = dth_max1 * x1_centers**(1.-1./2.)
    ddth_max_vs_r_3 = dth_max1 * x1_centers**(1.-1./3.)
    ddth_max_vs_r_4 = dth_max1 * x1_centers*(x1_centers/r_jet)**(-1./3.)
    ddth_max_vs_r_5 = dth_max1*x1_centers
    ddth_min  = dth_min * r_hor
    ddth_max  = dth_max1 * r_hor

    ##################
    # x y
    nth = len(x2_corners)
    nr = len(x1_corners)

    rout = np.outer(x1_corners,np.ones(nth))
    thout= np.outer(np.ones(nr),x2_corners)
    
    zz = rout * np.cos(thout)
    xx = rout * np.sin(thout)

    zz_hor = x1_corners * np.cos(h_o_r+0.5*np.pi)
    xx_hor = x1_corners * np.sin(h_o_r+0.5*np.pi)
    
    ########################
    # x2 plots :
    nwin = 0
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,x2_centers,'.')
    ax.plot([0.,0.],[0,np.pi])
    ax.plot([1.,1.],[0,np.pi])
    ax.plot([0,1],[0.,0.])
    ax.plot([0,1],[np.pi,np.pi])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\theta$')
    fig.savefig(tag+"x2.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,np.log10(dx2_centers),'.')
    ax.plot([0.,0.],[-30.,1.])
    ax.plot([1.,1.],[-30.,1.])
    ax.plot([0.,1.],np.log10([dth_min1,dth_min1]))
    ax.plot([0.,1.],np.log10([dth_max1,dth_max1]))
    ax.set_ylim([-5,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\log \Delta \theta$')
    fig.savefig(tag+"dx2-log.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp2_centers,(dx2_centers))
    ax.plot([0.,0.],[0.,1.])
    ax.plot([1.,1.],[0.,1.])
    ax.plot([0.,1.],[dth_min1,dth_min1])
    ax.plot([0.,1.],[dth_max1,dth_max1])
    #ax.set_ylim([-6,-0])
    ax.set_xlabel(r'$xp^2$')
    ax.set_ylabel(r'$\Delta \theta$')
    fig.savefig(tag+"dx2-lin.png", dpi=300)

    # x1 plots :
    nwin += 1 
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp1_centers,x1_centers,'.')
    ax.plot([xp1_0_1,xp1_0_1],[1e-10*r0,r0])
    ax.plot([0.,1.],[rmin,rmin])
    ax.plot([0.,1.],[rmax,rmax])
    ax.plot([0.,1.],[r0,r0])
    ax.set_xlabel(r'$xp^1$')
    ax.set_ylabel(r'$r$')
    fig.savefig(tag+"x1-lin.png", dpi=300)

    nwin += 1 
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp1_centers,np.log10(x1_centers),'.')
    ax.plot([xp1_0_1,xp1_0_1],np.log10([rmin,rmax]))
    ax.plot([0.,1.],np.log10([rmin,rmin]))
    ax.plot([0.,1.],np.log10([rmax,rmax]))
    ax.plot([0.,1.],np.log10([r0,r0]))
    ax.set_ylim([-2,6])
    ax.set_xlabel(r'$xp^1$')
    ax.set_ylabel(r'$\log r$')
    fig.savefig(tag+"x1-log.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp1_centers,np.log10(dx1_centers),'-',label='dx1',alpha=1.)
    ax.plot(xp1_corners,np.log10(dr_corners),'--',label='dr_corn',alpha=0.7)
    ax.plot(xp1_centers,np.log10(dr_centers),':',label='dr_cen',alpha=0.4)
    ax.set_ylim([-4,6])
    ax.set_xlabel(r'$xp^1$')
    ax.set_ylabel(r'$\log \Delta r$')
    ax.legend(loc=0)
    fig.savefig(tag+"dx1-log.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp1_centers,np.log10(dx1_2_corners),'-',label='dx1',alpha=1.)
    ax.plot(xp1_corners,np.log10(dr2_corners),'--',label='dr_corn',alpha=0.7)
    ax.plot(xp1_centers,np.log10(dr2_centers),':',label='dr_cen',alpha=0.4)
    ax.set_ylim([-8,6])
    ax.set_xlabel(r'$xp^1$')
    ax.set_ylabel(r'$\log \Delta r$')
    ax.legend(loc=0)
    fig.savefig(tag+"dx1_2-log.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(xp1_centers,(dx1_centers))
    ax.plot([0.,0.],[0.,1.])
    ax.plot([1.,1.],[0.,1.])
    ax.plot([0.,1.],[dth_min1,dth_min1])
    ax.plot([0.,1.],[dth_max1,dth_max1])
    #ax.set_ylim([-6,-0])
    ax.set_xlabel(r'$xp^1$')
    ax.set_ylabel(r'$\Delta r$')
    fig.savefig(tag+"dx1-lin.png", dpi=300)

    nwin += 1
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111)
    ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_1)))
    ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_2)))
    ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_3)))
    ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_4)))
    ax.plot(np.log10(x1_centers),np.log10((ddth_max_vs_r_5)))
    ax.plot(np.log10([1.,1000.]),np.log10([ddth_min,ddth_min]))
    ax.plot(np.log10([1.,1000.]),np.log10([ddth_max,ddth_max]))
    ax.plot(np.log10([r_jet,r_jet]),[-4,4])
    #ax.set_xlim([0.5,10])
    ax.set_xlabel(r'log $r$')
    ax.set_ylabel(r'log $r \Delta \theta$')
    fig.savefig(tag+"dx2-vs-r.png", dpi=300)

    nwin += 1
    rout = 100.
    fig = plt.figure(nwin,figsize=(8,6))
    fig.clf()
    ax = fig.add_subplot(111,aspect='equal')
    ax.set_ylim([-rout,rout])
    ax.set_xlim([-rout,rout])
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$z$')
    ax.plot(xx_hor, zz_hor,'r--',alpha=0.6)
    ax.plot(xx_hor,-zz_hor,'r--',alpha=0.6)
    for ith in np.arange(0,nth,nth_stride) :
        ax.plot(xx[:,ith],zz[:,ith],'w-')
        ax.plot(-xx[:,ith],zz[:,ith],'w-')
    for ir in np.arange(0,nr,nr_stride) :
        ax.plot(xx[ir,:],zz[ir,:],'w-')
        ax.plot(-xx[ir,:],zz[ir,:],'w-')
        
    fig.savefig(tag+"grid.png", dpi=300)

    return




###########################

#setup_grid(h_slope=0.1,diag3_exponent=21,N2=180,hor=0.3,tag='diag3-')

setup_grid2(N2=180,tag='mixed4-', plot_ghost_cells=False)

# ###############################################################################
# # This allows the python module to be executed like:   
# # 
# #   prompt>  python lum
# # 
# ###############################################################################
# if __name__ == "__main__":
#     import sys
#     print(sys.argv)
#     if len(sys.argv) > 1 :
#         lum(sys.argv[1])
#     else:
#         lum()
# 

