import cv2
import os

import glob 

def mkmovie(args) :

    movie_name = ''
    str = '*'
    for iarg in range(1,len(args)) :
        str += args[iarg]+'*'
        movie_name += args[iarg]+'-'

    str += '*'

    if( len(args) <= 1 ) :
        movie_name = 'movie'
    else : 
        movie_name = movie_name[0:len(movie_name)-1]
    
    image_folder = './'
    imglist = sorted(glob.glob(image_folder+str+'.png'))
    
    #images = [img for img in os.listdir(image_folder) if img.endswith(".png")]
    images = [img for img in imglist ]
    frame = cv2.imread(os.path.join(image_folder, images[0]))
    height, width, layers = frame.shape
    
    #video = cv2.VideoWriter(video_name, -1, 1, (width,height))
    video = cv2.VideoWriter(movie_name+'.avi', cv2.VideoWriter_fourcc(*"MJPG"), 30,(width,height))
    
    for image in images:
        filename=os.path.join(image_folder, image)
        print("Processing ", image,filename)
        video.write(cv2.imread(filename))
    #     if cv2.waitKey(1) & 0xFF == ord('q'):
    #         break 
    
    #cv2.destroyAllWindows()
    video.release()

    return 


###############################################################################
# This allows the python module to be executed like:   
# 
#   prompt>  python mk-movie [str1] [str2] ... [strn]
#
#      finds all the "*[str1]*[str2]*...*[strn]*.png" files, sorts
#      them, then makes a movie out of them called
#         
#       [str1]-[str2]-...-[strn].avi
#
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    mkmovie(sys.argv)

