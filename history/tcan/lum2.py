"""  

  lum.py: 
  ------------
    -- program to analyze the temporal energy spectrum of light curves from 
        various disk simulations#
    -- reads in data from the history files# 
    -- uses scipy methods for signal analysis#

"""

from __future__ import division
from __future__ import print_function

from scipy.fftpack import fft
import scipy.signal as signal 
import matplotlib.pyplot as plt
import numpy as np
import os,sys
import h5py

from get_sim_info import *
from readhist_h5_all import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################
###############################################################################

# def lum(runname='longer2',savepng=0):
"""  
Primary routine for analyzing light curve. 
"""

wfuncs = ['lomb','nuttal','kaiser','hann','boxcar','blackman','fft']
nwfuncs = len(wfuncs)

showplots=0
savepng=1

if not showplots :
    plt.ioff()


#for i in np.arange(nwfuncs) : 
for i in np.arange(1) : 
    winfunc = wfuncs[i]
    runname='longer2'
    
    dirout ='/home/scn/tex/mass-ratio-paper/'+runname+'/'
    
    pnames = ['dirname', 'freq_bin_orbit', 'rsep0', 'traj_file', 'header_file', 'hist_name', 'sigma_0', 't_lump_beg', 't_lump_end', 'hist_freq']
    
    sim_info = get_sim_info(pnames, runname=runname)
    
    print(" back in lum") 
    print( sim_info )
    
    #     fs = 10e3
    #     N = 1e5
    #     amp = 2*np.sqrt(2)
    #     freq = 1270.0
    #     noise_power = 0.001 * fs / 2
    #     time = np.arange(N) / fs
    #     x = amp*np.sin(2*np.pi*freq*time)
    #    x += np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
        
    #    f, Pper_spec = signal.periodogram(x, fs, 'blackmanharris', scaling='spectrum')
    #    f, Pper_spec = signal.periodogram(x, fs, 'nuttall', scaling='spectrum')
    #    f, Pper_spec = signal.welch(x, fs, scaling='spectrum')
    #    f, Pper_spec = signal.welch(x, fs, window='nuttall',scaling='spectrum')
    #    yf = fft(y)
    
    lum_dat, t_dat = readhist_h5_all('/Bound/H_Lut', sim_info['hist_name'])
    
    lcurve = np.sum(lum_dat,axis=1)
    lcurve = np.divide(lcurve,-sim_info['sigma_0'])
    
    loct = np.where( (sim_info['t_lump_beg'] <= t_dat) & (t_dat <= sim_info['t_lump_end']) )
    t_win = t_dat[loct[0]]
    lcurve_win = lcurve[loct[0]]
    
    #lcurve_win_detrend = signal.detrend(lcurve_win,type='linear')
    
    pfit = np.poly1d(np.polyfit(t_win,lcurve_win,deg=5))(t_win)
    lcurve_win_detrend = lcurve_win - pfit
    
    sampling_freq = 1./sim_info['hist_freq'][0] 
    
    if winfunc == 'fft' : 
        freq, fps_sq  = signal.periodogram(lcurve_win, sampling_freq, window=winfunc,scaling='spectrum')
    else : 
#        lcurve_win_detrend = np.cos(2.*sim_info['freq_bin_orbit']*t_win)
        freq_min = 1./(t_win[-1] - t_win[0])
        freq_max = 0.45*freq_min*len(t_win)
        freq = np.linspace(2.*np.pi*freq_min, 2.*np.pi*freq_max, len(t_win))
        fps_sq  = signal.lombscargle(t_win,lcurve_win_detrend, freq)
        ftmp = 1./len(t_win)
        fps_sq = np.multiply(fps_sq,ftmp)
        ftmp = 1./(2.*np.pi)
        freq = np.multiply(freq,ftmp)
    
    fps = np.sqrt(fps_sq)
    
    t_dat = np.multiply(t_dat,1.e-4)
    t_win = np.multiply(t_win,1.e-4)
    
    ftmp = 1./sim_info['freq_bin_orbit']
    freq = np.multiply(freq,ftmp)
    
    tag = '-'+winfunc
    nwin=0
    fignames = dirout + 'lightcurve-full'+tag
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylabel(r'Luminosity')
    ax.plot(t_dat, lcurve)
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'lightcurve-window'+tag))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylabel(r'Luminosity')
    ax.plot(t_win, lcurve_win, '-', t_win, pfit, '--')
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'lightcurve-window-detrend'+tag))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r't [$10^4$ M]')
    ax.set_ylabel(r'Luminosity')
    ax.plot(t_win, lcurve_win_detrend)
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'fft-lum-logy'+tag))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'$\omega [\Omega_{bin}]$')
    ax.set_ylabel(r'FPS of Luminosity')
#    plt.axis([5e-2,8.,1e-12,1.e-4])
    ax.semilogy(freq, fps)
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'fft-lum-loglog'+tag))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'$\omega [\Omega_{bin}]$')
    ax.set_ylabel(r'FPS of Luminosity')
   #plt.axis([5e-2,25.,1e-10,1.e-4])
    ax.loglog(freq, fps)
    
    nwin+=1
    fignames = np.append(fignames,(dirout + 'fft-lum-lin'+tag))
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    ax.set_xlabel(r'$\omega [\Omega_{bin}]$')
    ax.set_ylabel(r'FPS of Luminosity')
#    plt.axis([5e-2,3.,1e-12,1.e-4])
    ax.plot(freq, fps)
    
    
    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)
    
    if showplots : 
        plt.show()
    
    
    print("All done ")
    sys.stdout.flush()

#  
#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
