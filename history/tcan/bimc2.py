"""  

  bim.py: 
  ------------
    -- display image from bothros calculation

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################

def detrend(xfunc,yfunc) : 
    
#    pfit = np.poly1d(np.polyfit(xfunc,yfunc,deg=5))(xfunc)
    nlen = len(xfunc)
    pfit = np.poly1d(np.polyfit([xfunc[nlen-1],xfunc[0]],[yfunc[nlen-1],yfunc[0]],deg=1))(xfunc)
    y_detrend1 = yfunc - pfit
    y_final = y_detrend1 / np.max(y_detrend1)
    
    return y_final


###############################################################################
###############################################################################

def flux_conv(dirname='res_0300',
              dirout='./'
              ):
    """  
    lPrimary routine for analyzing light curve. 
    """

    showplots=0
    savepng=1
    nwin=0
    
    cmap  = plt.cm.Spectral_r
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 
    
    if not showplots :
        plt.ioff()
    
    files = sorted(glob.glob("res_*/light_curve_00.h5"))
    nfiles = len(files)

    print("files=", files)

    funcs = ['F_minidisks','F_circumbinary','F_cavity','F_tot']
    nfuncs = len(funcs)

    labels = ['300x300','500x500','800x800','1100x1100','1400x1400']

    for i in np.arange(nfiles) : 
        h5file = h5py.File(dirout+files[i])
        if( i == 0 ): 
            times =  h5file['times'].value
            nt = len(times)
            all_data = np.ndarray((nfiles,nfuncs,nt))
            
        for j in np.arange(nfuncs) : 
            all_data[i,j,:] = h5file[funcs[j]].value[:]
            
        h5file.close()


    figs = []
    fignames = []

    linetypes = ['k-','b-','r-','g-','k.','b.','r.','g.']

    for i in np.arange(nfuncs):
        fignames = np.append(fignames,(dirout + funcs[i]+'-conv'))
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        ax.set_xlabel(r't [M]')
        ax.set_ylabel(r'Flux')
        for j in np.arange(nfiles) :
            ax.plot(times, all_data[j,i,:], linetypes[j], label=labels[j])
        ax.legend(loc=0)
        nwin+=1
    
    
    if savepng :
        for i in np.arange(len(figs)) : 
            figs[i].savefig('%s.png' %fignames[i], dpi=300)
            
    print("All done ")
    sys.stdout.flush()


###############################################################################
###############################################################################

files = sorted(glob.glob("freq*/light_curve*.h5"))
#files = sorted(glob.glob("light_curve_*.h5"))
#pix_ratio = 1.8962962962962964
#cmaps = ['myReds','Violets','myGreens','myBlues']
#cmaps = ['myReds','Violets','myReds','myBlues']
cmaps = ['myReds','Violets','myReds','myIceyBlues']
dologs=[False,True,False,True]
#dologs=[True,True,True,True]
freqs = [1.e15,5.e15,3.e16,1.e19]
lofactors=[0.5,0.2,0.5,0.5]
maxall = 1.5e16
maxs =  np.multiply(np.ones(4),maxall)
#maxs = np.multiply(maxs,1e-1)
mins = np.copy(maxs)
mins *= [1e-6,1e-3,1e-6,1e-5]
#maxs = [None,None,None,None]
#mins = [None,None,None,None]

print("maxs = ", maxs)

if 1 : 
    i = 0 
    for file in files :
        for itime in np.arange(1) :
            print("doing file = ", file)
            tag = str('file%02d' %i) + str('%02d' %itime)
            h5file = h5py.File(file,'r')
            img = np.copy(h5file['I_image'].value[itime,:])
            freq = h5file['freq'].value[itime]
            img *= freq
            h5file.close()
            
            #make_image(filename=file,itime=itime,dolog=False,tag=tag+'-end-state',rmax=50.,aspect_ratio='equal',funcname='end_state')
            #make_image(filename=file,itime=itime,dolog=True,tag=tag+'-tau',rmax=50.,aspect_ratio='equal',funcname='I_image2')
            #make_image(filename=file,itime=itime,dolog=True,tag=tag+'-all',rmax=50.,aspect_ratio='equal')
            #make_image(filename=file,itime=itime,dolog=True,tag=tag+'-img-int',rmax=60.,image_only=True,width=8,height=4)
            make_image(filename=file,itime=itime,dolog=dologs[i],tag=tag+'-img2-int',rmax=60.,image_only=True,width=8,height=4,cmap_name=cmaps[i],cmap_reversed=True,set_over_color=False,set_under_color=False,minf=mins[i],maxf=maxs[i],imgout=img,color_transition=lofactors[i])
            #make_image(filename=file,itime=itime,dolog=True,tag=tag+'-img-tau',rmax=50.,funcname='I_image2',image_only=True,width=8,height=4)

        i += 1 

    
#flux_conv()

    



#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
