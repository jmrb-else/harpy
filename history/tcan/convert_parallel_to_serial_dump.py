"""  

  convert_parallel_to_serial_dump()
  ------------
    -- Converts old 3-d dump files to work with new "gather" IO routines in harm3d.
    -- Explicitly, it shrinks the arrays found in "/Header" of length = "nprocs" to 
       length "1". 

"""

from __future__ import print_function

import numpy as np
import h5py

import os,sys


###############################################################################
###############################################################################
###############################################################################

def convert_parallel_to_serial_dump(h5file):
    """  
    Returns parameters used in a run, specified by the run's nickname and list of parameter names. 
    """

    h5newfile = 'fixed-'+h5file

    # Get a list of all the parameters in the hdf5 header file:  
    fh5    = h5py.File(h5file,'r')
    fh5new = h5py.File(h5newfile,'w')

    all_h5_objs = []
    fh5.visit(all_h5_objs.append)

#    print(all_h5_objs)

    all_groups   = [ obj for obj in all_h5_objs if isinstance(fh5[obj],h5py.Group) ]
    all_datasets = [ obj for obj in all_h5_objs if isinstance(fh5[obj],h5py.Dataset) ]

    # Create all the groups: 
    for obj in all_groups : 
        fh5new.create_group(fh5[obj].name)

    print("  ") 

    # Copy over all datasets as is except for those in the Header, in which case 
    #  we copy over only the first element's worth of each array:
    for obj in all_datasets : 
        gd = fh5[obj]
        if '/Header' in gd.name  : 
            print(" Shrinking and Copying over :  " + gd.name)
            new_arr = np.zeros((1,), dtype=gd.dtype)
            gd.read_direct(new_arr, np.s_[0:1], np.s_[0:1])
            dnew = fh5new.create_dataset(gd.name, (1,) , dtype=gd.dtype, data=new_arr)

        else : 
            print(" Copying over :  " + gd.name)
            fh5.copy(obj,fh5new)

    ####################################################################################
    # We are done searching for parameters, now close up: 
    ####################################################################################
    fh5.close()
    fh5new.close()


###############################################################################
# This allows the python module to be executed like:
#
#   prompt>  python convert_parallel_to_serial_dump  <h5file> 
#
###############################################################################
if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) > 1 :
        convert_parallel_to_serial_dump(sys.argv[1])
    else:
        sys.exit("Need to specify the hdf5 file, Exiting ....")

