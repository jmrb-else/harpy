from __future__ import division
from __future__ import print_function
import os,sys
import numpy as np
import h5py 
import glob 
from get_sim_info import *
import matplotlib.pyplot as plt


# t_dat = 2.3*np.arange(10)
# f_dat = np.arange(50).reshape(10,5)
# loct = np.where( t_dat > 4. )
# t_new = t_dat[loct]
# f_new = f_dat[loct,:]

runname='longer2'

pnames = ['dirname', 'freq_bin_orbit', 'rsep0', 'traj_file', 'header_file', 'hist_name', 'sigma_0']

sim_info = get_sim_info(pnames, runname=runname)
hist_files = glob.glob(sim_info['hist_name']+"*.h5")
func_name = '/Bound/H_Lut'

first = True
t_dat = np.array([])
for hfile in  hist_files : 
    fh5 = h5py.File(hfile,'r')
    t_dat = np.append(t_dat, fh5['/H_t'].value,axis=0)
    if( first  ) : 
        f_dat = fh5[func_name].value
        first = False 
    else :
        f_dat = np.append(f_dat, fh5[func_name].value,axis=0)
    fh5.close()

t_orig = t_dat

t_thresh = 1.e-10
mask = ( t_dat > t_thresh )

# Mask out those points that are not monotonically increasing : 
min_t = t_dat[0] - 10. 
for i in np.arange(len(t_dat)) : 
    t_tmp = t_dat[i]
    if( t_tmp <= min_t ) : 
        mask[i] = False
    else : 
        min_t = t_tmp

loct = np.where( mask )

t_dat = t_dat[loct]
