
from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import h5py
import matplotlib 
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib.colors import Colormap
from scipy.interpolate import interp1d 
from coord_transforms import *
from harm import *
from matplotlib import ticker
from polp_lite import polp 


######################################

rhomin=-10.
rhomax=0.

Yemin=-10.
Yemax=0.5

bsq_min = -12.
bsq_max = -4.

uumin = bsq_min
uumax = bsq_max

divb_min = -20.
divb_max = 0.

k = 80
suf='-kslice'
dirout='./plots'

nt = 6
rmax = 15.
run_tag='KDHARM0'


if 1 : 
    kvals = np.arange(0,1)
    rmax = 100.
    xmax=10
    ymin=5
    ymax=10
    dirout = './plots'
    #for itime in np.arange(136,155) :
    for itime in np.arange(0,91,10) :
        for k in kvals : 
            suf = 'k='+str('%04d' %k)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,run_tag=run_tag,namesuffix=suf,funcname='rho',minf=rhomin,maxf=rhomax,dolog=True,rmax=rmax)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,run_tag=run_tag,namesuffix=suf,funcname='Ye',minf=Yemin,maxf=Yemax,dolog=True,rmax=rmax)
            polp(itime=itime,dirname="./",dirout=dirout,show_plot=False,savepng=True,irad=None,iph=k,ith=None,ubh=True,run_tag=run_tag,namesuffix=suf,funcname='Ye',minf=1e-20,maxf=1.1,dolog=False,rmax=rmax)


# 
