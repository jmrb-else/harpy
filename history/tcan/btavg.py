"""  

  btavg.py: 
  ------------
    -- script for time-averaging the frames of a MERGED bothros file; 

"""

from __future__ import division
from __future__ import print_function

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
import numpy as np
import os,sys
import h5py

import scipy.fftpack
from scipy import signal

import glob

from bothros import *

from IPython.core.debugger import Tracer


###############################################################################
###############################################################################



file ="../all_frames.h5"

# F_circumbinary           Dataset {182, 100}
# F_minidisks              Dataset {182, 100}
# F_tot                    Dataset {182, 100}
# Header                   Group
# I_image                  Dataset {182, 100, 500, 500}
# I_image2                 Dataset {182, 100, 500, 500}
# freq                     Dataset {182, 100}
# id                       Dataset {1, 500, 500}
# pix_area                 Dataset {1, 500, 500}
# pix_x                    Dataset {1, 500, 500}
# pix_y                    Dataset {1, 500, 500}
# snap_id                  Dataset {182, 100}

nfreq = 100

sl_all = slice(None,None,None)

if 1 :
    h5in  = h5py.File(file,'r')
    h5out = h5py.File("test-avg",'w')

    funcname = 'I_image'
#    for ifreq in np.arange(nfreq) :
    for ifreq in np.arange(2) :
        print("Processing...  ", funcname, "  at ifreq=", ifreq)
        slice_obj = (sl_all,slice(ifreq,ifreq+1,None),sl_all,sl_all)
        ftmp = read_3d_hdf5_slice(funcname,slice_obj,h5in)
        nt = func.shape[0]
        ftot = np.sum(ftmp,axis=0)
        ftot /= nt
        if( ifreq == 0 ) : 
            dset = fdest.create_dataset(funcname+'_tavg', (nfreq,func.shape[1],func.shape[2]), dtype=h5in[funcname].dtype)
        
        dset[ifreq,:,:] = ftot[:,:]
        

    funcname = 'I_image2'
#    for ifreq in np.arange(nfreq) :
    for ifreq in np.arange(2) :
        print("Processing...  ", funcname, "  at ifreq=", ifreq)
        slice_obj = (sl_all,slice(ifreq,ifreq+1,None),sl_all,sl_all)
        ftmp = read_3d_hdf5_slice(funcname,slice_obj,h5in)
        nt = func.shape[0]
        ftot = np.sum(ftmp,axis=0)
        ftot /= nt
        if( ifreq == 0 ) : 
            dset = fdest.create_dataset(funcname+'_tavg', (nfreq,func.shape[1],func.shape[2]), dtype=h5in[funcname].dtype)
        
        dset[ifreq,:,:] = ftot[:,:]
        

    h5in.close()
    h5out.close()
    

#  ###############################################################################
#  # This allows the python module to be executed like:   
#  # 
#  #   prompt>  python lum
#  # 
#  ###############################################################################
#  if __name__ == "__main__":
#      import sys
#      print(sys.argv)
#      if len(sys.argv) > 1 :
#          lum(sys.argv[1])
#      else:
#          lum()
#  
