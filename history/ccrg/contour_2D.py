#!/usr/bin/env python
import h5py
import matplotlib

matplotlib.use('Agg')
import numpy as np
from pylab import *
from matplotlib.pyplot import figure, show, rc, grid

from matplotlib.ticker import MultipleLocator, FormatStrFormatter

from numpy import *
import matplotlib.pyplot as plt

import matplotlib.font_manager

from pylab import figure, cm
from matplotlib.colors import LogNorm

from matplotlib import ticker


#rc('text', usetex=True)
#rc('font',**{'family':'serif','serif':['Computer Modern Roman']})

#########################################

#ymajorLocator   = MultipleLocator(2.0e4) #(xmax-xmin)/5.0)
#yminorLocator   = MultipleLocator(1.0e4)

#ax.xaxis.set_minor_locator(xminorLocator)
#ax.yaxis.set_minor_locator(yminorLocator)



xmajorFormatter = FormatStrFormatter('%1.1f')
ymajorFormatter = FormatStrFormatter('%1.1f')

#########################################

# directory where data is located
directory = '../dumps/'
out_dir = 'contours/'

n_i = 0
n_f = 51
dn = 1


#########################################

def read_init(name):

    global x1, x2, x3
    global N1, N2, N3
    global dx1, dx2, dx3
    global L1, L2, L3
    global X0, Y0, Z0
    timeid   = '%06d' % 0
    dumpfile = directory+name+timeid+'.h5' 
    h5file   = h5py.File(dumpfile,'r')

    x1 = h5file['x1'].value
    x2 = h5file['x2'].value
    x3 = h5file['x3'].value

    N1 = h5file['Header/Grid/totalsize1'].value[0]
    N2 = h5file['Header/Grid/totalsize2'].value[0]
    N3 = h5file['Header/Grid/totalsize3'].value[0]
    
    L1 = h5file['Header/Grid/gridlength1'].value[0]
    L2 = h5file['Header/Grid/gridlength1'].value[0]
    L3 = h5file['Header/Grid/gridlength1'].value[0]
    
    h5file.close()
########################################33

def main():

    # patch 0 #########################################

    name = 'SOD.patch_0.'

    read_init(name)
    
    x1_0 = x1
    x2_0 = x2
    x3_0 = x3

    N1_0 = N1
    N2_0 = N2
    N3_0 = N3
            
    xmin1_0 = -20.0
    xmax1_0 = 20.0

    xmin2_0 = -20.0
    xmax2_0 = 20.0
    
    
    # patch 1 #########################################
        
    name = 'SOD.patch_1.'

    read_init(name)
    
    x1_1 = x1
    x2_1 = x2
    x3_1 = x3

    N1_1 = N1
    N2_1 = N2
    N3_1 = N3

    xmin1_1 = -4.0
    xmax1_1 = 4.0
    xmin2_1 = -4.0
    xmax2_1 = 4.0
    
    for n in range(n_i,n_f,dn):

        timeid   = '%06d' % n        
        filename_o = 'fig.' + timeid + '.png'
        
        # patch 0 #########################################
        name = 'SOD.patch_0.'

        Ntot1 = N1_0
        Ntot2 = N2_0
        
        xmin1 = xmin1_0
        xmax1 = xmax1_0
        xmin2 = xmin2_0
        xmax2 = xmax2_0
        

        dumpfile = directory+name+timeid+'.h5' 
        print "%s\n" % dumpfile
        h5file   = h5py.File(dumpfile,'r')


        
        rho = h5file['rho'].value
       
        #################################

        rho_2D = np.zeros((Ntot1,Ntot2))
        k = 0 # Ntot/2

        for i in range(0, Ntot1):
            for j in range(0, Ntot2):
                rho_2D[i][j] = rho[i][j][k]

        rho_2D = np.transpose(rho_2D)
        rho_2D = np.flipud(rho_2D)

        ################################
       
        Z_0 = rho_2D
        

        # patch 1 #########################################
        name = 'SOD.patch_1.'

        Ntot1 = N1_1
        Ntot2 = N2_1

        xmin1 = xmin1_1
        xmax1 = xmax1_1
        xmin2 = xmin2_1
        xmax2 = xmax2_1        

        

        dumpfile = directory+name+timeid+'.h5' 
        print "%s\n" % dumpfile
        h5file   = h5py.File(dumpfile,'r')

        X0 = h5file['Header/Grid/centx_cart1'].value[0]
        Y0 = h5file['Header/Grid/centx_cart2'].value[0]
        Z0 = h5file['Header/Grid/centx_cart3'].value[0]

        xmin1 += X0
        xmax1 += X0
        xmin2 += Y0
        xmax2 += Y0

        print "X0 = %1.1e, " % X0 + "Y0 = %1.1e, " % Y0 + "Z0 = %1.1e\n" % Z0
        rho = h5file['rho'].value
       
        #################################

        rho_2D = np.zeros((Ntot1,Ntot2))
        k = 0 # Ntot/2

        for i in range(0, Ntot1):
            for j in range(0, Ntot2):
                rho_2D[i][j] = rho[i][j][k]

        rho_2D = np.transpose(rho_2D)
        rho_2D = np.flipud(rho_2D)

        ################################
       
        Z_1 = rho_2D
        
#        zmin = np.amin(Z)
#        zmax = np.amax(Z)

        ########################################

        zmin = np.amin(Z_0)
        zmax = np.amax(Z_0)
        
        ax = subplot(211)
        
        cs = plt.imshow(Z_0,interpolation='nearest',cmap='jet',vmin=zmin,vmax=zmax,extent = (xmin1_0,xmax1_0,xmin2_0,xmax2_0))
#        cs = plt.pcolor(Z,extent =(xmin,xmax,xmin,xmax))
        tick_locator = ticker.MaxNLocator(nbins=5)
        cb = plt.colorbar(cs,format = '%1.1e')
#        cb = plt.colorbar(cs,orientation = 'horizontal',format = '%1.1e')
        cb.locator = tick_locator
        cb.update_ticks()

       
        circle = plt.Circle((X0, Y0), 0.5, color='k')
        ax.add_artist(circle)

        
        ax.yaxis.set_major_formatter(ymajorFormatter)
        ax.xaxis.set_major_formatter(xmajorFormatter)

        xmajorLocator   = MultipleLocator((xmax1_0-xmin1_0)/5.0)
        ymajorLocator   = MultipleLocator((xmax2_0-xmin2_0)/5.0)
        
        ax.xaxis.set_major_locator(xmajorLocator)
        ax.yaxis.set_major_locator(ymajorLocator)
        
        ########################################

        ax = subplot(212)

        cs = plt.imshow(Z_1,interpolation='nearest',cmap='jet',vmin=zmin,vmax=zmax,extent = (xmin1,xmax1,xmin2,xmax2))        

#        cs = plt.pcolor(Z,extent =(xmin,xmax,xmin,xmax))
        tick_locator = ticker.MaxNLocator(nbins=5)
#        cb = plt.colorbar(cs,orientation = 'horizontal',format = '%1.1e')
        cb = plt.colorbar(cs,format = '%1.1e')
        cb.locator = tick_locator
        cb.update_ticks()
        
        ax.yaxis.set_major_formatter(ymajorFormatter)
        ax.xaxis.set_major_formatter(xmajorFormatter)

        xmajorLocator   = MultipleLocator((xmax1-xmin1)/5.0)
        ymajorLocator   = MultipleLocator((xmax2-xmin2)/5.0)        

        ax.xaxis.set_major_locator(xmajorLocator)
        ax.yaxis.set_major_locator(ymajorLocator)

        #################################

        savefig(filename_o,bbox_inches='tight')
        clf()
        #################################
        h5file.close()
    



####################################################################################################################################
if __name__ == "__main__":
    main()
