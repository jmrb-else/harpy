
from __future__ import division
from __future__ import print_function

import numpy as np
#import h5py

#from scipy.interpolate import griddata
#from scipy import random

from scipy.optimize import brentq

import matplotlib
import matplotlib.pyplot as plt 

import os,sys

sys.path.append('/home/dennis')
from colormaps import *
#from polp import polp

# the following module allows one to start an ipython session at a given point
# in the file with the command IPython.embed(). this allows us to access the
# local functions and variables.
import IPython


def stepfunc(x,x1,h):
    y = x - x1
    return np.tanh(h*(y))

# Square function with baseline = 0 , amplitude = 1 -- also called "tophat" in
# latex document, "square" in latex document is something else;
def square(x,x1,h,delta):
    return 0.5*(stepfunc(x,x1-delta,h) - stepfunc(x,x1+delta,h))

#Periodic square function over  x \in [0,1]
def square_per(x,x1,h,delta):
    return square(x,x1,h,delta)+square(x,x1-1.,h,delta)+square(x,x1+1.,h,delta)

def intstepfunc(x,x1,h):
    y = (x-x1)
    return np.log(np.cosh(h*(y)))/h 

def intsquare(x,x1,h,delta):
    return 0.5*(intstepfunc(x,x1-delta,h) - intstepfunc(x,x1+delta,h)) 

def intsquare_per(x,x1,h,delta):
    return intsquare(x,x1,h,delta)+intsquare(x,x1-1.,h,delta)+intsquare(x,x1+1.,h,delta)


def tcoord(nxp1=64, nxp2=65, nxp3=128, iout=None, jout=None, kout=None,
           xpmin=1e-10, xpmax=1.,
           x_bh1=0., x_bh2=0.5, z_bh1=0.5, z_bh2=0.5, r_bh1=10., r_bh2=10., r_bh3=None,
           xbh1_phys=None, ybh1_phys = 0, xbh2_phys = None, ybh2_phys = 0,
           Rout_1=300, Rout_2=300, Rout_3=300,
           Rin_1=0.1, Rin_2=0.1, Rin_3=0.1,
           br_1=1., br_2=1., br_3=40., 
           s_1=4, s_2=4, s_3=4,
           h_x1=30., h_x2=30., h_x3=15., h_x4=15., h_y1=10., h_y2=10., h_z1=20., 
                     h_z2=20.,
           delta_x1=0.05, delta_x2=0.05, delta_x3=0.1, delta_x4=0.1,
           delta_y1=0.1, delta_y2=0.1, delta_z1=0.4,  delta_z2=0.4,
           a_x10=1.5, a_x20=1.5, a_z10=4.3,
           dirout=None, nlev=256, nticks=10, nperplot=5, savepng=False, savepdf=False,
           xminj=None, xmaxj=None, yminj=None, ymaxj=None, xmink=None, xmaxk=None, zmink=None, zmaxk=None,
           funcnames=None, plot_grid=True, plot_bhs=False, plot_disk=False,
           time=0, namesuffix='', show_plot=True,
           nwin=0, background='white', plottitlek=None,plottitlej=None,
           outfilename=None, r_disk=30, mbh1=0.5, mbh2=0.5 ):
    
    cmap = plt.cm.Spectral_r

    # plot_grid ->  plot numerical grid?

    # xpi = (y, z, x). these are the numerical (uniform) coordinates

    # xp1 is the y coordinate in the latex notes. roughly related with a radial
    # coordinate r.

    # xp3 is the x coordinate in the latex notes. roughly related with an
    # azimuthal coordinate phi

    # xp2 is the z coordinate in the latex notes. roughly related with a
    # poloidal coordinate theta.


    ######################### setting default parameters #####################

    if dirout is None:
        dirout = os.getcwd()


    if background is 'black':
        grid_colour = 'white'
    else:
        grid_colour = 'black'


    if iout is None:
        iout=int(nxp1/2)      # not used
    if jout is None:
        jout=int((nxp2)/2)
    if kout is None:
        kout=0

    if r_bh3 is None:
        r_bh3 = max(r_bh1, r_bh2)

    if xbh1_phys is None:
        xbh1_phys = r_bh1

    if xbh2_phys is None:
        xbh2_phys = -r_bh2

    # if funcnames is None:
    #     # funcnames = ['drdxp1', 'drdxp2', 'drdxp3']
    #     funcnames = ['drdxp1']
    if funcnames is 'all':
        funcnames = ['drdxp1', 'drdxp2', 'drdxp3', 
                     'dphidxp1', 'dphidxp2', 'dphidxp3',
                     'dthdxp1', 'dthdxp2', 'dthdxp3',
                     'd2rdxp11', 'd2rdxp12', 'd2rdxp13',
                     'd2rdxp21', 'd2rdxp22', 'd2rdxp23',
                     'd2rdxp31', 'd2rdxp32', 'd2rdxp33',
                     'd2phidxp11', 'd2phidxp12', 'd2phidxp13', 
                     'd2phidxp21', 'd2phidxp22', 'd2phidxp23', 
                     'd2phidxp31', 'd2phidxp32', 'd2phidxp33',
                     'd2thdxp11', 'd2thdxp12', 'd2thdxp13',
                     'd2thdxp21', 'd2thdxp22', 'd2thdxp23',
                     'd2thdxp31', 'd2thdxp32', 'd2thdxp33',
                     'jac']
    elif funcnames is 'resolution':
        funcnames = ['dr','rdth','rdph']
    ###########################################################################

    def rfunc(y, ybh_i, Rout_i, Rin_i, br_i, s_i):

        B = ( Rout_i - Rin_i - br_i ) / ( np.sinh( s_i*(1.-ybh_i) ) / s_i     \
                                        - np.sinh( -s_i*ybh_i ) / s_i         \
                                        - 1.0 )
        btmp = br_i - B
        ftmp = B/s_i

        rf =  Rin_i + btmp*y + ftmp * ( np.sinh( s_i*(y-ybh_i) )
                                            - np.sinh(-s_i*ybh_i) )
        return rf

    # we want to find y_bh such that rfunc(y_bh,y_bh) = r_bh
    def newtfunc(ybh_i, Rout_i, Rin_i, br_i, s_i, rbh_i):
        return rfunc(ybh_i, ybh_i, Rout_i, Rin_i, br_i, s_i) - rbh_i


    # grid setup

    dxp1 = (xpmax-xpmin)/(nxp1)
    dxp2 = (xpmax-xpmin)/(nxp2)
    dxp3 = (xpmax-xpmin)/(nxp3)
    print( 'dxp', dxp1,dxp2,dxp3)

    # xp1 = np.linspace(xpmin,xpmax,nxp1) 
    # xp2 = np.linspace(xpmin,xpmax,nxp2)
    # xp3 = np.linspace(xpmin,xpmax,nxp3)

    # this way is better to make contact with the grid from harm

    xp1 = np.arange(xpmin,xpmax,dxp1) + dxp1*0.5
    xp2 = np.arange(xpmin,xpmax,dxp2) + dxp2*0.5
    xp3 = np.arange(xpmin,xpmax,dxp3) + dxp3*0.5

    zout, yout, xout = np.meshgrid(xp2, xp1, xp3)


    # we compute y_bh (the xp1 coordinate of each BH) with a root-finding algorithm
    y_bh1 = brentq(newtfunc, xpmin,xpmax, args=(Rout_1, Rin_1, br_1, s_1, r_bh1) )
    y_bh2 = brentq(newtfunc, xpmin,xpmax, args=(Rout_2, Rin_2, br_2, s_2, r_bh2) )
    y_bh3 = brentq(newtfunc, xpmin,xpmax, args=(Rout_3, Rin_3, br_3, s_3, r_bh3) )

    print('y_bh = ', y_bh1, y_bh2, y_bh3)


    # now we can compute r_of_xi
    r_of_x1_out = rfunc(yout, y_bh1, Rout_1, Rin_1, br_1, s_1)
    r_of_x2_out = rfunc(yout, y_bh2, Rout_2, Rin_2, br_2, s_2)
    r_of_x3_out = rfunc(yout, y_bh3, Rout_3, Rin_3, br_3, s_3)


    # and we now compute the coordinates (r,phi,theta) as grid functions of the
    # numerical coordinates xp (yout, xout, zout)

    xfunc = xout - square_per(zout,z_bh2,h_z2,delta_z2) * ( 
                                                            a_x10 * square(yout,y_bh1,h_y1,delta_y1) * ( 
                                                                                                           intsquare_per(xout,x_bh1,h_x1,delta_x1)
                                                                                                         - intsquare_per(x_bh1,x_bh1,h_x1,delta_x1)
                                                                                                         - (xout-x_bh1)*(  intsquare_per(1,x_bh1,h_x1,delta_x1)
                                                                                                                         - intsquare_per(0,x_bh1,h_x1,delta_x1)) 
                                                                                                           )
                                                         +  a_x20 * square(yout,y_bh2,h_y2,delta_y2) * ( 
                                                                                                           intsquare_per(xout,x_bh2,h_x2,delta_x2)
                                                                                                         - intsquare_per(x_bh2,x_bh2,h_x2,delta_x2)
                                                                                                         - (xout-x_bh2)*(  intsquare_per(1,x_bh2,h_x2,delta_x2)
                                                                                                                         - intsquare_per(0,x_bh2,h_x2,delta_x2)) 
                                                                                                           )  
                                                            )
    phiout =  2.*np.pi*xfunc 

    rout = r_of_x3_out + square_per(zout,z_bh1,h_z2,delta_z2) * ( 
                                                                  (r_of_x1_out - r_of_x3_out)*square_per(xout,x_bh1,h_x3,delta_x3)
                                                                + (r_of_x2_out - r_of_x3_out)*square_per(xout,x_bh2,h_x4,delta_x4)
                                                                  )

    thout = np.pi*( zout - a_z10 * (   intsquare_per(zout,z_bh1,h_z1,delta_z1)
                                     - intsquare_per(z_bh1,z_bh1,h_z1,delta_z1)
                                     - (zout-z_bh1)*(  intsquare_per(1,z_bh1,h_z1,delta_z1)
                                                     - intsquare_per(0,z_bh1,h_z1,delta_z1)) )
                    )

    #Calculate the spacings in drout and rout * dphiout for timestep estimates
    drmin = 2.e6
    dthmin = 2.e6
    dphimin = 2.e6
    for i in xrange(0,rout.shape[0]-1):
        for j in xrange(0,rout.shape[1]):
            for k in xrange(0,rout.shape[2]):
                dr = rout[i+1,j,k] - rout[i,j,k]
                if dr < drmin:
                    drmin = dr
    for i in xrange(0,rout.shape[0]):
        for j in xrange(0,rout.shape[1]-1):
            for k in xrange(0,rout.shape[2]):
                dth = rout[i,j,k] * (thout[i,j+1,k] - thout[i,j,k])
                if dth < dthmin:
                    dthmin = dth
    for i in xrange(0,rout.shape[0]):
        for j in xrange(0,rout.shape[1]):
            for k in xrange(0,rout.shape[2]-1):
                dphi = rout[i,j,k] * (phiout[i,j,k+1] - phiout[i,j,k])
                if dphi < dphimin:
                    dphimin = dphi

    print("Cell count ", nxp1, nxp2, nxp3, " Rout0 ", Rout_1)
    # drmin  = np.min(np.diff(rout,axis=0))
    # dthmin = np.diff(thout,axis=1)
    # zerrarr = np.zeros((np.shape(thout)[0],1,np.shape(thout)[2]))
    # zerrarr[:,0,:] = dthmin[:,0,:]
    # dthmin = np.append(dthmin,   zerrarr,   axis=1)
    # dthmin = np.min(rout * dthmin)
    # dphimin = np.diff(phiout,axis=2)
    # zerrarr = np.zeros((np.shape(phiout)[0],np.shape(phiout)[1],1))
    # zerrarr[:,:,0] = dphimin[:,:,0]
    # dphimin = np.append(dphimin,   zerrarr,   axis=2)
    # dphimin = np.min(rout * dphimin)

    #dphimin = np.min(np.diff(rout * phiout,axis=2))
    #dthmin  = np.min(np.diff(rout * thout,axis=1))
    print('Equatorial timestep drmin=',drmin,' and rdphimin=', dphimin, ' and rdthmin=', dthmin)
    equatorial_step = min(drmin,dphimin)/3.
    print('Equatorial Estimated timestep dt = ', equatorial_step)
    poloidal_step = min(equatorial_step,dthmin)
    print('Poloidal   Estimated timestep dt = ', poloidal_step)
    print('Smallest   Estimated timestep dt = ', min(equatorial_step,poloidal_step))


#    IPython.embed()

    # we now compute first and second derivatives of these grid functions with
    # respect to the numerical coordinates xp1, xp2, xp3

#    drdxp1, drdxp2, drdxp3 = np.gradient(rout, dxp1, dxp2, dxp3)
#    dphidxp1, dphidxp2, dphidxp3 = np.gradient(phiout, dxp1, dxp2, dxp3)
#    dthdxp1, dthdxp2, dthdxp3 = np.gradient(thout, dxp1, dxp2, dxp3)


#    d2rdxp11, d2rdxp12, d2rdxp13 = np.gradient(drdxp1, dxp1, dxp2, dxp3)
#    d2rdxp21, d2rdxp22, d2rdxp23 = np.gradient(drdxp2, dxp1, dxp2, dxp3)
#    d2rdxp31, d2rdxp32, d2rdxp33 = np.gradient(drdxp3, dxp1, dxp2, dxp3)

#    d2phidxp11, d2phidxp12, d2phidxp13 = np.gradient(dphidxp1, dxp1, dxp2, dxp3)
#    d2phidxp21, d2phidxp22, d2phidxp23 = np.gradient(dphidxp2, dxp1, dxp2, dxp3)
#    d2phidxp31, d2phidxp32, d2phidxp33 = np.gradient(dphidxp3, dxp1, dxp2, dxp3)

#    d2thdxp11, d2thdxp12, d2thdxp13 = np.gradient(dthdxp1, dxp1, dxp2, dxp3)
#    d2thdxp21, d2thdxp22, d2thdxp23 = np.gradient(dthdxp2, dxp1, dxp2, dxp3)
#    d2thdxp31, d2thdxp32, d2thdxp33 = np.gradient(dthdxp3, dxp1, dxp2, dxp3)


    #  we will want to have a dictionary between function names and their values
    dict_j = {}
    dict_k = {}


    # this way we save memory: we check if any element in funcnames is in the
    # set (ie, if we asked for the function). if so, then we compute the
    # gradients. we also update the dictionary

    if funcnames is not None and len(set(funcnames).intersection(set(('drdxp1', 'drdxp2', 'drdxp3', 
                                            'd2rdxp11', 'd2rdxp12', 'd2rdxp13',
                                            'd2rdxp21', 'd2rdxp22', 'd2rdxp23',
                                            'd2rdxp31', 'd2rdxp32', 'd2rdxp33')))) != 0:

        drdxp1, drdxp2, drdxp3 = np.gradient(rout, dxp1, dxp2, dxp3)
        dict_j.update({'drdxp1':drdxp1[:,jout,:], 'drdxp2':drdxp2[:,jout,:],
                       'drdxp3':drdxp3[:,jout,:]})
        dict_k.update({'drdxp1':drdxp1[:,:,kout], 'drdxp2':drdxp2[:,:,kout],
                       'drdxp3':drdxp3[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2rdxp11', 'd2rdxp12', 'd2rdxp13')))) != 0:
            d2rdxp11, d2rdxp12, d2rdxp13 = np.gradient(drdxp1, dxp1, dxp2, dxp3)
            dict_j.update({'d2rdxp11':d2rdxp11[:,jout,:],
                           'd2rdxp12':d2rdxp12[:,jout,:], 
                           'd2rdxp13':d2rdxp13[:,jout,:]})
            dict_k.update({'d2rdxp11':d2rdxp11[:,:,kout],
                           'd2rdxp12':d2rdxp12[:,:,kout], 
                           'd2rdxp13':d2rdxp13[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2rdxp21', 'd2rdxp22', 'd2rdxp23')))) != 0:
            d2rdxp21, d2rdxp22, d2rdxp23 = np.gradient(drdxp2, dxp1, dxp2, dxp3)
            dict_j.update({'d2rdxp21':d2rdxp21[:,jout,:],
                           'd2rdxp22':d2rdxp22[:,jout,:], 
                           'd2rdxp23':d2rdxp23[:,jout,:]})
            dict_k.update({'d2rdxp21':d2rdxp21[:,:,kout],
                           'd2rdxp22':d2rdxp22[:,:,kout], 
                           'd2rdxp23':d2rdxp23[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2rdxp31', 'd2rdxp32', 'd2rdxp33')))) != 0:
            d2rdxp31, d2rdxp32, d2rdxp33 = np.gradient(drdxp3, dxp1, dxp2, dxp3)
            dict_j.update({'d2rdxp31':d2rdxp31[:,jout,:],
                           'd2rdxp32':d2rdxp32[:,jout,:], 
                           'd2rdxp33':d2rdxp33[:,jout,:]})
            dict_k.update({'d2rdxp31':d2rdxp31[:,:,kout],
                           'd2rdxp32':d2rdxp32[:,:,kout], 
                           'd2rdxp33':d2rdxp33[:,:,kout]})


    if funcnames is not None and len(set(funcnames).intersection(set(('dphidxp1', 'dphidxp2', 'dphidxp3', 
                                            'd2phidxp11', 'd2phidxp12', 'd2phidxp13',
                                            'd2phidxp21', 'd2phidxp22', 'd2phidxp23',
                                            'd2phidxp31', 'd2phidxp32', 'd2phidxp33'))))  != 0:

        dphidxp1, dphidxp2, dphidxp3 = np.gradient(phiout, dxp1, dxp2, dxp3)
        dict_j.update({'dphidxp1':dphidxp1[:,jout,:], 'dphidxp2':dphidxp2[:,jout,:],
                       'dphidxp3':dphidxp3[:,jout,:]})
        dict_k.update({'dphidxp1':dphidxp1[:,:,kout], 'dphidxp2':dphidxp2[:,:,kout],
                       'dphidxp3':dphidxp3[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2phidxp11', 'd2phidxp12', 'd2phidxp13')))) != 0:
            d2phidxp11, d2phidxp12, d2phidxp13 = np.gradient(dphidxp1, dxp1, dxp2, dxp3)
            dict_j.update({'d2phidxp11':d2phidxp11[:,jout,:],
                           'd2phidxp12':d2phidxp12[:,jout,:], 
                           'd2phidxp13':d2phidxp13[:,jout,:]})
            dict_k.update({'d2phidxp11':d2phidxp11[:,:,kout],
                           'd2phidxp12':d2phidxp12[:,:,kout], 
                           'd2phidxp13':d2phidxp13[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2phidxp21', 'd2phidxp22', 'd2phidxp23')))) != 0:
            d2phidxp21, d2phidxp22, d2phidxp23 = np.gradient(dphidxp2, dxp1, dxp2, dxp3)
            dict_j.update({'d2phidxp21':d2phidxp21[:,jout,:],
                           'd2phidxp22':d2phidxp22[:,jout,:], 
                           'd2phidxp23':d2phidxp23[:,jout,:]})
            dict_k.update({'d2phidxp21':d2phidxp21[:,:,kout],
                           'd2phidxp22':d2phidxp22[:,:,kout], 
                           'd2phidxp23':d2phidxp23[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2phidxp31', 'd2phidxp32', 'd2phidxp33')))) != 0:
            d2phidxp31, d2phidxp32, d2phidxp33 = np.gradient(dphidxp3, dxp1, dxp2, dxp3)
            dict_j.update({'d2phidxp31':d2phidxp31[:,jout,:],
                           'd2phidxp32':d2phidxp32[:,jout,:], 
                           'd2phidxp33':d2phidxp33[:,jout,:]})
            dict_k.update({'d2phidxp31':d2phidxp31[:,:,kout],
                           'd2phidxp32':d2phidxp32[:,:,kout], 
                           'd2phidxp33':d2phidxp33[:,:,kout]})


    if funcnames is not None and len(set(funcnames).intersection(set(('dthdxp1', 'dthdxp2', 'dthdxp3', 
                                            'd2thdxp11', 'd2thdxp12', 'd2thdxp13',
                                            'd2thdxp21', 'd2thdxp22', 'd2thdxp23',
                                            'd2thdxp31', 'd2thdxp32', 'd2thdxp33')))) != 0:

        dthdxp1, dthdxp2, dthdxp3 = np.gradient(thout, dxp1, dxp2, dxp3)
        dict_j.update({'dthdxp1':dthdxp1[:,jout,:], 'dthdxp2':dthdxp2[:,jout,:],
                       'dthdxp3':dthdxp3[:,jout,:]})
        dict_k.update({'dthdxp1':dthdxp1[:,:,kout], 'dthdxp2':dthdxp2[:,:,kout],
                       'dthdxp3':dthdxp3[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2thdxp11', 'd2thdxp12', 'd2thdxp13')))) != 0:
            d2thdxp11, d2thdxp12, d2thdxp13 = np.gradient(dthdxp1, dxp1, dxp2, dxp3)
            dict_j.update({'d2thdxp11':d2thdxp11[:,jout,:],
                           'd2thdxp12':d2thdxp12[:,jout,:], 
                           'd2thdxp13':d2thdxp13[:,jout,:]})
            dict_k.update({'d2thdxp11':d2thdxp11[:,:,kout],
                           'd2thdxp12':d2thdxp12[:,:,kout], 
                           'd2thdxp13':d2thdxp13[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2thdxp21', 'd2thdxp22', 'd2thdxp23')))) != 0:
            d2thdxp21, d2thdxp22, d2thdxp23 = np.gradient(dthdxp2, dxp1, dxp2, dxp3)
            dict_j.update({'d2thdxp21':d2thdxp21[:,jout,:],
                           'd2thdxp22':d2thdxp22[:,jout,:], 
                           'd2thdxp23':d2thdxp23[:,jout,:]})
            dict_k.update({'d2thdxp21':d2thdxp21[:,:,kout],
                           'd2thdxp22':d2thdxp22[:,:,kout], 
                           'd2thdxp23':d2thdxp23[:,:,kout]})

        if len(set(funcnames).intersection(set(('d2thdxp31', 'd2thdxp32', 'd2thdxp33')))) != 0:
            d2thdxp31, d2thdxp32, d2thdxp33 = np.gradient(dthdxp3, dxp1, dxp2, dxp3)
            dict_j.update({'d2thdxp31':d2thdxp31[:,jout,:],
                           'd2thdxp32':d2thdxp32[:,jout,:], 
                           'd2thdxp33':d2thdxp33[:,jout,:]})
            dict_k.update({'d2thdxp31':d2thdxp31[:,:,kout],
                           'd2thdxp32':d2thdxp32[:,:,kout], 
                           'd2thdxp33':d2thdxp33[:,:,kout]})
       
        #if len(set(funcnames).intersection(set(('jac')))) != 0:
        if funcnames is not None:
            print('Jacobian only implemented for x-y plane')
            jac = drdxp1*dphidxp3 - drdxp3*dphidxp1
            dict_j.update({'jac':jac[:,jout,:]})
            dict_k.update({'jac':jac[:,:,kout]})
            print('Jacobian minimum is ',dict_j['jac'].min())
            if dict_j['jac'].min() < 0:
                print('NEGATIVE JACOBIAN')

    if funcnames is not None and len(set(funcnames).intersection(set(('dr', 'rdth', 'rdph')))) != 0:
        #d2thdxp31, d2thdxp32, d2thdxp33 = np.gradient(dthdxp3, dxp1, dxp2, dxp3)
        dr,junk,junk = np.gradient(rout,2); junk,rdth,junk = np.gradient(thout,2); junk,junk,rdph = np.gradient(phiout,2)
        rdth *= rout; rdph *= rout
        # for index in xrange(0,np.shape(rdth)[1]):
        #     rdth[:,index,:] *= rout[:,index,:]
        # for index in xrange(0,np.shape(rdph)[2]):
        #     rdph[:,:,index] *= rout[:,:,index]
        dict_j.update({'dr':dr[:,jout,:],
                       'rdth':rdth[:,jout,:], 
                       'rdph':rdph[:,jout,:]})
        dict_k.update({'dr':dr[:,:,kout],
                       'rdth':rdth[:,:,kout], 
                       'rdph':rdph[:,:,kout]})

    #Test the Jacobian
    if funcnames is None:
        print('Jacobian only implemented for x-y plane')
        drdxp1, drdxp2, drdxp3 = np.gradient(rout, dxp1, dxp2, dxp3)
        dphidxp1, dphidxp2, dphidxp3 = np.gradient(phiout, dxp1, dxp2, dxp3)
        dthdxp1, dthdxp2, dthdxp3 = np.gradient(thout, dxp1, dxp2, dxp3)
        jac = drdxp1*dphidxp3 - drdxp3*dphidxp1
        jac_jslice = jac[:,jout,:]
        print('Jacobian minimum is ', jac_jslice.min())
        if jac_jslice.min() < 0:
            print('NEGATIVE JACOBIAN')
    # we set the physical (X, Y, Z) coordinates, function of the xpi coordinates

    phi0 = 0.
    xnew = rout*np.cos(phiout-phi0)*np.sin(thout)
    ynew = rout*np.sin(phiout-phi0)*np.sin(thout)
    znew = rout*np.cos(thout)

    xnew_jslice = xnew[:,jout,:]
    ynew_jslice = ynew[:,jout,:]
    znew_jslice = znew[:,jout,:]

    xnew_kslice = xnew[:,:,kout]
    ynew_kslice = ynew[:,:,kout]
    znew_kslice = znew[:,:,kout]

    if xminj is None:
        xminj = np.min(xnew_jslice)
    if xmaxj is None:
        xmaxj = np.max(xnew_jslice)
    if yminj is None:
        yminj = np.min(ynew_jslice)
    if ymaxj is None:
        ymaxj = np.max(ynew_jslice)

    if xmink is None:
        xmink = np.min(xnew_kslice)
    if xmaxk is None:
        xmaxk = np.max(xnew_kslice)
    if zmink is None:
        zmink = np.min(znew_kslice)
    if zmaxk is None:
        zmaxk = np.max(znew_kslice)

    print('theta = ', thout[0,jout,0], ' zslice = ', znew[0,jout,0])
    print('phi   = ', phiout[0,0,kout], ' xslice = ', xnew[0,0,kout])


    # we plot the grid itself

    if(plot_grid):
        fig_gridj = plt.figure(nwin)
        fig_gridj.clf()
        ax = fig_gridj.add_subplot(111, aspect='equal') 

        if plottitlej is None:
            title = 'r-phi' + '  t =  ' + str('%0.1f' %time)
        else:
            title = plottitlej
        ax.set_title('%s' %title)

        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.axis([xminj,xmaxj,yminj,ymaxj])

        for i in range(nxp3)[::nperplot]:
            plt.plot(xnew_jslice[:,i],ynew_jslice[:,i], color=grid_colour)
        for i in range(nxp1)[::nperplot]:
            plt.plot(xnew_jslice[i,:],ynew_jslice[i,:], color=grid_colour)

        # we plot a disk
        if(plot_disk):
            th_range = np.linspace(0,2*np.pi,256)
#            r_disk   = 30
            xx_disk  = r_disk*np.cos(th_range)
            yy_disk  = r_disk*np.sin(th_range)
            plt.plot(xx_disk, yy_disk, '--', color='blue')
            mini1 = plt.Circle((xbh1_phys,ybh1_phys),9.,color='red',fill=False)
            fig_gridj.gca().add_artist(mini1)
            mini2 = plt.Circle((xbh2_phys,ybh2_phys),9.,color='red',fill=False)
            fig_gridj.gca().add_artist(mini2)
            #excise_in = plt.Circle((0,0),8.,color='green',fill=False)
            #excise_out = plt.Circle((0,0),12.,color='green',fill=False)
            #fig_gridj.gca().add_artist(excise_in)
            #fig_gridj.gca().add_artist(excise_out)
            #lightgreen  : #90EE90
            #brightyellow: #ffff00

        # we plot the BHs
        if(plot_bhs):

            # r12    = 20.

            # xbh1   = 0.5*r12
            # ybh1   = 0.
            # xbh2   = -0.5*r12
            # ybh2   = 0.

            #rhor = 0.5
            rhor1 = mbh1
            rhor2 = mbh2
            green_led = '#5DFC0A'
            bh1 = plt.Circle((xbh1_phys,ybh1_phys), rhor1, color=green_led)
            fig_gridj.gca().add_artist(bh1)
            bh2 = plt.Circle((xbh2_phys,ybh2_phys), rhor2, color=green_led)
            fig_gridj.gca().add_artist(bh2)

            #dennis attempting to add cells within horizon
            zbh1_phys = zbh2_phys = 0.
            dist1_jslice = np.sqrt( (xnew_jslice - xbh1_phys)*(xnew_jslice - xbh1_phys) + (ynew_jslice - ybh1_phys)*(ynew_jslice - ybh1_phys) + (znew_jslice - zbh1_phys)*(znew_jslice - zbh1_phys) )
            dist2_jslice = np.sqrt( (xnew_jslice - xbh2_phys)*(xnew_jslice - xbh2_phys) + (ynew_jslice - ybh2_phys)*(ynew_jslice - ybh2_phys) + (znew_jslice - zbh2_phys)*(znew_jslice - zbh2_phys) )

            plt.plot(xnew_jslice[dist1_jslice <= rhor1],ynew_jslice[dist1_jslice <= rhor1],marker='o',color='blue',linestyle='',markersize=5)
            plt.plot(xnew_jslice[dist2_jslice <= rhor2],ynew_jslice[dist2_jslice <= rhor2],marker='o',color='blue',linestyle='',markersize=5)

        fig_gridk = plt.figure(nwin+1000)
        fig_gridk.clf()
        ax = fig_gridk.add_subplot(111, aspect='equal') 

        if plottitlek is None:
            title = 'r-theta' + '  t =  ' + str('%0.1f' %time)
        else:
            title = plottitlek
        ax.set_title('%s' %title)
        ax.set_xlabel('X')
        ax.set_ylabel('Z')
        ax.axis([xmink,xmaxk,zmink,zmaxk])

        for i in range(nxp2)[::nperplot]:
            plt.plot(xnew_kslice[:,i],znew_kslice[:,i], color=grid_colour)
        for i in range(nxp1)[::nperplot]:
            plt.plot(xnew_kslice[i,:],znew_kslice[i,:], color=grid_colour)


        if(savepng):
            if outfilename is None:
                filenamek = '%s_r-theta_%s_t%06d.png' %(dirout+'/'+'grid', namesuffix, time )      
                filenamej = '%s_r-ph_%s_t%06d.png' %(dirout+'/'+'grid', namesuffix, time )
            else:
                filenamek = dirout + '/' + outfilename + '_r-th.png'
                filenamej = dirout + '/' + outfilename + '_r-ph.png'
            fig_gridk.savefig(filenamek)
            fig_gridj.savefig(filenamej)
        elif(savepdf):
            if outfilename is None:
                filenamek = '%s_r-theta_%s_t%06d.pdf' %(dirout+'/'+'grid', namesuffix, time )      
                filenamej = '%s_r-ph_%s_t%06d.pdf' %(dirout+'/'+'grid', namesuffix, time )
            else:
                filenamek = dirout + '/' + outfilename + '_r-th.pdf'
                filenamej = dirout + '/' + outfilename + '_r-ph.pdf'
            fig_gridk.savefig(filenamek)
            fig_gridj.savefig(filenamej)


    # we plot the jslice (r-phi plane)
    if funcnames is not None:
        for funcname in funcnames:

            rnew_jslice = np.sqrt(xnew_jslice*xnew_jslice + ynew_jslice*ynew_jslice)
            rmaxj       = np.sqrt(xmaxj*xmaxj + ymaxj*ymaxj)
            func_jslice = dict_j[funcname]
            
            if funcname is 'dr':
                min_value = 0.0054
                max_value = 0.1335
            elif funcname is 'rdph':
                min_value = 0.0052
                max_value = 0.2078
            else:
                min_value = np.min(func_jslice[rnew_jslice <= rmaxj])
                max_value = np.max(func_jslice[rnew_jslice <= rmaxj])
            if funcname == 'jac' and np.min(func_jslice) < 0:
                max_value = 0

            levels  = np.linspace(min_value,max_value,nlev)

            fig = plt.figure()
            ax  = fig.add_subplot(111, aspect='equal') 
            ax.set_title('%s' %funcname)
            ax.axis([xminj,xmaxj,yminj,ymaxj])
            ax.set_xlabel('X')
            ax.set_ylabel('Y')

            CS = ax.contourf(xnew_jslice,ynew_jslice,func_jslice,levels,cmap=cmap)
            rticks  = np.linspace(min_value,max_value,nticks)
            fig.colorbar(CS,ticks=rticks) 

            if(plot_disk):
                mini1 = plt.Circle((xbh1_phys,ybh1_phys),9.,color='black',fill=False)
                ax.add_artist(mini1)
                mini2 = plt.Circle((xbh2_phys,ybh2_phys),9.,color='black',fill=False)
                ax.add_artist(mini2)
            

            if(savepng):
                fig.savefig('%s_r-ph.png' %(dirout+'/'+funcname))


            # we plot the kslice (r-theta plane)

        for funcname in funcnames:
    
            func_kslice = dict_k[funcname]

            min_value = np.min(func_kslice)
            max_value = np.max(func_kslice)

            levels  = np.linspace(min_value,max_value,nlev)
            
            fig = plt.figure()
            ax  = fig.add_subplot(111, aspect='equal') 
            ax.set_title('%s' %funcname)
            ax.axis([xmink,xmaxk,zmink,zmaxk])
            ax.set_xlabel('X')
            ax.set_ylabel('Z')

            CS = ax.contourf(xnew_kslice,znew_kslice,func_kslice,levels,cmap=cmap)
            rticks  = np.linspace(min_value,max_value,nticks)
            fig.colorbar(CS,ticks=rticks) 

            if(savepng):
                fig.savefig('%s_r-th.png' %(dirout+'/'+funcname))

    if(show_plot):
        plt.show()


    if(0):
        h5filename='KDHARM0.gdump.h5'
        dirname='/home/mzilhao/RIT/BBHDisk/runs/coord_testing'

        funcs=['dxdxp11', 'dxdxp12', 'dxdxp13']

        for funcname in funcs:
            polp(h5filename=h5filename, dirname=dirname, funcname=funcname, 
                 # interpol=False, savepng=True)
                 interpol=False, savepng=False)
