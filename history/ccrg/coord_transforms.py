from __future__ import division
from __future__ import print_function


import numpy as np
from scipy.interpolate import interp1d

#Jacobians copied straight from Harm C-code and pow->np.power
################################################################################
#          S T A R T    J A C O B I A N    C A L C U L A T I O N S             #
################################################################################
def get_dxc_dxs( r, th, ph ):
    #initialize all elements as numpy arrays so constant values are correctly kept
    dxc_dxs_00 = np.zeros((np.shape(r)))
    dxc_dxs_01 = np.zeros((np.shape(r)))
    dxc_dxs_02 = np.zeros((np.shape(r)))
    dxc_dxs_03 = np.zeros((np.shape(r)))
    dxc_dxs_10 = np.zeros((np.shape(r)))
    dxc_dxs_11 = np.zeros((np.shape(r)))
    dxc_dxs_12 = np.zeros((np.shape(r)))
    dxc_dxs_13 = np.zeros((np.shape(r)))
    dxc_dxs_20 = np.zeros((np.shape(r)))
    dxc_dxs_21 = np.zeros((np.shape(r)))
    dxc_dxs_22 = np.zeros((np.shape(r)))
    dxc_dxs_23 = np.zeros((np.shape(r)))
    dxc_dxs_30 = np.zeros((np.shape(r)))
    dxc_dxs_31 = np.zeros((np.shape(r)))
    dxc_dxs_32 = np.zeros((np.shape(r)))
    dxc_dxs_33 = np.zeros((np.shape(r)))

    sth = np.sin(th)
    cth = np.cos(th)
    sph = np.sin(ph)
    cph = np.cos(ph)

    x = r * sth * cph;
    y = r * sth * sph;

    dxc_dxs_00 = 1.                    #;  // dt/dt
    dxc_dxs_11 =      cph * sth        #;  // dx/dr
    dxc_dxs_12 =  r * cph * cth        #;  // dx/dtheta
    dxc_dxs_13 = -y                    #;  // dx/dphi
    dxc_dxs_21 =      sph * sth        #;  // dy/dr
    dxc_dxs_22 =  r * sph * cth        #;  // dy/dtheta
    dxc_dxs_23 = x                     #;  // dy/dphi
    dxc_dxs_31 = cth                   #;  // dz/dr
    dxc_dxs_32 = -r*sth                #;  // dz/dtheta
    #print(r)
    dxc_dxs = { '00':dxc_dxs_00, '01':dxc_dxs_01, '02':dxc_dxs_02, '03':dxc_dxs_03,
                '10':dxc_dxs_10, '11':dxc_dxs_11, '12':dxc_dxs_12, '13':dxc_dxs_13,
                '20':dxc_dxs_20, '21':dxc_dxs_21, '22':dxc_dxs_22, '23':dxc_dxs_23,
                '30':dxc_dxs_30, '31':dxc_dxs_31, '32':dxc_dxs_32, '33':dxc_dxs_33 }
    return dxc_dxs


def get_dxs_dxc( x, y, z ):
    rho_sq     = x*x + y*y
    rho        = np.sqrt(rho_sq)
    rho_inv    = 1./rho
    rho_inv_sq = rho_inv * rho_inv
    r_sq       = rho_sq + z*z
    r          = np.sqrt(r_sq)
    r_inv      = 1./r
    r_inv_sq   = r_inv * r_inv

    th =  np.arccos( z * r_inv )
    ph = np.arctan2( y , x )
    ph[ ph < 0. ] += 2. * np.pi


    dth_term = z * r_inv_sq * rho_inv

    #initialize all elements as numpy arrays so constant values are correctly kept
    dxs_dxc_00 = np.zeros((np.shape(x)))
    dxs_dxc_01 = np.zeros((np.shape(x)))
    dxs_dxc_02 = np.zeros((np.shape(x)))
    dxs_dxc_03 = np.zeros((np.shape(x)))
    dxs_dxc_10 = np.zeros((np.shape(x)))
    dxs_dxc_11 = np.zeros((np.shape(x)))
    dxs_dxc_12 = np.zeros((np.shape(x)))
    dxs_dxc_13 = np.zeros((np.shape(x)))
    dxs_dxc_20 = np.zeros((np.shape(x)))
    dxs_dxc_21 = np.zeros((np.shape(x)))
    dxs_dxc_22 = np.zeros((np.shape(x)))
    dxs_dxc_23 = np.zeros((np.shape(x)))
    dxs_dxc_30 = np.zeros((np.shape(x)))
    dxs_dxc_31 = np.zeros((np.shape(x)))
    dxs_dxc_32 = np.zeros((np.shape(x)))
    dxs_dxc_33 = np.zeros((np.shape(x)))

    #Note time-dependence is implicitly left to the  x(xp) transformation, so all time derivatives here are zero:
    dxs_dxc_00 = 1.               #;  // dt/dt
    dxs_dxc_01 = 0.               #;  // dt/dx
    dxs_dxc_02 = 0.               #;  // dt/dy
    dxs_dxc_03 = 0.               #;  // dt/dz
    dxs_dxc_10 = 0.               #;  // dr/dt
    dxs_dxc_11 = x * r_inv        #;  // dr/dx
    dxs_dxc_12 = y * r_inv        #;  // dr/dy
    dxs_dxc_13 = z * r_inv        #;  // dr    /dz
    dxs_dxc_20 = 0.               #;  // dth/dt
    dxs_dxc_21 = x * dth_term     #;  // dtheta/dx
    dxs_dxc_22 = y * dth_term     #;  // dtheta/dy
    dxs_dxc_23 = -rho * r_inv_sq  #;  // dtheta/dz
    dxs_dxc_30 = 0.               #;  // dphi/dt
    dxs_dxc_31 = -y*rho_inv_sq    #;  // dphi/dx
    dxs_dxc_32 =  x*rho_inv_sq    #;  // dphi/dy
    dxs_dxc_33 = 0.               #;  // dphi/dz

    dxs_dxc = { '00':dxs_dxc_00, '01':dxs_dxc_01, '02':dxs_dxc_02, '03':dxs_dxc_03,
                '10':dxs_dxc_10, '11':dxs_dxc_11, '12':dxs_dxc_12, '13':dxs_dxc_13,
                '20':dxs_dxc_20, '21':dxs_dxc_21, '22':dxs_dxc_22, '23':dxs_dxc_23,
                '30':dxs_dxc_30, '31':dxs_dxc_31, '32':dxs_dxc_32, '33':dxs_dxc_33}
    return dxs_dxc


def get_dxBL_dxIZ( Mass, Spin, TIZ, XIZ, YIZ, ZIZ ):
    #Calculate dxBL_dxIZ as a function of known IZ coordinates
    cg  = XIZ
    cg1 = YIZ
    cg3 = ZIZ

    #initialize elements as arrays so const value are handled correctly
    dxBL_dxIZ_00 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_01 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_02 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_03 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_10 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_11 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_12 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_13 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_20 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_21 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_22 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_23 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_30 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_31 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_32 = np.zeros((np.shape(XIZ)))
    dxBL_dxIZ_33 = np.zeros((np.shape(XIZ)))
    t1 = np.zeros((np.shape(XIZ)))
    t2 = np.zeros((np.shape(XIZ)))
    t3 = np.zeros((np.shape(XIZ)))
    t4 = np.zeros((np.shape(XIZ)))
    t5 = np.zeros((np.shape(XIZ)))
    t6 = np.zeros((np.shape(XIZ)))
    t7 = np.zeros((np.shape(XIZ)))
    t8 = np.zeros((np.shape(XIZ)))
    t9 = np.zeros((np.shape(XIZ)))
    t10 = np.zeros((np.shape(XIZ)))
    t11 = np.zeros((np.shape(XIZ)))
    t12 = np.zeros((np.shape(XIZ)))
    t13 = np.zeros((np.shape(XIZ)))
    t14 = np.zeros((np.shape(XIZ)))
    t15 = np.zeros((np.shape(XIZ)))
    t16 = np.zeros((np.shape(XIZ)))
    t17 = np.zeros((np.shape(XIZ)))
    t18 = np.zeros((np.shape(XIZ)))
    t19 = np.zeros((np.shape(XIZ)))
    t20 = np.zeros((np.shape(XIZ)))
    t21 = np.zeros((np.shape(XIZ)))
    t22 = np.zeros((np.shape(XIZ)))
    t23 = np.zeros((np.shape(XIZ)))
    t24 = np.zeros((np.shape(XIZ)))
    t25 = np.zeros((np.shape(XIZ)))
    #print("get_dxBL_dxIZ shapes 1", np.shape(dxBL_dxIZ_00), np.shape(dxBL_dxIZ_11))
    t1 = Spin * Spin
    t2 = Mass * Mass - t1
    t3 = np.power(t2, -0.1e1 / 0.2e1)
    t2 = t2 * t3
    t4 = Mass + t2
    t5 = np.sqrt(0.2e1)
    t6 = np.power(cg3, 0.2e1)
    t7 = np.power(cg, 0.2e1)
    t8 = np.power(cg1, 0.2e1)
    t9 = np.power(t7, 0.2e1) + np.power(t1, 0.2e1) + np.power(t6, 0.2e1) + np.power(t8, 0.2e1) + 0.2e1 * (t7 - t1) * t8 - 0.2e1 * t7 * t1 + 0.2e1 * (t7 + t1 + t8) * t6
    t10 = np.power(t9, -0.1e1 / 0.2e1)
    t9 = t7 + t8 + t6 - t1 + t9 * t10
    t11 = np.power(t9, -0.3e1 / 0.2e1)
    t12 = t9 * t11
    t13 = t7 + t8 + t6 - t1
    t14 = 0.1e1 / 0.2e1
    t10 = t14 * t10
    t15 = 0.2e1 * cg + 0.4e1 * t10 * cg * t13
    t16 = t14 * t5
    t17 = t16 * np.power(t9, 0.2e1) * t11
    t18 = t17 + t2
    t2 = t17 - t2
    t17 = 0.1e1 / t18
    t19 = 0.1e1 - t17 * t2
    t20 = t5 * t12 / 0.4e1
    t21 = t20 * t15
    t22 = t21 * t17 * t19
    t13 = 0.2e1 * cg1 + 0.4e1 * t10 * cg1 * t13
    t23 = t20 * t13
    t24 = t23 * t17 * t19
    t7 = 0.2e1 * cg3 + 0.4e1 * t10 * cg3 * (t7 + t8 + t6 + t1)
    t10 = t20 * t7
    t17 = t10 * t17 * t19
    t2 = 0.1e1 / t2
    t4 = t14 * (np.power(t4, 0.2e1) + t1) * t3
    t9 = 0.1e1 / t9
    t6 = 0.1e1 - 0.2e1 * t6 * t9
    t6 = np.power(t6, -0.1e1 / 0.2e1)
    t16 = t16 * cg3 * t11
    t19 = 0.1e1 / cg
    t20 = np.power(t19, 0.2e1)
    t8 = 0.1e1 + t8 * t20
    t1 = 0.1e1 + 0.2e1 * t1 * t9
    t8 = 0.1e1 / t8
    t1 = 0.1e1 / t1
    t9 = t5 * t11
    t25 = t14 * Spin
    dxBL_dxIZ_00 = 1.
    dxBL_dxIZ_01 = -t4 * t22 * t2 * t18
    dxBL_dxIZ_02 = -t4 * t24 * t2 * t18
    dxBL_dxIZ_03 = -t4 * t17 * t2 * t18
    dxBL_dxIZ_10 = 0.
    dxBL_dxIZ_11 = t21
    dxBL_dxIZ_12 = t23
    dxBL_dxIZ_13 = t10
    dxBL_dxIZ_20 = 0.
    dxBL_dxIZ_21 = t16 * t15 * t6
    dxBL_dxIZ_22 = t16 * t13 * t6
    dxBL_dxIZ_23 = -t5 * (t12 - t14 * cg3 * t11 * t7) * t6
    dxBL_dxIZ_30 = 0.
    dxBL_dxIZ_31 = t25 * (t9 * t15 * t1 - t3 * t22 * t2 * t18) - cg1 * t20 * t8
    dxBL_dxIZ_32 = t25 * (t9 * t13 * t1 - t3 * t24 * t2 * t18) + t19 * t8
    dxBL_dxIZ_33 = t25 * (t9 * t7 * t1 - t3 * t17 * t2 * t18)

    #print("get_dxBL_dxIZ shapes 2", np.shape(dxBL_dxIZ_00), np.shape(dxBL_dxIZ_11))
    dxBL_dxIZ = {'00':dxBL_dxIZ_00, '01':dxBL_dxIZ_01, '02':dxBL_dxIZ_02, '03':dxBL_dxIZ_03,
                 '10':dxBL_dxIZ_10, '11':dxBL_dxIZ_11, '12':dxBL_dxIZ_12, '13':dxBL_dxIZ_13,
                 '20':dxBL_dxIZ_20, '21':dxBL_dxIZ_21, '22':dxBL_dxIZ_22, '23':dxBL_dxIZ_23,
                 '30':dxBL_dxIZ_30, '31':dxBL_dxIZ_31, '32':dxBL_dxIZ_32, '33':dxBL_dxIZ_33 }

    return dxBL_dxIZ

def get_dxIZ_dxNZ( trajfile, BH, TNZ, XNZ, YNZ, ZNZ, SETPHASE=None, spinning=False ):

    #Initialize elements as arrays for const value ones
    dxIZ_dxNZ_00 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_01 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_02 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_03 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_10 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_11 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_12 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_13 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_20 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_21 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_22 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_23 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_30 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_31 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_32 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_33 = np.zeros((np.shape(XNZ)))

    if spinning:
        traj_columns = (0, 7, 8, 24, 25, 26)
    else:
        traj_columns = (0, 1, 2, 3, 23, 24)
    tbh_traj, m_bh1_traj, m_bh2_traj, b_traj, phi_traj, omega_traj = np.loadtxt(trajfile, usecols=traj_columns, unpack=True)

    fm_bh1 = interp1d(tbh_traj, m_bh1_traj, kind='linear')
    fm_bh2 = interp1d(tbh_traj, m_bh2_traj, kind='linear')
    fb     = interp1d(tbh_traj, b_traj    , kind='linear')
    fphi   = interp1d(tbh_traj, phi_traj  , kind='linear')
    fomega = interp1d(tbh_traj, omega_traj, kind='linear')

    #get r12dot
    tbh_traj_even = np.linspace(np.min(tbh_traj),np.max(tbh_traj),len(tbh_traj)) #resample time evenly
    fb_even = fb(tbh_traj_even) #get b(evenly sampled time)
    fr12dot = np.gradient(fb_even, tbh_traj_even[1] - tbh_traj_even[0]) #calculate derivative
    fr12dot = interp1d(tbh_traj_even, fr12dot, kind='linear')

    m_bh1  = fm_bh1(TNZ)
    m_bh2  = fm_bh2(TNZ)
    b      = fb(TNZ)
    phi    = fphi(TNZ)
    omega  = fomega(TNZ)
    r12dot = fr12dot(TNZ)

    #print(m_bh1,m_bh2,phi,omega,b,r12dot)
    #print(TNZ)
    if SETPHASE is not None:
        phi = SETPHASE

    if( BH == 1 or BH is 'primary'):
        t = TNZ; x = XNZ
        y = YNZ; z = ZNZ

        #Set BH Traj Values
        m1    = m_bh1
        m2    = m_bh2

    elif( BH == 2 ):
        t = TNZ;  x = -XNZ
        y = -YNZ; z = ZNZ

        #Set BH Traj Values
        m1    = m_bh2
        m2    = m_bh1

    M = m1 + m2
    #Maple generated code
    t1 = b * b;
    t2 = t1 * b;
    t3 = 0.1e1 / t2;
    t4 = m2 * t3;
    t5 = m2 * b;
    t6 = 0.1e1 / M;
    t7 = np.cos(phi);
    t8 = t7 * t7;
    t11 = np.sin(phi);
    t12 = t11 * t11;
    t13 = t6 * t12;
    t16 = np.power(-t5 * t6 * t8 - t5 * t13, 0.2e1);
    t17 = m2 * m2;
    t18 = t17 * b;
    t19 = M * M;
    t20 = 0.1e1 / t19;
    t23 = t18 * t20 * t12 * r12dot;
    t24 = t20 * r12dot;
    t25 = t24 * t8;
    t41 = t17 * m2;
    t42 = t41 * t20;
    t43 = t42 * t12;
    t46 = 0.1e1 / t1;
    t47 = t46 * r12dot;
    t57 = t1 * t1;
    t59 = m2 / t57;
    t60 = t6 * t7;
    t63 = np.power(x - t5 * t60, 0.2e1);
    t64 = t6 * t11;
    t67 = np.power(y - t5 * t64, 0.2e1);
    t68 = z * z;
    t69 = t63 + t67 + t68;
    t73 = np.sqrt(t69);
    t78 = 0.1e1 / t73;
    t80 = t41 * t1;
    t82 = 0.1e1 / t19 / M;
    t83 = t12 * t12;
    t85 = t82 * t83 * t11;
    t89 = t41 * t2;
    t90 = t82 * t83;
    t94 = t12 * t11;
    t95 = t82 * t94;
    t99 = t82 * t12;
    t103 = t82 * r12dot;
    t122 = t17 * t6;
    t125 = -t122 - 0.4e1 * t122 * t12;
    t129 = m2 * t46;
    t130 = m2 * t6;
    t134 = -0.3e1 * t130 + 0.6e1 * t130 * t12;
    t138 = t17 * t3;
    t139 = r12dot * t6;
    t143 = t17 * t46;
    t145 = t6 * omega * t7;
    t159 = t11 * omega;
    t161 = m2 / b;
    t164 = np.sqrt(t161);
    t165 = 0.1e1 / t164;
    t167 = np.sqrt(t130);
    t169 = t20 * t83;
    t173 = t17 * t1;
    t177 = 0.6e1 * t173 * t20 * t94 * omega;
    t178 = 0.9e1 * t23;
    t180 = t159 * t7;
    t200 = m2 * r12dot;
    t201 = t200 * t64;
    t202 = t5 * t145;
    t250 = t64 * omega;
    t267 = t7 * omega;
    t277 = b * t20 * r12dot;
    dxIZ_dxNZ_00 = (-t4 * t16 * (0.2e1 * t23 + 0.2e1 * t18 * t25) / 0.2e1 + (0.1e1 + 0.5e1 / 0.128e3 * (0.2e1 * m1 + 0.3e1 * m2) * t20 / m1 * t1 * r12dot + ((t42 + t43) * t12 * t47 + ((t42 + 0.2e1 * t43) * t46 * r12dot + t41 * t46 * t25) * t8 - t59 * r12dot * t69) * t73) * t73) * t78 + ((t4 * (0.9e1 * t80 * t85 * r12dot + (0.3e1 * t89 * t90 * omega + (0.18e2 * t80 * t95 * r12dot + (0.6e1 * t89 * t99 * omega + (0.9e1 * t80 * t103 * t11 + 0.3e1 * t89 * t82 * omega * t7) * t7) * t7) * t7) * t7) / 0.3e1 + (t125 * t11 * t3 * r12dot + (t129 * t134 * omega / 0.3e1 + (-0.4e1 * t138 * t139 * t11 + 0.2e1 * t143 * t145) * t7) * t7) * t69) * t78 + (t7 * m2 * t47 / 0.2e1 + t159 * t161) * t165 * t167 + ((t4 * (-0.9e1 * t18 * t169 * r12dot + (-t177 + (-t178 - 0.6e1 * t173 * t20 * t180) * t7) * t7) / 0.3e1 + (-0.2e1 * t4 * t180 + 0.3e1 * t59 * t12 * r12dot) * t69) * t78 - t4 * t12 * t78 * (-0.2e1 * t201 - 0.2e1 * t202) * y / 0.2e1) * y) * y + ((t4 * (-0.3e1 * t89 * t85 * omega + (0.9e1 * t80 * t90 * r12dot + (-0.6e1 * t89 * t95 * omega + (0.18e2 * t80 * t99 * r12dot + (-0.3e1 * t89 * t82 * t11 * omega + 0.9e1 * t80 * t103 * t7) * t7) * t7) * t7) * t7) / 0.3e1 + (-t129 * t134 * t11 * omega / 0.3e1 + (t125 * t3 * r12dot + (-0.2e1 * t143 * t250 - 0.4e1 * t138 * t139 * t7) * t7) * t7) * t69) * t78 + (-t11 * m2 * t47 / 0.2e1 + t267 * t161) * t165 * t167 + ((t4 * (0.6e1 * t173 * t169 * omega + (-0.18e2 * t94 * t17 * t277 + (-0.18e2 * t11 * t17 * t277 - 0.6e1 * t173 * t20 * omega * t7) * t8) * t7) / 0.3e1 + (0.2e1 * t4 * t12 * omega + (0.6e1 * t59 * t11 * r12dot - 0.2e1 * t4 * t267) * t7) * t69) * t78 + t4 * (-0.3e1 * t5 * t6 * t94 * omega + (0.9e1 * t200 * t13 + 0.6e1 * t5 * t6 * t180) * t7) * t78 * y / 0.3e1) * y + ((t4 * (t177 + (-t178 + (0.6e1 * t173 * t20 * t11 * omega - 0.9e1 * t18 * t24 * t7) * t7) * t7) * t7 / 0.3e1 + (0.2e1 * t4 * t159 + 0.3e1 * t59 * r12dot * t7) * t7 * t69) * t78 + t4 * (-0.6e1 * t5 * t13 * omega + (0.9e1 * t201 + 0.3e1 * t202) * t7) * t7 * t78 * y / 0.3e1 - t4 * t8 * t78 * (-0.2e1 * t200 * t60 + 0.2e1 * t5 * t250) * x / 0.2e1) * x) * x;

    t1 = b * b;
    t4 = m2 / t1 / b;
    t5 = m2 * b;
    t6 = 0.1e1 / M;
    t7 = np.cos(phi);
    t8 = t7 * t7;
    t10 = t5 * t6 * t8;
    t11 = np.sin(phi);
    t12 = t11 * t11;
    t14 = t5 * t6 * t12;
    t15 = -t10 - t14;
    t16 = t15 * t15;
    t19 = b * t6 * t7;
    t22 = m2 * t6;
    t34 = np.power(x - t5 * t6 * t7, 0.2e1);
    t38 = np.power(y - t5 * t6 * t11, 0.2e1);
    t39 = z * z;
    t40 = t34 + t38 + t39;
    t43 = np.sqrt(t40);
    t44 = 0.1e1 / t43;
    t50 = np.sqrt(m2 / b);
    t51 = np.sqrt(t22);
    t57 = t11 * t7;
    t64 = m2 * m2;
    t74 = t64 * t1;
    t75 = M * M;
    t76 = 0.1e1 / t75;
    t77 = t12 * t12;
    dxIZ_dxNZ_01 = t4 * (0.3e1 * t16 * m2 * t19 + ((-0.3e1 * t22 + 0.6e1 * t22 * t12) * b + 0.6e1 * t10) * t7 * t40) * t44 / 0.3e1 + t50 * t51 * t11 + (t4 * (0.6e1 * t15 * t11 * m2 * t19 - 0.6e1 * t57 * t40) * t44 / 0.3e1 + t64 / t1 * t12 * t44 * t6 * t7 * y) * y + (t4 * ((-0.3e1 * t74 * t76 * t77 + (-0.12e2 * t74 * t76 * t12 - 0.9e1 * t74 * t76 * t8) * t8 + (0.3e1 - 0.6e1 * t8) * t40) * t44 + ((0.6e1 * t5 * t6 * t12 * t11 + 0.12e2 * t11 * t8 * t5 * t6) * t44 - 0.3e1 * t12 * t44 * y) * y) / 0.3e1 + (t4 * ((0.6e1 * t14 + 0.9e1 * t10) * t7 * t44 - 0.6e1 * t57 * t44 * y) / 0.3e1 - t4 * t8 * t44 * x) * x) * x;

    t1 = b * b;
    t4 = m2 / t1 / b;
    t5 = m2 * b;
    t6 = 0.1e1 / M;
    t7 = np.cos(phi);
    t8 = t7 * t7;
    t10 = t5 * t6 * t8;
    t11 = np.sin(phi);
    t12 = t11 * t11;
    t14 = t5 * t6 * t12;
    t15 = -t10 - t14;
    t16 = t15 * t15;
    t18 = b * t6;
    t22 = m2 * t6;
    t32 = 0.6e1 * t11 * t8 * t5 * t6;
    t37 = np.power(x - t5 * t6 * t7, 0.2e1);
    t40 = y - t5 * t6 * t11;
    t41 = t40 * t40;
    t42 = z * z;
    t43 = t37 + t41 + t42;
    t46 = np.sqrt(t43);
    t47 = 0.1e1 / t46;
    t53 = np.sqrt(m2 / b);
    t54 = np.sqrt(t22);
    t57 = m2 * m2;
    t58 = t57 * t1;
    t59 = M * M;
    t60 = 0.1e1 / t59;
    t61 = t12 * t12;
    t99 = t11 * t7;
    dxIZ_dxNZ_02 = t4 * (0.3e1 * t16 * m2 * t18 * t11 + ((-0.3e1 * t22 + 0.6e1 * t22 * t12) * t11 * b + t32) * t43) * t47 / 0.3e1 - t53 * t54 * t7 + (t4 * (-0.9e1 * t58 * t60 * t61 + (-0.12e2 * t58 * t60 * t12 - 0.3e1 * t58 * t60 * t8) * t8 + (0.3e1 - 0.6e1 * t12) * t43) * t47 / 0.3e1 + (t4 * (0.9e1 * t5 * t6 * t12 * t11 + t32) * t47 / 0.3e1 - t4 * t12 * t47 * y) * y) * y + (t4 * ((0.6e1 * t15 * t11 * m2 * t18 * t7 - 0.6e1 * t99 * t43) * t47 + ((0.12e2 * t14 + 0.6e1 * t10) * t7 * t47 - 0.6e1 * t99 * t47 * y) * y) / 0.3e1 - t4 * t8 * t47 * t40 * x) * x;

    t1 = b * b;
    t5 = m2 * b;
    t6 = 0.1e1 / M;
    t7 = np.cos(phi);
    t8 = t7 * t7;
    t10 = t5 * t6 * t8;
    t11 = np.sin(phi);
    t12 = t11 * t11;
    t14 = t5 * t6 * t12;
    t15 = -t10 - t14;
    t16 = t15 * t15;
    t20 = np.power(x - t5 * t6 * t7, 0.2e1);
    t24 = np.power(y - t5 * t6 * t11, 0.2e1);
    t25 = z * z;
    t29 = np.sqrt(t20 + t24 + t25);
    t30 = 0.1e1 / t29;
    t34 = t30 * z;
    dxIZ_dxNZ_03 = m2 / t1 / b * ((-0.3e1 * t16 + 0.3e1 * t20 + 0.3e1 * t24 + 0.3e1 * t25) * t30 * z + (-0.6e1 * t15 * t11 * t34 - 0.3e1 * t12 * t30 * z * y) * y + (-0.6e1 * (-t10 - t14 + t11 * y) * t7 * t34 - 0.3e1 * t8 * t30 * z * x) * x) / 0.3e1;

    t1 = 0.1e1 / M;
    t2 = m2 * t1;
    t3 = m2 * m2;
    t4 = M * M;
    t5 = 0.1e1 / t4;
    t7 = np.sin(phi);
    t8 = t7 * t7;
    t9 = t3 * t5 * t8;
    t15 = t1 * t7;
    t21 = t3 * m2;
    t23 = t7 * omega;
    t24 = np.cos(phi);
    t25 = t23 * t24;
    t30 = b * b;
    t31 = 0.1e1 / t30;
    t32 = m2 * t31;
    t33 = t2 * t8;
    t34 = -0.1e1 / 0.2e1 - t33;
    t39 = t3 * t1 * t8;
    t41 = -m2 - 0.2e1 * t39;
    t43 = 0.1e1 / t30 / b;
    t46 = t3 * t31;
    t48 = t46 * t15 * omega;
    t53 = 0.2e1 * t3 * t43 * t1 * r12dot * t24;
    t59 = z * z;
    t61 = 0.1e1 / b;
    t62 = m2 * t61;
    t64 = t2 / 0.2e1 + t9;
    t80 = t21 * t61;
    t90 = m2 * t43;
    t92 = t90 * t8 * omega;
    t93 = t30 * t30;
    t95 = m2 / t93;
    t101 = 0.3e1 * t95 * t7 * r12dot - t90 * omega * t24;
    t103 = t92 + t101 * t24;
    t110 = 0.4e1 * t39;
    t144 = t90 * t23;
    t148 = 0.3e1 * t95 * r12dot * t24;
    dxIZ_dxNZ_10 = (m2 * (t2 + t9 / 0.2e1) * t7 + m2 * b * t15) * omega + (-m2 * r12dot * t1 + t21 * t5 * t25 / 0.2e1) * t24 + (t32 * t34 * t7 * omega + (t41 * t43 * r12dot + (-t48 - t53) * t24) * t24) * t59 + (t62 * t64 * t8 * omega + (t32 * t64 * t7 * r12dot + (-t3 * t61 * t1 * omega / 0.2e1 + (t21 * t31 * t5 * t7 * r12dot - t80 * t5 * omega * t24) * t24) * t24) * t24 + t103 * t59 + (t32 * (-0.1e1 / 0.2e1 - 0.2e1 * t33) * t7 * omega + ((-m2 - t110) * t43 * r12dot + (t48 - t53) * t24) * t24 + t103 * y) * y) * y + (-t32 * (0.1e1 + (0.3e1 / 0.2e1 * t2 + t9) * t8) * r12dot + (t62 * (t2 + 0.2e1 * t9) * t7 * omega + (-t32 * (t2 + t9) * r12dot + 0.2e1 * t80 * t5 * t25) * t24) * t24 + (0.2e1 * t144 + t148) * t24 * t59 + ((0.2e1 * m2 + t110) * t7 * t43 * r12dot + t32 * (-0.6e1 * t33 - 0.1e1) * omega * t24 + (-0.3e1 * t95 * t8 * r12dot + (0.4e1 * t144 + t148) * t24) * y) * y + (-t32 * t34 * t7 * omega + (-t41 * t43 * r12dot - 0.2e1 * t46 * t1 * t25) * t24 + (-t92 - t101 * t24) * y) * x) * x;

    t2 = m2 / M;
    t4 = m2 * m2;
    t5 = M * M;
    t8 = np.sin(phi);
    t9 = t8 * t8;
    t10 = t4 / t5 * t9;
    t16 = 0.1e1 / b;
    t18 = m2 * t16;
    t20 = np.cos(phi);
    t21 = t20 * t20;
    t24 = b * b;
    t27 = m2 / t24 / b;
    t28 = z * z;
    t31 = 0.1e1 / t24;
    t35 = -0.1e1 - 0.2e1 * t2 * t9;
    dxIZ_dxNZ_11 = (m2 * (0.1e1 + (0.3e1 / 0.2e1 * t2 + t10) * t9) + b) * t16 + t18 * (t2 + t10) * t21 - t27 * t21 * t28 + (m2 * t31 * t35 * t8 - t27 * (-t9 + t21) * y) * y + t18 * (t35 * t16 * t20 + 0.2e1 * t31 * t8 * t20 * y) * x;

    t1 = 0.1e1 / b;
    t2 = m2 * t1;
    t3 = 0.1e1 / M;
    t4 = m2 * t3;
    t6 = m2 * m2;
    t7 = M * M;
    t9 = t6 / t7;
    t10 = np.sin(phi);
    t11 = t10 * t10;
    t15 = np.cos(phi);
    t16 = t15 * t15;
    t21 = b * b;
    t22 = 0.1e1 / t21;
    t23 = t22 * t10;
    t24 = z * z;
    t27 = t4 * t11;
    dxIZ_dxNZ_12 = t2 * (((-t4 / 0.2e1 - t9 * t11) * t10 - t9 * t10 * t16) * t15 - t23 * t15 * t24 + (((0.1e1 + 0.4e1 * t27) * t1 + 0.2e1 * t2 * t3 * t16) * t15 - 0.3e1 * t23 * t15 * y) * y + ((-0.1e1 - 0.2e1 * t27) * t10 * t1 - t22 * (0.2e1 * t16 - 0.2e1 * t11) * y + t23 * t15 * x) * x);

    t1 = 0.1e1 / b;
    t2 = m2 * t1;
    t3 = 0.1e1 / M;
    t5 = np.sin(phi);
    t6 = t5 * t5;
    t11 = np.cos(phi);
    t12 = t11 * t11;
    t19 = b * b;
    t21 = 0.1e1 / t19 * z;
    dxIZ_dxNZ_13 = t2 * (((0.2e1 * m2 * t3 * t6 + 0.1e1) * t1 + 0.2e1 * t2 * t3 * t12) * t11 * z - 0.2e1 * t21 * t5 * t11 * y - 0.2e1 * t21 * t12 * x);

    t2 = 0.1e1 / M;
    t3 = np.sin(phi);
    t4 = t2 * t3;
    t6 = m2 * t2;
    t7 = m2 * m2;
    t8 = M * M;
    t9 = 0.1e1 / t8;
    t11 = t3 * t3;
    t12 = t7 * t9 * t11;
    t20 = t7 * m2;
    t22 = np.cos(phi);
    t23 = t22 * t22;
    t34 = b * b;
    t36 = 0.1e1 / t34 / b;
    t38 = (-m2 - 0.2e1 * t7 * t2 * t11) * t3 * t36 * r12dot;
    t39 = 0.1e1 / t34;
    t40 = m2 * t39;
    t41 = t6 * t11;
    t45 = t7 * t36;
    t46 = r12dot * t2;
    t48 = t45 * t46 * t3;
    t50 = t7 * t39;
    t51 = t2 * omega;
    t53 = t50 * t51 * t22;
    t54 = -0.2e1 * t48 + t53;
    t59 = z * z;
    t64 = 0.1e1 / b;
    t65 = m2 * t64;
    t75 = t20 * t64;
    t76 = t9 * t3;
    t80 = t20 * t39;
    t90 = m2 * t36;
    t95 = t34 * t34;
    t97 = m2 / t95;
    t100 = 0.3e1 * t97 * t11 * r12dot;
    t103 = t3 * r12dot;
    t118 = t6 / 0.2e1 + t12;
    t141 = t90 * t11 * omega;
    t146 = 0.3e1 * t97 * t103 - t90 * omega * t22;
    t148 = t141 + t146 * t22;
    t150 = t3 * omega;
    dxIZ_dxNZ_20 = -m2 * r12dot * t4 + ((m2 * (-t6 - t12 / 0.2e1) - m2 * b * t2) * omega - t20 * t9 * omega * t23 / 0.2e1) * t22 + (t38 + (t40 * (t41 + 0.1e1 / 0.2e1) * omega + t54 * t22) * t22) * t59 + (-t40 * (0.1e1 + t41) * r12dot + (t65 * (-t6 - 0.2e1 * t12) * t3 * omega + (-t40 * (0.3e1 / 0.2e1 * t6 + t12) * r12dot + (-0.2e1 * t75 * t76 * omega - t80 * t9 * r12dot * t22) * t22) * t22) * t22 + (-0.2e1 * t90 * t22 * t3 * omega + t100) * t59 + (t90 * t103 + (t40 * (0.2e1 * t41 - 0.1e1 / 0.2e1) * omega - t54 * t22) * t22) * y) * y + (t65 * t118 * t11 * omega + (t40 * t118 * t3 * r12dot + (-t7 * t64 * t51 / 0.2e1 + (t80 * t76 * r12dot - t75 * t9 * omega * t22) * t22) * t22) * t22 + t148 * t59 + (t40 * t150 + (0.2e1 * t90 * r12dot + (0.6e1 * t50 * t4 * omega + 0.4e1 * t45 * t46 * t22) * t22) * t22 + (-t141 - t146 * t22) * y) * y + (t38 + (t40 * (-t41 + 0.1e1 / 0.2e1) * omega + (-0.4e1 * t48 + 0.2e1 * t53) * t22) * t22 + (t100 + (-0.4e1 * t90 * t150 - 0.3e1 * t97 * r12dot * t22) * t22) * y + t148 * x) * x) * x;

    t1 = 0.1e1 / b;
    t2 = m2 * t1;
    t3 = 0.1e1 / M;
    t4 = m2 * t3;
    t6 = m2 * m2;
    t7 = M * M;
    t9 = t6 / t7;
    t10 = np.sin(phi);
    t11 = t10 * t10;
    t15 = np.cos(phi);
    t16 = t15 * t15;
    t21 = b * b;
    t22 = 0.1e1 / t21;
    t23 = t22 * t10;
    t24 = z * z;
    dxIZ_dxNZ_21 = t2 * (((-t4 / 0.2e1 - t9 * t11) * t10 - t9 * t10 * t16) * t15 - t23 * t15 * t24 + ((-t1 - 0.2e1 * t2 * t3 * t16) * t15 + t23 * t15 * y) * y + ((0.2e1 * t4 * t11 + 0.1e1) * t10 * t1 + 0.4e1 * t2 * t3 * t10 * t16 - t22 * (0.2e1 * t11 - 0.2e1 * t16) * y - 0.3e1 * t23 * t15 * x) * x);

    t1 = 0.1e1 / M;
    t2 = m2 * t1;
    t3 = np.sin(phi);
    t4 = t3 * t3;
    t9 = 0.1e1 / b;
    t11 = m2 * t9;
    t13 = m2 * m2;
    t14 = M * M;
    t15 = 0.1e1 / t14;
    t22 = np.cos(phi);
    t23 = t22 * t22;
    t28 = b * b;
    t31 = m2 / t28 / b;
    t32 = z * z;
    dxIZ_dxNZ_22 = (m2 * (0.1e1 + t2 * t4) + b) * t9 + (t11 * (0.3e1 / 0.2e1 * t2 + t13 * t15 * t4) + t13 * m2 * t9 * t15 * t23) * t23 - t31 * t4 * t32 + t11 * (-t3 * t9 - 0.2e1 * t11 * t1 * t3 * t23) * y + (t11 * ((-t9 - 0.2e1 * t11 * t1 * t23) * t22 + 0.2e1 / t28 * t3 * t22 * y) - t31 * (t4 - t23) * x) * x;

    t1 = 0.1e1 / b;
    t2 = m2 * t1;
    t3 = 0.1e1 / M;
    t5 = np.sin(phi);
    t6 = t5 * t5;
    t13 = np.cos(phi);
    t14 = t13 * t13;
    t20 = b * b;
    t22 = 0.1e1 / t20 * z;
    dxIZ_dxNZ_23 = t2 * (((0.2e1 * m2 * t3 * t6 + 0.1e1) * t5 * t1 + 0.2e1 * t2 * t3 * t5 * t14) * z - 0.2e1 * t22 * t6 * y - 0.2e1 * t22 * t5 * t13 * x);

    t1 = b * b;
    t2 = 0.1e1 / t1;
    t3 = m2 * t2;
    t4 = 0.1e1 / M;
    t5 = m2 * t4;
    t6 = m2 * m2;
    t7 = M * M;
    t8 = 0.1e1 / t7;
    t10 = np.sin(phi);
    t11 = t10 * t10;
    t12 = t6 * t8 * t11;
    t25 = np.cos(phi);
    t26 = t25 * t25;
    t37 = 0.2e1 * m2 + 0.4e1 * t6 * t4 * t11;
    t40 = 0.1e1 / t1 / b;
    t45 = -0.1e1 - 0.2e1 * t5 * t11;
    t48 = t6 * t40;
    t49 = r12dot * t4;
    t53 = t6 * t2;
    t64 = t1 * t1;
    t66 = m2 / t64;
    t70 = m2 * t40;
    dxIZ_dxNZ_30 = (-t3 * (0.1e1 + (t5 + t12) * t11) * r12dot + (-t3 * (t5 + 0.2e1 * t12) * r12dot - t6 * m2 * t2 * t8 * r12dot * t26) * t26) * z + ((t37 * t10 * t40 * r12dot + (t3 * t45 * omega + (0.4e1 * t48 * t49 * t10 - 0.2e1 * t53 * t4 * omega * t25) * t25) * t25) * z + (-0.3e1 * t66 * t11 * r12dot + 0.2e1 * t70 * t25 * t10 * omega) * z * y) * y + ((-t3 * t45 * t10 * omega + (t37 * t40 * r12dot + (0.2e1 * t53 * t4 * t10 * omega + 0.4e1 * t48 * t49 * t25) * t25) * t25) * z + (-0.2e1 * t70 * t11 * omega + (-0.6e1 * t66 * t10 * r12dot + 0.2e1 * t70 * omega * t25) * t25) * z * y + (-0.2e1 * t70 * t10 * omega - 0.3e1 * t66 * r12dot * t25) * t25 * z * x) * x;

    t1 = 0.1e1 / b;
    t2 = m2 * t1;
    t3 = 0.1e1 / M;
    t5 = np.sin(phi);
    t6 = t5 * t5;
    t11 = np.cos(phi);
    t12 = t11 * t11;
    t19 = b * b;
    t21 = 0.1e1 / t19 * z;
    dxIZ_dxNZ_31 = t2 * (((-0.1e1 - 0.2e1 * m2 * t3 * t6) * t1 - 0.2e1 * t2 * t3 * t12) * t11 * z + 0.2e1 * t21 * t5 * t11 * y + 0.2e1 * t21 * t12 * x);

    t1 = 0.1e1 / b;
    t2 = m2 * t1;
    t3 = 0.1e1 / M;
    t5 = np.sin(phi);
    t6 = t5 * t5;
    t13 = np.cos(phi);
    t14 = t13 * t13;
    t20 = b * b;
    t22 = 0.1e1 / t20 * z;
    dxIZ_dxNZ_32 = t2 * (((-0.1e1 - 0.2e1 * m2 * t3 * t6) * t5 * t1 - 0.2e1 * t2 * t3 * t5 * t14) * z + 0.2e1 * t22 * t6 * y + 0.2e1 * t22 * t5 * t13 * x);

    t1 = 0.1e1 / M;
    t2 = m2 * t1;
    t3 = m2 * m2;
    t4 = M * M;
    t5 = 0.1e1 / t4;
    t7 = np.sin(phi);
    t8 = t7 * t7;
    t9 = t3 * t5 * t8;
    t15 = 0.1e1 / b;
    t17 = m2 * t15;
    t23 = np.cos(phi);
    t24 = t23 * t23;
    t31 = -0.1e1 - 0.2e1 * t2 * t8;
    t40 = b * b;
    t43 = m2 / t40 / b;
    dxIZ_dxNZ_33 = (m2 * (0.1e1 + (t2 + t9) * t8) + b) * t15 + (t17 * (t2 + 0.2e1 * t9) + t3 * m2 * t15 * t5 * t24) * t24 + (t17 * (t31 * t7 * t15 - 0.2e1 * t17 * t1 * t7 * t24) + t43 * t8 * y) * y + (t17 * ((t31 * t15 - 0.2e1 * t17 * t1 * t24) * t23 + 0.2e1 / t40 * t7 * t23 * y) + t43 * t24 * x) * x;

    dxIZ_dxNZ = {'00':dxIZ_dxNZ_00, '01':dxIZ_dxNZ_01, '02':dxIZ_dxNZ_02, '03':dxIZ_dxNZ_03,
                 '10':dxIZ_dxNZ_10, '11':dxIZ_dxNZ_11, '12':dxIZ_dxNZ_12, '13':dxIZ_dxNZ_13,
                 '20':dxIZ_dxNZ_20, '21':dxIZ_dxNZ_21, '22':dxIZ_dxNZ_22, '23':dxIZ_dxNZ_23,
                 '30':dxIZ_dxNZ_30, '31':dxIZ_dxNZ_31, '32':dxIZ_dxNZ_32, '33':dxIZ_dxNZ_33 }

    return dxIZ_dxNZ



#assumes r12dot -> 0
def get_dxIZ_dxNZ_r12const( trajfile, BH, TNZ, XNZ, YNZ, ZNZ, SETPHASE=None, spinning=False ):

    #Initialize elements as arrays for const value ones
    dxIZ_dxNZ_00 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_01 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_02 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_03 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_10 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_11 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_12 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_13 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_20 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_21 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_22 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_23 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_30 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_31 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_32 = np.zeros((np.shape(XNZ)))
    dxIZ_dxNZ_33 = np.zeros((np.shape(XNZ)))

    if spinning:
        traj_columns = (0, 7, 8, 24, 25, 26)
    else:
        traj_columns = (0, 1, 2, 3, 23, 24)
    tbh_traj, m_bh1_traj, m_bh2_traj, b_traj, phi_traj, omega_traj = np.loadtxt(trajfile,usecols=traj_columns, unpack=True)
    fm_bh1 = interp1d(tbh_traj, m_bh1_traj, kind='linear')
    fm_bh2 = interp1d(tbh_traj, m_bh2_traj, kind='linear')
    fb     = interp1d(tbh_traj, b_traj    , kind='linear')
    fphi   = interp1d(tbh_traj, phi_traj  , kind='linear')
    fomega = interp1d(tbh_traj, omega_traj, kind='linear')

    m_bh1 = fm_bh1(TNZ)
    m_bh2 = fm_bh2(TNZ)
    b     = fb(TNZ)
    phi   = fphi(TNZ)
    omega = fomega(TNZ)
    #arrays??? # i think column numbers are wrong
    #print(m_bh1,m_bh2,phi,omega,b)
    if SETPHASE is not None:
        phi = SETPHASE

    if( BH == 1 or BH is 'primary'):
        t = TNZ; x = XNZ
        y = YNZ; z = ZNZ

        #Set BH Traj Values
        m1    = m_bh1
        m2    = m_bh2

    elif( BH == 2 ):
        t = TNZ;  x = -XNZ
        y = -YNZ; z = ZNZ

        #Set BH Traj Values
        m1    = m_bh2
        m2    = m_bh1

    M = m1 + m2
    #Maple generated code
    t1 = np.cos(phi)
    t3 = np.sin(phi)
    t6 = m2 * (x * t1 + y * t3)
    t7 = m2 * b
    t8 = 0.1e1 / M
    t9 = t8 * t1
    t12 = np.power(x - t7 * t9, 0.2e1)
    t13 = t8 * t3
    t16 = np.power(y - t7 * t13, 0.2e1)
    t17 = z * z
    t19 = np.sqrt(t12 + t16 + t17)
    t28 = m2 * t8
    t30 = t3 * t3
    t31 = t28 * t30
    t33 = -0.3e1 * t28 + 0.6e1 * t31
    t35 = t1 * t1
    t37 = t28 * t35 * omega
    t42 = 0.1e1 / t19
    t45 = y * y
    t55 = t3 * omega
    t58 = 0.6e1 * t28 * t55 * t35
    t90 = -t28 * t35 - t31
    t93 = t13 * omega
    t96 = t9 * omega
    t104 = np.sqrt(m2 / b)
    t105 = np.sqrt(t28)
    t106 = t104 * t105
    t108 = m2 * m2
    t109 = t90 * t90
    t111 = t108 * t109 * t42
    t127 = b * b
    dxIZ_dxNZ_00 = (-0.2e1 * t6 * t19 * (-x * t3 * omega + y * t1 * omega) + (m2 * (((t33 * omega + 0.6e1 * t37) * t1 * t19 + 0.3e1 * t30 * t42 * m2 * t9 * omega * t45) * y + ((-t33 * t3 * omega - t58) * t19 + (-0.3e1 * t28 * t30 * t3 * omega + t58) * t42 * t45 + ((-0.6e1 * t28 * t30 * omega + 0.3e1 * t37) * t1 * t42 * y - 0.3e1 * t35 * t42 * m2 * t13 * omega * x) * x) * x) / 0.3e1 + (-t6 * t90 * t42 * (0.2e1 * x * m2 * t93 - 0.2e1 * y * m2 * t96) + (0.1e1 + (t106 * t55 + t111 * t96) * y + (t106 * t1 * omega - t111 * t93) * x) * b) * b) * b) / t127 / b
    t1 = np.sin(phi)
    t2 = y * t1
    t3 = m2 * b
    t4 = 0.1e1 / M
    t5 = np.cos(phi)
    t6 = t4 * t5
    t9 = np.power(x - t3 * t6, 0.2e1)
    t13 = np.power(y - t3 * t4 * t1, 0.2e1)
    t14 = z * z
    t16 = np.sqrt(t9 + t13 + t14)
    t20 = t5 * t5
    t24 = y * y
    t25 = t1 * t1
    t27 = 0.1e1 / t16
    t28 = t24 * t25 * t27
    t43 = m2 * t4
    t45 = t43 * t25
    t46 = 0.6e1 * t45
    t47 = t43 * t20
    t74 = -t47 - t45
    t80 = m2 * m2
    t81 = M * M
    t83 = t80 / t81
    t84 = t25 * t25
    t101 = np.sqrt(m2 / b)
    t102 = np.sqrt(t43)
    t105 = t74 * t74
    t117 = b * b
    dxIZ_dxNZ_01 = (m2 * (-0.6e1 * t2 * t16 * t5 + ((0.3e1 - 0.6e1 * t20) * t16 - 0.3e1 * t28 + (-0.6e1 * t2 * t5 * t27 - 0.3e1 * t20 * t27 * x) * x) * x) / 0.3e1 + (m2 * ((-0.3e1 * t43 + t46 + 0.6e1 * t47) * t5 * t16 + 0.3e1 * t28 * t43 * t5 + ((0.6e1 * t43 * t25 * t1 + 0.12e2 * t1 * t20 * t43) * t27 * y + (t46 + 0.9e1 * t47) * t5 * t27 * x) * x) / 0.3e1 + (m2 * (0.6e1 * t2 * t74 * t27 * m2 * t6 + (-0.3e1 * t83 * t84 + (-0.12e2 * t83 * t25 - 0.9e1 * t83 * t20) * t20) * t27 * x) / 0.3e1 + (t101 * t102 * t1 + t80 * t105 * t27 * t4 * t5) * b) * b) * b) / t117 / b
    t1 = np.sin(phi)
    t2 = t1 * t1
    t5 = m2 * b
    t6 = 0.1e1 / M
    t7 = np.cos(phi)
    t11 = np.power(x - t5 * t6 * t7, 0.2e1)
    t15 = np.power(y - t5 * t6 * t1, 0.2e1)
    t16 = z * z
    t18 = np.sqrt(t11 + t15 + t16)
    t20 = y * y
    t22 = 0.1e1 / t18
    t34 = t7 * t7
    t44 = m2 * t6
    t46 = t44 * t2
    t50 = t1 * t34
    t52 = 0.6e1 * t50 * t44
    t62 = t44 * t34
    t77 = m2 * m2
    t78 = M * M
    t80 = t77 / t78
    t81 = t2 * t2
    t93 = -t62 - t46
    t105 = np.sqrt(m2 / b)
    t106 = np.sqrt(t44)
    t109 = t93 * t93
    t121 = b * b
    dxIZ_dxNZ_02 = (m2 * (((0.3e1 - 0.6e1 * t2) * t18 - 0.3e1 * t20 * t2 * t22) * y + (-0.6e1 * t7 * t18 * t1 - 0.6e1 * t20 * t1 * t7 * t22 - 0.3e1 * t34 * t22 * y * x) * x) / 0.3e1 + (m2 * (((-0.3e1 * t44 + 0.6e1 * t46) * t1 + t52) * t18 + (0.9e1 * t44 * t2 * t1 + t52) * t22 * t20 + ((0.12e2 * t46 + 0.6e1 * t62) * t7 * t22 * y + 0.3e1 * t50 * t22 * t44 * x) * x) / 0.3e1 + (m2 * ((-0.9e1 * t80 * t81 + (-0.12e2 * t80 * t2 - 0.3e1 * t80 * t34) * t34) * t22 * y + 0.6e1 * t7 * t93 * t22 * t44 * t1 * x) / 0.3e1 + (-t105 * t106 * t7 + t77 * t109 * t22 * t6 * t1) * b) * b) * b) / t121 / b
    t1 = b * b
    t5 = m2 * b
    t6 = 0.1e1 / M
    t7 = np.cos(phi)
    t11 = np.power(x - t5 * t6 * t7, 0.2e1)
    t12 = np.sin(phi)
    t16 = np.power(y - t5 * t6 * t12, 0.2e1)
    t17 = z * z
    t19 = np.sqrt(t11 + t16 + t17)
    t22 = y * y
    t23 = t12 * t12
    t25 = 0.1e1 / t19
    t26 = t25 * z
    t29 = y * t12
    t34 = t7 * t7
    t43 = m2 * t6
    t46 = -t43 * t34 - t43 * t23
    t50 = t46 * t46
    dxIZ_dxNZ_03 = m2 / t1 / b * (0.3e1 * t19 * z - 0.3e1 * t22 * t23 * t26 + (-0.6e1 * t29 * t7 * t25 * z - 0.3e1 * t34 * t25 * z * x) * x + (-0.6e1 * (x * t7 + t29) * t46 * t26 - 0.3e1 * t50 * t25 * z * b) * b) / 0.3e1
    t1 = np.sin(phi)
    t2 = t1 * t1
    t4 = np.cos(phi)
    t5 = t4 * t4
    t6 = t5 * omega
    t7 = t2 * omega - t6
    t8 = z * z
    t10 = y * y
    t15 = t1 * omega
    t29 = 0.1e1 / M
    t30 = m2 * t29
    t31 = t30 * t2
    t32 = -0.1e1 / 0.2e1 - t31
    t35 = t15 * t5
    t36 = t30 * t35
    t61 = m2 * m2
    t62 = M * M
    t64 = t61 / t62
    t65 = t64 * t2
    t80 = t64 * t35
    t105 = b * b
    dxIZ_dxNZ_10 = (m2 * ((t7 * t8 + t7 * t10) * y + (0.2e1 * t8 * t4 * t15 + 0.4e1 * t10 * t1 * t4 * omega - t7 * y * x) * x) + (m2 * ((t32 * t1 * omega - t36) * t8 + ((-0.1e1 / 0.2e1 - 0.2e1 * t31) * t1 * omega + t36) * t10 + ((-0.6e1 * t31 - 0.1e1) * omega * t4 * y + (-t32 * t1 * omega - 0.2e1 * t36) * x) * x) + (m2 * (((t30 / 0.2e1 + t65) * t2 * omega + (-t30 * omega / 0.2e1 - t64 * t6) * t5) * y + ((t30 + 0.2e1 * t65) * t1 * omega + 0.2e1 * t80) * t4 * x) + (m2 * ((t30 + t65 / 0.2e1) * t1 * omega + t80 / 0.2e1) + m2 * b * t29 * t1 * omega) * b) * b) * b) / t105 / b
    t1 = z * z
    t2 = np.cos(phi)
    t3 = t2 * t2
    t5 = np.sin(phi)
    t6 = t5 * t5
    t8 = y * y
    t17 = m2 / M
    t20 = -0.1e1 - 0.2e1 * t17 * t6
    t28 = m2 * m2
    t29 = M * M
    t32 = t28 / t29 * t6
    t44 = b * b
    dxIZ_dxNZ_11 = (m2 * (-t1 * t3 + (t6 - t3) * t8 + 0.2e1 * y * t5 * t2 * x) + (m2 * (t20 * t5 * y + t20 * t2 * x) + (m2 * (0.1e1 + (0.3e1 / 0.2e1 * t17 + t32) * t6 + (t32 + t17) * t3) + b) * b) * b) / t44 / b
    t1 = b * b
    t5 = z * z
    t6 = np.cos(phi)
    t8 = np.sin(phi)
    t10 = y * y
    t14 = t6 * t6
    t15 = t8 * t8
    t24 = m2 / M
    t25 = t24 * t15
    t37 = m2 * m2
    t38 = M * M
    t40 = t37 / t38
    dxIZ_dxNZ_12 = m2 / t1 / b * (-t5 * t6 * t8 - 0.3e1 * t10 * t8 * t6 + ((-0.2e1 * t14 + 0.2e1 * t15) * y + t6 * t8 * x) * x + ((0.4e1 * t25 + 0.1e1 + 0.2e1 * t24 * t14) * t6 * y + (-0.1e1 - 0.2e1 * t25) * t8 * x + ((-t24 / 0.2e1 - t40 * t15) * t8 - t40 * t8 * t14) * t6 * b) * b)
    t1 = b * b
    t5 = np.cos(phi)
    t7 = np.sin(phi)
    t14 = m2 / M
    t15 = t7 * t7
    t18 = t5 * t5
    dxIZ_dxNZ_13 = m2 / t1 / b * (-0.2e1 * z * (x * t5 + y * t7) * t5 + (0.2e1 * t14 * t15 + 0.1e1 + 0.2e1 * t14 * t18) * t5 * z * b)
    t1 = z * z
    t2 = np.cos(phi)
    t4 = np.sin(phi)
    t5 = t4 * omega
    t9 = t4 * t4
    t11 = t2 * t2
    t12 = t11 * omega
    t13 = t9 * omega - t12
    t16 = y * y
    t29 = 0.1e1 / M
    t30 = m2 * t29
    t31 = t30 * t9
    t34 = t30 * t12
    t44 = t5 * t11
    t59 = m2 * m2
    t60 = M * M
    t62 = t59 / t60
    t63 = t62 * t9
    t79 = t62 * t12
    t104 = b * b
    dxIZ_dxNZ_20 = (m2 * (-0.2e1 * t1 * t2 * t5 * y + (t13 * t1 - t13 * t16 + (-0.4e1 * y * t4 * t2 * omega + t13 * x) * x) * x) + (m2 * (((0.1e1 / 0.2e1 + t31) * omega + t34) * t2 * t1 + ((0.2e1 * t31 - 0.1e1 / 0.2e1) * omega - t34) * t2 * t16 + ((t5 + 0.6e1 * t30 * t44) * y + ((-t31 + 0.1e1 / 0.2e1) * omega + 0.2e1 * t34) * t2 * x) * x) + (m2 * (((-t30 - 0.2e1 * t63) * t4 * omega - 0.2e1 * t62 * t44) * t2 * y + ((t30 / 0.2e1 + t63) * t9 * omega + (-t30 * omega / 0.2e1 - t79) * t11) * x) + (m2 * ((-t30 - t63 / 0.2e1) * omega - t79 / 0.2e1) * t2 - m2 * b * t29 * t2 * omega) * b) * b) * b) / t104 / b
    t1 = b * b
    t5 = z * z
    t6 = np.cos(phi)
    t8 = np.sin(phi)
    t10 = y * y
    t13 = t6 * t6
    t14 = t8 * t8
    t24 = m2 / M
    t34 = t8 * t13
    t40 = m2 * m2
    t41 = M * M
    t43 = t40 / t41
    dxIZ_dxNZ_21 = m2 / t1 / b * (-t5 * t6 * t8 + t10 * t8 * t6 + ((0.2e1 * t13 - 0.2e1 * t14) * y - 0.3e1 * t6 * t8 * x) * x + ((-0.1e1 - 0.2e1 * t24 * t13) * t6 * y + ((0.2e1 * t24 * t14 + 0.1e1) * t8 + 0.4e1 * t34 * t24) * x + ((-t24 / 0.2e1 - t43 * t14) * t8 - t43 * t34) * t6 * b) * b)
    t1 = z * z
    t2 = np.sin(phi)
    t3 = t2 * t2
    t6 = np.cos(phi)
    t9 = t6 * t6
    t18 = m2 / M
    t32 = m2 * m2
    t33 = M * M
    t35 = t32 / t33
    t47 = b * b
    dxIZ_dxNZ_22 = (m2 * (-t1 * t3 + (0.2e1 * y * t2 * t6 + (t9 - t3) * x) * x) + (m2 * ((-t2 - 0.2e1 * t2 * t9 * t18) * y + (-0.1e1 - 0.2e1 * t18 * t9) * t6 * x) + (m2 * (0.1e1 + t18 * t3 + (0.3e1 / 0.2e1 * t18 + t35 * t3 + t35 * t9) * t9) + b) * b) * b) / t47 / b
    t1 = b * b
    t5 = np.cos(phi)
    t7 = np.sin(phi)
    t14 = m2 / M
    t15 = t7 * t7
    t20 = t5 * t5
    dxIZ_dxNZ_23 = m2 / t1 / b * (-0.2e1 * z * (x * t5 + y * t7) * t7 + ((0.2e1 * t14 * t15 + 0.1e1) * t7 + 0.2e1 * t7 * t20 * t14) * z * b)
    t1 = b * b
    t5 = np.cos(phi)
    t7 = np.sin(phi)
    t19 = m2 / M
    t20 = t7 * t7
    t23 = -0.1e1 - 0.2e1 * t19 * t20
    t25 = t5 * t5
    dxIZ_dxNZ_30 = m2 / t1 / b * (0.2e1 * z * (x * t5 + y * t7) * (-x * t7 * omega + y * t5 * omega) + ((t23 * omega - 0.2e1 * t19 * t25 * omega) * t5 * z * y + (-t23 * t7 * omega + 0.2e1 * t19 * t7 * omega * t25) * z * x) * b)
    t1 = b * b
    t5 = np.cos(phi)
    t7 = np.sin(phi)
    t14 = m2 / M
    t15 = t7 * t7
    t18 = t5 * t5
    dxIZ_dxNZ_31 = m2 / t1 / b * (0.2e1 * z * (x * t5 + y * t7) * t5 + (-0.1e1 - 0.2e1 * t14 * t15 - 0.2e1 * t14 * t18) * t5 * z * b)
    t1 = b * b
    t5 = np.cos(phi)
    t7 = np.sin(phi)
    t14 = m2 / M
    t15 = t7 * t7
    t20 = t5 * t5
    dxIZ_dxNZ_32 = m2 / t1 / b * (0.2e1 * z * (x * t5 + y * t7) * t7 + ((-0.1e1 - 0.2e1 * t14 * t15) * t7 - 0.2e1 * t7 * t20 * t14) * z * b)
    t1 = np.cos(phi)
    t3 = np.sin(phi)
    t6 = np.power(x * t1 + y * t3, 0.2e1)
    t9 = m2 / M
    t10 = t3 * t3
    t12 = 0.2e1 * t9 * t10
    t15 = t1 * t1
    t28 = m2 * m2
    t29 = M * M
    t31 = t28 / t29
    t32 = t31 * t10
    t46 = b * b
    dxIZ_dxNZ_33 = (t6 * m2 + (m2 * (((-0.1e1 - t12) * t3 - 0.2e1 * t3 * t15 * t9) * y + (-0.1e1 - t12 - 0.2e1 * t9 * t15) * t1 * x) + (m2 * (0.1e1 + (t32 + t9) * t10 + (t9 + 0.2e1 * t32 + t31 * t15) * t15) + b) * b) * b) / t46 / b

    dxIZ_dxNZ = {'00':dxIZ_dxNZ_00, '01':dxIZ_dxNZ_01, '02':dxIZ_dxNZ_02, '03':dxIZ_dxNZ_03,
                 '10':dxIZ_dxNZ_10, '11':dxIZ_dxNZ_11, '12':dxIZ_dxNZ_12, '13':dxIZ_dxNZ_13,
                 '20':dxIZ_dxNZ_20, '21':dxIZ_dxNZ_21, '22':dxIZ_dxNZ_22, '23':dxIZ_dxNZ_23,
                 '30':dxIZ_dxNZ_30, '31':dxIZ_dxNZ_31, '32':dxIZ_dxNZ_32, '33':dxIZ_dxNZ_33 }

    return dxIZ_dxNZ
################################################################################
#          E N D    J A C O B I A N    C A L C U L A T I O N S                 #
################################################################################


################################################################################
#          S T A R T    C O O R D I N A T E    C A L C U L A T I O N S         #
################################################################################
def coordsBL_of_coordsIZ( Mass, Spin, TIZ, XIZ, YIZ, ZIZ ):
    #Define horizons
    rp = Mass + np.sqrt(Mass*Mass - Spin*Spin)
    rm = Mass - np.sqrt(Mass*Mass - Spin*Spin)

    #Define short hands for IZ coords
    tH = TIZ; xH = XIZ
    yH = YIZ; zH = ZIZ

    #Build radial quantities from IZ coords
    rH     = np.sqrt(xH*xH + yH*yH + zH*zH)
    rBL    = (1. / np.sqrt(2.)) * np.sqrt( rH*rH - Spin*Spin + np.sqrt( (rH*rH - Spin*Spin)*(rH*rH - Spin*Spin) + 4. * Spin*Spin * zH * zH ) ) + Mass
    lnarg  = ( rBL - rp ) / ( rBL - rm )
    lnterm = np.log( np.fabs( lnarg ) )

    #Build intermediate quantities
    phH = np.arctan2( yH, xH )
    phA = np.arctan2( Spin, rBL - Mass )

    #Calculate remaining coordinates
    tBL  = tH - ( (rp*rp + Spin*Spin)/(rp - rm) ) * lnterm
    thBL = np.arccos( zH / ( rBL - Mass ) )
    phBL = phH - phA - ( Spin / ( rp - rm ) ) * lnterm

    #Return approximate BL coordinates
    TTBL = tBL
    RRBL = rBL
    THBL = thBL
    PHBL = phBL

    return TTBL, RRBL, THBL, PHBL

def coordsIZ_of_coordsNZ(trajfile, MASS_TYPE, TNZ, XNZ, YNZ, ZNZ, SETPHASE=None, spinning=False):

    if spinning:
        traj_columns = (0, 7, 8, 24, 25)
    else:
        traj_columns = (0, 1, 2, 3, 23)
    tbh_traj, m_bh1_traj, m_bh2_traj, b_traj, phi_traj = np.loadtxt(trajfile, usecols=traj_columns, unpack=True)
    fm_bh1 = interp1d(tbh_traj, m_bh1_traj, kind='linear')
    fm_bh2 = interp1d(tbh_traj, m_bh2_traj, kind='linear')
    fb     = interp1d(tbh_traj, b_traj    , kind='linear')
    fphi   = interp1d(tbh_traj, phi_traj  , kind='linear')

    m_bh1 = fm_bh1(TNZ)
    m_bh2 = fm_bh2(TNZ)
    b     = fb(TNZ)
    if SETPHASE is None:
        phi   = fphi(TNZ)
    else:
        phi   = SETPHASE

    #rotate
    # xcor = XNZ*np.cos(phi) + YNZ*np.sin(phi)
    # ycor = -XNZ*np.sin(phi) + YNZ*np.cos(phi)
    # XNZ = xcor; YNZ = ycor

    if( MASS_TYPE == 1 or MASS_TYPE is 'primary'):
        #Set BBH information
        m1  = m_bh1
        m2  = m_bh2

        #Rename NZ coordinates
        t = TNZ; x = XNZ
        y = YNZ; z = ZNZ

    elif( MASS_TYPE == 2 ):
        #Set BBH information
        m1  = m_bh2
        m2  = m_bh1

        #Rename NZ coordinates
        t = TNZ;  x = -XNZ
        y = -YNZ; z = ZNZ

    t1 = 1./b
    t2 = m2 * t1
    t3 = m1 + m2
    t3 = 1./t3
    t4 = m2 * t3
    t5 = np.sin(phi)
    t6 = t4 * b
    t7 = y - t6 * t5
    t8 = np.cos(phi)
    t6 = x - t6 * t8
    t9 = t7 * t8 - t6 * t5
    t10 = t6*t6 + t7*t7 + z * z
    t11 = np.sqrt(t10)
    t12 = t6 * t8 + t7 * t5
    t13 = t12*t12
    t14 = t1*t1
    t15 = t10 * t8
    t16 = 1. - t12 * t1
    t17 = t4 * t9
    t3 = b * t3
    t18 = t10 * t5
    TIZ = t - np.sqrt(t2*t4) * t9 + m2 * t1 * t14 * (t10 * t11 - 3. * t13 * t11) / 3.
    XIZ = (-t3 * t8 + t1 * (t15 * t1 * 0.5 - t17 * t5 * 0.5 - t14 * t12 * (t15 - t12 * t6) + t6 * t16)) * m2 + x
    YIZ = (-t3 * t5 + t1 * (t18 * t1 * 0.5 + t17 * t8 * 0.5 - t14 * t12 * (t18 - t12 * t7) + t7 * t16)) * m2 + y
    ZIZ = z + t2 * z * (t14 * t13 + t16)

    return TIZ, XIZ, YIZ, ZIZ

################################################################################
#           E N D    C O O R D I N A T E    C A L C U L A T I O N S            #
################################################################################


################################################################################
#          S T A R T    4 - V E C T O R    T R A N S F O R M A T I O N         #
################################################################################
def transform_rank1con( dxp_dx, ucon0, ucon1, ucon2, ucon3 ):
    ucon0p = dxp_dx['00']*ucon0 + dxp_dx['01']*ucon1 + dxp_dx['02']*ucon2 + dxp_dx['03']*ucon3
    ucon1p = dxp_dx['10']*ucon0 + dxp_dx['11']*ucon1 + dxp_dx['12']*ucon2 + dxp_dx['13']*ucon3
    ucon2p = dxp_dx['20']*ucon0 + dxp_dx['21']*ucon1 + dxp_dx['22']*ucon2 + dxp_dx['23']*ucon3
    ucon3p = dxp_dx['30']*ucon0 + dxp_dx['31']*ucon1 + dxp_dx['32']*ucon2 + dxp_dx['33']*ucon3

    return ucon0p, ucon1p, ucon2p, ucon3p

def transform_tensordensity( rank, weight, dxp_dx, density ):
    #Calculate determinant of the Jacobian
    dxpdx00 = dxp_dx['00']; dxpdx01 = dxp_dx['01']; dxpdx02 = dxp_dx['02']; dxpdx03 = dxp_dx['03']
    dxpdx10 = dxp_dx['10']; dxpdx11 = dxp_dx['11']; dxpdx12 = dxp_dx['12']; dxpdx13 = dxp_dx['13']
    dxpdx20 = dxp_dx['20']; dxpdx21 = dxp_dx['21']; dxpdx22 = dxp_dx['22']; dxpdx23 = dxp_dx['23']
    dxpdx30 = dxp_dx['30']; dxpdx31 = dxp_dx['31']; dxpdx32 = dxp_dx['32']; dxpdx33 = dxp_dx['33']
    J = dxpdx03* dxpdx12* dxpdx21* dxpdx30 - dxpdx02* dxpdx13* dxpdx21* dxpdx30 - dxpdx03* dxpdx11* dxpdx22* dxpdx30 + dxpdx01* dxpdx13* dxpdx22* dxpdx30 + dxpdx02* dxpdx11* dxpdx23* dxpdx30 - dxpdx01* dxpdx12* dxpdx23* dxpdx30 - dxpdx03* dxpdx12* dxpdx20 *dxpdx31 + dxpdx02 *dxpdx13 *dxpdx20 *dxpdx31 + dxpdx03* dxpdx10* dxpdx22* dxpdx31 - dxpdx00 *dxpdx13* dxpdx22 *dxpdx31 - dxpdx02* dxpdx10* dxpdx23* dxpdx31 + dxpdx00 *dxpdx12* dxpdx23* dxpdx31 + dxpdx03 *dxpdx11* dxpdx20* dxpdx32 - dxpdx01 *dxpdx13* dxpdx20* dxpdx32 - dxpdx03* dxpdx10* dxpdx21* dxpdx32 + dxpdx00* dxpdx13* dxpdx21* dxpdx32 + dxpdx01* dxpdx10* dxpdx23* dxpdx32 - dxpdx00* dxpdx11* dxpdx23* dxpdx32 - dxpdx02* dxpdx11* dxpdx20* dxpdx33 + dxpdx01 *dxpdx12* dxpdx20* dxpdx33 + dxpdx02* dxpdx10* dxpdx21 *dxpdx33 - dxpdx00 *dxpdx12* dxpdx21* dxpdx33 - dxpdx01* dxpdx10 *dxpdx22 *dxpdx33 + dxpdx00 *dxpdx11* dxpdx22* dxpdx33

    Jw = np.power(J,weight)

    if( rank == 0 ):
        return Jw * density
    elif( rank == 1 ):
        densityp0 = Jw * ( dxp_dx['00'] * density['0'] + dxp_dx['01'] * density['1'] + dxp_dx['02'] * density['2'] + dxp_dx['03'] * density['3'] )
        densityp1 = Jw * ( dxp_dx['10'] * density['0'] + dxp_dx['11'] * density['1'] + dxp_dx['12'] * density['2'] + dxp_dx['13'] * density['3'] )
        densityp2 = Jw * ( dxp_dx['20'] * density['0'] + dxp_dx['21'] * density['1'] + dxp_dx['22'] * density['2'] + dxp_dx['23'] * density['3'] )
        densityp3 = Jw * ( dxp_dx['30'] * density['0'] + dxp_dx['31'] * density['1'] + dxp_dx['32'] * density['2'] + dxp_dx['33'] * density['3'] )
        densityp = { '0':densityp0, '1':densityp1, '2':densityp2, '3':densityp3 }
        return densityp
    else:
        print("Rank greater than 1 not yet implemented to tensor density transformations... FAIL")
################################################################################
#          E N D   4 - V E C T O R    T R A N S F O R M A T I O N              #
################################################################################

################################################################################
#          S T A R T   R A N K 2 C O N     T R A N S F O R M A T I O N         #
################################################################################
def transform_gcon( dxp_dx, gcon ):
    gcon00p = dxp_dx['00'] * ( dxp_dx['00'] * gcon['00'] + dxp_dx['01'] * gcon['01'] + dxp_dx['02'] * gcon['02'] + dxp_dx['03'] * gcon['03'] ) \
            + dxp_dx['01'] * ( dxp_dx['00'] * gcon['01'] + dxp_dx['01'] * gcon['11'] + dxp_dx['02'] * gcon['12'] + dxp_dx['03'] * gcon['13'] ) \
            + dxp_dx['02'] * ( dxp_dx['00'] * gcon['02'] + dxp_dx['01'] * gcon['12'] + dxp_dx['02'] * gcon['22'] + dxp_dx['03'] * gcon['23'] ) \
            + dxp_dx['03'] * ( dxp_dx['00'] * gcon['03'] + dxp_dx['01'] * gcon['13'] + dxp_dx['02'] * gcon['23'] + dxp_dx['03'] * gcon['33'] )

    gcon01p = dxp_dx['00'] * ( dxp_dx['10'] * gcon['00'] + dxp_dx['11'] * gcon['01'] + dxp_dx['12'] * gcon['02'] + dxp_dx['13'] * gcon['03'] ) \
            + dxp_dx['01'] * ( dxp_dx['10'] * gcon['01'] + dxp_dx['11'] * gcon['11'] + dxp_dx['12'] * gcon['12'] + dxp_dx['13'] * gcon['13'] ) \
            + dxp_dx['02'] * ( dxp_dx['10'] * gcon['02'] + dxp_dx['11'] * gcon['12'] + dxp_dx['12'] * gcon['22'] + dxp_dx['13'] * gcon['23'] ) \
            + dxp_dx['03'] * ( dxp_dx['10'] * gcon['03'] + dxp_dx['11'] * gcon['13'] + dxp_dx['12'] * gcon['23'] + dxp_dx['13'] * gcon['33'] )

    gcon02p = dxp_dx['00'] * ( dxp_dx['20'] * gcon['00'] + dxp_dx['21'] * gcon['01'] + dxp_dx['22'] * gcon['02'] + dxp_dx['23'] * gcon['03'] ) \
            + dxp_dx['01'] * ( dxp_dx['20'] * gcon['01'] + dxp_dx['21'] * gcon['11'] + dxp_dx['22'] * gcon['12'] + dxp_dx['23'] * gcon['13'] ) \
            + dxp_dx['02'] * ( dxp_dx['20'] * gcon['02'] + dxp_dx['21'] * gcon['12'] + dxp_dx['22'] * gcon['22'] + dxp_dx['23'] * gcon['23'] ) \
            + dxp_dx['03'] * ( dxp_dx['20'] * gcon['03'] + dxp_dx['21'] * gcon['13'] + dxp_dx['22'] * gcon['23'] + dxp_dx['23'] * gcon['33'] )

    gcon03p = dxp_dx['00'] * ( dxp_dx['30'] * gcon['00'] + dxp_dx['31'] * gcon['01'] + dxp_dx['32'] * gcon['02'] + dxp_dx['33'] * gcon['03'] ) \
            + dxp_dx['01'] * ( dxp_dx['30'] * gcon['01'] + dxp_dx['31'] * gcon['11'] + dxp_dx['32'] * gcon['12'] + dxp_dx['33'] * gcon['13'] ) \
            + dxp_dx['02'] * ( dxp_dx['30'] * gcon['02'] + dxp_dx['31'] * gcon['12'] + dxp_dx['32'] * gcon['22'] + dxp_dx['33'] * gcon['23'] ) \
            + dxp_dx['03'] * ( dxp_dx['30'] * gcon['03'] + dxp_dx['31'] * gcon['13'] + dxp_dx['32'] * gcon['23'] + dxp_dx['33'] * gcon['33'] )

    gcon11p = dxp_dx['10'] * ( dxp_dx['10'] * gcon['00'] + dxp_dx['11'] * gcon['01'] + dxp_dx['12'] * gcon['02'] + dxp_dx['13'] * gcon['03'] ) \
            + dxp_dx['11'] * ( dxp_dx['10'] * gcon['01'] + dxp_dx['11'] * gcon['11'] + dxp_dx['12'] * gcon['12'] + dxp_dx['13'] * gcon['13'] ) \
            + dxp_dx['12'] * ( dxp_dx['10'] * gcon['02'] + dxp_dx['11'] * gcon['12'] + dxp_dx['12'] * gcon['22'] + dxp_dx['13'] * gcon['23'] ) \
            + dxp_dx['13'] * ( dxp_dx['10'] * gcon['03'] + dxp_dx['11'] * gcon['13'] + dxp_dx['12'] * gcon['23'] + dxp_dx['13'] * gcon['33'] )

    gcon12p = dxp_dx['10'] * ( dxp_dx['20'] * gcon['00'] + dxp_dx['21'] * gcon['01'] + dxp_dx['22'] * gcon['02'] + dxp_dx['23'] * gcon['03'] ) \
            + dxp_dx['11'] * ( dxp_dx['20'] * gcon['01'] + dxp_dx['21'] * gcon['11'] + dxp_dx['22'] * gcon['12'] + dxp_dx['23'] * gcon['13'] ) \
            + dxp_dx['12'] * ( dxp_dx['20'] * gcon['02'] + dxp_dx['21'] * gcon['12'] + dxp_dx['22'] * gcon['22'] + dxp_dx['23'] * gcon['23'] ) \
            + dxp_dx['13'] * ( dxp_dx['20'] * gcon['03'] + dxp_dx['21'] * gcon['13'] + dxp_dx['22'] * gcon['23'] + dxp_dx['23'] * gcon['33'] )

    gcon13p = dxp_dx['10'] * ( dxp_dx['30'] * gcon['00'] + dxp_dx['31'] * gcon['01'] + dxp_dx['32'] * gcon['02'] + dxp_dx['33'] * gcon['03'] ) \
            + dxp_dx['11'] * ( dxp_dx['30'] * gcon['01'] + dxp_dx['31'] * gcon['11'] + dxp_dx['32'] * gcon['12'] + dxp_dx['33'] * gcon['13'] ) \
            + dxp_dx['12'] * ( dxp_dx['30'] * gcon['02'] + dxp_dx['31'] * gcon['12'] + dxp_dx['32'] * gcon['22'] + dxp_dx['33'] * gcon['23'] ) \
            + dxp_dx['13'] * ( dxp_dx['30'] * gcon['03'] + dxp_dx['31'] * gcon['13'] + dxp_dx['32'] * gcon['23'] + dxp_dx['33'] * gcon['33'] )

    gcon22p = dxp_dx['20'] * ( dxp_dx['20'] * gcon['00'] + dxp_dx['21'] * gcon['01'] + dxp_dx['22'] * gcon['02'] + dxp_dx['23'] * gcon['03'] ) \
            + dxp_dx['21'] * ( dxp_dx['20'] * gcon['01'] + dxp_dx['21'] * gcon['11'] + dxp_dx['22'] * gcon['12'] + dxp_dx['23'] * gcon['13'] ) \
            + dxp_dx['22'] * ( dxp_dx['20'] * gcon['02'] + dxp_dx['21'] * gcon['12'] + dxp_dx['22'] * gcon['22'] + dxp_dx['23'] * gcon['23'] ) \
            + dxp_dx['23'] * ( dxp_dx['20'] * gcon['03'] + dxp_dx['21'] * gcon['13'] + dxp_dx['22'] * gcon['23'] + dxp_dx['23'] * gcon['33'] )

    gcon23p = dxp_dx['20'] * ( dxp_dx['30'] * gcon['00'] + dxp_dx['31'] * gcon['01'] + dxp_dx['32'] * gcon['02'] + dxp_dx['33'] * gcon['03'] ) \
            + dxp_dx['21'] * ( dxp_dx['30'] * gcon['01'] + dxp_dx['31'] * gcon['11'] + dxp_dx['32'] * gcon['12'] + dxp_dx['33'] * gcon['13'] ) \
            + dxp_dx['22'] * ( dxp_dx['30'] * gcon['02'] + dxp_dx['31'] * gcon['12'] + dxp_dx['32'] * gcon['22'] + dxp_dx['33'] * gcon['23'] ) \
            + dxp_dx['23'] * ( dxp_dx['30'] * gcon['03'] + dxp_dx['31'] * gcon['13'] + dxp_dx['32'] * gcon['23'] + dxp_dx['33'] * gcon['33'] )

    gcon33p = dxp_dx['30'] * ( dxp_dx['30'] * gcon['00'] + dxp_dx['31'] * gcon['01'] + dxp_dx['32'] * gcon['02'] + dxp_dx['33'] * gcon['03'] ) \
            + dxp_dx['31'] * ( dxp_dx['30'] * gcon['01'] + dxp_dx['31'] * gcon['11'] + dxp_dx['32'] * gcon['12'] + dxp_dx['33'] * gcon['13'] ) \
            + dxp_dx['32'] * ( dxp_dx['30'] * gcon['02'] + dxp_dx['31'] * gcon['12'] + dxp_dx['32'] * gcon['22'] + dxp_dx['33'] * gcon['23'] ) \
            + dxp_dx['33'] * ( dxp_dx['30'] * gcon['03'] + dxp_dx['31'] * gcon['13'] + dxp_dx['32'] * gcon['23'] + dxp_dx['33'] * gcon['33'] )

    gconp = { '00':gcon00p, '01':gcon01p, '02':gcon02p, '03':gcon03p,
              '11':gcon11p, '12':gcon12p, '13':gcon13p,
              '22':gcon22p, '23':gcon23p,
              '33':gcon33p }

    return gconp

def get_ginverse(gcov):
    gcov00 = gcov['00']; gcov01 = gcov['01']; gcov02 = gcov['02']; gcov03 = gcov['03']
    gcov11 = gcov['11']; gcov12 = gcov['12']; gcov13 = gcov['13']
    gcov22 = gcov['22']; gcov23 = gcov['23']
    gcov33 = gcov['33']

    gcon00 = (np.power(gcov13,2)*gcov22 - 2*gcov12*gcov13*gcov23 + np.power(gcov12,2)*gcov33 + gcov11*(np.power(gcov23,2) - gcov22*gcov33))/ (-(np.power(gcov02,2)*np.power(gcov13,2)) + gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(-np.power(gcov12,2) + gcov11*gcov22) + 2*gcov01*gcov02*gcov13*gcov23 - 2*gcov00*gcov12*gcov13*gcov23 - np.power(gcov01,2)*np.power(gcov23,2) + gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(gcov02*gcov12*gcov13 - gcov01*gcov13*gcov22 - gcov02*gcov11*gcov23 + gcov01*gcov12*gcov23) + (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon01 = (-(gcov03*gcov13*gcov22) + gcov03*gcov12*gcov23 + gcov02*gcov13*gcov23 - gcov01*np.power(gcov23,2) - gcov02*gcov12*gcov33 + gcov01*gcov22*gcov33)/(-(np.power(gcov02,2)*np.power(gcov13,2)) + gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(-np.power(gcov12,2) + gcov11*gcov22) + 2*gcov01*gcov02*gcov13*gcov23 - 2*gcov00*gcov12*gcov13*gcov23 - np.power(gcov01,2)*np.power(gcov23,2) + gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(gcov02*gcov12*gcov13 - gcov01*gcov13*gcov22 - gcov02*gcov11*gcov23 + gcov01*gcov12*gcov23) + (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon02 = (-(gcov03*gcov12*gcov13) + gcov02*np.power(gcov13,2) + gcov03*gcov11*gcov23 - gcov01*gcov13*gcov23 - gcov02*gcov11*gcov33 + gcov01*gcov12*gcov33)/(np.power(gcov02,2)*np.power(gcov13,2) - gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(np.power(gcov12,2) - gcov11*gcov22) - 2*gcov01*gcov02*gcov13*gcov23 + 2*gcov00*gcov12*gcov13*gcov23 + np.power(gcov01,2)*np.power(gcov23,2) - gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(-(gcov02*gcov12*gcov13) + gcov01*gcov13*gcov22 + gcov02*gcov11*gcov23 - gcov01*gcov12*gcov23) - (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon03 = (-(gcov02*gcov12*gcov13) + gcov01*gcov13*gcov22 + gcov03*(np.power(gcov12,2) - gcov11*gcov22) + gcov02*gcov11*gcov23 - gcov01*gcov12*gcov23)/(np.power(gcov02,2)*np.power(gcov13,2) - gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(np.power(gcov12,2) - gcov11*gcov22) - 2*gcov01*gcov02*gcov13*gcov23 + 2*gcov00*gcov12*gcov13*gcov23 + np.power(gcov01,2)*np.power(gcov23,2) - gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(-(gcov02*gcov12*gcov13) + gcov01*gcov13*gcov22 + gcov02*gcov11*gcov23 - gcov01*gcov12*gcov23) - (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon11 = (np.power(gcov03,2)*gcov22 - 2*gcov02*gcov03*gcov23 + np.power(gcov02,2)*gcov33 + gcov00*(np.power(gcov23,2) - gcov22*gcov33))/(-(np.power(gcov02,2)*np.power(gcov13,2)) + gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(-np.power(gcov12,2) + gcov11*gcov22) + 2*gcov01*gcov02*gcov13*gcov23 - 2*gcov00*gcov12*gcov13*gcov23 - np.power(gcov01,2)*np.power(gcov23,2) + gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(gcov02*gcov12*gcov13 - gcov01*gcov13*gcov22 - gcov02*gcov11*gcov23 + gcov01*gcov12*gcov23) + (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon12 = (np.power(gcov03,2)*gcov12 + gcov00*gcov13*gcov23 - gcov03*(gcov02*gcov13 + gcov01*gcov23) + gcov01*gcov02*gcov33 - gcov00*gcov12*gcov33)/(np.power(gcov02,2)*np.power(gcov13,2) - gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(np.power(gcov12,2) - gcov11*gcov22) - 2*gcov01*gcov02*gcov13*gcov23 + 2*gcov00*gcov12*gcov13*gcov23 + np.power(gcov01,2)*np.power(gcov23,2) - gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(-(gcov02*gcov12*gcov13) + gcov01*gcov13*gcov22 + gcov02*gcov11*gcov23 - gcov01*gcov12*gcov23) - (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon13 = (np.power(gcov02,2)*gcov13 + gcov01*gcov03*gcov22 - gcov00*gcov13*gcov22 + gcov00*gcov12*gcov23 - gcov02*(gcov03*gcov12 + gcov01*gcov23))/(np.power(gcov02,2)*np.power(gcov13,2) - gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(np.power(gcov12,2) - gcov11*gcov22) - 2*gcov01*gcov02*gcov13*gcov23 + 2*gcov00*gcov12*gcov13*gcov23 + np.power(gcov01,2)*np.power(gcov23,2) - gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(-(gcov02*gcov12*gcov13) + gcov01*gcov13*gcov22 + gcov02*gcov11*gcov23 - gcov01*gcov12*gcov23) - (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon22 = (np.power(gcov03,2)*gcov11 - 2*gcov01*gcov03*gcov13 + np.power(gcov01,2)*gcov33 + gcov00*(np.power(gcov13,2) - gcov11*gcov33))/(-(np.power(gcov02,2)*np.power(gcov13,2)) + gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(-np.power(gcov12,2) + gcov11*gcov22) + 2*gcov01*gcov02*gcov13*gcov23 - 2*gcov00*gcov12*gcov13*gcov23 - np.power(gcov01,2)*np.power(gcov23,2) + gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(gcov02*gcov12*gcov13 - gcov01*gcov13*gcov22 - gcov02*gcov11*gcov23 + gcov01*gcov12*gcov23) + (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon23 = (-(gcov02*gcov03*gcov11) + gcov01*gcov03*gcov12 + gcov01*gcov02*gcov13 - gcov00*gcov12*gcov13 - np.power(gcov01,2)*gcov23 + gcov00*gcov11*gcov23)/(-(np.power(gcov02,2)*np.power(gcov13,2)) + gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(-np.power(gcov12,2) + gcov11*gcov22) + 2*gcov01*gcov02*gcov13*gcov23 - 2*gcov00*gcov12*gcov13*gcov23 - np.power(gcov01,2)*np.power(gcov23,2) + gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(gcov02*gcov12*gcov13 - gcov01*gcov13*gcov22 - gcov02*gcov11*gcov23 + gcov01*gcov12*gcov23) + (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon33 = (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + np.power(gcov01,2)*gcov22 + gcov00*(np.power(gcov12,2) - gcov11*gcov22))/(-(np.power(gcov02,2)*np.power(gcov13,2)) + gcov00*np.power(gcov13,2)*gcov22 + np.power(gcov03,2)*(-np.power(gcov12,2) + gcov11*gcov22) + 2*gcov01*gcov02*gcov13*gcov23 - 2*gcov00*gcov12*gcov13*gcov23 - np.power(gcov01,2)*np.power(gcov23,2) + gcov00*gcov11*np.power(gcov23,2) + 2*gcov03*(gcov02*gcov12*gcov13 - gcov01*gcov13*gcov22 - gcov02*gcov11*gcov23 + gcov01*gcov12*gcov23) + (np.power(gcov02,2)*gcov11 - 2*gcov01*gcov02*gcov12 + gcov00*np.power(gcov12,2) + np.power(gcov01,2)*gcov22 - gcov00*gcov11*gcov22)*gcov33)

    gcon = { '00':gcon00, '01':gcon01, '02':gcon02, '03':gcon03,
             '11':gcon11, '12':gcon12, '13':gcon13,
             '22':gcon22, '23':gcon23,
             '33':gcon33 }
    return gcon
################################################################################
#          E N D   R A N K 2  C O N    T R A N S F O R M A T I O N             #
################################################################################


################################################################################
#         C O M P L E T E   4 - V E C T O R   T O   B H 1   F R A M E          #
################################################################################
def rank1con2BH1(trajfile, TTNZ, RRNZ, THNZ, PHNZ, UTT, URR, UTH, UPH, dx_dxp=None, SETPHASE=None, spinning=False):
    """
    Description
    -----------
    Transforms tensor with components UTT,URR,UTH,UPH wrt physical spherical coords to BH1 BL coords
    """
    if spinning:
        traj_columns = (0, 7)
    else:
        traj_columns = (0, 1)
    tbh_traj, m_bh1_traj = np.loadtxt(trajfile, usecols=traj_columns, unpack=True)
    fm_bh1 = interp1d(tbh_traj, m_bh1_traj, kind='linear')
    m_bh1 = fm_bh1(TTNZ)

    XNZ = RRNZ * np.sin(THNZ) * np.cos(PHNZ); YNZ = RRNZ * np.sin(THNZ) * np.sin(PHNZ); ZNZ = RRNZ * np.cos(THNZ)
    TIZ1 , XIZ1 , YIZ1 , ZIZ1  = coordsIZ_of_coordsNZ(trajfile, 1, TTNZ, XNZ, YNZ, ZNZ, SETPHASE=SETPHASE, spinning=spinning)
    TTBL1, RRBL1, THBL1, PHBL1 = coordsBL_of_coordsIZ( m_bh1, 0, TIZ1, XIZ1, YIZ1, ZIZ1 )
    # for i in range(0,PHBL1.shape[0]):
    #     for j in range(0,PHBL1.shape[1]):
    #         if( PHBL1[i,j] < 0.):
    #             PHBL1[i,j] = 2. * np.pi - np.abs(PHBL1[i,j])
    #PHBL1[PHBL1 < 0] = 2. * np.pi - np.abs(PHBL1[PHBL1 < 0])
    PHBL1[PHBL1 < 0] += 2. * np.pi

    dxc_dxs     = get_dxc_dxs(RRNZ, THNZ, PHNZ)
    dxIZ1_dxNZ  = get_dxIZ_dxNZ(trajfile, 1, TTNZ, XNZ, YNZ, ZNZ, SETPHASE=SETPHASE, spinning=spinning)
    dxBL1_dxIZ1 = get_dxBL_dxIZ(m_bh1, 0, TIZ1, XIZ1, YIZ1, ZIZ1 )

    #Transform
    if dx_dxp is not None:
        ucon0, ucon1, ucon2, ucon3 = transform_rank1con(dx_dxp, UTT, URR, UTH, UPH) #Physical Spherical NZ coords
    ucon0, ucon1, ucon2, ucon3 = transform_rank1con(dxc_dxs, UTT, URR, UTH, UPH) #Cartesian NZ coords
    ucon0, ucon1, ucon2, ucon3 = transform_rank1con(dxIZ1_dxNZ, ucon0, ucon1, ucon2, ucon3 ) #Cartesian IZ1 coords
    ucontt1, uconrr1, uconth1, uconph1 = transform_rank1con(dxBL1_dxIZ1, ucon0, ucon1, ucon2, ucon3) #BL1 coords

    return ucontt1, uconrr1, uconth1, uconph1
################################################################################
#         C O M P L E T E   4 - V E C T O R   T O   B H 2   F R A M E          #
################################################################################
def rank1con2BH2(trajfile, TTNZ, RRNZ, THNZ, PHNZ, UTT, URR, UTH, UPH, SETPHASE=None, spinning=False):
    """
    Description
    -----------
    Transforms tensor with components UTT, URR, UTH, UPH wrt physical spherical coords to BH2 BL coords
    """
    if spinning:
        tbh_traj, spin_bh2, m_bh2_traj = np.loadtxt(trajfile, usecols=(0, 6, 8), unpack=True)
    else:
        spin_bh2 = [0,]
        tbh_traj, m_bh2_traj = np.loadtxt(trajfile, usecols=(0, 2), unpack=True)
    fm_bh2 = interp1d(tbh_traj, m_bh2_traj, kind='linear')
    m_bh2 = fm_bh2(TTNZ)

    XNZ = RRNZ * np.sin(THNZ) * np.cos(PHNZ); YNZ = RRNZ * np.sin(THNZ) * np.sin(PHNZ); ZNZ = RRNZ * np.cos(THNZ)
    TIZ2 , XIZ2 , YIZ2 , ZIZ2  = coordsIZ_of_coordsNZ(trajfile, 2, TTNZ, XNZ, YNZ, ZNZ,SETPHASE=SETPHASE, spinning=spinning)
    TTBL2, RRBL2, THBL2, PHBL2 = coordsBL_of_coordsIZ(m_bh2, spin_bh2[0], TIZ2, XIZ2, YIZ2, ZIZ2)
    # for i in range(0,PHBL2.shape[0]):
    #     for j in range(0,PHBL2.shape[1]):
    #         if( PHBL2[i,j] < 0.):
    #             PHBL2[i,j] = 2. * np.pi - np.abs(PHBL2[i,j])
    #PHBL2[PHBL2 < 0] = 2. * np.pi - np.abs(PHBL2[PHBL2 < 0])
    PHBL2[PHBL2 < 0] += 2. * np.pi

    dxc_dxs     = get_dxc_dxs(RRNZ, THNZ, PHNZ)
    dxIZ2_dxNZ  = get_dxIZ_dxNZ(trajfile, 2, TTNZ, XNZ, YNZ, ZNZ,SETPHASE=SETPHASE, spinning=spinning)
    dxBL2_dxIZ2 = get_dxBL_dxIZ(m_bh2, 0, TIZ2, XIZ2, YIZ2, ZIZ2 )

    #Transform
    ucon0, ucon1, ucon2, ucon3 = transform_rank1con(dxc_dxs, UTT, URR, UTH, UPH) #Cartesian NZ coords
    ucon0, ucon1, ucon2, ucon3 = transform_rank1con(dxIZ2_dxNZ, ucon0, ucon1, ucon2, ucon3 ) #Cartesian IZ2 coords
    ucon1 *= -1.; ucon2 *= -1. #correct jacobian
    ucontt2, uconrr2, uconth2, uconph2 = transform_rank1con(dxBL2_dxIZ2, ucon0, ucon1, ucon2, ucon3) #BL2 coords

    return ucontt2, uconrr2, uconth2, uconph2

def gdet2BL(BH, trajfile, TTNZ, RRNZ, THNZ, PHNZ, dx_dxp, gdet, SETPHASE=None, spinning=False):
    XNZ = RRNZ * np.sin(THNZ) * np.cos(PHNZ)
    YNZ = RRNZ * np.sin(THNZ) * np.sin(PHNZ)
    ZNZ = RRNZ * np.cos(THNZ)

    #Find BH mass from trajfile
    if spinning:
        s_bh1_traj, s_bh2_traj, m_bh1_traj, m_bh2_traj = np.loadtxt(trajfile, usecols=(3,6,7,8), unpack=True)
    else:
        s_bh1_traj = [0.,]; s_bh2_traj = [0.,]
        m_bh1_traj, m_bh2_traj = np.loadtxt(trajfile, usecols=(1,2), unpack=True)
    m_bh1 = m_bh1_traj[0]
    s_bh1 = s_bh1_traj[0]
    m_bh2 = m_bh2_traj[0]
    s_bh2 = s_bh2_traj[0]
    if( BH == 1 or BH is 'primary'):
        mass = m_bh1
        spin = s_bh1
    else:
        mass = m_bh2
        spin = s_bh2

    #Calculate Cook-Scheel and BL coordinates
    TIZ, XIZ, YIZ, ZIZ = coordsIZ_of_coordsNZ(trajfile,BH,TTNZ,XNZ,YNZ,ZNZ,SETPHASE=SETPHASE)
    TTBL, RRBL, THBL, PHBL = coordsBL_of_coordsIZ(mass, spin, TIZ, XIZ, YIZ, ZIZ)
    #for i in range(0,PHBL.shape[0]):
    #    for j in range(0,PHBL.shape[1]):
    #        if( PHBL[i,j] < 0. ):
    #            PHBL[i,j] = 2. * np.pi - np.abs(PHBL[i,j])
    #PHBL[PHBL < 0.] = 2. * np.pi - np.abs(PHBL[PHBL < 0.]) #rescale from 0,2*pi
    PHBL[PHBL < 0] += 2. * np.pi

    #Get Jacobians
    dxc_dxs   = get_dxc_dxs( RRNZ, THNZ, PHNZ )
    dxIZ_dxNZ = get_dxIZ_dxNZ(trajfile, BH, TTNZ, XNZ, YNZ, ZNZ, SETPHASE=SETPHASE, spinning=spinning)
    dxBL_dxIZ = get_dxBL_dxIZ( mass, 0, TIZ, XIZ, YIZ, ZIZ )
    if( BH == 2 ):
        #Correction jacobian for BH 2 CS2PN Jacobian
        correct00 = np.zeros((np.shape(RRNZ))); correct01 = np.zeros((np.shape(RRNZ))); correct02 = np.zeros((np.shape(RRNZ))); correct03 = np.zeros((np.shape(RRNZ)))
        correct10 = np.zeros((np.shape(RRNZ))); correct11 = np.zeros((np.shape(RRNZ))); correct12 = np.zeros((np.shape(RRNZ))); correct13 = np.zeros((np.shape(RRNZ)))
        correct20 = np.zeros((np.shape(RRNZ))); correct21 = np.zeros((np.shape(RRNZ))); correct22 = np.zeros((np.shape(RRNZ))); correct23 = np.zeros((np.shape(RRNZ)))
        correct30 = np.zeros((np.shape(RRNZ))); correct31 = np.zeros((np.shape(RRNZ))); correct32 = np.zeros((np.shape(RRNZ))); correct33 = np.zeros((np.shape(RRNZ)))
        correct00.fill(1.); correct11.fill(-1.); correct22.fill(-1.); correct33.fill(1.)
        correct = { '00':correct00, '01':correct01, '02':correct02, '03':correct03,
                    '10':correct10, '11':correct11, '12':correct12, '13':correct13,
                    '20':correct20, '21':correct21, '22':correct22, '23':correct23,
                    '30':correct30, '31':correct31, '32':correct32, '33':correct33 }
    #Transform gdet
    gdet = -gdet * gdet
    gdet = transform_tensordensity(0,-2, dx_dxp, gdet) #physical coords
    gdet = transform_tensordensity(0,-2, dxc_dxs, gdet) #cartesian coords
    gIZ  = transform_tensordensity(0,-2, dxIZ_dxNZ , gdet)
    if( BH == 2 ):
        gIZ = transform_tensordensity(0,-2,correct,gIZ)
    gBL  = transform_tensordensity(0,-2, dxBL_dxIZ, gIZ)
    gdet = np.sqrt(-gBL)

    return gdet

################################################################################
# TRANSFORM THE METRIC TO BL COORDS FROM NUMERICAL COORDS
################################################################################
def metric2BL(BH, trajfile, TTNZ, RRNZ, THNZ, PHNZ, dx_dxp, metric, SETPHASE=None, spinning=False):

    #The jacobian for CS2PN is only known one way. We need to transform gcon and invert to get gcov
    gdetBL = gdet2BL(BH, trajfile, TTNZ, RRNZ, THNZ, PHNZ, dx_dxp, metric['gdet'], SETPHASE=SETPHASE, spinning=spinning)

    #Let us now calculate all necessary coordinates and jacobians to transform gcon

    if spinning:
        tbh_traj, s_bh1_traj, s_bh2_traj, m_bh1_traj, m_bh2_traj = np.loadtxt(trajfile, usecols=(0,7,8), unpack=True)
    else:
        s_bh1_traj = [0.,]; s_bh2_traj = [0.,]
        tbh_traj, m_bh1_traj, m_bh2_traj = np.loadtxt(trajfile, usecols=(0,1,2), unpack=True)
    if( BH == 1 or BH is 'primary'):
        fm_bh1 = interp1d(tbh_traj, m_bh1_traj, kind='linear')
        mass = fm_bh1(TTNZ)
    else:
        fm_bh2 = interp1d(tbh_traj, m_bh2_traj, kind='linear')
        mass = fm_bh2(TTNZ)

    XNZ = RRNZ * np.sin(THNZ) * np.cos(PHNZ); YNZ = RRNZ * np.sin(THNZ) * np.sin(PHNZ); ZNZ = RRNZ * np.cos(THNZ)
    TIZ , XIZ , YIZ , ZIZ  = coordsIZ_of_coordsNZ(trajfile, BH, TTNZ, XNZ, YNZ, ZNZ, SETPHASE=SETPHASE, spinning=spinning)
    TTBL, RRBL, THBL, PHBL = coordsBL_of_coordsIZ( mass, 0, TIZ, XIZ, YIZ, ZIZ )
    #PHBL1[PHBL1 < 0] += 2. * np.pi

    dxc_dxs   = get_dxc_dxs(RRNZ, THNZ, PHNZ)
    dxIZ_dxNZ = get_dxIZ_dxNZ(trajfile, BH, TTNZ, XNZ, YNZ, ZNZ, SETPHASE=SETPHASE, spinning=spinning)
    dxBL_dxIZ = get_dxBL_dxIZ(mass, 0, TIZ, XIZ, YIZ, ZIZ)

    #Transform
    gcon = { '00':metric['gcon00'], '01':metric['gcon01'], '02':metric['gcon02'], '03':metric['gcon03'],
             '11':metric['gcon11'], '12':metric['gcon12'], '13':metric['gcon13'],
             '22':metric['gcon22'], '23':metric['gcon23'],
             '33':metric['gcon33'] }
    gcon = transform_gcon( dx_dxp, gcon ) #Spherical PN coords ( physical coords )
    gcon = transform_gcon( dxc_dxs, gcon) #Cartesian PN coords
    gcon = transform_gcon( dxIZ_dxNZ, gcon ) #Cartesian CS coords
    gcon = transform_gcon( dxBL_dxIZ, gcon ) #BL coordinates

    gcov = get_ginverse(gcon) #covariant metric in BL coordinates

    metricp = { 'gcov00':gcov['00'], 'gcov01':gcov['01'], 'gcov02':gcov['02'], 'gcov03':gcov['03'],
                'gcov11':gcov['11'], 'gcov12':gcov['12'], 'gcov13':gcov['13'],
                'gcov22':gcov['22'], 'gcov23':gcov['23'],
                'gcov33':gcov['33'],
                'gcon00':gcon['00'], 'gcon01':gcon['01'], 'gcon02':gcon['02'], 'gcon03':gcon['03'],
                'gcon11':gcon['11'], 'gcon12':gcon['12'], 'gcon13':gcon['13'],
                'gcon22':gcon['22'], 'gcon23':gcon['23'],
                'gcon33':gcon['33'],
                'gdet':gdetBL }
    return metricp
