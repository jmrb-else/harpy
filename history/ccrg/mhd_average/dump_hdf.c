
#include "defs.h"

#define USE_CHUNKS (1) 
/* Rank of hdf5 gridfunctions : */ 
#define HDF_RANK       (3)
#define HDF_GDUMP_RANK (3)
//#define N_HDF_FUNCS (NPRIM + 3)
//static hsize_t   offset[HDF_RANK], count[HDF_RANK], h5_memdims[HDF_RANK], h5_filedims[HDF_RANK];

/* global persistent variables : */
/* h5_dims is the same for all n_spatial_dims since we always pass a 3d array to H5Dwrite() */
static hsize_t   h5_memdims[HDF_RANK], h5_filedims[HDF_RANK];
static hsize_t   count[HDF_RANK];
static hsize_t   offset[HDF_RANK];



void     setup_hdf5(void); 
void     myH5_write_gfunc_orig( hid_t loc_id, char *name, double *value ) ;
void     myH5_write_int_gfunc_orig( hid_t loc_id, char *name, int *value );
//hid_t    myH5_Fopen(char *name);
void     myH5_read_scalar2_orig( hid_t loc_id, char *name, hid_t type_id, void *value ) ;
void     myH5_read_gfunc_orig(hid_t loc_id, char *name, double *value ) ;
hid_t    myH5_Fcreate_orig(char *name, int overwrite);




/*******************************************************************************************/
/*******************************************************************************************
 myH5_Fopen():
 --------------------------
   -- wrapper for opening hdf5 files; 
   -- handles differences between MPI and serial calls; 
*******************************************************************************************/
hid_t myH5_Fopen(char *name, int RW)
{
  hid_t prop_id, file_id; 

  prop_id = H5Pcreate(H5P_FILE_ACCESS);

  /* Currently if we open a file we are modifying it, and this needs to be done with MPI-IO 
     when using MPI */
#if( USE_MPI_IO ) 
  H5Pset_fapl_mpio(prop_id, MPI_COMM_WORLD, MPI_INFO_NULL);
#endif
  
  if( RW ) { file_id = H5Fopen(name, H5F_ACC_RDWR  , prop_id); }
  else     { file_id = H5Fopen(name, H5F_ACC_RDONLY, prop_id); }
  if( file_id  < 0 ) { 
    fprintf(stderr,"Cannot open file %s \n", name);
    //fflush(stderr);     fail(FAIL_HDF,0);
  }

  H5Pclose(prop_id);

  return( file_id ); 

}



/*******************************************************************************************/
/*******************************************************************************************
 myH5_read_gfunc_orig(): 
 ---------------------------
   -- driver routine for reading a gridfunction that is simple dataset under 
      given group/file id "loc_id";  
   -- the dataset is identified by the name "name", and the data 
      is stored under "value" of type "type_id"; the memory space to read i
   -- responsible for assuming a dataspace, opening dataset, reading dataset,
      and closing the dataset and the dataspace; 
   -- assumes same space layout as myH5_write_gfunc();
*******************************************************************************************/
void myH5_read_gfunc_orig(hid_t loc_id, char *name, double *value ) 
{
  hid_t   memspace_id, filespace_id, dataset_id, prop_id;

  h5_memdims[0] = N1; h5_filedims[0] = N1;
  h5_memdims[1] = N2; h5_filedims[1] = N2;
  h5_memdims[2] = N3; h5_filedims[2] = N3;
  offset[0] = offset[1] = offset[2] = 0;
  count[0] = count[1] = count[2] = 1;

  /************************************************************************************
     Open the data set: 
   ************************************************************************************/
  /* Open existing dataset :  */
  dataset_id = H5Dopen(loc_id, name); 

  /************************************************************************************
    Select the hyperslab, or section of space, to which our gridfunction will be written
  ************************************************************************************/
  filespace_id = H5Dget_space(dataset_id);  

#if( USE_CHUNKS ) 
  H5Sselect_hyperslab(filespace_id, H5S_SELECT_SET, offset, NULL, count, h5_memdims);
#else 
  H5Sselect_hyperslab(filespace_id, H5S_SELECT_SET, offset, NULL, h5_memdims, NULL);
#endif 
  
  memspace_id  = H5Screate_simple(HDF_RANK, h5_memdims , NULL); 
  
  /************************************************************************************
    Write the hyperslab to the dataset : 
  ************************************************************************************/
  /* Setup the properties of the write based upon type of run : */
  prop_id = H5Pcreate(H5P_DATASET_XFER);

#if( USE_MPI_IO ) 
  H5Pset_dxpl_mpio(prop_id, H5FD_MPIO_COLLECTIVE);
#endif

  /* Write the dataset using defined dataspace and properties. */
  H5Dread(dataset_id, H5T_NATIVE_DOUBLE, memspace_id, filespace_id, prop_id, value);

  /* Close the open object handles : */
  H5Dclose(dataset_id);
  H5Sclose(filespace_id);
  H5Sclose(memspace_id);
  H5Pclose(prop_id);

  return;
}

/*******************************************************************************************/
/*******************************************************************************************
 myH5_write_gfunc_orig(): 
 ---------------------------
   -- original version of the routine to write standard gfuncs; 
   -- driver routine for writing a grid function within a given group/object
      "loc_id"; 
   -- the grid function is a simple dataspace of dimensions given by the 
      macros "N1*N2*N3";
   -- H5T_NATIVE_DOUBLE  is the assumed datatype of all grid functions ;
   -- responsible for creating dataspace, creating dataset, writing dataset,
      and closing the dataset and the dataspace; 
   -- handles differences between MPI and serial calls; 
      -- assumes that all processes write gridfunctions in the typical domain 
         decomposition pattern;
*******************************************************************************************/
void myH5_write_gfunc_orig( hid_t loc_id, char *name, double *value )
{
  hid_t   memspace_id, filespace_id, dataset_id, prop_id;  

  h5_memdims[0] = N1; h5_filedims[0] = N1;
  h5_memdims[1] = N2; h5_filedims[1] = N2;
  h5_memdims[2] = N3; h5_filedims[2] = N3;
  offset[0] = offset[1] = offset[2] = 0;
  count[0] = count[1] = count[2] = 1;

  /************************************************************************************
     Create the data set: 
   ************************************************************************************/
  /* Create the data space for the dataset, always using 3 dimensions since we want 
     filespace to match memory space : */
  filespace_id = H5Screate_simple(HDF_RANK, h5_filedims, NULL);
  memspace_id  = H5Screate_simple(HDF_RANK, h5_memdims , NULL);

  /* Set the property list to for creation */
  prop_id = H5Pcreate(H5P_DATASET_CREATE);

#if( USE_CHUNKS )
  /* Set properties to use chunks, set chunk's extent : */
  H5Pset_chunk(prop_id, HDF_RANK, h5_memdims);
#endif

  /* Create a dataset assuming double var type */
  dataset_id = H5Dcreate(loc_id, name, H5T_NATIVE_DOUBLE, filespace_id, prop_id);
  H5Pclose(prop_id);
  H5Sclose(filespace_id);


  /************************************************************************************
    Select the hyperslab, or section of space, to which our gridfunction will be written
       -- I am not sure why we need to close and then reget the dataset's dataspace, 
          but that is what they usually do in the example programs in the hdf5 tutorial;
  ************************************************************************************/
  filespace_id = H5Dget_space(dataset_id);

#if( USE_CHUNKS )
  H5Sselect_hyperslab(filespace_id, H5S_SELECT_SET, offset, NULL, count, h5_memdims);
#else
  H5Sselect_hyperslab(filespace_id, H5S_SELECT_SET, offset, NULL, h5_memdims, NULL);
#endif
  
  
  /************************************************************************************
    Write the hyperslab to the dataset :
  ************************************************************************************/
  /* Setup the properties of the write based upon type of run : */
  prop_id = H5Pcreate(H5P_DATASET_XFER);

  /* Write the dataset using defined dataspace and properties. */
  H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, memspace_id, filespace_id, prop_id, value);

  /* Close the open object handles : */
  H5Dclose(dataset_id);
  H5Sclose(filespace_id);
  H5Sclose(memspace_id);
  H5Pclose(prop_id);


  return;
}

hid_t myH5_Fcreate(char *name, int overwrite )
{
  hid_t prop_id, file_id; 

  prop_id = H5Pcreate(H5P_FILE_ACCESS);

  
  if( overwrite ) { file_id = H5Fcreate(name, H5F_ACC_TRUNC,  H5P_DEFAULT, prop_id); }
  else            { file_id = H5Fcreate(name, H5F_ACC_EXCL ,  H5P_DEFAULT, prop_id); }

  if( file_id  < 0 ) { 
    fprintf(stderr,"Cannot create file %s \n", name);
  }

  H5Pclose(prop_id);

  return( file_id ); 
}





/*******************************************************************************************/
/*******************************************************************************************
 myH5_read_scalar2_orig(): 
 ---------------------------
   -- different than myH5_read_scalar() in that it selects just the first element 
      of a potentially larger dataset on disk than a scalar;
   -- driver routine for reading a scalar under given group/file id "loc_id";  
      the scalar is identified by the name "name", and the scalar
      is a single value "value" of type "type_id"; 
   -- responsible for opening dataset, opening dataspace, reading dataset,
      and closing the dataset and the dataspace; 
   -- handles differences between serial and MPI calls; 
        -- unlike myH5_write_scalar(), all processes read the scalar data (this eliminates
           a broadcast of the data);
*******************************************************************************************/
void myH5_read_scalar2_orig( hid_t loc_id, char *name, hid_t type_id, void *value ) 
{
  hid_t   filespace_id, memspace_id, dataset_id, prop_id;
  hsize_t dims[]={1}, scal_count[1]={1};
  hsize_t scal_offset[1]={0};

  /* Open scalar , which is like a small dataset  */
  dataset_id = H5Dopen(loc_id, name); 
  if( dataset_id < 0 ) { 
    fprintf(stderr,"%s(): Cannot open dataset named  %s \n",__func__, name);
    //fflush(stderr);     fail(FAIL_HDF,0);
  }

  /* Create memory and file spaces, removing extents if we are not the master node : */
  memspace_id  = H5Screate_simple(1, dims, NULL); 
  filespace_id = H5Dget_space(dataset_id);

#if( USE_CHUNKS ) 
  H5Sselect_hyperslab(filespace_id, H5S_SELECT_SET, scal_offset, NULL, scal_count, dims);
#else 
  H5Sselect_hyperslab(filespace_id, H5S_SELECT_SET, scal_offset, NULL, dims, NULL);
#endif 


  /* Write the dataset using defined dataspace and default properties. */
  prop_id = H5Pcreate(H5P_DATASET_XFER);

  /* Currently only configured to read a single restart file for MPI runs: */
#if( USE_MPI_IO ) 
  H5Pset_dxpl_mpio(prop_id, H5FD_MPIO_COLLECTIVE);
#endif 

  H5Dread(dataset_id, type_id, memspace_id, filespace_id, prop_id, value);

  /* Close all open object handles : */
  H5Dclose(dataset_id);	 
  H5Sclose(filespace_id);	 
  H5Sclose(memspace_id);	 
  H5Pclose(prop_id);

  return;
}
