#include "defs.h"


/* Read in harm data from hdf_name */
void get_source_gfuncs(char *hdf_name, char f_hdf_name[][100], double ****gfuncs)
{
  int ind,i,j,k,l,mu,nu;
  double tloc;
  hid_t file_id, header_id, grid_id;
  //double *func_tmp;
  //double *rho_tmp, *uu_tmp;
  //double *v1_tmp, *v2_tmp, *v3_tmp;
  //double *B1_tmp, *B2_tmp, *B3_tmp;
  //double *bsq_tmp, *divb_tmp, *gamma_tmp;
  double *dx_dxp10, *dx_dxp11, *dx_dxp12, *dx_dxp13;
  double *dx_dxp20, *dx_dxp21, *dx_dxp22, *dx_dxp23;
  double *dx_dxp30, *dx_dxp31, *dx_dxp32, *dx_dxp33;
  double dx_dxp[NDIM][NDIM];
  double vcon[NDIM], Bcon[NDIM];

  file_id = myH5_Fopen(hdf_name,0);


  /* Read local time */
  header_id = H5Gopen(file_id, "Header");
  if( header_id  < 0 ) { fprintf(stderr,"restart_read_hdf5(): Cannot open Header group in file %s \n", hdf_name); }

  /* Create a header subgroup that contains the grid parameters: */
  grid_id = H5Gopen(header_id, "Grid");
  if( grid_id  < 0 ) { fprintf(stderr,"restart_read_hdf5(): Cannot open Grid group in file %s \n", hdf_name); }
  myH5_read_scalar2_orig(grid_id, "t"   , H5T_NATIVE_DOUBLE, &tloc);
  printf("Reading %s time %e \n",hdf_name,tloc);


  //Allocate arrays
  //printf("allocate prims \n");
  /* ALLOC_ARRAY(rho_tmp , NCELLS); */
  /* ALLOC_ARRAY(uu_tmp  , NCELLS); */
  /* ALLOC_ARRAY(v1_tmp  , NCELLS); */
  /* ALLOC_ARRAY(v2_tmp  , NCELLS); */
  /* ALLOC_ARRAY(v3_tmp  , NCELLS); */
  /* ALLOC_ARRAY(B1_tmp  , NCELLS); */
  /* ALLOC_ARRAY(B2_tmp  , NCELLS); */
  /* ALLOC_ARRAY(B3_tmp  , NCELLS); */
  /* ALLOC_ARRAY(bsq_tmp , NCELLS); */
  /* ALLOC_ARRAY(divb_tmp, NCELLS); */
  /* ALLOC_ARRAY(gamma_tmp,NCELLS); */
  //ALLOC_ARRAY(func_tmp,NCELLS);
  //printf("allocate jacobians \n");
  ALLOC_ARRAY(dx_dxp10, NCELLS);
  ALLOC_ARRAY(dx_dxp11, NCELLS);
  ALLOC_ARRAY(dx_dxp12, NCELLS);
  ALLOC_ARRAY(dx_dxp13, NCELLS);
  ALLOC_ARRAY(dx_dxp20, NCELLS);
  ALLOC_ARRAY(dx_dxp21, NCELLS);
  ALLOC_ARRAY(dx_dxp22, NCELLS);
  ALLOC_ARRAY(dx_dxp23, NCELLS);
  ALLOC_ARRAY(dx_dxp30, NCELLS);
  ALLOC_ARRAY(dx_dxp31, NCELLS);
  ALLOC_ARRAY(dx_dxp32, NCELLS);
  ALLOC_ARRAY(dx_dxp33, NCELLS);


  printf("read coords\n");
  /* Read source coords */

  printf("read gfuncs\n");  
  GLOOP {
    printf("%d %s\n",l,f_hdf_name[l]);
    //myH5_read_gfunc_orig(file_id,f_hdf_name[l],func_tmp);
    myH5_read_gfunc_orig(file_id,f_hdf_name[l],vals);
    ind = 0;
    LOOP {
      //gfuncs[i][j][k][l] = func_tmp[ind];
      gfuncs[i][j][k][l] = vals[ind];
      ind++;
    }    
  }
  /* Read gfuncs */
  /* myH5_read_gfunc_orig(file_id, "/rho",rho_tmp); */
  /* myH5_read_gfunc_orig(file_id, "/uu", uu_tmp); */
  /* myH5_read_gfunc_orig(file_id, "/v1", v1_tmp); */
  /* myH5_read_gfunc_orig(file_id, "/v2", v2_tmp); */
  /* myH5_read_gfunc_orig(file_id, "/v3", v3_tmp); */
  /* myH5_read_gfunc_orig(file_id, "/B1", B1_tmp); */
  /* myH5_read_gfunc_orig(file_id, "/B2", B2_tmp); */
  /* myH5_read_gfunc_orig(file_id, "/B3", B3_tmp); */
  /* myH5_read_gfunc_orig(file_id, "/bsq" , bsq_tmp ); */
  /* myH5_read_gfunc_orig(file_id, "/divb" , divb_tmp ); */
  /* myH5_read_gfunc_orig(file_id, "/gamma" , gamma_tmp ); */

#if( !DUMP_NUMERIC )  
  printf("read jacobians\n");
  /* Jacobians to physical coords */
  myH5_read_gfunc_orig(file_id, "dx_dxp10", dx_dxp10);
  myH5_read_gfunc_orig(file_id, "dx_dxp11", dx_dxp11);
  myH5_read_gfunc_orig(file_id, "dx_dxp12", dx_dxp12);
  myH5_read_gfunc_orig(file_id, "dx_dxp13", dx_dxp13);
  myH5_read_gfunc_orig(file_id, "dx_dxp20", dx_dxp20);
  myH5_read_gfunc_orig(file_id, "dx_dxp21", dx_dxp21);
  myH5_read_gfunc_orig(file_id, "dx_dxp22", dx_dxp22);
  myH5_read_gfunc_orig(file_id, "dx_dxp23", dx_dxp23);
  myH5_read_gfunc_orig(file_id, "dx_dxp30", dx_dxp30);
  myH5_read_gfunc_orig(file_id, "dx_dxp31", dx_dxp31);
  myH5_read_gfunc_orig(file_id, "dx_dxp32", dx_dxp32);
  myH5_read_gfunc_orig(file_id, "dx_dxp33", dx_dxp33);
  
  ind = 0;
  printf("loop over grid and calculate gfuncs(r,th,ph) \n");
  LOOP {
    for(mu=0;mu<NDIM;mu++) for(nu=0;nu<NDIM;nu++) { dx_dxp[mu][nu] = 0.; }
    dx_dxp[0][0] = 1.;
    dx_dxp[1][0] = dx_dxp10[ind];
    dx_dxp[1][1] = dx_dxp11[ind];
    dx_dxp[1][2] = dx_dxp12[ind];
    dx_dxp[1][3] = dx_dxp13[ind];
    dx_dxp[2][0] = dx_dxp20[ind];
    dx_dxp[2][1] = dx_dxp21[ind];
    dx_dxp[2][2] = dx_dxp22[ind];
    dx_dxp[2][3] = dx_dxp23[ind];
    dx_dxp[3][0] = dx_dxp30[ind];
    dx_dxp[3][1] = dx_dxp31[ind];
    dx_dxp[3][2] = dx_dxp32[ind];
    dx_dxp[3][3] = dx_dxp33[ind];

    vcon[0] = 0. ;
    vcon[1] = gfuncs[i][j][k][U1];
    vcon[2] = gfuncs[i][j][k][U2];
    vcon[3] = gfuncs[i][j][k][U3];
    Bcon[0] = 0.;
    Bcon[1] = gfuncs[i][j][k][B1];
    Bcon[2] = gfuncs[i][j][k][B2];
    Bcon[3] = gfuncs[i][j][k][B3];
    
    transform_rank1con2(dx_dxp,vcon);
    transform_rank1con2(dx_dxp,Bcon);


    gfuncs[i][j][k][U1] = vcon[1];
    gfuncs[i][j][k][U2] = vcon[2];
    gfuncs[i][j][k][U3] = vcon[3];
    gfuncs[i][j][k][B1] = Bcon[1];
    gfuncs[i][j][k][B2] = Bcon[2];
    gfuncs[i][j][k][B3] = Bcon[3];

    ind++;
  }
#endif //!DUMP_NUMERIC

  /* Free up the allocated memory */
  //FREE(func_tmp);
  FREE(dx_dxp10);
  FREE(dx_dxp11);
  FREE(dx_dxp12);
  FREE(dx_dxp13);
  FREE(dx_dxp20);
  FREE(dx_dxp21);
  FREE(dx_dxp22);
  FREE(dx_dxp23);
  FREE(dx_dxp30);
  FREE(dx_dxp31);
  FREE(dx_dxp32);
  FREE(dx_dxp33);

 
  return;
}

/* Read in harm data from hdf_name */
void get_rotated_coords(char *hdf_name, double ***x1src, double ***x2src, double ***x3src)
{
  int ind,i,j,k;
  int roll_index;
  double tloc,dxp3;
  double Norb, phase_shift;
  hid_t file_id, header_id, grid_id;

  double xspher[NDIM];
  double *x1_tmp, *x2_tmp, *x3_tmp;
  double *rolling_array;
  struct of_bbh_traj bbh_traj;

  ALLOC_ARRAY(rolling_array,N3);
  ALLOC_ARRAY(x1_tmp  , NCELLS);
  ALLOC_ARRAY(x2_tmp  , NCELLS);
  ALLOC_ARRAY(x3_tmp  , NCELLS);

  file_id = myH5_Fopen(hdf_name,0);

  /* Read local time and dxp3 */
  header_id = H5Gopen(file_id, "Header");
  grid_id = H5Gopen(header_id, "Grid");
  myH5_read_scalar2_orig(grid_id, "t"   , H5T_NATIVE_DOUBLE, &tloc);
  myH5_read_scalar2_orig(grid_id, "dx3" , H5T_NATIVE_DOUBLE, &dxp3);
  printf("Rotating coords back to %s time %e \n",hdf_name,tloc);

  /* Set the BBH traj data to corotate */
  set_bbh_traj_data(tloc, &(bbh_traj));
  Norb = floor( 0.5 * bbh_traj.phi / M_PI );
  phase_shift = bbh_traj.phi - (2. * M_PI * Norb);

  /* Calculate index to roll arrays by in phi-index */
  roll_index = floor( phase_shift / (2. * M_PI * dxp3 ) - 0. + 0.5 );



  /* Read coords */
  myH5_read_gfunc_orig(file_id, "/x1", x1_tmp);
  myH5_read_gfunc_orig(file_id, "/x2", x2_tmp);
  myH5_read_gfunc_orig(file_id, "/x3", x3_tmp);
  
  ind = 0;
  //printf("loop over grid and calculate rotated coords (r,th,ph) \n");
  LOOP {
    xspher[1] = x1_tmp[ind];
    xspher[2] = x2_tmp[ind];
    xspher[3] = x3_tmp[ind] - phase_shift;
    if(xspher[PH] < 0.) { xspher[PH] += 2. * M_PI; }

    x1src[i][j][k] = xspher[1];
    x2src[i][j][k] = xspher[2];
    x3src[i][j][k] = xspher[3];

    ind++;
  }

  /* Roll the arrays */
  for(i=0;i<N1;i++) {
    for(j=0;j<N2;j++) {
      //roll x1
      for(k=0;k<N3;k++) { rolling_array[k] = x1src[i][j][k]; }
      roll_array(roll_index, N3, rolling_array); //roll the array
      for(k=0;k<N3;k++) { x1src[i][j][k] = rolling_array[k]; }
      //roll x2
      for(k=0;k<N3;k++) { rolling_array[k] = x2src[i][j][k]; }
      roll_array(roll_index, N3, rolling_array); //roll the array
      for(k=0;k<N3;k++) { x2src[i][j][k] = rolling_array[k]; }
      //roll x3
      for(k=0;k<N3;k++) { rolling_array[k] = x3src[i][j][k]; }
      roll_array(roll_index, N3, rolling_array); //roll the array
      for(k=0;k<N3;k++) { x3src[i][j][k] = rolling_array[k]; }
    }
  }

  /* Free up the allocated memory */
  FREE(rolling_array);
  FREE(x1_tmp );
  FREE(x2_tmp );
  FREE(x3_tmp );

 
  return;
}

/* Read in harm data from hdf_name */
void get_coords(char *hdf_name, double ***x1src, double ***x2src, double ***x3src)
{
  int ind,i,j,k;
  hid_t file_id;

  double *x1_tmp, *x2_tmp, *x3_tmp;


  ALLOC_ARRAY(x1_tmp  , NCELLS);
  ALLOC_ARRAY(x2_tmp  , NCELLS);
  ALLOC_ARRAY(x3_tmp  , NCELLS);

  file_id = myH5_Fopen(hdf_name,0);


  /* Read coords */
  myH5_read_gfunc_orig(file_id, "/x1", x1_tmp);
  myH5_read_gfunc_orig(file_id, "/x2", x2_tmp);
  myH5_read_gfunc_orig(file_id, "/x3", x3_tmp);
  
  ind = 0;
  //printf("loop over grid and calculate rotated coords (r,th,ph) \n");
  LOOP {
    x1src[i][j][k] = x1_tmp[ind];
    x2src[i][j][k] = x2_tmp[ind];
    x3src[i][j][k] = x3_tmp[ind];

    ind++;
  }


  /* Free up the allocated memory */
  FREE(x1_tmp );
  FREE(x2_tmp );
  FREE(x3_tmp );

 
  return;
}


/* Read in harm data from hdf_name */
void get_rotated_metric(int ROTATE, char *hdf_name, char metric_func_names[][100], double ****geometries)
{
  int ind,i,j,k,l,mu,nu;
  int roll_index;
  double tloc,dxp3;
  double Norb, phase_shift;
  double throw_away[NDIM][NDIM];
  hid_t file_id, header_id, grid_id;

  double *rolling_array, *func_tmp;
  double *dx_dxp10, *dx_dxp11, *dx_dxp12, *dx_dxp13;
  double *dx_dxp20, *dx_dxp21, *dx_dxp22, *dx_dxp23;
  double *dx_dxp30, *dx_dxp31, *dx_dxp32, *dx_dxp33;
  double dx_dxp[NDIM][NDIM];

  struct of_bbh_traj bbh_traj;
  struct of_geom geom_loc;


  ALLOC_ARRAY(rolling_array,N3);
  ALLOC_ARRAY(func_tmp , NCELLS);
  ALLOC_ARRAY(dx_dxp10, NCELLS);
  ALLOC_ARRAY(dx_dxp11, NCELLS);
  ALLOC_ARRAY(dx_dxp12, NCELLS);
  ALLOC_ARRAY(dx_dxp13, NCELLS);
  ALLOC_ARRAY(dx_dxp20, NCELLS);
  ALLOC_ARRAY(dx_dxp21, NCELLS);
  ALLOC_ARRAY(dx_dxp22, NCELLS);
  ALLOC_ARRAY(dx_dxp23, NCELLS);
  ALLOC_ARRAY(dx_dxp30, NCELLS);
  ALLOC_ARRAY(dx_dxp31, NCELLS);
  ALLOC_ARRAY(dx_dxp32, NCELLS);
  ALLOC_ARRAY(dx_dxp33, NCELLS);



  file_id = myH5_Fopen(hdf_name,0);


  GEOM_LOOP {
    printf("%d %s\n",l,metric_func_names[l]);
    myH5_read_gfunc_orig(file_id, metric_func_names[l], func_tmp);
    ind = 0;
    LOOP { 
      geometries[i][j][k][l] = func_tmp[ind]; 
      ind++; 
    } //NEED TO TRANFORM dennis
  }

#if( !DUMP_NUMERIC )
  /* Transform */
  printf("read jacobians\n");
  /* Jacobians to physical coords */
  myH5_read_gfunc_orig(file_id, "dx_dxp10", dx_dxp10);
  myH5_read_gfunc_orig(file_id, "dx_dxp11", dx_dxp11);
  myH5_read_gfunc_orig(file_id, "dx_dxp12", dx_dxp12);
  myH5_read_gfunc_orig(file_id, "dx_dxp13", dx_dxp13);
  myH5_read_gfunc_orig(file_id, "dx_dxp20", dx_dxp20);
  myH5_read_gfunc_orig(file_id, "dx_dxp21", dx_dxp21);
  myH5_read_gfunc_orig(file_id, "dx_dxp22", dx_dxp22);
  myH5_read_gfunc_orig(file_id, "dx_dxp23", dx_dxp23);
  myH5_read_gfunc_orig(file_id, "dx_dxp30", dx_dxp30);
  myH5_read_gfunc_orig(file_id, "dx_dxp31", dx_dxp31);
  myH5_read_gfunc_orig(file_id, "dx_dxp32", dx_dxp32);
  myH5_read_gfunc_orig(file_id, "dx_dxp33", dx_dxp33);
  
  ind = 0;
  printf("loop over grid and calculate gfuncs(r,th,ph) \n");
  LOOP {
    set_geom_struct(geometries[i][j][k],&geom_loc);
    for(mu=0;mu<NDIM;mu++) for(nu=0;nu<NDIM;nu++) { dx_dxp[mu][nu] = 0.; }
    dx_dxp[0][0] = 1.;
    dx_dxp[1][0] = dx_dxp10[ind];
    dx_dxp[1][1] = dx_dxp11[ind];
    dx_dxp[1][2] = dx_dxp12[ind];
    dx_dxp[1][3] = dx_dxp13[ind];
    dx_dxp[2][0] = dx_dxp20[ind];
    dx_dxp[2][1] = dx_dxp21[ind];
    dx_dxp[2][2] = dx_dxp22[ind];
    dx_dxp[2][3] = dx_dxp23[ind];
    dx_dxp[3][0] = dx_dxp30[ind];
    dx_dxp[3][1] = dx_dxp31[ind];
    dx_dxp[3][2] = dx_dxp32[ind];
    dx_dxp[3][3] = dx_dxp33[ind];

    transform_rank2con2(dx_dxp,geom_loc.gcon);
    invert_matrix2(geom_loc.gcon, geom_loc.gcov, &geom_loc.gdet);
    invert_matrix2(geom_loc.gcov, throw_away, &geom_loc.gdet);


    /* Store back in geometries */
    geometries[i][j][k][0] = geom_loc.gcov[0][0];
    geometries[i][j][k][1] = geom_loc.gcov[0][1];
    geometries[i][j][k][2] = geom_loc.gcov[0][2];
    geometries[i][j][k][3] = geom_loc.gcov[0][3];
    geometries[i][j][k][4] = geom_loc.gcov[1][1];
    geometries[i][j][k][5] = geom_loc.gcov[1][2];
    geometries[i][j][k][6] = geom_loc.gcov[1][3];
    geometries[i][j][k][7] = geom_loc.gcov[2][2];
    geometries[i][j][k][8] = geom_loc.gcov[2][3];
    geometries[i][j][k][9] = geom_loc.gcov[3][3];
    geometries[i][j][k][10]= geom_loc.gdet;

    ind++;
  }
#endif // !DUMP_NUMERIC

  if( ROTATE ) {
    /* Read local time and dxp3 */
    header_id = H5Gopen(file_id, "Header");
    grid_id = H5Gopen(header_id, "Grid");
    myH5_read_scalar2_orig(grid_id, "t"   , H5T_NATIVE_DOUBLE, &tloc);
    myH5_read_scalar2_orig(grid_id, "dx3" , H5T_NATIVE_DOUBLE, &dxp3);
    printf("Rotating metric back to %s time %e \n",hdf_name,tloc);

    /* Set the BBH traj data to corotate */
    set_bbh_traj_data(tloc, &(bbh_traj));
    Norb = floor( 0.5 * bbh_traj.phi / M_PI );
    phase_shift = bbh_traj.phi - (2. * M_PI * Norb);

    /* Calculate index to roll arrays by in phi-index */
    roll_index = floor( phase_shift / (2. * M_PI * dxp3 ) - 0. + 0.5 );

    GEOM_LOOP {
      for(i=0;i<N1;i++) {
	for(j=0;j<N2;j++) {  
	  for(k=0;k<N3;k++) { rolling_array[k] = geometries[i][j][k][l]; }
	  roll_array(roll_index, N3, rolling_array); //roll the array
	  for(k=0;k<N3;k++) { geometries[i][j][k][l] = rolling_array[k]; }
	}
      }  
    }
  }

  //printf("loop over metric funcs and rotate \n");
  /* GEOM_LOOP { */
  /*   printf("%d %s\n",l,metric_func_names[l]); */
  /*   myH5_read_gfunc_orig(file_id, metric_func_names[l], func_tmp); */
  /*   ind = 0; */
  /*   for(i=0;i<N1;i++) { */
  /*     for(j=0;j<N2;j++) { */
  /* 	for(k=0;k<N3;k++) { geometries[i][j][k][l] = func_tmp[ind]; ind++; } //NEED TO TRANFORM dennis */
  /* 	for(k=0;k<N3;k++) { rolling_array[k] = geometries[i][j][k][l]; }  */
  /* 	roll_array(roll_index, N3, rolling_array); //roll the array */
  /* 	for(k=0;k<N3;k++) { geometries[i][j][k][l] = rolling_array[k]; } */
  /*     } */
  /*   } */
  /* } */

 

  /* Free up the allocated memory */
  FREE(rolling_array);
  FREE(func_tmp );
  FREE(dx_dxp10);
  FREE(dx_dxp11);
  FREE(dx_dxp12);
  FREE(dx_dxp13);
  FREE(dx_dxp20);
  FREE(dx_dxp21);
  FREE(dx_dxp22);
  FREE(dx_dxp23);
  FREE(dx_dxp30);
  FREE(dx_dxp31);
  FREE(dx_dxp32);
  FREE(dx_dxp33);
 
  return;
}


/* Read in harm data from hdf_name --returns in numerical coords */
void get_metric(char *hdf_name, char metric_func_names[][100], double ****geometries)
{
  int ind,i,j,k,l;
  hid_t file_id;
  double *func_tmp;

  ALLOC_ARRAY(func_tmp,NCELLS);

  file_id = myH5_Fopen(hdf_name,0);


  //printf("loop over metric funcs and rotate \n");
  GEOM_LOOP {
    printf("%d %s\n",l,metric_func_names[l]);
    myH5_read_gfunc_orig(file_id, metric_func_names[l], func_tmp);
    ind = 0;
    LOOP { geometries[i][j][k][l] = func_tmp[ind]; ind++; }     
  }
 
  FREE(func_tmp);
  return;
}



/* 
   Rolls an array forward by some index roll_index
    that is maps array_in[roll_index] --> array_out[0]
*/
void roll_array(int roll_index, int array_length, double *array)
{
  int i,icurr;
  double *array_temp;
  ALLOC_ARRAY(array_temp,array_length);
  //printf("Rolling array by %d\n",roll_index);
  for(i=0;i<array_length;i++) {
    if     ( i < roll_index ) { icurr = array_length - roll_index + i; }
    else if( i > roll_index ) { icurr = i - roll_index;                }
    else                      { icurr = 0;                             }
    //printf(" %d --> %d \n", i, icurr);
    array_temp[icurr] = array[i];
  }
  /* Rewrite the array with array_temp */
  for(i=0;i<array_length;i++) { array[i] = array_temp[i]; }

  FREE(array_temp);
  return;
}

void corotate_gfuncs(char *hdf_name, double ****gfuncs)
{
  hid_t file_id, header_id, grid_id;
  int i,j,k,l;
  int Norb,roll_index;
  double tloc;
  double dxp3;
  double phase_shift;
  double *rolling_array;
  struct of_bbh_traj bbh_traj;

  ALLOC_ARRAY(rolling_array,N3);

  file_id  = myH5_Fopen(hdf_name,0);
  header_id = H5Gopen(file_id,"Header");
  grid_id = H5Gopen(header_id,"Grid");
  myH5_read_scalar2_orig(grid_id,"t"    ,H5T_NATIVE_DOUBLE,&tloc);
  myH5_read_scalar2_orig(grid_id,"dx3" ,H5T_NATIVE_DOUBLE,&dxp3); //doesn't like

  /* Set the BBH traj data to corotate */
  set_bbh_traj_data(tloc, &(bbh_traj));
  Norb = floor( 0.5 * bbh_traj.phi / M_PI );
  phase_shift = bbh_traj.phi - (2. * M_PI * Norb);

  /* Calculate index to roll arrays by in phi-index */
  roll_index = floor( phase_shift / (2. * M_PI * dxp3 ) - 0. + 0.5 );
  printf("Rotating gfuncs back by %e\n",phase_shift);
  //printf("roll_index %d \n",roll_index);
  /* Now populate and roll our gfunc array */
  GLOOP {
    for(i=0;i<N1;i++) {
      for(j=0;j<N2;j++) {
	//populate our rolling_array
	for(k=0;k<N3;k++) { rolling_array[k] = gfuncs[i][j][k][l]; }
	roll_array(roll_index, N3, rolling_array); //roll the array
	//replace the gfuncs with rotated prims
	for(k=0;k<N3;k++) { gfuncs[i][j][k][l] = rolling_array[k]; }
      }
    }
  }
  FREE(rolling_array);
  return;
}

void write_3d_gfuncs(hid_t loc_id, int overwrite, int NFUNCS, char f_func_names[][100], double ****funcs)
{
  int ind,i,j,k,l;
  /* Write averaged gfuncs */
  for(l=0;l<NFUNCS;l++) {
    ind = 0;
    LOOP {
      vals[ind] = funcs[i][j][k][l];
      ind++;
    }
    printf("writing %s\n",f_func_names[l]);
    myH5_write_gfunc_orig(loc_id,f_func_names[l],vals);    
  }

  return;
}

void write_3d_gfunc(hid_t loc_id, int overwrite,  char *f_func_name, double ***func)
{
  int ind,i,j,k;
  /* Write single 3d func */
  ind = 0;
  LOOP {
    vals[ind] = func[i][j][k];
    ind++;
  }
  printf("writing %s\n",f_func_name);
  myH5_write_gfunc_orig(loc_id,f_func_name,vals);    

  return;
}

int invert_matrix2( double Am[][NDIM], double invOut[][NDIM], double *detg )  
{
  double inv[16], det;
  int i,j,k;

  inv[0] =   Am[1][1]*Am[2][2]*Am[3][3] - Am[1][1]*Am[2][3]*Am[3][2] - Am[2][1]*Am[1][2]*Am[3][3]
    + Am[2][1]*Am[1][3]*Am[3][2] + Am[3][1]*Am[1][2]*Am[2][3] - Am[3][1]*Am[1][3]*Am[2][2];
  inv[4] =  -Am[1][0]*Am[2][2]*Am[3][3] + Am[1][0]*Am[2][3]*Am[3][2] + Am[2][0]*Am[1][2]*Am[3][3]
    - Am[2][0]*Am[1][3]*Am[3][2] - Am[3][0]*Am[1][2]*Am[2][3] + Am[3][0]*Am[1][3]*Am[2][2];
  inv[8] =   Am[1][0]*Am[2][1]*Am[3][3] - Am[1][0]*Am[2][3]*Am[3][1] - Am[2][0]*Am[1][1]*Am[3][3]
    + Am[2][0]*Am[1][3]*Am[3][1] + Am[3][0]*Am[1][1]*Am[2][3] - Am[3][0]*Am[1][3]*Am[2][1];
  inv[12] = -Am[1][0]*Am[2][1]*Am[3][2] + Am[1][0]*Am[2][2]*Am[3][1] + Am[2][0]*Am[1][1]*Am[3][2]
    - Am[2][0]*Am[1][2]*Am[3][1] - Am[3][0]*Am[1][1]*Am[2][2] + Am[3][0]*Am[1][2]*Am[2][1];
  inv[1] =  -Am[0][1]*Am[2][2]*Am[3][3] + Am[0][1]*Am[2][3]*Am[3][2] + Am[2][1]*Am[0][2]*Am[3][3]
    - Am[2][1]*Am[0][3]*Am[3][2] - Am[3][1]*Am[0][2]*Am[2][3] + Am[3][1]*Am[0][3]*Am[2][2];
  inv[5] =   Am[0][0]*Am[2][2]*Am[3][3] - Am[0][0]*Am[2][3]*Am[3][2] - Am[2][0]*Am[0][2]*Am[3][3]
    + Am[2][0]*Am[0][3]*Am[3][2] + Am[3][0]*Am[0][2]*Am[2][3] - Am[3][0]*Am[0][3]*Am[2][2];
  inv[9] =  -Am[0][0]*Am[2][1]*Am[3][3] + Am[0][0]*Am[2][3]*Am[3][1] + Am[2][0]*Am[0][1]*Am[3][3]
    - Am[2][0]*Am[0][3]*Am[3][1] - Am[3][0]*Am[0][1]*Am[2][3] + Am[3][0]*Am[0][3]*Am[2][1];
  inv[13] =  Am[0][0]*Am[2][1]*Am[3][2] - Am[0][0]*Am[2][2]*Am[3][1] - Am[2][0]*Am[0][1]*Am[3][2]
    + Am[2][0]*Am[0][2]*Am[3][1] + Am[3][0]*Am[0][1]*Am[2][2] - Am[3][0]*Am[0][2]*Am[2][1];
  inv[2] =   Am[0][1]*Am[1][2]*Am[3][3] - Am[0][1]*Am[1][3]*Am[3][2] - Am[1][1]*Am[0][2]*Am[3][3]
    + Am[1][1]*Am[0][3]*Am[3][2] + Am[3][1]*Am[0][2]*Am[1][3] - Am[3][1]*Am[0][3]*Am[1][2];
  inv[6] =  -Am[0][0]*Am[1][2]*Am[3][3] + Am[0][0]*Am[1][3]*Am[3][2] + Am[1][0]*Am[0][2]*Am[3][3]
    - Am[1][0]*Am[0][3]*Am[3][2] - Am[3][0]*Am[0][2]*Am[1][3] + Am[3][0]*Am[0][3]*Am[1][2];
  inv[10] =  Am[0][0]*Am[1][1]*Am[3][3] - Am[0][0]*Am[1][3]*Am[3][1] - Am[1][0]*Am[0][1]*Am[3][3]
    + Am[1][0]*Am[0][3]*Am[3][1] + Am[3][0]*Am[0][1]*Am[1][3] - Am[3][0]*Am[0][3]*Am[1][1];
  inv[14] = -Am[0][0]*Am[1][1]*Am[3][2] + Am[0][0]*Am[1][2]*Am[3][1] + Am[1][0]*Am[0][1]*Am[3][2]
    - Am[1][0]*Am[0][2]*Am[3][1] - Am[3][0]*Am[0][1]*Am[1][2] + Am[3][0]*Am[0][2]*Am[1][1];
  inv[3] =  -Am[0][1]*Am[1][2]*Am[2][3] + Am[0][1]*Am[1][3]*Am[2][2] + Am[1][1]*Am[0][2]*Am[2][3]
    - Am[1][1]*Am[0][3]*Am[2][2] - Am[2][1]*Am[0][2]*Am[1][3] + Am[2][1]*Am[0][3]*Am[1][2];
  inv[7] =   Am[0][0]*Am[1][2]*Am[2][3] - Am[0][0]*Am[1][3]*Am[2][2] - Am[1][0]*Am[0][2]*Am[2][3]
    + Am[1][0]*Am[0][3]*Am[2][2] + Am[2][0]*Am[0][2]*Am[1][3] - Am[2][0]*Am[0][3]*Am[1][2];
  inv[11] = -Am[0][0]*Am[1][1]*Am[2][3] + Am[0][0]*Am[1][3]*Am[2][1] + Am[1][0]*Am[0][1]*Am[2][3]
    - Am[1][0]*Am[0][3]*Am[2][1] - Am[2][0]*Am[0][1]*Am[1][3] + Am[2][0]*Am[0][3]*Am[1][1];
  inv[15] =  Am[0][0]*Am[1][1]*Am[2][2] - Am[0][0]*Am[1][2]*Am[2][1] - Am[1][0]*Am[0][1]*Am[2][2]
    + Am[1][0]*Am[0][2]*Am[2][1] + Am[2][0]*Am[0][1]*Am[1][2] - Am[2][0]*Am[0][2]*Am[1][1];

  det = Am[0][0]*inv[0] + Am[0][1]*inv[4] + Am[0][2]*inv[8] + Am[0][3]*inv[12];

  if( det == 0 ) {
    fprintf(stderr, "invert_matrix2(): singular matrix encountered! \n");
    fflush(stderr); 
    return(1);
  }

  *detg = sqrt(fabs(det));

  det = 1.0 / det;

  k = 0; 
  DLOOP2 { invOut[i][j] = inv[k++] * det; }

  return(0); 

}

/* find contravariant four-velocity */
void ucon_calc(double *pr, struct of_geom *geom, double *ucon)
{
	double gamma ;
	//	double alpha,beta[NDIM] ;
	//	int i ;

	//	alpha = 1./sqrt(-geom->gcon[TT][TT]) ;
	//	SDLOOP1 beta[i] = geom->gcon[TT][i]*alpha*alpha ;

	if( gamma_calc(pr,geom,&gamma) ) { 
	  fflush(stderr);
	  fprintf(stderr,"\nucon_calc(): gamma failure \n");
	  fflush(stderr);
	  //fail(FAIL_GAMMA_CALC,0);
	}

	//	ucon[TT] = gamma/alpha ;
	//	SDLOOP1 ucon[i] = pr[U1+i-1] - ucon[TT]*beta[i];

	ucon[TT] = gamma*geom->ncon[0] ;
	ucon[1] = pr[U1] - ucon[TT]*geom->beta[0];
	ucon[2] = pr[U2] - ucon[TT]*geom->beta[1];
	ucon[3] = pr[U3] - ucon[TT]*geom->beta[2];

	return ;
}

/* finds the primitive velocity components from the 4-velocity */
void ucon2pr( double *pr, double ucon[NDIM], double gcon[][NDIM] )
{
  double f_tmp ;

  f_tmp = ucon[TT] / gcon[TT][TT] ; 
  pr[ U1] = ucon[RR] - gcon[TT][RR] * f_tmp ;
  pr[ U2] = ucon[TH] - gcon[TT][TH] * f_tmp ;
  pr[ U3] = ucon[PH] - gcon[TT][PH] * f_tmp ;

  return ;
}


/* calculate magnetic field four-vector */
void bcon_calc(double *pr, double *ucon, double *ucov, double *bcon) 
{
  register double inv_ut;
  //  register unsigned int j;

  inv_ut = 1./ucon[TT];
  bcon[TT] = pr[B1]*ucov[1] + pr[B2]*ucov[2] + pr[B3]*ucov[3] ;
  bcon[RR] = (pr[B1] + bcon[TT]*ucon[RR])*inv_ut ;
  bcon[TH] = (pr[B2] + bcon[TT]*ucon[TH])*inv_ut ;
  bcon[PH] = (pr[B3] + bcon[TT]*ucon[PH])*inv_ut ;
	
//	for(j=1;j<4;j++)
//		bcon[j] = (pr[B1-1+j] + bcon[TT]*ucon[j])/ucon[TT] ;

	return ;
}

void Bcalc(double *pr, double *ucon, double *bcon)
{
  pr[B1] = bcon[RR]*ucon[TT] - bcon[TT]*ucon[RR];
  pr[B2] = bcon[RR]*ucon[TT] - bcon[TT]*ucon[RR];
  pr[B3] = bcon[RR]*ucon[TT] - bcon[TT]*ucon[RR];
  return;
}
/* Lowers a contravariant rank-1 tensor to a covariant one */
void lower(double *ucon, struct of_geom *geom, double *ucov)
{

	ucov[0] = geom->gcov[0][0]*ucon[0] 
		+ geom->gcov[0][1]*ucon[1] 
		+ geom->gcov[0][2]*ucon[2] 
		+ geom->gcov[0][3]*ucon[3] ;
	ucov[1] = geom->gcov[1][0]*ucon[0] 
		+ geom->gcov[1][1]*ucon[1] 
		+ geom->gcov[1][2]*ucon[2] 
		+ geom->gcov[1][3]*ucon[3] ;
	ucov[2] = geom->gcov[2][0]*ucon[0] 
		+ geom->gcov[2][1]*ucon[1] 
		+ geom->gcov[2][2]*ucon[2] 
		+ geom->gcov[2][3]*ucon[3] ;
	ucov[3] = geom->gcov[3][0]*ucon[0] 
		+ geom->gcov[3][1]*ucon[1] 
		+ geom->gcov[3][2]*ucon[2] 
		+ geom->gcov[3][3]*ucon[3] ;

        return ;
}

/* Raises a covariant rank-1 tensor to a contravariant one */
void raise(double *ucov, struct of_geom *geom, double *ucon)
{

	ucon[0] = geom->gcon[0][0]*ucov[0] 
		+ geom->gcon[0][1]*ucov[1] 
		+ geom->gcon[0][2]*ucov[2] 
		+ geom->gcon[0][3]*ucov[3] ;
	ucon[1] = geom->gcon[1][0]*ucov[0] 
		+ geom->gcon[1][1]*ucov[1] 
		+ geom->gcon[1][2]*ucov[2] 
		+ geom->gcon[1][3]*ucov[3] ;
	ucon[2] = geom->gcon[2][0]*ucov[0] 
		+ geom->gcon[2][1]*ucov[1] 
		+ geom->gcon[2][2]*ucov[2] 
		+ geom->gcon[2][3]*ucov[3] ;
	ucon[3] = geom->gcon[3][0]*ucov[0] 
		+ geom->gcon[3][1]*ucov[1] 
		+ geom->gcon[3][2]*ucov[2] 
		+ geom->gcon[3][3]*ucov[3] ;

        return ;
}

void set_geom_struct(double *geometries, struct of_geom *geom)
{
  
  geom->gcov[0][0] = geometries[0];
  geom->gcov[0][1] = geom->gcov[1][0] = geometries[1];
  geom->gcov[0][2] = geom->gcov[2][0] = geometries[2];
  geom->gcov[0][3] = geom->gcov[3][0] = geometries[3];
  geom->gcov[1][1] = geometries[4];
  geom->gcov[1][2] = geom->gcov[2][1] = geometries[5];
  geom->gcov[1][3] = geom->gcov[3][1] = geometries[6];
  geom->gcov[2][2] = geometries[7];
  geom->gcov[2][3] = geom->gcov[3][2] = geometries[8];
  geom->gcov[3][3] = geometries[9];

  invert_matrix2(geom->gcov,geom->gcon,&(geom->gdet));

  geom->ncon[0] = sqrt(-geom->gcon[0][0]);
  geom->alpha = 1./geom->ncon[0];

  geom->ncon[1] = -geom->alpha * geom->gcon[0][1];   
  geom->ncon[2] = -geom->alpha * geom->gcon[0][2];   
  geom->ncon[3] = -geom->alpha * geom->gcon[0][3]; 
  geom->beta[0] = -geom->gcon[0][1]/geom->gcon[0][0];
  geom->beta[1] = -geom->gcon[0][2]/geom->gcon[0][0]; 
  geom->beta[2] = -geom->gcon[0][3]/geom->gcon[0][0]; 


  return;
}

/* find gamma-factor wrt normal observer */
int gamma_calc(double *pr, struct of_geom *geom, double *gamma)
{
  double qsq ;

  qsq =     geom->gcov[1][1]*pr[U1]*pr[U1]
    + geom->gcov[2][2]*pr[U2]*pr[U2]
    + geom->gcov[3][3]*pr[U3]*pr[U3]
    + 2*(geom->gcov[1][2]*pr[U1]*pr[U2]
	 + geom->gcov[1][3]*pr[U1]*pr[U3]
	 + geom->gcov[2][3]*pr[U2]*pr[U3]) ;

  if( qsq < 0. ){
    if( fabs(qsq) > 1.E-10 ){ // then assume not just machine precision
      fprintf(stderr,"gamma_calc():  failed: qsq = %28.18e \n", qsq);
      fprintf(stderr,"v[1-3] = %28.18e %28.18e %28.18e  \n",pr[U1],pr[U2],pr[U3]);
      *gamma = 1.;
      return (1);
    }
    else qsq=1.E-10; // set floor
  }

  *gamma = sqrt(1. + qsq);

  return(0) ;
}
