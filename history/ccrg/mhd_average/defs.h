#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>

#include <hdf5.h>
#include <hdf5_hl.h>

#include <omp.h>

#define DUMP_NUMERIC (1) //useful for ray-tracing data

#define MTOT (0)
#define MBH1 (1)
#define MBH2 (2)
#define M    (1.)

#define NPRIM (8)
#define NDIM  (4)
#define RHO  (0)
#define UU   (1)
#define U1   (2)
#define U2   (3)
#define U3   (4)
#define B1   (5)
#define B2   (6)
#define B3   (7)
#define BSQ  (8)
#define DIVB (9)
#define GAM  (10)
#define TT  (0)
#define XX  (1)
#define YY  (2)
#define ZZ  (3)
#define RR  (1)
#define TH  (2)
#define PH  (3)
#define N_HDF_FUNCS (NPRIM + 3) //prims + bsq + divb + gamma
#define N_GEOM_FUNCS (11)

int N1, N2, N3, NCELLS;
#define LOOP for(i=0;i<N1;i++) for(j=0;j<N2;j++) for(k=0;k<N3;k++) 
#define DLOOP1 for(i=0;i<NDIM;i++)
#define DLOOP2 for(i=0;i<NDIM;i++) for(j=0;j<NDIM;j++)
#define PLOOP for(l=0;l<NPRIM;l++)
#define GLOOP for(l=0;l<N_HDF_FUNCS;l++) //loop over gfuncs
#define GEOM_LOOP for(l=0;l<N_GEOM_FUNCS;l++)




#define USE_STRICT_ARRAY_BOUNDS (1)
#define TOPOLOGY_CARTESIAN (0)
#define TOPOLOGY_SPHERICAL (1)

#define SOURCE_TOPOLOGY_TYPE ( TOPOLOGY_SPHERICAL )
#define DEST_TOPOLOGY_TYPE ( TOPOLOGY_SPHERICAL )



/* FUNCTION DEFINITIONS */
#define sincos my_sincos






struct of_bbh_traj 
{
  double xi1x;
  double xi1y;
  double xi1z;
  double xi2x;
  double xi2y;
  double xi2z;
  double v1x;
  double v1y;
  double v1z;
  double v2x;
  double v2y;
  double v2z;
  double t_c;
  double phi;
  double omega;
  double r12;
  double r12dot;
};

struct of_geom 
{
  double alpha;
  double ncon[NDIM];
  double beta[NDIM];
  double gcon[NDIM][NDIM];
  double gcov[NDIM][NDIM];
  double gdet;
};



/* coord.c */
void     my_sincos( double th, double *sth, double *cth);
void     transform_rank1con2( double dxp_dx[][NDIM], double vcon[]);
void transform_rank1cov2(double dx_dxp[][NDIM], double vcov[]);
void transform_rank2con2( double dxp_dx[][NDIM], double vcon[][NDIM]);
void     xcart_of_xspher(double *xcart, double *xspher, double dxc_dxs[][NDIM] );
void jac_xBL_to_xIZ( double Mass, double Spin, double xBL[NDIM], double dxIZ_dxBL[NDIM][NDIM]);
void jac_xIZ_to_xBL( double Mass, double Spin, double xIZ[NDIM], double dxBL_dxIZ[NDIM][NDIM]);
void jac_xNZ_to_xIZ( int BH, double xNZ[NDIM], double dxIZ_dxNZ[NDIM][NDIM]);
void jac_xNZ_to_xIZ_r12const( int BH, double xNZ[NDIM], double dxIZ_dxNZ[NDIM][NDIM]);
void coordsBL_of_coordsIZ( double Mass, double Spin, double xIZ[NDIM], double xBL[NDIM]);
void coordsIZ_of_coordsNZ(int BH, double xNZ[NDIM], double xIZ[NDIM]);
void transform_rank1con_PN2BL(int BH, double *xloc, double *ucon, double *xBL);
void transform_geom_PN2BL(int BH, double *xloc, struct of_geom *geom_loc, double *xBL);

/* dump_hdf.c */
hid_t    myH5_Fopen(char *name, int RW);
hid_t    myH5_Fcreate(char *name, int overwrite );
void     myH5_read_scalar2_orig( hid_t loc_id, char *name, hid_t type_id, void *value ) ;
void     myH5_read_gfunc_orig(hid_t loc_id, char *name, double *value ) ;
void     myH5_write_gfunc_orig( hid_t loc_id, char *name, double *value );
void     myH5_write_gfunc_orig( hid_t loc_id, char *name, double *value );
/* interp_data.c */

/* main.c */
double get_current_time(char *name);

/* metric_dyn_bbh_fullpn_nz_iz_2nd_v01.c */
void     initialize_metric_routines(char *name);
//void dyn_fullpn_nz_iz_2nd_gcov_func_setup(double tt, struct of_bbh_traj *bbh_traj);
void     set_bbh_traj_data(double tt, struct of_bbh_traj *bbh_traj);

/* utility.c */
void get_source_gfuncs(char *hdf_name, char f_hdf_name[][100], double ****gfuncs);
void write_3d_gfuncs(hid_t loc_id, int overwrite, int NFUNCS, char f_func_names[][100], double ****funcs);
void write_3d_gfunc(hid_t loc_id, int overwrite, char *f_func_name, double ***func);
void get_rotated_coords(char *hdf_name, double ***x1src, double ***x2src, double ***x3src);
void get_coords(char *hdf_name, double ***x1src, double ***x2src, double ***x3src);
void get_rotated_metric(int ROTATE, char *hdf_name, char metric_func_names[][100], double ****geometries);
void get_metric(char *hdf_name, char metric_func_names[][100], double ****geometries);
void roll_array(int roll_index, int array_length, double *array);
void corotate_gfuncs(char *hdf_name, double ****gfuncs);
int invert_matrix2( double Am[][NDIM], double invOut[][NDIM], double *detg );  
void ucon_calc(double *pr, struct of_geom *geom, double *ucon);
void bcon_calc(double *pr, double *ucon, double *ucov, double *bcon) ;
void ucon2pr( double *pr, double ucon[NDIM], double gcon[][NDIM] );
void lower(double *ucon, struct of_geom *geom, double *ucov);
void raise(double *ucov, struct of_geom *geom, double *ucon);
void set_geom_struct(double *geometries, struct of_geom *geom);
int gamma_calc(double *pr, struct of_geom *geom, double *gamma);
void Bcalc(double *pr, double *ucon, double *bcon);


/* ALLOCATION MACROS */
#define FREE(_p) { free(_p); _p = NULL; } 

#define ALLOC_ARRAY(_array,_npts)    {\
    if( ( (_array) = calloc( (_npts) , sizeof(*(_array)))) == NULL ) {	\
     fprintf(stdout,"%s(): Cannot allocate %s on line %d  of %s \n",__func__,#_array,__LINE__,__FILE__);  exit(0); \
   }\
 }

#define ALLOC_2D_ARRAY(_array,_n1,_n2) { \
 int _i1;\
 ALLOC_ARRAY(_array,_n1);\
 for(_i1=0; _i1<_n1; _i1++) { ALLOC_ARRAY(_array[_i1],_n2); }\
 }

#define ALLOC_3D_ARRAY(_array,_n1,_n2,_n3) { \
 int _i1, _i2;\
 ALLOC_2D_ARRAY(_array,_n1,_n2);\
 for(_i1=0; _i1<_n1; _i1++) for(_i2=0; _i2<_n2; _i2++) {  ALLOC_ARRAY(_array[_i1][_i2],_n3); }\
 }

#define ALLOC_4D_ARRAY(_array,_n1,_n2,_n3,_n4) { \
 int _i1, _i2, _i3;\
 ALLOC_3D_ARRAY(_array,_n1,_n2,_n3);\
 for(_i1=0; _i1<_n1; _i1++) for(_i2=0; _i2<_n2; _i2++) for(_i3=0; _i3<_n3; _i3++) {  ALLOC_ARRAY(_array[_i1][_i2][_i3],_n4); } \
 }
#define DEALLOC_ARRAY(_array,_npts)    {FREE((_array)); }

#define DEALLOC_2D_ARRAY(_array,_n1,_n2) { \
 int _i1;\
 for(_i1=0; _i1<_n1; _i1++) { DEALLOC_ARRAY(_array[_i1],_n2); }\
 DEALLOC_ARRAY(_array,_n1);\
 }

#define DEALLOC_3D_ARRAY(_array,_n1,_n2,_n3) { \
 int _i1, _i2;\
 for(_i1=0; _i1<_n1; _i1++) for(_i2=0; _i2<_n2; _i2++) {  DEALLOC_ARRAY(_array[_i1][_i2],_n3); }\
 DEALLOC_2D_ARRAY(_array,_n1,_n2);\
 }

#define DEALLOC_4D_ARRAY(_array,_n1,_n2,_n3,_n4) { \
 int _i1, _i2, _i3;\
 for(_i1=0; _i1<_n1; _i1++) for(_i2=0; _i2<_n2; _i2++) for(_i3=0; _i3<_n3; _i3++) {  DEALLOC_ARRAY(_array[_i1][_i2][_i3],_n4); } \
 DEALLOC_3D_ARRAY(_array,_n1,_n2,_n3);\
 }

#define DEALLOC_5D_ARRAY(_array,_n1,_n2,_n3,_n4,_n5) {     \
 int _i1, _i2, _i3,_i4;\
 for(_i1=0; _i1<_n1; _i1++) for(_i2=0; _i2<_n2; _i2++) for(_i3=0; _i3<_n3; _i3++) for(_i4=0; _i4<_n4; _i4++) {  DEALLOC_ARRAY(_array[_i1][_i2][_i3][_i4],_n5); }\
 DEALLOC_4D_ARRAY(_array,_n1,_n2,_n3,_n4);\
 }


/* Global Variables */
int MASS_TYPE;
double *vals;
double m_bh1, m_bh2, initial_bbh_separation;
double a_bh1, a_bh2;
