#include "defs.h"

/* Read in source file for gfuncs 
   and coordinates of simulatin data.
   Specify dump file to write
   onto an interpolated grid

   Arguments:
   1 - first file index
   2 - final file index
   3 - N1
   4 - N2
   5 - N3 
*/

#define TIME_AVERAGE (1)
#define GENERATE_COMOVING_DATA  (0)


int main(int argc,char *argv[])
{

  static char f_hdf_name[N_HDF_FUNCS][100];
  static char f_metric_name[N_GEOM_FUNCS][100];
  double ****gfuncs, ****gfuncs_sum, ****geometries;
  double ***x1src, ***x2src, ***x3src;
  char hdf_source[200];
  int l,i,j,k,ind;
  int fdump,ldump, findex,reads;
  hid_t out_id;

  //Set grid sizes
  fdump = atoi(argv[1]); ldump = atoi(argv[2]);
  N1 = atoi(argv[3]); N2 = atoi(argv[4]); N3 = atoi(argv[5]);
  NCELLS = N1 * N2 * N3;
  printf("N1 N2 N3 %d %d %d\n",N1,N2,N3);  

  /* Allocate Array memory */
  ALLOC_4D_ARRAY(gfuncs,N1,N2,N3,N_HDF_FUNCS);
  ALLOC_4D_ARRAY(gfuncs_sum,N1,N2,N3,N_HDF_FUNCS);
  ALLOC_4D_ARRAY(geometries,N1,N2,N3,N_GEOM_FUNCS);
  ALLOC_3D_ARRAY(x1src,N1,N2,N3);
  ALLOC_3D_ARRAY(x2src,N1,N2,N3);
  ALLOC_3D_ARRAY(x3src,N1,N2,N3);
  ALLOC_ARRAY(vals,NCELLS);

  l = 0;
  sprintf(f_hdf_name[l], "/rho"   ); l++;
  sprintf(f_hdf_name[l], "/uu"    ); l++;
  sprintf(f_hdf_name[l], "/v1"    ); l++;
  sprintf(f_hdf_name[l], "/v2"    ); l++;
  sprintf(f_hdf_name[l], "/v3"    ); l++;
  sprintf(f_hdf_name[l], "/B1"    ); l++;
  sprintf(f_hdf_name[l], "/B2"    ); l++;
  sprintf(f_hdf_name[l], "/B3"    ); l++;
  sprintf(f_hdf_name[l], "/bsq"   ); l++;
  sprintf(f_hdf_name[l], "/divb"  ); l++;
  sprintf(f_hdf_name[l], "/gamma" );

  l = 0;
  sprintf(f_metric_name[l], "/gcov00"   ); l++;
  sprintf(f_metric_name[l], "/gcov01"   ); l++;
  sprintf(f_metric_name[l], "/gcov02"   ); l++;
  sprintf(f_metric_name[l], "/gcov03"   ); l++;
  sprintf(f_metric_name[l], "/gcov11"   ); l++;
  sprintf(f_metric_name[l], "/gcov12"   ); l++;
  sprintf(f_metric_name[l], "/gcov13"   ); l++;
  sprintf(f_metric_name[l], "/gcov22"   ); l++;
  sprintf(f_metric_name[l], "/gcov23"   ); l++;
  sprintf(f_metric_name[l], "/gcov33"   ); l++;
  sprintf(f_metric_name[l], "/gdet"   ); 



  /* Initialize the metric routines */
  initialize_metric_routines("dumps/KDHARM0.000000.h5");

  /* Zero out all memory before using */
  ind = 0;
  LOOP {
    x1src[i][j][k] = x2src[i][j][k] = x3src[i][j][k] = 0.;
    GLOOP { gfuncs[i][j][k][l] = gfuncs_sum[i][j][k][l] = 0.; }
    vals[ind] = 0.;
    ind++;
  }
  /* Read in all files and keep running sum of gfuncs    */
#if( TIME_AVERAGE )
  printf("STARTING TIME-AVERAGING from dump %d to %d\n",fdump,ldump);
  reads = 0;
  for(findex=fdump;findex<=ldump;findex+=1) {
    sprintf(hdf_source,"dumps/KDHARM0.%06d.h5",findex);
    printf("Reading source %s\n",hdf_source);
  
    /* Read the hdf5 file to get coords and gfuncs */
    get_source_gfuncs(hdf_source,f_hdf_name,gfuncs);
    /* Roll the gfuncs back to the t=0 position */
    corotate_gfuncs(hdf_source, gfuncs);
    /* Keep a running sum of rolled gfuncs */
    LOOP GLOOP { gfuncs_sum[i][j][k][l] += gfuncs[i][j][k][l]; }
    reads++;
  }
  /* Take Average of the summed gfuncs */
  LOOP GLOOP { gfuncs_sum[i][j][k][l] /= reads; }

  /* Get coordinates averaged onto */
  sprintf(hdf_source,"dumps/KDHARM0.%06d.h5",fdump);
  get_rotated_coords(hdf_source,x1src,x2src,x3src);

  /* Get metric quantities at time averaged point */
  get_rotated_metric(1,hdf_source,f_metric_name,geometries);
 
  /* write to disk */
  out_id = myH5_Fcreate("averaged_gfuncs.h5",0); 
  write_3d_gfuncs(out_id,0,N_HDF_FUNCS,f_hdf_name,gfuncs_sum);
  write_3d_gfuncs(out_id,1,N_GEOM_FUNCS,f_metric_name,geometries);
  write_3d_gfunc( out_id,1,"/x1",x1src);
  write_3d_gfunc( out_id,1,"/x2",x2src);
  write_3d_gfunc( out_id,1,"/x3",x3src);
  
#endif //TIME_AVERAGE

#if( GENERATE_COMOVING_DATA)
  struct of_geom geom_loc;
  double tloc;
  double xloc[NDIM], ucon_loc[NDIM], ucov_loc[NDIM], bcon_loc[NDIM];
  double xBL_loc[NDIM];
  printf("GENERATING COMOVING DUMPS from dump %d to %d\n",fdump,ldump);
  reads = 0;
  for(findex=fdump;findex<=ldump;findex+=100) {
    sprintf(hdf_source,"dumps/KDHARM0.%06d.h5",findex);
    printf("Reading source %s\n",hdf_source);
  
    tloc = get_current_time(hdf_source);
    /* Read the hdf5 file to get coords and gfuncs */
    get_source_gfuncs(hdf_source,f_hdf_name,gfuncs);

    /* Read the metric funcs */
    get_rotated_metric(0,hdf_source,f_metric_name,geometries);
  
    /* Get local coords */
    get_coords(hdf_source, x1src, x2src, x3src);
    /* Transform gfuncs and metric */
    printf("loop over grid and transform to BL\n");
#pragma omp parallel for default(shared) private(i,j,k,xloc,geom_loc,ucon_loc,ucov_loc,bcon_loc,xBL_loc)
    LOOP {
      xloc[TT] = tloc;           xloc[RR] = x1src[i][j][k];
      xloc[TH] = x2src[i][j][k]; xloc[PH] = x3src[i][j][k];
      set_geom_struct(geometries[i][j][k],&geom_loc);
      ucon_calc(gfuncs[i][j][k], &geom_loc, ucon_loc);
      lower(ucon_loc, &geom_loc, ucov_loc);
      bcon_calc(gfuncs[i][j][k], ucon_loc, ucov_loc, bcon_loc);

      /* Let's transform!!! */
      transform_rank1con_PN2BL(1, xloc, ucon_loc, xBL_loc);
      transform_rank1con_PN2BL(1, xloc, bcon_loc, xBL_loc);
      transform_geom_PN2BL(1, xloc, &geom_loc, xBL_loc);
      /* Convert to prims */
      ucon2pr( gfuncs[i][j][k], ucon_loc, geom_loc.gcon );
      Bcalc(gfuncs[i][j][k], ucon_loc, bcon_loc);

      /* Overwrite coords with local BL coords */
      x1src[i][j][k] = xBL_loc[RR];
      x2src[i][j][k] = xBL_loc[TH];
      x3src[i][j][k] = xBL_loc[PH];
    }
    
    /* write to disk */
    sprintf(hdf_source,"dumps/KDHARM0.MINIDISK.%06d.h5",findex);
    out_id = myH5_Fcreate(hdf_source,0); 
    write_3d_gfuncs(out_id,0,N_HDF_FUNCS,f_hdf_name,gfuncs_sum);
    write_3d_gfuncs(out_id,1,N_GEOM_FUNCS,f_metric_name,geometries);
    write_3d_gfunc( out_id,1,"/rrBL",x1src);
    write_3d_gfunc( out_id,1,"/thBL",x2src);
    write_3d_gfunc( out_id,1,"/phBL",x3src);
  
  }

 
#endif //GENERATE_COMOVING_DATA

  DEALLOC_4D_ARRAY(gfuncs,N1,N2,N3,N_HDF_FUNCS);
  DEALLOC_4D_ARRAY(gfuncs_sum,N1,N2,N3,N_HDF_FUNCS);
  DEALLOC_4D_ARRAY(geometries,N1,N2,N3,N_GEOM_FUNCS);
  DEALLOC_3D_ARRAY(x1src,N1,N2,N3);
  DEALLOC_3D_ARRAY(x2src,N1,N2,N3);
  DEALLOC_3D_ARRAY(x3src,N1,N2,N3);
  FREE(vals);

  return 0;
}


double get_current_time(char *name)
{
  hid_t file_id, header_id, grid_id;
  double tglob;

  file_id  = myH5_Fopen(name,0);
  header_id = H5Gopen(file_id,"Header");
  grid_id = H5Gopen(header_id,"Grid");
  myH5_read_scalar2_orig(grid_id,"t"    ,H5T_NATIVE_DOUBLE,&tglob);
  printf("Obtained current time %e\n",tglob);
  return tglob;
}

