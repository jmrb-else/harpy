
from __future__ import division
from __future__ import print_function

import numpy as np
import h5py


from scipy.interpolate import interp1d


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap

from matplotlib.ticker import MaxNLocator

import os,sys

sys.path.insert(0, '../')
from coord_transforms import *

# the following module allows one to stop at a given point in the file with the
# command Tracer()(). this allows us to access the local functions and variables
# (closest thing to 'stop' in IDL).
#from IPython.core.debugger import Tracer
#Tracer()()

def get_dx_dxp(h5filename):
    h5file = h5py.File(h5filename,'r')
    dxdxp10 = h5file['dx_dxp10'].value
    dxdxp11 = h5file['dx_dxp11'].value
    dxdxp12 = h5file['dx_dxp12'].value
    dxdxp13 = h5file['dx_dxp13'].value
    dxdxp20 = h5file['dx_dxp20'].value
    dxdxp21 = h5file['dx_dxp21'].value
    dxdxp22 = h5file['dx_dxp22'].value
    dxdxp23 = h5file['dx_dxp23'].value
    dxdxp30 = h5file['dx_dxp30'].value
    dxdxp31 = h5file['dx_dxp31'].value
    dxdxp32 = h5file['dx_dxp32'].value
    dxdxp33 = h5file['dx_dxp33'].value
    h5file.close()

    #Here we assume that dt_dxp = delta[0,0]
    dxdxp00 = np.zeros((np.shape(dxdxp10))); dxdxp00.fill(1.)
    dxdxp01 = np.zeros((np.shape(dxdxp10))); dxdxp02 = np.zeros((np.shape(dxdxp10))); dxdxp03 = np.zeros((np.shape(dxdxp10)))

    dx_dxp = { '00':dxdxp00, '01':dxdxp01, '02':dxdxp02, '03':dxdxp03,
               '10':dxdxp10, '11':dxdxp11, '12':dxdxp12, '13':dxdxp13,
               '20':dxdxp20, '21':dxdxp21, '22':dxdxp22, '23':dxdxp23,
               '30':dxdxp30, '31':dxdxp31, '32':dxdxp32, '33':dxdxp33 }
    return dx_dxp

def get_header(h5filename):
    h5file = h5py.File(h5filename,'r')
    dx0 = h5file['Header/Grid/dx0'].value
    dx1 = h5file['Header/Grid/dx1'].value
    dx2 = h5file['Header/Grid/dx2'].value
    dx3 = h5file['Header/Grid/dx3'].value
    m_bh1 = h5file['Header/Grid/m_bh1'].value
    m_bh2 = h5file['Header/Grid/m_bh2'].value
    initial_bbh_separation = h5file['Header/Grid/initial_bbh_separation'].value

    Header = {'dx0':dx0, 'dx1':dx1, 'dx2':dx2, 'dx3':dx3, 'm_bh1':m_bh1, 'm_bh2':m_bh2, 'initial_bbh_separation':initial_bbh_separation}
    return Header

def get_metric(h5filename):
    h5file = h5py.File(h5filename, 'r')
    gcov00 = h5file['gcov00'].value
    gcov01 = h5file['gcov01'].value
    gcov02 = h5file['gcov02'].value
    gcov03 = h5file['gcov03'].value
    gcov11 = h5file['gcov11'].value
    gcov12 = h5file['gcov12'].value
    gcov13 = h5file['gcov13'].value
    gcov22 = h5file['gcov22'].value
    gcov23 = h5file['gcov23'].value
    gcov33 = h5file['gcov33'].value
    gdet   = h5file['gdet'].value
    h5file.close()


    # copied straight from Scott's idl set_metric_gen2.pro

    gcon00 =  gcov11*gcov22*gcov33 - gcov11*gcov23*gcov23 - gcov12*gcov12*gcov33 + gcov12*gcov13*gcov23 + gcov13*gcov12*gcov23 - gcov13*gcov13*gcov22
    gcon01 = -gcov01*gcov22*gcov33 + gcov01*gcov23*gcov23 + gcov02*gcov12*gcov33 - gcov02*gcov13*gcov23 - gcov03*gcov12*gcov23 + gcov03*gcov13*gcov22
    gcon02 =  gcov01*gcov12*gcov33 - gcov01*gcov23*gcov13 - gcov02*gcov11*gcov33 + gcov02*gcov13*gcov13 + gcov03*gcov11*gcov23 - gcov03*gcov13*gcov12
    gcon03 = -gcov01*gcov12*gcov23 + gcov01*gcov22*gcov13 + gcov02*gcov11*gcov23 - gcov02*gcov12*gcov13 - gcov03*gcov11*gcov22 + gcov03*gcov12*gcov12
    gcon11 =  gcov00*gcov22*gcov33 - gcov00*gcov23*gcov23 - gcov02*gcov02*gcov33 + gcov02*gcov03*gcov23 + gcov03*gcov02*gcov23 - gcov03*gcov03*gcov22
    gcon12 = -gcov00*gcov12*gcov33 + gcov00*gcov23*gcov13 + gcov02*gcov01*gcov33 - gcov02*gcov03*gcov13 - gcov03*gcov01*gcov23 + gcov03*gcov03*gcov12
    gcon13 =  gcov00*gcov12*gcov23 - gcov00*gcov22*gcov13 - gcov02*gcov01*gcov23 + gcov02*gcov02*gcov13 + gcov03*gcov01*gcov22 - gcov03*gcov02*gcov12
    gcon22 =  gcov00*gcov11*gcov33 - gcov00*gcov13*gcov13 - gcov01*gcov01*gcov33 + gcov01*gcov03*gcov13 + gcov03*gcov01*gcov13 - gcov03*gcov03*gcov11
    gcon23 = -gcov00*gcov11*gcov23 + gcov00*gcov12*gcov13 + gcov01*gcov01*gcov23 - gcov01*gcov02*gcov13 - gcov03*gcov01*gcov12 + gcov03*gcov02*gcov11
    gcon33 =  gcov00*gcov11*gcov22 - gcov00*gcov12*gcov12 - gcov01*gcov01*gcov22 + gcov01*gcov02*gcov12 + gcov02*gcov01*gcov12 - gcov02*gcov02*gcov11

    det = gcov00*gcon00 + gcov01*gcon01 + gcov02*gcon02 + gcov03*gcon03
    det[det==0] += 1.e-10    # to not prevent further computations in case a
                             # singular matrix is found at some points. this
                             # happens for instance when using excision...
    gcon00[gcon00==0] += -1.e-10

    inv_det = 1.0 / det
    det = 0

    gcon00 *= inv_det
    gcon01 *= inv_det
    gcon02 *= inv_det
    gcon03 *= inv_det
    gcon11 *= inv_det
    gcon12 *= inv_det
    gcon13 *= inv_det
    gcon22 *= inv_det
    gcon23 *= inv_det
    gcon33 *= inv_det

    inv_det = 0

    alpha = np.sqrt(-1./gcon00)
    beta1 =  -gcon01/gcon00
    beta2 =  -gcon02/gcon00
    beta3 =  -gcon03/gcon00


    metric = {'gdet':gdet,
              'gcov00':gcov00, 'gcov01':gcov01, 'gcov02':gcov02, 'gcov03':gcov03,
              'gcov11':gcov11, 'gcov12':gcov12, 'gcov13':gcov13,
              'gcov22':gcov22, 'gcov23':gcov23,
              'gcov33':gcov33,
              'gcon00':gcon00, 'gcon01':gcon01, 'gcon02':gcon02, 'gcon03':gcon03,
              'gcon11':gcon11, 'gcon12':gcon12, 'gcon13':gcon13,
              'gcon22':gcon22, 'gcon23':gcon23,
              'gcon33':gcon33,
              'alpha':alpha, 'beta1':beta1, 'beta2':beta2, 'beta3':beta3 }

    return metric


# copied straight from Scott's idl calc_ucon_mks2.pro
def calc_ucon(h5filename, v1, v2, v3):
    metric = get_metric(h5filename)

    vsq = metric['gcov11']*v1*v1 + metric['gcov22']*v2*v2 + metric['gcov33']*v3*v3 \
        + 2.*(    metric['gcov12']*v1*v2 + metric['gcov13']*v1*v3
               + metric['gcov23']*v2*v3 )

    gamma = np.sqrt(1. + vsq)
    vsq   = 0
    alpha = metric['alpha']
    beta1 = metric['beta1']
    beta2 = metric['beta2']
    beta3 = metric['beta3']

    ucon0 = gamma/alpha
    #gamma = 0
    ucon1 = v1 - ucon0 * beta1
    ucon2 = v2 - ucon0 * beta2
    ucon3 = v3 - ucon0 * beta3

    return ucon0, ucon1, ucon2, ucon3

def lower(gcov00,gcov01,gcov02,gcov03,gcov11,gcov12,gcov13,gcov22,gcov23,gcov33,ucon0,ucon1,ucon2,ucon3):
    ucov0 = gcov00*ucon0 + gcov01*ucon1 + gcov02*ucon2 + gcov03*ucon3
    ucov1 = gcov01*ucon0 + gcov11*ucon1 + gcov12*ucon2 + gcov13*ucon3
    ucov2 = gcov02*ucon0 + gcov12*ucon1 + gcov22*ucon2 + gcov23*ucon3
    ucov3 = gcov03*ucon0 + gcov13*ucon1 + gcov23*ucon2 + gcov33*ucon3

    return ucov0, ucov1, ucov2, ucov3

def lower2(metric,ucon0,ucon1,ucon2,ucon3):
    gcov00 = metric['gcov00']; gcov01 = metric['gcov01']; gcov02 = metric['gcov02']; gcov03 = metric['gcov03']
    gcov11 = metric['gcov11']; gcov12 = metric['gcov12']; gcov13 = metric['gcov13']
    gcov22 = metric['gcov22']; gcov23 = metric['gcov23']
    gcov33 = metric['gcov33']
    ucov0 = gcov00*ucon0 + gcov01*ucon1 + gcov02*ucon2 + gcov03*ucon3
    ucov1 = gcov01*ucon0 + gcov11*ucon1 + gcov12*ucon2 + gcov13*ucon3
    ucov2 = gcov02*ucon0 + gcov12*ucon1 + gcov22*ucon2 + gcov23*ucon3
    ucov3 = gcov03*ucon0 + gcov13*ucon1 + gcov23*ucon2 + gcov33*ucon3

    return ucov0, ucov1, ucov2, ucov3

def calc_ucov(h5filename, v1, v2, v3):
    metric = get_metric(h5filename)

    vsq = ( metric['gcov11']*v1*v1 + metric['gcov22']*v2*v2
           + metric['gcov33']*v3*v3 + 2.*(metric['gcov12']*v1*v2
           + metric['gcov13']*v1*v3 + metric['gcov23']*v2*v3) )

    gamma = np.sqrt(1. + vsq)
    vsq   = 0
    alpha = metric['alpha']
    beta1 = metric['beta1']
    beta2 = metric['beta2']
    beta3 = metric['beta3']

    #contravariant 4-velocity
    u0 = gamma/alpha
    #gamma = 0
    u1 = v1 - u0 * beta1
    u2 = v2 - u0 * beta2
    u3 = v3 - u0 * beta3

    #Lower 4-velocity
    u0, u1, u2, u3 = lower2(metric,u0,u1,u2,u3)
    return u0, u1, u2, u3



def bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3):
    inv_ut = 1. / ucon0
    bcon0  = B1*ucov1 + B2*ucov2 + B3*ucov3
    bcon1  = (B1 + bcon0*ucon1)*inv_ut
    bcon2  = (B2 + bcon0*ucon2)*inv_ut
    bcon3  = (B3 + bcon0*ucon3)*inv_ut

    return bcon0, bcon1, bcon2, bcon3



def calc_uconKS(h5filename, v1, v2, v3,gdump=False):
    h5file = h5py.File(h5filename, 'r')
    if( gdump ):
        dx_dxp11 = h5file['dxdxp11'].value
        dx_dxp12 = h5file['dxdxp12'].value
        dx_dxp13 = h5file['dxdxp13'].value
        dx_dxp21 = h5file['dxdxp21'].value
        dx_dxp22 = h5file['dxdxp22'].value
        dx_dxp23 = h5file['dxdxp23'].value
        dx_dxp31 = h5file['dxdxp31'].value
        dx_dxp32 = h5file['dxdxp32'].value
        dx_dxp33 = h5file['dxdxp33'].value
    else:
        dx_dxp11 = h5file['dx_dxp11'].value
        dx_dxp12 = h5file['dx_dxp12'].value
        dx_dxp13 = h5file['dx_dxp13'].value
        dx_dxp21 = h5file['dx_dxp21'].value
        dx_dxp22 = h5file['dx_dxp22'].value
        dx_dxp23 = h5file['dx_dxp23'].value
        dx_dxp31 = h5file['dx_dxp31'].value
        dx_dxp32 = h5file['dx_dxp32'].value
        dx_dxp33 = h5file['dx_dxp33'].value
    h5file.close()

    uconrr = dx_dxp11 * v1 + dx_dxp12 * v2 + dx_dxp13 * v3
    uconth = dx_dxp21 * v1 + dx_dxp22 * v2 + dx_dxp23 * v3
    uconph = dx_dxp31 * v1 + dx_dxp32 * v2 + dx_dxp33 * v3

    return uconrr, uconth, uconph

def uconp2ucon(h5filename, v1, v2, v3,gdump=False):
    uconrr, uconth, uconph = calc_uconKS(h5filename, v1, v2, v3,gdump=gdump)

    return uconrr, uconth, uconph
def uconp2ucon_dynamic(h5filename,ucon0,ucon1,ucon2,ucon3):
    h5file = h5py.File(h5filename, 'r')
    dx_dxp10 = h5file['dx_dxp10'].value
    dx_dxp11 = h5file['dx_dxp11'].value
    dx_dxp12 = h5file['dx_dxp12'].value
    dx_dxp13 = h5file['dx_dxp13'].value
    dx_dxp20 = h5file['dx_dxp20'].value
    dx_dxp21 = h5file['dx_dxp21'].value
    dx_dxp22 = h5file['dx_dxp22'].value
    dx_dxp23 = h5file['dx_dxp23'].value
    dx_dxp30 = h5file['dx_dxp30'].value
    dx_dxp31 = h5file['dx_dxp31'].value
    dx_dxp32 = h5file['dx_dxp32'].value
    dx_dxp33 = h5file['dx_dxp33'].value
    h5file.close()

    #assume that tprime = t (problem for ucontt?)
    ucontt = ucon0
    uconrr = dx_dxp10 * ucon0 + dx_dxp11 * ucon1 + dx_dxp12 * ucon2 + dx_dxp13 * ucon3
    uconth = dx_dxp20 * ucon0 + dx_dxp21 * ucon1 + dx_dxp22 * ucon2 + dx_dxp23 * ucon3
    uconph = dx_dxp30 * ucon0 + dx_dxp31 * ucon1 + dx_dxp32 * ucon2 + dx_dxp33 * ucon3
    return ucontt, uconrr, uconth, uconph

def ks2bl_con(h5filename,uconrrKS,uconthKS,uconphKS):
    metric = get_metric(h5filename)

    #normalize to find ucon00
    AA = metric['gcov00']
    BB = 2. * (metric['gcov01']*uconrrKS + metric['gcov03']*uconphKS)
    CC = metric['gcov11']*uconrrKS*uconrrKS + metric['gcov22']*uconthKS*uconthKS + metric['gcov33']*uconphKS*uconphKS + 1.

    uconttKS = (-BB + np.sqrt(BB*BB - 4.*AA*CC))/(2.*AA)

    #convert to BL coordinates (assuming total mass = 1)
    h5file = h5py.File(h5filename,'r')
    spin = h5file['/Header/Grid/a'].value[0]
    rr   = h5file['x1'].value
    th   = h5file['x2'].value
    ph   = h5file['x3'].value
    h5file.close()

    delta = rr*rr - 2.*rr + spin*spin

    dtBL_drKS = -2*rr/delta
    dphiBL_drKS = -spin/delta

    ucon0BL = uconttKS + dtBL_drKS*uconrrKS
    ucon1BL = uconrrKS
    ucon2BL = uconthKS
    ucon3BL = uconphKS + dphiBL_drKS*uconrrKS

    return ucon0BL, ucon1BL, ucon2BL, ucon3BL


def get_grid(h5filename):

    h5file       = h5py.File(h5filename, 'r')
    NG           = h5file['/Header/Grid/NG'].value[0]
    totalsize1   = h5file['/Header/Grid/totalsize1'].value[0]
    totalsize2   = h5file['/Header/Grid/totalsize2'].value[0]
    totalsize3   = h5file['/Header/Grid/totalsize3'].value[0]
    startx1      = h5file['/Header/Grid/startx1'].value[0]
    startx2      = h5file['/Header/Grid/startx2'].value[0]
    startx3      = h5file['/Header/Grid/startx3'].value[0]
    dx1          = h5file['/Header/Grid/dx1'].value[0]
    dx2          = h5file['/Header/Grid/dx2'].value[0]
    dx3          = h5file['/Header/Grid/dx3'].value[0]
    h5file.close()

    idim = totalsize1
    jdim = totalsize2
    kdim = totalsize3

    i1  = NG + np.arange(idim)
    xp1 = startx1 + (i1 + 0.5)*dx1
    i2  = NG + np.arange(jdim)
    xp2 = startx2 + (i2 + 0.5)*dx2
    i3  = NG + np.arange(kdim)
    xp3 = startx3 + (i3 + 0.5)*dx3

def vchar(h5filename,direction):
    metric = get_metric(h5filename)
    h5file = h5py.File(h5filename,'r')
    gam   = h5file['Header/Grid/gam'].value
    rho   = h5file['rho'].value
    uu    = h5file['uu' ].value
    bsq   = h5file['bsq'].value
    gamma = h5file['gamma'].value
    gcon00 = metric['gcon00']
    if direction == 1:
        vi = h5file['v1'].value
        gcon0i = metric['gcon01']
        gconii = metric['gcon11']
        betai  = metric['beta1']
    elif direction == 2:
        vi = h5file['v2'].value
        gcon0i = metric['gcon02']
        gconii = metric['gcon22']
        betai  = metric['beta2']
    elif direction == 3:
        vi = h5file['v3'].value
        gcon0i = metric['gcon03']
        gconii = metric['gcon33']
        betai  = metric['beta3']
    else:
        print("Failed in vchar calc... invalid direction")
        return
    h5file.close()
    print("vars set by dir")

    pgas = (gam - 1.)*uu
    eta  = pgas + rho + uu + bsq
    cms2 = (gam * pgas + bsq) / eta
    eta = 0
    pgas = 0

    ucon_t = gamma / metric['alpha']
    ucon_i = vi - ucon_t * betai

    Au2  = ucon_i*ucon_i
    Bu2  = ucon_t*ucon_t
    AuBu = ucon_i*ucon_t

    ucon_t = 0
    ucon_i = 0

    A = Bu2  - (gcon00 + Bu2 )*cms2
    B = AuBu - (gcon0i + AuBu)*cms2
    C = Au2  - (gconii + Au2 )*cms2

    discr = np.maximum(B*B - A*C,np.zeros((np.shape(B))))
    discr = np.sqrt(discr)
    denom = 1. / A

    vp = -(-B + discr)*denom
    vm = -(-B - discr)*denom

    vmax = np.maximum(vp,vm)
    vmin = np.minimum(vp,vm)

    vmin *= -1.

    vmax = np.maximum(vmax,np.zeros((np.shape(vmax))))
    vmin = np.maximum(vmin,np.zeros((np.shape(vmin))))

    vmax = np.maximum(np.abs(vmax),np.abs(vmin))

    return vmax

class polp(object):
    """
    polp class

    Description
    -----------
    Function that helps to plot harm3D files.

    Parameters
    ----------
    itime : int
        ID number of harm HDF5 dump.
    dirname : str
        Directory containing the HDF5 dumps. Fefault is None.
    funcname : str
        Quantity to be plotted. The available values are:
            'dM', 'dMdt': Accretion ratio.
            'uconrr':
            'uconth':
            'uconph':
            'uconrrbh':
            'uconph_bh':
            'vrbh':
            'omega_bh':
            'ucon_0', 'ucon_1', 'ucon_2', 'ucon_3':
            'ucov0':
            'temp':
            'dtmin1':
            'dtmin2':
            'dtmin3':
            'cs':
            'cooling':
            'entropy':
            'alpha':
            'beta1', 'beta2', 'beta3':
            'ncon0':
            'gcon00':
            'potential':
            'geom_alpha':
            'D':
            'mass_flux':
            'dg_dxp1', 'dg_dxp2', 'dg_dxp3':
            'd2g_dxp11', 'd2g_dxp12', 'd2g_dxp13':
            'd2g_dxp21', 'd2g_dxp22', 'd2g_dxp22':
            'd2g_dxp31', 'd2g_dxp31', 'd2g_dxp31':
            'gradg_rat_x1', 'gradg_rat_x2', 'gradg_rat_x3':
            'dr':
            'dth':
            'dph':
            'testclean':
    func0 :
    ith :
    iph :
    rmin :
    rmax :
    minf :
    maxf :
    xmax :
    ymax :
    xmin :
    ymin :
    zmax :
    zmin :
    nlev : int
        Default 256.
    nticks :
        Default 10.
    ticks_range :
        Default 'minmax'.
    use_num_coord : bool
        Use numerical coordinates. Default False. If True, the variables `plot_horizon`, `plot_bhs_traj` and `plot_grid` are set to False.
    plot_grid : bool
        Show grids. Default False.
    nperplot : int
    savepng : bool
        Save plot in PNG format. Default False.
    dirout :
    dolog :
    show_plot : bool
        Turn interactive plotting on/off. Default False.
    plottitle :
    userad :
    plot_bhs_traj :
    trajfile :
    plot_horizon :
    nwin :
    background : str
        Default 'black'
    namesuffix :
    plotcentre :
    ax :
    axislabels :
    xticks :
    yticks :
    yticksloc :
    retfunc : bool
        Default True.
    fillwedge :
    ubh :
    visc_circles :
    fillcutout :
    corotate :
    get_time :
    use_stat2 :
    stat2_factor :
    use_conserved_vars :
    scale :
    axis :
    take_ratio :
    relerror :
    usegdump :
    compare_runs :
    compare_dir :
    spinning :
    R1T0 :
    plot_potential:
    pminf :
    pmaxf :
    pnlev :
    pfontsize :
    pot_labels :
    plot_slosh_box :
    normalize_func :
    EXTEND_CENTRAL_CUTOUT :

    Returns
    -------
    """

    def __init__(self, **kwargs):
        # list of keys and default value
        self.itime = 0
        self.dirname = None
        self.funcname = None
        self.func0 = None
        self.ith = None
        self.iph = 0
        self.rmin = None
        self.rmax = None
        self.minf = None
        self.maxf = None
        self.xmax = None
        self.ymax = None
        self.xmin = None
        self.ymin = None
        self.zmax = None
        self.zmin = None
        self.nlev = 256
        self.nticks = 10
        self.ticks_range = 'minmax'
        self.nperplot = 20
        self.dirout = None
        self.plottitle = None
        self.userad = False
        self.plot_bhs_traj = False
        self.trajfile = None
        self.plot_horizon = False
        self.nwin = 0
        self.background = 'black'
        self.namesuffix = ''
        self.plotcentre = None
        self.ax = None
        self.axislabels = True
        self.xticks = None
        self.yticks = None
        self.yticksloc = 'left'
        self.retfunc = True
        self.ubh = False
        self.visc_circles = False
        self.fillcutout = False
        self.corotate = False
        self.get_time = False
        self.use_stat2 = False
        self.stat2_factor = 0
        self.use_conserved_vars = False
        self.cale = True
        self.axis = None
        self.take_ratio = False
        self.relerror = False
        self.usegdump = False
        self.compare_runs = False
        self.pnlev = 10
        self.compare_dir = None
        self.spinning = False
        self.R1T0 = None
        self.plot_potential = False
        self.pminf = None
        self.pmaxf = None
        self.pfontsize = 8
        self.pot_labels = True
        self.plot_slosh_box = False
        self.normalize_func = False
        self.EXTEND_CENTRAL_CUTOUT = False

        # list of extra keys
        more_allowed_keys = []
        # get list of all predefined values
        allowed_keys = list(self.__dict__.keys()) + more_allowed_keys
        # update __dict__ only for keys that have been predefined
        self.__dict__.update((key, value) for key, value in kwargs.items() if kwargs in allowed_keys)
        # To NOT silently ignore rejected keys
        rejected_keys = set(kwargs.keys()) - set(allowed_keys)
        if rejected_keys:
            raise ValueError("Invalid arguments in constructor:{}".format(rejected_keys))

    # nticks    -> number of ticks for the colorbar
    # plot_grid ->  plot numerical grid?
    def get_func(self):
        if self.use_num_coord:
            self.plot_horizon  = False
            self.plot_bhs_traj = False
            self.plot_grid     = False

        if self.ith is None:
            self.plot_bhs_traj = False

        if self.plotcentre is not None:
            self.plot_bhs_traj=True


        if self.dirname is None:
            self.dirname = os.getcwd()  # should tell me my current working directory,
                                # *not* where this script is located

        if self.trajfile is None:
            if self.ubh is False:
                self.trajfile = self.dirname + '/dumps/KDHARM0_bbh_trajectory.out'
            #trajfile = self.dirname + '/my_bbh_trajectory.out'

        timeid = '{:06d}'.format(self.itime) #harm HDF5 dumps

        if self.use_stat2:
            timestat2 = '{:06d}'.format(self.itime - self.stat2_factor)
            stat2file = self.dirname + '/dumps/' + 'KDHARM0.STAT2.' + timestat2 + '.h5'

        ###dumpfile  = self.dirname + '/dumps/' + 'KDHARM0.' +  timeid + '.h5'
        dumpfile  = self.dirname + '/' + 'BBH-DISK-CART-ORIGIN.patch_1.' +  timeid + '.h5'
        #dumpfile = self.dirname + '/dumps/' + 'monopole_cleaner_it' + timeid + '0000.h5'

        ###radfile = self.dirname + '/dumps/' + 'KDHARM0.RADFLUX.' +  timeid + '.h5'
        radfile   = self.dirname + '/dumps/' + 'KDHARM0.RADFLUX.' +  timeid + '.h5'
        gdumpfile = self.dirname + '/dumps/' + 'KDHARM0.gdump.h5'

        fullname = dumpfile

        h5file   = h5py.File(fullname, 'r')

        rr = h5file['x1'].value   # 'x1' is the name of the dataset inside the hdf5 file
        ph = h5file['x3'].value
        th = h5file['x2'].value
        # we get the time level:
        time = h5file['Header/Grid/t'][0]

        if self.ith == 'equator':
            self.ith = int((len(rr[0,:,0])) / 2)
            if rr.shape[1] == 1:
                EQUATORIAL_RUN = True
            if self.funcname == 'dM' or self.funcname == 'dMdt':
                SUM_THETA = True
        if self.ith is None and (self.corotate or self.iph is None):
            if( self.spinning ):
                # spinning
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile, usecols=(0,1,2,4,5), unpack=True)
            else:
                # old metrics
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile, usecols=(0,4,5,6,7), unpack=True)

            fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
            fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
            fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
            fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

            #theta = th_slice[0,0]

            xbh1  = fxbh1(time)#/np.sin(theta)
            ybh1  = fybh1(time)#/np.sin(theta)
            xbh2  = fxbh2(time)#/np.sin(theta)
            ybh2  = fybh2(time)#/np.sin(theta)

            phase = np.arctan2(ybh1,xbh1)
            #renormalize phase s.t phase \in (0,2pi)
            if phase < 0.:
                phase = 2. * np.pi - np.abs(phase)
            dxp3 = h5file['Header/Grid/dx3'].value[0]
            kshift = int( phase / ( 2. * np.pi * dxp3 ) - 0. + 0.5 )
            phasediffmin = 2.e6
            for index in xrange(0,np.shape(ph)[2]):
                phasediff = np.abs(ph[0,0,index] - phase)
                if phasediffmin > phasediff:
                    phasediffmin = phasediff
                    indexcrit = index
            print("Expected shift found shift ",kshift,indexcrit)
            iph = kshift + 1

        # set grid coordinate functions
        if ith is not None:
            rr_slice = rr[:,ith,:]
            ph_slice = ph[:,ith,:]
            th_slice = th[:,ith,:]
        else:
            rr_slice = rr[:,:,iph]
            ph_slice = ph[:,:,iph]
            th_slice = th[:,:,iph]

        if rmin is None:
            rmin = np.min(rr)
        if rmax is None:
            rmax = np.max(rr)

        if userad:
            fullname = radfile
            h5file.close()
            h5file = h5py.File(fullname, 'r')
        elif usegdump:
            fullname = gdumpfile
            h5file.close()
            h5file = h5py.File(fullname, 'r')
        if use_stat2:
            #fullname = stat2file
            #h5file.close()
            #h5file = h5py.File(stat2file,'r')
            h5file = h5py.File(fullname,'r')
            h5stat2 = h5py.File(stat2file,'r')

        # if we've specified something for func0 we will plot it instead.
        if func0 is not None:
            func = func0
        elif self.funcname == 'uconrr':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
            h5file2.close()
            #uconrr, uconth, uconph = calc_uconKS(dumpfile, v1, v2, v3) #assumes diagonal metric to get velocities
            ucon0, uconrrp, uconthp, uconphp = calc_ucon(dumpfile,v1,v2,v3) #get ucon wrt numerical coordinates
            #non-dynamic coords
            #uconrr, uconth, uconph = uconp2ucon(dumpfile,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
            #dynamic coordinates
            ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(dumpfile,ucon0,uconrrp,uconthp,uconphp)
            if met_type == 2:
                ucon0, uconrr, uconth, uconph =  ks2bl_con(dumpfile,uconrr,uconth,uconph) #convert from ks to bl if necessary...


            uconth = 0
            uconph = 0
            func   = uconrr  / ucon00 #should be uconrr dennis
        elif self.funcname == 'uconrrbh' or self.funcname == 'vrbh':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1 = h5file2['v1'].value
            v2 = h5file2['v2'].value
            v3 = h5file2['v3'].value
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
            h5file2.close()

            ucon0, uconrrp, uconthp, uconphp = calc_ucon(dumpfile,v1,v2,v3) #get ucon wrt numerical coordinates
            #dynamic coordinates
            ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(dumpfile,ucon0,uconrrp,uconthp,uconphp)

            r1max = 45.
            r2max = 45.
            tt = np.zeros((np.shape(rr))); tt.fill(time)
            uconttb, uconrrb, uconthb, uconphb = rank1con2BH1(self.trajfile, tt, rr, th, ph, ucon00, uconrr, uconth, uconph, dx_dxp=None, SETPHASE=None)

            if self.funcname == 'uconrrbh':
                func = uconrrb
            elif self.funcname == 'vrbh':
                func = uconrrb/uconttb

            #Normalize to adot
            tbh_traj, r12_traj = np.loadtxt(self.trajfile,usecols=[0,23],unpack=True)
            adot_traj          = np.gradient(r12_traj,tbh_traj[1] - tbh_traj[0])
            fadot              = interp1d(tbh_traj,adot_traj,kind='linear')
            print(fadot(time), "Normalizing vr by adot")
            func              /= fadot(time)
        elif self.funcname == 'uconth':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
            h5file2.close()
            #uconrr, uconth, uconph = calc_uconKS(dumpfile, v1, v2, v3) #assumes diagonal metric to get velocities
            ucon0, uconrrp, uconthp, uconphp = calc_ucon(dumpfile,v1,v2,v3) #get ucon wrt numerical coordinates
            #non-dynamic coords
            #uconrr, uconth, uconph = uconp2ucon(dumpfile,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
            #dynamic coordinates
            ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(dumpfile,ucon0,uconrrp,uconthp,uconphp)
            if met_type == 2:
                ucon0, uconrr, uconth, uconph =  ks2bl_con(dumpfile,uconrr,uconth,uconph) #convert from ks to bl if necessary...

            uconrr = 0
            uconph = 0
            func   = uconth
        elif self.funcname == 'uconph':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
            h5file2.close()
            #uconrr, uconth, uconph = calc_uconKS(dumpfile, v1, v2, v3) #assumes diagonal metric to get physical ucon
            ucon0, uconrrp, uconthp, uconphp = calc_ucon(dumpfile,v1,v2,v3) #returns ucon wrt numerical coordinates
            #non-dynamic coords
            #uconrr, uconth, uconph = uconp2ucon(dumpfile,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
            #dynamic coordinates
            ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(dumpfile,ucon0,uconrrp,uconthp,uconphp)
            if met_type == 2:
                ucon0, uconrr, uconth, uconph =  ks2bl_con(dumpfile,uconrr,uconth,uconph) #convert from ks to bl if necessary...

            uconth = 0
            uconrr = 0
            func   = uconph #should be uconph dennis here
        elif self.funcname == 'uconph_bh' or self.funcname == 'omega_bh':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')

            v1 = h5file2['v1'].value
            v2 = h5file2['v2'].value
            v3 = h5file2['v3'].value
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'].value[0]
            h5file2.close()

            ucon0, uconrrp, uconthp, uconphp = calc_ucon(dumpfile,v1,v2,v3) #get ucon wrt numerical coordinates
            #dynamic coordinates
            ucon00,uconrr, uconth, uconph = uconp2ucon_dynamic(dumpfile,ucon0,uconrrp,uconthp,uconphp)

            uconCM = np.zeros((uconrr.shape[0],uconrr.shape[1],uconrr.shape[2],4))
            uconCM[:,:,:,0] = ucon00[:,:,:]
            uconCM[:,:,:,1] = uconrr[:,:,:]
            uconCM[:,:,:,2] = uconth[:,:,:]
            uconCM[:,:,:,3] = uconph[:,:,:]
            r1max = 50.
            r2max = 50.
            #uconttb,uconrrb,uconthb,uconphb = ucon2bhframes(trajfile, uconCM,rr,ph,r1max,r2max,time)
            dx_dxp = get_dx_dxp(dumpfile)
            TNZ = np.zeros(np.shape(rr)); TNZ = TNZ.fill(time)
            uconttb,uconrrb,uconthb,uconphb = rank1con2BH1(trajfile,TNZ,rr,th,ph,ucon00,uconrr,uconth,uconph)
            #print(np.min(ucon0),np.min(ucon00),np.min(uconttb))
            #print(ucon00 - ucon0)
            if self.funcname == 'uconph_bh':
                func = uconphb
            elif self.funcname == 'omega_bh':
                func = uconrrb/uconttb #DENNIS WRONG
        elif self.funcname == 'ucon0':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            h5file2.close()
            ucon0, ucon1, ucon2, ucon3 = calc_ucon(dumpfile, v1, v2, v3)
            ucon1 = 0
            ucon2 = 0
            ucon3 = 0
            func  = ucon0
        elif self.funcname == 'ucon1':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            h5file2.close()
            ucon0, ucon1, ucon2, ucon3 = calc_ucon(dumpfile, v1, v2, v3)
            ucon0 = 0
            ucon2 = 0
            ucon3 = 0
            func  = ucon1
        elif self.funcname == 'ucon2':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            h5file2.close()
            ucon0, ucon1, ucon2, ucon3 = calc_ucon(dumpfile, v1, v2, v3)
            ucon0 = 0
            ucon1 = 0
            ucon3 = 0
            func  = ucon2
        elif self.funcname == 'ucon3':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            h5file2.close()
            ucon0, ucon1, ucon2, ucon3 = calc_ucon(dumpfile, v1, v2, v3)
            ucon0 = 0
            ucon1 = 0
            ucon2 = 0
            func  = ucon3

        elif self.funcname == 'ucov0':
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            h5file2.close()
            metric = get_metric(dumpfile)
            ucon0, ucon1, ucon2, ucon3 = calc_ucon(dumpfile, v1, v2, v3)
            ucov0, ucov1, ucov2, ucov3 = lower2(metric,ucon0,ucon1,ucon2,ucon3)
            func  = ucov0

        elif self.funcname == 'temp':
            USE_SIMPLE_EOS = h5file['Header/Macros/USE_SIMPLE_EOS'].value[0]
            if( USE_SIMPLE_EOS ):
                print("Using ISOTHERMAL EOS")
                pressure = h5file['B1'].value
                rho      = h5file['rho'].value
                #assume Isothermal EOS
                func = pressure / rho
            else:
                print("Using GAMMA-LAW EOS")
                gam = h5file['Header/Grid/gam'].value[0]
                uu  = h5file['uu'].value
                rho = h5file['rho'].value
                func = (gam - 1.)*uu/rho
        elif self.funcname == 'dtmin1':
            dx1 = h5file['Header/Grid/dx1'].value[0]
            vmax = vchar(fullname,1)
            func = np.abs(dx1 / vmax)
            print("dtmin1 found to be ",np.min(func), " at coords (r,th,ph) = ",rr[func == np.min(func)],th[func == np.min(func)],ph[func == np.min(func)])
        elif self.funcname == 'dtmin2':
            dx2 = h5file['Header/Grid/dx2'].value[0]
            vmax = vchar(fullname,2)
            func = np.abs(dx2 / vmax)
            print("dtmin2 found to be ",np.min(func), " at coords (r,th,ph) = ",rr[func == np.min(func)],th[func == np.min(func)],ph[func == np.min(func)])
        elif self.funcname == 'dtmin3':
            dx3 = h5file['Header/Grid/dx3'].value[0]
            vmax = vchar(fullname,3)
            func = np.abs(dx3 / vmax)
            print("dtmin3 found to be ",np.min(func), " at coords (r,th,ph) = ",rr[func == np.min(func)],th[func == np.min(func)],ph[func == np.min(func)])
            print( np.where(func == func.min()) )
        elif self.funcname == 'cs':
            #sqrt{gam p / rho}
            gam = h5file['Header/Grid/gam'].value[0]
            uu  = h5file['uu'].value
            rho = h5file['rho'].value
            p   = (gam - 1.)*uu
            #func = np.sqrt(gam) * p / rho
            func = np.sqrt(gam * p / rho)
        elif self.funcname == 'cooling':
            gam = h5file['Header/Grid/gam'].value[0]
            uu  = h5file['uu'].value
            rho = h5file['rho'].value
            S0  = 0.01
            S   = (gam - 1.)*uu / np.power(rho,gam)
            Sterm = (S - S0)/S0
            #calculate BL
            TNZ = np.zeros((np.shape(rr))); TNZ.fill(time)
            XNZ = rr * np.sin(th) * np.cos(ph)
            YNZ = rr * np.sin(th) * np.sin(ph)
            ZNZ = rr * np.cos(th)

            if( self.spinning ):
                #
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile, usecols=(0,1,2,4,5), unpack=True) #spinning
            else:
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile, usecols=(0,4,5,6,7), unpack=True) #old metrics

            fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
            fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
            fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
            fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

            theta = th_slice[0,0]

            xbh1  = fxbh1(time)/np.sin(theta)
            ybh1  = fybh1(time)/np.sin(theta)
            xbh2  = fxbh2(time)/np.sin(theta)
            ybh2  = fybh2(time)/np.sin(theta)
            sep   = np.sqrt( (xbh1 - xbh2)*(xbh1 - xbh2) + (ybh1 - ybh2)*(ybh1 - ybh2) )

            #Assume that q = 1 ( so m1 = m2 = 0.5 )
            rBL = rr + 1.
            rBL[rBL < 1.5 * sep] = 1.5 * sep
            r1  = np.sqrt( (XNZ - xbh1)*(XNZ - xbh1) + (YNZ - ybh1)*(YNZ - ybh1) + (ZNZ)*(ZNZ) ) + 0.5
            r2  = np.sqrt( (XNZ - xbh2)*(XNZ - xbh2) + (YNZ - ybh2)*(YNZ - ybh2) + (ZNZ)*(ZNZ) ) + 0.5
            #Reset to ISCO inside ISCO
            r1[ r1 <= 3. ] = 3.
            r2[ r2 <= 3. ] = 3.

            Tcool = 2. * np.pi * np.power( rBL, 1.5 )
            Tcool[r1 < 0.45 * sep] = 2. * np.pi * np.power( r1[r1 < 0.45 * sep], 1.5 ) / np.sqrt(0.5)
            Tcool[r2 < 0.45 * sep] = 2. * np.pi * np.power( r2[r2 < 0.45 * sep], 1.5 ) / np.sqrt(0.5)

            func = (uu/Tcool) * ( Sterm + np.abs(Sterm) ) #/ rho #divide by rho to get cooling per unit mass

        elif self.funcname == 'entropy':
            rho = h5file['rho'].value
            uu  = h5file['uu'].value
            gam = h5file['Header/Grid/gam'].value
            p   = (gam - 1.)*uu
            func = p / np.power(rho,gam)

        elif self.funcname == 'beta1':
            metric = get_metric(fullname)
            func = metric['beta1']

        elif self.funcname == 'beta2':
            metric = get_metric(fullname)
            func = metric['beta2']

        elif self.funcname == 'beta3':
            metric = get_metric(fullname)
            func = metric['beta3']

        elif self.funcname == 'alpha':
            metric = get_metric(fullname)
            func = metric['alpha']

        elif self.funcname == 'ncon0':
            metric = get_metric(fullname)
            func = 1./metric['alpha']

        elif self.funcname == 'gcon00':
            metric = get_metric(fullname)
            func = metric['gcon00']

        elif self.funcname == 'potential':
            transformBL = False #whether to plot wrt BL or PN coords
            scalesep    = True
            metric = get_metric(fullname)
            gcon = { '00':metric['gcon00'], '01':metric['gcon01'], '02':metric['gcon02'], '03':metric['gcon03'],
                '11':metric['gcon11'], '12':metric['gcon12'], '13':metric['gcon13'],
                '22':metric['gcon22'], '23':metric['gcon23'],
                '33':metric['gcon33'] }
            if( transformBL ):
                ttNZ   = np.zeros((np.shape(rr)))
                ttNZ.fill(time)
                dx_dxp = get_dx_dxp(fullname)
                metricBL = metric2BL(1, trajfile, ttNZ, rr, th, ph, dx_dxp, metric)
                func = -0.5 * ( metric['gcov00'] + 1. )
            else:
                dx_dxp  = get_dx_dxp(fullname)
                dxc_dxs = get_dxc_dxs( rr, th, ph )
                gcon    = transform_gcon( dx_dxp, gcon ) #Spherical PN Coords (Physical)
                if( self.corotate ):
                    '''
                    rewrite dx_dxp to be jacobian to corotating frame
                    dx_dxp = |   1    0 0 0 |
                            |   0    1 0 0 |
                            |   0    0 1 0 |
                            | -omega 0 0 1 |
                    '''
                    if( self.spinning ):
                        tbh_traj, omega_bin_traj = np.loadtxt(trajfile,usecols=(0,16),unpack=True) #spinning
                    else:
                        tbh_traj, omega_bin_traj = np.loadtxt(trajfile,usecols=(0,24),unpack=True) #old metrics

                    fomega_bin = interp1d(tbh_traj, omega_bin_traj, kind='linear')
                    omega_bin = fomega_bin(time)
                    dx_dxp['00'].fill(1.);         dx_dxp['01'].fill(0.); dx_dxp['02'].fill(0.); dx_dxp['03'].fill(0.)
                    dx_dxp['10'].fill(0.);         dx_dxp['11'].fill(1.); dx_dxp['12'].fill(0.); dx_dxp['13'].fill(0.)
                    dx_dxp['20'].fill(0.);         dx_dxp['21'].fill(0.); dx_dxp['22'].fill(1.); dx_dxp['23'].fill(0.)
                    dx_dxp['30'].fill(-omega_bin); dx_dxp['31'].fill(0.); dx_dxp['32'].fill(0.); dx_dxp['33'].fill(1.)
                    gcon    = transform_gcon( dx_dxp,  gcon) # Corotating Frame ( spherical coords )
                gcon    = transform_gcon( dxc_dxs, gcon) #Cartesian PN Coords (Physical)
                #Get gcov from gcon
                gcov    = get_ginverse(gcon)
                func    = -0.5 * ( gcov['00'] + 1. )
                if( scalesep ):
                    sep = h5file['Header/Grid/initial_bbh_separation'].value[0]
                    func *= sep
        elif self.funcname == 'geom_alpha':
            metric = get_metric(fullname)
            func = metric['alpha']
        elif self.funcname == 'D':
            rho = h5file['rho'].value
            gam = h5file['gamma'].value
            func = rho * gam
        elif self.funcname == 'mass_flux':
            #mass flux wrt numerical coordinates
            if userad:
                h5file2 = h5py.File(radfile, 'r')
            else:
                h5file2 = h5py.File(dumpfile, 'r')
            v1      = h5file2['v1'].value
            v2      = h5file2['v2'].value
            v3      = h5file2['v3'].value
            h5file2.close()
            #uconrr, uconth, uconph = calc_uconKS(dumpfile, v1, v2, v3) #assumes diagonal metric to get velocities
            ucon0, uconrrp, uconthp, uconphp = calc_ucon(dumpfile,v1,v2,v3) #get ucon wrt numerical coordinates

            rho = h5file['rho'].value
            gdet = h5file['gdet'].value
            global mdot, mdotthp
            func   = uconrrp * rho * gdet
            mdot = func
            mdotthp = uconthp * rho * gdet
        elif self.funcname == 'dg_dxp1' or self.funcname == 'dg_dxp2' or self.funcname == 'dg_dxp3':
            gdet = h5file['gdet'].value
            dxp1 = h5file['Header/Grid/dx1'].value[0]
            dxp2 = h5file['Header/Grid/dx2'].value[0]
            dxp3 = h5file['Header/Grid/dx3'].value[0]
            if EQUATORIAL_RUN:
                dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
                func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
            else:
                dg_dxp1, dg_dxp2, dg_dxp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
            if self.funcname == 'dg_dxp1':
                if EQUATORIAL_RUN:
                    func[:,0,:] = dg_dxp1[:,:]
                else:
                    func = dg_dxp1
            elif self.funcname == 'dg_dxp2':
                func = dg_dxp2
            elif self.funcname == 'dg_dxp3':
                if EQUATORIAL_RUN:
                    func[:,0,:] = dg_dxp3[:,:]
                else:
                    func = dg_dxp3
        elif self.funcname == 'd2g_dxp11' or self.funcname == 'd2g_dxp12' or self.funcname == 'd2g_dxp13':
            gdet = h5file['gdet'].value
            dxp1 = h5file['Header/Grid/dx1'].value[0]
            dxp2 = h5file['Header/Grid/dx2'].value[0]
            dxp3 = h5file['Header/Grid/dx3'].value[0]
            if EQUATORIAL_RUN:
                dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
                d2g_dxp11, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp3)
                func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
                if self.funcname == 'd2g_dxp11':
                    func[:,0,:] = d2g_dxp11[:,:]
                elif self.funcname == 'd2g_dxp13':
                    func[:,0,:] = d2g_dxp13[:,:]
            else:
                dg_dxp1, dg_xp2, dg_xp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
                d2g_dxp11, d2g_dxp12, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp2,dxp3)
                if self.funcname == 'd2g_dxp11':
                    func = d2g_dxp11
                elif self.funcname == 'd2g_dxp12':
                    func = d2g_dxp12
                elif self.funcname == 'd2g_dxp13':
                    func = d2g_dxp13
        elif self.funcname == 'd2g_dxp21' or self.funcname == 'd2g_dxp22' or self.funcname == 'd2g_dxp23':
            gdet = h5file['gdet'].value
            dxp1 = h5file['Header/Grid/dx1'].value[0]
            dxp2 = h5file['Header/Grid/dx2'].value[0]
            dxp3 = h5file['Header/Grid/dx3'].value[0]
            dg_dxp1, dg_xp2, dg_xp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
            d2g_dxp21, d2g_dxp22, d2g_dxp23 = np.gradient(dg_dxp2,dxp1,dxp2,dxp3)
            if self.funcname == 'd2g_dxp21':
                func = d2g_dxp21
            elif self.funcname == 'd2g_dxp22':
                func = d2g_dxp22
            elif self.funcname == 'd2g_dxp23':
                func = d2g_dxp23
        elif self.funcname == 'd2g_dxp31' or self.funcname == 'd2g_dxp32' or self.funcname == 'd2g_dxp33':
            gdet = h5file['gdet'].value
            dxp1 = h5file['Header/Grid/dx1'].value[0]
            dxp2 = h5file['Header/Grid/dx2'].value[0]
            dxp3 = h5file['Header/Grid/dx3'].value[0]
            if EQUATORIAL_RUN:
                dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
                d2g_dxp31, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp3)
                func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
                if self.funcname == 'd2g_dxp31':
                    func[:,0,:] = d2g_dxp31[:,:]
                elif self.funcname == 'd2g_dxp33':
                    func[:,0,:] = d2g_dxp33[:,:]
            else:
                dg_dxp1, dg_xp2, dg_xp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
                d2g_dxp31, d2g_dxp32, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp2,dxp3)
                if self.funcname == 'd2g_dxp31':
                    func = d2g_dxp31
                elif self.funcname == 'd2g_dxp32':
                    func = d2g_dxp32
                elif self.funcname == 'd2g_dxp33':
                    func = d2g_dxp33
        elif self.funcname == 'gradg_rat_x1' or self.funcname == 'gradg_rat_x2' or self.funcname == 'gradg_rat_x3':
            gdet = h5file['gdet'].value
            dxp1 = h5file['Header/Grid/dx1'].value[0]
            dxp2 = h5file['Header/Grid/dx2'].value[0]
            dxp3 = h5file['Header/Grid/dx3'].value[0]
            if EQUATORIAL_RUN:
                dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
                d2g_dxp11, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp3)
                d2g_dxp31, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp3)
                func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
                if self.funcname == 'gradg_rat_x1':
                    func[:,0,:] = dxp1 * d2g_dxp11[:,:] / dg_dxp1[:,:]
                elif self.funcname == 'gradg_rat_x3':
                    func[:,0,:] = dxp3 * d2g_dxp33[:,:] / dg_dxp3[:,:]
            else:
                dg_dxp1, dg_xp2, dg_xp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
                d2g_dxp11, d2g_dxp12, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp2,dxp3)
                d2g_dxp21, d2g_dxp22, d2g_dxp32 = np.gradient(dg_dxp2,dxp1,dxp2,dxp3)
                d2g_dxp31, d2g_dxp32, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp2,dxp3)
                if self.funcname == 'gradg_rat_x1':
                    func = dxp1 * d2g_dxp11 / dg_dxp1
                elif self.funcname == 'gradg_rat_x2':
                    func = dxp2 * d2g_dxp22 / dg_dxp2
                elif self.funcname == 'gradg_rat_x3':
                    func = dxp3 * d2g_dxp33 / dg_dxp3
        elif self.funcname == 'dr':
            func = np.diff(h5file['x1'].value,axis=0)
            zerrarr = np.zeros((1,np.shape(func)[1],np.shape(func)[2]))
            zerrarr[0,:,:] = func[0,:,:]
            func = np.append(func,   zerrarr,   axis=0)

        elif self.funcname == 'dth':
            x1 = h5file['x1'].value
            x2 = h5file['x2'].value
            x2 = np.diff(x2,axis=1)
            zerrarr = np.zeros((np.shape(x2)[0],1,np.shape(x2)[2]))
            zerrarr[:,0,:] = x2[:,0,:]
            x2 = np.append(x2,   zerrarr,   axis=1)
            func = x1 * x2
        elif self.funcname == 'dph':
            x1 = h5file['x1'].value
            x3 = h5file['x3'].value
            x3 = np.diff(x3,axis=2)
            zerrarr = np.zeros((np.shape(x3)[0],np.shape(x3)[1],1))
            zerrarr[:,:,0] = x3[:,:,0]
            x3 = np.append(x3,   zerrarr,   axis=2)
            func = x1 * x3
        elif self.funcname == 'testclean':
            print("Testing Cleaner Status target is 1.e-3")
            print("READ GFUNCS")
            dx1  = h5file['Header/Grid/dx1'].value[0]
            dx2  = h5file['Header/Grid/dx2'].value[0]
            dx3  = h5file['Header/Grid/dx3'].value[0]
            divb = h5file['divb'].value
            B1   = h5file['B1'].value
            B2   = h5file['B2'].value
            B3   = h5file['B3'].value
            v1   = h5file['v1'].value
            v2   = h5file['v2'].value
            v3   = h5file['v3'].value
            #Try reading gcov to do better on memory
            gcov00 = h5file['gcov00'].value
            gcov01 = h5file['gcov01'].value
            gcov02 = h5file['gcov02'].value
            gcov03 = h5file['gcov03'].value
            gcov11 = h5file['gcov11'].value
            gcov12 = h5file['gcov12'].value
            gcov13 = h5file['gcov13'].value
            gcov22 = h5file['gcov22'].value
            gcov23 = h5file['gcov23'].value
            gcov33 = h5file['gcov33'].value
            #print("get metric")
            #metric = get_metric(fullname)
            #Get min dx^i
            print("Get min dx^i")
            dxmin = min(min(dx1,dx3),min(dx2,dx3))
            #print(dx1,dx2,dx3,dxmin)
            #Get bsq
            print("calc_ucon")
            ucon0, ucon1, ucon2, ucon3 = calc_ucon(fullname,v1,v2,v3)
            print("lower ucon")
            #ucov0, ucov1, ucov2, ucov3 = lower2(metric,ucon0,ucon1,ucon2,ucon3)
            ucov0, ucov1, ucov2, ucov3 = lower(gcov00,gcov01,gcov02,gcov03,gcov11,gcov12,gcov13,gcov22,gcov23,gcov33,ucon0,ucon1,ucon2,ucon3)
            print("bcon_calc")
            bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
            print("lower bcon")
            #bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)
            bcov0, bcov1, bcov2, bcov3 = lower(gcov00,gcov01,gcov02,gcov03,gcov11,gcov12,gcov13,gcov22,gcov23,gcov33,bcon0,bcon1,bcon2,bcon3)
            print("bsq calc")
            bsq = bcov0*bcon0 + bcov1*bcon1 + bcov2*bcon2 + bcov3*bcon3
            #avoid bsq == 0
            #print(bsq)
            bsq[bsq <= 1.e-10] = divb[bsq <= 1.e-10]
            #print(bsq)
            print("return func")
            #Use cleaning target
            func = divb * dxmin / np.sqrt(bsq)
            print("Cleaned cells ", np.shape(func[func <= 1.e-3]), "Total Cells ",np.shape(func))
            #rewrite when divb is just tiny because no Bfield
            #func[divb <= 1.e-10] = 1.e-40
            #func[bsq <= 1.e-10]  = 1.e-40
            #print(func)
        else:
            if use_stat2:
                func = h5stat2[funcname]
            else:
                if( take_ratio ):
                    func1 = h5file[funcname[0]].value
                    func2 = h5file[funcname[1]].value
                    func2[func2 == 0] = 1.e-40
                    func = func1 / func2
                    self.funcname = self.funcname[0] + '/' + self.funcname[1]
                elif( relerror ):
                    func1 = h5file[funcname[0]].value
                    func2 = h5file[funcname[1]].value
                    func = np.abs(func1 - func2) / func2
                    func[func1 == 0] = 1.e-40
                    self.funcname = 'relative error (' + self.funcname[0] + ')'
                elif( compare_runs ):
                    if( compare_dir is None ):
                        h5file2n = '/home/dennis/runs/mass_sharing/a100_hydrostatic_eq_data/conn-dumps/dumps/KDHARM0.' + timeid + '.h5'
                    else:
                        h5file2n = compare_dir + '/dumps/KDHARM0.' + timeid + '.h5'
                    h5file2 = h5py.File(h5file2n,'r')
                    func1 = h5file[funcname].value
                    func2 = h5file2[funcname].value
                    #mask1 = h5file['evmask'].value
                    #mask2 = h5file['evmask'].value
                    #func1[mask1 != 2] = 1.e-40
                    #func2[mask2 != 2] = 1.e-40
                    #func = np.abs( func1 - func2) / np.abs(func1)
                    func1[func1 == 0] += 1.e-40
                    func2[func2 == 0] += 1.e-40
                    func = np.abs( 2. * ( func1 - func2 ) / ( func1 + func2 ) )
                    #func[mask1 != 2] = 0
                    #func[func1 == 0] = 1.e-40
                    self.funcname = 'relative-error-' + self.funcname
                    h5file2.close()
                else:
                    func = h5file[funcname].value
                    #mask = h5file['evmask'].value
                    #func[mask != 2] = 1.e20

        #EXTEND_CENTRAL_CUTOUT
        if(EXTEND_CENTRAL_CUTOUT):
            mask = h5file['evmask'].value
            func[mask == 3] = 0. #set the values to 0 in the mask

        #potential overplot
        if( plot_potential ):
            metric = get_metric(fullname)
            gcon = { '00':metric['gcon00'], '01':metric['gcon01'], '02':metric['gcon02'], '03':metric['gcon03'],
                    '11':metric['gcon11'], '12':metric['gcon12'], '13':metric['gcon13'],
                    '22':metric['gcon22'], '23':metric['gcon23'],
                    '33':metric['gcon33'] }
            dx_dxp  = get_dx_dxp(fullname)
            dxc_dxs = get_dxc_dxs( rr, th, ph )
            gcon    = transform_gcon( dx_dxp, gcon ) #Spherical PN Coords (Physical)
            if( corotate ):
                '''
                rewrite dx_dxp to be jacobian to corotating frame
                dx_dxp = |   1    0 0 0 |
                |   0    1 0 0 |
                |   0    0 1 0 |
                | -omega 0 0 1 |
                '''
                if( spinning ):
                    tbh_traj, omega_bin_traj = np.loadtxt(trajfile,usecols=(0,16),unpack=True) #spinning
                else:
                    tbh_traj, omega_bin_traj = np.loadtxt(trajfile,usecols=(0,24),unpack=True) #old metrics

                fomega_bin = interp1d(tbh_traj, omega_bin_traj, kind='linear')
                omega_bin = fomega_bin(time)
                dx_dxp['00'].fill(1.);         dx_dxp['01'].fill(0.); dx_dxp['02'].fill(0.); dx_dxp['03'].fill(0.)
                dx_dxp['10'].fill(0.);         dx_dxp['11'].fill(1.); dx_dxp['12'].fill(0.); dx_dxp['13'].fill(0.)
                dx_dxp['20'].fill(0.);         dx_dxp['21'].fill(0.); dx_dxp['22'].fill(1.); dx_dxp['23'].fill(0.)
                dx_dxp['30'].fill(-omega_bin); dx_dxp['31'].fill(0.); dx_dxp['32'].fill(0.); dx_dxp['33'].fill(1.)
                gcon    = transform_gcon( dx_dxp,  gcon) # Corotating Frame ( spherical coords )
            gcon    = transform_gcon( dxc_dxs, gcon) #Cartesian PN Coords (Physical)
            #Get gcov from gcon
            gcov    = get_ginverse(gcon)
            pot_func    = -0.5 * ( gcov['00'] + 1. )
            # function to plot
            if ith is not None:
                pot_slice = pot_func[:,ith,:]
            else:
                pot_slice = pot_func[:,:,iph]


        #dennis modifying
        #func /= np.max(func[rr < 100.])

        # function to plot
        if ith is not None:
            func_slice = func[:,ith,:]
        else:
            func_slice = func[:,:,iph]

        # Get BH masses if plotting trajs before closing file
        if( self.plot_bhs_traj ):
            m_bh1 = h5file['Header/Grid/m_bh1'].value[0]
            m_bh2 = h5file['Header/Grid/m_bh2'].value[0]
            if( m_bh1 != 0.5 ):
                print('Unequal Masses')
                print('m_bh1', m_bh1)
                print('m_bh2', m_bh2)

        h5file.close()

    ############################################################################
    def do_the_plot(self, func_slice, fillwedge=True, show_plot=True, dolog=False, savepng=False, use_num_coord=False, plot_grid = False):
        """
        do_the_plot function

        Description
        -----------
        Plot the function
        """

        if self.show_plot is False:
            # Turn interactive plotting off
            plt.ioff()

        if self.background == 'black':
            grid_colour = 'white'
        else:
            grid_colour = 'black'


        if (self.fillwedge and self.ith is not None):
            rr_slice   = np.append(rr_slice,   np.array([rr_slice[:,0]]).T,   axis=1)
            ph_slice   = np.append(ph_slice,   np.array([ph_slice[:,0]]).T,   axis=1)
            func_slice = np.append(func_slice, np.array([func_slice[:,0]]).T, axis=1)
            if( plot_potential ):
                pot_slice = np.append(pot_slice, np.array([pot_slice[:,0]]).T, axis=1)


        if(fillcutout and ith is not None):
            zerarr = np.zeros((1,rr_slice.shape[1]))
            phval = np.linspace(0,2.*np.pi,rr_slice.shape[1])
            rr_slice = np.append(rr_slice, zerarr, axis=0)
            zerarr[0,:] = phval[:]
            ph_slice = np.append(ph_slice, zerarr, axis=0)
            zerarr[0,:] = np.average(func_slice[0,:])
            #print(zerarr)
            func_slice = np.append(func_slice, zerarr,axis=0)
            #roll so index is 0 of func_slice
            rr_slice   = np.roll(rr_slice,1,axis=0)
            ph_slice   = np.roll(ph_slice,1,axis=0)
            func_slice = np.roll(func_slice,1,axis=0)
            if( plot_potential ):
                zerarr[0,:] = np.average(pot_slice[0,:])
                pot_slice = np.append(pot_slice, zerarr, axis=0)
                pot_slice = np.roll(pot_slice,1,axis=0)


        #Test equator first
        if ith  == int((len(rr[0,:,0]))/2):
            xx_slice = rr_slice*np.cos(ph_slice)   #*np.sin(th_slice)
            yy_slice = rr_slice*np.sin(ph_slice)   #*np.sin(th_slice)
        elif ith is not None:
            xx_slice = rr_slice*np.cos(ph_slice)   *np.sin(th_slice)
            yy_slice = rr_slice*np.sin(ph_slice)   *np.sin(th_slice)
        else:
            xx_slice = rr_slice*np.sin(th_slice)   *np.cos(ph_slice)
            yy_slice = rr_slice*np.sin(th_slice)   *np.sin(ph_slice)
            xx_slice = np.sqrt(xx_slice*xx_slice + yy_slice*yy_slice) #really r
            yy_slice = rr_slice*np.cos(th_slice) #really z
            #dennis hack here for cartesian coords (long-box testing)
            #xx_slice = rr_slice
            #yy_slice = th_slice
        if plottitle is None:
            plottitle = self.funcname
            if dolog:
                plottitle = 'log10|' + plottitle + '|'

        ########################## set default parameters ##########################

        if xmax is None:
            if use_num_coord:
                xmax = len(func_slice[:,0])
            else:
                xmax = rmax
        if ymax is None:
            if use_num_coord:
                ymax = len(func_slice[0,:])
            else:
                ymax = rmax
        if zmax is None:
            if use_num_coord:
                zmax = len(func_slice[0,:])
            else:
                zmax = 0.5*rmax
        if xmin is None:
            if use_num_coord:
                xmin = 0
            else:
                if ith is not None:
                    xmin = -rmax
                else:
                    xmin = 0
        if ymin is None:
            if use_num_coord:
                ymin = 0
            else:
                ymin = -rmax
        if zmin is None:
            if use_num_coord:
                zmin = 0
            else:
                zmin = -0.5*rmax

        if dirout is None:
            dirout = os.getcwd()

        if( plot_bhs_traj ):
            if( spinning ):
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,1,2,4,5),unpack=True)
            else:
                #old metrics
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,4,5,6,7),unpack=True)

            fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
            fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
            fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
            fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

            theta = th_slice[0,0]

            xbh1  = fxbh1(time)/np.sin(theta)
            ybh1  = fybh1(time)/np.sin(theta)
            xbh2  = fxbh2(time)/np.sin(theta)
            ybh2  = fybh2(time)/np.sin(theta)

        #xbh1 = 10.
        #ybh1 = 0.
        #xbh2 = -10.
        #ybh2 = 0.
            #print('corotate ?', corotate)
            if (corotate):
                phase = np.arctan2(ybh1,xbh1)
                #renormalize phase s.t phase \in (0,2pi)
                if phase < 0.:
                    phase = 2. * np.pi - np.abs(phase)
                print('adjusting phase by ', phase * 180. / np.pi, ' degrees')
                #we now need to subtract this angle off from all angle arrays
                ph_slice = ph_slice - phase
                #reset the x,y slices
                if ith  == int((len(rr[0,:,0]))/2):
                    xx_slice = rr_slice*np.cos(ph_slice)   #*np.sin(th_slice)
                    yy_slice = rr_slice*np.sin(ph_slice)   #*np.sin(th_slice)
                elif ith is not None:
                    xx_slice = rr_slice*np.cos(ph_slice)   *np.sin(th_slice)
                    yy_slice = rr_slice*np.sin(ph_slice)   *np.sin(th_slice)
                else:
                    xx_slice = rr_slice*np.sin(th_slice)   *np.cos(ph_slice)
                    xx_slice = np.abs(xx_slice)
                    yy_slice = rr_slice*np.cos(th_slice)
                #move BHs back to x axis according to phase
                xbh1 = np.sqrt( xbh1*xbh1 + ybh1*ybh1 )
                ybh1 = 0.
                xbh2 = -np.sqrt( xbh2*xbh2 + ybh2*ybh2 )
                ybh2 = 0.
            if plotcentre is None:
                pass
            elif plotcentre is 'BH1':
                xmin = xbh1 - rmax
                xmax = xbh1 + rmax
                ymin = ybh1 - rmax
                ymax = ybh1 + rmax
            elif plotcentre is 'BH2':
                xmin = xbh2 - rmax
                xmax = xbh2 + rmax
                ymin = ybh2 - rmax
                ymax = ybh2 + rmax
            else:
                sys.exit('invalid plotcentre option')

        if normalize_func is True:
            func_slice /= np.max(func_slice)


        if dolog:
            func_slice[func_slice == 0] += 1.e-40       # so that there is no log of zero...
            func_slice = np.log10(np.abs(func_slice))


        if minf is None:
            minf = np.min(func_slice)
        if maxf is None:
            maxf = np.max(func_slice)


        if( plot_potential ):
            if pminf is None:
                pminf = np.min(pot_slice)
            if pmaxf is None:
                pmaxf = np.max(pot_slice)


        # we can now do the plotting

        fig1    = plt.figure(nwin)
        fig1.clf()

        if ax is None:
            ax   = fig1.add_subplot(111, aspect='equal', axisbg=background)

        if axislabels is True:
            if use_num_coord:
                if ith is not None:
                    ax.set_xlabel('x3')
                    ax.set_ylabel('x1')
                else:
                    ax.set_xlabel('x2')
                    ax.set_ylabel('x1')
            else:
                ax.set_xlabel('')
                ax.set_ylabel('')
        ax.set_autoscalex_on(False)
        ax.set_autoscaley_on(False)

        if ith is not None:
            ax.axis([xmin,xmax,ymin,ymax])
        else:
            ax.axis([xmin,xmax,zmin,zmax])

        if xticks is not None:
            ax.set_xticks(xticks)
        if yticks is not None:
            ax.set_yticks(yticks)
        if yticksloc is 'right':
            ax.yaxis.tick_right()

        cmap  = plt.cm.Spectral_r
        #cmap = plt.cm.gray
        darkgrey = '#202020'
        purple   = '#800080'
        darkblue = '#0000A0'
        Colormap.set_under(cmap,color=darkgrey)
        Colormap.set_over(cmap,color='w')

        levels = np.linspace(minf,maxf,nlev)
        if use_num_coord:
            CS = ax.contourf(func_slice,levels, cmap=cmap, extend='both')
        else:
            CS = ax.contourf(xx_slice,yy_slice,func_slice,levels, cmap=cmap, extend='both')
        if( plot_potential ):
            matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
            plevels = np.linspace(pminf,pmaxf,pnlev)
            CS_pot = ax.contour(xx_slice,yy_slice,pot_slice,plevels,colors='k',extend='both')
            if( pot_labels ):
                plt.clabel(CS_pot, fontsize=pfontsize)

        if(plot_grid):
            for i in range(len(xx_slice[0,:]))[::nperplot]:
                ax.plot(xx_slice[:,i],yy_slice[:,i], color=grid_colour)

            for i in range(len(xx_slice[:,0]))[::nperplot]:
                ax.plot(xx_slice[i,:],yy_slice[i,:], color=grid_colour)

        if self.ubh:
            ubh_horizon = True
        else:
            ubh_horizon = False

        if(ubh_horizon):
            rhor = 2.
            iscor = 6.
            bh = plt.Circle((0,0),rhor,color='k')
            isco = plt.Circle((0,0),iscor,color='k',linestyle='dashed',fill=False)
            ax.add_artist(bh)
            ax.add_artist(isco)

        if(plot_bhs_traj):
            green_led = '#5DFC0A'
            #rhor = 0.5 #only for equal mass rat
            rhor1 = m_bh1
            rhor2 = m_bh2
            #actually plot the BHS
            bh1 = plt.Circle((xbh1,ybh1), rhor1, color=grid_colour)
            bh2 = plt.Circle((xbh2,ybh2), rhor2, color=grid_colour)
            #bh1 = plt.Circle((xbh1,ybh1), rhor1, color='k')
            #bh2 = plt.Circle((xbh2,ybh2), rhor1, color='k')
            #bh1 = plt.Circle((xbh1,ybh1), rhor1, color='k',fill=False)
            #bh2 = plt.Circle((xbh2,ybh2), rhor2, color='k',fill=False)

            ax.add_artist(bh1)
            ax.add_artist(bh2)

            if(visc_circles):
                sep = np.sqrt( (xbh1 - xbh2)*(xbh1 - xbh2) + (ybh1 - ybh2)*(ybh1 - ybh2) )
                print("sep = ",sep)
                #isco1 = plt.Circle((xbh1,ybh1),3.,color=green_led,fill=False)
                #isco2 = plt.Circle((xbh2,ybh2),3.,color=green_led,fill=False)
                #isco1  = plt.Circle((xbh1,ybh1),5.*m_bh1,color=grid_colour,fill=False)
                #isco2  = plt.Circle((xbh2,ybh2),5.*m_bh2,color=grid_colour,fill=False)
                #tidal1 = plt.Circle((xbh1,ybh1),0.3*np.power(m_bh2/m_bh1,-0.3)*sep,color=grid_colour,fill=False,linestyle='dashed')
                #tidal2 = plt.Circle((xbh2,ybh2),0.3*np.power(m_bh2/m_bh1, 0.3)*sep,color=grid_colour,fill=False,linestyle='dashed')

                isco1  = plt.Circle((xbh1,ybh1),5.*m_bh1,color='k',fill=False,linestyle='solid')
                isco2  = plt.Circle((xbh2,ybh2),5.*m_bh2,color='k',fill=False,linestyle='solid')
                #dsig   = plt.Circle((xbh1,ybh1),0.22*sep,color='white',fill=False,linestyle='solid')
                tidal1 = plt.Circle((xbh1,ybh1),0.3*np.power(m_bh2/m_bh1,-0.3)*sep,color='k',fill=0,linestyle='dashed')
                tidal2 = plt.Circle((xbh2,ybh2),0.3*np.power(m_bh2/m_bh1, 0.3)*sep,color='k',fill=0,linestyle='dashed')

                #cool1 = plt.Circle((xbh1,ybh1),0.45*np.power(m_bh2/m_bh1,-0.3)*sep,color='k',fill=False,linestyle='solid')
                #cool2 = plt.Circle((xbh2,ybh2),0.45*np.power(m_bh2/m_bh1, 0.3)*sep,color='k',fill=False,linestyle='solid')


                #Keep these
                ax.add_artist(isco1)
                ax.add_artist(isco2)
                ax.add_artist(tidal1)
                ax.add_artist(tidal2)
                #ax.add_artist(dsig)
                #ax.add_artist(cool1)
                #ax.add_artist(cool2)

                if R1T0 is not None:
                    Transinner1 = plt.Circle((xbh1,ybh1),R1T0,color='black',fill=False,linestyle='dashdot')
                    Transinner2 = plt.Circle((xbh2,ybh2),R1T0,color='black',fill=False,linestyle='dashdot')

                    ax.add_artist(Transinner1); ax.add_artist(Transinner2)

                #FUDGE1 = plt.Circle((xbh1,ybh1),0.1*m_bh1,color='white',fill=False,linestyle='dashed')
                #FUDGE2 = plt.Circle((xbh2,ybh2),0.1*m_bh2,color='white',fill=False,linestyle='dashed')
                #ax.add_artist(FUDGE1)
                #ax.add_artist(FUDGE2)

        if(plot_horizon):
            yellow_neon = '#F3F315'
            r0          = 2.

            if ith is not None:
                phi0   = np.linspace(0, 2*np.pi, 100)
                xh1    = r0 * np.cos(phi0)
                yh1    = r0 * np.sin(phi0)
                ax.plot( xh1, yh1, color=yellow_neon)
            else:
                theta = np.linspace(np.pi*0.5, -np.pi*0.5, 100)
                ax.plot(r0*np.cos(theta), r0*np.sin(theta), color=yellow_neon)

        if(plot_slosh_box):
            if not corotate:
                print("Must Corotate for sloshing patches")
            else:
                sep = np.sqrt( (xbh1 - xbh2)*(xbh1 - xbh2) + (ybh1 - ybh2)*(ybh1 - ybh2) )
                slosh = matplotlib.patches.Rectangle((0-0.2*sep,0-0.3*sep),0.4*sep,0.6*sep,fill=False,color='k',linestyle='dashed')
                ax.add_artist(slosh)

        if plottitle is not 'notitle':
            title = plottitle + '  t =  ' + str('%0.1f' %time)
            ax.set_title('%s' %title)

        # ticks for the colorbar
        if ticks_range is 'best':
            rticks = MaxNLocator(nticks+1)
        elif ticks_range is 'minmax':
            rticks = np.linspace(minf,maxf,nticks)

        if scale is not 'off':
            fig1.colorbar(CS,ticks=rticks,format='%1.4g') #colorbar removal dennis here
        if axis is 'off':
            ax.set_axis_off()#axis off here dennis


        if ith is not None:
            figname = dirout+'/'+plottitle + namesuffix + '_it='+timeid + '_ith='+ '%04d' %ith
        else:
            figname = dirout+'/'+plottitle + namesuffix + '_it='+timeid + '_iph='+ '%04d' %iph

        if(savepng):
            fig1.savefig('%s.png' %figname, dpi=200, bbox_inches='tight')


        if show_plot:
            plt.show()

        if (get_time):
            if retfunc:
                return time, func_slice
            else:
                return time, CS
        else:
            if retfunc:
                return func_slice
            else:
                return CS

