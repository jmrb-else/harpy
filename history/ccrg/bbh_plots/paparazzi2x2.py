# Description:
#   -- 2x2 set of plots:
#       1. BH1 equatorial
#       2. BH2 equatorial
#       3. BH1 x-z
#       4. BH2 x-z
# %%
# Loading modules
import matplotlib
matplotlib.use('Agg')
import h5py
import sys
import matplotlib.pyplot as plt
from matplotlib import animation as am
import numpy as np
import scipy as sp
from scipy.interpolate import interp1d
import astropy.constants as ks
import astropy.units as u
import math
import glob
import os
import os.path
import errno
sys.path.insert(0, 'module/')
# import module_snapshot as ms
# import module
# import module_physics as phys
# np.set_printoptions(threshold=np.nan)
matplotlib.rcParams.update({'font.size': 30})

darkgrey = '#202020'
purple   = '#800080'
darkblue = '#0000A0'
varNames = ['density', 'uu', 'bsq', 'divb', 'g_tt', 'pressure', 'plasma_beta', 'gamma', 'v1', 'v2', 'v3', 'B1', 'B2', 'B3']
cmap_list = [plt.cm.Spectral_r, plt.cm.coolwarm, plt.cm.magma, plt.cm.YlGnBu_r, plt.cm.brg, plt.cm.viridis]

# %%
#-----  Dirs and files -----
# fileDir ='/scratch1/07876/jmruebec/ccrgHarm3D/stage/dumps'
fileDir = '/Users/jesus/Codes/ccrgHarm3D/stage'
# fileDir = os.curdir
os.chdir(fileDir)
outFiles_p0 = ['BBH-DISK-CART-ORIGIN.patch_0.{:06d}.h5'.format(i) for i in range(0, 8)]
trajDir = fileDir
trajFile = trajDir + '/BBH-DISK-CART-ORIGIN_bbh_trajectory.out'

# %%
#-----  Parameters  -----
plot_bhs_traj = True
spinning = True
bh_circle = 'ring'
up_and_under = True
logscale = True
vars = [varNames[0], varNames[0], varNames[0], varNames[0]]
nlevs=64

# %%
#-----  Data reading and plotting  -----
for count in range(len(outFiles_p0)):
    filename_path_p0 = os.path.join(fileDir, outFiles_p0[count])
    filename_path_p0 = os.path.abspath(os.path.realpath(filename_path_p0))
    filename_p0      = os.path.basename(filename_path_p0)

    f_p0 = h5py.File(filename_path_p0, 'r')

    c_flag_p0 = np.array(f_p0['cflag'])

    time = np.array(f_p0['/Header/Grid/t'])[0]
    print("[TIME]=", time, filename_p0)

    totalsize_p0 = np.zeros(4)
    gridlen_p0 = np.zeros(4)
    
    totalsize_p0[1] = np.array(f_p0['/Header/Grid/totalsize1'])[0]
    totalsize_p0[2] = np.array(f_p0['/Header/Grid/totalsize2'])[0]
    totalsize_p0[3] = np.array(f_p0['/Header/Grid/totalsize3'])[0]

    gridlen_p0[1] = np.array(f_p0['/Header/Grid/gridlength1'])[0]
    gridlen_p0[2] = np.array(f_p0['/Header/Grid/gridlength2'])[0]
    gridlen_p0[3] = np.array(f_p0['/Header/Grid/gridlength3'])[0]

    x_p0 = np.array(f_p0['x1'])
    y_p0 = np.array(f_p0['x2'])
    z_p0 = np.array(f_p0['x3'])
    x_p0 = np.array(x_p0)
    y_p0 = np.array(y_p0)
    z_p0 = np.array(z_p0)
    vars_p0 = []
    vmin_p0 = []
    vmax_p0 = []
    titles = []
    for var in vars:
        if var == 'pressure':
            titles.append(r"$\log_{10}(p)$")
            var_tmp = np.array(f_p0['uu']) * (np.array(f_p0['gamma']) - 1.0)
            vmin_p0.append(np.amin(var_tmp))
            vmax_p0.append(np.amax(var_tmp))
            np.amax(var_tmp)
            cmap_num = 3
            vars_p0.append(var_tmp)
        elif var == 'density':
            titles.append(r"$\log_{10}(\rho)$")
            cmap_num = 3
            var_tmp = np.log10(np.array(f_p0['rho']))
            vmin_p0.append(-6)#(np.amin(var_tmp))
            vmax_p0.append(-4)#(np.amax(var_tmp))
            vars_p0.append(var_tmp)
        elif var == 'plasma_beta':
            var_tmp = np.array(f_p0['uu']) * (np.array(f_p0['gamma']) - 1.0)
            var_tmp = 2. * var_tmp / np.array(f_p0['bsq'])
            vars_p0.append(np.log10(var_tmp))
            vmin_p0.append(-6)#(np.amin(var_tmp))
            vmax_p0.append(-4)#(np.amax(var_tmp))
        elif var == 'divb':
            vars_p0.append(np.absolute(np.array(f_p0['divb'])))
        elif var == 'B3':
            titles.append(r"$\log_{10}(|B^{\mathrm{z}}|)$")
            cmap_num = 3
            var_tmp = np.log10(np.array(f_p0['rho']))
            vmin_p0.append(-6)#(np.amin(var_tmp))
            vmax_p0.append(-4)#(np.amax(var_tmp))
            vars_p0.append(var_tmp)

        else:
            exit

    #---> Trajectory
    if plot_bhs_traj:
        if spinning:
            m_bh1 = 0.5
            m_bh2 = 0.5
            chi_bh1 = 0.9
            chi_bh2 = 0.9
            r_hor1 = np.sqrt( 2. * m_bh1 * ( m_bh1 + np.sqrt( np.power(m_bh1, 2.) - np.power((m_bh1 * chi_bh1), 2.))))
            r_hor2 = np.sqrt( 2. * m_bh2 * ( m_bh2 + np.sqrt( np.power(m_bh2, 2.) - np.power((m_bh2 * chi_bh2), 2.))))
            r_ergo1 = np.sqrt( 4. * np.power(m_bh1, 2.) + np.power((m_bh1 * chi_bh1), 2.) )
            r_ergo2 = np.sqrt( 4. * np.power(m_bh2, 2.) + np.power((m_bh2 * chi_bh2), 2.) )
            tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajFile,usecols=(0,1,2,4,5),unpack=True)
        else:
            tbh_traj, m_bh1, m_bh2, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajFile,usecols=(0,1,2,4,5,6,7), unpack=True)
            m_bh1 = m_bh1[0]
            m_hor2 = m_bh2[0]
            r_hor1 = m_bh1
            r_hor2 = m_bh2
        fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear', fill_value=0.0)
        fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear', fill_value=0.0)
        fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear', fill_value=0.0)
        fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear', fill_value=0.0)
        #theta = th_slice[0,0]
        zbh1  = 0.
        zbh2  = 0.
        xbh1  = fxbh1(time+0.1)
        ybh1  = fybh1(time+0.1)
        xbh2  = fxbh2(time+0.1)
        ybh2  = fybh2(time+0.1)

    #------  2D cuts -----
    cut_x_p0 = int(totalsize_p0[1] / 2)
    cut_y_p0 = int(totalsize_p0[2] / 2)
    cut_z_p0 = int(totalsize_p0[3] / 2)

    #----- Localizeing bhs -----
    cut_x_bh1 = int(totalsize_p0[1] * (0.5 * gridlen_p0[1] - xbh1) / gridlen_p0[1])
    cut_y_bh1 = int(totalsize_p0[2] * (0.5 * gridlen_p0[2] - ybh1) / gridlen_p0[2])
    cut_x_bh2 = int(totalsize_p0[1] * (0.5 * gridlen_p0[1] - xbh2) / gridlen_p0[1])
    cut_y_bh2 = int(totalsize_p0[2] * (0.5 * gridlen_p0[2] - ybh2) / gridlen_p0[2])

    #-----  Global grid  -----
    xs_p0 = x_p0[:,        cut_y_p0, cut_z_p0]
    ys_p0 = y_p0[cut_x_p0, :,        cut_z_p0]
    zs_p0 = z_p0[cut_x_p0, cut_y_p0, :       ]
    Y_p0, X_p0 = np.meshgrid(ys_p0, xs_p0)
    Z2_p0, X2_p0 = np.meshgrid(zs_p0, xs_p0)

    #-----  Grid for BH1  -----
    xs_bh1 = x_p0[:,         cut_y_bh1, cut_z_p0]
    ys_bh1 = y_p0[cut_x_bh1, :,         cut_z_p0]
    zs_bh1 = z_p0[cut_x_bh1, cut_y_bh1, :]
    Y_bh1, X_bh1 = np.meshgrid(ys_bh1, xs_bh1)
    Z2_bh1, X2_bh1 = np.meshgrid(zs_bh1, xs_bh1)

    #-----  Grid for BH1  -----
    xs_bh2 = x_p0[:,         cut_y_bh2, cut_z_p0]
    ys_bh2 = y_p0[cut_x_bh2, :,         cut_z_p0]
    zs_bh2 = z_p0[cut_x_bh2, cut_y_bh2, :]
    Y_bh2, X_bh2 = np.meshgrid(ys_bh2, xs_bh2)
    Z2_bh2, X2_bh2 = np.meshgrid(zs_bh2, xs_bh2)


    #-----  Plotting  -----
    fig, ax = plt.subplots(2, 2, figsize=(35, 27), sharey=False, sharex=False)

    cmap = cmap_list[cmap_num]
    if up_and_under:
        cmap = cmap.with_extremes(under=darkgrey, over='w')
        extend = 'both'
    else:
        extend = 'neither'

    levs = np.linspace(vmin_p0[0], vmax_p0[0], num=nlevs, endpoint=True)
    quad1 = ax[0,0].contourf(X_bh1, Y_bh1,
                             vars_p0[0][:, :, cut_z_p0],
                             levs, cmap=cmap, extend=extend)
    levs = np.linspace(vmin_p0[1], vmax_p0[1], num=nlevs, endpoint=True)
    quad2 = ax[1,0].contourf(X_bh2, Y_bh2,
                             vars_p0[1][:, :, cut_z_p0],
                             levs, cmap=cmap, extend=extend)
    levs = np.linspace(vmin_p0[2], vmax_p0[2], num=nlevs, endpoint=True)
    quad3 = ax[0,1].contourf(X2_bh1, Z2_bh1,
                             vars_p0[2][:, cut_y_bh1, :],
                             levs, cmap=cmap, extend=extend)
    levs = np.linspace(vmin_p0[2], vmax_p0[2], num=nlevs, endpoint=True)
    quad4 = ax[1,1].contourf(X2_bh2, Z2_bh2,
                             vars_p0[3][:, cut_y_bh2, :],
                             levs, cmap=cmap, extend=extend)

    ax[0,0].set_title(titles[0])
    ax[1,0].set_title(titles[1])
    ax[0,1].set_title(titles[2])
    ax[1,1].set_title(titles[3])

    ax[0,0].axis('square')
    ax[0,0].set_xlim(xbh2-5, xbh2+5)
    ax[0,0].set_ylim(ybh2-5, ybh2+5)
    ax[1,0].axis('square')
    ax[1,0].set_xlim(xbh1-5, xbh1+5)
    ax[1,0].set_ylim(ybh1-5, ybh1+5)
    ax[0,1].axis('square')
    ax[0,1].set_xlim(xbh2-5, xbh2+5)
    ax[0,1].set_ylim(-5, 5)
    ax[1,1].axis('square')
    ax[1,1].set_xlim(xbh1-5, xbh1+5)
    ax[1,1].set_ylim(-5, 5)

    circ_col = 'r'
    if bh_circle == 'circle':
        bh1 = plt.Circle((xbh1, ybh1), r_hor1, color=circ_col, zorder=1, alpha=0.8)
        bh2 = plt.Circle((xbh2, ybh2), r_hor2, color=circ_col, zorder=1, alpha=0.8)
        ax[0,0].add_artist(bh1)
        ax[0,0].add_artist(bh2)
        bh1 = plt.Circle((xbh1, zbh1), r_hor1, color=circ_col, zorder=1, alpha=0.8)
        ax[1,1].add_artist(bh1)
        bh2 = plt.Circle((xbh2, zbh2), r_hor2, color=circ_col, zorder=1, alpha=0.8)
        ax[0,1].add_artist(bh2)
    elif bh_circle == 'ring':
        bh1 = plt.Circle((xbh1, ybh1), r_hor1, ec=circ_col, zorder=10, lw=2., fill=False)
        bh2 = plt.Circle((xbh2, ybh2), r_hor2, ec=circ_col, zorder=10, lw=2., fill=False)
        ax[1,0].add_artist(bh1)
        ax[0,0].add_artist(bh2)
        bh1 = plt.Circle((xbh1, zbh1), r_hor1, ec=circ_col, zorder=10, lw=2.5, fill=False)
        ax[1,1].add_artist(bh1)
        bh2 = plt.Circle((xbh2, zbh2), r_hor2, ec=circ_col, zorder=10, lw=2.5, fill=False)
        ax[0,1].add_artist(bh2)
        if spinning:
            bh1 = plt.Circle((xbh1, ybh1), r_ergo1, ec=circ_col, ls='--', zorder=10, lw=2., fill=False, alpha=0.8)
            bh2 = plt.Circle((xbh2, ybh2), r_ergo2, ec=circ_col, ls='--', zorder=10, lw=2., fill=False, alpha=0.8)
            ax[1,0].add_artist(bh1)
            ax[0,0].add_artist(bh2)
            bh1 = plt.Circle((xbh1, zbh1), r_ergo1, ec=circ_col, ls='--', zorder=10, lw=2., fill=False, alpha=0.8)
            ax[1,1].add_artist(bh1)
            bh2 = plt.Circle((xbh2, zbh2), r_ergo2, ec=circ_col, ls='--', zorder=10, lw=2., fill=False, alpha=0.8)
            ax[0,1].add_artist(bh2)
    else:
        pass

    fig.colorbar(quad1, ax=ax[0,0])#, ticks=np.arange(-6.0, 7.0, step=1))
    fig.colorbar(quad2, ax=ax[1,0])#, ticks=np.arange(-6.0, 7.0, step=1))
    fig.colorbar(quad3, ax=ax[0,1])#, ticks=np.arange(-6.0, 7.0, step=1))
    fig.colorbar(quad4, ax=ax[1,1])#, ticks=np.arange(-6.0, 7.0, step=1))

    # plt.suptitle(r"Global patch (Left two columns) /Local patch (Right two columns) at "+str(time)[:5], fontsize=100)
    plt.suptitle('t = {:>5.1f}'.format(time))
    plt.tight_layout()

    filename_png = filename_p0.strip('.h5')
    plt.savefig("snap_" + filename_png+".png")
    plt.close()
    # plt.show()

# %%
