
from __future__ import division
from __future__ import print_function

import numpy as np
import h5py

#from scipy.interpolate import griddata
from scipy.interpolate import interp1d #, interp2d
#from scipy import random

import matplotlib
import matplotlib.pyplot as plt 
from matplotlib.colors import Colormap

from matplotlib.ticker import MaxNLocator

import os,sys


# the following module allows one to stop at a given point in the file with the
# command Tracer()(). this allows us to access the local functions and variables
# (closest thing to 'stop' in IDL).
from IPython.core.debugger import Tracer
#Tracer()()


def calc_uconKS(h5filename, v1, v2, v3, idx):

    h5file = h5py.File(h5filename, 'r')
    dx_dxp11 = h5file['dx_dxp11'][idx]
    dx_dxp12 = h5file['dx_dxp12'][idx]
    dx_dxp13 = h5file['dx_dxp13'][idx]
    dx_dxp21 = h5file['dx_dxp21'][idx]
    dx_dxp22 = h5file['dx_dxp22'][idx]
    dx_dxp23 = h5file['dx_dxp23'][idx]
    dx_dxp31 = h5file['dx_dxp31'][idx]
    dx_dxp32 = h5file['dx_dxp32'][idx]
    dx_dxp33 = h5file['dx_dxp33'][idx]

    uconrr = dx_dxp11 * v1 + dx_dxp12 * v2 + dx_dxp13 * v3
    uconth = dx_dxp21 * v1 + dx_dxp22 * v2 + dx_dxp23 * v3
    uconph = dx_dxp31 * v1 + dx_dxp32 * v2 + dx_dxp33 * v3

    h5file.close()

    return uconrr, uconth, uconph



def polp(itime=0, dirname=None, funcname=None, func0=None, filename=None,
         ith=None, iph=0,
         rmin=None, rmax=None, minf=None, maxf=None,
         xmax=None,ymax=None,xmin=None,ymin=None,zmax=None,zmin=None,
         nlev=256, nticks=10, ticks_range='minmax',
         use_num_coord=False,
         plot_grid=False, 
         nperplot=10, savepng=False, dirout=None, dolog=False, show_plot=True,
         plottitle=None, userad=False,
         plot_bhs_traj=False, trajfile=None, plot_horizon=False,
         nwin=0, background='black', namesuffix='', plotcentre=None,
         ax=None, axislabels=True, xticks=None, yticks=None, yticksloc='left', retfunc=True, fillwedge=True, N1step=1, N2step=1, N3step=1
     ):

    # nticks    -> number of ticks for the colorbar
    # plot_grid ->  plot numerical grid?

    if show_plot is False:
        # Turn interactive plotting off
        plt.ioff()

    if use_num_coord:
        plot_horizon  = False
        plot_bhs_traj = False
        plot_grid     = False

    if ith is None:
        plot_bhs_traj = False

    if background is 'black':
        grid_colour = 'white'
    else:
        grid_colour = 'black'

    if plotcentre is not None:
         plot_bhs_traj=True


    if dirname is None:
        dirname = os.getcwd()  # should tell me my current working directory,
                               # *not* where this script is located

    if trajfile is None:
        trajfile = dirname + '/KDHARM0_bbh_trajectory.out'
    timeid = '%06d' %itime


    if filename is None:
        dumpfile = dirname + '/dumps/' + 'KDHARM0.' +  timeid + '.h5'
        radfile  = dirname + '/dumps/' + 'KDHARM0.RADFLUX.' +  timeid + '.h5'
    else:
        dumpfile = filename

    fullname = dumpfile

    h5file   = h5py.File(fullname, 'r')

    rr   = h5file['x1']   # 'x1' is the name of the dataset inside the hdf5 file
    ph   = h5file['x3']
    th   = h5file['x2']
    # we get the time level:
    time = h5file['Header/Grid/t'][0]

    # create mask for indexing arrays
    N1, N2, N3 = np.shape(rr)
    if ith is 'equator':
        ith = N2//2
    if ith is not None:
        idx = (slice(0,N1,N1step), ith, slice(0,N3,N3step))
    else:
        idx = (slice(0,N1,N1step), slice(0,N2,N2step), iph)

    # set grid coordinate functions
    rr_slice = rr[idx]
    ph_slice = ph[idx]
    th_slice = th[idx]

    if rmin is None:
        rmin = np.min(rr)
    if rmax is None:
        rmax = np.max(rr)

    if userad:
        fullname = radfile
        h5file.close()
        h5file = h5py.File(fullname, 'r')

    # if we've specified something for func0 we will plot it instead.
    if func0 is not None:
        func = h5file[func0][idx]

    elif funcname is 'uconrr':
        if userad:
            h5file2 = h5py.File(radfile, 'r')
        else:
            h5file2 = h5py.File(dumpfile, 'r')
        v1      = h5file2['v1'][idx]
        v2      = h5file2['v2'][idx]
        v3      = h5file2['v3'][idx]
        h5file2.close()
        uconrr, uconth, uconph = calc_uconKS(dumpfile, v1, v2, v3, idx)
        uconth = 0
        uconph = 0
        func   = uconrr
    elif funcname is 'uconth':
        if userad:
            h5file2 = h5py.File(radfile, 'r')
        else:
            h5file2 = h5py.File(dumpfile, 'r')
        v1      = h5file2['v1'][idx]
        v2      = h5file2['v2'][idx]
        v3      = h5file2['v3'][idx]
        h5file2.close()
        uconrr, uconth, uconph = calc_uconKS(dumpfile, v1, v2, v3, idx)
        uconrr = 0
        uconph = 0
        func   = uconth
    elif funcname is 'uconph':
        if userad:
            h5file2 = h5py.File(radfile, 'r')
        else:
            h5file2 = h5py.File(dumpfile, 'r')
        v1      = h5file2['v1'][idx]
        v2      = h5file2['v2'][idx]
        v3      = h5file2['v3'][idx]
        h5file2.close()
        uconrr, uconth, uconph = calc_uconKS(dumpfile, v1, v2, v3, idx)
        uconth = 0
        uconrr = 0
        func   = uconph

    else:
        func = h5file[funcname][idx]

    # function to plot
    func_slice = func
    # if ith is not None:
    #     func_slice = func[:,ith,:]
    # else:
    #     func_slice = func[:,:,iph]

    h5file.close()


    if(fillwedge and ith is not None):
        rr_slice   = np.append(rr_slice,   np.array([rr_slice[:,0]]).T,   axis=1)
        ph_slice   = np.append(ph_slice,   np.array([ph_slice[:,0]]).T,   axis=1)
        func_slice = np.append(func_slice, np.array([func_slice[:,0]]).T, axis=1)


    if ith is not None:
        xx_slice = rr_slice*np.cos(ph_slice)   #*np.sin(th_slice)
        yy_slice = rr_slice*np.sin(ph_slice)   #*np.sin(th_slice)
    else:
        xx_slice = rr_slice*np.sin(th_slice)   #*np.cos(ph_kslice)
        xx_slice = np.abs(xx_slice)
        yy_slice = rr_slice*np.cos(th_slice)


    if plottitle is None:
        plottitle = funcname
        if dolog:
            plottitle = 'log10|' + plottitle + '|'

    ########################## set default parameters ##########################

    if xmax is None:
        if use_num_coord:
            xmax = len(func_slice[:,0])
        else:
            xmax = rmax
    if ymax is None:
        if use_num_coord:
            ymax = len(func_slice[0,:])
        else:
            ymax = rmax
    if zmax is None:
        if use_num_coord:
            zmax = len(func_slice[0,:])
        else:
            zmax = 0.5*rmax
    if xmin is None:
        if use_num_coord:
            xmin = 0
        else:
            if ith is not None:
                xmin = -rmax
            else:
                xmin = 0
    if ymin is None:
        if use_num_coord:
            ymin = 0
        else:
            ymin = -rmax
    if zmin is None:
        if use_num_coord:
            zmin = 0
        else:
            zmin = -0.5*rmax

    if dirout is None:
        dirout = os.getcwd()



    ############################################################################

    if(plot_bhs_traj):

        tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,4,5,6,7),unpack=True)

        fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
        fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
        fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
        fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

        theta = th_slice[0,0]

        xbh1  = fxbh1(time)/np.sin(theta)
        ybh1  = fybh1(time)/np.sin(theta)
        xbh2  = fxbh2(time)/np.sin(theta)
        ybh2  = fybh2(time)/np.sin(theta)

        if plotcentre is None:
            pass
        elif plotcentre is 'BH1':
            xmin = xbh1 - rmax
            xmax = xbh1 + rmax
            ymin = ybh1 - rmax
            ymax = ybh1 + rmax
        elif plotcentre is 'BH2':
            xmin = xbh2 - rmax
            xmax = xbh2 + rmax
            ymin = ybh2 - rmax
            ymax = ybh2 + rmax
        else:
            sys.exit('invalid plotcentre option')


    if dolog:
        func_slice[func_slice == 0] += 1.e-40       # so that there is no log of zero...
        func_slice = np.log10(np.abs(func_slice))

    if minf is None:
        minf = np.min(func_slice)
    if maxf is None:
        maxf = np.max(func_slice)


    # we can now do the plotting

    fig1    = plt.figure(nwin)
    fig1.clf()

    if ax is None:
        ax   = fig1.add_subplot(111, aspect='equal') 

    if axislabels is True:
        if use_num_coord:
            if ith is not None:
                ax.set_xlabel('x3')
                ax.set_ylabel('x1')
            else:
                ax.set_xlabel('x2')
                ax.set_ylabel('x1')
        else:
            ax.set_xlabel('')
            ax.set_ylabel('')
    ax.set_autoscalex_on(False)
    ax.set_autoscaley_on(False)

    if ith is not None:
        ax.axis([xmin,xmax,ymin,ymax])
    else:
        ax.axis([xmin,xmax,zmin,zmax])

    if xticks is not None:
        ax.set_xticks(xticks)
    if yticks is not None:
        ax.set_yticks(yticks)
    if yticksloc is 'right':
        ax.yaxis.tick_right()

    cmap  = plt.cm.jet    # default
    darkgrey = '#202020'
    purple   = '#800080'
    darkblue = '#0000A0'
    Colormap.set_under(cmap,color=darkgrey) 
    Colormap.set_over(cmap,color='w') 

    levels = np.linspace(minf,maxf,nlev)
    if use_num_coord:
        CS = ax.contourf(func_slice,levels, cmap=cmap, extend='both')
    else:
        CS = ax.contourf(xx_slice,yy_slice,func_slice,levels, cmap=cmap, extend='both')


    if(plot_grid):
        for i in range(len(xx_slice[0,:]))[::nperplot]:
            ax.plot(xx_slice[:,i],yy_slice[:,i], color=grid_colour)
        for i in range(len(xx_slice[:,0]))[::nperplot]:
            ax.plot(xx_slice[i,:],yy_slice[i,:], color=grid_colour)

    if(plot_bhs_traj):
        green_led = '#5DFC0A'
        rhor = 0.5

        # bh1 = plt.Circle((xbh1,ybh1), rhor, color=grid_colour)
        # bh2 = plt.Circle((xbh2,ybh2), rhor, color=grid_colour)
        bh1 = plt.Circle((xbh1,ybh1), rhor, color=green_led)
        bh2 = plt.Circle((xbh2,ybh2), rhor, color=green_led)

        ax.add_artist(bh1)
        ax.add_artist(bh2)


    if(plot_horizon):
        yellow_neon = '#F3F315'
        r0          = 2.

        if ith is not None:
            phi0   = np.linspace(0, 2*np.pi, 100)
            xh1    = r0 * np.cos(phi0)
            yh1    = r0 * np.sin(phi0)
            ax.plot( xh1, yh1, color=yellow_neon)
        else:
            theta = np.linspace(np.pi*0.5, -np.pi*0.5, 100)
            ax.plot(r0*np.cos(theta), r0*np.sin(theta), color=yellow_neon)

    if plottitle is not 'notitle':
        title = plottitle + '  t =  ' + str('%0.1f' %time)
        ax.set_title('%s' %title)

    # ticks for the colorbar
    if ticks_range is 'best':
        rticks = MaxNLocator(nticks+1)
    elif ticks_range is 'minmax':
        rticks = np.linspace(minf,maxf,nticks)

    fig1.colorbar(CS,ticks=rticks,format='%1.4g') 


    if ith is not None:
        figname = dirout+'/'+plottitle + namesuffix + '_it='+timeid + '_ith='+ '%04d' %ith
    else:
        figname = dirout+'/'+plottitle + namesuffix + '_it='+timeid + '_iph='+ '%04d' %iph

    if(savepng):
        fig1.savefig('%s.png' %figname, dpi=200)


    if show_plot:
        plt.show()

    if retfunc:
        return func_slice
    else:
        return CS

