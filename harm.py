###############################################################################
###############################################################################
#  Routines useful for reading in datasets from HARM3d HDF5 files.
###############################################################################
###############################################################################

from __future__ import division
from __future__ import print_function

import numpy as np
import os,sys
import h5py
import glob
import re
from scipy.interpolate import interp1d
from . import get_sim_info

###############################################################################
###############################################################################

def lookup_full_name(nickname,h5file):
    all_h5_objs = []
    h5file.visit(all_h5_objs.append)
    regname1 = '\/'+nickname+'$'
    regname2 = '^'+nickname+'$'
    fullname = [ x for x in all_h5_objs if (re.search(regname1,x) or re.search(regname2,x)) ]
    return fullname

###############################################################################
def get_val(nickname, h5file):
    rout_name = 'get_val'
    fullnames = lookup_full_name(nickname, h5file)
    if( len(fullnames) != 1 ) :
        sys.exit(rout_name+"():  No datasets  matching the expression!  Exitting..." + nickname)
        val = -666
    else:
        val = h5file[fullnames[0]][...]

    return val

###############################################################################
def check_if_dataset_exists(nickname,h5file):
    fullnames = lookup_full_name(nickname,h5file)
    if( len(fullnames) != 1 ) :
        return False
    else:
        return True


###############################################################################
#  Reads in a slice of data given the "ind_fil"  index filter, whose
#   elements are negative if you want to select all of a range.
###############################################################################
def read_3d_hdf5_slice(fullname,slice_obj,h5file):

    val = np.squeeze(h5file[fullname][slice_obj])

    return val


#  ###############################################################################
#  #  Reads in a slice of data given the "ind_fil"  index filter, whose
#  #   elements are negative if you want to select all of a range.
#  ###############################################################################
#  def read_3d_hdf5_slice(fullname,ind_fil,h5file):
#
#      if (ind_fil[0] < 0) :
#          if (ind_fil[1] < 0) :
#              if (ind_fil[2] < 0) :
#                  val = h5file[fullname][:,:,:]
#              else :
#                  val = h5file[fullname][:,:,ind_fil[2]]
#          else :
#              if (ind_fil[2] < 0) :
#                  val = h5file[fullname][:,ind_fil[1],:]
#              else :
#                 val = h5file[fullname][:,ind_fil[1],ind_fil[2]]
#      else :
#          if (ind_fil[1] < 0) :
#              if (ind_fil[2] < 0) :
#                  val = h5file[fullname][ind_fil[0],:,:]
#              else :
#                  val = h5file[fullname][ind_fil[0],:,ind_fil[2]]
#          else :
#              if (ind_fil[2] < 0) :
#                  val = h5file[fullname][ind_fil[0],ind_fil[1],:]
#              else :
#                  val = h5file[fullname][ind_fil[0],ind_fil[1],ind_fil[2]]
#
#      return val
#
###############################################################################
def get_dx_dxp(slice_obj,h5file,assume_simple_time=False):
    dxdxp10 = read_3d_hdf5_slice('dx_dxp10',slice_obj,h5file)
    dxdxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
    dxdxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
    dxdxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
    dxdxp20 = read_3d_hdf5_slice('dx_dxp20',slice_obj,h5file)
    dxdxp21 = read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
    dxdxp22 = read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
    dxdxp23 = read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)
    dxdxp30 = read_3d_hdf5_slice('dx_dxp30',slice_obj,h5file)
    dxdxp31 = read_3d_hdf5_slice('dx_dxp31',slice_obj,h5file)
    dxdxp32 = read_3d_hdf5_slice('dx_dxp32',slice_obj,h5file)
    dxdxp33 = read_3d_hdf5_slice('dx_dxp33',slice_obj,h5file)

    #Here we assume that dt_dxp = delta[0,0]
    if( assume_simple_time ) :
        dxdxp00 = np.zeros((np.shape(dxdxp10))); dxdxp00.fill(1.)
        dxdxp01 = np.zeros((np.shape(dxdxp10))); dxdxp02 = np.zeros((np.shape(dxdxp10))); dxdxp03 = np.zeros((np.shape(dxdxp10)))

    dx_dxp = { '00':dxdxp00, '01':dxdxp01, '02':dxdxp02, '03':dxdxp03,
               '10':dxdxp10, '11':dxdxp11, '12':dxdxp12, '13':dxdxp13,
               '20':dxdxp20, '21':dxdxp21, '22':dxdxp22, '23':dxdxp23,
               '30':dxdxp30, '31':dxdxp31, '32':dxdxp32, '33':dxdxp33 }

    return dx_dxp

###############################################################################
def get_header(h5file):

    dx0 = h5file['Header/Grid/dx0'][...]
    dx1 = h5file['Header/Grid/dx1'][...]
    dx2 = h5file['Header/Grid/dx2'][...]
    dx3 = h5file['Header/Grid/dx3'][...]
    m_bh1 = h5file['Header/Grid/m_bh1'][...]
    m_bh2 = h5file['Header/Grid/m_bh2'][...]
    initial_bbh_separation = h5file['Header/Grid/initial_bbh_separation'][...]

    Header = {'dx0':dx0, 'dx1':dx1, 'dx2':dx2, 'dx3':dx3, 'm_bh1':m_bh1, 'm_bh2':m_bh2, 'initial_bbh_separation':initial_bbh_separation}
    return Header

###############################################################################
def get_metric(slice_obj,h5file,gdump=None):

    if( gdump is not None ):
        gcov00 = read_3d_hdf5_slice('gcov300',slice_obj,gdump)
        gcov01 = read_3d_hdf5_slice('gcov301',slice_obj,gdump)
        gcov02 = read_3d_hdf5_slice('gcov302',slice_obj,gdump)
        gcov03 = read_3d_hdf5_slice('gcov303',slice_obj,gdump)
        gcov11 = read_3d_hdf5_slice('gcov311',slice_obj,gdump)
        gcov12 = read_3d_hdf5_slice('gcov312',slice_obj,gdump)
        gcov13 = read_3d_hdf5_slice('gcov313',slice_obj,gdump)
        gcov22 = read_3d_hdf5_slice('gcov322',slice_obj,gdump)
        gcov23 = read_3d_hdf5_slice('gcov323',slice_obj,gdump)
        gcov33 = read_3d_hdf5_slice('gcov333',slice_obj,gdump)
        gdet   = read_3d_hdf5_slice('gdet3',slice_obj,gdump)
    else :
        gcov00 = read_3d_hdf5_slice('gcov00',slice_obj,h5file)
        gcov01 = read_3d_hdf5_slice('gcov01',slice_obj,h5file)
        gcov02 = read_3d_hdf5_slice('gcov02',slice_obj,h5file)
        gcov03 = read_3d_hdf5_slice('gcov03',slice_obj,h5file)
        gcov11 = read_3d_hdf5_slice('gcov11',slice_obj,h5file)
        gcov12 = read_3d_hdf5_slice('gcov12',slice_obj,h5file)
        gcov13 = read_3d_hdf5_slice('gcov13',slice_obj,h5file)
        gcov22 = read_3d_hdf5_slice('gcov22',slice_obj,h5file)
        gcov23 = read_3d_hdf5_slice('gcov23',slice_obj,h5file)
        gcov33 = read_3d_hdf5_slice('gcov33',slice_obj,h5file)
        gdet   = read_3d_hdf5_slice(  'gdet',slice_obj,h5file)

    # copied straight from Scott's idl set_metric_gen2.pro

    gcon00 =  gcov11*(gcov22*gcov33 - gcov23*gcov23) - gcov12*(gcov12*gcov33 - gcov13*gcov23) + gcov13*(gcov12*gcov23 - gcov13*gcov22)
    gcon01 = -gcov01*(gcov22*gcov33 - gcov23*gcov23) + gcov02*(gcov12*gcov33 - gcov13*gcov23) - gcov03*(gcov12*gcov23 - gcov13*gcov22)
    gcon02 =  gcov01*(gcov12*gcov33 - gcov23*gcov13) - gcov02*(gcov11*gcov33 - gcov13*gcov13) + gcov03*(gcov11*gcov23 - gcov13*gcov12)
    gcon03 = -gcov01*(gcov12*gcov23 - gcov22*gcov13) + gcov02*(gcov11*gcov23 - gcov12*gcov13) - gcov03*(gcov11*gcov22 - gcov12*gcov12)
    gcon11 =  gcov00*(gcov22*gcov33 - gcov23*gcov23) - gcov02*(gcov02*gcov33 - gcov03*gcov23) + gcov03*(gcov02*gcov23 - gcov03*gcov22)
    gcon12 = -gcov00*(gcov12*gcov33 - gcov23*gcov13) + gcov02*(gcov01*gcov33 - gcov03*gcov13) - gcov03*(gcov01*gcov23 - gcov03*gcov12)
    gcon13 =  gcov00*(gcov12*gcov23 - gcov22*gcov13) - gcov02*(gcov01*gcov23 - gcov02*gcov13) + gcov03*(gcov01*gcov22 - gcov02*gcov12)
    gcon22 =  gcov00*(gcov11*gcov33 - gcov13*gcov13) - gcov01*(gcov01*gcov33 - gcov03*gcov13) + gcov03*(gcov01*gcov13 - gcov03*gcov11)
    gcon23 = -gcov00*(gcov11*gcov23 - gcov12*gcov13) + gcov01*(gcov01*gcov23 - gcov02*gcov13) - gcov03*(gcov01*gcov12 - gcov02*gcov11)
    gcon33 =  gcov00*(gcov11*gcov22 - gcov12*gcov12) - gcov01*(gcov01*gcov22 - gcov02*gcov12) + gcov02*(gcov01*gcov12 - gcov02*gcov11)

    det = gcov00*gcon00 + gcov01*gcon01 + gcov02*gcon02 + gcov03*gcon03
    det[det==0] += 1.e-10    # to not prevent further computations in case a
                             # singular matrix is found at some points. this
                             # happens for instance when using excision...
    gcon00[gcon00==0] += -1.e-10

    inv_det = 1.0 / det
    det = 0

    gcon00 *= inv_det
    gcon01 *= inv_det
    gcon02 *= inv_det
    gcon03 *= inv_det
    gcon11 *= inv_det
    gcon12 *= inv_det
    gcon13 *= inv_det
    gcon22 *= inv_det
    gcon23 *= inv_det
    gcon33 *= inv_det

    inv_det = 0

    alpha = np.sqrt(-1./gcon00)
    beta1 =  -gcon01/gcon00
    beta2 =  -gcon02/gcon00
    beta3 =  -gcon03/gcon00


    metric = {'gdet':gdet,
              'gcov00':gcov00, 'gcov01':gcov01, 'gcov02':gcov02, 'gcov03':gcov03,
              'gcov11':gcov11, 'gcov12':gcov12, 'gcov13':gcov13,
              'gcov22':gcov22, 'gcov23':gcov23,
              'gcov33':gcov33,
              'gcon00':gcon00, 'gcon01':gcon01, 'gcon02':gcon02, 'gcon03':gcon03,
              'gcon11':gcon11, 'gcon12':gcon12, 'gcon13':gcon13,
              'gcon22':gcon22, 'gcon23':gcon23,
              'gcon33':gcon33,
              'alpha':alpha, 'beta1':beta1, 'beta2':beta2, 'beta3':beta3 }

    return metric


###############################################################################
# copied straight from Scott's idl calc_ucon_mks2.pro
def calc_ucon(slice_obj, h5file, v1, v2, v3,gdump=None):

    rout_name = 'calc_ucon'

    metric = get_metric(slice_obj, h5file, gdump=gdump)

    if( np.shape(metric['gcov11']) != np.shape(v1) ) :
        print("shape 1 = ", np.shape(metric['gcov11']), "  shape2  = ", np.shape(v1) )
        sys.exit(rout_name+"():  shapes are not right...")


    vsq = metric['gcov11']*v1*v1 + metric['gcov22']*v2*v2 + metric['gcov33']*v3*v3 + 2.*( v1*(metric['gcov12']*v2 + metric['gcov13']*v3) + metric['gcov23']*v2*v3 )

    gamma = np.sqrt(1. + vsq)
    vsq   = 0
    alpha = metric['alpha']
    beta1 = metric['beta1']
    beta2 = metric['beta2']
    beta3 = metric['beta3']

    ucon0 = gamma/alpha
    #gamma = 0
    ucon1 = v1 - ucon0 * beta1
    ucon2 = v2 - ucon0 * beta2
    ucon3 = v3 - ucon0 * beta3

    return ucon0, ucon1, ucon2, ucon3

###############################################################################
# similar to calc_ucon() but this version reads in v1-v3 too:
def calc_ucon2(slice_obj, h5file,gdump=None):

    rout_name = 'calc_ucon2'

    v1 = read_3d_hdf5_slice('v1',slice_obj,h5file)
    v2 = read_3d_hdf5_slice('v2',slice_obj,h5file)
    v3 = read_3d_hdf5_slice('v3',slice_obj,h5file)

    metric = get_metric(slice_obj, h5file,gdump=gdump)

    if( np.shape(metric['gcov11']) != np.shape(v1) ) :
        print("shape 1 = ", np.shape(metric['gcov11']), "  shape2  = ", np.shape(v1) )
        sys.exit(rout_name+"():  shapes are not right...")


    vsq = metric['gcov11']*v1*v1 + metric['gcov22']*v2*v2 + metric['gcov33']*v3*v3 + 2.*( v1*(metric['gcov12']*v2 + metric['gcov13']*v3) + metric['gcov23']*v2*v3 )

    gamma = np.sqrt(1. + vsq)
    vsq   = 0
    alpha = metric['alpha']
    beta1 = metric['beta1']
    beta2 = metric['beta2']
    beta3 = metric['beta3']

    ucon0 = gamma/alpha
    #gamma = 0
    ucon1 = v1 - ucon0 * beta1
    ucon2 = v2 - ucon0 * beta2
    ucon3 = v3 - ucon0 * beta3

    return ucon0, ucon1, ucon2, ucon3, v1, v2, v3, metric

###############################################################################
# similar to calc_ucon() but this version reads in v1-v3 too:
def calc_ucon3(slice_obj, h5file, v1, v2, v3, metric=None,gdump=None):

    rout_name = 'calc_ucon3'

    if metric is None :
        metric = get_metric(slice_obj, h5file,gdump=gdump)

    if( np.shape(metric['gcov11']) != np.shape(v1) ) :
        print("shape 1 = ", np.shape(metric['gcov11']), "  shape2  = ", np.shape(v1) )
        sys.exit(rout_name+"():  shapes are not right...")


    vsq = metric['gcov11']*v1*v1 + metric['gcov22']*v2*v2 + metric['gcov33']*v3*v3 + 2.*( v1*(metric['gcov12']*v2 + metric['gcov13']*v3) + metric['gcov23']*v2*v3 )

    gamma = np.sqrt(1. + vsq)
    vsq   = 0
    alpha = metric['alpha']
    beta1 = metric['beta1']
    beta2 = metric['beta2']
    beta3 = metric['beta3']

    ucon0 = gamma/alpha
    #gamma = 0
    ucon1 = v1 - ucon0 * beta1
    ucon2 = v2 - ucon0 * beta2
    ucon3 = v3 - ucon0 * beta3

    return ucon0, ucon1, ucon2, ucon3, metric
###############################################################################
# calculates the contravariant 4-vector from harm3d's  v1-3  grid functions
def calc_ucon_bl(slice_obj, h5file, metric=None, h5gdump=None, gslice_obj=None ):

    rout_name = 'calc_ucon_bl'

    v1  = read_3d_hdf5_slice( 'v1',slice_obj,h5file)
    v2  = read_3d_hdf5_slice( 'v2',slice_obj,h5file)
    v3  = read_3d_hdf5_slice( 'v3',slice_obj,h5file)

    sltmp = slice_obj
    if( gslice_obj ) :
        sltmp = gslice_obj

    if h5gdump  :
        geom_coord_h5 = h5gdump
    else :
        geom_coord_h5 = h5file

    if metric is None :
        if h5gdump is None:
            sys.exit(rout_name+"():  we need some geometry data somehow...")

        metric = get_metric(sltmp, geom_coord_h5,gdump=h5gdump)

    if( np.shape(metric['gcov11']) != np.shape(v1) ) :
        print("shape 1 = ", np.shape(metric['gcov11']), "  shape2  = ", np.shape(v1) )
        sys.exit(rout_name+"():  shapes are not right...")

    vsq = metric['gcov11']*v1*v1 + metric['gcov22']*v2*v2 + metric['gcov33']*v3*v3 + 2.*( v1*(metric['gcov12']*v2 + metric['gcov13']*v3) + metric['gcov23']*v2*v3 )

    gamma = np.sqrt(1. + vsq)
    vsq   = 0
    alpha = metric['alpha']
    beta1 = metric['beta1']
    beta2 = metric['beta2']
    beta3 = metric['beta3']

    ucon0 = gamma/alpha
    ucon1 = v1 - ucon0 * beta1
    ucon2 = v2 - ucon0 * beta2
    ucon3 = v3 - ucon0 * beta3
    gamma = v1 = v2 = v3 = 0

    ucon0_ks, ucon1_ks, ucon2_ks, ucon3_ks = uconp2ucon_dynamic(sltmp, geom_coord_h5, ucon0,ucon1,ucon2,ucon3)
    ucon0, ucon1, ucon2, ucon3 = 0

    ucon0_bl, ucon1_bl, ucon2_bl, ucon3_bl = ks2bl_con2(sltmp, geom_coord_h5, ucon0_ks, ucon1_ks, ucon2_ks, ucon3_ks)
    ucon0_ks, ucon1_ks, ucon2_ks, ucon3_ks = 0

    return ucon0_bl, ucon1_bl, ucon2_bl, ucon3_bl, metric


###############################################################################
def lower(gcov00,gcov01,gcov02,gcov03,gcov11,gcov12,gcov13,gcov22,gcov23,gcov33,ucon0,ucon1,ucon2,ucon3):

    ucov0 = gcov00*ucon0 + gcov01*ucon1 + gcov02*ucon2 + gcov03*ucon3
    ucov1 = gcov01*ucon0 + gcov11*ucon1 + gcov12*ucon2 + gcov13*ucon3
    ucov2 = gcov02*ucon0 + gcov12*ucon1 + gcov22*ucon2 + gcov23*ucon3
    ucov3 = gcov03*ucon0 + gcov13*ucon1 + gcov23*ucon2 + gcov33*ucon3

    return ucov0, ucov1, ucov2, ucov3

###############################################################################
def lower2(metric,ucon0,ucon1,ucon2,ucon3):

    gcov00 = metric['gcov00']; gcov01 = metric['gcov01']; gcov02 = metric['gcov02']; gcov03 = metric['gcov03']
    gcov11 = metric['gcov11']; gcov12 = metric['gcov12']; gcov13 = metric['gcov13']
    gcov22 = metric['gcov22']; gcov23 = metric['gcov23']
    gcov33 = metric['gcov33']
    ucov0 = gcov00*ucon0 + gcov01*ucon1 + gcov02*ucon2 + gcov03*ucon3
    ucov1 = gcov01*ucon0 + gcov11*ucon1 + gcov12*ucon2 + gcov13*ucon3
    ucov2 = gcov02*ucon0 + gcov12*ucon1 + gcov22*ucon2 + gcov23*ucon3
    ucov3 = gcov03*ucon0 + gcov13*ucon1 + gcov23*ucon2 + gcov33*ucon3

    return ucov0, ucov1, ucov2, ucov3

###############################################################################
def calc_ucov(slice_obj, h5file, v1, v2, v3,gdump=None):
    metric = get_metric(slice_obj, h5file,gdump=gdump)

    vsq = metric['gcov11']*v1*v1 + metric['gcov22']*v2*v2 + metric['gcov33']*v3*v3 \
        + 2.*(    metric['gcov12']*v1*v2 + metric['gcov13']*v1*v3
               + metric['gcov23']*v2*v3 )

    gamma = np.sqrt(1. + vsq)
    vsq   = 0
    alpha = metric['alpha']
    beta1 = metric['beta1']
    beta2 = metric['beta2']
    beta3 = metric['beta3']

    #contravariant 4-velocity
    u0 = gamma/alpha
    #gamma = 0
    u1 = v1 - u0 * beta1
    u2 = v2 - u0 * beta2
    u3 = v3 - u0 * beta3

    #Lower 4-velocity
    u0, u1, u2, u3 = lower2(metric,u0,u1,u2,u3)

    return u0, u1, u2, u3


###############################################################################
def calc_ucon_ucov(slice_obj, h5file, v1, v2, v3,gdump=None):
    metric = get_metric(slice_obj, h5file,gdump=gdump)

    vsq = metric['gcov11']*v1*v1 + metric['gcov22']*v2*v2 + metric['gcov33']*v3*v3 \
        + 2.*(    metric['gcov12']*v1*v2 + metric['gcov13']*v1*v3
               + metric['gcov23']*v2*v3 )

    gamma = np.sqrt(1. + vsq)
    vsq   = 0
    alpha = metric['alpha']
    beta1 = metric['beta1']
    beta2 = metric['beta2']
    beta3 = metric['beta3']

    #contravariant 4-velocity
    ucon0 = gamma/alpha
    #gamma = 0
    ucon1 = v1 - ucon0 * beta1
    ucon2 = v2 - ucon0 * beta2
    ucon3 = v3 - ucon0 * beta3

    #Lower 4-velocity
    ucov0, ucov1, ucov2, ucov3 = lower2(metric, ucon0, ucon1, ucon2, ucon3)

    return ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric



###############################################################################
def bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3):

    inv_ut = 1. / ucon0
    bcon0  = B1*ucov1 + B2*ucov2 + B3*ucov3
    bcon1  = (B1 + bcon0*ucon1)*inv_ut
    bcon2  = (B2 + bcon0*ucon2)*inv_ut
    bcon3  = (B3 + bcon0*ucon3)*inv_ut

    return bcon0, bcon1, bcon2, bcon3



###############################################################################
def uconp2ucon(slice_obj, h5file, ucon0, ucon1, ucon2, ucon3, gdump=None):

    if( gdump is not None ):
        dx_dxp11 = read_3d_hdf5_slice('dxdxp11',slice_obj,gdump)
        dx_dxp12 = read_3d_hdf5_slice('dxdxp12',slice_obj,gdump)
        dx_dxp13 = read_3d_hdf5_slice('dxdxp13',slice_obj,gdump)
        dx_dxp21 = read_3d_hdf5_slice('dxdxp21',slice_obj,gdump)
        dx_dxp22 = read_3d_hdf5_slice('dxdxp22',slice_obj,gdump)
        dx_dxp23 = read_3d_hdf5_slice('dxdxp23',slice_obj,gdump)
        dx_dxp31 = read_3d_hdf5_slice('dxdxp31',slice_obj,gdump)
        dx_dxp32 = read_3d_hdf5_slice('dxdxp32',slice_obj,gdump)
        dx_dxp33 = read_3d_hdf5_slice('dxdxp33',slice_obj,gdump)
    else:
        dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
        dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
        dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
        dx_dxp21 = read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
        dx_dxp22 = read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
        dx_dxp23 = read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)
        dx_dxp31 = read_3d_hdf5_slice('dx_dxp31',slice_obj,h5file)
        dx_dxp32 = read_3d_hdf5_slice('dx_dxp32',slice_obj,h5file)
        dx_dxp33 = read_3d_hdf5_slice('dx_dxp33',slice_obj,h5file)

    ucontt = ucon0
    uconrr = dx_dxp11 * ucon1 + dx_dxp12 * ucon2 + dx_dxp13 * ucon3
    uconth = dx_dxp21 * ucon1 + dx_dxp22 * ucon2 + dx_dxp23 * ucon3
    uconph = dx_dxp31 * ucon1 + dx_dxp32 * ucon2 + dx_dxp33 * ucon3

    return ucontt, uconrr, uconth, uconph

###############################################################################
def uconp2ucon_dynamic(slice_obj, h5file,ucon0,ucon1,ucon2,ucon3):
    dx_dxp10 = read_3d_hdf5_slice('dx_dxp10',slice_obj,h5file)
    dx_dxp11 = read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
    dx_dxp12 = read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
    dx_dxp13 = read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
    dx_dxp20 = read_3d_hdf5_slice('dx_dxp20',slice_obj,h5file)
    dx_dxp21 = read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
    dx_dxp22 = read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
    dx_dxp23 = read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)
    dx_dxp30 = read_3d_hdf5_slice('dx_dxp30',slice_obj,h5file)
    dx_dxp31 = read_3d_hdf5_slice('dx_dxp31',slice_obj,h5file)
    dx_dxp32 = read_3d_hdf5_slice('dx_dxp32',slice_obj,h5file)
    dx_dxp33 = read_3d_hdf5_slice('dx_dxp33',slice_obj,h5file)

    #assume that tprime = t (problem for ucontt?)
    ucontt = ucon0
    uconrr = dx_dxp10 * ucon0 + dx_dxp11 * ucon1 + dx_dxp12 * ucon2 + dx_dxp13 * ucon3
    uconth = dx_dxp20 * ucon0 + dx_dxp21 * ucon1 + dx_dxp22 * ucon2 + dx_dxp23 * ucon3
    uconph = dx_dxp30 * ucon0 + dx_dxp31 * ucon1 + dx_dxp32 * ucon2 + dx_dxp33 * ucon3

    return ucontt, uconrr, uconth, uconph

###############################################################################
def calc_uconKS(slice_obj, h5file, v1, v2, v3,gdump=None):
    ucon0, ucon1, ucon2, ucon3 = calc_ucon(slice_obj, h5file, v1, v2, v3, gdump=gdump)
    ucontt, uconrr, uconth, uconph = uconp2ucon(slice_obj, h5file, ucon0, ucon1, ucon2, ucon3, gdump=gdump)
    return ucontt, uconrr, uconth, uconph

###############################################################################
def calc_Qmri(slice_obj, h5file, gdump=None, gam=None, omega=None, dx1=None, dx2=None, dx3=None, h5file2=None):
    if( h5file2 is None ) :
        h5file2 = h5file
    v1     = read_3d_hdf5_slice( 'v1',slice_obj,h5file2)
    v2     = read_3d_hdf5_slice( 'v2',slice_obj,h5file2)
    v3     = read_3d_hdf5_slice( 'v3',slice_obj,h5file2)
    rho    = read_3d_hdf5_slice('rho',slice_obj,h5file2)
    uu     = read_3d_hdf5_slice( 'uu',slice_obj,h5file )
    B1     = read_3d_hdf5_slice( 'B1',slice_obj,h5file )
    B2     = read_3d_hdf5_slice( 'B2',slice_obj,h5file )
    B3     = read_3d_hdf5_slice( 'B3',slice_obj,h5file )
    bsq    = read_3d_hdf5_slice('bsq',slice_obj,h5file )
    ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
    rho_gdet = metric['gdet'] * rho
    metric = 0
    bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
    v1 = v2 = v3 = 0
    B1 = B2 = B3 = 0

    if gdump is None:
        gdump = h5file
    if( gam is None ) :
        gam = get_val('gam', gdump)
    if( dx1 is None ) :
        dx1 = get_val('dx1', gdump)
    if( dx2 is None ) :
        dx2 = get_val('dx2', gdump)
    if( dx3 is None ) :
        dx3 = get_val('dx3', gdump)
    if( omega is None ) :
        r_func = read_3d_hdf5_slice('x1',slice_obj,h5file)
        omega = r_func**(-1.5)
        r_func = 0
    else :
        if( np.shape(omega) != np.shape(rho) ) :
            sys.exit("Shape mismatch with omega")

    f0 = omega * np.sqrt(rho + bsq + uu*gam)
    rho = bsq = uu = 0

    f1 = 2.*np.pi / dx1[0]
    f2 = 2.*np.pi / dx2[0]
    f3 = 2.*np.pi / dx3[0]

    Qmri1 = f1 * np.abs(bcon1) / f0
    Qmri2 = f2 * np.abs(bcon2) / f0
    Qmri3 = f3 * np.abs(bcon3) / f0

    return Qmri1, Qmri2, Qmri3, rho_gdet

###############################################################################
def ks2bl_con(slice_obj, h5file,uconrrKS,uconthKS,uconphKS,gdump=None):
    metric = get_metric(slice_obj, h5file,gdump=gdump)

    #normalize to find ucon00
    AA = metric['gcov00']
    BB = 2. * (metric['gcov01']*uconrrKS + metric['gcov03']*uconphKS)
    CC = metric['gcov11']*uconrrKS*uconrrKS + metric['gcov22']*uconthKS*uconthKS + metric['gcov33']*uconphKS*uconphKS + 1.

    uconttKS = (-BB + np.sqrt(BB*BB - 4.*AA*CC))/(2.*AA)

    #convert to BL coordinates (assuming total mass = 1)
    spin = h5file['/Header/Grid/a'][0]
    rr   = read_3d_hdf5_slice('x1',slice_obj,h5file)
    th   = read_3d_hdf5_slice('x2',slice_obj,h5file)
    ph   = read_3d_hdf5_slice('x3',slice_obj,h5file)

    delta = rr * rr - 2. * rr + spin * spin

    dtBL_drKS = -2. * rr / delta
    dphiBL_drKS = -spin / delta

    ucon0BL = uconttKS + dtBL_drKS * uconrrKS
    ucon1BL = uconrrKS
    ucon2BL = uconthKS
    ucon3BL = uconphKS + dphiBL_drKS * uconrrKS

    return ucon0BL, ucon1BL, ucon2BL, ucon3BL


###############################################################################
def ks2bl_con2(slice_obj, h5file,uconttKS,uconrrKS,uconthKS,uconphKS):

    #convert to BL coordinates (assuming total mass = 1)
    spin = h5file['/Header/Grid/a'][0]
    rr = read_3d_hdf5_slice('x1', slice_obj, h5file)
    th = read_3d_hdf5_slice('x2', slice_obj, h5file)
    ph = read_3d_hdf5_slice('x3', slice_obj, h5file)

    delta = rr * rr - 2. * rr + spin * spin

    dtBL_drKS = -2*rr/delta
    dphiBL_drKS = -spin/delta

    ucon0BL = uconttKS + dtBL_drKS*uconrrKS
    ucon1BL = uconrrKS
    ucon2BL = uconthKS
    ucon3BL = uconphKS + dphiBL_drKS*uconrrKS

    return ucon0BL, ucon1BL, ucon2BL, ucon3BL


###############################################################################
def get_grid(h5file):

    NG           = h5file['/Header/Grid/NG'][0]
    totalsize1   = h5file['/Header/Grid/totalsize1'][0]
    totalsize2   = h5file['/Header/Grid/totalsize2'][0]
    totalsize3   = h5file['/Header/Grid/totalsize3'][0]
    startx1      = h5file['/Header/Grid/startx1'][0]
    startx2      = h5file['/Header/Grid/startx2'][0]
    startx3      = h5file['/Header/Grid/startx3'][0]
    dx1          = h5file['/Header/Grid/dx1'][0]
    dx2          = h5file['/Header/Grid/dx2'][0]
    dx3          = h5file['/Header/Grid/dx3'][0]

    idim = totalsize1
    jdim = totalsize2
    kdim = totalsize3

    i1  = NG + np.arange(idim)
    xp1 = startx1 + (i1 + 0.5) * dx1
    i2  = NG + np.arange(jdim)
    xp2 = startx2 + (i2 + 0.5) * dx2
    i3  = NG + np.arange(kdim)
    xp3 = startx3 + (i3 + 0.5) * dx3

    return idim, jdim, kdim, xp1, xp2, xp3


###############################################################################
#  Calculate here any function derived from the standard functions in the
#   harm3d hdf5 datasets.
#     -- some of these are just translation maps between existing functions;
#           TODO:  make a lookup table for these functions;
###############################################################################
def calc_3d_hdf5_slice_func(nickname, slice_obj, h5file, h5gdump=None):

    rout_name = 'calc_3d_hdf5_slice_func'
    val = -666

    TOP_CARTESIAN, TOP_SPHERICAL, TOP_CYLINDRICAL = [0, 1, 2]
    top_type = get_val('TOP_TYPE_CHOICE', h5file)

    if nickname == 'poynting_flux_r':
        v1  = read_3d_hdf5_slice( 'v1', slice_obj, h5file)
        v2  = read_3d_hdf5_slice( 'v2', slice_obj, h5file)
        v3  = read_3d_hdf5_slice( 'v3', slice_obj, h5file)
        B1  = read_3d_hdf5_slice( 'B1', slice_obj, h5file)
        B2  = read_3d_hdf5_slice( 'B2', slice_obj, h5file)
        B3  = read_3d_hdf5_slice( 'B3', slice_obj, h5file)
        bsq = read_3d_hdf5_slice('bsq', slice_obj, h5file)

        if h5gdump is None:
            dx_dxp11 = read_3d_hdf5_slice('dx_dxp11', slice_obj, h5file)
            dx_dxp12 = read_3d_hdf5_slice('dx_dxp12', slice_obj, h5file)
            dx_dxp13 = read_3d_hdf5_slice('dx_dxp13', slice_obj, h5file)
        else:
            dx_dxp11 = read_3d_hdf5_slice('dxdxp11', slice_obj, h5gdump)
            dx_dxp12 = read_3d_hdf5_slice('dxdxp12', slice_obj, h5gdump)
            dx_dxp13 = read_3d_hdf5_slice('dxdxp13', slice_obj, h5gdump)

        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
        bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)

        bsq *= ucov0

        #  EM flux =  - T^r_t
        em_flux_1t = - ucon1 * bsq + bcon1 * bcov0
        em_flux_2t = - ucon2 * bsq + bcon2 * bcov0
        em_flux_3t = - ucon3 * bsq + bcon3 * bcov0

        val = em_flux_1t * dx_dxp11  + em_flux_2t * dx_dxp12  + em_flux_3t * dx_dxp13

    if nickname == 'poynting_flux_th':
        v1  = read_3d_hdf5_slice( 'v1', slice_obj, h5file)
        v2  = read_3d_hdf5_slice( 'v2', slice_obj, h5file)
        v3  = read_3d_hdf5_slice( 'v3', slice_obj, h5file)
        B1  = read_3d_hdf5_slice( 'B1', slice_obj, h5file)
        B2  = read_3d_hdf5_slice( 'B2', slice_obj, h5file)
        B3  = read_3d_hdf5_slice( 'B3', slice_obj, h5file)
        bsq = read_3d_hdf5_slice('bsq', slice_obj, h5file)

        if h5gdump is None:
            dx_dxp21 = read_3d_hdf5_slice('dx_dxp21', slice_obj, h5file)
            dx_dxp22 = read_3d_hdf5_slice('dx_dxp22', slice_obj, h5file)
            dx_dxp23 = read_3d_hdf5_slice('dx_dxp23', slice_obj, h5file)
        else:
            dx_dxp21 = read_3d_hdf5_slice('dxdxp21', slice_obj, h5gdump)
            dx_dxp22 = read_3d_hdf5_slice('dxdxp22', slice_obj, h5gdump)
            dx_dxp23 = read_3d_hdf5_slice('dxdxp23', slice_obj, h5gdump)

        ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
        bcon0, bcon1, bcon2, bcon3 = bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
        bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)

        bsq *= ucov0

        em_flux_1t = - ucon1 * bsq + bcon1 * bcov0
        em_flux_2t = - ucon2 * bsq + bcon2 * bcov0
        em_flux_3t = - ucon3 * bsq + bcon3 * bcov0

        val = em_flux_1t * dx_dxp21  + em_flux_2t * dx_dxp22  + em_flux_3t * dx_dxp23

    if( nickname == 'r' or nickname == 'rr'):
        if( top_type[0] == TOP_CARTESIAN ) :
            xx = read_3d_hdf5_slice('x1',slice_obj,h5file)
            yy = read_3d_hdf5_slice('x2',slice_obj,h5file)
            zz = read_3d_hdf5_slice('x3',slice_obj,h5file)
            val = np.sqrt( xx*xx + yy*yy + zz*zz )
        elif ( top_type[0] == TOP_SPHERICAL ) :
            val = read_3d_hdf5_slice('x1',slice_obj,h5file)
        else :
            s = read_3d_hdf5_slice('x1',slice_obj,h5file)
            z = read_3d_hdf5_slice('x2',slice_obj,h5file)
            val = np.sqrt(s*s + z*z)


    if( nickname == 'th' or nickname == 'theta' ) :
        if( top_type[0] == TOP_CARTESIAN ) :
            xx = read_3d_hdf5_slice('x1',slice_obj,h5file)
            yy = read_3d_hdf5_slice('x2',slice_obj,h5file)
            zz = read_3d_hdf5_slice('x3',slice_obj,h5file)
            r = np.sqrt( xx*xx + yy*yy + zz*zz )
            val = np.arccos( zz / r )
        elif ( top_type[0] == TOP_SPHERICAL ) :
            val = read_3d_hdf5_slice('x2',slice_obj,h5file)
        else :
            s = read_3d_hdf5_slice('x1',slice_obj,h5file)
            z = read_3d_hdf5_slice('x2',slice_obj,h5file)
            r = np.sqrt(s*s + z*z)
            val = np.acos( z / r )

    if( nickname == 'ph' or nickname == 'phi' ) :
        if( top_type[0] == TOP_CARTESIAN ) :
            xx = read_3d_hdf5_slice('x1',slice_obj,h5file)
            yy = read_3d_hdf5_slice('x2',slice_obj,h5file)
            val = np.arctan2(yy,xx)
            mask = val < 0.
            #renormalize phi to be in (0,2pi)
            val[mask] = 2. * np.pi - np.abs(val[np.where(mask)])
        else :
            val = read_3d_hdf5_slice('x3',slice_obj,h5file)

    if( nickname == 'xcart' or nickname == 'xx' or nickname == 'x' ) :
        if( top_type[0] == TOP_CARTESIAN ) :
            val = read_3d_hdf5_slice('x1',slice_obj,h5file)
        elif ( top_type[0] == TOP_SPHERICAL ) :
            r  = read_3d_hdf5_slice('x1',slice_obj,h5file)
            th = read_3d_hdf5_slice('x2',slice_obj,h5file)
            ph = read_3d_hdf5_slice('x3',slice_obj,h5file)
            val = r * np.sin(th) * np.cos(ph)
        else :
            s  = read_3d_hdf5_slice('x1',slice_obj,h5file)
            ph = read_3d_hdf5_slice('x3',slice_obj,h5file)
            val = s * np.cos(ph)

    if( nickname == 'ycart' or nickname == 'yy' or nickname == 'y' ) :
        if( top_type[0] == TOP_CARTESIAN ) :
            val = read_3d_hdf5_slice('x2',slice_obj,h5file)
        elif ( top_type[0] == TOP_SPHERICAL ) :
            r  = read_3d_hdf5_slice('x1',slice_obj,h5file)
            th = read_3d_hdf5_slice('x2',slice_obj,h5file)
            ph = read_3d_hdf5_slice('x3',slice_obj,h5file)
            val = r * np.sin(th) * np.sin(ph)
        else :
            s  = read_3d_hdf5_slice('x1',slice_obj,h5file)
            ph = read_3d_hdf5_slice('x3',slice_obj,h5file)
            val = s * np.sin(ph)

    if( nickname == 'zcart' or nickname == 'zz' or nickname == 'z' ) :
        if( top_type[0] == TOP_CARTESIAN ) :
            val = read_3d_hdf5_slice('x3',slice_obj,h5file)
        elif ( top_type[0] == TOP_SPHERICAL ) :
            r  = read_3d_hdf5_slice('x1',slice_obj,h5file)
            th = read_3d_hdf5_slice('x2',slice_obj,h5file)
            val = r * np.cos(th)
        else :
            val = read_3d_hdf5_slice('x2',slice_obj,h5file)


    if( not isinstance(val, np.ndarray) ) :
        if( val == -666 ) :
            print(rout_name+' function is not defined : '+nickname)

    return val


###############################################################################
def get_func(nickname,slice_obj,h5file,must_exist=True):
    rout_name = 'get_func'
    fullnames = lookup_full_name(nickname,h5file)

    # If there is not just one name matching the given one, then search for
    # functions derived from others in our list:
    if( len(fullnames) != 1 ) :

        # Check list of special functions:
        val = h5file[nickname][slice_obj]

        if( not isinstance(val, np.ndarray) ) :
            if( val == -666 ) :
                if( must_exist ) :
                    sys.exit(rout_name+"():  No datasets  matching the expression!  Exitting..." + nickname)

    # if there is only one matching the given name, then read it directly:
    else:
        val = h5file[fullnames[0]][slice_obj]


    return val


#  ###############################################################################
#  #  Should always return a 1-d or 2-d set of function and coordinate
#  #  data ready to be plotted.
#  ###############################################################################
#  def get_harm_gridfunction(nickname,plot_labels,slice,plot_dims,nout,do_1d,h5file):
#      rout_name = 'get_harm_gridfunction'
#
#      val = get_func(nickname,slice,h5file)
#
#      if( do_1d ) :
#          val = np.reshape(val,[nout[0]])
#          coord2 = val
#          coord1 = get_func(plot_labels[0],slice,h5file,must_exist=False)
#          if( not isinstance(coord1, np.ndarray) ) :
#              coord1 = np.arange(nout[0])
#          else :
#              coord1 = np.reshape(coord1,[nout[0]])
#      else:
#          val = np.reshape(val,[nout[0],nout[1]])
#          coord1 = get_func(plot_labels[0],slice,h5file,must_exist=False)
#          if( not isinstance(coord1, np.ndarray) ) :
#              coord1 = np.outer( np.arange(nout[0]) , np.ones(nout[1]) )
#          else :
#              coord1 = np.reshape(coord1,[nout[0],nout[1]])
#
#          coord2 = get_func(plot_labels[1],slice,h5file,must_exist=False)
#          if( not isinstance(coord2, np.ndarray) ) :
#              coord2 = np.outer( np.ones(nout[0]), np.arange(nout[1]) )
#          else :
#              coord2 = np.reshape(coord2,[nout[0],nout[1]])
#
#      return val, coord1, coord2
#
################################################################################
#  Sets the index filter or slice array based on more primitive settings for
#  "iph" and "ith".
#   -- slice[dim] < 0  if we want to select all the indices in dimension "dim"
#   -- enforces 2-d slices.
#   -- this assumes that the full extents are used
################################################################################
def calc_slice(irad, ith, iph, ndims, assume_axi_coord_geom=False):

    prog_name = 'calc_slice'

    # The default is selecting all indices in all dimensions:
    slice_arr = np.array([-1,-1,-1])

    if( ith is not None ) :
        slice_arr[1] = ith

    if( iph is not None ) :
        slice_arr[2] = iph
        # the geom and coord data may be phi-independent and only
        # written for the first slice, so we just select it :
        if( assume_axi_coord_geom ) :
            slice_arr[2] = 0


    if( irad is not None ) :
        slice_arr[0] = irad


    plot_dims = np.array([ (ndims[0] > 1), (ndims[1] > 1), (ndims[2] > 1) ])
    print("plot_dims1 = ", plot_dims)

    if( len(slice_arr) != 3 ) :
        sys.exit(prog_name+"():  Problem with length of slice_arr: ", slice_arr)

    plot_dims[ slice_arr >= 0 ] = False   # We do not plot along dimensions we are slicing through

    print("plot_dims2 = ", plot_dims)

    if( any( np.greater_equal(slice_arr, ndims) ) ):
        sys.exit(prog_name+"():  We are trying to slice_arr at an index out of bounds:  slice_arr = ", slice_arr, "  ndims = ", ndims )

    if( any( plot_dims ) ) :
        print("{0}():  We are plotting  func in dimensions = {1}".format(prog_name, plot_dims))
    else :
        sys.exit(prog_name + "():  No real way to plot a point of data!!! ")

    if( np.sum(plot_dims) == 3 ) :
        print("{}():  User did not specify a valid 2d slice_arr , so we will use the default slice_arr through the third dimension at k=0".format(prog_name))
        slice_arr[2] = 0
        plot_dims[2] = False

    if( np.sum(plot_dims) == 1 ) :
        print("{0}():  User did not specify a valid 2d slice_arr , slice_arr = {1}".format(prog_name, slice_arr))

        if( not plot_dims[1] ) :
            plot_dims[1] = True
            slice_arr[1] = -1
        elif( not plot_dims[2] ) :
            plot_dims[2] = True
            slice_arr[2] = -1
        else : # can there be an else?
            plot_dims[1] = True
            slice_arr[1] = -1

    print("slice_arr = ", slice_arr, "   plot_dims = ", plot_dims)

#  From https://docs.scipy.org/doc/numpy-1.13.0/reference/arrays.indexing.html
#
# Remember that a slicing tuple can always be constructed as obj
# and used in the x[obj] notation. Slice objects can be used in the
# construction in place of the [start:stop:step] notation. For
# example, x[1:10:5,::-1] can also be implemented as obj =
# (slice(1,10,5), slice(None,None,-1)); x[obj] .
#

    if( plot_dims[0] ) :
        slice1 = slice(None,None,None)  # selects all elements
    else :
        slice1 = slice(slice_arr[0],slice_arr[0]+1,1)

    if( plot_dims[1] ) :
        slice2 = slice(None,None,None)  # selects all elements
    else :
        slice2 = slice(slice_arr[1],slice_arr[1]+1,1)

    if( plot_dims[2] ) :
        slice3 = slice(None,None,None)  # selects all elements
    else :
        slice3 = slice(slice_arr[2],slice_arr[2]+1,1)

    slice_obj = (slice1,slice2,slice3)

    print("slice_obj  = ", slice_obj)

    return slice_obj

###############################################################################
###############################################################################
def rel_diff(f1,f2):

    mag = 0.5*(np.abs(f1)+np.abs(f2))
    diff = np.abs(f1-f2)

    rd =np. where( mag > 0., diff/mag, diff)

    return( rd )



###############################################################################
###############################################################################
def isclose(f1,f2,rel_tol=1.e-13):

    reld = rel_diff(f1,f2)
    return (reld < rel_tol)


######################################
# make time-avg
######################################
def phi_avgs_func(funcname,h5file,av_h5file,func_av=None,func=None):

    if( func_av is None ) :
        slc_all=slice(None,None,None)
        slice_obj_tmp = (slc_all,slc_all,slc_all)

        if( func is None ) :
            func = read_3d_hdf5_slice(funcname,slice_obj_tmp,h5file)
            clear_func = True

        n3 = func.shape[2]
        func_av = np.sum(func,axis=2)
        func_av /= n3


    dset = av_h5file.create_dataset(funcname+'_phavg', data=func_av)

    return func_av, func


######################################
# make A_phi
######################################
def make_aphi(bx,by,h5file,av_h5file,gdump_h5file):

    dx1 = h5file['Header/Grid/dx1'][0]
    dx2 = h5file['Header/Grid/dx1'][0]

    half_dx1 = 0.5*dx1
    half_dx2 = 0.5*dx2

    gdet = gdump_h5file['gdet3'][:,:,0]
    gterm = gdet * np.sqrt(4.*np.pi)

    bx *= gterm
    by *= gterm

    psi = 0.*bx

    idim = bx.shape[0]
    kdim = bx.shape[1]
    kmid = int(kdim/2)

    #print("idim = ", idim)
    #print("kdim = ", kdim)

    j = 0
    for i in np.arange(idim-1) :
        psi[i+1,j] = psi[i,j] - half_dx1 * (by[i,j] + by[i+1,j])

    i = 0
    for j in np.arange(kdim-1):
        psi[i,j+1] = psi[i,j] + half_dx2 * (bx[i,j+1] + bx[i,j+1])

    j = kdim-1
    for i in np.arange(idim-1) :
        psi[i+1,j] = psi[i,j] - half_dx1 * (by[i,j] + by[i+1,j])

    i = idim-1
    for j in np.arange(kdim-1):
        psi[i,j+1] = psi[i,j] + half_dx2 * (bx[i,j+1] + bx[i,j+1])

#    for i in np.arange(1,idim-2):
#        for j in np.arange(kmid):
#            psi[i,j+1] = psi[i,j] + half_dx2 * (bx[i,j+1] + bx[i,j+1])
#
#        for j in np.arange(kdim-1,kmid,-1):
#            psi[i,j-1] = psi[i,j] - half_dx2 * (bx[i,j-1] + bx[i,j])

    for i in np.arange(1,idim-2):
        for j in np.arange(kdim-1):
            psi[i,j+1] = psi[i,j] + half_dx2 * (bx[i,j+1] + bx[i,j+1])


    return psi


######################################
# make phi-avg
######################################
def make_phi_avgs():

    file = 'KDHARM0.000800.h5'
    av_file = 'KDHARM0.000800.avg.h5'
    gdump_file = 'KDHARM0.gdump.h5'
    h5file   = h5py.File(file, 'r')
    av_h5file  = h5py.File(av_file, 'w')
    gdump_h5file  = h5py.File(gdump_file, 'r')

    phi_avgs_func('gamma',h5file,av_h5file)
    phi_avgs_func('rho',  h5file,av_h5file)
    phi_avgs_func('bsq',  h5file,av_h5file)
    bx = phi_avgs_func('B1',   h5file,av_h5file)
    by = phi_avgs_func('B2',   h5file,av_h5file)
    phi_avgs_func('uu',   h5file,av_h5file)

    aphi = make_aphi(bx,by,h5file,av_h5file,gdump_h5file)
    dset = av_h5file.create_dataset('Aphi', data=aphi)

    h5file.close()
    av_h5file.close()
    gdump_h5file.close()

    return


######################################
# make phi-avg
#  -- special version for EHT code comparison paper
#  -- reads in 3-d data only once to minimize IO
######################################
def make_phi_avgs_eht(file='KDHARM0.000800.h5',
                      gdump_file='KDHARM0.gdump.h5',
                      av_file=None
):

    print("phi-averaging file = ", file)

    slc_all=slice(None,None,None)
    slice_obj_tmp = (slc_all,slc_all,slc_all)

    if( av_file is None ) :
        av_file=file.split('.h5')[0]+'.phavg.h5'

    h5file   = h5py.File(file, 'r')
    gdump_h5file  = h5py.File(gdump_file, 'r')
    av_h5file  = h5py.File(av_file, 'w')

    h5file.copy('Header',av_h5file)

    gamma_av, gamma = phi_avgs_func('gamma',h5file,av_h5file)
    gamma_av=0
    gamma=0

    bx_av   , bx    = phi_avgs_func('B1',   h5file,av_h5file)
    by_av   , by    = phi_avgs_func('B2',   h5file,av_h5file)
    aphi = make_aphi(bx_av,by_av,h5file,av_h5file,gdump_h5file)
    dset = av_h5file.create_dataset('Aphi', data=aphi)
    bx = 0
    by = 0
    bx_av = 0
    by_av = 0
    aphi=0

    rho_av  , rho   = phi_avgs_func('rho',  h5file,av_h5file)
    bsq_av  , bsq   = phi_avgs_func('bsq',  h5file,av_h5file)
    sigma_mag = bsq/rho
    phi_avgs_func('sigma_mag', h5file, av_h5file, func=sigma_mag)
    sigma_mag = 0

    uu_av   , uu    = phi_avgs_func('uu',   h5file,av_h5file)
    gam = h5file['Header/Grid/gam'][0]
    #pgas = (gam-1.)*uu
    #pmag = 0.5*bsq
    #beta_inv = pmag/pgas
    beta_inv = (0.5/(gam-1.))*bsq/uu
    phi_avgs_func('beta_inv', h5file, av_h5file, func=beta_inv)

    h5file.close()
    av_h5file.close()
    gdump_h5file.close()

    return

###############################################################################
#
# calc_divb():
# -----------
#   -- given a hdf5 file and a slice prescription, reads in the
#      necesssary data and calculates divb, zeroing out elements
#      without sufficient data (boundary elements)
#   -- uses FluxCT stencil
#
###############################################################################
def calc_divb(slice_obj, h5file):

    dx1  = h5file['Header/Grid/dx1'][0]
    dx2  = h5file['Header/Grid/dx2'][0]
    dx3  = h5file['Header/Grid/dx3'][0]

    # factors for later, including the 1/4 factor for the average:
    invdx1 = 0.25/dx1
    invdx2 = 0.25/dx2
    invdx3 = 0.25/dx3

    b1   = read_3d_hdf5_slice('B1'  ,slice_obj,h5file)
    b2   = read_3d_hdf5_slice('B2'  ,slice_obj,h5file)
    b3   = read_3d_hdf5_slice('B3'  ,slice_obj,h5file)
    gdet = read_3d_hdf5_slice('gdet',slice_obj,h5file)
    b1  *= gdet
    b2  *= gdet
    b3  *= gdet

    # Shift data for efficient finite difference:
    b1_m1 = np.roll(b1,1,axis=0)
    b2_m1 = np.roll(b2,1,axis=1)
    b3_m1 = np.roll(b3,1,axis=2)

    db1 = b1 - b1_m1
    db2 = b2 - b2_m1
    db3 = b3 - b3_m1

    # to save memory:
    b1_m1 = 0
    b2_m1 = 0
    b3_m1 = 0

    # Shift data for efficient averaging:
    i = 1
    j = 2
    db_m1_m0 = np.roll(db1,1,axis=i)
    db_m0_m1 = np.roll(db1,1,axis=j)
    db_m1_m1 = np.roll(np.roll(db1,1,axis=i),1,axis=j)
    ddb1 = db1 + db_m1_m0 + db_m0_m1 + db_m1_m1
    ddb1 *= invdx1

    i = 0
    j = 2
    db_m1_m0 = np.roll(db2,1,axis=i)
    db_m0_m1 = np.roll(db2,1,axis=j)
    db_m1_m1 = np.roll(np.roll(db2,1,axis=i),1,axis=j)
    ddb2 = db2 + db_m1_m0 + db_m0_m1 + db_m1_m1
    ddb2 *= invdx2

    i = 0
    j = 1
    db_m1_m0 = np.roll(db3,1,axis=i)
    db_m0_m1 = np.roll(db3,1,axis=j)
    db_m1_m1 = np.roll(np.roll(db3,1,axis=i),1,axis=j)
    ddb3 = db3 + db_m1_m0 + db_m0_m1 + db_m1_m1
    ddb3 *= invdx3

    db_m1_m0 = 0
    db_m0_m1 = 0
    db_m1_m1 = 0
    db1      = 0
    db2      = 0
    db3      = 0

    divb = ddb1 + ddb2 + ddb3

    return divb

###############################################################################
#
# make_3d_dump():
# -----------
#
#   -- often you need a GDUMP file for an axisymmetric spacetime and
#      your your run is 3-d and large making it difficult or
#      impossible to write the GDUMP (because of per node memory
#      constraints and the limitations of how harm3d gathers data for
#      IO;
#
#   -- this can be rectified by writing a 2-d version of GDUMP
#      (i.e. setting totalsize[3] to 1 and redoing a run for up until
#      it writes the GDUMP file);
#
#   -- However, one needs to set the parameters in GDUMP to those in
#      the 3-d configuration;
#
#   -- This routine copies over the relevant grid parameters from the
#      3-d hdf5 file to the 2-d fille
#
#
###############################################################################
def make_3d_dump(filename_3d, filename_2d):

    h5_3d = h5py.File(filename_3d, 'r')
    h5_2d = h5py.File(filename_2d, 'r+')

    parameters = ['dx3', 'startx3', 'totalsize3']

    for param in parameters :
        fullname_3d = lookup_full_name(param, h5_3d)
        fullname_2d = lookup_full_name(param, h5_2d)
        print(fullname_3d, fullname_2d)
        if( fullname_3d and fullname_2d ) :
            h5_2d[fullname_2d[0]][:] = h5_3d[fullname_3d[0]][:]

        else :
            sys.exit('parameters cannot be found')

    h5_3d.close()
    h5_2d.close()

    return

###############################################################################
#
# calc_curl_vec():
# -----------
#
#   -- calculates the curl of a vector field, returning the curl and
#      its magnitude;
#
###############################################################################
def calc_curl_vec(slice_obj, h5file, vector_type='B', metric=None, dx=None, fill_value=1e-20, full_2pi=True,densitized=True,h5file2=None):

    # Read in the appropriate vector field:
    if( vector_type == 'B' ) :
        b1   = read_3d_hdf5_slice('B1'  ,slice_obj,h5file)
        b2   = read_3d_hdf5_slice('B2'  ,slice_obj,h5file)
        b3   = read_3d_hdf5_slice('B3'  ,slice_obj,h5file)

    else :
        if( h5file2 is None ) :
            b1   = read_3d_hdf5_slice('v1'  ,slice_obj,h5file)
            b2   = read_3d_hdf5_slice('v2'  ,slice_obj,h5file)
            b3   = read_3d_hdf5_slice('v3'  ,slice_obj,h5file)
        else :
            b1   = read_3d_hdf5_slice('v1'  ,slice_obj,h5file2)
            b2   = read_3d_hdf5_slice('v2'  ,slice_obj,h5file2)
            b3   = read_3d_hdf5_slice('v3'  ,slice_obj,h5file2)


    gshape = b1.shape
    ndims = len(gshape)

    if( ndims > 3 ) :
        sys.exit("Data set has too many dimensions : ", ndims, gshape)

    if( ndims < 2 ) :
        print("Warning:  Data set has too few dimensions for a meaningful curl: ", ndims, gshape)
        curl_1 = np.zeros_like(b1)
        curl_2 = np.zeros_like(b1)
        curl_3 = np.zeros_like(b1)
        curl_mag = np.zeros_like(b1)
        return curl_1, curl_2, curl_3, curl_mag

    # If one of the 3 dimensions is missing, then we have to figure
    #   out what the 2 dimensions refer to:
    dims = [0,1,2]

    if( ndims == 2 ) :
        if( len( slice_obj) != 3 ) :
            sys.exit("Slice object must be 3-d : ", slice_obj)

        for idim in np.arange(3) :
            if( slice_obj[idim].start is not None ) :
                if(  (slice_obj[idim].stop - slice_obj[idim].start)/slice_obj[idim].step <= 1 ) :
                    dims[idim] = None

    i = 0
    for idim in np.arange(len(dims)) :
        if( dims[idim] is not None ) :
            dims[idim] = i
            i += 1

    if( i <= 1 ) :
        print("Warning 2: Data set has too few dimensions for a meaningful curl: ", ndims, gshape, dims)

    print("dims = ", dims)

    #  Setup necessary metric elements:
    if( metric is None ) :
        gcov11   = read_3d_hdf5_slice('gcov11'  ,slice_obj,h5file)
        gcov12   = read_3d_hdf5_slice('gcov12'  ,slice_obj,h5file)
        gcov13   = read_3d_hdf5_slice('gcov13'  ,slice_obj,h5file)
        gcov22   = read_3d_hdf5_slice('gcov22'  ,slice_obj,h5file)
        gcov23   = read_3d_hdf5_slice('gcov23'  ,slice_obj,h5file)
        gcov33   = read_3d_hdf5_slice('gcov33'  ,slice_obj,h5file)

    else :
        gcov11 = metric['gcov11']
        gcov12 = metric['gcov12']
        gcov13 = metric['gcov13']
        gcov22 = metric['gcov22']
        gcov23 = metric['gcov23']
        gcov33 = metric['gcov33']


    read_dx = False
    if( dx is None ) :
        read_dx = True
    else :
        if( len(dx) != 4 ) :
            read_dx = True

    if( read_dx ) :
        dx = np.ndarray(4)
        dx[0] = get_val('dx0',h5file)
        dx[1] = get_val('dx1',h5file)
        dx[2] = get_val('dx2',h5file)
        dx[3] = get_val('dx3',h5file)


    # Lower vector field for curl:
    Bdn1 = gcov11 * b1 + gcov12 * b2 + gcov13 * b3
    Bdn2 = gcov12 * b1 + gcov22 * b2 + gcov23 * b3
    Bdn3 = gcov13 * b1 + gcov23 * b2 + gcov33 * b3

    # for testing purposes:
    if( False ) :
        Bdn1[:,:] = np.outer(np.ones(gshape[0]),np.arange(gshape[1]))
        Bdn2[:,:] = np.outer(2.5*np.arange(gshape[0]),3.2*np.arange(gshape[1]))
        Bdn3[:,:] = np.outer(4.*np.arange(gshape[0]),np.ones(gshape[1]))


    # Calculate the curl:  (make sure to not take derivative in missing dimension)
    if( dims[0] is not None ) :
        d1_B2 = Bdn2 - np.roll(Bdn2,1,axis=dims[0])
        d1_B3 = Bdn3 - np.roll(Bdn3,1,axis=dims[0])
        d1_B2 /= dx[1]
        d1_B3 /= dx[1]
    else :
        d1_B2 = np.zeros_like(b1)
        d1_B3 = np.zeros_like(b1)

    if( dims[1] is not None ) :
        d2_B1 = Bdn1 - np.roll(Bdn1,1,axis=dims[1])
        d2_B3 = Bdn3 - np.roll(Bdn3,1,axis=dims[1])
        d2_B1 /= dx[2]
        d2_B3 /= dx[2]
    else :
        d2_B1 = np.zeros_like(b1)
        d2_B3 = np.zeros_like(b1)

    if( dims[2] is not None ) :
        d3_B1 = Bdn1 - np.roll(Bdn1,1,axis=dims[2])
        d3_B2 = Bdn2 - np.roll(Bdn2,1,axis=dims[2])
        d3_B1 /= dx[3]
        d3_B2 /= dx[3]
    else :
        d3_B1 = np.zeros_like(b1)
        d3_B2 = np.zeros_like(b1)


    #  (curl of B)^i = - \eps^{tijk} d_j B_k
    curl_1 = - ( d2_B3 - d3_B2 )
    curl_2 = - ( d3_B1 - d1_B3 )
    curl_3 = - ( d1_B2 - d2_B1 )


    # Take magnitude of curl:
    curl_mag = np.sqrt(gcov11 * curl_1 * curl_1 +
                       gcov22 * curl_2 * curl_2 +
                       gcov33 * curl_3 * curl_3 +
                       2. * ( gcov12 * curl_1 * curl_2 +
                              gcov13 * curl_1 * curl_3 +
                              gcov23 * curl_2 * curl_3 ) )


    curl_1[  0,:] = fill_value
    curl_2[  0,:] = fill_value
    curl_3[  0,:] = fill_value
    curl_mag[0,:] = fill_value

    if( not full_2pi ) :
        curl_1[  :,0] = fill_value
        curl_2[  :,0] = fill_value
        curl_3[  :,0] = fill_value
        curl_mag[:,0] = fill_value

    if( densitized ) :
        curl_1 *= np.sqrt(gcov11)
        curl_2 *= np.sqrt(gcov22)
        curl_3 *= np.sqrt(gcov33)

    return curl_1, curl_2, curl_3, curl_mag

###############################################################################
#
# get_bbh_pos():
# -----------
#
#   -- given a time, returns with the x,y,z positions of a pair of
#       black holes, as stored in "trajectory" data
#
#   -- will interpolate the data in time to get the best values of the coordinates;
#
#   -- we probably want to turn this into a general routine that
#      returns a dictionary of values so it can be used for any values
#      stored in the trajectory;
#
#       -- this would be trivial if the trajectory data used a
#          standard header line, though I like the descriptive one use now ;
#
###############################################################################
def get_bbh_pos(timeout, runname='longer2', trajfile=None, spinning=False):

    if( trajfile is None ) :
        if( runname is None ) :
            sys.exit("Need to provide one or the other!! ")
        else :
            pnames = ['traj_file']
            sim_info = get_sim_info(pnames, runname=runname)
            trajfile = sim_info['traj_file']

    if( spinning ):
        # TODO: update columns to the new output format
        tbh_traj, xbh1_traj, ybh1_traj, zbh1_traj, xbh2_traj, ybh2_traj, zbh2_traj = np.loadtxt(trajfile,usecols=(0,1,2,3,4,5,6),unpack=True) #spinning
        fzbh1 = interp1d(tbh_traj, zbh1_traj, kind='linear')
        fzbh2 = interp1d(tbh_traj, zbh2_traj, kind='linear')
        zbh1  = fzbh1(timeout)
        zbh2  = fzbh2(timeout)

    else:
        tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile,usecols=(0,4,5,6,7),unpack=True) #old metrics
        zbh1  = np.zeros_like(timeout)
        zbh2  = np.zeros_like(timeout)


    fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
    fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
    fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
    fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

    xbh1  = fxbh1(timeout)
    ybh1  = fybh1(timeout)
    xbh2  = fxbh2(timeout)
    ybh2  = fybh2(timeout)


    return xbh1, ybh1, zbh1, xbh2, ybh2, zbh2

###############################################################################
#
# viz_identify_symmetry_boundary():
# -----------
#
#   -- when making contour plots of slices in spherical coordinates
#      across boundaries that should be identified with each other but
#      are not in memory, one often finds a an empty "wedge" where
#      there should not be one;
#
#   -- the trick to removing this wedge is to add a row/column of data
#      from the other side to the edge, so the identification is
#      explicit at the edge;
#
#   -- this function performs this operation automatically ;
#
#   -- assumes by default that the 2nd dimension is symmetric one;
#
###############################################################################
def viz_identify_symmetry_boundary(gridfunc,axis=1):

    fshape = gridfunc.shape
    if( len(fshape) != 2 ) :
        sys.exit("this routine only works for 2-d datasets!! " )

    if( (axis != 0) and (axis != 1) ) :
        sys.exit("axis must be 0 or 1 !! ")

    newshape = np.copy(fshape)
    newshape[axis] += 1

    newfunc = np.empty(newshape)

    if( axis == 0 ) :
        newfunc[:-1,:] = gridfunc[:,:]
        newfunc[-1,:] = gridfunc[0,:]
    else :
        newfunc[:,:-1] = gridfunc[:,:]
        newfunc[:,-1] = gridfunc[:,0]


    return newfunc

###############################################################################
#
# find_indices_of_dumps():
# -----------
#
#   -- using run/simulation information stored in get_sim_info.py,
#        find the list of time indices of normal 3-d dump files;
#
###############################################################################
def find_indices_of_dumps(runname,dumptype='dump_name',verbose=False):

    pnames = [dumptype]
    sim_info = get_sim_info(pnames, runname=runname)
    filenames = sim_info[dumptype] + '.0*.h5'
    files = sorted(glob.glob(filenames))
    if( verbose ) :
        print("files = ", files)
    index_list = []
    for file in files :
        if( verbose ) :
            print("file = ", file)
        res = re.search('\.[0-9]+\.',file)
        if( res is not None ):
            str_tmp =  res.group(0)
            new_str = str_tmp[1:-1]
            index = int( new_str)
            index_list = np.append(index_list, index)
            if( verbose ) :
                print("str_tmp = ", str_tmp)
                print("new_str = ", new_str)
                print("index  = ", index)

    if( verbose ) :
        print("index_list = ", index_list)

    # Check for missing times:
    if( len(index_list) != (index_list[-1]+1) ) :
        print("missing times for run = ", runname, "  ntimes = ",len(index_list), index_list[-1])

    return index_list


################################################################################
#
# search_all_datasets_for_nans():
# -----------
#
#   -- given an hdf5 file, look at all data sets and returns
#      information where nans occur.
#
#   -- verbose=True will write all the indices of all datasets where
#      there is a nan;
#
################################################################################
def search_all_datasets_for_nans(h5file, verbose=True) :

    all_h5_objs = []
    h5file.visit(all_h5_objs.append)
    total_num = 0

    for var in all_h5_objs :
        ftmp = h5file[var]
        if isinstance(ftmp, h5py.Dataset):
            size = len(np.shape(ftmp))
            if( size > 0 ) :  # It is not a scalar
                dset = ftmp
                wherenans = np.isnan(dset)
                num_nans = np.sum(wherenans)
                total_num += num_nans
                print(var, dset.shape, dset.dtype, ' has  ', num_nans , ' / ', dset.size, '   NANs')

    print("Total number of nans in file = ", total_num)

    return
