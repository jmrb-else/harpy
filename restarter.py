"""
Restarter functions
"""
import h5py
import os
import numpy as np

def renorm_fields(old_dump, new_dump, renorm_dump):
    """
    This file renormalizes the B and v fields.

    Parameters
    ----------
    
    oldg_files : str
    dumps with the old gauge

    newg_files : str
    dumps with the new gauge

    renorm_files : str
    The files that will have the renormalized fields. These should be duplicates of oldg_files
    """
    os.system('cp {0} {1}'.format(old_dump, renorm_dump))

    print(renorm_dump)
    h5new = h5py.File(new_dump, 'r')
    h5old = h5py.File(old_dump, 'r')
    with h5py.File(renorm_dump, 'r+') as h5ren:
        # B-field
        gdet_new = h5new['gdet'][...]
        gdet_old = h5old['gdet'][...]
        bfactor = np.divide(gdet_old, gdet_new, out=np.zeros_like(gdet_old), where=gdet_new!=0.)
        for func in ['B1', 'B2', 'B3']:
            print(" func = ", func)
            dat_tmp = h5ren[func]
            dat_tmp[...] = h5old[func][...] * bfactor

        # v-field
        gcov11 = h5new['gcov11'][...]
        gcov22 = h5new['gcov11'][...]
        gcov33 = h5new['gcov11'][...]
        gcov12 = h5new['gcov11'][...]
        gcov13 = h5new['gcov11'][...]
        gcov23 = h5new['gcov11'][...]
        v1 = h5new['v1'][...]
        v2 = h5new['v2'][...]
        v3 = h5new['v3'][...]
        vsq = gcov11*v1*v1 + gcov22*v2*v2 + gcov33*v3*v3 + 2.*( v1*(gcov12*v2 + gcov13*v3) + gcov23*v2*v3 )
        W_new = np.sqrt(1. + vsq)
        gcov11 = h5old['gcov11'][...]
        gcov22 = h5old['gcov11'][...]
        gcov33 = h5old['gcov11'][...]
        gcov12 = h5old['gcov11'][...]
        gcov13 = h5old['gcov11'][...]
        gcov23 = h5old['gcov11'][...]
        v1 = h5old['v1'][...]
        v2 = h5old['v2'][...]
        v3 = h5old['v3'][...]
        vsq = gcov11*v1*v1 + gcov22*v2*v2 + gcov33*v3*v3 + 2.*( v1*(gcov12*v2 + gcov13*v3) + gcov23*v2*v3 )
        W_old = np.sqrt(1. + vsq)
        Wfactor = np.divide(W_old, W_new, out=np.zeros_like(W_old), where=W_new!=0.)
        for func in ['v1','v2','v3']:
            print(" func = ", func)
            dat_tmp = h5ren[func]
            dat_tmp[...] = h5old[func][...] * Wfactor

    h5new.close()
    h5old.close()
    return


def modif_headers(rdump, rdump_new):
    """
    This script modifies the headers of rdumps
    """

    os.system('cp {0} {1}'.format(rdump, rdump_new))

    with h5py.File(rdump, 'r') as h5f:
        bbh_sep = np.sqrt((h5f['/Header/Grid/bh2_traj1'][0] - h5f['/Header/Grid/bh1_traj1'][0])**2 + (h5f['/Header/Grid/bh2_traj2'][0] - h5f['/Header/Grid/bh1_traj2'][0])**2)
        print("bbh_sep =", bbh_sep)

    heads_and_newvals = {"Header/Grid/t": 0.,
                         "Header/Grid/t_n": 0.,
                         "Header/Grid/t_restart": 0.,
                         "Header/Grid/t_sync": 0.,
                         "Header/Grid/a": 0.4,
                         "Header/Grid/initial_bbh_separation": bbh_sep,
                         "Header/IO/T_out0": 0.,
                         "Header/IO/T_out1": 0.,
                         "Header/IO/T_out2": 0.,
                         "Header/IO/T_out3": 0.,
                         "Header/IO/T_out4": 0.,
                         "Header/IO/T_out5": 0.,
                         "Header/IO/T_out6": 0.,
                         "Header/IO/T_out7": 0.,
                         "Header/IO/T_out8": 0.,
                         "Header/IO/T_out9": 0.,
                         "Header/IO/T_out10": 0.,
                         "Header/IO/T_out11": 0.,
                         "Header/IO/N_out0": 0,
                         "Header/IO/N_out1": 0,
                         "Header/IO/N_out2": 0,
                         "Header/IO/N_out3": 0,
                         "Header/IO/N_out4": 0,
                         "Header/IO/N_out5": 0,
                         "Header/IO/N_out6": 0,
                         "Header/IO/N_out7": 0,
                         "Header/IO/N_out8": 0,
                         "Header/IO/N_out9": 0,
                         "Header/IO/N_out10": 0,
                         "Header/IO/N_out11": 0,
    }

    print(rdump_new)

    with h5py.File(rdump_new, 'r+') as h5f:
        for header, new_value in heads_and_newvals.items():
            #print(h5f[header][()], new_value)
            tmp = h5f[header]
            tmp[...] = new_value
            #print(h5f[header][()], new_value)
    return


def trimB(dump=None, rdump=None, ln_rdump=False):
    """
    Trimming the B-field. Step needed to clean data for the Patchwork BHOG.

    Parameters
    ----------

    dump : str

    rdump : str

    ln_rdump : bool
    """
    if dump is None:
        dump = 'KDHARM0.000000.h5'
    if rdump is None:
        rdump = 'rdump_start.h5'

    print("Original dump file: ", dump)
    print("Trimmed dump file: ", rdump)

    os.system('cp {0} {1}'.format(dump, rdump))

    with h5py.File(rdump, 'r+') as h5f:

        NT2 = h5f['/Header/Grid/totalsize2'][0]
        tc = 10      # transition cells
        zc = 20      # zero-out this number of cells
        zc1 = zc - 1
        ztc = zc1 + tc
        NT2zc = NT2 - 1 - ztc
        for j in range(0, ztc + 1):
            h5f['B1'][:,j,:] = h5f['B1'][:,ztc,:] * (j - zc1) * np.heaviside(j - zc1, 0.) / tc
            h5f['B2'][:,j,:] = h5f['B2'][:,ztc,:] * (j - zc1) * np.heaviside(j - zc1, 0.) / tc
            h5f['B3'][:,j,:] = h5f['B3'][:,ztc,:] * (j - zc1) * np.heaviside(j - zc1, 0.) / tc
            h5f['B1'][:,NT2zc + j,:] = h5f['B1'][:,NT2zc,:] * (tc - j) * np.heaviside(tc - j, 0.) / tc
            h5f['B2'][:,NT2zc + j,:] = h5f['B2'][:,NT2zc,:] * (tc - j) * np.heaviside(tc - j, 0.) / tc
            h5f['B3'][:,NT2zc + j,:] = h5f['B3'][:,NT2zc,:] * (tc - j) * np.heaviside(tc - j, 0.) / tc
        tc = 20      # transition cells
        zc = 50      # zero-out this number of cells
        zc1 = zc - 1
        ztc = zc1 + tc
    if ln_rdump:
        os.system('ln -sf {} rdump_start.h5'.format(rdump))

    return


def replace_cleanB(monopole_cleaner=None, dump=None, rdump=None, ln_rdump=False):
    if monopole_cleaner is None:
        monopole_cleaner = 'monopole_cleaner_it000000000.h5'
    if dump is None:
        dump = 'KDHARM0.000000.h5'
    if rdump is None:
        rdump = 'rdump_start.h5'

    print("Original dump file: ", dump)
    print("Using: ", monopole_cleaner)
    print("New rdump file: ", rdump)

    os.system('cp {0} {1}'.format(dump, rdump))

    with h5py.File(monopole_cleaner, 'r') as h5f:
            B1 = h5f['B1'][...]
            B2 = h5f['B2'][...]
            B3 = h5f['B3'][...]
    with h5py.File(rdump, 'r+') as h5f:
            h5f['B1'][...] = B1
            h5f['B2'][...] = B2
            h5f['B3'][...] = B3
    
    if ln_rdump:
        os.system('ln -sf {} rdump_start.h5'.format(rdump))

    return
