def runitall(ibeg,iend):

    dirout = './movies'
    # ibeg = 181
    # iend = 182
    # iend = 51

    iph = 0

    rhomin=-7
    rhomax=-1.5

    uumin = -9
    uumax = -3

    bsqmin = -9
    bsqmax = -3

    rhogammin = 1.
    rhogammax = 1.

    gammamin = 1.
    gammamax = 2.

    bor_min = -4.
    bor_max = 4.


    for i in np.arange(ibeg,iend) :
        suf='-rmax1'
        rmax = 50.
        # polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=rhomin,maxf=rhomax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=rhomin,maxf=rhomax,dirout=dirout)

        # polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=bsqmin,maxf=bsqmax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=bsqmin,maxf=bsqmax,dirout=dirout)

        # polp(itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=bor_min,maxf=bor_max,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=bor_min,maxf=bor_max,dirout=dirout)

        # polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=uumin,maxf=uumax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=uumin,maxf=uumax,dirout=dirout)

        # polp(itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=gammamin,maxf=gammamax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=gammamin,maxf=gammamax,dirout=dirout)


        suf='-rmax2'
        rmax=200.

        # polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=rhomin,maxf=rhomax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=rhomin,maxf=rhomax,dirout=dirout)

        # polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=bsqmin,maxf=bsqmax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=bsqmin,maxf=bsqmax,dirout=dirout)

        # polp(itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=bor_min,maxf=bor_max,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='bsq_o_rho',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=bor_min,maxf=bor_max,dirout=dirout)

        # polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=uumin,maxf=uumax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=uumin,maxf=uumax,dirout=dirout)

        # polp(itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,ith=80,xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax,plot_bhs_traj=True,minf=gammamin,maxf=gammamax,dirout=dirout)
        polp(itime=i,dirname="./",show_plot=False,funcname='gamma',dolog=False,savepng=True,iph=0,xmin=1e-6,xmax=rmax,zmin=-0.5*rmax,zmax=0.5*rmax,corotate=True,plot_bhs_traj=True,minf=gammamin,maxf=gammamax,dirout=dirout)

    return



######################################
def runitall2(ibeg,iend):

    dirout='./r-mov'
    irads = [400,500,590]
    xmin=0.2
    xmax=np.pi-xmin

    for i in np.arange(ibeg,iend) :
        for irad in irads :
            #    polp(itime=i,dirname="./",show_plot=False,funcname='poynting_flux_z',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf,dirout=dirout,with_gdet=True)
            #    polp(itime=i,dirname="./",show_plot=False,funcname='total_flux_z',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf   ,dirout=dirout,with_gdet=True)
            #    polp(itime=i,dirname="./",show_plot=False,funcname='hydro_flux_z',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf   ,dirout=dirout,with_gdet=True)
            #    polp(itime=i,dirname="./",show_plot=False,funcname='mass_flux_z',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf    ,dirout=dirout,with_gdet=True)
            polp(itime=i,dirname="./",show_plot=False,funcname='poynting_flux_r',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf,dirout=dirout,with_gdet=True,xmin=xmin,xmax=xmax,minf=-4,maxf=-0.1)
            polp(itime=i,dirname="./",show_plot=False,funcname='total_flux_r',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf   ,dirout=dirout,with_gdet=True,xmin=xmin,xmax=xmax,minf=-4,maxf=3)
            polp(itime=i,dirname="./",show_plot=False,funcname='hydro_flux_r',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf   ,dirout=dirout,with_gdet=True,xmin=xmin,xmax=xmax,minf=-4,maxf=-0.1)
            polp(itime=i,dirname="./",show_plot=False,funcname='mass_flux_r',dolog=True,savepng=True,irad=irad,iph=None,ith=None,corotate=False,plot_bhs_traj=True,namesuffix=suf    ,dirout=dirout,with_gdet=True,xmin=xmin,xmax=xmax,minf=-4,maxf=3)

    return

######################################


######################################

# rhomin=0
# rhomax=4.5
#
# uumin = -9
# uumax = -3
#
# bsq_min = -9
# bsq_max = -3
#
# gammamin = 1.
# gammamax = 2.
#
# bor_min = -4.
# bor_max = 4.
#
# i = 30
# suf='-rmax2'
# dirout='./plots'
#
# nt = 6
#
# if 1 :
#     itime = 1
#     rmax = 40.
#     polp(itime=itime,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,minf=-14,maxf=0.5,xmin=79,xmax=121,zmin=-2,zmax=2.,freescale=True)
#     polp(itime=itime,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,irad=None,iph=0,ith=None,ubh=True,xmin=79,xmax=121,zmin=-2,zmax=2.,freescale=True)
#     polp(itime=itime,dirname="./",show_plot=False,funcname='B2',dolog=False,savepng=True,irad=None,iph=0,ith=None,ubh=True,xmin=79,xmax=121,zmin=-2,zmax=2.,freescale=True)
#     polp(itime=itime,dirname="./",show_plot=False,funcname='rho',dolog=True,savepng=True,irad=None,iph=None,ith=16,ubh=True,minf=-15,maxf=0.5,xmin=-121,xmax=121,ymin=-121,zmax=121)
#     polp(itime=itime,dirname="./",show_plot=False,funcname='B2',dolog=False,savepng=True,irad=None,iph=None,ith=16,ubh=True,xmin=-121,xmax=121,ymin=-121,zmax=121)
#     polp(itime=itime,dirname="./",show_plot=False,funcname='uu',dolog=True,savepng=True,irad=None,iph=None,ith=16,ubh=True,xmin=-121,xmax=121,ymin=-121,zmax=121)
# #
# if 1 :
#     rmax = 20.
#     dolog=False
#     for i in np.arange(nt) :
#         run_tag = 'SOD.patch_1'
#         polp(itime=i,dirname="./",dirout=dirout,readfunc=True,move_patch=True,show_plot=False,funcname='ucon0',dolog=dolog,savepng=True,irad=None,ith=4,iph=None,ubh=True,run_tag=run_tag,cart_coords=False,namesuffix='-loc-only',xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax)
#         polp(itime=i,dirname="./",dirout=dirout,readfunc=True,move_patch=True,show_plot=False,funcname='ucon1',dolog=dolog,savepng=True,irad=None,ith=4,iph=None,ubh=True,run_tag=run_tag,cart_coords=False,namesuffix='-loc-only',xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax)
#         polp(itime=i,dirname="./",dirout=dirout,readfunc=True,move_patch=True,show_plot=False,funcname='ucon2',dolog=dolog,savepng=True,irad=None,ith=4,iph=None,ubh=True,run_tag=run_tag,cart_coords=False,namesuffix='-loc-only',xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax)
#         polp(itime=i,dirname="./",dirout=dirout,readfunc=True,move_patch=True,show_plot=False,funcname='ucon3',dolog=dolog,savepng=True,irad=None,ith=4,iph=None,ubh=True,run_tag=run_tag,cart_coords=False,namesuffix='-loc-only',xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax)
#
#         run_tag = 'SOD.patch_0'
#         polp(itime=i,dirname="./",dirout=dirout,readfunc=True,move_patch=True,show_plot=False,funcname='ucon0',dolog=dolog,savepng=True,irad=None,iph=4,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,namesuffix='-glob-only')
#         polp(itime=i,dirname="./",dirout=dirout,readfunc=True,move_patch=True,show_plot=False,funcname='ucon1',dolog=dolog,savepng=True,irad=None,iph=4,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,namesuffix='-glob-only')
#         polp(itime=i,dirname="./",dirout=dirout,readfunc=True,move_patch=True,show_plot=False,funcname='ucon2',dolog=dolog,savepng=True,irad=None,iph=4,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,namesuffix='-glob-only')
#         polp(itime=i,dirname="./",dirout=dirout,readfunc=True,move_patch=True,show_plot=False,funcname='ucon3',dolog=dolog,savepng=True,irad=None,iph=4,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,namesuffix='-glob-only')
#
#
#
# if 1 :
#     rmax = 20.
#     dolog=False
#     for i in np.arange(nt) :
# #    for i in np.arange(1) :
#         run_tag = 'SOD.patch_1'
#         polp(itime=i,dirname="./",dirout=dirout,move_patch=True,show_plot=False,funcname='rho',dolog=dolog,savepng=True,irad=None,iph=None,ith=4,ubh=True,run_tag=run_tag,cart_coords=False,minf=1.24e4*1.01,maxf=1.1e5,namesuffix='-loc-only',xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax)
#         artists, xx1, yy1, func1 = polp(itime=i,dirname="./",move_patch=True,show_plot=False,funcname='rho',irad=None,iph=None,ith=4,run_tag=run_tag,just_get_data=True,cart_coords=False,dolog=dolog,ubh=True,make_bbox=True)
#
#         run_tag = 'SOD.patch_0'
#         polp(itime=i,dirname="./",artists=artists,dirout=dirout,move_patch=True,show_plot=False,funcname='rho',dolog=dolog,savepng=True,irad=None,iph=4,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,other_data=[xx1,yy1,func1],minf=1.24e4*1.01,maxf=1.1e5)
#         polp(itime=i,dirname="./",dirout=dirout,move_patch=True,show_plot=False,funcname='rho',dolog=dolog,savepng=True,irad=None,iph=4,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,minf=1.24e4*1.01,maxf=1.1e5,namesuffix='-glob-only')
#
#         if 1 :
#             run_tag = 'SOD.patch_1'
#             polp(itime=i,dirname="./",dirout=dirout,move_patch=True,show_plot=False,funcname='uu',dolog=dolog,savepng=True,irad=None,iph=None,ith=4,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=False,minf=0.24,maxf=2.6,namesuffix='-loc-only',xmin=-rmax,xmax=rmax,ymin=-rmax,ymax=rmax)
#             artists, xx1, yy1, func1 = polp(itime=i,dirname="./",move_patch=True,show_plot=False,funcname='uu',irad=None,iph=None,ith=4,run_tag=run_tag,just_get_data=True,cart_coords=False,dolog=dolog,ubh=True,make_bbox=True)
#
#
#             run_tag = 'SOD.patch_0'
#             polp(itime=i,dirname="./",dirout=dirout,artists=artists,move_patch=True,show_plot=False,funcname='uu',dolog=dolog,savepng=True,irad=None,iph=4,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,other_data=[xx1,yy1,func1],minf=0.24,maxf=2.6)
#             polp(itime=i,dirname="./",dirout=dirout,move_patch=True,show_plot=False,funcname='uu',dolog=dolog,savepng=True,irad=None,iph=4,ith=None,ubh=True,xmin=-rmax,xmax=rmax,zmin=-rmax,zmax=rmax,run_tag=run_tag,cart_coords=True,minf=0.24,maxf=2.6,namesuffix='-glob-only')
#
#
