import numpy as np
import h5py
from . import harm
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt

######
#     #
#     #
######
#   #
#    #
#     #

################################################################################
def poynting_flux_r(slice_obj, h5file, h5gdump=None, with_gdet=False):
    v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file)
    v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file)
    v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file)
    B1 = harm.read_3d_hdf5_slice('B1', slice_obj, h5file)
    B2 = harm.read_3d_hdf5_slice('B2', slice_obj, h5file)
    B3 = harm.read_3d_hdf5_slice('B3', slice_obj, h5file)
    bsq = harm.read_3d_hdf5_slice('bsq', slice_obj, h5file)

    if h5gdump is None:
        dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11', slice_obj, h5file)
        dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12', slice_obj, h5file)
        dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13', slice_obj, h5file)
    else:
        dx_dxp11 = harm.read_3d_hdf5_slice('dxdxp11', slice_obj, h5gdump)
        dx_dxp12 = harm.read_3d_hdf5_slice('dxdxp12', slice_obj, h5gdump)
        dx_dxp13 = harm.read_3d_hdf5_slice('dxdxp13', slice_obj, h5gdump)

    ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
    bcon0, bcon1, bcon2, bcon3 = harm.bcon_calc(B1, B2, B3, ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3)
    bcov0, bcov1, bcov2, bcov3 = harm.lower2(metric, bcon0, bcon1, bcon2, bcon3)

    bsq = bsq * ucov0

    #  EM flux =  - T^r_t
    em_flux_1t = - ucon1 * bsq + bcon1 * bcov0
    em_flux_2t = - ucon2 * bsq + bcon2 * bcov0
    em_flux_3t = - ucon3 * bsq + bcon3 * bcov0

    if h5gdump is None:
        dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11', slice_obj, h5file)
        dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12', slice_obj, h5file)
        dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13', slice_obj, h5file)
    else:
        dx_dxp11 = harm.read_3d_hdf5_slice('dxdxp11', slice_obj, h5gdump)
        dx_dxp12 = harm.read_3d_hdf5_slice('dxdxp12', slice_obj, h5gdump)
        dx_dxp13 = harm.read_3d_hdf5_slice('dxdxp13', slice_obj, h5gdump)

    val = em_flux_1t * dx_dxp11 + em_flux_2t * dx_dxp12 + em_flux_3t * dx_dxp13
    if with_gdet:
        val *= metric['gdet']

    return val


################################################################################
def hydro_flux_r(slice_obj, h5file, h5gdump=None, with_gdet=False):
    v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file)
    v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file)
    v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file)
    rho = harm.read_3d_hdf5_slice('rho',slice_obj,h5file)
    uu = harm.read_3d_hdf5_slice( 'uu',slice_obj,h5file)
    ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)

    gam = h5file['Header/Grid/gam'][0]
    energy = -(rho + gam*uu)*ucov0

    #  flux =  - T^r_t - rho u^r
    hyd_flux_1t = (energy - rho) * ucon1
    hyd_flux_2t = (energy - rho) * ucon2
    hyd_flux_3t = (energy - rho) * ucon3

    if h5gdump is None:
        dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11', slice_obj, h5file)
        dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12', slice_obj, h5file)
        dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13', slice_obj, h5file)
    else:
        dx_dxp11 = harm.read_3d_hdf5_slice('dxdxp11', slice_obj, h5gdump)
        dx_dxp12 = harm.read_3d_hdf5_slice('dxdxp12', slice_obj, h5gdump)
        dx_dxp13 = harm.read_3d_hdf5_slice('dxdxp13', slice_obj, h5gdump)

    val = hyd_flux_1t * dx_dxp11 + hyd_flux_2t * dx_dxp12 + hyd_flux_3t * dx_dxp13
    if with_gdet :
        val *= metric['gdet']

    return val


################################################################################
def mass_flux_r(slice_obj, h5file, h5gdump=None, with_gdet=False):
    v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file)
    v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file)
    v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file)
    rho = harm.read_3d_hdf5_slice('rho',slice_obj,h5file)
    ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)

    #  mass flux =  rho u^r
    mass_flux_1 = rho * ucon1
    mass_flux_2 = rho * ucon2
    mass_flux_3 = rho * ucon3

    if h5gdump is None:
        dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11', slice_obj, h5file)
        dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12', slice_obj, h5file)
        dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13', slice_obj, h5file)
    else:
        dx_dxp11 = harm.read_3d_hdf5_slice('dxdxp11', slice_obj, h5gdump)
        dx_dxp12 = harm.read_3d_hdf5_slice('dxdxp12', slice_obj, h5gdump)
        dx_dxp13 = harm.read_3d_hdf5_slice('dxdxp13', slice_obj, h5gdump)

    val = mass_flux_1 * dx_dxp11 + mass_flux_2 * dx_dxp12 + mass_flux_3 * dx_dxp13
    if with_gdet :
        val *= metric['gdet']

    return val


################################################################################
def total_flux_r(slice_obj, h5file, h5gdump=None, with_gdet=False):
    v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file)
    v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file)
    v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file)
    rho = harm.read_3d_hdf5_slice('rho',slice_obj,h5file)
    uu = harm.read_3d_hdf5_slice( 'uu',slice_obj,h5file)
    B1 = harm.read_3d_hdf5_slice( 'B1',slice_obj,h5file)
    B2 = harm.read_3d_hdf5_slice( 'B2',slice_obj,h5file)
    B3 = harm.read_3d_hdf5_slice( 'B3',slice_obj,h5file)
    bsq = harm.read_3d_hdf5_slice('bsq',slice_obj,h5file)

    if h5gdump is None:
        dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11', slice_obj, h5file)
        dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12', slice_obj, h5file)
        dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13', slice_obj, h5file)
    else:
        dx_dxp11 = harm.read_3d_hdf5_slice('dxdxp11', slice_obj, h5gdump)
        dx_dxp12 = harm.read_3d_hdf5_slice('dxdxp12', slice_obj, h5gdump)
        dx_dxp13 = harm.read_3d_hdf5_slice('dxdxp13', slice_obj, h5gdump)

    ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
    bcon0, bcon1, bcon2, bcon3 = harm.bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
    bcov0, bcov1, bcov2, bcov3 = harm.lower2(metric,bcon0,bcon1,bcon2,bcon3)

    gam = h5file['Header/Grid/gam'][0]
    energy = -(rho + gam * uu) * ucov0

    bsq = bsq * ucov0

    #  EM flux =  - T^r_t
    em_flux_1t = -ucon1 * bsq + bcon1 * bcov0
    em_flux_2t = -ucon2 * bsq + bcon2 * bcov0
    em_flux_3t = -ucon3 * bsq + bcon3 * bcov0
    hyd_flux_1t = energy * ucon1
    hyd_flux_2t = energy * ucon2
    hyd_flux_3t = energy * ucon3
    mass_flux_1 = rho * ucon1
    mass_flux_2 = rho * ucon2
    mass_flux_3 = rho * ucon3
    total_flux_1t = hyd_flux_1t + em_flux_1t - mass_flux_1
    total_flux_2t = hyd_flux_2t + em_flux_2t - mass_flux_2
    total_flux_3t = hyd_flux_3t + em_flux_3t - mass_flux_3

    val = total_flux_1t * dx_dxp11 + total_flux_2t * dx_dxp12  + total_flux_3t * dx_dxp13
    if with_gdet:
        val *= metric['gdet']

    return val


################################################################################
def all_fluxes_r(slice_obj, h5file, h5gdump=None, with_gdet=False):
    v1  = harm.read_3d_hdf5_slice( 'v1', slice_obj, h5file)
    v2  = harm.read_3d_hdf5_slice( 'v2', slice_obj, h5file)
    v3  = harm.read_3d_hdf5_slice( 'v3', slice_obj, h5file)
    rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
    uu  = harm.read_3d_hdf5_slice( 'uu', slice_obj, h5file)
    B1  = harm.read_3d_hdf5_slice( 'B1', slice_obj, h5file)
    B2  = harm.read_3d_hdf5_slice( 'B2', slice_obj, h5file)
    B3  = harm.read_3d_hdf5_slice( 'B3', slice_obj, h5file)
    bsq = harm.read_3d_hdf5_slice('bsq', slice_obj, h5file)

    if h5gdump is None:
        dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11', slice_obj, h5file)
        dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12', slice_obj, h5file)
        dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13', slice_obj, h5file)
    else:
        dx_dxp11 = harm.read_3d_hdf5_slice('dxdxp11', slice_obj, h5gdump)
        dx_dxp12 = harm.read_3d_hdf5_slice('dxdxp12', slice_obj, h5gdump)
        dx_dxp13 = harm.read_3d_hdf5_slice('dxdxp13', slice_obj, h5gdump)

    ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
    bcon0, bcon1, bcon2, bcon3 = harm.bcon_calc(B1, B2, B3, ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3)
    bcov0, bcov1, bcov2, bcov3 = harm.lower2(metric, bcon0, bcon1, bcon2, bcon3)

    gam = h5file['Header/Grid/gam'][0]
    energy = -(rho + gam*uu)*ucov0

    bsq = bsq * ucov0

    #  EM flux =  - T^r_t
    em_flux_1t  = - ucon1 * bsq + bcon1 * bcov0
    em_flux_2t  = - ucon2 * bsq + bcon2 * bcov0
    em_flux_3t  = - ucon3 * bsq + bcon3 * bcov0
    hyd_flux_1t = energy * ucon1
    hyd_flux_2t = energy * ucon2
    hyd_flux_3t = energy * ucon3
    mass_flux_1 = rho * ucon1
    mass_flux_2 = rho * ucon2
    mass_flux_3 = rho * ucon3
    total_flux_1t = hyd_flux_1t + em_flux_1t - mass_flux_1
    total_flux_2t = hyd_flux_2t + em_flux_2t - mass_flux_2
    total_flux_3t = hyd_flux_3t + em_flux_3t - mass_flux_3
    hyd_flux_1t -= mass_flux_1
    hyd_flux_2t -= mass_flux_2
    hyd_flux_3t -= mass_flux_3

    total_r    = total_flux_1t * dx_dxp11 + total_flux_2t * dx_dxp12 + total_flux_3t * dx_dxp13
    mflux_r    = mass_flux_1   * dx_dxp11 + mass_flux_2   * dx_dxp12 + mass_flux_3   * dx_dxp13
    hyd_flux_r = hyd_flux_1t   * dx_dxp11 + hyd_flux_2t   * dx_dxp12 + hyd_flux_3t   * dx_dxp13
    em_flux_r  = em_flux_1t    * dx_dxp11 + em_flux_2t    * dx_dxp12 + em_flux_3t    * dx_dxp13

    if with_gdet:
        total_r    *= metric['gdet']
        mflux_r    *= metric['gdet']
        hyd_flux_r *= metric['gdet']
        em_flux_r  *= metric['gdet']

    return total_r, mflux_r, hyd_flux_r, em_flux_r


####### #     # ####### #######    #
   #    #     # #          #      # #
   #    #     # #          #     #   #
   #    ####### #####      #    #     #
   #    #     # #          #    #######
   #    #     # #          #    #     #
   #    #     # #######    #    #     #
# TODO: Finish all fluxes in theta
################################################################################
def total_flux_th(slice_obj, h5file, h5gdump=None, with_gdet=False):
    v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file)
    v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file)
    v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file)
    rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
    uu = harm.read_3d_hdf5_slice('uu', slice_obj, h5file)
    B1 = harm.read_3d_hdf5_slice('B1', slice_obj, h5file)
    B2 = harm.read_3d_hdf5_slice('B2', slice_obj, h5file)
    B3 = harm.read_3d_hdf5_slice('B3', slice_obj, h5file)
    bsq = harm.read_3d_hdf5_slice('bsq', slice_obj, h5file)

    if h5gdump is None:
        dx_dxp21 = harm.read_3d_hdf5_slice('dx_dxp21', slice_obj, h5file)
        dx_dxp22 = harm.read_3d_hdf5_slice('dx_dxp22', slice_obj, h5file)
        dx_dxp23 = harm.read_3d_hdf5_slice('dx_dxp23', slice_obj, h5file)
    else:
        dx_dxp21 = harm.read_3d_hdf5_slice('dxdxp21', slice_obj, h5gdump)
        dx_dxp22 = harm.read_3d_hdf5_slice('dxdxp22', slice_obj, h5gdump)
        dx_dxp23 = harm.read_3d_hdf5_slice('dxdxp23', slice_obj, h5gdump)

    ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
    bcon0, bcon1, bcon2, bcon3 = harm.bcon_calc(B1, B2, B3, ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3)
    bcov0, bcov1, bcov2, bcov3 = harm.lower2(metric, bcon0, bcon1, bcon2, bcon3)

    gam = h5file['Header/Grid/gam'][0]
    energy = -(rho + gam * uu) * ucov0

    bsq = bsq * ucov0

    #  EM flux =  - T^th_t
    em_flux_1t = -ucon1 * bsq + bcon1 * bcov0
    em_flux_2t = -ucon2 * bsq + bcon2 * bcov0
    em_flux_3t = -ucon3 * bsq + bcon3 * bcov0
    hyd_flux_1t = energy * ucon1
    hyd_flux_2t = energy * ucon2
    hyd_flux_3t = energy * ucon3
    mass_flux_1 = rho * ucon1
    mass_flux_2 = rho * ucon2
    mass_flux_3 = rho * ucon3
    total_flux_1t = hyd_flux_1t + em_flux_1t - mass_flux_1
    total_flux_2t = hyd_flux_2t + em_flux_2t - mass_flux_2
    total_flux_3t = hyd_flux_3t + em_flux_3t - mass_flux_3

    val = total_flux_1t * dx_dxp21 + total_flux_2t * dx_dxp22 + total_flux_3t * dx_dxp23
    if with_gdet :
        val *= metric['gdet']

    return val


################################################################################
def flux_integrate(ibeg, iend, irr):

    nt = iend - ibeg

    # dirout='./'
    # dumpbase = 'KDHARM0'
    # timeid = '%05d' %itime #monopole cleaner dumps

    # irr = 590
    slice_obj = (slice(irr, irr+1, 1), slice(2, 14, 1), slice(None,None,None))

    id = 0
    for itime in np.arange(ibeg, iend):
        # timeid = '{06d}'.format(self.itime) #harm HDF5 dumps
        # timeid = '{05d}'.format(self.itime) #monopole cleaner dumps
        # dumpfile  = dirout + 'dumps/' + 'KDHARM0.' +  timeid + '.h5'
        h5file = h5py.File(dumpfile, 'r')

        if itime == ibeg:
            idim, jdim, kdim, xp1, xp2, xp3  = harm.get_grid(h5file)
            total_out = np.zeros((nt, kdim))
            mflux_out = np.zeros((nt, kdim))
            hyd_out = np.zeros((nt, kdim))
            em_out = np.zeros((nt, kdim))
            t_out = np.zeros(nt)
            ph_tmp = harm.read_3d_hdf5_slice('x3', slice_obj,h5file)
            ph_out = np.squeeze(ph_tmp[0,:])
            ph_tmp = 0

        t_out[id] = h5file['/Header/Grid/t'][0]
        total_r, mflux_r, hyd_flux_r, em_flux_r = harm.calc_fluxes(slice_obj, h5file)
        total_out[id,:] = np.sum(total_r, axis=0)
        mflux_out[id,:] = np.sum(mflux_r, axis=0)
        hyd_out[id,:] = np.sum(hyd_flux_r, axis=0)
        em_out[id,:] = np.sum(em_flux_r, axis=0)

        id += 1
        h5file.close()

    with h5py.File('fluxes.h5', 'w') as outh5:
        outh5.create_dataset('total_flux', data=total_out)
        outh5.create_dataset('mflux_flux', data=mflux_out)
        outh5.create_dataset('hydro_flux', data=hyd_out)
        outh5.create_dataset('em_flux', data=em_out)
        outh5.create_dataset('phi', data=ph_out)
        outh5.create_dataset('times', data=t_out)

    return


################################################################################
def flux_plot(flux_file, dirout, trajfile, dolog=False, minf=None, maxf=None):

    h5file = h5py.File(flux_file, 'r')
    xout = h5file['phi'][0]
    yout = h5file['times'][0]

    total_out = h5file['total_flux'][...]
    mflux_out = h5file['mflux_flux'][...]
    hyd_out = h5file['hydro_flux'][...]
    em_out = h5file['em_flux'][...]

    # trajfile = './dumps/my_bbh_trajectory.out'
    # TODO: upgrade for spinning BHs
    tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(trajfile, usecols=(0,4,5,6,7), unpack=True) #old metrics
    phase1 = np.arctan2(ybh1_traj, xbh1_traj)
    phase2 = np.arctan2(ybh2_traj, xbh2_traj)

    loc = phase1 < 0.
    phase1[loc] = 2.*np.pi - np.abs(phase1[loc])
    loc = phase2 < 0.
    phase2[loc] = 2.*np.pi - np.abs(phase2[loc])

    [xmin, xmax, ymin, ymax] = [np.min(xout), np.max(xout), np.min(yout), np.max(yout)]

    minf = 0.1
    maxf = 4

    if dolog :
        minf = np.log10(minf)
        maxf = np.log10(maxf)

    nticks = 10.
    nlev = 256.
    nwin=0
    savepng = 1
    cmap=plt.cm.Spectral_r

    func = total_out
    if dolog:
        func[func == 0] += 1.e-40       # so that there is no log of zero...
        func = np.log10(np.abs(func))
    funcname = 'total_flux'
    fignames = [dirout + funcname + '-'+'phi-vs-t']
    figs = [plt.figure(nwin)]
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    # ax.set_title('%s' %title)
    ax.set_xlabel('phi')
    ax.set_ylabel('t [M]')
    levels = np.linspace(minf, maxf, nlev)
    rticks = np.linspace(minf, maxf, nticks)
    ax.axis([xmin, xmax, ymin, ymax])
    ax.plot(phase1, tbh_traj, 'w,')
    ax.plot(phase2, tbh_traj, 'w,')
    CP = ax.contourf(xout, yout, func, levels, cmap=cmap, extend='both')
    figs[-1].colorbar(CP, ticks=rticks, format='%1.4g')

    nwin+=1
    func = hyd_out
    if dolog:
        func[func == 0] += 1.e-40       # so that there is no log of zero...
        func = np.log10(np.abs(func))
    funcname = 'hydro_flux'
    fignames = np.append(fignames, dirout + funcname + '-' + 'phi-vs-t')
    figs = np.append(figs,plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    # ax.set_title('%s' %title)
    ax.set_xlabel('phi')
    ax.set_ylabel('t [M]')
    levels = np.linspace(minf,maxf,nlev)
    rticks = np.linspace(minf,maxf,nticks)
    ax.axis([xmin,xmax,ymin,ymax])
    ax.plot(phase1, tbh_traj, 'w,')
    ax.plot(phase2, tbh_traj, 'w,')
    CP = ax.contourf(xout, yout, func, levels, cmap=cmap, extend='both')
    figs[-1].colorbar(CP, ticks=rticks, format='%1.4g')

    nwin+=1
    func = em_out
    if dolog:
        func[func == 0] += 1.e-40       # so that there is no log of zero...
        func = np.log10(np.abs(func))
    funcname = 'em_flux'
    fignames = np.append(fignames, dirout + funcname + '-' + 'phi-vs-t')
    figs = np.append(figs, plt.figure(nwin))
    figs[-1].clf()
    ax = figs[-1].add_subplot(111)
    # ax.set_title('%s' %title)
    ax.set_xlabel('phi')
    ax.set_ylabel('t [M]')
    levels = np.linspace(minf,maxf,nlev)
    rticks = np.linspace(minf,maxf,nticks)
    ax.axis([xmin,xmax,ymin,ymax])
    ax.plot(phase1,tbh_traj,'w,')
    ax.plot(phase2,tbh_traj,'w,')
    CP = ax.contourf(xout, yout,func,levels,cmap=cmap,extend='both')
    figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

    print("fignames = ", fignames)

    if savepng :
        for i in np.arange(len(figs)) :
            figs[i].savefig('{}.png'.format(fignames[i]), dpi=300)

    print("All done ")

    return
