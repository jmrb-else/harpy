from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import h5py
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
from scipy.interpolate import interp1d
from . import coord_transforms as ct
from . import harm
from . import fluxes as flx
from matplotlib import ticker

polp_dir = os.path.dirname(os.path.abspath(__file__))

def vchar(h5filename, direction):
    metric = harm.get_metric(h5filename)
    h5file = h5py.File(h5filename,'r')
    gam = h5file['Header/Grid/gam'][...]
    rho = h5file['rho'][...]
    uu = h5file['uu' ][...]
    bsq = h5file['bsq'][...]
    gamma = h5file['gamma'][...]
    gcon00 = metric['gcon00']
    if direction == 1:
        vi = h5file['v1'][...]
        gcon0i = metric['gcon01']
        gconii = metric['gcon11']
        betai = metric['beta1']
    elif direction == 2:
        vi = h5file['v2'][...]
        gcon0i = metric['gcon02']
        gconii = metric['gcon22']
        betai = metric['beta2']
    elif direction == 3:
        vi = h5file['v3'][...]
        gcon0i = metric['gcon03']
        gconii = metric['gcon33']
        betai = metric['beta3']
    else:
        print("Failed in vchar calc... invalid direction")
        return
    h5file.close()
    print("vars set by dir")

    pgas = (gam - 1.)*uu
    eta  = pgas + rho + uu + bsq
    cms2 = (gam * pgas + bsq) / eta
    eta = 0
    pgas = 0

    ucon_t = gamma / metric['alpha']
    ucon_i = vi - ucon_t * betai

    Au2  = ucon_i*ucon_i
    Bu2  = ucon_t*ucon_t
    AuBu = ucon_i*ucon_t

    ucon_t = 0
    ucon_i = 0

    A = Bu2  - (gcon00 + Bu2 )*cms2
    B = AuBu - (gcon0i + AuBu)*cms2
    C = Au2  - (gconii + Au2 )*cms2

    discr = np.maximum(B*B - A*C,np.zeros((np.shape(B))))
    discr = np.sqrt(discr)
    denom = 1. / A

    vp = -(-B + discr)*denom
    vm = -(-B - discr)*denom

    vmax = np.maximum(vp,vm)
    vmin = np.minimum(vp,vm)

    vmin *= -1.

    vmax = np.maximum(vmax,np.zeros((np.shape(vmax))))
    vmin = np.maximum(vmin,np.zeros((np.shape(vmin))))

    vmax = np.maximum(np.abs(vmax),np.abs(vmin))

    return vmax


class harmfunc(object):
    """
    Reading and plotting HARM3D quantities.
    """

    def __init__(self, funcname):
        """
        Parameters
        ----------
        funcname : str
            Quantity to be analyzed. List of options: 'dMdt
        """
        self.funcname = funcname
        super().__init__()

    # TODO: compare dumps/runs as a separate function

    def slice_func(self, itime=0, irad=None, ith=None, iph=None, func0=None, rmin=None, rmax=None, xmin=None, xmax=None, ymin=None, ymax=None, zmin=None, zmax=None, use_num_coord=False, cart_coords=False, spinning=False, plot_bhs_traj=False, trajfile=None, ubh=False, corotate=False, plot_potential=False, dirname=None, dumpfile=None, radfile=None, gdumpfile=None, run_tag="KDHARM0", fieldlines=None, dolog=False, slice_obj=None, usegdump=False, usegdumpcoord=False, use_stat2=False, stat2_factor=0, userad=False, readfunc=False, rho_cutout_value=None, take_ratio=False, relerror=False, compare_runs=False, compare_dir=None, with_gdet=False, EXTEND_CENTRAL_CUTOUT=False, fillaxis=False, fillwedge=True, fillcutout=False, use_spher_coord=False, plotcentre=None, normalize_func=False, move_patch=False, flip_horizontal=False):
        """
        Parameters
        ----------
        nticks : int, optional
            number of ticks for the colorbar
        ubh : bool, optional

        Returns
        -------
        func : float array, optional
        """

        self.itime = itime
        self.irad = irad
        self.ith = ith
        self.iph = iph
        self.use_num_coord = use_num_coord
        self.plot_bhs_traj = plot_bhs_traj
        self.spinning = spinning
        self.corotate = corotate
        self.cart_coords = cart_coords
        self.plot_potential = plot_potential
        self.run_tag = run_tag
        self.dolog = dolog
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.zmin = zmin
        self.zmax = zmax
        self.fieldlines = fieldlines

        if dirname is None:
            # should tell me my current working directory, *not* where this
            # script is located
            self.dirname = os.getcwd()
        else:
            self.dirname = dirname

        if trajfile is None:
            self.trajfile = self.dirname + self.run_tag + '_bbh_trajectory.out'
            #trajfile = self.dirname + '/my_bbh_trajectory.out'
        else:
            self.trajfile = trajfile

        if ubh is True:
            self.ubh_horizon = True
        else:
            self.ubh_horizon = False

        if self.use_num_coord:
            self.plot_bhs_traj = False
        #-scn ok
        #if ith is None:
        #    self.plot_bhs_traj = False

        if self.irad is not None and self.corotate:
            print(" corotate is not implemented for th-phi plots yet!! ")
            return

        self.timeid = '{:06d}'.format(self.itime) #harm HDF5 dumps
        #self.timeid = '{:05d}'.format(self.itime) #monopole cleaner dumps

        if dumpfile is None:
            self.dumpfile = self.dirname + self.run_tag + '.' + self.timeid + '.h5'
        else:
            self.dumpfile = dumpfile

        if radfile is None:
            self.radfile = self.dirname + self.run_tag + '.RADFLUX.' + self.timeid + '.h5'
        else:
            self.radfile = radfile

        if gdumpfile is None:
            self.gdumpfile = self.dirname + self.run_tag + '.gdump.h5'
        else:
            self.gdumpfile = gdumpfile

        self.fullname = self.dumpfile


        print("################")
        print("funcname = ", self.funcname)
        print("readfunc = ", readfunc)
        print("################")

        if plotcentre is not None:
            self.plot_bhs_traj = True

        h5file = h5py.File(self.fullname, 'r')

        self.idim, self.jdim, self.kdim, xp1, xp2, xp3 = harm.get_grid(h5file)
        ndims = np.array([self.idim, self.jdim, self.kdim])

        # we get the time level:
        self.time = h5file['Header/Grid/t'][0]
        # self.time = 5e-3
        print("time = ", self.time)

        if self.ith == 'equator':
            self.ith = int(ndims[1] / 2)
            if ndims[1] == 1:
                EQUATORIAL_RUN = True
            if self.funcname == 'dM' or self.funcname == 'dMdt':
                SUM_THETA = True
        # if ith is None and (corotate or iph is None):
        if self.corotate and self.ith is None and self.iph is not None:
            if self.spinning:
                # tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile,usecols=(0,1,2,4,5), unpack=True) #spinning
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile, usecols=(0,1,2,4,5), unpack=True)
            else:
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile, usecols=(0,4,5,6,7), unpack=True) #old metrics
                # r_hor1 = m_bh1[0]
                # r_hor2 = m_bh2[0]

            fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
            fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
            fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
            fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

            self.xbh1 = fxbh1(self.time+0.1)#/np.sin(theta)
            self.ybh1 = fybh1(self.time+0.1)#/np.sin(theta)
            self.xbh2 = fxbh2(self.time+0.1)#/np.sin(theta)
            self.ybh2 = fybh2(self.time+0.1)#/np.sin(theta)

            phase = np.arctan2(self.ybh1, self.xbh1)
            #renormalize phase s.t phase \in (0,2pi)
            if phase < 0.:
                phase = 2. * np.pi - np.abs(phase)
            dxp3 = h5file['Header/Grid/dx3'][0]
            kshift = int( (phase) / ( 2. * np.pi * dxp3 ) - 0. + 0.5 )
            phasediffmin = 2.e6
            print("kdim = ", self.kdim)
            slice_obj_tmp = (slice(0,1,1), slice(0,1,1), slice(None, None, None))
            ph_tmp = harm.read_3d_hdf5_slice('x3', slice_obj_tmp, h5file)
            for index in range(self.kdim):
                phasediff = np.abs(ph_tmp[index] - phase)
                if phasediffmin > phasediff:
                    phasediffmin = phasediff
                    indexcrit = index
            print("Expected shift found shift ",kshift,indexcrit)
            self.iph = kshift + 1

        # set grid coordinate functions
        if slice_obj is None:
            slice_obj = harm.calc_slice(self.irad, self.ith, self.iph, ndims)


        if usegdumpcoord:
            h5coord = h5py.File(self.gdumpfile, 'r')
        else:
            h5coord = h5file

        rr = harm.read_3d_hdf5_slice('x1', slice_obj, h5coord)
        th = harm.read_3d_hdf5_slice('x2', slice_obj, h5coord)
        ph = harm.read_3d_hdf5_slice('x3', slice_obj, h5coord)

        if usegdumpcoord:
            h5coord.close()

        print("##################################")
        print(" rr.shape()  = ", np.shape(rr))

        self.rr_slice = rr
        self.ph_slice = ph
        self.th_slice = th

        if rmin is None:
            self.rmin = np.min(rr)
        else:
            self.rmin = rmin
        if rmax is None:
            self.rmax = np.max(rr)
        else:
            self.rmax = rmax

        # if userad:
        #     fullname = self.radfile
        #     h5file.close()
        #     h5file = h5py.File(fullname, 'r')

        if usegdump:
            fullname = self.gdumpfile
            h5file.close()
            h5file = h5py.File(fullname, 'r')

        if use_stat2:
            stat2file = '{0:s}/dumps/{1:s}.STAT2.{2:06d}.h5'.format(self.dirname, self.run_tag, self.itime - stat2_factor)
            #fullname = self.stat2file
            #h5file.close()
            #h5file = h5py.File(self.stat2file,'r')
            h5file = h5py.File(fullname,'r')
            h5stat2 = h5py.File(stat2file,'r')

        if userad:
            h5file2 = h5py.File(self.radfile, 'r')
        else:
            h5file2 = h5py.File(self.dumpfile, 'r')

        if self.irad is not None :
            print("r of slice = ", self.rr_slice[0,0])


        # if we've specified something for func0 we will plot it instead.
        if func0 is not None:
            func = func0
        elif readfunc:
            if harm.check_if_dataset_exists(self.funcname, h5file):
                func = harm.read_3d_hdf5_slice(self.funcname, slice_obj, h5file)
            elif harm.check_if_dataset_exists(self.funcname, h5file2):
                func = harm.read_3d_hdf5_slice(self.funcname, slice_obj, h5file2)
            else:
                sys.exit("slice_func(): trying to readfunc but funcname does not exist: ", self.funcname)

        elif self.funcname == 'uconrr':
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file2)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file2)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file2)
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'][0]
            h5file2.close()
            #uconrr, uconth, uconph = harm.calc_uconKS(slice_obj, h5file, v1, v2, v3) #assumes diagonal metric to get velocities
            ucon0, uconrrp, uconthp, uconphp = harm.calc_ucon(slice_obj, h5file, v1, v2, v3)  #get ucon wrt numerical coordinates
            #non-dynamic coords
            #uconrr, uconth, uconph = uconp2ucon(slice_obj,h5file,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
            #dynamic coordinates
            ucon00,uconrr, uconth, uconph = harm.uconp2ucon_dynamic(slice_obj, h5file, ucon0,uconrrp,uconthp,uconphp)
            if met_type == 2:
                ucon0, uconrr, uconth, uconph = harm.ks2bl_con(slice_obj, h5file,uconrr,uconth,uconph) #convert from ks to bl if necessary.
            uconth = 0
            uconph = 0
            func   = uconrr  / ucon00 #should be uconrr dennis
        elif self.funcname == 'uconrrbh' or self.funcname == 'vrbh':
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file2)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file2)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file2)
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'][0]
            h5file2.close()

            ucon0, uconrrp, uconthp, uconphp = harm.calc_ucon(slice_obj, h5file, v1, v2, v3) #get ucon wrt numerical coordinates
            #dynamic coordinates
            ucon00, uconrr, uconth, uconph = harm.uconp2ucon_dynamic(slice_obj, h5file, ucon0, uconrrp, uconthp, uconphp)

            r1max = 45.
            r2max = 45.
            tt = np.zeros((np.shape(rr))); tt.fill(self.time)
            uconttb, uconrrb, uconthb, uconphb = ct.rank1con2BH1(self.trajfile, tt, rr, th, ph, ucon00, uconrr, uconth,uconph, dx_dxp=None, SETPHASE=None)

            if self.funcname == 'uconrrbh':
                func = uconrrb
            elif self.funcname == 'vrbh':
                func = uconrrb/uconttb

            #Normalize to adot
            tbh_traj, r12_traj = np.loadtxt(self.trajfile, usecols=[0,23], unpack=True)
            adot_traj = np.gradient(r12_traj, tbh_traj[1] - tbh_traj[0])
            fadot = interp1d(tbh_traj, adot_traj, kind='linear')
            print(fadot(self.time), "Normalizing vr by adot")
            func /= fadot(self.time)
        elif self.funcname == 'uconth':
            v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file2)
            v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file2)
            v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file2)
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'][0]
            h5file2.close()
            #uconrr, uconth, uconph = harm.calc_uconKS(slice_obj, h5file, v1, v2, v3) #assumes diagonal metric to get velocities
            ucon0, uconrrp, uconthp, uconphp = harm.calc_ucon(slice_obj, h5file, v1, v2, v3) #get ucon wrt numerical coordinates
            #non-dynamic coords
            #uconrr, uconth, uconph = uconp2ucon(slice_obj, h5file,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
            #dynamic coordinates
            ucon00, uconrr, uconth, uconph = harm.uconp2ucon_dynamic(slice_obj, h5file, ucon0, uconrrp, uconthp, uconphp)
            if met_type == 2:
                ucon0, uconrr, uconth, uconph = harm.ks2bl_con(slice_obj, h5file, uconrr, uconth, uconph) #convert from ks to bl if necessary...

            uconrr = 0
            uconph = 0
            func = uconth
        elif self.funcname == 'uconph':
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file2)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file2)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file2)
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'][0]
            h5file2.close()
            #uconrr, uconth, uconph = harm.calc_uconKS(slice_obj, h5file, v1, v2, v3) #assumes diagonal metric to get physical ucon
            ucon0, uconrrp, uconthp, uconphp = harm.calc_ucon(slice_obj, h5file,v1,v2,v3) #returns ucon wrt numerical coordinates
            #non-dynamic coords
            #uconrr, uconth, uconph = uconp2ucon(slice_obj,h5file,uconrrp,uconthp,uconphp) #take ucon wrt numerical coord to physical coord
            #dynamic coordinates
            ucon00,uconrr, uconth, uconph = harm.uconp2ucon_dynamic(slice_obj, h5file,ucon0,uconrrp,uconthp,uconphp)
            if met_type == 2:
                ucon0, uconrr, uconth, uconph = harm.ks2bl_con(slice_obj, h5file,uconrr,uconth,uconph) #convert from ks to bl if necessary...

            uconth = 0
            uconrr = 0
            func   = uconph #should be uconph dennis here
        elif self.funcname == 'uconph_bh' or self.funcname == 'omega_bh':
            v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file2)
            v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file2)
            v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file2)
            met_type = h5file2['/Header/Macros/METRIC_TYPE_CHOICE'][0]
            h5file2.close()

            ucon0, uconrrp, uconthp, uconphp = harm.calc_ucon(slice_obj, h5file, v1, v2, v3) #get ucon wrt numerical coordinates
            #dynamic coordinates
            ucon00, uconrr, uconth, uconph = harm.uconp2ucon_dynamic(slice_obj, h5file, ucon0, uconrrp, uconthp, uconphp)

            # uconCM = np.zeros((uconrr.shape[0],uconrr.shape[1],uconrr.shape[2],4))
            # uconCM[:,:,:,0] = ucon00[:,:,:]
            # uconCM[:,:,:,1] = uconrr[:,:,:]
            # uconCM[:,:,:,2] = uconth[:,:,:]
            # uconCM[:,:,:,3] = uconph[:,:,:]
            r1max = 50.
            r2max = 50.
            #uconttb, uconrrb, uconthb, uconphb = harm.ucon2bhframes(trajfile, uconCM, rr, ph, r1max, r2max, self.time)
            dx_dxp = harm.get_dx_dxp(slice_obj, h5file)
            TNZ = np.zeros(np.shape(rr))
            TNZ = TNZ.fill(self.time)
            uconttb, uconrrb, uconthb, uconphb = ct.rank1con2BH1(self.trajfile, TNZ, rr, th, ph, ucon00, uconrr, uconth, uconph)
            #print(np.min(ucon0),np.min(ucon00),np.min(uconttb))
            #print(ucon00 - ucon0)
            if self.funcname == 'uconph_bh':
                func = uconphb
            elif self.funcname == 'omega_bh':
                func = uconrrb/uconttb #DENNIS WRONG
        elif self.funcname == 'ucon0_special':
            v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file2)
            v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file2)
            v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file2)
            h5file2.close()
            ucon0, ucon1, ucon2, ucon3 = harm.calc_ucon(slice_obj, h5file, v1, v2, v3)
            ucon1 = 0
            ucon2 = 0
            ucon3 = 0
            func  = ucon0
        elif self.funcname == 'ucon1_special':
            v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file2)
            v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file2)
            v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file2)
            h5file2.close()
            ucon0, ucon1, ucon2, ucon3 = harm.calc_ucon(slice_obj,h5file, v1, v2, v3)
            ucon0 = 0
            ucon2 = 0
            ucon3 = 0
            func  = ucon1
        elif self.funcname == 'ucon2_special':
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file2)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file2)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file2)
            h5file2.close()
            ucon0, ucon1, ucon2, ucon3 = harm.calc_ucon(slice_obj, h5file, v1, v2, v3)
            ucon0 = 0
            ucon1 = 0
            ucon3 = 0
            func  = ucon2
        elif self.funcname == 'ucon3_special':
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file2)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file2)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file2)
            h5file2.close()
            ucon0, ucon1, ucon2, ucon3 = harm.calc_ucon(slice_obj, h5file, v1, v2, v3)
            ucon0 = 0
            ucon1 = 0
            ucon2 = 0
            func  = ucon3

        elif self.funcname == 'ucov0':
            v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file2)
            v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file2)
            v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file2)
            h5file2.close()
            metric = harm.get_metric(slice_obj,h5file)
            ucon0, ucon1, ucon2, ucon3 = harm.calc_ucon(slice_obj, h5file, v1, v2, v3)
            ucov0, ucov1, ucov2, ucov3 = harm.lower2(metric, ucon0, ucon1, ucon2, ucon3)
            func = ucov0

        elif self.funcname == 'temp':
            USE_SIMPLE_EOS = h5file['Header/Macros/USE_SIMPLE_EOS'][0]
            if( USE_SIMPLE_EOS ):
                print("Using ISOTHERMAL EOS")
                pressure = harm.read_3d_hdf5_slice('B1', slice_obj, h5file)
                rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
                #assume Isothermal EOS
                func = pressure / rho
            else:
                print("Using GAMMA-LAW EOS")
                gam = h5file['Header/Grid/gam'][0]
                uu  = harm.read_3d_hdf5_slice('uu', slice_obj, h5file)
                rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
                func = (gam - 1.) * uu / rho
        elif self.funcname == 'dtmin1':
            dx1 = h5file['Header/Grid/dx1'][0]
            vmax = vchar(fullname, 1)
            func = np.abs(dx1 / vmax)
            print("dtmin1 found to be {0} at coords (r,th,ph) = {1} {2} {3}".format(np.min(func), rr[func == np.min(func)], th[func == np.min(func)], ph[func == np.min(func)]))
        elif self.funcname == 'dtmin2':
            dx2 = h5file['Header/Grid/dx2'][0]
            vmax = vchar(fullname,2)
            func = np.abs(dx2 / vmax)
            print("dtmin2 found to be {0} at coords (r,th,ph) = {1} {2} {3}".format(np.min(func), rr[func == np.min(func)], th[func == np.min(func)], ph[func == np.min(func)]))
        elif self.funcname == 'dtmin3':
            dx3 = h5file['Header/Grid/dx3'][0]
            vmax = vchar(fullname,3)
            func = np.abs(dx3 / vmax)
            print("dtmin3 found to be {0} at coords (r,th,ph) = {1} {2} {3}".format(np.min(func), rr[func == np.min(func)], th[func == np.min(func)], ph[func == np.min(func)]))
            print( np.where(func == func.min()) )
        elif self.funcname == 'cs':
            #sqrt{gam p / rho}
            gam = h5file['Header/Grid/gam'][0]
            uu  = harm.read_3d_hdf5_slice( 'uu',slice_obj,h5file)
            rho = harm.read_3d_hdf5_slice('rho',slice_obj,h5file)
            p   = (gam - 1.)*uu
            #func = np.sqrt(gam) * p / rho
            func = np.sqrt(gam * p / rho)
        elif self.funcname == 'cooling':
            gam = h5file['Header/Grid/gam'][0]
            uu = harm.read_3d_hdf5_slice('uu',slice_obj,h5file)
            rho = harm.read_3d_hdf5_slice('rho',slice_obj,h5file)
            S0 = 0.01
            S = (gam - 1.)*uu / np.power(rho,gam)
            Sterm = (S - S0)/S0
            #calculate BL
            TNZ = np.zeros((np.shape(rr)))
            TNZ.fill(self.time)
            XNZ = rr * np.sin(th) * np.cos(ph)
            YNZ = rr * np.sin(th) * np.sin(ph)
            ZNZ = rr * np.cos(th)

            if( self.spinning ):
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile,usecols=(0,1,2,4,5),unpack=True) #spinning
            else:
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile,usecols=(0,4,5,6,7),unpack=True) #old metrics

            fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
            fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
            fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
            fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

            theta = self.th_slice[0,0]

            self.xbh1 = fxbh1(self.time) / np.sin(theta)
            self.ybh1 = fybh1(self.time) / np.sin(theta)
            self.xbh2 = fxbh2(self.time) / np.sin(theta)
            self.ybh2 = fybh2(self.time) / np.sin(theta)
            sep = np.sqrt( (self.xbh1 - self.xbh2) * (self.xbh1 - self.xbh2) + (self.ybh1 - self.ybh2) * (self.ybh1 - self.ybh2) )

            #Assume that q = 1 ( so m1 = m2 = 0.5 )
            rBL = rr + 1.
            rBL[rBL < 1.5 * sep] = 1.5 * sep
            r1 = np.sqrt( (XNZ - self.xbh1)*(XNZ - self.xbh1) + (YNZ - self.ybh1) * (YNZ - self.ybh1) + (ZNZ * ZNZ) ) + 0.5
            r2 = np.sqrt( (XNZ - self.xbh2)*(XNZ - self.xbh2) + (YNZ - self.ybh2)*(YNZ - self.ybh2) + (ZNZ * ZNZ) ) + 0.5
            #Reset to ISCO inside ISCO
            r1[ r1 <= 3. ] = 3.
            r2[ r2 <= 3. ] = 3.

            Tcool = 2. * np.pi * np.power( rBL, 1.5 )
            Tcool[r1 < 0.45 * sep] = 2. * np.pi * np.power( r1[r1 < 0.45 * sep], 1.5 ) / np.sqrt(0.5)
            Tcool[r2 < 0.45 * sep] = 2. * np.pi * np.power( r2[r2 < 0.45 * sep], 1.5 ) / np.sqrt(0.5)

            func = (uu/Tcool) * ( Sterm + np.abs(Sterm) ) #/ rho #divide by rho to get cooling per unit mass

        elif self.funcname == 'entropy':
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            uu  = harm.read_3d_hdf5_slice('uu', slice_obj, h5file)
            gam = h5file['Header/Grid/gam'][...]
            p = (gam - 1.) * uu
            func = p / np.power(rho,gam)

        elif self.funcname == 'plas_beta':
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            uu = harm.read_3d_hdf5_slice('uu', slice_obj, h5file)
            bsq = harm.read_3d_hdf5_slice('bsq', slice_obj, h5file)
            gam = h5file['Header/Grid/gam'][...]
            p = (gam - 1.) * uu
            func = 2. * p / bsq

        elif self.funcname == 'beta1':
            metric = (slice_obj, h5file)
            func = metric['beta1']

        elif self.funcname == 'beta2':
            metric = harm.get_metric(slice_obj, h5file)
            func = metric['beta2']

        elif self.funcname == 'beta3':
            metric = harm.get_metric(slice_obj, h5file)
            func = metric['beta3']

        elif self.funcname == 'alpha':
            metric = harm.get_metric(slice_obj, h5file)
            func = metric['alpha']

        elif self.funcname == 'ncon0':
            metric = harm.get_metric(slice_obj, h5file)
            func = 1. / metric['alpha']

        elif self.funcname == 'gcon00':
            metric = harm.get_metric(slice_obj,h5file)
            func = metric['gcon00']

        elif self.funcname == 'potential':
            transformBL = False #whether to plot wrt BL or PN coords
            scalesep = True
            metric = harm.get_metric(slice_obj,h5file)
            gcon = {
                '00':metric['gcon00'], '01':metric['gcon01'], '02':metric['gcon02'], '03':metric['gcon03'],
                '11':metric['gcon11'], '12':metric['gcon12'], '13':metric['gcon13'],
                '22':metric['gcon22'], '23':metric['gcon23'],
                '33':metric['gcon33']
            }
            if( transformBL ):
                ttNZ   = np.zeros((np.shape(rr)))
                ttNZ.fill(self.time)
                dx_dxp = harm.get_dx_dxp(slice_obj,h5file)
                metricBL = ct.metric2BL(1, self.trajfile, ttNZ, rr, th, ph, dx_dxp, metric)
                func = -0.5 * ( metric['gcov00'] + 1. )
            else:
                dx_dxp = harm.get_dx_dxp(slice_obj,h5file)
                dxc_dxs = ct.get_dxc_dxs( rr, th, ph )
                gcon = ct.transform_gcon( dx_dxp, gcon ) #Spherical PN Coords (Physical)
                if self.corotate:
                    '''
                    rewrite dx_dxp to be jacobian to corotating frame
                    dx_dxp = |   1    0 0 0 |
                             |   0    1 0 0 |
                             |   0    0 1 0 |
                             | -omega 0 0 1 |
                    '''
                    if( self.spinning ):
                        tbh_traj, omega_bin_traj = np.loadtxt(self.trajfile,usecols=(0,16),unpack=True) #spinning
                    else:
                        tbh_traj, omega_bin_traj = np.loadtxt(self.trajfile,usecols=(0,24),unpack=True) #old metrics

                    fomega_bin = interp1d(tbh_traj, omega_bin_traj, kind='linear')
                    omega_bin = fomega_bin(self.time)
                    dx_dxp['00'].fill(1.); dx_dxp['01'].fill(0.); dx_dxp['02'].fill(0.); dx_dxp['03'].fill(0.)
                    dx_dxp['10'].fill(0.); dx_dxp['11'].fill(1.); dx_dxp['12'].fill(0.); dx_dxp['13'].fill(0.)
                    dx_dxp['20'].fill(0.); dx_dxp['21'].fill(0.); dx_dxp['22'].fill(1.); dx_dxp['23'].fill(0.)
                    dx_dxp['30'].fill(-omega_bin); dx_dxp['31'].fill(0.); dx_dxp['32'].fill(0.); dx_dxp['33'].fill(1.)
                    gcon = ct.transform_gcon( dx_dxp, gcon) # Corotating Frame ( spherical coords )
                gcon = ct.transform_gcon( dxc_dxs, gcon) #Cartesian PN Coords (Physical)
                #Get gcov from gcon
                gcov    = ct.get_ginverse(gcon)
                func    = -0.5 * ( gcov['00'] + 1. )
                if( scalesep ):
                    sep = h5file['Header/Grid/initial_bbh_separation'][0]
                    func *= sep
        elif self.funcname == 'geom_alpha':
            metric = harm.get_metric(slice_obj,h5file)
            func = metric['alpha']
        elif self.funcname == 'D':
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            gam = harm.read_3d_hdf5_slice('gamma', slice_obj, h5file)
            func = rho * gam
        elif self.funcname == 'mass_flux':
            #mass flux wrt numerical coordinates
            v1 = harm.read_3d_hdf5_slice('v1',slice_obj,h5file2)
            v2 = harm.read_3d_hdf5_slice('v2',slice_obj,h5file2)
            v3 = harm.read_3d_hdf5_slice('v3',slice_obj,h5file2)
            h5file2.close()
            #uconrr, uconth, uconph = harm.calc_uconKS(slice_obj, h5file, v1, v2, v3) #assumes diagonal metric to get velocities
            ucon0, uconrrp, uconthp, uconphp = harm.calc_ucon(slice_obj,h5file,v1,v2,v3) #get ucon wrt numerical coordinates

            rho  = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            gdet = harm.read_3d_hdf5_slice('gdet', slice_obj, h5file)

            func = uconrrp * rho * gdet
        elif self.funcname == 'dg_dxp1' or self.funcname == 'dg_dxp2' or self.funcname == 'dg_dxp3':
            gdet = harm.read_3d_hdf5_slice('gdet', slice_obj, h5file)
            dxp1 = h5file['Header/Grid/dx1'][0]
            dxp2 = h5file['Header/Grid/dx2'][0]
            dxp3 = h5file['Header/Grid/dx3'][0]
            if EQUATORIAL_RUN:
                dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
                func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
            else:
                dg_dxp1, dg_dxp2, dg_dxp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
            if self.funcname == 'dg_dxp1':
                if EQUATORIAL_RUN:
                    func[:,0,:] = dg_dxp1[:,:]
                else:
                    func = dg_dxp1
            elif self.funcname == 'dg_dxp2':
                func = dg_dxp2
            elif self.funcname == 'dg_dxp3':
                if EQUATORIAL_RUN:
                    func[:,0,:] = dg_dxp3[:,:]
                else:
                    func = dg_dxp3
        elif self.funcname == 'd2g_dxp11' or self.funcname == 'd2g_dxp12' or self.funcname == 'd2g_dxp13':
            gdet = harm.read_3d_hdf5_slice('gdet',slice_obj,h5file)
            dxp1 = h5file['Header/Grid/dx1'][0]
            dxp2 = h5file['Header/Grid/dx2'][0]
            dxp3 = h5file['Header/Grid/dx3'][0]
            if EQUATORIAL_RUN:
                dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
                d2g_dxp11, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp3)
                func = np.zeros((gdet.shape[0],gdet.shape[1],gdet.shape[2]))
                if self.funcname == 'd2g_dxp11':
                    func[:,0,:] = d2g_dxp11[:,:]
                elif self.funcname == 'd2g_dxp13':
                    func[:,0,:] = d2g_dxp13[:,:]
            else:
                dg_dxp1, dg_dxp2, dg_dxp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
                d2g_dxp11, d2g_dxp12, d2g_dxp13 = np.gradient(dg_dxp1, dxp1, dxp2, dxp3)
                if self.funcname == 'd2g_dxp11':
                    func = d2g_dxp11
                elif self.funcname == 'd2g_dxp12':
                    func = d2g_dxp12
                elif self.funcname == 'd2g_dxp13':
                    func = d2g_dxp13
        elif self.funcname == 'd2g_dxp21' or self.funcname == 'd2g_dxp22' or self.funcname == 'd2g_dxp23':
            gdet = harm.read_3d_hdf5_slice('gdet',slice_obj,h5file)
            dxp1 = h5file['Header/Grid/dx1'][0]
            dxp2 = h5file['Header/Grid/dx2'][0]
            dxp3 = h5file['Header/Grid/dx3'][0]
            dg_dxp1, dg_dxp2, dg_dxp3 = np.gradient(gdet, dxp1,dxp2, dxp3)
            d2g_dxp21, d2g_dxp22, d2g_dxp23 = np.gradient(dg_dxp2, dxp1, dxp2, dxp3)
            if self.funcname == 'd2g_dxp21':
                func = d2g_dxp21
            elif self.funcname == 'd2g_dxp22':
                func = d2g_dxp22
            elif self.funcname == 'd2g_dxp23':
                func = d2g_dxp23
        elif self.funcname == 'd2g_dxp31' or self.funcname == 'd2g_dxp32' or self.funcname == 'd2g_dxp33':
            gdet = harm.read_3d_hdf5_slice('gdet',slice_obj,h5file)
            dxp1 = h5file['Header/Grid/dx1'][0]
            dxp2 = h5file['Header/Grid/dx2'][0]
            dxp3 = h5file['Header/Grid/dx3'][0]
            if EQUATORIAL_RUN:
                dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:], dxp1,dxp3)
                d2g_dxp31, d2g_dxp33 = np.gradient(dg_dxp3, dxp1,dxp3)
                func = np.zeros((gdet.shape[0], gdet.shape[1], gdet.shape[2]))
                if self.funcname == 'd2g_dxp31':
                    func[:,0,:] = d2g_dxp31[:,:]
                elif self.funcname == 'd2g_dxp33':
                    func[:,0,:] = d2g_dxp33[:,:]
            else:
                dg_dxp1, dg_dxp2, dg_dxp3 = np.gradient(gdet, dxp1, dxp2, dxp3)
                d2g_dxp31, d2g_dxp32, d2g_dxp33 = np.gradient(dg_dxp3, dxp1, dxp2, dxp3)
                if self.funcname == 'd2g_dxp31':
                    func = d2g_dxp31
                elif self.funcname == 'd2g_dxp32':
                    func = d2g_dxp32
                elif self.funcname == 'd2g_dxp33':
                    func = d2g_dxp33
        elif self.funcname == 'gradg_rat_x1' or self.funcname == 'gradg_rat_x2' or self.funcname == 'gradg_rat_x3':
            gdet = harm.read_3d_hdf5_slice('gdet',slice_obj,h5file)
            dxp1 = h5file['Header/Grid/dx1'][0]
            dxp2 = h5file['Header/Grid/dx2'][0]
            dxp3 = h5file['Header/Grid/dx3'][0]
            if EQUATORIAL_RUN:
                dg_dxp1, dg_dxp3 = np.gradient(gdet[:,0,:],dxp1,dxp3)
                d2g_dxp11, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp3)
                d2g_dxp31, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp3)
                func = np.zeros((gdet.shape[0], gdet.shape[1], gdet.shape[2]))
                if self.funcname == 'gradg_rat_x1':
                    func[:,0,:] = dxp1 * d2g_dxp11[:,:] / dg_dxp1[:,:]
                elif self.funcname == 'gradg_rat_x3':
                    func[:,0,:] = dxp3 * d2g_dxp33[:,:] / dg_dxp3[:,:]
            else:
                dg_dxp1, dg_dxp2, dg_dxp3 = np.gradient(gdet,dxp1,dxp2,dxp3)
                d2g_dxp11, d2g_dxp12, d2g_dxp13 = np.gradient(dg_dxp1,dxp1,dxp2,dxp3)
                d2g_dxp21, d2g_dxp22, d2g_dxp32 = np.gradient(dg_dxp2,dxp1,dxp2,dxp3)
                d2g_dxp31, d2g_dxp32, d2g_dxp33 = np.gradient(dg_dxp3,dxp1,dxp2,dxp3)
                if self.funcname == 'gradg_rat_x1':
                    func = dxp1 * d2g_dxp11 / dg_dxp1
                elif self.funcname == 'gradg_rat_x2':
                    func = dxp2 * d2g_dxp22 / dg_dxp2
                elif self.funcname == 'gradg_rat_x3':
                    func = dxp3 * d2g_dxp33 / dg_dxp3
        elif self.funcname == 'dr':
            func = np.diff(harm.read_3d_hdf5_slice('x1', slice_obj, h5file), axis=0)
            zerrarr = np.zeros((1,np.shape(func)[1],np.shape(func)[2]))
            zerrarr[0,:,:] = func[0,:,:]
            func = np.append(func, zerrarr, axis=0)
        elif self.funcname == 'dth':
            x1 = harm.read_3d_hdf5_slice('x1', slice_obj, h5file)
            x2 = harm.read_3d_hdf5_slice('x2', slice_obj, h5file)
            x2 = np.diff(x2, axis=1)
            zerrarr = np.zeros((np.shape(x2)[0], 1, np.shape(x2)[2]))
            zerrarr[:,0,:] = x2[:,0,:]
            x2 = np.append(x2, zerrarr, axis=1)
            func = x1 * x2
        elif self.funcname == 'dph':
            x1 = harm.read_3d_hdf5_slice('x1', slice_obj, h5file)
            x3 = harm.read_3d_hdf5_slice('x3', slice_obj, h5file)
            x3 = np.diff(x3, axis=2)
            print("need to fix this option")
            zerrarr = np.zeros((np.shape(x3)[0],np.shape(x3)[1],1))
            zerrarr[:,:,0] = x3[:,:,0]
            x3 = np.append(x3, zerrarr, axis=2)
            func = x1 * x3
        elif self.funcname == 'testclean':
            print("Testing Cleaner Status target is 1.e-3")
            print("READ GFUNCS")
            dx1 = h5file['Header/Grid/dx1'][0]
            dx2 = h5file['Header/Grid/dx2'][0]
            dx3 = h5file['Header/Grid/dx3'][0]
            divb = harm.read_3d_hdf5_slice('divb', slice_obj, h5file)
            B1 = harm.read_3d_hdf5_slice('B1', slice_obj, h5file)
            B2 = harm.read_3d_hdf5_slice('B2', slice_obj, h5file)
            B3 = harm.read_3d_hdf5_slice('B3', slice_obj, h5file)
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file)
            #Try reading gcov to do better on memory
            gcov00 = harm.read_3d_hdf5_slice('gcov00', slice_obj, h5file)
            gcov01 = harm.read_3d_hdf5_slice('gcov01', slice_obj, h5file)
            gcov02 = harm.read_3d_hdf5_slice('gcov02', slice_obj, h5file)
            gcov03 = harm.read_3d_hdf5_slice('gcov03', slice_obj, h5file)
            gcov11 = harm.read_3d_hdf5_slice('gcov11', slice_obj, h5file)
            gcov12 = harm.read_3d_hdf5_slice('gcov12', slice_obj, h5file)
            gcov13 = harm.read_3d_hdf5_slice('gcov13', slice_obj, h5file)
            gcov22 = harm.read_3d_hdf5_slice('gcov22', slice_obj, h5file)
            gcov23 = harm.read_3d_hdf5_slice('gcov23', slice_obj, h5file)
            gcov33 = harm.read_3d_hdf5_slice('gcov33', slice_obj, h5file)

            #print("get metric")
            #metric = harm.get_metric(slice_obj,h5file)
            #Get min dx^i
            print("Get min dx^i")
            dxmin = min(min(dx1,dx3),min(dx2,dx3))
            #print(dx1,dx2,dx3,dxmin)
            #Get bsq
            print("calc_ucon")
            ucon0, ucon1, ucon2, ucon3 = harm.calc_ucon(slice_obj, h5file, v1, v2, v3)
            print("lower ucon")
            #ucov0, ucov1, ucov2, ucov3 = lower2(metric,ucon0,ucon1,ucon2,ucon3)
            ucov0, ucov1, ucov2, ucov3 = harm.lower(gcov00,gcov01,gcov02,gcov03,gcov11,gcov12,gcov13,gcov22,gcov23,gcov33,ucon0,ucon1,ucon2,ucon3)
            print("bcon_calc")
            bcon0, bcon1, bcon2, bcon3 = harm.bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
            print("lower bcon")
            #bcov0, bcov1, bcov2, bcov3 = lower2(metric,bcon0,bcon1,bcon2,bcon3)
            bcov0, bcov1, bcov2, bcov3 = harm.lower(gcov00,gcov01,gcov02,gcov03,gcov11,gcov12,gcov13,gcov22,gcov23,gcov33,bcon0,bcon1,bcon2,bcon3)
            print("bsq calc")
            bsq = bcov0*bcon0 + bcov1*bcon1 + bcov2*bcon2 + bcov3*bcon3
            #avoid bsq == 0
            #print(bsq)
            bsq[bsq <= 1.e-10] = divb[bsq <= 1.e-10]
            #print(bsq)
            print("return func")
            #Use cleaning target
            func = divb * dxmin / np.sqrt(bsq)
            print("Cleaned cells ", np.shape(func[func <= 1.e-3]), "Total Cells ",np.shape(func))
            #rewrite when divb is just tiny because no Bfield
            #func[divb <= 1.e-10] = 1.e-40
            #func[bsq <= 1.e-10]  = 1.e-40
            #print(func)
        elif self.funcname == 'rho_gamma':
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            gamma = harm.read_3d_hdf5_slice('gamma', slice_obj, h5file)
            func = rho * gamma
        elif self.funcname == 'bsq_o_rho':
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            bsq = harm.read_3d_hdf5_slice('bsq', slice_obj, h5file)
            func = bsq / rho
        elif self.funcname == 'total_flux_r':
            func = flx.total_flux_r(slice_obj, h5file, with_gdet=with_gdet)

        elif self.funcname == 'poynting_flux_r':
            func = flx.poynting_flux_r(slice_obj, h5file, with_gdet=with_gdet)

        elif self.funcname == 'hydro_flux_r':
            func = flx.hydro_flux_r(slice_obj, h5file, with_gdet)

        elif self.funcname == 'mass_flux_r':
            func = flx.mass_flux_r(slice_obj, h5file, with_gdet)

        elif self.funcname == 'total_flux_z':
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file)
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            uu = harm.read_3d_hdf5_slice('uu', slice_obj, h5file)
            B1 = harm.read_3d_hdf5_slice('B1', slice_obj, h5file)
            B2 = harm.read_3d_hdf5_slice('B2', slice_obj, h5file)
            B3 = harm.read_3d_hdf5_slice('B3', slice_obj, h5file)
            bsq = harm.read_3d_hdf5_slice('bsq', slice_obj, h5file)
            ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
            bcon0, bcon1, bcon2, bcon3 = harm.bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
            bcov0, bcov1, bcov2, bcov3 = harm.lower2(metric,bcon0,bcon1,bcon2,bcon3)

            gam = h5file['Header/Grid/gam'][...]
            energy = -(rho + gam * uu) * ucov0

            bsq = bsq * ucov0

            #  EM flux = - T^z_t
            em_flux_1t = - ucon1 * bsq + bcon1 * bcov0
            em_flux_2t = - ucon2 * bsq + bcon2 * bcov0
            em_flux_3t = - ucon3 * bsq + bcon3 * bcov0
            hyd_flux_1t = energy * ucon1
            hyd_flux_2t = energy * ucon2
            hyd_flux_3t = energy * ucon3
            mass_flux_1 = rho * ucon1
            mass_flux_2 = rho * ucon2
            mass_flux_3 = rho * ucon3
            total_flux_1t = em_flux_1t - mass_flux_1
            total_flux_2t = em_flux_2t - mass_flux_2
            total_flux_3t = em_flux_3t - mass_flux_3

            dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11', slice_obj, h5file)
            dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12', slice_obj, h5file)
            dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13', slice_obj, h5file)
            dx_dxp21 = harm.read_3d_hdf5_slice('dx_dxp21', slice_obj, h5file)
            dx_dxp22 = harm.read_3d_hdf5_slice('dx_dxp22', slice_obj, h5file)
            dx_dxp23 = harm.read_3d_hdf5_slice('dx_dxp23', slice_obj, h5file)

            func_rr = total_flux_1t * dx_dxp11  + total_flux_2t * dx_dxp12  + total_flux_3t * dx_dxp13
            func_th = total_flux_1t * dx_dxp21  + total_flux_2t * dx_dxp22  + total_flux_3t * dx_dxp23
            # x^z =  x^r dz/dr  + x^th dz/dth =  x^r cos(th)  - x^th * r * sin(th)
            func = func_rr * np.cos(self.th_slice) - func_th * np.sin(self.th_slice) * self.rr_slice
            if with_gdet :
                func *= metric['gdet']
            # TODO: fluxes.py simplifies this
            # func_rr = flx.total_flux_r(slice_obj, h5file, with_gdet=with_gdet)
            # func_th = flx.total_flux_th(slice_obj, h5file, with_gdet=with_gdet)

        elif self.funcname == 'poynting_flux_z':
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file)
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            B1 = harm.read_3d_hdf5_slice('B1', slice_obj, h5file)
            B2 = harm.read_3d_hdf5_slice('B2', slice_obj, h5file)
            B3 = harm.read_3d_hdf5_slice('B3', slice_obj, h5file)
            bsq = harm.read_3d_hdf5_slice('bsq', slice_obj, h5file)
            ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)
            bcon0, bcon1, bcon2, bcon3 = harm.bcon_calc(B1,B2,B3,ucon0,ucon1,ucon2,ucon3,ucov0,ucov1,ucov2,ucov3)
            bcov0, bcov1, bcov2, bcov3 = harm.lower2(metric,bcon0,bcon1,bcon2,bcon3)

            bsq = bsq * ucov0

            #  EM flux =  - T^r_t
            em_flux_1t = - ucon1 * bsq  + bcon1 * bcov0
            em_flux_2t = - ucon2 * bsq  + bcon2 * bcov0
            em_flux_3t = - ucon3 * bsq  + bcon3 * bcov0

            dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
            dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
            dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
            dx_dxp21 = harm.read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
            dx_dxp22 = harm.read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
            dx_dxp23 = harm.read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)

            func_rr = em_flux_1t * dx_dxp11  + em_flux_2t * dx_dxp12  + em_flux_3t * dx_dxp13
            func_th = em_flux_1t * dx_dxp21  + em_flux_2t * dx_dxp22  + em_flux_3t * dx_dxp23
            # x^z =  x^r dz/dr  + x^th dz/dth =  x^r cos(th)  - x^th * r * sin(th)
            func = func_rr * np.cos(self.th_slice) - func_th * np.sin(self.th_slice) * self.rr_slice
            if with_gdet :
                func *= metric['gdet']

        elif self.funcname == 'hydro_flux_z':
            v1 = harm.read_3d_hdf5_slice('v1', slice_obj, h5file)
            v2 = harm.read_3d_hdf5_slice('v2', slice_obj, h5file)
            v3 = harm.read_3d_hdf5_slice('v3', slice_obj, h5file)
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file)
            uu = harm.read_3d_hdf5_slice( 'uu', slice_obj, h5file)
            ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)

            gam = h5file['Header/Grid/gam'][0]
            energy = -(rho + gam * uu) * ucov0

            #  flux =  - T^r_t - rho u^r
            hyd_flux_1t = (energy - rho) * ucon1
            hyd_flux_2t = (energy - rho) * ucon2
            hyd_flux_3t = (energy - rho) * ucon3

            dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
            dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
            dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
            dx_dxp21 = harm.read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
            dx_dxp22 = harm.read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
            dx_dxp23 = harm.read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)

            func_rr = hyd_flux_1t * dx_dxp11  + hyd_flux_2t * dx_dxp12  + hyd_flux_3t * dx_dxp13
            func_th = hyd_flux_1t * dx_dxp21  + hyd_flux_2t * dx_dxp22  + hyd_flux_3t * dx_dxp23
            # x^z =  x^r dz/dr  + x^th dz/dth =  x^r cos(th)  - x^th * r * sin(th)
            func = func_rr * np.cos(self.th_slice) - func_th * np.sin(self.th_slice) * self.rr_slice
            if with_gdet :
                func *= metric['gdet']

        elif self.funcname == 'mass_flux_z':
            v1     = harm.read_3d_hdf5_slice('v1',slice_obj,h5file)
            v2     = harm.read_3d_hdf5_slice('v2',slice_obj,h5file)
            v3     = harm.read_3d_hdf5_slice('v3',slice_obj,h5file)
            rho    = harm.read_3d_hdf5_slice('rho',slice_obj,h5file)
            ucon0, ucon1, ucon2, ucon3, ucov0, ucov1, ucov2, ucov3, metric = harm.calc_ucon_ucov(slice_obj, h5file, v1, v2, v3)

            #  mass flux =  rho u^r
            mass_flux_1 =  rho * ucon1
            mass_flux_2 =  rho * ucon2
            mass_flux_3 =  rho * ucon3

            dx_dxp11 = harm.read_3d_hdf5_slice('dx_dxp11',slice_obj,h5file)
            dx_dxp12 = harm.read_3d_hdf5_slice('dx_dxp12',slice_obj,h5file)
            dx_dxp13 = harm.read_3d_hdf5_slice('dx_dxp13',slice_obj,h5file)
            dx_dxp21 = harm.read_3d_hdf5_slice('dx_dxp21',slice_obj,h5file)
            dx_dxp22 = harm.read_3d_hdf5_slice('dx_dxp22',slice_obj,h5file)
            dx_dxp23 = harm.read_3d_hdf5_slice('dx_dxp23',slice_obj,h5file)

            func_rr = mass_flux_1 * dx_dxp11  + mass_flux_2 * dx_dxp12  + mass_flux_3 * dx_dxp13
            func_th = mass_flux_1 * dx_dxp21  + mass_flux_2 * dx_dxp22  + mass_flux_3 * dx_dxp23
            # x^z =  x^r dz/dr  + x^th dz/dth =  x^r cos(th)  - x^th * r * sin(th)
            func = func_rr * np.cos(self.th_slice) - func_th * np.sin(self.th_slice) * self.rr_slice
            if with_gdet :
                func *= metric['gdet']

        elif self.funcname == 'coolfunc_rho_mask':
            rho = harm.read_3d_hdf5_slice('rho', slice_obj, h5file2)
            coolfunc = harm.read_3d_hdf5_slice('coolfunc', slice_obj,h5file2)

            if( rho_cutout_value is None ):
                rho_cutout_value = 1e-4

            func = coolfunc
            func[ rho < rho_cutout_value ] = 1e-30

        else:
            if use_stat2:
                func = h5stat2[self.funcname]
            else:
                if take_ratio:
                    func1 = harm.read_3d_hdf5_slice(self.funcname[0], slice_obj, h5file)
                    func2 = harm.read_3d_hdf5_slice(self.funcname[1], slice_obj, h5file)
                    func2[func2 == 0] = 1.e-40
                    func = func1 / func2
                    self.funcname = self.funcname[0] + '/' + self.funcname[1]
                elif relerror:
                    func1 = harm.read_3d_hdf5_slice(self.funcname[0],slice_obj,h5file)
                    func2 = harm.read_3d_hdf5_slice(self.funcname[1],slice_obj,h5file)
                    func = np.abs(func1 - func2) / func2
                    func[func1 == 0] = 1.e-40
                    self.funcname = 'relative error (' + self.funcname[0] + ')'
                elif compare_runs:
                    if( compare_dir is None ):
                        h5file2n = os.getcwd() + '/dumps/KDHARM0.' + self.timeid + '.h5'
                    else:
                        h5file2n = compare_dir + '/dumps/KDHARM0.' + self.timeid + '.h5'
                    h5file2.close()
                    h5file2 = h5py.File(h5file2n, 'r')
                    func1 = harm.read_3d_hdf5_slice(self.funcname, slice_obj, h5file)
                    func2 = harm.read_3d_hdf5_slice(self.funcname, slice_obj, h5file2)
                    #mask1 = harm.read_3d_hdf5_slice('evmask',slice_obj,h5file)
                    #mask2 = harm.read_3d_hdf5_slice('evmask',slice_obj,h5file2)
                    #func1[mask1 != 2] = 1.e-40
                    #func2[mask2 != 2] = 1.e-40
                    #func = np.abs( func1 - func2) / np.abs(func1)
                    func1[func1 == 0] += 1.e-40
                    func2[func2 == 0] += 1.e-40
                    func = np.abs( 2. * ( func1 - func2 ) / ( func1 + func2 ) )
                    #func[mask1 != 2] = 0
                    #func[func1 == 0] = 1.e-40
                    self.funcname = 'relative-error-' + self.funcname
                    h5file2.close()
                else:
                    func = harm.read_3d_hdf5_slice(self.funcname, slice_obj, h5file2)
                    #mask = harm.read_3d_hdf5_slice('evmask',slice_obj,h5file)
                    #func[mask != 2] = 1.e20

        #EXTEND_CENTRAL_CUTOUT
        if(EXTEND_CENTRAL_CUTOUT):
            mask = harm.read_3d_hdf5_slice('evmask', slice_obj, h5file)
            func[mask == 3] = 0. #set the values to 0 in the mask

        #potential overplot
        if self.plot_potential:
            metric = harm.get_metric(slice_obj,h5file)
            gcon = { '00':metric['gcon00'], '01':metric['gcon01'], '02':metric['gcon02'], '03':metric['gcon03'],
                    '11':metric['gcon11'], '12':metric['gcon12'], '13':metric['gcon13'],
                    '22':metric['gcon22'], '23':metric['gcon23'],
                    '33':metric['gcon33'] }
            dx_dxp  = harm.get_dx_dxp(slice_obj,h5file)
            dxc_dxs = ct.get_dxc_dxs( rr, th, ph )
            gcon    = ct.transform_gcon( dx_dxp, gcon ) #Spherical PN Coords (Physical)
            if self.corotate:
                '''
                rewrite dx_dxp to be jacobian to corotating frame
                dx_dxp = |   1    0 0 0 |
                         |   0    1 0 0 |
                         |   0    0 1 0 |
                         | -omega 0 0 1 |
                '''
                if( self.spinning ):
                    tbh_traj, omega_bin_traj = np.loadtxt(self.trajfile, usecols=(0,16),unpack=True) #spinning
                else:
                    tbh_traj, omega_bin_traj = np.loadtxt(self.trajfile, usecols=(0,24),unpack=True) #old metrics

                fomega_bin = interp1d(tbh_traj, omega_bin_traj, kind='linear')
                omega_bin = fomega_bin(self.time)
                dx_dxp['00'].fill(1.); dx_dxp['01'].fill(0.)
                dx_dxp['02'].fill(0.); dx_dxp['03'].fill(0.)
                dx_dxp['10'].fill(0.); dx_dxp['11'].fill(1.)
                dx_dxp['12'].fill(0.); dx_dxp['13'].fill(0.)
                dx_dxp['20'].fill(0.); dx_dxp['21'].fill(0.)
                dx_dxp['22'].fill(1.); dx_dxp['23'].fill(0.)
                dx_dxp['30'].fill(-omega_bin)
                dx_dxp['31'].fill(0.); dx_dxp['32'].fill(0.)
                dx_dxp['33'].fill(1.)
                gcon = ct.transform_gcon( dx_dxp,  gcon) # Corotating Frame ( spherical coords )
            gcon = ct.transform_gcon( dxc_dxs, gcon) #Cartesian PN Coords (Physical)
            #Get gcov from gcon
            gcov = ct.get_ginverse(gcon)
            pot_func = -0.5 * ( gcov['00'] + 1. )
            # function to plot
            self.pot_slice = pot_func

        #dennis modifying
        #func /= np.max(func[rr < 100.])

        # function to plot
        self.func_slice = func

        # Get BH masses if plotting trajs before closing file
        if self.plot_bhs_traj:
            self.m_bh1 = h5file['Header/Grid/m_bh1'][0]
            self.m_bh2 = h5file['Header/Grid/m_bh2'][0]
            if self.m_bh1 != 0.5:
                print('Unequal Masses')
                print('m_bh1', self.m_bh1)
                print('m_bh2', self.m_bh2)

        if self.fieldlines is not None:
            self.aphi = self.fieldlines[1]
            minaphi = self.aphi.min()
            if (minaphi < 0.):
                self.aphi -= minaphi

        if (fillaxis and self.iph is not None and not self.cart_coords):
            print("rr slice new = ", self.rr_slice.shape)
            tmpnew = np.ndarray((self.idim, self.jdim+2))
            print("tmpnew slice new = ", tmpnew.shape)
            print("idim jdim =  ", self.idim, self.jdim)

            tmpnew[:,0] = self.rr_slice[:,0]
            tmpnew[:,1:self.jdim+1] = self.rr_slice[:,:]
            tmpnew[:,self.jdim+1] = self.rr_slice[:,self.jdim-1]
            self.rr_slice = tmpnew

            tmpnew = np.ndarray((self.idim, self.jdim+2))
            tmpnew[:,0] = 0.
            tmpnew[:,1:self.jdim+1] = self.th_slice[:,:]
            tmpnew[:,self.jdim+1] = np.pi
            self.th_slice = tmpnew

            tmpnew = np.ndarray((self.idim,self.jdim+2))
            tmpnew[:,0] = self.ph_slice[:,0]
            tmpnew[:,1:self.jdim+1] = self.ph_slice[:,:]
            tmpnew[:,self.jdim+1] = self.ph_slice[:,self.jdim-1]
            self.ph_slice = tmpnew

            tmpnew = np.ndarray((self.idim,self.jdim+2))
            tmpnew[:,0] = self.func_slice[:,0]
            tmpnew[:,1:self.jdim+1] = self.func_slice[:,:]
            tmpnew[:,self.jdim+1] = self.func_slice[:,self.jdim-1]
            self.func_slice = tmpnew

            if (self.fieldlines is not None):
                tmpnew = np.ndarray((self.idim, self.jdim+2))
                # tmpnew[:,0] = self.aphi[:,0]
                tmpnew[:,0] = 0.
                tmpnew[:,1:self.jdim+1] = self.aphi[:,:]
                # tmpnew[:,self.jdim+1] = self.aphi[:,self.jdim-1]
                tmpnew[:,self.jdim+1] = 0.
                self.aphi = tmpnew
                print("min max of aphi slice = ", np.min(self.aphi),np.max(self.aphi))

            print("min max of r    slice = ", np.min(self.rr_slice), np.max(self.rr_slice))
            print("min max of th   slice = ", np.min(self.th_slice), np.max(self.th_slice))
            print("min max of f    slice = ", np.min(self.func_slice), np.max(self.func_slice))

        if (fillwedge and self.ith is not None and not self.cart_coords and not use_spher_coord):
            self.rr_slice = np.append(self.rr_slice, np.array([self.rr_slice[:,0]]).T, axis=1)
            self.ph_slice = np.append(self.ph_slice, np.array([self.ph_slice[:,0]]).T, axis=1)
            self.th_slice = np.append(self.th_slice, np.array([self.th_slice[:,0]]).T, axis=1)
            self.func_slice = np.append(self.func_slice, np.array([self.func_slice[:,0]]).T, axis=1)
            if self.plot_potential:
                self.pot_slice = np.append(self.pot_slice, np.array([self.pot_slice[:,0]]).T, axis=1)

        if (fillcutout and self.ith is not None and not self.cart_coords and not use_spher_coord):
            zerarr = np.zeros((1, self.rr_slice.shape[1]))
            phval = np.linspace(0,2.*np.pi, self.rr_slice.shape[1])
            self.rr_slice = np.append(self.rr_slice, zerarr, axis=0)
            zerarr[0,:] = phval[:]
            self.ph_slice = np.append(self.ph_slice, zerarr, axis=0)
            self.th_slice = np.append(self.th_slice, zerarr, axis=0)
            zerarr[0,:] = np.average(self.func_slice[0,:])
            #print(zerarr)
            self.func_slice = np.append(self.func_slice, zerarr,axis=0)
            #roll so index is 0 of func_slice
            self.rr_slice = np.roll(self.rr_slice, 1, axis=0)
            self.ph_slice = np.roll(self.ph_slice, 1, axis=0)
            self.th_slice = np.roll(self.th_slice, 1, axis=0)
            self.func_slice = np.roll(self.func_slice, 1, axis=0)
            if self.plot_potential:
                zerarr[0,:] = np.average(self.pot_slice[0,:])
                self.pot_slice = np.append(self.pot_slice, zerarr, axis=0)
                self.pot_slice = np.roll(self.pot_slice, 1, axis=0)

        #Test equator first
        print("rr shape = ", np.shape(self.rr_slice))
        print("th shape = ", np.shape(self.th_slice))
        print("ph shape = ", np.shape(self.ph_slice))

        if self.cart_coords:
            self.xx_slice = self.rr_slice
            self.yy_slice = self.th_slice
        else:
            if self.ith  == int(self.jdim/2):
                if use_spher_coord:
                    self.xx_slice = self.rr_slice
                    self.yy_slice = self.ph_slice
                else :
                    self.xx_slice = self.rr_slice * np.cos(self.ph_slice)   #*np.sin(th_slice)
                    self.yy_slice = self.rr_slice * np.sin(self.ph_slice)   #*np.sin(th_slice)
            elif self.ith is not None:
                if use_spher_coord:
                    self.xx_slice = self.rr_slice
                    self.yy_slice = self.ph_slice
                else :
                    self.xx_slice = self.rr_slice * np.cos(self.ph_slice) * np.sin(self.th_slice)
                    self.yy_slice = self.rr_slice * np.sin(self.ph_slice) * np.sin(self.th_slice)
            else:
                if self.iph is None:
                    self.xx_slice = self.th_slice
                    self.yy_slice = self.ph_slice
                else:
                    if use_spher_coord:
                        self.xx_slice = self.rr_slice
                        self.yy_slice = self.th_slice
                    else:
                        self.xx_slice = self.rr_slice * np.sin(self.th_slice) * np.cos(self.ph_slice)
                        self.yy_slice = self.rr_slice * np.sin(self.th_slice) * np.sin(self.ph_slice)
                        self.xx_slice = np.sqrt(self.xx_slice * self.xx_slice + self.yy_slice * self.yy_slice) #really r
                        self.yy_slice = self.rr_slice * np.cos(self.th_slice) #really z
            #dennis hack here for cartesian coords (long-box testing)
            #xx_slice = rr_slice
            #yy_slice = th_slice

        if self.plot_bhs_traj:
            if self.spinning:
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile,usecols=(0,1,2,4,5),unpack=True) #spinning
            else:
                tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile,usecols=(0,4,5,6,7),unpack=True) #old metrics

            self.fxbh1 = interp1d(tbh_traj, xbh1_traj, kind='linear')
            self.fybh1 = interp1d(tbh_traj, ybh1_traj, kind='linear')
            self.fxbh2 = interp1d(tbh_traj, xbh2_traj, kind='linear')
            self.fybh2 = interp1d(tbh_traj, ybh2_traj, kind='linear')

            self.zbh1 = 0.
            self.zbh2 = 0.

            self.xbh1 = self.fxbh1(self.time+0.1)
            self.ybh1 = self.fybh1(self.time+0.1)
            self.xbh2 = self.fxbh2(self.time+0.1)
            self.ybh2 = self.fybh2(self.time+0.1)

            if self.ith != int(self.jdim / 2) and self.ith is not None:
                theta = self.th_slice[0,0]
                self.xbh1 /= np.sin(theta)
                self.ybh1 /= np.sin(theta)
                self.xbh2 /= np.sin(theta)
                self.ybh2 /= np.sin(theta)

            #print('corotate ?', self.corotate)
            if self.corotate:
                phase = np.arctan2(self.ybh1, self.xbh1)
                #renormalize phase s.t phase \in (0,2pi)
                if phase < 0.:
                    phase = 2. * np.pi - np.abs(phase)
                print('adjusting phase by ', phase * 180. / np.pi, ' degrees')
                #we now need to subtract this angle off from all angle arrays
                self.ph_slice = self.ph_slice - phase
                print(" phase = ", phase)
                print("shape( rr_slice ) = " , np.shape(self.rr_slice))
                print("shape( th_slice ) = " , np.shape(self.th_slice))
                print("shape( ph_slice ) = " , np.shape(self.ph_slice))

                #reset the x,y slices
                if self.ith  == int(self.jdim / 2):
                    self.xx_slice = self.rr_slice * np.cos(self.ph_slice) #*np.sin(th_slice)
                    self.yy_slice = self.rr_slice * np.sin(self.ph_slice) #*np.sin(th_slice)
                elif self.ith is not None:
                    self.xx_slice = self.rr_slice * np.cos(self.ph_slice) * np.sin(self.th_slice)
                    self.yy_slice = self.rr_slice * np.sin(self.ph_slice) * np.sin(self.th_slice)
                else:
                    if self.iph is None:
                        self.xx_slice = self.th_slice
                        self.yy_slice = self.ph_slice
                    else :
                        self.xx_slice = self.rr_slice * np.sin(self.th_slice) * np.cos(self.ph_slice)
                        self.xx_slice = np.abs(self.xx_slice)
                        self.yy_slice = self.rr_slice * np.cos(self.th_slice)
                #move BHs back to x axis according to phase
                self.xbh1 = np.sqrt(self.xbh1 * self.xbh1 + self.ybh1 * self.ybh1 + self.zbh1 * self.zbh1)
                self.ybh1 = 0.
                self.xbh2 = -np.sqrt(self.xbh2 * self.xbh2 + self.ybh2 * self.ybh2 + self.zbh2 * self.zbh2)
                self.ybh2 = 0.

            if plotcentre is None:
                pass
            elif plotcentre == 'BH1':
                self.xmin = self.xbh1 - self.rmax
                self.xmax = self.xbh1 + self.rmax
                self.ymin = self.ybh1 - self.rmax
                self.ymax = self.ybh1 + self.rmax
                self.zmin = self.zbh1 - self.rmax
                self.zmax = self.zbh1 + self.rmax
            elif plotcentre == 'BH2':
                self.xmin = self.xbh2 - self.rmax
                self.xmax = self.xbh2 + self.rmax
                self.ymin = self.ybh2 - self.rmax
                self.ymax = self.ybh2 + self.rmax
                self.zmin = self.zbh2 - self.rmax
                self.zmax = self.zbh2 + self.rmax
            else:
                sys.exit('invalid plotcentre option')

        if normalize_func is True:
            self.func_slice /= self.func_slice.max()


        if self.dolog:
            self.func_slice[self.func_slice == 0] += 1.e-40       # so that there is no log of zero...
            self.func_slice = np.log10(np.abs(self.func_slice))

        if move_patch:
            self.xx_center = h5file['Header/Grid/centx_cart1'][0]
            self.yy_center = h5file['Header/Grid/centx_cart2'][0]
            self.zz_center = h5file['Header/Grid/centx_cart3'][0]
            print("moving patch to new center = ", self.xx_center, self.yy_center, self.zz_center)
            self.xx_slice += self.xx_center
            self.yy_slice += self.yy_center
        else :
            self.xx_center = 0.
            self.yy_center = 0.
            self.zz_center = 0.

        if flip_horizontal:
            self.xx_slice *= -1.
            # xmin_orig = xmin
            # xmax_orig = xmax
            # if xmax is not None :
            #     xmax = -xmin_orig
            # if xmin is not None :
            #     xmin *= -xmax_orig

        h5file.close()

        return


    ############################################################################
    def get_slice(self, get_time=False):
        """
        Returns
        -------
        func_slice : float, array
            Array with slice of data.
        """

        if self.use_num_coord:
            if get_time:
                return self.time, self.func_slice
            else:
                return self.func_slice
        else:
            if get_time:
                return self.time, self.xx_slice, self.yy_slice, self.func_slice
            else:
                return self.xx_slice, self.yy_slice, self.func_slice


    ############################################################################
    def mask_slice(self, mask_reg=None, r_trim=0.):
        """
        Parameters
        ----------
        """

        if mask_reg == 'inner':
            inner = np.sqrt(self.xx_slice**2 + self.yy_slice**2) <= r_trim
            self.func_slice = np.ma.masked_where(inner, self.func_slice)
            #self.func_slice[inner] = np.ma.masked
            return

        if mask_reg == 'outer':
            outer = np.sqrt(self.xx_slice**2 + self.yy_slice**2) >= r_trim
            self.func_slice = np.ma.masked_where(outer, self.func_slice)
            return


    ############################################################################
    def make_plot(self, fig=None, ax=None, show_plot=True, plottitle=None, notitle=False, plot_horizon=False, plot_grid=False, background='black', minf=None, maxf=None, pminf=None, pmaxf=None, nlev=256, nticks=10, ticks_range='minmax', make_bbox=False, axislabels=True, xticks=None, yticks=None, yticksloc='left', nwin=0, retfunc=True, freescale=False, scale='on', use_contourf=True, other_data=None, artists=None, pnlev=10, pfontsize=8, plot_labels=True, nperplot=20, visc_circles=False, plot_slosh_box=False, R1T0=None, set_axis_off=False, cmap=None, up_and_under=True):
        """
        Parameters
        ----------

        plot_grid : bool
            plot numerical grid?

        Returns
        -------
        """

        print("\n-----<<<  make_plot  >>>-----")
        if show_plot is False:
            # Turn interactive plotting off
            plt.ioff()
        else:
            plt.ion()

        if self.use_num_coord:
            plot_horizon = False
            plot_grid = False

        if background == 'black':
            plt.style.use('dark_background')
            grid_colour = 'w'
        else:
            grid_colour = 'k'

        if plottitle is None:
            self.plottitle = self.funcname
            if self.dolog:
                self.plottitle = 'log10|{}|'.format(self.plottitle)
        else:
            self.plottitle = plottitle

        if make_bbox:
            if self.cart_coords:
                other_bbox = [np.min(self.xx_slice), np.max(self.xx_slice), np.min(self.yy_slice), np.max(self.yy_slice)]
                out_artists = [matplotlib.patches.Rectangle((other_bbox[0], other_bbox[2]), (other_bbox[1]-other_bbox[0]), (other_bbox[3]-other_bbox[2]), fill=False, color='k', linestyle='dashed')]
            else:
                self.rmin = np.min(self.rr_slice)
                self.rmax = np.max(self.rr_slice)
                inner_circle = plt.Circle((self.xx_center, self.yy_center), self.rmin, color='black', fill=False, linestyle='dashed')
                outer_circle = plt.Circle((self.xx_center, self.yy_center), self.rmax, color='black', fill=False, linestyle='dashed')
                out_artists = [inner_circle, outer_circle]


        ######################## set default parameters #######################

        if self.xmax is None:
            if self.use_num_coord:
                self.xmax = len(self.func_slice[:,0])
            elif self.cart_coords:
                self.xmax = np.max(self.xx_slice)
            else:
                if self.irad is not None:
                    self.xmax = np.pi
                else:
                    self.xmax = self.rmax

        if self.ymax is None:
            if self.use_num_coord:
                self.ymax = len(self.func_slice[0,:])
            elif self.cart_coords:
                self.ymax = np.max(self.yy_slice)
            else:
                if self.irad is not None:
                    self.ymax = 2.*np.pi
                else:
                    self.ymax = self.rmax

        if self.zmax is None:
            if self.use_num_coord:
                self.zmax = len(self.func_slice[0,:])
            elif self.cart_coords :
                self.zmax = np.max(self.yy_slice)
            else:
                self.zmax = 0.5 * self.rmax

        if self.xmin is None:
            if self.use_num_coord:
                self.xmin = 0
            elif self.cart_coords :
                self.xmin = np.min(self.xx_slice)
            else:
                if self.ith is not None:
                    self.xmin = -self.rmax
                else:
                    self.xmin = 0

        if self.ymin is None:
            if self.use_num_coord:
                self.ymin = 0
            elif self.cart_coords :
                self.ymin = np.min(self.yy_slice)
            else:
                if self.irad is not None:
                    self.ymin = 0.
                else :
                    self.ymin = -self.rmax

        if self.zmin is None:
            if self.use_num_coord:
                self.zmin = 0
            elif self.cart_coords :
                self.zmin = np.min(self.yy_slice)
            else:
                self.zmin = -0.5 * self.rmax

        if minf is None:
            minf = self.func_slice.min()
        if maxf is None:
            maxf = self.func_slice.max()

        print("minf maxf = ", minf, maxf)
        print("del range = ", (minf-maxf))
        if harm.isclose(minf, maxf):
            print("minf maxf here here = ", minf, maxf)
            if harm.isclose(minf,0.):
                maxf = 0.1
            else:
                if minf < 0.:
                    maxf = 0.9 * minf
                else:
                    maxf = 1.1 * minf
            print("minf maxf here here2 = ", minf, maxf)

        if self.plot_potential:
            if pminf is None:
                pminf = self.pot_slice.min()
            if pmaxf is None:
                pmaxf = self.pot_slice.max()


        # we can now do the plotting
        if fig is None:
            self.fig = plt.figure(nwin)
            self.fig.clf()
            if ax is None:
                if freescale:
                    # self.ax = self.fig.add_subplot(111, fc=background)
                    self.ax = self.fig.add_subplot(111)
                else:
                    # self.ax = self.fig.add_subplot(111, aspect='equal',fc=background)
                    self.ax = self.fig.add_subplot(111, aspect='equal')
            else:
                self.ax = ax
        else:
            self.fig = fig
            self.fig.clf()
            if ax is None:
                if freescale:
                    # self.ax = self.fig.add_subplot(111, fc=background)
                    self.ax = self.fig.add_subplot(111)
                else:
                    # self.ax = self.fig.add_subplot(111, aspect='equal',fc=background)
                    self.ax = self.fig.add_subplot(111, aspect='equal')
            else:
                self.ax = ax

        if axislabels:
            if self.use_num_coord:
                if self.ith is not None:
                    self.ax.set_xlabel('x3')
                    self.ax.set_ylabel('x1')
                    num_bbox=[self.zmin, self.zmax, self.xmin, self.xmax]
                else:
                    if self.iph is not None:
                        self.ax.set_xlabel('x2')
                        self.ax.set_ylabel('x1')
                        num_bbox=[self.ymin, self.ymax, self.xmin, self.xmax]
                    else:
                        self.ax.set_xlabel('x3')
                        self.ax.set_ylabel('x2')
                        num_bbox=[self.zmin, self.zmax, self.ymin, self.ymax]
        else:
            self.ax.set_xlabel('')
            self.ax.set_ylabel('')


        self.ax.set_autoscalex_on(False)
        self.ax.set_autoscaley_on(False)

        if self.cart_coords:
            self.ax.axis([self.xmin, self.xmax, self.ymin, self.ymax])
        else:
            if self.use_num_coord:
                self.ax.axis(num_bbox)
            else:
                if self.ith is not None:
                    self.ax.axis([self.xmin, self.xmax, self.ymin, self.ymax])
                else:
                    if self.iph is None:
                        self.ax.axis([self.xmin, self.xmax, self.ymin, self.ymax])
                    else :
                        self.ax.axis([self.xmin, self.xmax, self.zmin, self.zmax])


        if xticks is not None:
            self.ax.set_xticks(xticks)
        if yticks is not None:
            self.ax.set_yticks(yticks)
        if yticksloc == 'right':
            self.ax.yaxis.tick_right()

        if cmap is None:
            cmap = plt.cm.Spectral_r
            #cmap = plt.cm.gray
        darkgrey = '#202020'
        purple   = '#800080'
        darkblue = '#0000A0'
        if up_and_under:
            extend = 'both'
            Colormap.set_under(cmap, color=darkgrey)
            Colormap.set_over(cmap, color='w')
        else:
            extend = 'neither'

        print(" min max func     = ", np.min(self.func_slice), np.max(self.func_slice))
        print(" min max xx_slice = ", np.min(self.xx_slice), np.max(self.xx_slice))
        print(" min max yy_slice = ", np.min(self.yy_slice), np.max(self.yy_slice))
        #print(" rr_slice = ", xx_slice)
        #print(" yy_slice = ", yy_slice)
        #print(" func_slice = ", self.func_slice)
        print(" ith = ", self.ith)
        print(" iph = ", self.iph)

        print("minf maxf = ", minf, maxf)

        levels = np.linspace(minf, maxf, nlev)
        if self.use_num_coord:
            CS = self.ax.contourf(self.func_slice, levels, cmap=cmap, extend=extend)
        else:
            if use_contourf:
                CS = self.ax.contourf(self.xx_slice, self.yy_slice, self.func_slice, levels, cmap=cmap, extend=extend)
            else:
                CS = self.ax.pcolormesh(self.xx_slice, self.yy_slice, self.func_slice, vmin=np.min(levels), vmax=np.max(levels), cmap=cmap)


        if artists is not None:
            for artist in artists:
                self.ax.add_artist(artist)

        if other_data is not None:
            if self.use_num_coord:
                CS_other = self.ax.contourf(other_data[0], levels, cmap=cmap, extend=extend)
            else:
                CS_other = self.ax.contourf(other_data[0], other_data[1], other_data[2], levels, cmap=cmap, extend=extend)


        if self.plot_potential:
            matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
            plevels = np.linspace(pminf, pmaxf, pnlev)
            CS_pot = self.ax.contour(self.xx_slice, self.yy_slice, self.pot_slice, plevels, colors='k', extend=extend)
            if plot_labels:
                plt.clabel(CS_pot, fontsize=pfontsize)

        if plot_grid:
            for i in range(len(self.xx_slice[0,:]))[::nperplot]:
                self.ax.plot(self.xx_slice[:,i], self.yy_slice[:,i], color=grid_colour)

            for i in range(len(self.xx_slice[:,0]))[::nperplot]:
                self.ax.plot(self.xx_slice[i,:], self.yy_slice[i,:], color=grid_colour)


        if self.fieldlines is not None:
            fieldlevels = np.linspace(self.aphi.min() * 1.2, self.aphi.max(), self.fieldlines[0])
            # fieldlevels = np.linspace(0.004, self.aphi.max(), self.fieldlines[0])
            newfieldlevels = fieldlevels[:]
            CS_field = self.ax.contour(self.xx_slice, self.yy_slice, self.aphi, newfieldlevels, colors='k', extend=extend, linewidths=.3)
            # plt.clabel(CS_field)


        if self.plot_bhs_traj:
            if plot_horizon:
                green_led = '#5DFC0A'
                #rhor = 0.5 #only for equal mass rat
                # rhor1 = self.m_bh1
                # rhor2 = self.m_bh2
                chi_bh1, chi_bh2 = np.loadtxt(self.trajfile,usecols=(3,6),unpack=True)
                chi_bh1 = chi_bh1[0]
                chi_bh2 = chi_bh2[0]
                rhor1 = np.sqrt( 2. * self.m_bh1 * ( self.m_bh1 + np.sqrt( np.power(self.m_bh1, 2.) - np.power((self.m_bh1 * chi_bh1), 2.))))
                rhor2 = np.sqrt( 2. * self.m_bh2 * ( self.m_bh2 + np.sqrt( np.power(self.m_bh2, 2.) - np.power((self.m_bh2 * chi_bh2), 2.))))

                #actually plot the BHS
                ybh1_bak = self.ybh1
                ybh2_bak = self.ybh2
                xbh1_bak = self.xbh1
                xbh2_bak = self.xbh2

                if not self.cart_coords:
                    if self.ith is None:
                        if self.iph is None:
                            phase1 = np.arctan2(self.ybh1, self.xbh1)
                            phase2 = np.arctan2(self.ybh2, self.xbh2)
                            #renormalize phase s.t phase \in (0,2pi)
                            if phase1 < 0.:
                                phase1 = 2. * np.pi - np.abs(phase1)
                            if phase2 < 0.:
                                phase2 = 2. * np.pi - np.abs(phase2)

                            self.xbh1 = 0.5 * np.pi
                            self.xbh2 = 0.5 * np.pi
                            self.ybh1 = phase1
                            self.ybh2 = phase2
                            rhor1 = 0.025 * np.pi
                            rhor2 = rhor1
                        else:
                            self.ybh1 = self.zbh1
                            self.ybh2 = self.zbh2

                bh1 = plt.Circle((self.xbh1, self.ybh1), rhor1, color=green_led)
                bh2 = plt.Circle((self.xbh2, self.ybh2), rhor2, color=green_led)
                #bh1 = plt.Circle((xbh1,ybh1), rhor1, color='k')
                #bh2 = plt.Circle((xbh2,ybh2), rhor1, color='k')
                #bh1 = plt.Circle((xbh1,ybh1), rhor1, color='k',fill=False)
                #bh2 = plt.Circle((xbh2,ybh2), rhor2, color='k',fill=False)

                self.ax.add_artist(bh1)
                self.ax.add_artist(bh2)

            if visc_circles:
                sep = np.sqrt( (self.xbh1 - self.xbh2) * (self.xbh1 - self.xbh2) + (self.ybh1 - self.ybh2) * (self.ybh1 - self.ybh2) )
                print("sep = ", sep)
                #isco1 = plt.Circle((xbh1,ybh1),3.,color=green_led,fill=False)
                #isco2 = plt.Circle((xbh2,ybh2),3.,color=green_led,fill=False)
                #isco1  = plt.Circle((xbh1,ybh1),5.*m_bh1,color=grid_colour,fill=False)
                #isco2  = plt.Circle((xbh2,ybh2),5.*m_bh2,color=grid_colour,fill=False)
                #tidal1 = plt.Circle((xbh1,ybh1),0.3*np.power(m_bh2/m_bh1,-0.3)*sep,color=grid_colour,fill=False,linestyle='dashed')
                #tidal2 = plt.Circle((xbh2,ybh2),0.3*np.power(m_bh2/m_bh1, 0.3)*sep,color=grid_colour,fill=False,linestyle='dashed')

                isco1 = plt.Circle((self.xbh1, self.ybh1), 5. * self.m_bh1, color='k', fill=False,linestyle='solid')
                isco2 = plt.Circle((self.xbh2, self.ybh2), 5. * self.m_bh2, color='k', fill=False,linestyle='solid')
                #dsig = plt.Circle((xbh1,ybh1),0.22*sep,color='white',fill=False,linestyle='solid')
                tidal1 = plt.Circle((self.xbh1, self.ybh1), 0.3 * np.power(self.m_bh2 / self.m_bh1, -0.3) * sep, color='k', fill=0, linestyle='dashed')
                tidal2 = plt.Circle((self.xbh2, self.ybh2), 0.3 * np.power(self.m_bh2 / self.m_bh1, 0.3) * sep,color='k', fill=0,linestyle='dashed')

                #cool1 = plt.Circle((xbh1,ybh1),0.45*np.power(m_bh2/m_bh1,-0.3)*sep,color='k',fill=False,linestyle='solid')
                #cool2 = plt.Circle((xbh2,ybh2),0.45*np.power(m_bh2/m_bh1, 0.3)*sep,color='k',fill=False,linestyle='solid')

                #Keep these
                self.ax.add_artist(isco1)
                self.ax.add_artist(isco2)
                self.ax.add_artist(tidal1)
                self.ax.add_artist(tidal2)
                #self.ax.add_artist(dsig)
                #self.ax.add_artist(cool1)
                #self.ax.add_artist(cool2)

                if R1T0 is not None:
                    Transinner1 = plt.Circle((self.xbh1, self.ybh1), R1T0, color='black', fill=False, linestyle='dashdot')
                    Transinner2 = plt.Circle((self.xbh2, self.ybh2), R1T0, color='black', fill=False, linestyle='dashdot')

                    self.ax.add_artist(Transinner1); ax.add_artist(Transinner2)

                #FUDGE1 = plt.Circle((xbh1,ybh1),0.1*m_bh1,color='white',fill=False,linestyle='dashed')
                #FUDGE2 = plt.Circle((xbh2,ybh2),0.1*m_bh2,color='white',fill=False,linestyle='dashed')
                #self.ax.add_artist(FUDGE1)
                #self.ax.add_artist(FUDGE2)

            self.ax.plot(self.xbh1, self.ybh1)
            self.ax.plot(self.fxbh1(np.linspace(0.2, self.time+0.3, num=self.itime+1)), self.fybh1(np.linspace(0.2, self.time+0.3, num=self.itime+1)), ls='--', c='k', lw=1.)
            self.ax.plot(self.fxbh2(np.linspace(0.2, self.time+0.3, num=self.itime+1)), self.fybh2(np.linspace(0.2, self.time+0.3, num=self.itime+1)), ls='--', c='k', lw=1.)
            self.xbh1 = xbh1_bak
            self.xbh2 = xbh2_bak
            self.ybh1 = ybh1_bak
            self.ybh2 = ybh2_bak
        else:
            if plot_horizon:
                if self.ubh_horizon:
                    h5file = h5py.File(self.fullname, 'r')
                    rhor = h5file['Header/Grid/r_horizon'][0]
                    iscor = h5file['Header/Grid/r_isco'][0]
                    # rhor = 2.
                    # iscor = 6.
                    bh = plt.Circle((0,0), rhor, color='k')
                    self.ax.add_artist(bh)
                    # isco = plt.Circle((0,0),iscor,color='k',linestyle='dashed',fill=False)
                    # self.ax.add_artist(isco)
                    h5file.close()

                # TODO: Check if this if clause is up to date
                yellow_neon = '#F3F315'
                r0 = 2.

                if self.ith is not None:
                    phi0 = np.linspace(0, 2. * np.pi, 100)
                    xh1 = r0 * np.cos(phi0)
                    yh1 = r0 * np.sin(phi0)
                    self.ax.plot(xh1, yh1, color=yellow_neon)
                else:
                    theta = np.linspace(np.pi * 0.5, -np.pi * 0.5, 100)
                    self.ax.plot(r0 * np.cos(theta), r0 * np.sin(theta), color=yellow_neon)

        if plot_slosh_box:
            if not self.corotate:
                print("Must Corotate for sloshing patches")
            else:
                sep = np.sqrt( (self.xbh1 - self.xbh2) * (self.xbh1 - self.xbh2) + (self.ybh1 - self.ybh2) * (self.ybh1 - self.ybh2) )
                slosh = matplotlib.patches.Rectangle((0 - 0.2 * sep, 0 - 0.3 * sep), 0.4 * sep, 0.6 * sep, fill=False,color='k', linestyle='dashed')
                self.ax.add_artist(slosh)

        if notitle is False:
            title = self.plottitle + '  t =  {:0.1f}'.format(self.time)
            self.ax.set_title('{}'.format(title))

        # ticks for the colorbar
        if ticks_range == 'best':
            rticks = ticker.MaxNLocator(nticks+1)
        elif ticks_range == 'minmax':
            rticks = np.linspace(minf, maxf, nticks)

        if scale != 'off':
            if self.dolog is True:
                CB = self.fig.colorbar(CS, ax=self.ax, ticks=rticks, format='%0.1f') #colorbar removal dennis here
            else:
                CB = self.fig.colorbar(CS, ax=self.ax, ticks=rticks, format='%1.1e') #colorbar removal dennis here
            tick_locator = ticker.MaxNLocator(nbins=6)
            CB.locator = tick_locator
            CB.update_ticks()

        if set_axis_off:
            self.ax.set_axis_off()#axis off here dennis

        if show_plot:
            plt.show()

        if make_bbox:
            return out_artists
        else:
            return


    ############################################################################
    def print_plot(self, dirout=None, savefmt='png', namesuffix='', dpi=300):
        '''
        This function saves the plot

        Parameters
        ----------

        dirout : str
        The directory where the plot will be saved. Default is the current working directory.

        savefmt : str
        Format of the plot. Must be any of these values: 'png', 'eps', 'ps', 'pdf', 'svg'. Default 'png'
        '''
        print("--> Running: print_plot()")
        if dirout is None:
            dirout = os.getcwd()

        if self.ith is not None:
            figname = dirout + '/' + self.plottitle + namesuffix + '_it=' + self.timeid + '_ith=' + '{:04d}'.format(self.ith)
        else:
            if self.iph is not None:
                figname = dirout + '/' + self.plottitle + namesuffix + '_it=' + self.timeid + '_iph=' + '{:04d}'.format(self.iph)
            else:
                figname = dirout + '/' + self.plottitle + namesuffix + '_it=' + self.timeid + '_irad=' + '{:04d}'.format(self.irad)

        print("######################################")
        print("Saving plot here:")
        print("     {}".format(dirout))
        print("Plot name:")
        print("     {}".format(figname))
        if savefmt not in ['png', 'eps', 'ps', 'pdf', 'svg']:
            savefmt = 'png'

        self.fig.savefig('{0}.{1}'.format(figname, savefmt), dpi=300, bbox_inches='tight')

        if self.ith is not None:
            print("th = ", self.th_slice[0,0])

        if self.iph is not None:
            print("ph = ", self.ph_slice[0,0])

        if self.irad is not None:
            print("r  = ", self.rr_slice[0,0])
        print("================")

        return



################################################################################
################################################################################
class flux(object):
    def flux_integrate(self, ibeg, iend, irad):

        nt = iend - ibeg

        # dirout='./'
        # dumpbase = 'KDHARM0'
        # timeid = '%05d' %itime #monopole cleaner dumps

        # irad = 590
        slice_obj = (slice(irad, irad+1, 1), slice(2, 14, 1), slice(None,None,None))

        id = 0
        for self.itime in np.arange(ibeg, iend):
            # timeid = '{06d}'.format(self.itime) #harm HDF5 dumps
            # timeid = '{05d}'.format(self.itime) #monopole cleaner dumps
            # dumpfile  = dirout + 'dumps/' + 'KDHARM0.' +  timeid + '.h5'
            h5file   = h5py.File(self.dumpfile, 'r')

            if self.itime == ibeg:
                self.idim, self.jdim, self.kdim, xp1, xp2, xp3  = harm.get_grid(h5file)
                total_out = np.zeros((nt, self.kdim))
                mflux_out = np.zeros((nt, self.kdim))
                hyd_out = np.zeros((nt, self.kdim))
                em_out = np.zeros((nt, self.kdim))
                t_out = np.zeros(nt)
                ph_tmp = harm.read_3d_hdf5_slice('x3', slice_obj,h5file)
                ph_out = np.squeeze(ph_tmp[0,:])
                ph_tmp = 0

            t_out[id] = h5file['/Header/Grid/t'][0]
            total_r, mflux_r, hyd_flux_r, em_flux_r = harm.calc_fluxes(slice_obj, h5file)
            total_out[id,:] = np.sum(total_r, axis=0)
            mflux_out[id,:] = np.sum(mflux_r, axis=0)
            hyd_out[id,:] = np.sum(hyd_flux_r, axis=0)
            em_out[id,:] = np.sum(em_flux_r, axis=0)

            id += 1
            h5file.close()


        outh5 = h5py.File('fluxes.h5', 'w')
        dset = outh5.create_dataset('total_flux', data=total_out)
        dset = outh5.create_dataset('mflux_flux', data=mflux_out)
        dset = outh5.create_dataset('hydro_flux', data=hyd_out)
        dset = outh5.create_dataset('em_flux', data=em_out)
        dset = outh5.create_dataset('phi', data=ph_out)
        dset = outh5.create_dataset('times', data=t_out)
        outh5.close()

        return



    ############################################################################
    ############################################################################
    def flux_plot(self):

        dirout = './plots/'

        h5file = h5py.File('fluxes_590.h5','r')
        xout = h5file['phi'][0]
        yout = h5file['times'][0]

        total_out = h5file['total_flux'][...]
        mflux_out = h5file['mflux_flux'][...]
        hyd_out = h5file['hydro_flux'][...]
        em_out = h5file['em_flux'][...]

        # trajfile = './dumps/my_bbh_trajectory.out'
        tbh_traj, xbh1_traj, ybh1_traj, xbh2_traj, ybh2_traj = np.loadtxt(self.trajfile, usecols=(0,4,5,6,7), unpack=True) #old metrics
        phase1 = np.arctan2(ybh1_traj, xbh1_traj)
        phase2 = np.arctan2(ybh2_traj, xbh2_traj)

        loc = phase1 < 0.
        phase1[loc] = 2.*np.pi - np.abs(phase1[loc])
        loc = phase2 < 0.
        phase2[loc] = 2.*np.pi - np.abs(phase2[loc])


        [xmin, xmax, ymin, ymax] = [np.min(xout),np.max(xout),np.min(yout),np.max(yout)]

        dolog = False

        minf = 0.1
        maxf = 4

        if dolog :
            minf = np.log10(minf)
            maxf = np.log10(maxf)

        nticks = 10.
        nlev = 256.
        nwin=0
        savepng = 1
        cmap=plt.cm.Spectral_r

        func = total_out
        if dolog:
            func[func == 0] += 1.e-40       # so that there is no log of zero...
            func = np.log10(np.abs(func))
        funcname = 'total_flux'
        fignames = [dirout + funcname + '-'+'phi-vs-t']
        figs = [plt.figure(nwin)]
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        # ax.set_title('%s' %title)
        ax.set_xlabel('phi')
        ax.set_ylabel('t [M]')
        levels = np.linspace(minf,maxf,nlev)
        rticks = np.linspace(minf,maxf,nticks)
        ax.axis([xmin,xmax,ymin,ymax])
        ax.plot(phase1,tbh_traj,'w,')
        ax.plot(phase2,tbh_traj,'w,')
        CP = ax.contourf(xout, yout,func,levels,cmap=cmap,extend='both')
        figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

        nwin+=1
        func = hyd_out
        if dolog:
            func[func == 0] += 1.e-40       # so that there is no log of zero...
            func = np.log10(np.abs(func))
        funcname = 'hydro_flux'
        fignames = np.append(fignames,dirout + funcname + '-'+'phi-vs-t')
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        # ax.set_title('%s' %title)
        ax.set_xlabel('phi')
        ax.set_ylabel('t [M]')
        levels = np.linspace(minf,maxf,nlev)
        rticks = np.linspace(minf,maxf,nticks)
        ax.axis([xmin,xmax,ymin,ymax])
        ax.plot(phase1,tbh_traj,'w,')
        ax.plot(phase2,tbh_traj,'w,')
        CP = ax.contourf(xout, yout, func, levels, cmap=cmap, extend='both')
        figs[-1].colorbar(CP, ticks=rticks, format='%1.4g')

        nwin+=1
        func = em_out
        if dolog:
            func[func == 0] += 1.e-40       # so that there is no log of zero...
            func = np.log10(np.abs(func))
        funcname = 'em_flux'
        fignames = np.append(fignames,dirout + funcname + '-'+'phi-vs-t')
        figs = np.append(figs,plt.figure(nwin))
        figs[-1].clf()
        ax = figs[-1].add_subplot(111)
        # ax.set_title('%s' %title)
        ax.set_xlabel('phi')
        ax.set_ylabel('t [M]')
        levels = np.linspace(minf,maxf,nlev)
        rticks = np.linspace(minf,maxf,nticks)
        ax.axis([xmin,xmax,ymin,ymax])
        ax.plot(phase1,tbh_traj,'w,')
        ax.plot(phase2,tbh_traj,'w,')
        CP = ax.contourf(xout, yout,func,levels,cmap=cmap,extend='both')
        figs[-1].colorbar(CP,ticks=rticks,format='%1.4g')

        print("fignames = ", fignames)

        if savepng :
            for i in np.arange(len(figs)) :
                figs[i].savefig('{}.png'.format(fignames[i]), dpi=300)

        print("All done ")
        sys.stdout.flush()


        return
